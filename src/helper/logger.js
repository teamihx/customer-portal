const winston = require('winston');
const moment = require('moment');

const appSettings = {
    winston: {
        sillyLogConfig: {
            level: 'silly',
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json(),
                winston.format.printf(info => {
                    const dateObj=(`time`, moment().format('DD/MMM/YYYY:HH:mm:ss'));
                    return `${dateObj} ${info.level.toUpperCase()} : ${info.message}`
                })
            ),
            transports: [
                new winston.transports.File({
                    filename: './logs/cxportal.log'
                }),
                new winston.transports.Console()
            ]
        },
    },
    log4js: {
        traceLogConfig: {
            appenders: {
                fileAppender: { type: 'file', filename: './logs/trace.log'},
                consoleAppender: { type: 'console' }
            },
            categories: {
                default: { appenders: ['fileAppender', 'consoleAppender'], level: 'trace'}
            }
        }
    }
};

module.exports = appSettings;