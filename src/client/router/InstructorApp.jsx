import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import CompanyComponent from '../pages/company/companyConfig/CompanyComponent.jsx';
import LoginComponent from '../pages/authentication/LoginComponent';
import FPHComponent from '../pages/fphTrips/generic/FPHComponent';
import LogoutComponent from '../pages/authentication/LogoutComponent';
import AuthenticationService from '../services/authentication/AuthenticationService';
import AccessDenied from '../pages/authentication/AccessDenied';
import GstComponent from '../pages/company/gst/GstComponent';
import PagenotFound from 'Pages/common/PagenotFound'
import log from 'loglevel';
import axios from 'axios';
import TripDetails from '../pages/trips/generic/TripDetails.jsx';
import TripJson from '../pages/trips/generic/TripJson.jsx';
import FPHCancellationQueue from '../pages/workflow/fph/queues/FPHCancellationQueue';
export const URL_COMPANY = 'company'
export const USER_AUTH_DATA = 'userAuthData'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
export const contextPath=process.env.contextpath;
export const TRIP_REF = 'tripRef'
export const COMP_CONFIG_ROLES = 'compConfigRoles'
import FPHPrintEticket from '../pages/trips/fph/FPHPrintEticket';
import OtherTripSearch from "../pages/trips/fph/OtherTripSearch"
import FPHPrintVoucher from '../pages/trips/fph/FPHPrintVoucher';

import Button from "Components/buttons/button.jsx";
import Ldpopup from 'Components/Ldpopup.jsx';
import Icon from "Components/Icon";
import PageLoad from "../pages/common/PageLoad.jsx";
import HotelCancelTrip from '../pages/trips/hotel/HotelCancelTrip.jsx';
class InstructorApp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loginEnable: false,
            compRoleEnable: false,
            gstRoleEnable:false,
            authData:'',
            auth:'',
            checkAuth:false,
            tripsRoleEnable:false,
            username:"",
            password:"",
            checkLdap:false,
            loading: false,
            hasLoginFailed: false,
            showUserName: false,
            showPassword: false,
            success:false
        }
        this.handleChange = this.handleChange.bind(this);
        this.checkLdapAuth=this.checkLdapAuth.bind(this);

    }

    handleChange(event) {
        this.setState({
          [event.target.name]: event.target.value
        });
      }
      checkLdapAuth(event) {
        try{
            event.preventDefault();
            this.setState({ loading: true });
            console.log('CheckLdap Authentication start...'+this.state.username);
            if(this.state.username!=="" && this.state.password!==""){
            AuthenticationService.checkLdapAuthentication(this.state.username,this.state.password).then(response => {
                    console.log('LDAP RESPONSE'+ JSON.stringify(response.data));
                    if(response!==null && response!==undefined && response.data.status!==undefined && response.data.status===200){
                      console.log('Ldap Authenticated Successfully....');
                      AuthenticationService.saveLdapToken(this.state.username,this.state.password,response.data.data).then(resp => {
					 // console.log('Mongo persistance RESPONSE'+ JSON.stringify(resp));
                        if(resp.status===200){
                          console.log('Ldap Key Saved Successfully....');
                        }
                        this.setState({ success: true });
 						this.setState({ loading: true });
                      //this.props.history.push(`${contextPath}/login`);
                       window.location = contextPath+"/login"
                      }).catch(function(error) {                        
                        this.setState({ loading: false });
                        log.error('Exception occured in checkLdapAuthentication call---'+error);
                      });
                     
 					// <Route path={contextPath+"/login"}exact component={LoginComponent} />
                   }else{
                    this.setState({ hasLoginFailed: true });
                    this.setState({ loading: false });
                   }
                   //window.location = contextPath + "/login";
                  }).catch(function(error) {
                    log.error('Exception occured in checkLdapAuthentication1 call---'+error);
                    
                  });
                }else{
                  if (this.state.username === "") {
                    this.setState({ showUserName: true });
                    this.setState({ hasLoginFailed: false });
                  } else {
                    this.setState({ showUserName: false });
                  }
                  if (this.state.password === "") {
                    this.setState({ showPassword: true });
                    this.setState({ hasLoginFailed: false });
                  } else {
                    this.setState({ showPassword: false });
                  }
                  this.setState({ loading: false });
                }
        }catch(err){
            log.error('Exception occured in removeTag function---'+err);
        }
    }

    createUrlHistory(){
        var url = window.location.href;
        var filename = url.substring(url.lastIndexOf('/') + 1);
        var urlString = "/" + filename;
        let urlArray=url.split("/")
        let tripRef=urlArray[urlArray.length-2]
        if(!url.includes("/login")){
            if(url.includes("/trips") ){
            var tripsurl="trips";
            localStorage.setItem(URL_COMPANY, tripsurl);
            if(filename==='eTicket'){
                localStorage.setItem(TRIP_REF, tripRef);
            }else{
                localStorage.setItem(TRIP_REF, filename);
            }
            }else{
            localStorage.setItem(URL_COMPANY, urlString);
            }
        }
    }

    UNSAFE_componentWillMount(){
       // console.log('Login componentWillMount');
        //this.checkUserAuthrization();
            
            AuthenticationService.checkLdapAuthAvail().then(response => {
            if(response!==null && response!=="" && response!==undefined){
                
                if(response!==undefined && response.data!=="" && response.data.status!==undefined && response.data.status===200){
                this.checkUserAuthrization();
                this.setState({checkLdap:false})
            }else{
                this.setState({checkLdap:true})
            }
            }else{
                this.setState({checkLdap:true})
            }
            this.createUrlHistory();
        }).catch(function (error) {
            if (error) {
                var resp = {
                    status: '404',
                    msg:'Resource Not Found'
                }                
            }
            log.error('Exception occured in UNSAFE_componentWillMount ---'+error);
        });

        }
    

     checkUserAuthrization(){
         try{
        var url = window.location.href;
        var filename = url.substring(url.lastIndexOf('/') + 1);
        var urlString = "/" + filename;
        let urlArray=url.split("/")
        let tripRef=urlArray[urlArray.length-2]
       this.setState({tripRef:tripRef})
       // console.log('....Entered URL.....' + url);
        if(urlString!=='/logout' && (urlString==="/gst" || urlString==="/config" || urlString==="/company" || url.includes("/trips"))){
            this.createUrlHistory();
        AuthenticationService.getCookieValue()
            .then(response => {
              var authKey=response.data;
        if(authKey!==null && authKey!==""){
            console.log('.......Auth key Available..........');
            var authData=localStorage.getItem(USER_AUTH_DATA);
            if(authData!==null && authData!==""){
                var obj = JSON.parse(authData);
                AuthenticationService.createRolesAndPermissions(url);
                if(obj.data!==null && obj.data.valid){
                console.log('User is Available...... ')
                //Getting ROLES DATA from local storage
                let tripsObj=JSON.parse(localStorage.getItem(COMP_CONFIG_ROLES));
                var comp =false;
                var gst =false;
                if(tripsObj!==null && tripsObj.companyConfigRoles!==null && tripsObj.companyConfigRoles!==undefined){
                    comp=tripsObj.companyConfigRoles.COMPANY_ROLE_ENABLE;
                    gst= tripsObj.companyConfigRoles.GST_ROLE_ENABLE;
                }
                if (urlString === '/company'){
                    if (comp){
                        this.setState({compRoleEnable:true})
                    }else{
                        window.location = contextPath+"/accessdenied"
                    }
                }else if(urlString === '/config'){
                        if (comp){
                            this.setState({compRoleEnable:true})
                        }else{
                            window.location = contextPath+"/accessdenied"
                        }
                }else if (urlString === '/gst'){
                        if (gst){
                            this.setState({gstRoleEnable:true})
                        }
               }else if(tripsurl!=="trips" && (urlString === '/login' || urlString === contextPath || urlString.includes("/"))){
                if((comp && gst) || comp){
                    window.location = contextPath+"/company"
                }else if(gst){
                    window.location = contextPath+"/company/gst"
                }
                }else if(tripsurl!==null && tripsurl==="trips"){
                    this.setState({tripsRoleEnable:true})
                    localStorage.removeItem(TRIP_REF);
                }
            }else{
                this.setState({compRoleEnable:false})
            }
            }else{
           // console.log('Calling user Authrization service...... ') 
            AuthenticationService.checkRegisterAutherization()
                .then(response => {
                    var authResponse = response.data;
                    var obj = JSON.parse(authResponse);
                    if (obj!==null && obj.status===200){
                        if(obj.data!==null && obj.data.valid){
                        console.log('Usr Authrization Status: ' + obj.data.valid +" User Name :"+obj.data.email)
                        localStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME,obj.data.email);
                        localStorage.setItem(USER_AUTH_DATA,authResponse);
                        AuthenticationService.createRolesAndPermissions(url);
                        //Getting ROLES DATA from local storage
                        let tripsObj=JSON.parse(localStorage.getItem(COMP_CONFIG_ROLES));
                        var comp =false;
                        var gst =false;
                        if(tripsObj!==null && tripsObj.companyConfigRoles!==null && tripsObj.companyConfigRoles!==undefined){
                            comp=tripsObj.companyConfigRoles.COMPANY_ROLE_ENABLE;
                            gst= tripsObj.companyConfigRoles.GST_ROLE_ENABLE;
                        }
                        if (urlString === '/company'){
                            if (comp){
                                this.setState({compRoleEnable:true})
                            }else{
                                 window.location = contextPath+"/accessdenied"
                            }
                        }else if(urlString == '/config'){
                        if (comp){
                            this.setState({compRoleEnable:true})
                        }
                        }else if (urlString === '/gst'){
                        if (gst){
                            this.setState({gstRoleEnable:true})
                        }
                        }else if(tripsurl!=="trips" && (urlString === '/login' || urlString === contextPath || urlString.includes("/"))){
                            if((comp && gst) || comp){
                               window.location = contextPath+"/company"
                            }else if(gst){
                                window.location = contextPath+"/company/gst"
                            }
                        }else if(tripsurl!==null && tripsurl==="trips"){
                            this.setState({tripsRoleEnable:true})
                            localStorage.removeItem(TRIP_REF);
                        }
                    }else{
                        this.setState({compRoleEnable:false})
                    }
                    }else{
                        this.setState({compRoleEnable:false})
                    }
                }).catch(function (error) {
                    if (error.response) {
                        var resp = {
                            status: '404',
                            msg:'Resource Not Found'
                        } 
                       // console.log('Auth response Failed..........'+resp)
                        this.setState({compRoleEnable:false})                       
                        
                    }
                    log.error('Exception occured in checkRegisterAutherization ---'+error);
                });
            }
        }else{
           if(urlString!=='/login'){
            console.log('Auth response Failed..........'+urlString)
            this.setState({compRoleEnable:false})
            this.setState({gstRoleEnable:false})
            window.location = contextPath+"/login"
           }
            
        }
    
}).catch(function (error) {
    if (error.response) {
        var resp = {
            status: '404',
            msg:'Resource Not Found'
        } 
    }
    log.error('Exception occured in InstructorApp checkRegisterAutherization1 function---'+error);
    
});
}
}catch(err){
    
    log.error('Exception occured in InstructorApp checkUserAuthrization function---'+err);
}
}
    render() {
        if(this.state.checkLdap && !this.state.success){
            return (
                <Ldpopup size="sm" viewType="layover">
                <form className="form-group" onSubmit={e => this.checkLdapAuth(e)}>
                <h4>Ldap Authentication</h4>
                {this.state.showUserName && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Enter User name
                </div>
              )}
              {this.state.showPassword && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Enter Password
                </div>
              )}
              {this.state.hasLoginFailed && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Invalid Credentials
                </div>
              )}
                <div className="loginInput">
                <label>User Name </label>
                <input
                type="text"
                placeholder="Enter User Name"
                name="username"
                value={this.state.username}
                onChange={this.handleChange}
                />
                </div>
                <div className="loginInput">
                <label>Password </label>
                <input
                type="password"
                placeholder="Enter Password"
                name="password"
                value={this.state.password}
                onChange={this.handleChange}
                />
                </div>
                <div className="text-right">
                <Button
                size="sm"
                viewType="primary"
                className="w-65"
                onClickBtn={this.checkLdapAuth}
                loading={this.state.loading}
                btnTitle="Login"
                ></Button>
                </div>
                </form>
                </Ldpopup>
                )
        }else{
        return (
            <BrowserRouter>
                <Switch>
                 <Route path={contextPath+"/login"}exact component={LoginComponent} />
                 <Route path={contextPath+"/trips/:tripId/eTicket"} exact component={FPHPrintEticket} />
                 <Route path={contextPath+"/trips/:tripId/voucher"} exact component={FPHPrintVoucher} />
                
                 <Route path={contextPath+ "/trips/search"} exact component={OtherTripSearch} />
                 <Route path={contextPath+"/logout"} exact component={LogoutComponent} />
                 <Route path={contextPath+"/company/gst"}
                            render={
                            () =>
                            this.state.gstRoleEnable?
                            <GstComponent /> :
                            <AccessDenied />
                            }
                            />
                    <Route path={contextPath+"/company/config"}
                            render={
                            () =>
                            this.state.compRoleEnable ?
                            <CompanyComponent /> :
                            <AccessDenied />
                            }
                            />
                    <Route path={contextPath+"trips/:tripId"}
                            render={
                            () =>
                            this.state.tripsRoleEnable?
                            <TripDetails /> :
                            <AccessDenied />
                            }
                            />
                
                <Route path={contextPath+"/company"} exact component={CompanyComponent}/>
                <Route path={contextPath+"/accessdenied"} exact component={AccessDenied}/>
		         <Route path={contextPath+"/trips/:tripId"} exact component={TripDetails} /> 
                <Route path={contextPath+"/trips/:tripId/json"} exact component={TripJson} />
				 <Route path={contextPath+"/fph/workflows/cancellations"} exact component={FPHCancellationQueue}/>
                 <Route path={contextPath+"/trips/:tripId/cancel"} exact component={HotelCancelTrip} />
                <Route path="*" exact component={PagenotFound}/>
                </Switch>
            </BrowserRouter>
        )
    }
}
}


export default InstructorApp