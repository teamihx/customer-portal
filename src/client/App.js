/**import React, { Component } from 'react';
import './app.css';
import ReactImage from './react.png';

export default class App extends Component {
  state = { username: null };

  componentDidMount() {
    fetch('/api/getUsername')
      .then(res => res.json())
      .then(user => this.setState({ username: user.username }));


      fetch('/api/getCmpConfByDomain',{
        method: 'POST',
        body: JSON.stringify({
          domain_name:'demo.cleartripforbusiness.com'
        }),
        headers: {"Content-Type": "application/json"}
      })
      .then(function(response){
        return response.json()
      }).then(function(body){
        console.log(body);
       // alert(self.refs.task.value)
      });
  }
 render() {
    const { username } = this.state;
    return (
      <div>
        {username ? <h1>{`Hello ${username}`}</h1> : <h1>Loading.. please wait!</h1>}
        <img src={ReactImage} alt="react" />
      </div>
    );
  }
}*/



import React, { Component } from 'react';
//import "typeface-roboto";
//import './App.css';
import InstructorApp from './router/InstructorApp.jsx';
import ClientLogger from 'Services/commonUtils/ClientLogger';
import log from 'loglevel';
ClientLogger.loadLoggerinfo(log);
class App extends Component {
  render() {
    return (
      
        <InstructorApp />

    );
  }
}
export default App;