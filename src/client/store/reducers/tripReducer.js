import * as actionTypes from '../actions/tripActionType'
import { updateObject } from '../utility';
let paymntStatusMaster = require('./PaymentStatus.json');
let paymntTypeMaster = require('./PaymentTypeMasterData.json');
let cabinTypeTypeMaster = require('./CabinTypeMasterData.json');
let airLineMaster = require('./AirLineMaster.json');
let airPortMaster = require('./AirPortMaster.json');
let Txn_type_masterdata = require('./Txn_type_masterdata.json');
let txn_description = require('./Txn_description.json');
let hotel_supplier_master = require('./HotelSupplierMaster.json');
let paymentSubTypeMaster = require('./PaymentSubTypeMaster.json');
let paymentCategoryMaster = require('./PaymentCategoryMaster.json');
let BankMasterData = require('./BankMasterData.json');
let PaymentGatewaysMasterData = require('./PaymentGatewaysMaster.json');
let InsuranceMasterData = require('./InsuranceMasterData.json');
let FlightSupplierMasterData = require('./FlightSupplierMasterData.json');
let InsuranceStatusMasterData = require('./InsuranceStatusMasterData.json');
let CashCardsMasterData = require('./CashCardsMasterData.json')
let AmexGatewayResponse = require('./AmexGatewayResponse.json')


const initialState ={
    tripDetail:{},
    airLineMaster:airLineMaster,
    InsuranceMasterData:InsuranceMasterData,
    PaymentGatewaysMasterData:PaymentGatewaysMasterData,
    airPortMaster:airPortMaster,
    CashCardsMasterData:CashCardsMasterData,
    paymentSubTypeMaster:paymentSubTypeMaster,
    BankMasterData:BankMasterData,
    FlightSupplierMasterData:FlightSupplierMasterData,
    paymentCategoryMaster:paymentCategoryMaster,
    InsuranceStatusMasterData:InsuranceStatusMasterData,
    AmexGatewayResponse:AmexGatewayResponse,
    bkngSatatusMaster:{
        "W":"WAITING FOR APPROVAL",
        "B":"APPROVED" ,
        "J":"REJECTED",
        "COMPLETED":"COMPLETED",
        "UPCOMING":"UPCOMING",
        "P":"CONFIRMED",
        "Q":"CANCELLED",
        "D":"AIRLINE CANCELLED",
        "H":"FAILED" ,
        "K":"REFUNDED",
        "Z":"PAYMENT FAILED",
        "R":"SEATS RESERVED",
        "C":"AIRLINE COUPON",
        "E":"ELAPSED ",
        "X":"DISCARDED",
        "F":"SEATSELL FAILED",
        "M":"AMENDMENT",
        "":"UNKNOWN",
        "T":"PENDING" ,
        "I":"INITIATED",
        "N":"NO SHOW",
        "Y":"NOT ATTEMPTED",
        "G":"DROPPED",
        "L":"FLEXI PAY HOLD",
        "V":"ROOMER REFUNDED",
        "U":"ROOMER_CANCELLED",
        "S":"DELAYED TICKET",
        "A":"CC RECONFIRMATION PENDING",
        "O":"CC PENALTY COLLECTED",
        "AA":"CC PENALTY COLLECTION FAILED",
        "IC":"IRCTC CALLBACK PENDING",
        "IF":"IRCTC CALLBACK FAILED",
        "INPROGRESS":"INPROGRESS"
    },
    paymentStatusMaster:paymntStatusMaster,
    paymentTypeMaster:paymntTypeMaster,
    cabinTypeMaster:cabinTypeTypeMaster,
    txnTypeMasterData:Txn_type_masterdata,
    txnDescription:txn_description,
    walletPromotons:'',
    usermasterdetails:{},
    pricingDetails:{},
    hotelSupplierMaster:hotel_supplier_master,
    ctConfigData:{}
}
const updateTicketDetails = (state, action) => {
    //console.log("update ticket---"+JSON.stringify(action.tripDetail.air_bookings[0].air_booking_infos));
    
    return {
        ...state,tripDetail:{
        ...state.tripDetail,...action.tripDetail
        }
    };
};
const updatePaxDetails = (state, action) => {
    //console.log("update Pax---"+JSON.stringify(action.tripDetail.air_bookings[0].pax_infos));
    return {
        ...state,tripDetail:{
        ...state.tripDetail,...action.tripDetail
        }
    };
};
const setTripDetail = (state, action) => {
    return updateObject( state, {tripDetail:action.tripDetail} );
};
const setAirlineMaster = (state, action) => {
    return updateObject( state, {airLineMaster:action.airLineMaster} );
};
const setAirPortMaster = (state, action) => {
    return updateObject( state, {airPortMaster:action.airPortMaster} );
};
const updateWalletDetails = (state, action) => {
    return updateObject( state, {walletPromotons:action.walletDetail} );
};

const setUserDetail = (state, action) => {
   // console.log('setUserDetail----tripreducer0'+JSON.stringify(state.usermasterdetails))
    return !isEmpty(state.usermasterdetails)?updateUserDetails( state, {userDetail:action.userDetails} ):
    updateObject( state, {usermasterdetails:action.userDetails} );
};
const isEmpty=(obj) =>{
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
const updateUserDetails = (state, action) => {
    //console.log("updateUserDetails tripReducer---"+JSON.stringify(action.userDetail));
    
    return {
        ...state,usermasterdetails:{
        ...state.usermasterdetails,...action.userDetail
        }
    };
};
const updateTxnsDetails = (state, action) => {
    //console.log("update ticket---"+JSON.stringify(action.tripDetail.air_bookings[0].air_booking_infos));
    
    return {
        ...state,tripDetail:{
        ...state.tripDetail,...action.tripDetail
        }
    };
};

const updateNoteDetails = (state, action) => {
    //console.log("updateNoteDetails tripReducer---"+JSON.stringify(action.tripDetail.notes));
    
    return {
        ...state,tripDetail:{
        ...state.tripDetail,...action.tripDetail
        }
    };
};

const setPricingDetails = (state, action) => {
    return updateObject( state, {pricingDetails:action.pricingDetails} );
};

const setCTConfigData = (state, action) => {
    return updateObject( state, {ctConfigData:action.ctConfigResData} );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.STORE_TRIP_DETAIL: return setTripDetail(state, action);    
        
        case actionTypes.STORE_AIRLINE_MASTER: return setAirlineMaster(state, action);
        
        case actionTypes.STORE_AIRPORT_MASTER: return setAirPortMaster(state, action); 

        case actionTypes.STORE_UPDATED_TICKET_TRIP_DETAIL: return updateTicketDetails(state, action);

        case actionTypes.STORE_UPDATED_PAX_TRIP_DETAIL: return updatePaxDetails(state, action);

        case actionTypes.STORE_WALLET_PROMO_DETAIL: return updateWalletDetails(state, action);
        
        case actionTypes.STORE_UPDATED_USER_DETAILS: return setUserDetail(state, action);

        case actionTypes.STORE_UPDATED_TXNS_DETAILS: return updateTxnsDetails(state, action);
        
        case actionTypes.STORE_UPDATED_NOTE_DETAILS: return updateNoteDetails(state, action);

        case actionTypes.STORE_PRICING_DETAILS: return setPricingDetails(state, action);

        case actionTypes.STORE_CT_CONFIG_DATA: return setCTConfigData(state, action);

        default: return state;
    }
};
export default reducer;