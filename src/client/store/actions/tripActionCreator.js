import * as actionTypes from './tripActionType';
import fphService from '../../services/fph/FphService'
import UserFetchService from '../../services/trips/user/UserFetchService'

export const initTripDtl = (req) => {
    return dispatch => {
        fphService.getTripDetail(req)
            .then( response => {
                const tripRes = JSON.parse(response.data);
               // console.log("REDUX====response---:"+JSON.stringify(tripRes));
               dispatch(setTripDetail(tripRes));
            } )
            
    };
};
export const initTripNdCtConfig=(req)=> {
    return dispatch => Promise.all([
      dispatch(initTripDtl(req)),
      dispatch(initCTConfigData(req))
    ]);
  }
export const initUserDetails = (req) => {
    //console.log("REDUX====response tripRes---:");
    return dispatch => {
        UserFetchService.fetchUserMailid(req)
            .then( response => {
                const tripRes = JSON.parse(response.data);
              //  console.log("REDUX====response tripRes---:"+JSON.stringify(tripRes));
               dispatch(setUpdateUserdetails(tripRes));
            } )
            
    };
};

export const initAirLineMasterData = (req) => {
    return dispatch => {
        fphService.getAirMasterData(req)
            .then( response => {
                const tripRes = JSON.parse(response.data);
               // console.log("REDUX====response---:"+JSON.stringify(tripRes));
               dispatch(setAirLineMaster(tripRes));
            } )
            
    };
};
export const setAirLineMaster = ( masterData ) => {
    return {
        type: actionTypes.STORE_AIRLINE_MASTER,
        airLineMaster: masterData
    };
};
export const initAirPortMasterData = (req) => {
    return dispatch => {
        fphService.getAirPortMasterData(req)
            .then( response => {
                const tripRes = JSON.parse(response.data);
               // console.log("REDUX====response AIRPORT---:"+JSON.stringify(tripRes));
               dispatch(setAirPortMaster(tripRes));
            } )
            
    };
};
export const setAirPortMaster = ( masterData ) => {
    return {
        type: actionTypes.STORE_AIRPORT_MASTER,
        airPortMaster: masterData
    };
};
export const setTripDetail = ( tripDtl ) => {
    return {
        type: actionTypes.STORE_TRIP_DETAIL,
        tripDetail: tripDtl
    };
};
export const setUserDetail = ( tripUsrDtl ) => {
    return {
        type: actionTypes.STORE_TRIP_DETAIL,
        tripUsrDetail: tripUsrDtl
    };
};
export const updateTicketDetails = (req) => {
    //('updateTicketDetails---');
    return dispatch => {
       
    dispatch(setUpdatedTripDetail(req));
            
    };
};
export const setUpdatedTripDetail = ( tripDtl ) => {
    //console.log('setUpdatedTripDetail---');
    return {
        type: actionTypes.STORE_UPDATED_TICKET_TRIP_DETAIL,
        tripDetail: tripDtl
    };
};

export const updatePaxDetails = (req) => {
    //console.log('updatePaxDetails---');
    return dispatch => {
       
    dispatch(setUpdatedPaxDetail(req));
            
    };
};

export const setUpdatedPaxDetail = ( tripDtl ) => {
   // console.log('setUpdatedTripDetail---');
    return {
        type: actionTypes.STORE_UPDATED_PAX_TRIP_DETAIL,
        tripDetail: tripDtl
    };
};


export const updateWalletPromo = (req) => {
    //console.log('updatePaxDetails---');
    return dispatch => {
       
    dispatch(setupdateWalletPromo(req));
            
    };
};

export const setupdateWalletPromo = ( tripDtl ) => {
    //console.log('setUpdatedTripDetail---');
    return {
        type: actionTypes.STORE_WALLET_PROMO_DETAIL,
        walletDetail: tripDtl
    };
};

export const updateUserDetails = (userDtl) => {
    //console.log('updateUserDetails tripaction createor---'+JSON.stringify(userDtl));
    return dispatch => {
       
    dispatch(setUpdateUserdetails(userDtl));
            
    };
};


export const setUpdateUserdetails = ( userDtl ) => {
    //console.log('setUpdatedTripDetail---'+JSON.stringify(userDtl));
    return {
        type: actionTypes.STORE_UPDATED_USER_DETAILS,
        userDetails: userDtl
    };
};
export const updateTxnsDetails = (tripDtl) => {
    //console.log('updateUserDetails tripaction createor---'+JSON.stringify(userDtl));
    return dispatch => {
       
    dispatch(setUpdateTxnsDetails(tripDtl));
            
    };
};
export const setUpdateTxnsDetails = ( tripDtl ) => {
    //console.log('setUpdatedTripDetail---'+JSON.stringify(userDtl));
    return {
        type: actionTypes.STORE_UPDATED_TXNS_DETAILS,
        tripDetail: tripDtl
    };
};

export const updateNoteDetails = (noteData) => {
    //console.log('updateNoteDetails tripaction createor---'+JSON.stringify(noteData));
    return dispatch => {
       
    dispatch(setUpdateNotedetails(noteData));
            
    };
};


export const setUpdateNotedetails = ( noteData ) => {
    //console.log('setUpdateNotedetails---'+JSON.stringify(noteData));
    return {
        type: actionTypes.STORE_UPDATED_NOTE_DETAILS,
        tripDetail: noteData
    };
};
export const initPricingObject = (priceObj) => {
    return dispatch => {
               dispatch(setPricingDetail(priceObj));
            } 
            
    };
export const setPricingDetail = ( priceObj ) => {
        //console.log('setUpdateNotedetails---'+JSON.stringify(noteData));
        return {
            type: actionTypes.STORE_PRICING_DETAILS,
            pricingDetails: priceObj
        };
    };

    export const initCTConfigData = (req) => {
        return dispatch => {
            fphService.getFltSuppService(req)
                .then( response => {
                    const ctConfigResp = JSON.parse(response.data);
                    console.log("CT CONFIG DATA:"+JSON.stringify(ctConfigResp));
                   dispatch(setCTConfigData(ctConfigResp));
                } )
                
        };
    };
    export const setCTConfigData = ( ctData ) => {
        return {
            type: actionTypes.STORE_CT_CONFIG_DATA,
            ctConfigResData: ctData
        };
    };
//function initTripAllData(reqAirport,reqAirLine,reqTrip) {
  //  return dispatch => Promise.all([
//      dispatch(initAirPortMasterData(reqAirport)),
 //     dispatch(initAirLineMasterData(reqAirLine)),
 //     dispatch(initTripDtl(reqTrip))
  //  ]);
 // }

//export const fetchTripDetailFailed = () => {
//    return {
 //       type: actionTypes.FETCH_TRIP_DETAIL_FAILED
//    };
//};