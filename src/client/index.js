import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'Assets/styles/index.scss';
import {Provider} from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import tripReducer from '../client/store/reducers/tripReducer'
import thunk from 'redux-thunk';

const rootReducer= combineReducers({
    trpReducer:tripReducer
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

ReactDOM.render( <Provider store={store}><App /></Provider>, document.getElementById('root'));

