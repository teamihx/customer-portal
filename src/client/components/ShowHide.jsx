import React, { Component } from 'react';
import Icon from 'Components/Icon';
class ShowHide extends Component {
  
    constructor(props) {
        super(props);
        this.state = {            
          visible: this.props.visible || false,
        };
        this.handleClick = this.handleClick.bind(this)        
    }   
    handleClick(e) {
    	this.setState({ 
        visible:!this.state.visible
      });
    } 
        render() {
          const{
            className = 'penal',
          } = this.props
          const divClass = this.state.visible ? className : className +  ' hide'
            return <>
                <h5 onClick={this.handleClick} className={"show-tg-line mt-5 "+(this.state.visible ?  'up' : 'down')}>{this.props.title}<Icon className="arrow" color="#203152" size={16} icon="down-arrow" /></h5>
                {/* <ReactCSSTransitionGroup transitionName="example"> */}
                  <div className={divClass}> {this.props.children}</div>
                {/* </ReactCSSTransitionGroup> */} 
     
            </>
        }
    } 
    
 
export default ShowHide;
