import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Icon from "Components/Icon";

class CommonMsg extends React.Component {
  getContent = errorType => {
    let content;
    if (errorType === "datanotFound") {
      content = (
        <>
          <Icon color="#F4675F" size={50} icon="warning" />{" "}
          <h5 className="secondary-cr mb-10 mt-10">
            Oops, {this.props.errorMessageText}.
          </h5>
        </>
      );
    } else if (errorType === "accessDenied") {
      content = (
        <div className="denied">
          <Icon color="#F4675F" size={40} icon="accessdenied" />
          <h5 className="secondary-cr mb-10 mt-10">Access Denied..</h5>
          <div className="container">Please contact with Administrator....</div>
        </div>
      );
    } else if (errorType === "error") {
      content = (
        <p className="t-color2 mb-15">
          <Icon color="#F4675F" size={40} icon="warning" />
          <strong className="secondary-cr d-b mt-10">
            Opps, Something went wrong.
          </strong>
          {this.props.errorMessageText}
        </p>
      );
    }
    return content;
  };

  render() {
    return (
      <>
        <div className="main-container cont-Box text-center pt-50 cont-sec">
          {this.getContent(this.props.errorType)}
        </div>
      </>
    );
  }
}
CommonMsg.propTypes = {
  errorType: PropTypes.oneOf(["datanotFound", "accessDenied", "error"]),
  /**
   * Specifies classnames for the Button.
   */
  className: PropTypes.string,
  /**
   * Event handler for on click event of the Button.
   */
  onClick: PropTypes.func,
  /**
   * Specifies whether to enable the ripple effect for Button.
   */
  enableRipple: PropTypes.bool
};
CommonMsg.defaultProps = {
  errorType: "datanotFound",
  infoMessageText: "loss of information"
};

export default CommonMsg;
