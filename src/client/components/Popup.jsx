import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from "classnames";
import Icon from 'Components/Icon'; 
class Popup extends Component {

  handleCloseModal =() =>{
    this.props.togglePopup()
  }
  
  render () {
    const modalClass = classnames(
      'p-modal',
      this.props.className,
    `p-modal-${this.props.viewType}`,  
    )
    const panelClass = classnames(
      'm-panel',
      this.props.className,
      `m-panel-${this.props.size}`,  
    )
    return (
      <div>
        <a className="close" onClick={this.handleOpenModal}>Open</a>
        <div className={modalClass}>
          <div className={panelClass}>
          {/* <button className="modal-close" onClick={this.handleCloseModal}><Icon color="#F4675F" size={12} icon="close" /></button> */}
          {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}
Popup.propTypes = {
  size: PropTypes.oneOf(['lg', 'md', 'sm']),
  /**
   * Specifies type of the Popup.
   */
  viewType: PropTypes.oneOf(['layover','default']),

 /**
 * Specifies classnames for the Popup.
 */
className: PropTypes.string,
/**
 * Event handler for on click event of the Popup.
 */
onClick: PropTypes.func,
/**
 * Specifies whether to enable the ripple effect for Popup.
 */
enableRipple: PropTypes.bool
}
Popup.defaultProps = {
size:'lg',
viewType:'layover',


}
export default Popup;