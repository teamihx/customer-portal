import React from 'react';
import PropTypes from 'prop-types';
import classnames from "classnames"; 
import Icon from 'Components/Icon';  

class Notification extends React.Component {
 

alertHide=()=> {
  this.props.toggleAlert();
}
  getAppropriateIcon = (viewType)=> {
    let icon
    if(viewType ==='error') {
      icon = <Icon className="noticicon" color="#F4675F" size={16} icon="warning"/>
    } else if(viewType ==='success') {
      icon = <Icon className="noticicon" color="#02AE79" size={16} icon="success"/>
    } else if(viewType==='info'){
      icon = <Icon className="noticicon" color="#365aa1" size={16} icon="info2"/>
    }else if(viewType==='warning'){
      icon = <Icon className="noticicon" color="#cda11e" size={16} icon="warning"/>
    }
    return icon;
  }
    render() {
        const alertClass = classnames(
          'alert',
          this.props.className,
        `alert-${this.props.viewType}`,
        `alert-${this.props.viewPosition}`
      
      )
      if(window.notificationTimeout){
        clearTimeout(window.notificationTimeout)
      }
      window.notificationTimeout = setTimeout(() => {
        this.props.toggleAlert();      
      }, 5000);
      const isArrayOfMsg = (this.props.messageText instanceof Array ) ? true : false
        return (
          <>
            {this.props.showAlert && (
              <div className={alertClass}>
                <div className="alertMessage">
                {this.getAppropriateIcon(this.props.viewType)}
                {isArrayOfMsg ? (
                  <ul>
                    {this.props.messageText.map((msg, index) => {
                      return <li key={index}>{msg}</li>;
                    })}
                  </ul>
                ) : (
                  this.props.messageText
                )}
                </div>
                <a className="alrtClose" onClick={this.alertHide}>
                  <Icon color="#F4675F" size={12} icon="close" />
                </a>
              </div>
            )}
          </>
        );
    
        

  
      }

};
Notification.propTypes = {

    viewType: PropTypes.oneOf(['info', 'warning', 'success', 'error']),
   /**
   * Specifies classnames for the Notification position.
   */  
    viewPosition: PropTypes.oneOf(['normal', 'fixed']),

   /**
   * Specifies classnames for the Notification.
   */
  className: PropTypes.string,
  /**
   * Event handler for on click event of the Notification.
   */
  onClick: PropTypes.func,
  /**
   * Specifies whether to enable the ripple effect for Notification.
   */
  enableRipple: PropTypes.bool
}
Notification.defaultProps = {
  viewType:'success',
  onClickBtn:()=>{},
  messageText: 'successFull Added'

}

export default Notification;