import axios from 'axios'

const API_URL = 'http://localhost:8080'

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
export const USER_AUTH_DATA = 'userAuthData'
export const URL_COMPANY = 'company' 
export const contextPath=process.env.contextpath;       
export const PAX_DATA = 'paxData';
export const TRIP_REF = 'tripRef';
import log from 'loglevel';
export const TRIPS_ROLES = 'tripsRoles'
export const COMP_CONFIG_ROLES = 'compConfigRoles'
export const DOMAIN_HOST_NAME = 'domainHostName'
class AuthenticationService {

    executeBasicAuthenticationService(username, password) {
        console.log('executeBasicAuthenticationService'+username)
        return axios.get(`${API_URL}/basicauth`,
            { headers: { authorization: this.createBasicAuthToken(username, password) } })
    }

    executeJwtAuthenticationService(username, password) {
       // console.log(username);
        return axios.post(`${API_URL}/authenticate`, {
            username,
            password
        })
    }


    createBasicAuthToken(username, password) {
        return 'Basic ' + window.btoa(username + ":" + password)
    }

    checkRegisterAutherization() {
        //console.log('Calling checkRegisterAutherization.....')
     return axios.post(contextPath+'/api/checkUsrAuthorzation');
     
    }

    getCookie = function(name) {
        var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
        if (match) return match[2];
      }

    registerSuccessfulLogin(username, password) {
       // console.log('Login User Name :'+username)
        localStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME,username);
        const myObj = {
            email: username,
            password:password,
            redirect:"true",
            //service:"/boportal/company",
            service:contextPath+"/company",
			source:"ui",
		    caller:"hq"
          };
         return axios.post(contextPath+'/api/checkUsrLogin',myObj);
            

            
    }

    registerSuccessfulLoginForJwt(username, token) {
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }

    createJWTToken(token) {
        return 'Bearer ' + token
    }
    
    logout() {
    var domainpath = process.env.domainpath;
	var myObj ='';
	localStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    localStorage.removeItem(USER_AUTH_DATA);
    localStorage.removeItem(URL_COMPANY);
    localStorage.removeItem(PAX_DATA);
    localStorage.removeItem(TRIP_REF);
    localStorage.removeItem(TRIPS_ROLES);
    localStorage.removeItem(COMP_CONFIG_ROLES);
    localStorage.removeItem(DOMAIN_HOST_NAME);

    return axios.post(contextPath+'/api/logout',myObj).then(response => {
        console.log('Loggedout Successfully.....'+response)
    if(response.status===200){
        window.location = contextPath+'/login';
    }
    });
    }

    isUserLoggedIn() {
        let user = localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
       // console.log('isUserLoggedIn'+user)
        if (user === null) return ''
        return true
    }

    getLoggedInUserName() {
        let user = localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        //console.log('getLoggedInUserName'+user)
        if (user === null) return ''
        return user
    }

    setupAxiosInterceptors(token) {
        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = token
                }
                return config
            }
        )
    }

    getCookieValue(){
       // console.log('getCookieValue..............')
        //return axios.post('/boportal/api/getAuthToken');
        return axios.post(contextPath+'/api/getAuthToken');
    }

    checkRolesFromProperty(roleProperty){
	let checkRole=false;
        try{
            var authData=localStorage.getItem(USER_AUTH_DATA);
            var obj = JSON.parse(authData);
            if(roleProperty.includes(",")){
	 			const tripArray = roleProperty.split(",");
                for(let trip of tripArray){
                    for(let value in obj.data.rolesList){
                        let role1=obj.data.rolesList[value];
                        if(Number(trip)===Number(role1)){
                            return checkRole=true;
                       }
                }
            }
            }else{
                var trips = parseInt(roleProperty, 10);
                const tripEnable = obj.data.rolesList.find(x => x === trips);
                if(tripEnable){
                    return checkRole=true;
                }
            }
        }catch(err){
        log.error('Exception occured in createRolesAndPermissions fetchPublicIp '+err);
		return checkRole;
        }
		return checkRole;
    }

	checkPermissionFromProperty(permissionProperty){
		let checkRole=false;
        try{            
            var authData=localStorage.getItem(USER_AUTH_DATA);
            var obj = JSON.parse(authData);
            if(permissionProperty.includes(",")){
	 			const tripArray = permissionProperty.split(",");
                for(let trip of tripArray){
                    for(let value in obj.data.permissionsList){
                        let permission=obj.data.permissionsList[value];
                        if(Number(trip)===Number(permission)){
                            checkRole =  true;
							break;
                       }
                }
            }
            }else{
                var trips = parseInt(permissionProperty, 10);
                const tripEnable = obj.data.permissionsList.find(x => x === trips);
                if(tripEnable){
                    checkRole =  true;
                }else{
					checkRole = false;
				}
            }
        }catch(err){
        log.error('Exception occured in checkPermissionFromProperty fetchPublicIp '+err);
		return checkRole;
        }
		return checkRole;
    }

    createRolesAndPermissions(moduleType){
        try{
        if(moduleType.includes("trips")){
            var tripsJsonData=localStorage.getItem(TRIPS_ROLES);
            if(tripsJsonData===null || tripsJsonData===undefined){
                this.createTripsRoles();
            }
        }else{
            var compJsonData=localStorage.getItem(COMP_CONFIG_ROLES);
            if(compJsonData===null || compJsonData===undefined){
                this.createCompanyConfigRoles();
            }
        }
        }catch(err){
            log.error('Exception occured in createRolesAndPermissions fetchPublicIp '+err);
            }
        }


    createTripsRoles(){
        try{
        let offRefundRoleEnable=false;
        let lossTrackerEnable=false;
        let insuranceEnable=false;
        let offlineAmenEnable=false;
        let onlineAmendEnable=false;
        let amendTripEnable=false;
        let changeBookCloseTrxn=false;
        let htlChangeBookCloseTrxn=false;
        let offlineRefundRoleAvail=this.checkRolesFromProperty(process.env.fltofflinerefundrole);
        let offlinerefundrole1=this.checkRolesFromProperty(process.env.fltofflinerefundrole1);
        let offlinerefundrole2=this.checkRolesFromProperty(process.env.fltofflinerefundrole2);
        let losstrackerroleRole=this.checkRolesFromProperty(process.env.losstrackerrole);
        let losstrackerroleRole1=this.checkRolesFromProperty(process.env.losstrackerrole1);
        let bookinsuranceRole=this.checkRolesFromProperty(process.env.bookinsurancerole);
        let bookinsuranceRole1=this.checkRolesFromProperty(process.env.bookinsurancerole1);
        let offlineamnedRole=this.checkRolesFromProperty(process.env.offlineamnedrole);
        let offlineamnedRole1=this.checkRolesFromProperty(process.env.offlineamnedrole1);
        let onlineamendRole=this.checkRolesFromProperty(process.env.onlineamendrole);
        let onlineamendRole1=this.checkRolesFromProperty(process.env.onlineamendrole1);
        let amendthistripRole=this.checkRolesFromProperty(process.env.amendthistriprole);
        let changebookstatusclstrxnRole=this.checkRolesFromProperty(process.env.changebookstatusclstrxnrole);
        let changebookstatusclstrxnRole1=this.checkRolesFromProperty(process.env.changebookstatusclstrxnrole1);
        let htlChangeBkgStatusClsTxnRole=this.checkRolesFromProperty(process.env.htlChangeBkgStatusClsTxn)
        let htlChangeBkgStatusClsTxnRole1=this.checkRolesFromProperty(process.env.htlChangeBkgStatusClsTxn1)

        let activity_loss_track_role=false;
        let activity_change_bkng_close_txn_role=false;
        const activitylosstrackerRole=this.checkRolesFromProperty(process.env.activitylosstrackerrole);
        const activitylosstrackerRole1=this.checkRolesFromProperty(process.env.activitylosstrackerrole1);
        const activitychangebkngclosetxnRole=this.checkRolesFromProperty(process.env.activitychangebookstatusclstrxnrole);
        const activitychangebkngclosetxnRole1=this.checkRolesFromProperty(process.env.activitychangebookstatusclstrxnrole1);
        
        let train_loss_track_role=false;
        let train_change_bkng_close_txn_role=false;
        const trainlosstrackerRole=this.checkRolesFromProperty(process.env.trainlosstrackerrole);
        const trainlosstrackerRole1=this.checkRolesFromProperty(process.env.trainlosstrackerrole1);
        const trainchangebkngclosetxnRole=this.checkRolesFromProperty(process.env.trainchangebookstatusclstrxnrole);
        const trainchangebkngclosetxnRole1=this.checkRolesFromProperty(process.env.trainchangebookstatusclstrxnrole1);

        if(offlineRefundRoleAvail && offlinerefundrole1 && offlinerefundrole2){
            offRefundRoleEnable=true;
        }
        if(bookinsuranceRole && bookinsuranceRole1){
            insuranceEnable=true;
        }
        if(losstrackerroleRole && losstrackerroleRole1){
            lossTrackerEnable=true;
        }
        if(offlineamnedRole && offlineamnedRole1){
            offlineAmenEnable=true;
        }
        if(onlineamendRole && onlineamendRole1){
            onlineAmendEnable=true;
        }
        if(amendthistripRole && offlineamnedRole1){
            amendTripEnable=true;
        }
        if(changebookstatusclstrxnRole && changebookstatusclstrxnRole1){
            changeBookCloseTrxn=true;
        }
        if(htlChangeBkgStatusClsTxnRole && htlChangeBkgStatusClsTxnRole1){
            htlChangeBookCloseTrxn=true;
        }
        if(activitylosstrackerRole && activitylosstrackerRole1){
            activity_loss_track_role=true;
        }
        if(activitychangebkngclosetxnRole && activitychangebkngclosetxnRole1){
            activity_change_bkng_close_txn_role=true;
        }

        if(trainlosstrackerRole && trainlosstrackerRole1){
            train_loss_track_role=true;
        }
        if(trainchangebkngclosetxnRole && trainchangebkngclosetxnRole1){
            train_change_bkng_close_txn_role=true;
        }
        var data = {
            ftrips: {
                FLT_TRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.flttripsrole),
                PAX_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltpaxrole),
                FLT_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltnotesrole),
                FLT_PAYMENT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltpaymentrole),
                FLT_AUDIT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltauditrole),
                UPDATE_TKT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltupdatetktrole),
                UPDATE_PRICE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltupdatepricerole),
                OFFLINE_REF_ROLE_ENABLE:offRefundRoleEnable,
                TRIP_STATUS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.flttripstatusrole),
                FLT_WALLET_PRO_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltwalletpromorole),
                FLT_REFUND_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltrefunds),
                FLT_CONTACT_INFO_ROLE_ENABLE:this.checkRolesFromProperty(process.env.contactinforole),
                FLT_MISC_INFO_ROLE_ENABLE:this.checkRolesFromProperty(process.env.misctripinforole),
                FLT_MISC_VIEWLINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.miscviewtrxnlinkrole),
                FARERULE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.flightfarerulesrole)
             },
             ftripPermissions: {
                UPDATE_OFFLINE_REF_PERMSN_ENABLE:this.checkPermissionFromProperty(process.env.updateofflinerefpermission),
                PAX_EDIT_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.paxeditpermission),
                PAX_UPDATE_TKT_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.updatetktnumberpermission),
                ADD_NOTE_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.addnoterolepermission),
                ADD_TAG_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.addtagpermission),
                CANCL_TAG_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.canceltagpermission),
                UPDATE_PRICE_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.updatepricepermission)
             },
             htripPermissions: {
                RATE_RULE_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.htlraterulespermission),
                HTL_ADD_TAG_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.htladdtagpermission),
                HTL_CANCL_TAG_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.htldeletetagpermission),
                HTL_ADD_NOTE_PERMSN_ENABLE:this.checkRolesFromProperty(process.env.htladdnotepermission),
             },
             htrips: {
                HTL_TRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htltripsrole),
                HTL_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlnotesrole),
                HTL_PAYMENT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlpaymentrole),
                HTL_AUDIT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlauditrole),
                HTL_WALLET_PRO_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlwalletpromorole),
                HTL_REFUND_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlrefundrole),
                HTL_CONTACT_INFO_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlcontactinforole),
                HTL_MISC_INFO_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlmisctripinforole),

                HTL_EMAI_TRIP_DETAILS:this.checkRolesFromProperty(process.env.htlEmailDetails),
                HTL_EMAI_VOUCHER:this.checkRolesFromProperty(process.env.htlEmailVoucher),
                HTL_EMAI_TAX_INVOICE:this.checkRolesFromProperty(process.env.htlEmailTaxInvoice),
                HTL_EMAI_BOOK_STEP_SCREEN:this.checkRolesFromProperty(process.env.htlEmailBookStepScreen),
                HTL_PRINT_TAX_INVOICE:this.checkRolesFromProperty(process.env.htlPrintTaxInvoice),
                HTL_PRINT_PAY_RECEIPT:this.checkRolesFromProperty(process.env.htlPrintPayReceipt),
                HTL_PIRNT_INVOICE:this.checkRolesFromProperty(process.env.htlPirntInvice),
                HTL_PRINT_TAX_CREDIT_MEMO:this.checkRolesFromProperty(process.env.htlPrintTaxCreditMemo),
                HTL_PRINT_CANCLE_INVOICE:this.checkRolesFromProperty(process.env.htlPirntCancelInvoice),
                HTL_PRINT_VOUCHER:this.checkRolesFromProperty(process.env.htlPrintVoucher),
                HTL_LOSS_TRACKER:this.checkRolesFromProperty(process.env.htlLossTracker),
                HTL_BOOK_STEP_SCREEN:this.checkRolesFromProperty(process.env.htlBookStepScreen),
                HTL_TRIP_JSON:this.checkRolesFromProperty(process.env.htlTripJSON),
                HTL_CHANG_BKNG_STATUS_CLS_TXN:htlChangeBookCloseTrxn,
                HTL_CANCLEL_TRIP:this.checkRolesFromProperty(process.env.htlCancelTrip),
                HTL_TRIP_XML:this.checkRolesFromProperty(process.env.htltripxmlrole),
                HTL_PRICING_DEATAILS:this.checkRolesFromProperty(process.env.htlpricingdetailsrole),
                HTL_TAG_ROLE:this.checkRolesFromProperty(process.env.tagsrole)
             },
             htriplinks: {
                HTL_MISC_VIEWLINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlmiscviewtrxnlinkrole),
                HTL_SEND_PAY_LINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlsendpaylinkrole),
                HTL_COPY_PAY_LINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.htlcopypaylinkrole),		
             },
             ftriplinks: {
                EMAIL_DT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltemaildetailsrole),
                SALE_RECE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltsalereceiptrole),
                EMAIL_TKT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltemailticketrole),
                PRINT_RECE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.printreceipt),
                EMAIL_RECE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.emailreceipt),
                EMAIL_VAT_INV_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltemailvatinvoice),
                PRINT_SALE_INV_ROLE_ENABLE:this.checkRolesFromProperty(process.env.printsaleinvoice),
                EMAIL_SALE_INV_ROLE_ENABLE:this.checkRolesFromProperty(process.env.emailsaleinvoice),
                INSU_ROLE_ENABLE:insuranceEnable,
                LOSS_TRCR_ROLE_ENABLE:lossTrackerEnable,
                REWARD_PRGRM_ROLE_ENABLE:this.checkRolesFromProperty(process.env.rewardprgrm),
                XML_ROLE_ENABLE:this.checkRolesFromProperty(process.env.tripxmlrole),
                JSON_ROLE_ENABLE:this.checkRolesFromProperty(process.env.tripjsonrole),
                SYNC_GDS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.syncgdsrole),
                SMS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.smstriprole),
                EMAIL_INSU_ROLE_ENABLE:this.checkRolesFromProperty(process.env.emailinsurancerole),
                CHANGE_BK_STATUS_CLSTXN_ROLE_ENABLE:changeBookCloseTrxn,
                TAGS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.tagsrole),
                PRICE_DT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.pricingdetailsrole),
                CAN_TRIP_CALCREFU_ROLE_ENABLE:this.checkRolesFromProperty(process.env.canceltripcalrefundrole),
                EMAIL_INV_TRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.emailinvoicetriprole),
                PRINT_VAT_INV_ROLE_ENABLE:this.checkRolesFromProperty(process.env.printvatinvoicerole),
                PRINT_CAN_INV_ROLE_ENABLE:this.checkRolesFromProperty(process.env.printcanclinvoicerole),
                PRINT_REFU_CRDT_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.printrefundcreditnoterole),
                PRINT_INV_TRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.printinvoicetriprole),
                EMAIL_REFU_CRDT_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.emailrefundcreditnotesrole),
                PRINT_REFU_CAN_INV_ROLE_ENABLE:this.checkRolesFromProperty(process.env.printrefuncanclinvrole),
                PRINT_REFU_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.printrefundnoterole),
                EMAIL_BK_STRP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.emailbookstraprole),
                PRINT_ETKT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.fltprinteticket),
                OFFLINE_AMEND_ROLE_ENABLE:offlineAmenEnable,
                ONLINE_AMEND_ROLE_ENABLE:onlineAmendEnable,
                ROLLBACK_AMEND_ROLE_ENABLE:this.checkRolesFromProperty(process.env.rollbackamendrole),
                AMEND_TRIP_ROLE_ENABLE:amendTripEnable,
                UPDATE_TKT_COUPON_ROLE_ENABLE:this.checkRolesFromProperty(process.env.updatetktnumbercoupnrole),
                CAN_DOC_ROLE_ENABLE:this.checkRolesFromProperty(process.env.cancellationdocsrole),
                SEND_PAY_LINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.sendpaymentlink),
                COPY_PAY_LINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.copypaymentlink),
                BK_STRP_SCREEN_ROLE_ENABLE:this.checkRolesFromProperty(process.env.bookstrapscreenrole)
             },
             localtripsRoles: {                
                LOCAL_TRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitytripsrole),
                LOCAL_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitynotesrole),
                LOCAL_ADD_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activityaddnoterolepermission),                
                LOCAL_PAYMENT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitypaymentrole),
				LOCAL_SEND_PAYMENT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitysendpaylinkrole),
				LOCAL_COPY_PAYMENT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitycopypaylinkrole),
                LOCAL_AUDIT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activityauditrole),
				LOCAL_REFUND_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activityrefundrole),
				LOCAL_RATERULE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activityraterulespermission),				
				LOCAL_SEND_EMAIL_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activityemaildetailsrole),
				LOCAL_SEND_SMS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitysmstriprole),
				LOCAL_SEND_EMAIL_INVOICE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activityemailinvoiceRole),
				LOCAL_PRINT_INVOICE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activityprintinvoice),				
				LOCAL_LOSS_TRACKER_ROLE_ENABLE:activity_loss_track_role,				
				LOCAL_TRIP_XML_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitytripxmlrole),
				LOCAL_TRIP_JSON_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitytripjsonrole),				
				LOCAL_CHANGE_BKNG_STS_CLOSE_TXN_ROLE_ENABLE:activity_change_bkng_close_txn_role,				
				LOCAL_CANCEL_TRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitycanceltriprole),
				LOCAL_TAG_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitytagsrole),
				LOCAL_ADD_TAG_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activityaddtagpermission),
				LOCAL_DELETE_TAG_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitydeletetagpermission),
				LOCAL_PRICING_DETAILS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitypricingdetailsrole),
				LOCAL_CONTACT_DETAILS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitycontactinforole),
				LOCAL_MISC_TRIP_INFO_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitymisctripinforole),
				LOCAL_MISC_VIEW_TXN_LINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.activitymiscviewtrxnlinkrole),			
                },
                trainTripsRoles: {                
                TRAIN_TRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.traintripsrole),
                TRAIN_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainnotesrole),
                TRAIN_ADD_NOTE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainaddnoterolepermission),                
                TRAIN_PAYMENT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainpaymentrole),
                TRAIN_SEND_PAYMENT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainsendpaylinkrole),
                TRAIN_COPY_PAYMENT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.traincopypaylinkrole),
                TRAIN_AUDIT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainauditrole),
                TRAIN_REFUND_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainrefundrole),
                TRAIN_SEND_EMAIL_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainemaildetailsrole),
                TRAIN_SEND_SMS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainsmstriprole),
                TRAIN_SEND_EMAIL_INVOICE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainemailinvoice),
                TRAIN_PRINT_INVOICE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainprintinvoice),				
                TRAIN_LOSS_TRACKER_ROLE_ENABLE:train_loss_track_role,				
                TRAIN_TRIP_XML_ROLE_ENABLE:this.checkRolesFromProperty(process.env.traintripxmlrole),
                TRAIN_TRIP_JSON_ROLE_ENABLE:this.checkRolesFromProperty(process.env.traintripjsonrole),				
                TRAIN_CHANGE_BKNG_STS_CLOSE_TXN_ROLE_ENABLE:train_change_bkng_close_txn_role,				
                TRAIN_CANCEL_TRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.traincanceltriprole),
                TRAIN_TAG_ROLE_ENABLE:this.checkRolesFromProperty(process.env.traintagsrole),
                TRAIN_ADD_TAG_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainaddtagpermission),
                TRAIN_DELETE_TAG_ROLE_ENABLE:this.checkRolesFromProperty(process.env.traindeletetagpermission),
                TRAIN_PRICING_DETAILS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.trainpricingdetailsrole),
                TRAIN_CONTACT_DETAILS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.traincontactinforole),
                    },
             companyMenuRoles:{
                COMPANY_DASHBOARD_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companydashboardrole),
                COMPANY_TRIPS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companytripsrole),
                COMPANY_PEOPLE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companypeoplerole),
                COMPANY_PLACES_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyplacesrole),
                COMPANY_MYTRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companymytripsrole),
                COMPANY_MYACCOUNT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companymyaccountrole),
                COMPANY_WHATUTHINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companywhatyouthinkrole),
                COMPANY_BESTFARE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companybestfaresrole),
                COMPANY_FARES_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyairfaresrole),
                COMPANY_HOTELDIR_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyhoteldirrole),
                COMPANY_BLOG_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyblogrole),
                COMPANY_ABTCT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyabtcleartriprole),
                COMPANY_FAQ_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyfaqsrole),
                COMPANY_PRIVACY_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyprivacyrole),
                COMPANY_SECU_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companysecurtyrole),
                COMPANY_TERMS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companytermsofuserole),
                COMPANY_CONTACT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companycontactrole),
                COMPANY_PRINT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyprintemailrole)
             }
          };
          localStorage.setItem(TRIPS_ROLES,JSON.stringify(data));
        }catch(err){
            log.error('Exception occured in createTripsRoles fetchPublicIp '+err);
        }
    }
   
    createCompanyConfigRoles(){
        try{
            var data = {
                 companyConfigRoles:{
                    COMPANY_ROLE_ENABLE:this.checkRolesFromProperty(process.env.comapanyroleaccess),
                    GST_ROLE_ENABLE:this.checkRolesFromProperty(process.env.gstroleaccess)
                 },
                 companyConfigPermisions:{
                    COMPANY_EDIT_PERMSN_ENABLE:this.checkPermissionFromProperty(process.env.comapanyeditpermission),
                    COMPANY_UPDATE_PERMSN_ENABLE:this.checkPermissionFromProperty(process.env.comapanyupdatepermission),
                    GST_ADD_PERMSN_ENABLE:this.checkPermissionFromProperty(process.env.gstaddpermission),
                    GST_EDIT_PERMSN_ENABLE:this.checkPermissionFromProperty(process.env.gsteditpermission),
                    GST_DELETE_PERMSN_ENABLE:this.checkPermissionFromProperty(process.env.gstdeletpermission)
                 },
                 companyMenuRoles:{
                    COMPANY_DASHBOARD_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companydashboardrole),
                    COMPANY_TRIPS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companytripsrole),
                    COMPANY_PEOPLE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companypeoplerole),
                    COMPANY_PLACES_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyplacesrole),
                    COMPANY_MYTRIP_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companymytripsrole),
                    COMPANY_MYACCOUNT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companymyaccountrole),
                    COMPANY_WHATUTHINK_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companywhatyouthinkrole),
                    COMPANY_BESTFARE_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companybestfaresrole),
                    COMPANY_FARES_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyairfaresrole),
                    COMPANY_HOTELDIR_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyhoteldirrole),
                    COMPANY_BLOG_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyblogrole),
                    COMPANY_ABTCT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyabtcleartriprole),
                    COMPANY_FAQ_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyfaqsrole),
                    COMPANY_PRIVACY_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyprivacyrole),
                    COMPANY_SECU_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companysecurtyrole),
                    COMPANY_TERMS_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companytermsofuserole),
                    COMPANY_CONTACT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companycontactrole),
                    COMPANY_PRINT_ROLE_ENABLE:this.checkRolesFromProperty(process.env.companyprintemailrole)
                 }
              };
              localStorage.setItem(COMP_CONFIG_ROLES,JSON.stringify(data));

        }catch(err){
        log.error('Exception occured in createCompanyConfigRoles fetchPublicIp '+err);
        }
        }

    checkLdapAuthentication(username, password) {
           // console.log('checkLdapAuthentication start... :'+username)
            const req = {
                username:username,
                password:password
            }
    return axios.post(contextPath+'/api/auth/validateLDAP',req);
                
        }
        

getLdapKeyValue(){
    //console.log('getLdapKeyValue..............')
    return axios.post(contextPath+'/api/getLdapToken');
}



async saveLdapToken(username, password,data) {
   // console.log('saveLdapToken..............'+data)
    const req = {
        username:username,
        password:password,
        token_id:data
    }   
   return await axios.post(contextPath+'/api/auth/insertUserMongo',req);
}



async checkLdapAuthAvail(){
    //console.log('checkLdapAuthAvail..............')
    return await axios.post(contextPath+'/api/auth/checkIfAuthUser');
}

}


export default new AuthenticationService()