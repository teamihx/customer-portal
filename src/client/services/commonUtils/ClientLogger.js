import remote from "loglevel-plugin-remote";
export const contextPath=process.env.contextpath;
class ClientLogger {
  loadLoggerinfo(log) {
    const customJSON = log => ({
      msg: log.message,
      level: log.level.label,
      stacktrace: log.stacktrace,
      timestamp: new Date().toLocaleString(),     
      format: remote.plain(log)
      //format: remote.json(log)
    });

    remote.apply(log, { format: customJSON, url: contextPath+"/api/logger" });
    log.enableAll();
  }
}
export default new ClientLogger();
