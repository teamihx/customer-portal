
export const DOMAIN_HOST_NAME = 'domainHostName'

class Domainpath {
    getHqDomainPath(){
        console.log("Domain path :"+window.location.hostname);
        let domainPath =localStorage.getItem(DOMAIN_HOST_NAME)
        //var path="qacx.cleartripcorp.com";
        if(domainPath===null || domainPath==="" || domainPath===undefined){
        if(window.location.hostname!==undefined){
        if(window.location.hostname.includes("cx.cleartripcorp") || window.location.hostname.includes("localhost")){
            localStorage.setItem(DOMAIN_HOST_NAME,process.env.domainpath);
            return process.env.domainpath;
        }else{
            var path="https://"+window.location.hostname;
            localStorage.setItem(DOMAIN_HOST_NAME,path);
            return path;
        }
        }
       }else{
        return domainPath;
        }
    }
}
export default new Domainpath();
