import axios from 'axios';
const contextPath=process.env.contextpath;


class paymentSchedule {   
   

   insertPaymentTxn(req) {   
	   // console.log('insertPaymentTxn'+JSON.stringify(req));
		
	   return axios.post(contextPath+'/api/insertPaymentTxn',req);
	}
	getCompanyNameForCashPmnt(req) {   
	    //console.log('getPayment'+JSON.stringify(req));
		
	   return axios.post(contextPath+'/api/getCompanyNameForCashPmnt',req);
	}
	getExpressCheckoutDtl(req) {   
	   // console.log('getExpressCheckoutDtl'+JSON.stringify(req));
		
	   return axios.post(contextPath+'/api/getExpressCheckoutDtl',req);
	}
}

export default new paymentSchedule()