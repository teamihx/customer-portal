import axios from 'axios';
const contextPath=process.env.contextpath;


class FphService{

    updatePaxDetails(updatePaxDetailsReq) {
         const obj={
            "pax_infos":updatePaxDetailsReq
         };
        return axios.post(contextPath+'/api/updatePaxDetails',obj);

        /* return axios.put(
            'http://172.17.26.11:9031/trips', updatePaxDetailsReq,
            {
                headers: {
                    'Content Type': 'application/json',
                    'Accept': 'application/json',
                    'trip-version': 'V1'
                }
            }); */
    }
     getTripDetail(req) {
        //console.log('getTripDetail Req '+JSON.stringify(req))
        //return axios.post('http://192.168.44.141:9098/cmpGstDelete', deleteReq);
        //return axios.post('http://localhost:3001/deleteGstCompanyData', deleteReq);

       
        //return axios.post(delgstURL,deleteReq);
        //const getTrpDtlUrl = process.env.REACT_APP_deletegst;
		// console.log('getTripDetail contextPath  '+contextPath)
        return  axios.post(contextPath+'/api/getTripDetail',req);
    }

    getAirMasterData(req) {
        //console.log('getTripDetail Req '+JSON.stringify(req))
        //return axios.post('http://192.168.44.141:9098/cmpGstDelete', deleteReq);
        //return axios.post('http://localhost:3001/deleteGstCompanyData', deleteReq);

       
        //return axios.post(delgstURL,deleteReq);
        //const getTrpDtlUrl = process.env.REACT_APP_deletegst;
        return axios.post(contextPath+'/api/getTripMasterDetail',req);
    }
    getAirPortMasterData(req) {
       // console.log('getTripDetail Req '+JSON.stringify(req))
        //return axios.post('http://192.168.44.141:9098/cmpGstDelete', deleteReq);
        //return axios.post('http://localhost:3001/deleteGstCompanyData', deleteReq);

       
        //return axios.post(delgstURL,deleteReq);
        //const getTrpDtlUrl = process.env.REACT_APP_deletegst;
        return axios.post(contextPath+'/api/getAirportMasterDetail',req);
    }

      getWalletPromotions(tripRef){
       const req = {
            tripRefNumber: tripRef
        }
       // console.log('getWalletPromotions start...');
        return axios.post(contextPath+'/api/getPromotions',req);
       }
      //For Saving the tag we are passing status as "A" and 
      //For removeing the tag we are passing stattus as "D" in to service
       saveTags(tripRef,tagName,statusType){
        let tagList=[];
        const tagObj ={
            tag_name:tagName,
            source_type:"HQ",
            status:statusType
          };
          tagList.push(tagObj)
        const req = {
             tag_type:"Trip",
             reference_id: tripRef,
             tags:tagList
         }
         //console.log('saveTags start...');
         return axios.post(contextPath+'/api/saveTags',req);
        }


        async getWalletCurrencies(userId){
            // console.log('getWalletCurrencies start...');
             const req = {
                bookedUsrId:userId
            }
             return await axios.post(contextPath+'/api/getWalletCurrencies',req);
            }

            async fetchWallet(userId,cur){
               // console.log('fetchWallet start...');
                const req = {
                   bookedUsrId:userId,
                   currency:cur
               }
                return await axios.post(contextPath+'/api/fetchWallet',req);
               }

               async getHotelStatus(tripRef){
                //console.log('tripRef start...');
                const req = {
                   tripRefNum:tripRef
               }
                return await axios.post(contextPath+'/api/fetchHotelStatus',req);
               }

               async getFltSuppService(tripRef){
                //console.log('getSupplierService start...'+tripRef);
                return await axios.post(contextPath+'/api/getSupplierService');
               }
}



export default new FphService()