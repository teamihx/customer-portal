import axios from 'axios'
const contextPath=process.env.contextpath;
class CompanyService {
    
    fetchCompanyDetails(searchRequest) {
        //console.log("Domain name search request : "+JSON.stringify(searchRequest));
        //return axios.post('/boportal/api/getCmpConfByDomain',searchRequest);
        //return axios.post(realativePath+'getCmpConfByDomain',searchRequest);
        return axios.post(contextPath+'/api/getCmpConfByDomain',searchRequest);
    }

    addCompanyDetails(addReq) {
       return axios.post('addCompanyConfigURL',addReq);
    }

    editCompanyDetails(editReq) {
        //return axios.post('/boportal/api/updateCompConf',editReq);
        //return axios.post(realativePath+'updateCompConf',editReq);
        return axios.post(contextPath+'/api/updateCompConf',editReq);
    }

    editCompanyContactDetails(editReq) {
        //return axios.post('http://192.168.50.211:9098/updateCmpContDet', editReq);
        //return axios.post('http://localhost:3001/updateCmpContDet', editReq);
        
        const updateCompanyContactURL=process.env.REACT_APP_updatecmpcontdet;
        //console.log('updateCompanyContactURL  '+updateCompanyContactURL)
        return axios.post(updateCompanyContactURL,editReq);

    }

}

export default new CompanyService()