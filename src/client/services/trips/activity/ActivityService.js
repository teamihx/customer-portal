import axios from "axios";
const contextPath = process.env.contextpath;

class ActivityService {
  //below function service call for Emailtripdetails
  sendEmailDetails(emailDetails) {
    return axios.post(
      contextPath + "/api/trips/activity/sendEmailDetails",
      emailDetails
    );
  }

  //below function Hq redirection for Emailtripdetails
  sendEmails(emailJSON) {
    //console.log("sendEmailDetails emailJSON---" + JSON.stringify(emailJSON));
    return axios.post(contextPath + "/api/trips/activity/sendEmail", emailJSON);
  }

  //below function service call for send activity SMS
  sendActivitySMS(requestObj) {
   // console.log("sendActivitySMS start...");
    return axios.post(
      contextPath + "/api/trips/activity/sendActivitySMS",
      requestObj
    );
  }

  //below function service call for get activity SMS Link
  activitySMSLink(tripRef) {
    //console.log("getSMSLink start...");
    return axios.post(
      contextPath + "/api/trips/activity/activitySMSlink",
      tripRef
    );
  }

  activitySmsCancelLink(tripRef) {
    //console.log("getSMSLink start...");
    return axios.post(
      contextPath + "/api/trips/activity/getSMSCancelLink",
      tripRef
    );
  }

  //below function service call for send activity Email Invoice
  sendEmailInvoice(emailInvoice) {
    return axios.post(
      contextPath + "/api/trips/activity/activityEmailInvoice",
      emailInvoice
    );
  }
}

export default new ActivityService();
