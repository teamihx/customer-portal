import axios from 'axios';
const contextPath=process.env.contextpath;


class PaymentService {   
   

   getPaymentLink(req) {   
	    //console.log('getPaymentLink'+JSON.stringify(req));
		
	   return axios.post(contextPath+'/api/getPaymentLink',req);
	}
  createNewTransaction(req) {   
	    //console.log('createNewTransaction'+JSON.stringify(req));
		
	   return axios.post(contextPath+'/api/createNewTransaction',req);
	}

insertPayment(req) {   
	//console.log('insertPayment'+JSON.stringify(req));
	
   return axios.post(contextPath+'/api/insertPayment',req);
}
}
export default new PaymentService()