import axios from 'axios';
const contextPath=process.env.contextpath;
import moment from 'moment';
import DateUtils from 'Services/commonUtils/DateUtils.js';
import log from 'loglevel';
class FlightService {
    
    updateTicketDetails(updateTicketReq) {

       // console.log('Entered---updateticket api'+JSON.stringify(updateTicketReq))

        return axios.post(contextPath+'/api/upateAirTicketapi',updateTicketReq);

	}
	
	createNewTransaction(req) {   
	    //console.log('createNewTransaction'+JSON.stringify(req));
		
	   return axios.post(contextPath+'/api/createNewTransaction',req);
	}

	getpublicIp() {

     return axios.post(contextPath+'/api/getPublicIp');

	}


   getAirlineNameFromAirLineCode(airLineMaster,code){

	let airlinedata='';
	if(airLineMaster!==null && airLineMaster!==undefined && 
		code!==null && code!==undefined){
			
			if(airLineMaster[code]!==null && airLineMaster[code]!==undefined
				&& airLineMaster[code]!==''){

			airlinedata=airLineMaster[code].name;
			}

	}
       
    return airlinedata;

   }
   getAirPortNameFromAirPorteCode(AirPortMaster,code){
    //console.log("Airport===="+AirPortMaster[code].airport_name)
    return AirPortMaster[code].airport_name
   }
   getCityFromAirPorteCode(AirPortMaster,code){
    //console.log("Airport===="+AirPortMaster[code].city)
    return AirPortMaster[code].city
   }
   getFlag(flagReq){
    //console.log("flagReq===="+JSON.stringify(flagReq))
    return axios.post(contextPath+'/api/getFlag',flagReq);
   }
   updatePaxDetails(updatePaxDetailsReq) {
    if(updatePaxDetailsReq){
   for(let value of updatePaxDetailsReq){
       if(value.date_of_birth!==null && value.date_of_birth!==""){
        var newdate = value.date_of_birth.split("-").reverse().join("-");
        value.date_of_birth=newdate;
       }
       if(value.passport_detail!==null && value.passport_detail.date_of_expiry!=null && value.passport_detail.date_of_expiry!==""){
        var newdate = value.passport_detail.date_of_expiry.split("-").reverse().join("-");
        value.passport_detail.date_of_expiry=newdate;
    }
    }
    }
    const paxObj={
       "pax_infos":updatePaxDetailsReq
    };
   // console.log('update pax start...'+JSON.stringify(paxObj))
   return axios.post(contextPath+'/api/updatePaxDetails',paxObj);
}

sendFlightSMS(requestObj) {
   // console.log('sendFlightSMS start...');
   return axios.post(contextPath+'/api/sendFlightSMS',requestObj);
}

constructEmailJson(responseData1,airPortMaster,airLineMaster) {
		
	let tripList =new Object();	
	let trip = new Object();
	trip.tripRef =  responseData1.trip_ref;
	trip.contact_detail = responseData1.contact_detail;
	trip.domain =responseData1.domain;
	trip.currency =responseData1.currency;
	
	let air_bookingsList =[];
	let flightsList = [];
	let pax_infosList = [];
	let air_bookings = new Object();
	let price_summary = new Object();
	
	
	price_summary.tot_disc = 0; 
	price_summary.tot_cachBack = 0; 
	price_summary.tot_oth_charges = 0; 
	price_summary.tot_insur = 0; 
	price_summary.tot_pro_fee = 0; 
	price_summary.tot_fare = responseData1.air_bookings[0].total_fare; 
	  
	air_bookings.air_booking_type = responseData1.air_bookings[0].air_booking_type;
	let totAdultPrice =0;
	let totChdPrice =0;
	let totInfPrice =0;
	for(let pax_info of responseData1.air_bookings[0].pax_infos){
		let pax_infos = new Object(); 
		pax_infos = pax_info;
		let paxType = '';
		if(pax_infos.pax_type_code=='ADT'){
			paxType = 'Adult';
			totAdultPrice = totAdultPrice + this.getPaxAmount(pax_info,responseData1);	
		}else if(pax_infos.pax_type_code=='CHD'){
			paxType = 'Child';
			totChdPrice = totChdPrice + this.getPaxAmount(pax_info,responseData1);
		}else if(pax_infos.pax_type_code=='INF'){
			paxType = 'Infant';
			totInfPrice = totInfPrice + this.getPaxAmount(pax_info,responseData1);
		}
		pax_infos.pax_type_code_str = paxType;
		pax_infos.paxAmount =pax_info.total_fare;
		pax_infosList.push(pax_infos);
	}
	
	
	
	price_summary.tot_adt_price = totAdultPrice;
	price_summary.tot_chd_price = totChdPrice; 
	price_summary.tot_inf_price = totInfPrice; 
	let seq =1;
	for(let flts of responseData1.air_bookings[0].flights){
		let flights = new Object(); 
		flights  = flts;
		flights.flight_index = seq;
		seq = seq +1;
		flights.arrival_airportName=this.getAirPortNameFromAirPorteCode(airPortMaster,flights.arrival_airport);
		flights.arrival_city=this.getCityFromAirPorteCode(airPortMaster,flights.arrival_airport);
		flights.departure_airportName= this.getAirPortNameFromAirPorteCode(airPortMaster,flights.departure_airport);
		flights.departure_city=this.getCityFromAirPorteCode(airPortMaster,flights.departure_airport);
		flights.departure_date_time_str=DateUtils.ConvertIsoToSimpleDateFormat(flights.departure_date_time);
		flights.arrival_date_time_str=DateUtils.ConvertIsoToSimpleDateFormat(flights.arrival_date_time);
		flights.fareRulesUrl = contextPath+"/hq/trips/flight/"+flights.id+"/fare_rule";
		
		
		flights.marketing_airline_name=this.getAirlineNameFromAirLineCode(airLineMaster,flights.segments[0].marketing_airline);
		flights.operating_airline_name=this.getAirlineNameFromAirLineCode(airLineMaster,flights.segments[0].operating_airline);
		let bookInfos = responseData1.air_bookings[0].air_booking_infos.filter( function (airbookInfo) {
					      return airbookInfo.segment_id === flights.segments[0].id
					    });
		let pnr = 'NA';
		if(bookInfos[0].airline_pnr !==null){
			pnr = bookInfos[0].airline_pnr;
		}else if(bookInfos[0].gds_pnr !==null){
			pnr = bookInfos[0].gds_pnr;
		}
		flights.pnr= pnr;
		for(let segment of flights.segments){
			segment.arrival_airportName=this.getAirPortNameFromAirPorteCode(airPortMaster,segment.arrival_airport);
			segment.arrival_city=this.getCityFromAirPorteCode(airPortMaster,segment.arrival_airport);
			segment.departure_airportName= this.getAirPortNameFromAirPorteCode(airPortMaster,segment.departure_airport);
			segment.departure_city=this.getCityFromAirPorteCode(airPortMaster,segment.departure_airport);
			segment.departure_date_time_str=DateUtils.ConvertIsoToSimpleDateFormat(segment.departure_date_time);
			segment.arrival_date_time_str=DateUtils.ConvertIsoToSimpleDateFormat(segment.arrival_date_time);
			segment.marketing_airline_name=this.getAirlineNameFromAirLineCode(airLineMaster,segment.marketing_airline);
		segment.operating_airline_name=this.getAirlineNameFromAirLineCode(airLineMaster,segment.operating_airline);
		}		
		flightsList.push(flights);		
	}
	air_bookings.flights = flightsList;	
	air_bookings.pax_infos = pax_infosList;
	air_bookingsList.push(air_bookings);
	
	trip.domainCurr = "RS";
	trip.price_summary = price_summary;
	trip.air_bookings = air_bookingsList;
	trip.booking_status = responseData1.booking_status;
	tripList.trip=trip;
	
	return tripList;
	}
	
	getPaxAmount(pax_info,responseData1){
		let totAdultPrice =0;
			let bookInfos = responseData1.air_bookings[0].air_booking_infos.filter( function (airbookInfo) {
					      return airbookInfo.pax_info_id === pax_info.id
					    });
			if(bookInfos.length >0){
				for(let bookInfo of bookInfos){
				let priceObjects = responseData1.air_bookings[0].pricing_objects.filter( function (pricing_objects) {
					      return pricing_objects.id === bookInfo.pricing_object_id
					    });
				if(bookInfos.length >0){
					for(let priceObject of priceObjects){
					totAdultPrice =  totAdultPrice + priceObject.total_base_fare;
				}
				}				
			
			}
			}
			return totAdultPrice;
			
    }
    
       getSMSLink(tripRef){
       // console.log('getSMSLink start...');
        return axios.post(contextPath+'/api/getSMSLink',tripRef);
        }
        getFltSmsCancelLink(tripRef){
        //console.log('getSMSLink start...');
        return axios.post(contextPath+'/api/getSMSCancelLink',tripRef);
        }
        getshortSMSLink(tripRef){
        // console.log('getSMSLink start...');
         return axios.post(contextPath+'/api/getSMSLink',tripRef);
        }

	constructReceiptJson(responseData1,tripList,paymentTypeMaster){
	let invoice = new Object();
	let paydetails = new Object();
	
	for(let invoices of responseData1.invoices){
		if(invoices.category=='PaymentReceipt'){
			invoice = invoices;
			break;
		}
	}
	tripList.trip.invoice = invoice;
	tripList.trip.booked_user_title="Mr";
	tripList.trip.booked_user_name="Test Test";
	tripList.trip.booked_user_email="test@email.com";
	
	let paymode = responseData1.payments_service_data[0].payment_type;
	let tnxNumb = '';
	let paymentmode = paymentTypeMaster[responseData1.payments_service_data[0].payment_type];
	if(paymode ==='CC'){
		 tnxNumb = responseData1.payments_service_data[0].payment_card_details[0].card_number;
	}
	paydetails.mode = paymentmode;
	paydetails.txn_num = tnxNumb;
	paydetails.amount = responseData1.payments_service_data[0].amount;
	
	tripList.trip.paydetails = paydetails;
	
	
	
}

offlineRefund(reqJSON) {   
	    //console.log('changeBkgStatsClsTxn'+JSON.stringify(reqJSON));
		
	return axios.post(contextPath+'/api/offlineRefund',reqJSON).
			then(function(response) {	
	   if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200) {
	 	const responseSuccess = {
            status: "200",
            description: response.data,
            updateStatus: true,
          };  
		
          return responseSuccess;
        } else {
          const responseOther = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };         
          return responseOther;
        }
      }
		});
	
	}

	async getSyncGDS(tripRef){
		//console.log('getSyncGDS start...');
		const req = {
		   tripRefNum:tripRef
	   }
		return await axios.post(contextPath+'api/processSyncGDS',req);
	   }

	   async getFltSuppService(){
		//console.log('getSupplierService start...');
		return await axios.post(contextPath+'api/getSupplierService');
	   }

	   async processRollbackAmend(tripRef){
		//console.log('processRollbackAmend start...');
		const req = {
		   tripRefNum:tripRef
	   }
		return await axios.post(contextPath+'api/processRollbackAmend',req);
	   }

}

export default new FlightService()