import axios from "axios";
const contextPath = process.env.contextpath;

class TrainService{

      sendTrainSMS(phoneObj) {
        return axios.post(contextPath + "/api/trips/train/sendTrainSMS",phoneObj);
      }
      getStaionName(stationCode) {
        const req={
        code:stationCode
        }
        return axios.post(contextPath + "/api/trips/train/getStationName",req);
        }

}

export default new TrainService();