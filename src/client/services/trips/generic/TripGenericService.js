import axios from 'axios';
import log from 'loglevel';
const contextPath=process.env.contextpath;


class TripGenericService {   
   

   changeBkgStatsClsTxn(reqJSON) {   
	    //console.log('changeBkgStatsClsTxn'+JSON.stringify(reqJSON));
		
	return axios.post(contextPath+'/api/closeTxn',reqJSON).
			then(function(response) {	
	   if (response !== undefined && response !== "") {
      const delGstResponse = JSON.parse(response.data);
      if (response.data !== "" &&
        response.data !== undefined &&  delGstResponse.status!==undefined
       && delGstResponse.status ==='SUCCESS' &&
        response.status === 200) {
	 	const responseSuccess = {
            status: "200",
            description: "Success",
            updateStatus: true,
          };  
		
          return responseSuccess;
        } else {
          const responseOther = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };         
          return responseOther;
        }
      }
		});
	
  }
  
  checkForOpenTxnAvailable(txns){
    let isTxnOpen=false;
    try{
    for(let txn in txns){
	  let txnData=txns[txn];
      //checking is any txn is open or not
      if(txnData.status!==null && txnData.status!==undefined && txnData.status!=='' 
         && txnData.status==='O'){
        isTxnOpen=true;
        break;

      }
    }
  
	}catch(err){
	
	  log.error('exception occured in TripGenericService js checkForOpenTxnAvailable function '+err);
	
	}
	return isTxnOpen;
	}
	
	getDomain=(domain)=>{
      let country ='';
            if(typeof domain!=='undefined'){
                let frm=domain.lastIndexOf('.')+1;
            country=domain.substring(frm,domain.length);
            if(country==='com'){
                country='in'
            }
           
            }
 		return country

    }
	
	 isEmpty = (obj) => {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }

async getBookingPolicies(tripId){
             //console.log('getBookingPolicies start...');
             const req = {
                tripId:tripId
            }
             return await axios.post(contextPath+'/api/bookPolicy',req);
            }
	
	
	
}

export default new TripGenericService()