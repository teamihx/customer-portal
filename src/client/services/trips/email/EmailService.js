import axios from 'axios';
import { json } from 'body-parser';
const contextPath=process.env.contextpath;


class EmailService {   
   

   sendFightDetailEmail(flightJSON) {   
	    //console.log('sendFightDetailEmail'+JSON.stringify(flightJSON));

	return axios.post(contextPath+'/api/sendFlightDetailEmailApi',flightJSON).
			then(function(response) {	

       // console.log("response data mail===="+JSON.stringify(response.data))
        let data =JSON.parse(response.data)
	   if (typeof data !== 'undefined') {
        if (typeof data !== "undefined" &&
             data.status === "ACCEPTED") {

       
	 	const responseSuccess = {
            status: "200",
            description: response.data,
            updateStatus: true,
          };  
		
          return responseSuccess;
        } else {
          const responseOther = {
            status: "404",
            description: "Unable to send Email",
            updateStatus: false,
          };         
          return responseOther;
        }
      }
		});
	}
	
	sendEmails(emailJSON) {   
	    console.log('sendEmails'+JSON.stringify(emailJSON));
	   

	return axios.post(contextPath+'/api/sendEmail',emailJSON).
			then(function(response) {	
	   if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200) {
	 	const responseSuccess = {
            status: "200",
            description: response.data,
            updateStatus: true,
          };  
		
          return responseSuccess;
        } else {
          const responseOther = {
            status: "404",
            description: "Unable to send Email",
            updateStatus: false,
          };         
          return responseOther;
        }
      }
		});
	}

}

export default new EmailService()