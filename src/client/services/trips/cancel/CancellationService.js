import axios from 'axios'
export const contextPath=process.env.contextpath;  

class CancellationService {

cancelRefundInfo(req){
    return axios.post(contextPath+'/api/trips/cancellation/refundInfo',req);
}

autoCancellation(req){
    return axios.post(contextPath+'/api/trips/cancellation/autoCancel',req);
}

async expressWayCardNumber(req){
    return await axios.post(contextPath+'/api/trips/cancellation/expressCardDetails',req);
    }
    
async getUserApiKey(req){
        return await axios.post(contextPath+'/api/trips/cancellation/userApiKey',req);
 }

}
export default new CancellationService()