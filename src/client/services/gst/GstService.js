import axios from 'axios';
//import {} from 'dotenv/config';
const contextPath=process.env.contextpath;

class GstService {
    
    fetchGstCompanyDetails(gstSearchReq) {
        //console.log('fetchGstCompanyDetails Req '+JSON.stringify(gstSearchReq))
        //console.log('contextPath is---'+contextPath)

        //return axios.post('http://192.168.44.141:9098/getCompanyGstDtls', gstSearchReq);
        //return axios.post('http://localhost:3001/getGstCompanyData',gstSearchReq);
        
        const environMent=process.env.environment;
        //console.log('React environMent is---  '+environMent)
        //return axios.post(gstSearchURL,gstSearchReq);
        //return axios.post('/boportal/api/getGstCompanyData',gstSearchReq);
        //return axios.post(realativePath+'getGstCompanyData',gstSearchReq);
        return axios.post(contextPath+'/api/getGstCompanyData',gstSearchReq);

    }

     addGstCompanyDetails(addGstReq) {
        //console.log('addGstCompanyDetails Req '+JSON.stringify(addGstReq))
       // return axios.post('http://192.168.44.141:9098/updateCmpGstDetails', addGstReq);
        //return axios.post('http://localhost:3001/addGstCompanyData', addGstReq);

        const addgstURL=process.env.REACT_APP_adddgst;
        //return axios.post(addgstURL,addGstReq);
        //return axios.post('/boportal/api/addGstCompanyData',addGstReq);
        //return axios.post(realativePath+'addGstCompanyData',addGstReq);
        return axios.post(contextPath+'/api/addGstCompanyData',addGstReq);
    }

    editGstCompanyDetails(editReq) {
        //console.log('editGstCompanyDetails Req '+JSON.stringify(editReq))
        //return axios.post('http://192.168.44.141:9098/updateCmpGstDetails', editReq);
        //return axios.post('http://localhost:3001/editGstCompanyData', editReq);

        const editgstURL=process.env.REACT_APP_editgst;
        //return axios.post(editgstURL,editReq);
        //return axios.post('/boportal/api/editGstCompanyData',editReq);
       // return axios.post(realativePath+'editGstCompanyData',editReq);
        return axios.post(contextPath+'/api/editGstCompanyData',editReq);
    }

    deleteGstCompanyDetails(deleteReq) {
        //console.log('deleteGstCompanyDetails Req '+JSON.stringify(deleteReq))
        //return axios.post('http://192.168.44.141:9098/cmpGstDelete', deleteReq);
        //return axios.post('http://localhost:3001/deleteGstCompanyData', deleteReq);

        const delgstURL=process.env.REACT_APP_deletegst;
        //return axios.post(delgstURL,deleteReq);
        //return axios.post('/boportal/api/deleteGstCompanyData',deleteReq);
        //return axios.post(realativePath+'deleteGstCompanyData',deleteReq);
        return axios.post(contextPath+'/api/deleteGstCompanyData',deleteReq);
    }

}

export default new GstService()