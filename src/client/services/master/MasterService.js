import axios from 'axios'


class MasterService {
    
    loadAirlinesMasterData() {
        return axios.get('http://staging.flyinstatic.com/hq/json/AirlinesMasterData.json');
    }

}
export default new MasterService()