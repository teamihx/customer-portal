import React, { Component } from 'react';
import Header from 'Pages/common/Header.js';
import Footer from 'Pages/common/Footer.js';
import Icon from 'Components/Icon';
class AddCompanyComponent extends React.Component {
    constructor(props) {
        super(props)
        var dmn = this.props.dname
        this.state = {
            companyName: '',
            domainName: dmn,
            emailId: '',
            emailTypes: '',
            contactNum: '',
            contactTypes: '',
            address: '',
            addressTypes: '',
            city: '',
            newConfigName: false,
            newConfigValue: false,
            flyerDtls: [{ newConfigName: "" ,newConfigValue: ""}]
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    newConfigDrop1 = idx => evt => {
        let newFlyerDtls = this.state.flyerDtls.map((flyerDtl, sidx) => {
          if (idx !== sidx) return flyerDtl;
          return { ...flyerDtl,newConfigName:evt.target.value};
        });
        this.setState({ flyerDtls: newFlyerDtls });
    }
    newConfigDrop2 = idx => evt => {
            let newFlyerDtls = this.state.flyerDtls.map((flyerDtl, sidx) => {
              if (idx !== sidx) return flyerDtl;
              return { ...flyerDtl,newConfigValue:evt.target.value};
            });
    
        this.setState({ flyerDtls: newFlyerDtls });
      };
      handleAddFlyerDtl = () => {
        this.setState({
          flyerDtls: this.state.flyerDtls.concat([{ newConfigName: "" ,newConfigValue: ""}])
        });
      };
      handleRemoveFlyerDtl = idx => () => {
        this.setState({
          flyerDtls: this.state.flyerDtls.filter((s, sidx) => idx !== sidx)
        });
      };

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit(e) {
        const { flyerDtls } = this.state;
        alert(JSON.stringify(flyerDtls));

        const infoReq={
            "company":{
                name: this.state.companyName,
                domain_name: this.state.domainName,
                "companyConfig" :[{
                    "configName":'conf1',
                    "configValue":'conf2'
                }]
            }
        }
        this.createReq = {
            name: this.state.companyName,
            domain_name: this.state.domainName,
            emailId: this.state.emailId,
            emailTypes: this.state.emailTypes,
            contactNum: this.state.contactNum,
            contactTypes: this.state.contactTypes,
            address: this.state.address,
            addressTypes: this.state.addressTypes,
            city: this.state.city,
            newConfigName: this.state.selConfig,
            newConfigValue: this.state.selConfigName
        }

        alert(JSON.stringify(infoReq));
        //console.log(infoReq)
    }

    gotoAddCheckPage = () => {
        this.props.clickAddHandler('checkAddPage');
    }

    render() {
        const { channelName } = this.state
        return (
            <>
                <Header />
                <section>
                <div className="main-container pageTtl">
                <h3>Add Company Details</h3>
                </div>
                </section>
                <section className="cont-Box">
                <div className="main-container">
                   
                    <p className="t-color2 mb-15">Enter all the necessary details about your domain '{this.state.domainName}'</p>
                    <div className="form-group">
                        <div className="row">
                            <div className="col-5">
                                <div className="row">
                                    <div className="col-4"><label>Domain Name:</label></div>
                                    <div className="col-6">{this.state.domainName}</div>
                                </div>
                                <div className="row">
                                    <div className="col-4"><label> Company Nmae:</label></div>
                                    <div className="col-6">
                                        <input type="text" name="companyName" value={this.state.companyName} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-4"><label> City:</label></div>
                                    <div className="col-6">
                                        <input type="text" name="city" value={this.state.city} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="col-5">
                                <div className="row">
                                    <div className="col-4"><label>Email :</label></div>
                                    <div className="col-5">
                                        <input type="text" name="emailId" value={this.state.emailId} onChange={this.handleChange} />
                                    </div>
                                    <div className="col-3"><div className="custom-select-v3">
                                        <select name="emailTypes" name="emailTypes" value={this.state.emailTypes} onChange={this.handleChange}>
                                            <option value="">-Select-</option>
                                            <option value="PERSONAL">PERSONAL</option>
                                            <option value="OTHER">OTHER</option>
                                            <option value="WORK">WORK</option>
                                            <option value="HOME">HOME</option>
                                        </select>
                                        <Icon className="select-me" color="#333" size={20} icon="down-arrow" />
                                    </div> </div>
                                </div>
                                <div className="row">
                                    <div className="col-4"><label> Contact Number:</label></div>
                                    <div className="col-5">
                                        <input type="text" name="contactNum" value={this.state.contactNum} onChange={this.handleChange} />
                                    </div>
                                    <div className="col-3">
                                        <div className="custom-select-v3">
                                            <select name="contactTypes" name="contactTypes" value={this.state.contactTypes} onChange={this.handleChange}>
                                                <option value="">-Select-</option>
                                                <option value="OTHER">OTHER</option>
                                                <option value="LANDLINE">LANDLINE</option>
                                                <option value="PROPERTY">PROPERTY</option>
                                                <option value="WORK">WORK</option>
                                                <option value="MOBILE">MOBILE</option>
                                            </select>
                                            <Icon className="select-me" color="#333" size={20} icon="down-arrow" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-4"><label>Address:</label></div>
                                    <div className="col-5">
                                        <input type="text" name="address" value={this.state.address} onChange={this.handleChange} />
                                    </div>
                                    <div className="col-3">
                                        <div className="custom-select-v3">
                                            <select name="addressTypes" name="addressTypes" value={this.state.addressTypes} onChange={this.handleChange}>
                                                <option value="">-Select-</option>
                                                <option value="OTHER">OTHER</option>
                                                <option value="BILLING">BILLING</option>
                                                <option value="PROPERTY">PROPERTY</option>
                                                <option value="WORK">WORK</option>
                                                <option value="HOME">HOME</option>
                                                <option value="SHIPPING">SHIPPING</option>
                                            </select>
                                            <Icon className="select-me" color="#333" size={20} icon="down-arrow" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
          <div className="addComp">
          <div className="row">      
          <div className="col-6">
          
          
          <label>Add new configs</label>
                {this.state.flyerDtls.map((flyerDtl, idx) => (
         
         <div className="flyerDtl">    
         <div className="row">      
          <div className="col-6">
            <input
              type="text"
              placeholder={`New Config #${idx + 1} name`}
              value={flyerDtl.newConfigName}
             
              onChange={this.newConfigDrop1(idx)}
            />
            </div>
            <div className="col-5">
            <input
              type="text"
              placeholder={`New config value`}
              value={flyerDtl.newConfigValue}
              
              onChange={this.newConfigDrop2(idx)}
            />
            </div>
            <div className="col-1">
            <button
              type="button"
              onClick={this.handleRemoveFlyerDtl(idx)}
              className="btnEvent closeme"
            >
              <Icon color="#c45757" size={10} icon="close" />
            </button>
            </div>
            </div>
          </div>
        ))}
        </div>
        <div className="col-3 addmePlus">
        <button
          type="button"
          onClick={this.handleAddFlyerDtl}
          className="btnEvent addme"
        >
           <Icon color="#316131" size={10} icon="add" />
        </button>
          </div>
        </div>
        </div>
        

                        <div className="btnSec">
                            <button type="button" onClick={this.gotoAddCheckPage} className="btn btn-xs btn-primary"><b>Back</b></button>
                            <button type="button" onClick={this.handleSubmit} className="btn btn-xs btn-primary"><b>submit</b></button>

                        </div>
                    </div>
                </div>
                </section>
                <Footer />
            </>
        )
    }
}
export default AddCompanyComponent