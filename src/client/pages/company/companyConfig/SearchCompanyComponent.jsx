import React, { Component } from 'react';
import Icon from "Components/Icon";
import Header from 'Pages/common/Header.js';
import Footer from 'Pages/common/Footer.js';
import CompanyService from 'Services/company/CompanyService.js';
import Button from 'Components/buttons/button.jsx'
import log from 'loglevel';
const contextPath=process.env.contextpath;

class SearchCompanyComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            domainName: '',
            errorMsg: '',
            resData:{},
            loading:false,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        //console.log('contextPath is---'+contextPath);

    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value, errorMsg :"" });
    }

    handleSubmit(e) {
        try{
        e.preventDefault();
        this.setState({loading:true});
        this.showloader=false;
        this.setState({ errorMsg: '' })
        this.props.getDomainName(this.state.domainName.trim());
        if (this.state.domainName.trim() === '') {
            this.setState({loading:false});
            this.forceUpdate();
            this.setState({ errorMsg: 'Domain name must be provided' })
        } else {
            this.searchRequest = {
                domain_name: this.state.domainName.trim(),
            }
            this.fetchCompanyDetails(this.searchRequest);
        }
    }catch(err){
        this.setState({loading:false});
        log.error('Exception occured in SearchCompanyComponent handleSubmit function---'+err);
        
    }
    }

    fetchCompanyDetails(searchRequest) {
        CompanyService.fetchCompanyDetails(searchRequest)
            .then(response => {
                try{
                var res = JSON.stringify(response);
                const fetchCmpData = JSON.parse(res);
                this.setState({ fetchCmpData });
                if(fetchCmpData !== undefined && fetchCmpData.status !== undefined){
                    if(fetchCmpData.data.status !== 200){
                        this.showloader=true;
                        this.setState({loading:false});
                        this.state.errorMsg = 'Searched Domain name does not exist';
                        this.forceUpdate();
                    }else{
                        this.setState({loading:false});
                        this.props.clickSearchHandler(fetchCmpData.data);
                        this.showloader=true;
                        
                    }
                }else{
                    this.showloader=true;
                    this.setState({loading:false});
                    this.state.errorMsg = 'Searched Domain name does not exist';
                    this.forceUpdate();
                }
                } catch(err){
                    this.setState({loading:false});
                    log.error('exception Occured in SearchCompanyComponent fetchCompanyDetails function---'+err);
                }
            })        
    }

    render() {
        const { channelName } = this.state
        return (
            <>
                <Header />
                <form onSubmit={(e)=>this.handleSubmit(e)}>
                    <section>
                <div className="main-container pageTtl">
                <h3>Company Config Details</h3>
                </div>
                </section>
                <section className="cont-Box">
                <div className="main-container ">
                {this.state.errorMsg !== '' && <div className="alert alert-danger"><Icon className="noticicon" color="#F4675F" size={16} icon="warning"/> {this.state.errorMsg}</div>}                                                          
                    <div className="form-group">
                    <div className="row">
                            <div className="col-"><label className="required">Domain name </label></div>
                            <div className="col-2">
                                <input type="text" name="domainName" value={this.state.domainName} 
                                 onChange={this.handleChange} placeholder="Enter domain name"/>
                            </div>
                            <div className="col-2">
                                
                    <Button
                size="xs"
                viewType="primary"
                loading = {this.state.loading}
                btnTitle='Search'
                onClickBtn={this.handleSubmit} 
              >
                
              </Button>
                            </div>
                            </div>
                    </div>

                </div>
                </section>
                </form>
                <Footer />
            </>
        )
    }
}
export default SearchCompanyComponent