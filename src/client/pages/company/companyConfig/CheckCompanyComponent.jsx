import React, { Component } from 'react';
import Header from 'Pages/common/Header.js';
import Footer from 'Pages/common/Footer.js';
import log from 'loglevel';
//import moment from 'moment';
class CheckCompanyComponent extends React.Component {
    constructor(props) {
        super(props)
        var dmn = this.props.dname
        this.state = {
            domainName: dmn,
            errorMsg: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit(e) {
        try{
        this.setState({ errorMsg: '' })
        if (this.state.domainName === undefined || this.state.domainName.trim() === '') {
            this.setState({ errorMsg: 'Domain name must be provided' })
        } else {
            this.createReq = {
                domainName: this.state.domainName.trim(),
            }
            this.props.clickCheckAddHandler('addPage');
        }
    }catch(err){
        log.error('Exception occured in CheckCompanyComponent handleSubmit function---'+err);
    }
    }

    render() {
        const { channelName } = this.state
        return (
            <>
                <Header />
                <section>
                <div className="main-container pageTtl">
                <h3>Company Config Details</h3>
                </div>
                </section>
                <section className="cont-Box">
                <div className="main-container">
                    {this.state.errorMsg !== '' && <div className="alert alert-warning">{this.state.errorMsg}</div>}
                    
                    <p className="t-color2 mb-15">This subdomain is not on the list. Do you want to add it?</p>
                    <div className="form-group">
                        <div className="row">
                            <div className="col-5">
                                <div className="row">
                                    <div className="col-4"><label>Domain Name:</label></div>
                                    <div className="col-6"><input type="text" name="domainName" value={this.state.domainName} onChange={this.handleChange} /></div>
                                    <div className="col-1"><button type="button" onClick={this.handleSubmit} className="btn btn-xs btn-primary"><b>Add</b></button></div>
                                </div>
                            </div>
                        </div>
                    </div>

                   
                </div>
                </section>
                <Footer />
            </>
        )
    }
}
export default CheckCompanyComponent