import React, { Component } from "react";
import Header from "Pages/common/Header.js";
import Footer from "Pages/common/Footer.js";
import { Link } from "react-router-dom";
import Button from "Components/buttons/button.jsx";
import log from "loglevel";
export const COMP_CONFIG_ROLES = 'compConfigRoles'
class CompanyDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrayData: this.props.detailsPagedata,
      loading: false,
      enableEditComp: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);

    let tripsObj=JSON.parse(localStorage.getItem(COMP_CONFIG_ROLES));
    if(tripsObj!==null && tripsObj.companyConfigPermisions!==null && tripsObj.companyConfigPermisions!==undefined){
      this.state.enableEditComp = tripsObj.companyConfigPermisions.COMPANY_EDIT_PERMSN_ENABLE;
    }
   
  }

  handleSubmit(e) {
    try {
      this.setState({ loading: true });
      this.props.clickCompanyDetailsHandler(this.state.arrayData);
      this.setState({ loading: false });
    } catch (err) {
      this.setState({ loading: false });
      log.error(
        "Exception occured in CompanyDetailsComponent handleSubmit function---" +
          err
      );
    }
  }
  gotoSearchPage = () => {
    this.props.clickEditBackAddHandler("searchPage");
  };

  render() {
    return (
      <>
        <Header />
        <section>
        <div className="main-container pageTtl">
          <h3> Company Config Details</h3>
          <span className="titleBtn">
            <Button
              size="xs"
              viewType="default"
              onClickBtn={this.gotoSearchPage}
              btnTitle="Back"
            ></Button>

            {this.state.enableEditComp && (
              <Button
                size="xs"
                viewType="primary"
                className="w-160"
                onClickBtn={this.handleSubmit}
                loading={this.state.loading}
                btnTitle="edit company config"
              ></Button>
            )}
          </span>
        </div>
        </section>
        <section className="cont-Box">
        <div className="main-container">
          <div className="resTable">
            <table
              className="config-table  dataTbl"
              id="contentDataId"
              key="dataItm"
            >
              <tbody>
                <tr>
                  <th>Config Name</th>
                  <th>Config Value</th>
                </tr>
                {this.state.arrayData.company_configs.length > 0 &&
                  this.state.arrayData.company_configs.map((person, index) => (
                    <tr key={index}>
                      <td width="35%">{person.config_name}</td>
                      <td width="65%">
                        <span title={person.config_value}>
                          {person.config_value}
                        </span>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
          <div className="btnSec">
            <Button
              size="xs"
              viewType="default"
              onClickBtn={this.gotoSearchPage}
              btnTitle="Back"
            ></Button>

            {this.state.enableEditComp && (
              <Button
                size="xs"
                viewType="primary"
                className="w-160"
                onClickBtn={this.handleSubmit}
                loading={this.state.loading}
                btnTitle="edit company config"
              ></Button>
            )}
          </div>
        </div>
        </section>
        <Footer />
      </>
    );
  }
}

export default CompanyDetailsComponent;
