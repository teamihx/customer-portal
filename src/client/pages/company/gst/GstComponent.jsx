import React, { Component } from "react";
import SearchGstCompanyComponent from "../gst/SearchGstCompanyComponent";
import AddGstDetailsComponent from "../gst/AddGstDetailsComponent";
import GstDetailsComponent from "../gst/GstDetailsComponent";
import log from 'loglevel';

class GstComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyGstSearchPage: "true",
      addGstPage: "false",
      gstDetailsPage: "false",
      pageName: "search",
      gstResultsData: "",
      gstCompanyName: "",
      message: ""
    };

    this.searchGstrersult = this.searchGstrersult.bind(this);
    this.gstDetails = this.gstDetails.bind(this);
    this.companyNameEvent = this.companyNameEvent.bind(this);
    this.messageHandler = this.messageHandler.bind(this);
  }

  searchGstrersult(gstResults) {
    if (
      gstResults !== undefined &&
      gstResults.responsecode !== undefined &&
      gstResults.responsecode === 200
    ) {
      this.state.gstResultsData = gstResults;

      this.setState({
        companyGstSearchPage: "false",
        addGstPage: "false",
        gstDetailsPage: "true"
      });
    }
  }
  companyNameEvent(gstSearchCompanyName) {
    this.state.gstCompanyName = gstSearchCompanyName;
  }

  gstDetails(gstData,gstRes) {
    if (gstData !== "SEARCH PAGE") {
      this.setState({
        companyGstSearchPage: "false",
        addGstPage: "true",
        gstDetailsPage: "false",
        message: '',
        gstReults:gstRes
      });
    } else if (gstData === "SEARCH PAGE") {
      this.setState({
        companyGstSearchPage: "false",
        addGstPage: "false",
        gstDetailsPage: "true",
        message: ''
      });
    }
  }

  messageHandler(msg,addGstReq) {
    try{
    if (msg !== undefined && msg !== "") {


      if(addGstReq!=='' && addGstReq !==undefined 
      && addGstReq.gst_details!==undefined && addGstReq.gst_details!=='' 
      && addGstReq.gst_details.length>0  ){
      var newGstArray = []      
      
      newGstArray.push({ gst_holder_address: addGstReq.gst_details[0].gst_holder_address, 
      gst_holder_name: addGstReq.gst_details[0].gst_holder_name,
      gst_holder_state_code: addGstReq.gst_details[0].gst_holder_state_code, 
      gst_holder_state_name: addGstReq.gst_details[0].gst_holder_state_name,
      gst_number: addGstReq.gst_details[0].gst_number.slice(0, 15)});     
      Array.prototype.push.apply(this.state.gstResultsData.gst_details, newGstArray)
      }

      this.setState({
        companyGstSearchPage: "false",
        addGstPage: "false",
        gstDetailsPage: "true",
        message: msg
      });
    }
  }catch(err){

    log.error('Exception occured in GstComponent messageHandler function---'+err);

  }
  }

  render() {
    var content;
    if (this.state.companyGstSearchPage === "true") {
      content = (
        <SearchGstCompanyComponent
          gstSearchHandler={this.searchGstrersult}
          gstCompanyName={this.companyNameEvent}          
        />
      );
    } else if (this.state.gstDetailsPage === "true") {
      content = (
        <GstDetailsComponent
          gstData={this.state.gstResultsData}
          gstCompanyName={this.state.gstCompanyName}
          gstDetailsHandler={this.gstDetails}          
          addgstmsg={this.state.message}
        />
      );
    } else if (this.state.addGstPage === "true") {
      content = (
        <AddGstDetailsComponent
          gstAddDetailsHandler={this.gstDetails}
          gstCompanyName={this.state.gstCompanyName}
          gstDetailsMsg={this.messageHandler}
          gstResult={this.state.gstReults}
        />
      );
    }

    return (
      <>
        <section>{content}</section>
      </>
    );
  }
}

export default GstComponent;
