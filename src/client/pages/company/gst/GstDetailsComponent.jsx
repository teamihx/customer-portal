import React, { Component } from "react";
import GstService from "Services/gst/GstService.js";
import Icon from "Components/Icon";
import Header from "Pages/common/Header";
import Footer from "Pages/common/Footer";
import Button from "Components/buttons/button.jsx";
import { confirmAlert } from "react-confirm-alert";
import log from "loglevel";
export const COMP_CONFIG_ROLES = 'compConfigRoles'
class GstDetailsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gstHolderName: "",
      gstHolderaddress: "",
      stateCode: "",
      stateName: "",
      gstinList: "false",
      selGSTNo: "",
      gstinfields: "false",
      filtyeredGstData: "",
      responseData: "",
      message: "",
      errorMsg: "",
      gstId: "",
      loading: false,
      delEve: "",
      editEve: "",
      enableAddGst: false,
      enableEditGst: false,
      enableDeleteGst: false,
      gstResultData: this.props.gstData,
      addgstSuccessMsg: this.props.addgstmsg,
      errorMsgList: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.stateCodeChangeEvent = this.stateCodeChangeEvent.bind(this);
    this.addRecordEvent = this.addRecordEvent.bind(this);
    this.deletEvent = this.deletEvent.bind(this);
    this.editEvent = this.editEvent.bind(this);
    this.backBtnEvent = this.backBtnEvent.bind(this);
    this.gstinListener = this.gstinListener.bind(this);
    this.confirmDlg = this.confirmDlg.bind(this);
    //alert('gstData '+JSON.stringify(this.props.gstData));
    //alert('gstData '+JSON.stringify(this.props.gstCompanyName));
    
    let tripsObj=JSON.parse(localStorage.getItem(COMP_CONFIG_ROLES));
  if(tripsObj!==null && tripsObj.companyConfigPermisions!==null && tripsObj.companyConfigPermisions!==undefined){
    this.state.enableAddGst = tripsObj.companyConfigPermisions.GST_ADD_PERMSN_ENABLE;
    this.state.enableEditGst = tripsObj.companyConfigPermisions.GST_EDIT_PERMSN_ENABLE;
    this.state.enableDeleteGst = tripsObj.companyConfigPermisions.GST_DELETE_PERMSN_ENABLE;
  }
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.state.errorMsgList = [];
    this.state.errorMsg = "";
    this.state.message = "";
    this.state.addgstSuccessMsg = "";
  };

  stateCodeChangeEvent(e) {
    try {
      this.setState({ [e.target.name]: e.target.value });
      this.state.errorMsgList = [];
      this.state.errorMsg = "";
      this.state.message = "";
      this.state.addgstSuccessMsg = "";

      if (
        e.target.value !== undefined &&
        e.target.value !== "" &&
        Number(e.target.value.trim()) &&
        e.target.value.trim().length === 2
      ) {
        console.log("entered value is number");
      } else {
        this.state.errorMsgList.push("Gst state code must be 2 digits number");
      }
    } catch (err) {
      log.error(
        "Exception occured in GstDetailsComponent stateCodeChangeEvent function---" +
          err
      );
    }
  }

  addRecordEvent(e) {
    const addGstpage = "ADD PAGE";

    this.props.gstDetailsHandler(addGstpage, this.state.gstResultData);
  }

  confirmDlg = () => {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <>
            <div className="custom-ui">
              <h3>Are you sure?</h3>
              <p className="t-color2 mb-15">You want to delete the GST config?</p>
            </div>
            <div className="btn-grid text-right">
              <button
                className="btn btn-xs btn-primary w-50"
                onClick={() => {
                  this.deletEvent();
                  onClose();
                }}
              >
                Yes
              </button>
              <button className="btn btn-default btn-xs w-50" onClick={onClose}>
                No
              </button>
            </div>
          </>
        );
      }
    });
  };

  deletEvent(e) {
    try {
      this.state.errorMsgList = [];
      this.setState({ deletLoading: true });
      this.state.delEve = "true";
      this.state.editEve = "false";
      this.forceUpdate();
      const gstdelDetails = [
        {
          gst_number: this.state.selGSTNo
        }
      ];

      this.delGstReq = {
        domain_name: this.props.gstCompanyName,
        gst_details: gstdelDetails
      };
      this.deleteCompanyGSTReq(this.delGstReq);
    } catch (err) {
      this.setState({ deletLoading: false });
      this.forceUpdate();
      log.error(
        "Exception occured in GstDetailsComponent deletEvent function---" + err
      );
    }
  }

  deleteCompanyGSTReq(delGstReq) {
    this.state.message = "";
    GstService.deleteGstCompanyDetails(delGstReq).then(response => {
      try {
        const delGstResponse = JSON.parse(response.data);
        this.setState({ responseData: delGstResponse });

        console.log(
          "deleteGstCompanyDetails Res " +
            JSON.stringify(this.state.responseData)
        );

        if (
          this.state.responseData !== undefined &&
          this.state.responseData !== "" &&
          this.state.responseData.status !== undefined &&
          this.state.responseData.status !== "" &&
          this.state.responseData.status === 200
        ) {
          const msg =
            "GST number (" + this.state.selGSTNo + ") deleted successfully";
          this.setState({ message: msg });
          this.setState({ deletLoading: false });
          this.state.gstinfields = "false";
          let gstupdatedData = this.state.gstResultData.gst_details.filter(
            item => item.gst_number !== this.state.selGSTNo
          );
          this.state.gstResultData.gst_details = gstupdatedData;
          this.forceUpdate();
          //this.props.gstDetailsMsg(this.state.message);
        } else {
          this.setState({ deletLoading: false });
          this.forceUpdate();
          this.setState({ errorMsg: "GST number not deleted  " });
        }
      } catch (err) {
        this.setState({ deletLoading: false });
        this.forceUpdate();
        log.error(
          "Exception occured in GstCompanyDetails deleteCompanyGSTReq function---" +
            err
        );
      }
    });
  }

  editEvent(e) {
    try {
      this.setState({ editLoading: true });
      this.state.delEve = "false";
      this.state.editEve = "true";

      if (this.validateGSTEditFields()) {
        const gsteditDetails = [
          {
            id: this.state.gstId,
            gst_holder_address: this.state.gstHolderaddress.trim(),
            gst_holder_name: this.state.gstHolderName.trim(),
            gst_holder_state_code: this.state.stateCode.trim(),
            gst_holder_state_name: this.state.stateName.trim(),
            gst_number: this.state.selGSTNo
          }
        ];

        this.editReq = {
          domain_name: this.props.gstCompanyName,
          gst_details: gsteditDetails
        };

        this.editCompanyGSTReq(this.editReq);
      } else {
        console.log("Mandatory fileds required ");
        this.setState({ editLoading: false });
        this.forceUpdate();
      }
    } catch (err) {
      this.setState({ editLoading: false });
      this.forceUpdate();
      log.error(
        "Exception occured in GstDetailsComponent editEvent function---" + err
      );
    }
  }

  validateGSTEditFields() {
    var result = true;
    try {
      this.state.errorMsgList = [];

      if (
        this.state.gstHolderName === undefined ||
        this.state.gstHolderName.trim() === ""
      ) {
        this.state.errorMsgList.push("Gst Holder name must be provided");
        result = false;
      }

      if (
        this.state.gstHolderaddress === undefined ||
        this.state.gstHolderaddress.trim() === ""
      ) {
        this.state.errorMsgList.push("Gst Holder address must be provided");
        result = false;
      }

      if (
        this.state.stateCode === undefined ||
        this.state.stateCode.trim() === ""
      ) {
        this.state.errorMsgList.push("State code must be provided");
        result = false;
      } else {
        if (
          this.state.stateCode !== undefined &&
          this.state.stateCode.trim() !== "" &&
          Number(this.state.stateCode) &&
          this.state.stateCode.length == 2
        ) {
        } else {
          this.state.errorMsgList.push(
            "Gst state code must be 2 digits number"
          );
          result = false;
        }
      }

      if (
        this.state.stateName === undefined ||
        this.state.stateName.trim() === ""
      ) {
        this.state.errorMsgList.push("State name must be provided");
        result = false;
      }
    } catch (err) {
      this.setState({ editLoading: false });
      this.forceUpdate();
      log.error(
        "Exception occured in GstDetailsComponent validateGSTEditFields function---" +
          err
      );
    }

    return result;
  }

  editCompanyGSTReq(editRequest) {
    this.state.message = "";
    this.state.errorMsg = "";
    //this.setState({loading:true});
    this.forceUpdate();
    GstService.editGstCompanyDetails(editRequest).then(response => {
      try {
        this.state.responseData = JSON.parse(response.data);
        console.log(
          "editGstCompanyDetails Res " + JSON.stringify(this.state.responseData)
        );
        if (
          this.state.responseData !== undefined &&
          this.state.responseData !== "" &&
          this.state.responseData.status !== undefined &&
          this.state.responseData.status !== "" &&
          this.state.responseData.status === 200
        ) {
          const msg =
            "GST (" + this.state.selGSTNo + ") details updated successfully";
          this.setState({ message: msg });
          this.setState({ editLoading: false });
          let gstupdatedData = this.state.gstResultData.gst_details.map(
            (gstres, sidx) => {
              if (this.state.selGSTNo !== gstres.gst_number) return gstres;
              return {
                ...gstres,
                id: this.state.gstId,
                gst_holder_address: this.state.gstHolderaddress,
                gst_holder_name: this.state.gstHolderName,
                gst_holder_state_code: this.state.stateCode,
                gst_holder_state_name: this.state.stateName,
                gst_number: this.state.selGSTNo
              };
            }
          );

          this.state.gstResultData.gst_details = gstupdatedData;
          this.forceUpdate();
          //this.props.gstDetailsMsg(this.state.message);
        } else {
          this.setState({ editLoading: false });
          this.forceUpdate();
          this.setState({ errorMsg: "GST details not updated " });
        }
      } catch (err) {
        this.setState({ editLoading: false });
        this.forceUpdate();
        log.error(
          "Exception occured in GstDetailsComponent editCompanyGSTReq function---" +
            err
        );
      }
    });
  }

  backBtnEvent(e) {
    const backbtn = "SEARCH PAGE";
    this.state.errorMsgList = [];
    this.state.message = "";
    //this.props.gstDetailsHandler(backbtn);
    this.state.gstinfields = "false";
    this.state.selGSTNo = "";
    this.forceUpdate();
  }

  componentWillMount() {
    if (
      this.props.gstData != undefined &&
      this.props.gstData.responsecode != undefined &&
      this.props.gstData.responsecode === 200 &&
      this.props.gstData.gst_details != undefined &&
        this.props.gstData.gst_details.length > 0
    ) {
      this.state.gstinList = "true";
    } else {
      this.state.gstinList = "false";
    }
  }

  gstinListener = e => {
    try {
      this.setState({ [e.target.name]: e.target.value });
      this.state.selGSTNo = e.target.value;
      this.state.errorMsg = "";
      this.state.message = "";
      this.state.addgstSuccessMsg = "";
      this.state.errorMsgList = [];

      if (this.state.selGSTNo !== "" && this.state.selGSTNo !== undefined) {
        this.state.gstinfields = "true";
      } else {
        this.state.gstinfields = "false";
      }

      this.state.filtyeredGstData = this.state.gstResultData.gst_details.map(
        (gstResult, index) => {
          if (this.state.selGSTNo === gstResult.gst_number) {
            this.state.gstHolderName = gstResult.gst_holder_name;
            this.state.gstHolderaddress = gstResult.gst_holder_address;
            this.state.stateCode = gstResult.gst_holder_state_code;
            this.state.stateName = gstResult.gst_holder_state_name;
            this.state.gstId = gstResult.id;
            return gstResult;
          } else {
            return "";
          }
        }
      );
    } catch (err) {
      log.error(
        "Exception occured in GstDetailsComponent gstinListener function---" +
          err
      );
    }
  };

  render() {
    return (
      <>
        <Header />
        <section>
        <div className="main-container pageTtl">
          <h3>GST Record</h3>
        </div>
        </section>
        <section className="cont-Box">
        <div className="main-container">
          {this.state.errorMsgList !== "" &&
            this.state.errorMsgList.length > 0 && (
              <div className="alert alert-danger">
                <Icon
                  className="noticicon"
                  color="#F4675F"
                  size={16}
                  icon="warning"
                />
                {this.state.errorMsgList.map((item, key) => (
                  <span key={item}>{item}</span>
                ))}
              </div>
            )}

          {this.state.errorMsg !== "" && this.state.errorMsg !== undefined && (
            <div className="alert alert-danger">
              <Icon
                className="noticicon"
                color="#F4675F"
                size={16}
                icon="warning"
              />
              {this.state.errorMsg}
            </div>
          )}

          {this.state.message !== "" && this.state.message !== undefined && (
            <div className="alert notification-success">
              <Icon color="#02AE79" size={16} icon="success" />
              {this.state.message}
            </div>
          )}

          {this.state.addgstSuccessMsg !== "" &&
            this.state.addgstSuccessMsg !== undefined && (
              <div className="alert notification-success">
                <Icon color="#02AE79" size={16} icon="success" />
                {this.state.addgstSuccessMsg}
              </div>
            )}

          <div className="selectedComp">
            <label className="SubTitle">
              Domain name : <strong>{this.props.gstCompanyName}</strong>
            </label>
          </div>
          <div className="form-group">
            <div className="row">
              <div className="col-5">
                <div className="row">
                  {this.state.gstinList == "true" ? (
                    <>
                      <div className="col-4">
                        
                        <label>Select GST</label>
                      </div>
                      <div className="col-6">
                        <div className="custom-select-v3">
                          <Icon
                            className="select-me"
                            color="#CAD6E3"
                            size={20}
                            icon="down-arrow"
                          />

                          <select
                            name="selGSTNo"
                            name="selGSTNo"
                            value={this.state.selGSTNo}
                            onChange={this.gstinListener}
                          >
                            <option value="">-Select-</option>
                            {this.state.gstResultData.gst_details.map(
                              (gstResult, index) => (
                                <option
                                  key={index}
                                  value={gstResult.gst_number}
                                >
                                  {gstResult.gst_number}
                                </option>
                              )
                            )}
                          </select>
                        </div>
                      </div>
                    </>
                  ) : (
                    ""
                  )}
                  {this.state.enableAddGst && (
                    <div className="col-1">
                      <Button
                        size="xs"
                        type="primary"
                        id="addbtnId"
                        className="w-108"
                        onClickBtn={this.addRecordEvent}
                        btnTitle="Add New GST"
                      ></Button>
                    </div>
                  )}
                </div>
              </div>
            </div>

            {this.state.gstinfields == "true" ? (
              <>
                <div className="row">
                  <div className="col-5">
                    <div className="row">
                      <div className="col-4">
                        <label className="required">GST holder name</label>
                      </div>
                      <div className="col-6">
                        <input
                          type="text"
                          name="gstHolderName"
                          onChange={this.handleChange}
                          value={this.state.gstHolderName.trim()}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-4">
                        <label className="required">GST holder address</label>
                      </div>
                      <div className="col-6">
                        <input
                          type="text"
                          name="gstHolderaddress"
                          onChange={this.handleChange}
                          value={this.state.gstHolderaddress.trim()}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-5">
                    <div className="row">
                      <div className="col-3">
                        <label className="required">State code</label>
                      </div>

                      <div className="col-6">
                        <input
                          type="text"
                          name="stateCode"
                          onChange={this.stateCodeChangeEvent}
                          value={this.state.stateCode.trim()}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-3">
                        <label className="required">State name</label>
                      </div>
                      <div className="col-6">
                        <input
                          type="text"
                          name="stateName"
                          onChange={this.handleChange}
                          value={this.state.stateName.trim()}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="btnSec">
                  <Button
                    size="xs"
                    viewType="default"
                    id="backbtnId"
                    onClickBtn={this.backBtnEvent}
                    btnTitle="Cancel"
                    className="w-100"
                  ></Button>

                  {this.state.enableDeleteGst && (
                    /*  <Button
                    size="xs"
                    viewType="primary"
                    id="delbtnId"
                    className="w-100"
                    onClickBtn={this.deletEvent}
                    loading = {this.state.deletLoading}
                    btnTitle='Delete GST'
                  >
                    
                  </Button> */

                    <Button
                      size="xs"
                      viewType="primary"
                      id="delbtnId"
                      className="w-100"
                      onClickBtn={this.confirmDlg}
                      loading={this.state.deletLoading}
                      btnTitle="Delete GST"
                    ></Button>
                  )}

                  {this.state.enableEditGst && (
                    <Button
                      size="xs"
                      viewType="primary"
                      id="editbtnId"
                      className="w-100"
                      onClickBtn={this.editEvent}
                      loading={this.state.editLoading}
                      btnTitle="Update GST"
                    ></Button>
                  )}
                </div>
              </>
            ) : (
              ""
            )}
          </div>
        </div>
        </section>
        <Footer />
      </>
    );
  }
}
export default GstDetailsComponent;
