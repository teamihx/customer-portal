import React, { Component } from "react";
import GstService from "Services/gst/GstService.js";
import Icon from "Components/Icon";
import Header from "Pages/common/Header";
import Footer from "Pages/common/Footer";
import Button from "Components/buttons/button.jsx";
import log from "loglevel";

class SearchGstCompanyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      companyName: "",
      companyGstDetails: "",
      isResult: false,
      pageData: "gstDetails",
      errorMsg: "",
      //successMsg: this.props.successMsg,
      successMsg: "",
      loading: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    // alert('jsonData'+JSON.stringify(jsonData));
    //alert('jsonData'+this.props.successMsg);
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value, successMsg: "" });
  };

  handleSubmit(e) {
    try {
      e.preventDefault();
      this.state.errorMsg = "";
      this.setState({ loading: true });

      if (
        this.state.companyName.trim() !== undefined &&
        this.state.companyName.trim() !== ""
      ) {
        this.searchGstComReq = {
          domain_name: this.state.companyName.trim()
        };

        this.searchCompanyReq(this.searchGstComReq);
        this.forceUpdate();
      } else {
        this.setState({ errorMsg: "Domain name must be provided" });
        this.setState({ loading: false });
        this.forceUpdate();
      }
    } catch (err) {
      this.setState({ loading: false });
      this.forceUpdate();
      log.error(
        "Exception occured in SearchGstCompanyComponent handleSubmit function---" +
          err
      );
    }
  }

  searchCompanyReq(searchComReq) {
    this.state.errorMsg = "";
    GstService.fetchGstCompanyDetails(searchComReq).then(response => {
      try {
        const companyGstDetails = JSON.parse(response.data);
        this.setState({ companyGstDetails });
        //console.log("fetchGstCompanyDetails Res " + response.data);
        if (
          this.state.companyGstDetails !== undefined &&
          this.state.companyGstDetails !== "" &&
          this.state.companyGstDetails.responsecode !== undefined &&
          this.state.companyGstDetails.responsecode === 200
        ) {
          this.state.errorMsg = "";

          this.props.gstCompanyName(this.state.companyName);
          this.props.gstSearchHandler(this.state.companyGstDetails);
        } else {
          this.state.errorMsg = "Searched domain does not exist";
          this.setState({ loading: false });
          this.forceUpdate();
        }
      } catch (error) {
        this.setState({ loading: false });
        this.forceUpdate();
        /* console.log(
          "exception Occured in searchCompanyReq function---" + error
        ); */
        log.error("exception Occured in searchCompanyReq function---" + error);
      }
    });
  }

  render() {
    return (
      <>
        <Header />
        <form onSubmit={e => this.handleSubmit(e)}>
          <section>
          <div className="main-container pageTtl">
            <h3>GST Details</h3>
          </div>
          </section>
          <section className="cont-Box">
          <div className="main-container">
            {this.state.errorMsg !== "" && this.state.errorMsg !== undefined && (
              <div className="alert alert-danger">
                <Icon
                  className="noticicon"
                  color="#F4675F"
                  size={16}
                  icon="warning"
                />
                {this.state.errorMsg}
              </div>
            )}
            {this.state.successMsg !== "" &&
              this.state.successMsg !== undefined && (
                <div className="alert notification-success">
                  <Icon color="#02AE79" size={16} icon="success" />
                  {this.state.successMsg}
                </div>
              )}
            <div className="form-group">
              <div className="row">
                <div className="col-">
                  <label className="required">Domain name</label>
                </div>
                <div className="col-2">
                  <input
                    placeholder="Enter your domain"
                    type="text"
                    name="companyName"
                    onChange={this.handleChange}
                  />
                </div>
                <div className="col-3">
                  <Button
                    size="xs"
                    viewType="primary"
                    className="w-90"
                    loading={this.state.loading}
                    btnTitle="Search"
                  ></Button>
                </div>
              </div>
            </div>
          </div>
          </section>
        </form>
        <Footer />
      </>
    );
  }
}
export default SearchGstCompanyComponent;
