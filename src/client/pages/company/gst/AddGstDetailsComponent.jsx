import React, { Component } from "react";
import Header from "Pages/common/Header";
import Footer from "Pages/common/Footer";
import Icon from "Components/Icon";
import GstService from "Services/gst/GstService.js";
import Button from "Components/buttons/button.jsx";
const gstCodesMasterData = require("Assets/json/GSTCodesMasterData.json");
import log from "loglevel";

class AddGstDetailsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gstNumber: "",
      gstHolderName: "",
      gstHolderaddress: "",
      gstStateCode: "",
      gstStateName: "",
      message: "",
      loading: false,
      gstAvaliable: "",
      errorMsgList: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.backBtnEvent = this.backBtnEvent.bind(this);
    this.addgstDetEvent = this.addgstDetEvent.bind(this);
    //alert('this.props.gstCompanyName '+JSON.stringify(this.props.gstCompanyName));
    //console.log('this.props.gstResult '+JSON.stringify(this.props.gstResult));
  }

  handleChange = e => {
    this.state.gstAvaliable = "";
    this.setState({
      [e.target.name]: e.target.value,
      message: "",
      errorMsgList: []
    });
  };

  addgstDetEvent(e) {
    try {
      this.setState({ loading: true });
      this.state.gstAvaliable = "";
      this.forceUpdate();

      if (this.validateGSTaddFields()) {
        this.props.gstResult.gst_details.map((gstResult, index) => {
          if (
            this.state.gstNumber !== "" &&
            this.state.gstNumber !== undefined &&
            this.state.gstNumber.slice(0, 15) === gstResult.gst_number
          ) {
            this.state.gstAvaliable = "GST number already exists";
          }
        });

        if (
          this.state.gstAvaliable === "" ||
          this.state.gstAvaliable === undefined
        ) {
          const gst_value = this.state.gstNumber.slice(0, 2);
          if (
            this.state.gstNumber !== undefined &&
            this.state.gstNumber !== "" &&
            this.state.gstNumber.slice(0, 2) != undefined &&
            this.state.gstNumber.slice(0, 2) !== "" &&
            Number(gst_value)
          ) {
            const statename = gstCodesMasterData[gst_value];

            if (statename !== "" && statename !== undefined) {
              this.state.gstStateName = statename.state_name;
            } else {
              this.state.gstStateName = "";
            }

            this.state.gstStateCode = this.state.gstNumber.slice(0, 2);
          } else {
            this.setState({ gstStateName: "", gstStateCode: "" });
          }

          const gsteditDetails = [
            {
              gst_holder_address: this.state.gstHolderaddress,
              gst_holder_name: this.state.gstHolderName,
              gst_holder_state_code: this.state.gstStateCode,
              gst_holder_state_name: this.state.gstStateName,
              gst_number: this.state.gstNumber
            }
          ];

          this.addGstReq = {
            domain_name: this.props.gstCompanyName,
            gst_details: gsteditDetails
          };

          this.addGstDetailsReq(this.addGstReq);
        } else {
          this.setState({ loading: false });
          this.forceUpdate();
        }
      } else {
        this.setState({ loading: false });
        //this.state.message = 'GST number must be required';
        this.forceUpdate();
      }
    } catch (err) {
      this.setState({ loading: false });
      this.forceUpdate();
      log.error(
        "Exception occured in AddGstDetailsComponent addgstDetEvent function---" +
          err
      );
    }
  }

  validateGSTaddFields() {
    var result = true;
    try {
      this.state.errorMsgList = [];

      if (
        this.state.gstNumber === undefined ||
        this.state.gstNumber.trim() === ""
      ) {
        this.state.errorMsgList.push("Gst number must be provided");
        result = false;
      }

      if (
        this.state.gstHolderName === undefined ||
        this.state.gstHolderName.trim() === ""
      ) {
        this.state.errorMsgList.push("Gst Holder name must be provided");
        result = false;
      }
    } catch (err) {
      log.error(
        "Exception occured in AddGstDetailsComponent validateGSTaddFields function---" +
          err
      );
    }

    return result;
  }

  addGstDetailsReq(addGstReq) {
    this.state.message = "";
    GstService.addGstCompanyDetails(addGstReq).then(response => {
      try {
        const addGstResponse = JSON.parse(response.data);
        this.setState({ addGstResponse });
        /* console.log(
          "addGstCompanyDetails Res " +
            JSON.stringify(this.state.addGstResponse)
        ); */

        if (
          this.state.addGstResponse !== undefined &&
          this.state.addGstResponse !== "" &&
          this.state.addGstResponse.status !== undefined &&
          this.state.addGstResponse.status !== "" &&
          this.state.addGstResponse.status === 200
        ) {
          const msg =
            "GST number (" +
            this.state.gstNumber.slice(0, 15) +
            ") added successfully";
          //alert(msg);
          this.setState({ message: msg });
          this.setState({ loading: false });
          this.props.gstDetailsMsg(this.state.message, addGstReq);
        } else {
          this.state.message = "GST number not added  ";
          this.setState({ loading: false });
          this.forceUpdate();
        }
      } catch (err) {
        this.setState({ loading: false });
        this.forceUpdate();
        log.error(
          "exception Occured in AddGstDetailsComponent addGstDetailsReq function---" +
            err
        );
      }
    });
  }

  backBtnEvent(e) {
    const backbtn = "SEARCH PAGE";
    this.props.gstAddDetailsHandler(backbtn);
  }

  render() {
    return (
      <>
        <Header />
        <section>
        <div className="main-container pageTtl">
          <h3>Add GST Record</h3>
        </div>
        </section>
        <section className="cont-Box">
        <div className="main-container">
          {this.state.errorMsgList !== "" &&
            this.state.errorMsgList.length > 0 && (
              <div className="alert alert-danger">
                <Icon
                  className="noticicon"
                  color="#F4675F"
                  size={16}
                  icon="warning"
                />
                {this.state.errorMsgList.map((item, key) => (
                  <span key={item}>{item}</span>
                ))}
              </div>
            )}

          {this.state.message !== "" && this.state.message !== undefined && (
            <div className="alert alert-danger">
              <Icon
                className="noticicon"
                color="#F4675F"
                size={16}
                icon="warning"
              />
              {this.state.message}
            </div>
          )}

          {this.state.gstAvaliable !== "" &&
            this.state.gstAvaliable !== undefined && (
              <div className="alert alert-danger">
                <Icon
                  className="noticicon"
                  color="#F4675F"
                  size={16}
                  icon="warning"
                />
                {this.state.gstAvaliable}
              </div>
            )}

          <div className="selectedComp">
            <label className="SubTitle">
              Domain name : <strong>{this.props.gstCompanyName}</strong>
            </label>
          </div>
          <div className="form-group">
            <div className="row">
              <div className="col-10">
                <div className="row">
                  <div className="col-2">
                    <label className="required">GST number</label>
                  </div>
                  <div className="col-3">
                    <input
                      type="text"
                      name="gstNumber"
                      placeholder="Enter GST number"
                      onChange={this.handleChange}
                      value={this.state.gstNumber}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <label className="required">GST holder name</label>
                  </div>
                  <div className="col-3">
                    <input
                      type="text"
                      placeholder="Enter GST holder name"
                      name="gstHolderName"
                      onChange={this.handleChange}
                      value={this.state.gstHolderName}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <label>GST holder address </label>
                  </div>
                  <div className="col-3">
                    <input
                      type="text"
                      placeholder="Enter GST holder address"
                      name="gstHolderaddress"
                      onChange={this.handleChange}
                      value={this.state.gstHolderaddress}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="btnSec">
            <Button
              size="xs"
              viewType="default"
              onClickBtn={this.backBtnEvent}
              btnTitle="Cancel"
              className="w-80"
            ></Button>

            <Button
              size="xs"
              viewType="primary"
              className="w-115"
              onClickBtn={this.addgstDetEvent}
              loading={this.state.loading}
              btnTitle="Add New GST"
            ></Button>
          </div>
        </div>
        </section>
        <Footer />
      </>
    );
  }
}
export default AddGstDetailsComponent;
