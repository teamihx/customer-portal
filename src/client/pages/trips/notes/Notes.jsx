import React, { Component } from "react";
import { connect } from "react-redux";
import ReactHtmlParser from "react-html-parser";
import { MONTHS, DAYS } from "Pages/common/constants.js";
import NoteService from "../../../services/trips/note/NoteService";
import Button from "Components/buttons/button.jsx";
import Icon from "Components/Icon";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import UserFetchService from "../../../services/trips/user/UserFetchService";
import Notification from "Components/Notification.jsx";
import * as actions from "../../../store/actions/index";
export const USER_AUTH_DATA = "userAuthData";
export const TRIPS_ROLES = "tripsRoles";
class Notes extends Component {
  constructor(props) {
    super(props);
    //var data =require('../../../assets/json/data.json')
    let minirules, baggage;
    if (
      this.props.tripDetail &&
      this.props.tripDetail.air_bookings &&
      this.props.tripDetail.air_bookings[0]
    ) {
      minirules = this.props.tripDetail.air_bookings[0].minirule_info || {};
      baggage = this.props.tripDetail.air_bookings[0].free_baggage_info || {};
    }

    // var tripNotes1=JSON.parse(data);
    // console.log(tripNotes1);
    this.state = {
      minirules,
      baggage,
      tripNotes: Object.values(this.props.tripDetail.notes),
      newNote: "",
      errorMessage: "",
      message: "",
      userIds: [],
      userid: "",
      userDetailsresponse: "",
      userMasterDatamap: "",
      addNote: false,
      loading: false,
      ftripsPermissions: "",
      htripsPermissions: ""
      //tripNotes: notes.sort((a,b)=>new Date(a.created_at)-new Date(b.created_at))
    };
    this.addNoteHandler = this.addNoteHandler.bind(this);
    this.updateTripNotes = this.updateTripNotes.bind(this);
    this.handleChangeAddnote = this.handleChangeAddnote.bind(this);

    //Getting ROLES DATA from local storage
    let tripsObj = JSON.parse(localStorage.getItem(TRIPS_ROLES));
    this.state.ftripsPermissions = tripsObj.ftripPermissions;
    this.state.htripsPermissions = tripsObj.htripPermissions;
    this.state.localTripRoles = tripsObj.localtripsRoles;
    this.state.trainTripsRoles = tripsObj.trainTripsRoles;
  }
  componentDidMount() {
    //console.log(' trip id: '+this.props.tripid)
    this.setState({
      tripNotes: this.state.tripNotes
        .sort((a, b) => {
          return (
            new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
          );
        })
        .reverse()
    });
    this.fetchNoteUserMailids();
  }

  fetchNoteUserMailids() {
    try {
      let userids = [];
      this.state.tripNotes.map((note, idx) => {
        userids.push(note.user_id);
      });
      //console.log('fetchNoteUserMailids----'+userids);

      userids = Array.from(new Set(userids));
      userids = userids.filter(function(el) {
        return el != null;
      });
      this.state.userids = userids;
      this.fetchNoteUserDetails(userids);
    } catch (error) {
      log.error(
        "Exception occured in AuditTrail fetchUserMailids function---" + error
      );
    }
  }

  fetchNoteUserDetails(usermailids) {
    for (var id in usermailids) {
      const data = this.props.usermasterdetails[usermailids[id]];
      if (data !== undefined) {
      } else {
        this.state.userid = usermailids[id];
        this.fetchNoteUserEmailService(usermailids[id]);
      }
    }
  }

  fetchNoteUserEmailService(usermailids) {
    let userids = [];
    userids.push(usermailids);

    const usermailidsReq = {
      person_ids: userids,
      query: ["username", "personal_data"]
    };

    UserFetchService.fetchUserMailid(usermailidsReq).then(response => {
      let userDetailsresponse = JSON.parse(response.data);
      this.state.userDetailsresponse = userDetailsresponse;
      this.props.usermasterdetails[
        this.state.userid
      ] = this.state.userDetailsresponse[this.state.userid];
      this.props.updateUserDetails(this.state.userDetailsresponse);
    });
  }

  getEmailid(userid) {
    //console.log('userid----'+userid);

    let emailid = "";
    if (
      this.state.userMasterDatamap !== undefined &&
      this.state.userMasterDatamap !== ""
    ) {
      let data = this.state.userMasterDatamap[userid];
      if (data !== undefined && data !== "") {
        emailid = data.uname;
      }
    }
    return emailid;
  }

  getFnameLname(userid) {
    let fname = "";
    let lname = "";
    if (
      this.state.userMasterDatamap !== undefined &&
      this.state.userMasterDatamap !== ""
    ) {
      let data = this.state.userMasterDatamap[userid];
      if (data !== undefined && data !== "") {
        fname = data.fname;
        lname = data.lname;
      }
    }
    //console.log('getFnameLname----'+fname+" "+lname);
    return fname + " " + lname;
  }

  builduserDataMap() {
    let userData = this.props.usermasterdetails;
    var items = {};
    if (this.state.userids !== undefined && this.state.userids !== "") {
      for (var key in this.state.userids) {
        if (userData !== undefined && userData !== "") {
          const d = userData[this.state.userids[key]];
          let key1 = new Object();
          if (
            d !== undefined &&
            d !== "" &&
            key !== "responsecode" &&
            key !== "msg"
          ) {
            (key1.uname = d.username),
              (key1.fname = d.personal_data.first_name);
            key1.lname = d.personal_data.last_name;
            items[this.state.userids[key]] = key1;
          }
        }
      }
    }

    this.state.userMasterDatamap = items;
  }

  fetchUpdateUserEmailds() {
    this.setState({
      tripNotes: this.state.tripNotes
        .sort((a, b) => {
          return (
            new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
          );
        })
        .reverse()
    });
    this.fetchNoteUserMailids();
    this.setState({ addNotes: false });
  }

  handleChangeAddnote(event) {
    this.setState({ addNotes: false });
    event.persist();
    //console.log("Event===="+event.target.value)
    this.setState({
      newNote: event.target.value
    });
  }

  updateTripNotes(noteTobeAdded) {
    let noteData = JSON.parse(noteTobeAdded);
    let noteDetails = noteData.modelsUpdated.notes;
    this.setState({
      //tripNotes:this.state.tripNotes.concat(respnote)
      //[...this.state.tripNotes,respnote]
      tripNotes: this.state.tripNotes
        .concat(noteDetails)
        .sort(
          (a, b) =>
            new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
        )
        .reverse(),
      newNote: ""
    });
    this.props.tripDetail.notes = this.state.tripNotes;
    this.props.updateNotes(this.props.tripDetail);
    if (this.state.addNote === true) {
      //console.log('render---this.state.addNote==='+this.state.addNote)
      this.fetchUpdateUserEmailds();
    }
  }

  addNoteHandler(event) {
    try {
      //console.log('in addNote handle'+this.state.newNote)
      this.setState({ addNotes: true });
      if (this.state.newNote.length>0) {
        this.state.addNote = true;
        let authDat = localStorage.getItem(USER_AUTH_DATA);
        let obj1 = JSON.parse(authDat);
        let userid = obj1.data.id;

        const noteTobeAdded = {
          notes: [
            {
              note: this.state.newNote,
              parent_note_id: null,
              subject: "Trip Details: ",
              user_id: userid,
              trip_id: this.props.tripDetail.id,
              created_at: new Date().toUTCString()
            }
          ]
        };
        //console.log( 'request'+noteTobeAdded.notes[0].trip_id+''+this.state.newNote)
        //console.log( 'request------'+JSON.stringify(noteTobeAdded))
        NoteService.updatetripNotes(noteTobeAdded)
          .then(response => {
            //console.log("updated response===="+JSON.stringify(response));

            const data = JSON.parse(response.data);
            const { updateStatus } = data;
            console.log("updateStatus==="+updateStatus)
            if (updateStatus === "true") {
              //console.log('response from json {response.notes}'+response.data)
              this.setState({message:"Note added successfully", showAlert: true})
              //this.state.message = "Note added successfully";
              this.forceUpdate();
              this.updateTripNotes(response.data);
            } else {
              this.setState({errorMessage:"Note not added", showAlert: true, addNotes: false,})
              this.forceUpdate();

            }
          })

          .catch(error => this.setState({ error, addNotes: false }));
      } else {
        this.setState({errorMessage:"Please Enter the  note", showAlert: true, addNotes: false,})
      }
    } catch (err) {
      this.setState({errorMessage:"Please Enter the  note", showAlert: true, addNotes: false,})
      this.forceUpdate();
    }
  }
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  render() {
    let viewType, messageText;
    if (this.state.errorMessage !== "" &&  this.state.errorMessage !== undefined) {
      viewType = "error";
      messageText = this.state.errorMessage;
    } else if (this.state.message !== "" && this.state.message !== undefined) {
      viewType = "success";
      messageText = this.state.message;
    }

    this.builduserDataMap();
    let finalData = null;
    try {
      finalData = JSON.parse(this.state.baggage);
    } catch (e) {
      finalData = this.state.baggage;
    }
    let mnrul = null;

    try {
      mnrul = JSON.parse(this.state.minirules);
    } catch (e) {
      // You can read e for more info
      // Let's assume the error is that we already have parsed the payload
      // So just return that
      mnrul = this.state.minirules;
    }

    return (
      <>
      {viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}
        <div className="">
          {/* <Notification viewType='error' messageText= {this.state.errorMessage}/>
          {this.state.errorMessage !== "" &&  this.state.errorMessage !== undefined && (
              <div className="alert alert-danger">
                <Icon
                  className="noticicon"
                  color="#F4675F"
                  size={16}
                  icon="warning"
                />
                {this.state.errorMessage}
              </div>
            )}
          {/* <Notification viewType='success' messageText= {this.state.message}/> 
          {this.state.message !== "" && this.state.message !== undefined && (
            <div className="alert notification-success">
              <Icon color="#02AE79" size={16} icon="success" />
              {this.state.message}
            </div>
          )} */}
          <div className="highInfo border mb-10 ">Add a note to this trip</div>
          <textarea
            id="addNoteID"
            type="text"
            value={this.state.newNote}
            onChange={this.handleChangeAddnote}
          ></textarea>
          <div className="btnSec">
            {((this.props.tripDetail.trip_type === 1 &&
              this.state.ftripsPermissions.ADD_NOTE_PERMSN_ENABLE) ||
              (this.props.tripDetail.trip_type === 2 &&
                this.state.htripsPermissions.HTL_ADD_NOTE_PERMSN_ENABLE) ||
              (this.props.tripDetail.trip_type === 16 &&
                this.state.localTripRoles.LOCAL_ADD_NOTE_ROLE_ENABLE) ||
              (this.props.tripDetail.trip_type === 4 &&
                this.state.trainTripsRoles.TRAIN_ADD_NOTE_ROLE_ENABLE)) && (
              <Button
                size="xs"
                viewType="primary"
                onClickBtn={this.addNoteHandler}
                loading={this.state.addNotes}
                btnTitle="Add note">                  
                </Button>
            )}
            {/* <button className="btn btn-xs btn-primary" onClick ={this.addNoteHandler}>Add this note</button> */}
          </div>
        </div>
        <div className="highInfo border mb-10">Mini Rule and Baggage info</div>
        <div className="minirule resTable">
          <table>
            <tbody>
              {!this.isEmpty(this.state.minirules) &&
              this.state.minirules !== null ? (
                <tr key={this.state.minirules.toString()}>
                  <td width="10%">Note</td>
                  <td width="80%">
                    <span>Mini Rule</span>
                    <div className="h170 mt-15 bb">
                      <CustomScroll heightRelativeToParent="calc(100% - 20px)">
                        <ReactJson src={mnrul} />
                      </CustomScroll>
                    </div>
                  </td>
                </tr>
              ) : (
                ""
              )}
              {!this.isEmpty(this.state.baggage) &&
              this.state.baggage !== null ? (
                <tr key={this.state.baggage.toString()}>
                  <td width="10%">Note</td>
                  <td width="80%">
                    <span>Baggage Info</span>
                    {/* {PrettyPrintJson()} */}
                    <div className="h170  mt-15">
                      <CustomScroll heightRelativeToParent="calc(100% - 20px)">
                        <ReactJson src={finalData} />
                      </CustomScroll>
                    </div>
                  </td>
                </tr>
              ) : (
                ""
                )}
            </tbody>
          </table>
        </div>
        <div>
          <div className="highInfo border mb-10">Trip notes</div>
          {this.state.tripNotes.map(trip => {
            const createdDate = new Date(trip.created_at);
            const fomattedData = `${
              MONTHS[createdDate.getMonth()]
            } ${createdDate.getDate()} `;
            const time = `${createdDate.getHours()}:  ${createdDate.getMinutes()}`;
            return (
              <div key={trip.id} className="resTable">
                <table>
                  <thead>
                    <tr key={trip.id}>
                      <td width="10%">Note</td>
                      <td width="80%">
                        <span className="d-b mb-5">{trip.subject}</span>
                        <p className="font-12 mb-5">
                          {ReactHtmlParser(trip.note)}
                        </p>
                        <span>
                          Posted by {this.getFnameLname(trip.user_id)} (
                          {this.getEmailid(trip.user_id)})
                        </span>
                      </td>
                      <td align="right" width="10%">
                        <span className="w-50 d-b font-12">
                          {fomattedData} <span className="d-b">{time}</span>
                        </span>
                      </td>
                    </tr>
                  </thead>
                </table>
              </div>
            );
          })}
        </div>
      </>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: req => dispatch(actions.updateUserDetails(req)),
    updateNotes: req => dispatch(actions.updateNoteDetails(req))
  };
};

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    usermasterdetails: state.trpReducer.usermasterdetails
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Notes);
