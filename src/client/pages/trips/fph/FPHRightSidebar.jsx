import React, { Component } from 'react';
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import Button from 'Components/buttons/button.jsx'
import ToggleDisplay from 'react-toggle-display';
//import { Link } from 'react-router-dom';
import log from 'loglevel';
import FlightSMS from "./../flight/FlightSMS";
//import Notification from 'Components/Notification.jsx';
import FlightEmailDetails from './../flight/FlightEmailDetails';
import FlightEmailReceipt from './../flight/FlightSMS';
export const contextPath = process.env.contextpath;
import FlightPricingDetail from "./../flight/FlightSMS";
import ChangeBkgStatusClsTxn from "../generic/ChangeBkgStatusClsTxn";
import Tags from 'Pages/trips/generic/Tags.jsx';
import ShowHide from 'Components/ShowHide.jsx';
import FlightSalesReceipt from './../flight/FlightSMS';
import FlightEmailETicket from './../flight/FlightSMS';
import FlightEmailVATInvoice from './../flight/FlightSMS';
import EmailBookStepScreenShot from '../generic/EmailBookStepScreenShot'
import TripGenericService from '../../../services/trips/generic/TripGenericService'
import FPHEmailInvoice from './FPHEmailInvoice';
import FPHEmailDetails from './FPHEmailDetails';
import FPHPrintEticket from './FPHPrintEticket';

import Domainpath from '../../../services/commonUtils/Domainpath';

import { BrowserRouter as Router, Link, Route } from "react-router-dom";




class FPHRightSidebar extends Component {
  constructor(props) {
    super(props)
    //let tripid = [];
    this.redirect = this.redirect.bind(this);
    this.state = {
      //fphRS: this.props.paxData,
      tripid: this.props.tripDetail.trip_ref,
      tripDetail: this.props.tripDetail,
      show: false,
      emailDetailsShow: false,
      emailReceiptShow: false,
      emailDetailsContent: '',
      loading: false,
      mobileNum: "",
      showMessage: false,
      sucessmsg: false,
      smsData: '',
      buttonclick: false,
      emailSalesReceiptShow: false,
      emailTicketShow: false,
      emailVATInvoiceShow: false,
      emailShowBookStepSceenShow: false,
      pricingObject: {},
      ticketUrl: '',
      voucherUrl: '',
      invoiceUrl: '',
      openTxnAvailable: false,
      ticketAvailable: false,
      vatInvoiceApplicatable: false,
      voucherAvailable: false,
      emailInvoiceShow: false,
      receiptAvailableHtl: false,
      openTxnAvailable: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.sendFltSMS = this.sendFltSMS.bind(this)
    this.renderComponents = this.renderComponents.bind(this)
    this.checkIfOpenTxn();
    this.loadTripConditions();
    var domainPath=Domainpath.getHqDomainPath();
    this.state.ticketUrl= domainPath + "/ticket";
    this.state.voucherUrl=domainPath + "/voucher";
    this.state.invoiceUrl=domainPath + "/voucher";
  }

  checkIfOpenTxn = () => {
    this.openTxnAvailable = TripGenericService.checkForOpenTxnAvailable(this.props.tripDetail.txns);
    for (let air_booking_info in this.props.tripDetail.air_bookings[0].air_booking_infos) {
      let air_boookingInfoData = this.props.tripDetail.air_bookings[0].air_booking_infos[air_booking_info];
      if (air_boookingInfoData.ticket_number !== null && air_boookingInfoData.ticket_number !== undefined && air_boookingInfoData.ticket_number !== '') {
        this.ticketAvailable = true;
        break;

      }

    }
    let country_code = TripGenericService.getDomain(this.props.tripDetail.domain);
    if (country_code !== null && country_code !== undefined && country_code !== '') {
      if (country_code === 'ae' || country_code === 'bh' || country_code === 'bh' || country_code === 'sa') {
        this.vatInvoiceApplicatable = true;
      }

    }


  }

  loadTripConditions = () => {
    this.openTxnAvailable = TripGenericService.checkForOpenTxnAvailable(this.props.tripDetail.txns);
    let voucherNumber = this.props.tripDetail.hotel_bookings[0].voucher_number;
    if (voucherNumber !== null && voucherNumber !== undefined && voucherNumber !== '') {
      this.voucherAvailable = true;
    }
    for (let invoices in this.props.tripDetail.invoices) {
      let invoiceData = this.props.tripDetail.invoices[invoices];
      if (invoiceData.category !== null && invoiceData.category !== undefined && invoiceData.category !== '') {
        if (invoiceData.category === 'PaymentReceipt') {
          this.receiptAvailableHtl = true;
          break;
        }
      }
    }
  }
  redirect(event) {

    try {
      event.preventDefault();
      let url = "";
      var domainpath = Domainpath.getHqDomainPath();

      if (event.target.id === 'printinvoice') {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/invoice"
      }
      else if (event.target.id === 'Printhotelinvoice') {
        url = domainpath + "/hq/trips/hotel_invoice/" + this.props.tripDetail.trip_ref
      }
      else if (event.target.id === 'lossTracker') {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/loss_tracker/"
      }
      else if (event.target.id === 'bookInsurance') {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/book_insurance/"
      }
      else if (event.target.id === 'bookstepScreen') {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/screenshots"
        //window.location.replace(url);
      } else if (event.target.id === 'xml') {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/xml"
      }

      window.open(
        url
      );
    } catch (err) {

      log.error('Exception occured in Footer redirect function---' + err);

    }

  }
  handleChange(event) {
    this.setState(
      {
        [event.target.name]
          : event.target.value
      }
    )
  }

  handleClick() {
    this.setState({
      show: !this.state.show,
    });
    if (this.state.tripDetail.contact_detail.mobile !== null && this.state.tripDetail.contact_detail.mobile !== undefined) {
      this.state.mobileNum = this.state.tripDetail.contact_detail.mobile;
    } else {
      this.state.mobileNum = this.state.tripDetail.contact_detail.mobile_number;
    }
  }

  renderComponents(e) {
    this.currentClick = e.target.id;

    if (this.currentClick === 'fphemailDtls') {
      this.setState({
        emailDetailsShow: !this.state.emailDetailsShow,
      });
    } else if (this.currentClick === 'emailInvoice') {
      this.setState({
        emailInvoiceShow: !this.state.emailInvoiceShow,
      });

    }

  }

  sendFltSMS(event) {
    try {
      if (this.state.mobileNum !== "") {
        this.state.buttonclick = true;
        //console.log('Entered Mobile number :' + this.state.mobileNum);
        this.forceUpdate();
        this.setState({ loading: true });
      } else {
        this.setState({ showMessage: true });
        this.setState({ loading: false });
      }
    } catch (err) {
      this.setState({ loading: false });
      log.error('Exception occured in Send SMS function---' + err);
    }
  }
 routePage=(page)=>{
    switch(page){
      case 'printEticket':
        return <FPHPrintEticket />
  
      
    }
 }
  render() {

    let tripJsonUrl = contextPath + "/trips/" + this.state.tripDetail.trip_ref + "/json"

    return (
      <>
        {/* {this.state.buttonclick && (
            <FlightSMS  getMobileNumber={this.state.mobileNum} />
          )} */}

        <div className="container">

          {this.state.showMessage && <div className="alert alert-error"><Icon className="noticicon" color="#F4675F" size={16} icon="warning" />Enter mobile number</div>}
        </div>



        <div className="side-pnl">
          <ShowHide visible="true" title="Tips, tools & extras">
            <div className="showHide-content">
              <ul className="mb-20">

                {/* Email Trip Details */}
                {!this.openTxnAvailable && this.props.tripDetail.air_bookings[0].booking_status === 'P' && this.ticketAvailable &&
                  <li>
                    <a id="fphemailDtls" onClick={this.renderComponents} className={this.currentClick === "fphemailDtls" ? "active" : ""}>
                      <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow" />Email Trip Details</a>
                    <ToggleDisplay show={this.state.emailDetailsShow}>
                      {/* <FPHEmailDetails/> */}
                      <FlightEmailDetails />
                    </ToggleDisplay>
                  </li>
                }

                {/* SMS Trip Details */}
                {/* <li>
        <Link onClick={ () => this.handleClick() }> <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/> SMS trip details</Link>
        <ToggleDisplay className="test123s" show={this.state.show}>
        <div className="row">
        <div className="col-9 form-group">
        <input type="text"  name="mobileNum" value={this.state.mobileNum} onChange={this.handleChange} />
        </div>	
        <div className="col-3 pl-0">
        <Button
        size="xs"
        viewType="primary"                      
        onClickBtn={this.sendFltSMS}
        loading = {this.state.loading}
        btnTitle="Send"
        />
        </div>
        </div>
        </ToggleDisplay>
        </li> */}
                
                {!this.openTxnAvailable &&
                  <li>
                    <a id="emailInvoice" onClick={this.renderComponents} className={this.currentClick === "emailInvoice" ? "active" : ""}>
                      <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow" />Email invoice</a>
                    <ToggleDisplay show={this.state.emailInvoiceShow}>
                      <FPHEmailInvoice />
                    </ToggleDisplay>

                  </li>
                }
                {!this.openTxnAvailable && this.ticketAvailable && this.props.tripDetail.air_bookings[0].booking_status === 'P' &&
                <> 
             
                   <Router>
                    <li>                     
                    <Link to={`/cxportal/trips/${this.state.tripid}/eTicket`} target="_blank"> <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Print E-Ticket</Link>
                    </li>
                    <li>                   
                    <Link to={`/cxportal/trips/${this.state.tripid}/voucher`} target="_blank"> <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Print Vocher</Link>
                    </li>    
                 </Router>
                 </>
                }
                {!this.openTxnAvailable && this.state.voucherAvailable && this.props.tripDetail.hotel_bookings[0].booking_status === 'P' &&
                  <li>

                    <form id="eticketForm" target="_blank" action={this.state.voucherUrl} method="post">
                      <input type="hidden" name="confirmation_number" value={this.props.tripDetail.trip_ref} />
                      <input type="hidden" name="last_name" value={this.props.tripDetail.contact_detail.last_name} />
                      <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />
                      <input className="btn btn-text link-Color no-text-decr" type="submit" value=" &nbsp;Print voucher" />
                    </form>
                  </li>
                }
                {!this.openTxnAvailable &&
                  <li><a id="printinvoice" href="/" onClick={this.redirect} title="Print invoice for this trip">
                    <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Print invoice</a></li>
                }

                {/* Print hotel invoice */}
                {!this.openTxnAvailable &&
                  <li><a id="Printhotelinvoice" href="/" onClick={this.redirect} title="Print hotel invoice">
                    <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Print hotel invoice</a></li>
                }


                {/* Loss Tracker For trip */}
                <li><a id="lossTracker" href="/loss_tracker" target="_blank" onClick={this.redirect} title=" Loss Tracker For trip">
                  <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Loss Tracker For trip</a></li>


                {/* Book Insurance */}
                <li><a id="bookInsurance" href="/book_insurance" target="_blank" onClick={this.redirect} title="Book Insurance">
                  <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Book Insurance</a></li>

                {/* Bookstep Screenshots */}
                <li><a id="bookstepScreen" href="/" onClick={this.redirect} title="Bookstep Screenshots">
                  <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Bookstep Screenshots</a></li>


                <li><a id="xml" href="/xml_tracker" target="_blank" onClick={this.redirect} title="Trip XML">
                  {/* Trip XML */}
                  <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" title="Trip XML" />Trip XML</a></li>


                {/* Cancel Air/Hotel  */}
                <li><a id="CancelAirandHotel" href="/" onClick={this.redirect} title="Cancel Air/Hotel">
                  <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Cancel Air/Hotel</a></li>



                {/* Trip JSON */}
                <li>
                  <a id="json" href={tripJsonUrl} target="_blank" title="Trip JSON">
                    <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow" />Trip JSON</a>
                </li>



              </ul>
              {/* Tags */}


              {/* Change Booking status Closes Txn */}
              <>
                <ChangeBkgStatusClsTxn tripId={this.state.tripDetail.trip_ref} />
              </>

            </div>
          </ShowHide>

          <Tags />
        </div>

        {/* {this.props.tripDetail.air_bookings[0].booking_status==='P'? <div className="side-pnl amndmnt">
            
               <h5 className="show-tg-line mb-10">Amendment </h5>
               <ShowHide>
               <ul>
               <li><a id="onlineamendmnt" href="/reschedule/intro" target="_blank" onClick={this.redirect} title="Online Amendment"><Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Online Amendment</a></li>
               <li><a id="amendmnt"  href="/amend_air" target="_blank" onClick={this.redirect} title="Offline Amend"><Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Offline Amend</a></li>
               </ul>
               </ShowHide>
                    </div>: ''} */}


        {/* <Tags/> */}



      </>

    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
}
export default connect(mapStateToProps, null)(FPHRightSidebar);