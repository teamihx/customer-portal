import React, { Component } from 'react'
import { connect } from 'react-redux';
import EmailService from '../../../services/trips/email/EmailService';
import Notification from 'Components/Notification.jsx';
import Button from 'Components/buttons/button.jsx'
export const contextPath=process.env.domainpath;

class FPHEmailInvoice extends Component {

    constructor(props) {
        super(props)
        this.state = {
            responseData1: this.props.tripDetail,
			email: this.props.tripDetail.contact_detail.email,
			showSucessMessage:false,
            showFailMessage:false,
 			message:false,
			loading:false,
			errorMsgList:[],
			showInputMessage:false,
			
			
          
        }
 		
        //console.log('constructor');
		this.sendFPHEmailInvoice = this.sendFPHEmailInvoice.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.getMessage = this.getMessage.bind(this);
		this.validateEmails = this.validateEmails.bind(this);				
    }

   
	
	handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

	sendFPHEmailInvoice = () => {
		try{   
		this.setState({sendTicket:true}); 
		if(this.validateEmails()){
		let email = new Object();		
		email.to=this.state.email;
		email.tripRefNumber=this.state.responseData1.trip_ref;
		email.type='FPH_EMAIL_INVOICE';
		email.data=this.state.responseData1.air_bookings[0].pax_infos[0].last_name;		
				
		EmailService.sendEmails(email).then((valid) => {
	    if (valid) {
		// console.log('send Emails : '+JSON.stringify(valid));
		if(valid !==undefined && valid !==''){
			let response = valid;
			//console.log('description: '+response.description);
			if(response.description.includes('success')){
				this.setState({showSucessMessage: true, sendTicket:false});
				
			}else{
				this.setState({showFailMessage: true, sendTicket:false});
				
			}
			
		}
	
		}
	 	
		}).catch((err)=>{});
		}     	
		
		}catch(err){
			this.setState({sendTicket:false});    
		}   
    }
	
	getMessage(type){
		let msg ='';
		if(type === 'success'){
			msg = 'We have sent Invoice in an email to '+this.state.email;
		}else if(type === 'error'){
			msg = 'We are not able to send a mail with Invoice  details. You can try again at some other time ';
		}
		return msg;
	}
	
	validateEmails(){
		let isValid = true;
		if(this.state.email === null || this.state.email.trim() === ''){
	        this.state.errorMsgList.push('Email must be provided')
	        isValid= false;
			this.setState({showInputMessage: true, sendTicket:false});
	     }
		 if(this.state.email != null && this.state.email.trim() !== ''){
			const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
			isValid =  expression.test(String(this.state.email).toLowerCase());
			if(!isValid){
			this.state.errorMsgList.push('Enter a valid email address');
			this.setState({showInputMessage: true, sendTicket:false}); 		
			}
			
		}
		//console.log('valid email---'+isValid);
		return isValid;
	
	}
	
    render() {
        
        return (
            <>
            <div>
			{this.state.showInputMessage && (
                <Notification viewType='error' messageText= {this.state.errorMsgList}/>
             )}
			 {this.state.showFailMessage && (
                <Notification viewType='error' messageText= {this.getMessage('error')}/>
             )}
             {this.state.showSucessMessage && (
                 <Notification viewType='success' messageText= {this.getMessage('success')}/>
             )}
            <div className="form-group">
			  <div className="row">
			  <div className="col-9">
				 	<input type="text" name="email" value={this.state.email} onChange={this.handleChange} />
					 </div>
					 <div className="col-3 pl-0">				  		
					  <Button 
						size="xs"
						viewType="primary"
						onClickBtn={this.sendFPHEmailInvoice}
						loading = {this.state.sendTicket}
						btnTitle='Send'> 
						</Button>
				 </div>
                   </div>

			</div>

            </div>
           
</>

        )
    }


}

const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,       
        
        
       
     };
}

export default connect(mapStateToProps, null)(FPHEmailInvoice);



