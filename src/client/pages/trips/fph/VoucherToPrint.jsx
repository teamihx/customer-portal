import React from "react";
import logo from "Assets/images/logo.gif";
import ReactToPrint from "react-to-print";
import dateUtil from "../../../services/commonUtils/DateUtils";
import FlightService from "../../../services/trips/flight/FlightService";
import { connect } from "react-redux";
import converter from "number-to-words";

class VoucherToPrint extends React.Component {
  constructor(props) {
    super(props);
    //var data = require('../../../assets/json/data.json');
    this.state = {
      fltItinerary: {}
    };
  }

  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  render() {
    //console.log("this.state.fltItinerary==" + JSON.stringify(this.props.data));
    return (
      <>
        <div className="print-layout">
          <p className="print-info">Ticket Printer | Cleartrip</p>
          <table
            className="t-color1 logo"
            width="100%"
            cellPadding="0"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <td width="50%" valign="bottom">
                  <strong className="font-22">
                    Voucher
                  </strong>
                </td>
                <td
                  className="ct-logo"
                  width="50%"
                  valign="middle"
                  align="right"
                >
                  <img
                    src={logo}
                    className="ct-logo"
                    title="Cleartrip Boportal"
                    alt="logo"
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <div className="border6"></div>
          <table className="pb-10">
            <tbody>
            <tr>
              <td>
              <strong className="font-22 t-color1">Hotel Celebrations Inn</strong>
              <span className="d-b mt-5">Maldhakka Road, Old Pune Mumbai Highway, Chinchwad Station, Chinchwad , Pune, 411 005</span>
             <span>Host: support@tripvillas.com</span>
              </td>
            </tr>
            </tbody>
          </table>
          <table
            className="border t-color1"
            width="100%"
            cellPadding="0"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <td valign="middle">
                  <strong>Voucher Number: CHMM-8528882</strong>
                  <span> (Issued on 30 Dec, 2019)</span>
                </td>
                <td valign="middle" align="right">
                  <strong>Cleartrip Trip ID: Q191230678978</strong>
                </td>
              </tr>
            </tbody>
          </table>
          <table width="100%" cellPadding="0" cellSpacing="0" border="0">
            <tbody>
              <tr>
                <td width="15%" valign="top">
                  <table
                    width="100%"
                    cellPadding="0"
                    cellSpacing="0"
                    border="0"
                  >
                    <tbody>
                      <tr>
                        <td width="140px">
                          {" "}
                          <strong className="t-color1">
                            Whole property
                          </strong>{" "}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td width="50%" valign="top">
                  <table cellPadding="0" cellSpacing="0" border="0">
                    <tbody>
                      <tr>
                        <td width="40%" valign="middle" align="right">
                          CHECK-IN
                        </td>
                        <td valign="middle" align="center"></td>
                        <td width="40%" valign="middle" align="left">
                          CHECK-OUT
                        </td>
                      </tr>
                      <tr>
                        <td
                          width="40%"
                          className="t-color1 font-22"
                          valign="middle"
                          align="right"
                        >
                          <strong>DEC 31</strong>
                        </td>
                        <td valign="middle" align="center">
                          <img
                            width="30"
                            src="https://qa2.cltpstatic.com/images/icons/landing.png"
                          />
                        </td>
                        <td
                          width="40%"
                          className="t-color1 font-22"
                          valign="middle"
                          align="left"
                        >
                          <strong>02 JAN</strong>
                        </td>
                      </tr>
                      <tr>
                        <td width="40%" valign="top" align="right">
                          24 hrs
                        </td>
                        <td valign="top" align="center">
                          2 Nights
                        </td>
                        <td width="40%" valign="top" align="left">
                          24 hrs
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td width="30%"></td>
              </tr>
            </tbody>
          </table>
          <table
            width="100%"
            className="travel-table"
            cellPadding="5"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <th align="left" nowrap="nowrap">
                  Guest name
                </th>
                <th className="pl-10" align="left">
                  No. of guests
                </th>
              </tr>

              <tr>
                <td>
                  <table className="no-border">
                  <tbody>
                    <tr>
                      <td width="20">
                        <img
                          src="https://qa2.cltpstatic.com/images/icons/women.png"
                          width="14"
                        />
                      </td>
                      <td width="90%">
                        {" "}
                        <strong className="t-color1">Ms. kirti pandey</strong>
                        <span className="font-10 d-b mt-5">
                          varalakshmivaru29@gmail.com
                        </span>
                        <span className="font-10">9663949690</span>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
                <td className="pl-10">2 adults</td>
              </tr>
            </tbody>
          </table>
          <table
            className="t-c"
            width="100%"
            cellPadding="0"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <td valign="top">
                  <strong className="t-color1">Additional Information</strong>
                  <ul>
                    <li>
                      This voucher serves as confirmation of your booking at the
                      property and has been made on behalf of the owner.
                    </li>
                    <li>
                      Use your booking ID for all communication with Tripvillas
                      about this booking.
                    </li>
                    <li>
                      Carry a print out of this voucher and present it to the
                      owner’s staff at the time of check-in or when demanded
                      for.
                    </li>
                    <li>
                      The amount paid has been held in escrow by Tripvillas. The
                      owner will receive full payment from us automatically 48
                      hours post your check-in time unless you report trouble.
                    </li>
                    <li>
                      We advise to carry photo identification, as most owners
                      will require to see one while checking in.
                    </li>
                  </ul>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <table className="price" width="100%">
                    <tbody>
                      <tr>
                        <td width="50%">
                          <strong className="t-color1">Inclusions</strong>
                        </td>
                        <td width="50%">
                          <strong className="t-color1">FARE BREAKUP</strong>
                        </td>
                      </tr>
                      <tr>
                        <td className="border-none">
                          <div>
                            <span>
                              <img
                                src="https://qa2.cltpstatic.com/images/icons/tick-circle.png"
                                height="12"
                                hspace="5"
                              />
                              Free Wi-Fi &nbsp;
                            </span>
                            <span>
                              <img
                                src="https://qa2.cltpstatic.com/images/icons/tick-circle.png"
                                height="12"
                                hspace="5"
                              />
                              All Applicable Taxes &nbsp;
                            </span>
                          </div>
                        </td>
                        <td className="border-none">
                          <ul>
                            <li>
                              <span>Base fare: </span>
                              <span>Rs. 3,986 </span>
                            </li>
                            <li>
                              <span>Taxes and fees: </span>
                              <span>Rs. 12,282</span>
                            </li>
                            <li>
                              <span>Total fare: </span>
                              <span>Rs. 16,268</span>
                            </li>
                          </ul>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

          <table
            className="support"
            cellPadding="0"
            cellSpacing="5"
            border="0"
            width="100%"
          >
            <tbody>
            <tr>
              <td>
                <table cellPadding="0" cellSpacing="5" border="0" width="100%">
                <tbody>
                  <tr>
                    <td>
                      <strong>Amenities</strong> (Cleartrip assured amenities by
                      the hotel)
                    </td>
                  </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table>
                <tbody>
                  <tr>
                    <td align="right" width="30" valign="top">
                      <img
                        src="https://ui.cltpstatic.com/images/icons/helpline.gif"
                        width="20"
                      />
                    </td>
                    <td>
                      <strong className="d-b">Cleartrip support</strong>
                     (+91) 95 95 333 333{" "}
                    </td>

                    <td align="right" width="30" valign="top">
                      <img
                        src="https://qa2.cltpstatic.com/images/icons/email.png"
                        width="20"
                      />
                    </td>
                    <td>
                    <strong className="d-b">Cleartrip support</strong>
                      support@cleartrip.com
                    </td>
                    <td align="right" width="30" valign="top">
                      <img
                        src="https://ui.cltpstatic.com/images/icons/helpline.gif"
                        width="20"
                      />
                    </td>
                    <td>
                    <strong className="d-b">Hotel Celebrations Inn </strong>
                      1234567890
                    </td>
                    <td></td>
                    <td></td>
                  </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            </tbody>
          </table>
          <table
            className="border support mt-10"
            cellPadding="0"
            cellSpacing="5"
            border="0"
            width="100%"
          >
            <tbody>
            <tr>
              <td className="pt-15">
                <strong className="mb-5 d-b">Specific Rules For Deluxe AC Room Only</strong>
                If you cancel within 24 hours before checkin, you will be
                charged 1 room night charges per room.
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default VoucherToPrint;
