import React from 'react';
import ReactToPrint from 'react-to-print';
import dateUtil from '../../../services/commonUtils/DateUtils';
import FlightService from '../../../services/trips/flight/FlightService';
import { connect } from "react-redux";
import converter from 'number-to-words';
import Button from 'Components/buttons/button.jsx'
import { BrowserRouter as Router, Link, Route,Switch } from "react-router-dom";
import FPHPrintEticket from './FPHPrintEticket';
 
class OtherTripSearch extends React.Component {
    constructor(props) {
        super(props)
        //var data = require('../../../assets/json/data.json');
        this.state = {
           tripId:'',
           newTrip:false
        }
        
    }
 
    handleChange = evt => {
        evt.persist();
        this.setState({ [evt.target.name]: evt.target.value }, () =>
          console.log(this.state.tripId)
        );
      };
      routeChange=()=> {
        
        let url ="/cxportal/trips/"+this.state.tripId+"/eTicket"
        console.log("url==="+url)
        this.props.history.push(url);
      }
      newTrip =()=>{
        console.log("newTrip==="+this.state.newTrip)
        this.setState({newTrip:true},()=>console.log("newTrip==="+this.state.newTrip))
    }
  render() {
      
    return (
        <>
       <div>See and print your etickets</div>
       <div className="col-1">
       <div>Trip Id</div>
                <input
                  id="curId"
                  type="text"
                  name="tripId"
                  value={this.state.tripId}
                  onChange={this.handleChange}
                />
              </div>
              
              <Link to={`/cxportal/trips/${this.state.tripId}/eTicket` } >Find another trip!</Link>
              
              
              
              <Switch>
             <Route exact path={`/cxportal/trips/${this.state.tripId}/eTicket`}  component={FPHPrintEticket}>
         
        </Route>
       
      </Switch>
              
              </>
       );
  }
}
      
    export default OtherTripSearch