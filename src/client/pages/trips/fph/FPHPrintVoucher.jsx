import React, { Component } from "react";
import { connect } from "react-redux";
import GenericAddonTemp from "Pages/common/GenericAddonTemp";
import ShowHide from 'Components/ShowHide.jsx';
import FlightService from "../../../services/trips/flight/FlightService";
import EmailService from "../../../services/trips/email/EmailService";
import Notification from "Components/Notification.jsx";
import Button from "Components/buttons/button.jsx";
export const contextPath = process.env.domainpath;
import * as actions from "../../../store/actions/index";
import VoucherToPrint from "./VoucherToPrint";
import ReactToPrint from "react-to-print";
import { Link } from "react-router-dom";
import OtherTripSearch from "./OtherTripSearch";
import 'Assets/styles/_print.scss'; 
class FPHPrintVoucher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tripId: window.location.href.split("/")[
        window.location.href.split("/").length - 2
      ],
      newTrip: false
    };

    this.req = {
      tripId: this.state.tripId
    };
  }
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  loadTripDetails = () => {
    let air_bookings = new Object();
    if (!this.isEmpty(this.props.data)) {
      //console.log("trip dtl=="+JSON.stringify(this.props.tripDetail))

      // this.props.tripDetail.insurances.length>0?this.state.insuranceAvailable=true:null
      air_bookings = { ...this.props.data.air_bookings[0] };
      //air_bookings.itinerary_id=this.props.tripDetail.itinerary_id;
      //air_bookings.amend_itinerary_id=this.props.tripDetail.amend_itinerary_id;
      air_bookings.display_current_itinerary = this.state.showCurrentItinerary
        ? true
        : false;
      //air_bookings.is_amended=typeof this.props.tripDetail.air_bookings[0].amend_itinerary_id!=='undefined'&& air_bookings.amend_itinerary_id!==null?true:false
      if (this.props.data.currency === "INR") {
        air_bookings.currency = "Rs";
      } else {
        air_bookings.currency = this.props.data.currency;
      }

      //this.state.fltItinerary={...air_bookings}

      for (let info of air_bookings.air_booking_infos) {
        for (let flight of air_bookings.flights) {
          flight.itinerary_id = air_bookings.itinerary_id;
          flight.air_booking_type = air_bookings.air_booking_type;
          for (let seg of flight.segments) {
            if (seg.id === info.segment_id) {
              seg.cabin_type = info.cabin_type;
            }
          }
        }
      }
      for (let flt of air_bookings.flights) {
        if (flt.segments.length > 1) {
          for (let seg of flt.segments) {
            let last_index = flt.segments.length - 1;
            if (flt.segments.indexOf(seg) !== last_index) {
              let nextSeg = flt.segments[flt.segments.indexOf(seg) + 1];
              seg.lay_over = Math.abs(
                new Date(nextSeg.departure_date_time) -
                  new Date(seg.arrival_date_time)
              );
            }
          }
        }
      }
      //console.log("fltItinerary===="+JSON.stringify(air_bookings.flights))

      air_bookings.travellers = this.props.data.travellers;
      this.setState({ fltItinerary: { ...air_bookings } }, () =>
        console.log("fltItinerary==" + JSON.stringify(this.state.fltItinerary))
      );

      this.props.tripDetail.air_bookings = { ...air_bookings };
    }
  };

  componentDidMount() {
    this.props.onInitTripDtl(this.req);
  }
  newTrip = () => {
    console.log("newTrip===" + this.state.newTrip);
    this.setState({ newTrip: true }, () =>
      console.log("newTrip===" + this.state.newTrip)
    );
  };
  render() {
    const printMe = ( <ReactToPrint
      trigger={() => (
        <Link to="#" onClick={this.loadTripDetails}>
          Print this out!
        </Link>
      )}
      content={() => this.componentRef}
    />)
    return !this.isEmpty(this.props.tripDetail) ? (
      <>
      <GenericAddonTemp sideHeaderComponent = {printMe} name="Voucher">
      <section>
        <div className="main-container pb-10 bg-c-gr">
          <div className="row">
          <div className="col-9 bg-white pb-15"> 
              
              <div className="print voucher">
                <VoucherToPrint
                  data={this.props.tripDetail}
                  ref={el => (this.componentRef = el)}
                />
              </div>
            </div>
            {/* <div className="col-3"> 
            <div className="side-pnl">
          <ShowHide visible="true" title="Ticket tools">
            <div className="showHide-content">
           
                </div>
                </ShowHide>
                </div>
            </div> */}
          </div>
        </div>
      </section>
</GenericAddonTemp>
      </>
    ) : null;
  }
}
const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airLineMaster: state.trpReducer.airLineMaster,
    airPortMaster: state.trpReducer.airPortMaster,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster,
    usermasterdetails: state.trpReducer.usermasterdetails
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onInitTripDtl: req => dispatch(actions.initTripDtl(req))

    //onInitAirLineMaster:(req)=>dispatch(actions.initAirLineMasterData(req)),
    //onInitAirPortMaster:(req)=>dispatch(actions.initAirPortMasterData(req))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FPHPrintVoucher);
