import React, { Component } from 'react'
import { connect } from 'react-redux';
import FlightService from '../../../services/trips/flight/FlightService';
import EmailService from '../../../services/trips/email/EmailService';
import Notification from 'Components/Notification.jsx';
import Button from 'Components/buttons/button.jsx'
export const contextPath=process.env.domainpath;

class FPHEmailDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
            responseData1: this.props.tripDetail,
			email: this.props.tripDetail.contact_detail.email,
			showSucessMessage:false,
            showFailMessage:false,
			showInputMessage:false,
 			message:false,
			loading:false,
			errorMsgList:[],
			iCalendar :true
          
        }
 		
       // console.log('constructor');
		this.sendFlightDetailsEmail = this.sendFlightDetailsEmail.bind(this);
		this.sendFlightDetailsEmailNew = this.sendFlightDetailsEmailNew.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.getMessage = this.getMessage.bind(this);
		this.validateEmails = this.validateEmails.bind(this);
		this.handleChangeIcalender = this.handleChangeIcalender.bind(this);
				
    }

  
	
	handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
		if(e.target.id === 'icalInput'){
			if(this.state.iCalendar){
					this.state.iCalendar = false;
				}else{
					this.state.iCalendar = true;
				}
		}
		
		
 		//console.log('icalender: '+this.state.iCalendar);
		
    }

	


	sendFlightDetailsEmailNew = () => {
		try{
			this.setState({sendTrip:true}); 			
			if(this.validateEmails()){
			let email = new Object();		
			email.to=this.state.email;
			email.tripRefNumber=this.state.responseData1.trip_ref;		
			email.type='FLIGHT_DTLS';
			email.ical=this.state.iCalendar;			
			EmailService.sendEmails(email).then((valid) => {
				if (valid) {
				//	console.log('send Emails : '+JSON.stringify(valid));
					if(valid !==undefined && valid !==''){
						let response = valid;
						//console.log('description: '+response.description);
						if(response.description.includes('success')){
							this.setState({showSucessMessage: true, sendTrip:false});							
						}else{
							this.setState({showFailMessage: true, sendTrip:false});			
						}			
					}		
				}			
			});
			}         	
			
		}catch(err){
			this.setState({sendTrip:false});    
		}
	}

 	sendFlightDetailsEmail = () => {
	
     	let flightJson =    FlightService.constructEmailJson(this.state.responseData1,this.props.airPortMaster,this.props.airLineMaster);
		let email = new Object();
		let toList =[];
		email.from = "no-reply@cleartrip.com";
		let to = this.state.email;
		toList.push(to);
		let size = flightJson.trip.air_bookings[0].flights.length-1;
		let lastsegment = flightJson.trip.air_bookings[0].flights[size].arrival_airportName;
		let firstsegment = flightJson.trip.air_bookings[0].flights[0].departure_airportName;
		email.subject = "[Trip Details] "+firstsegment+"-"+lastsegment+"(Trip ID: "+flightJson.trip.tripRef +")";
		email.mailContent = JSON.stringify(flightJson);
		email.useTemplate = "true";
		email.category = "bo-flights-details";
		email.to=toList;
		email.tripRefNumber=this.state.responseData1.trip_ref;
		//console.log('email json: '+JSON.stringify(email));
		
		EmailService.sendFightDetailEmail(email);
		

    }

	
	getMessage(type){
		let msg ='';
		if(type === 'success'){
			msg = 'We have sent the itinerary details in an email to '+this.state.email;;
		}else if(type === 'error'){
			msg = 'We are not able to send a mail with itinerary details. You can try again at some other time ';
		}
		return msg;
	}
	
	validateEmails(){
		let isValid = true;
		if(this.state.email === null || this.state.email.trim() === ''){
	        this.state.errorMsgList.push('Email must be provided')
	        isValid= false;
			this.setState({showInputMessage: true, sendTrip:false});
	     }
		 if(this.state.email != null && this.state.email.trim() !== ''){
			const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
			isValid =  expression.test(String(this.state.email).toLowerCase());
			if(!isValid){
			this.state.errorMsgList.push('Enter a valid email address');
			this.setState({showInputMessage: true, sendTrip:false});
			}
			 		
		}
		//console.log('valid email---'+isValid);
		return isValid;
	
	}
	
    render() {
        
        return (
            <>
            <div>
			{this.state.showInputMessage && (
                <Notification viewType='error' messageText= {this.state.errorMsgList}/>
             )}
			 {this.state.showFailMessage && (
                <Notification viewType='error' messageText= {this.getMessage('error')}/>
             )}
             {this.state.showSucessMessage && (
                <Notification viewType='success' messageText= {this.getMessage('success')}/>
             )}
            <div className="form-group">
			  <div className="row">
              <div className="col-12">
                <input
                  type="text"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
			  <div className="dis-flx-btw-ned w-100p">
              <div>
                <input
                  id="icalInput"
                  type="checkbox"
                  className="customCheckbox"
                  defaultChecked={this.state.iCalendar}
                  value={this.state.iCalendar}
                  onChange={this.handleChange}
                />
                <label htmlFor="icalInput">Send iCalendar</label>
                {/* <button type="button" onClick={this.sendFlightDetailsEmailNew} className="btn btn-xs btn-primary"><b>Send</b></button>
                 */}
              </div>
              <div>
                <Button
                  size="xs"
                  viewType="primary"
                  onClickBtn={this.sendFlightDetailsEmailNew}
                  loading={this.state.sendTrip}
                  btnTitle="Send"
                ></Button>
              </div>
			  </div>
            </div>

			</div>

            </div>
           
</>

        )
    }


}

const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,       
        airPortMaster: state.trpReducer.airPortMaster ,
		airLineMaster: state.trpReducer.airLineMaster,
        
       
     };
}

export default connect(mapStateToProps, null)(FPHEmailDetails);



