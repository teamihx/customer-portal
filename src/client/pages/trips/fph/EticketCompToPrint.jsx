import React from "react";
import ReactToPrint from "react-to-print";
import logo from "Assets/images/logo.gif";
import dateUtil from "../../../services/commonUtils/DateUtils";
import FlightService from "../../../services/trips/flight/FlightService";
import { connect } from "react-redux";
import converter from "number-to-words";

class EticketCompToPrint extends React.Component {
  constructor(props) {
    super(props);
    //var data = require('../../../assets/json/data.json');
    this.state = {
      fltItinerary: {}
    };
  }

  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  render() {
   // console.log("this.state.fltItinerary==" + JSON.stringify(this.props.data));
    return (
      <>
        <div className="print-layout">
          <p className="print-info">Ticket Printer | Cleartrip</p>
          <table
            className="t-color1 logo"
            width="100%"
            cellPadding="0"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <td width="50%" valign="bottom">
                  <strong>
                    Ticket <span> Trip ID: Q191223654274</span>
                  </strong>
                </td>
                <td
                  className="ct-logo"
                  width="50%"
                  valign="middle"
                  align="right"
                >
                  <img
                    src={logo}
                    className="ct-logo"
                    title="Cleartrip Boportal"
                    alt="logo"
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <table
            className="border t-color1"
            width="100%"
            cellPadding="0"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <td valign="middle">
                  <strong>Riyadh to Dubai</strong>
                  <span> Fri, 14 Feb 2020</span>
                </td>
                <td valign="middle"></td>
              </tr>
            </tbody>
          </table>
          <table width="100%" cellPadding="0" cellSpacing="0" border="0">
            <tbody>
              <tr>
                <td width="15%" valign="top">
                  <table
                    width="100%"
                    cellPadding="0"
                    cellSpacing="0"
                    border="0"
                  >
                    <tbody>
                      <tr>
                        <td width="140px">
                          <strong className="t-color1">flydubai</strong>{" "}
                        </td>
                      </tr>
                      <tr>
                        <td> FZ-848 </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td width="80%" valign="top">
                  <table cellPadding="0" cellSpacing="0" border="0">
                    <tbody>
                      <tr>
                        <td
                          className="t-color1 font-16"
                          width="40%"
                          valign="middle"
                          align="right"
                        >
                          RUH <strong>20:35</strong>
                        </td>
                        <td valign="middle" align="center">
                          <img
                            width="16"
                            src="https://qa2.cltpstatic.com/images/icons/landing.png"
                          />
                        </td>
                        <td
                          width="40%"
                          className="t-color1 font-16"
                          valign="middle"
                          align="left"
                        >
                          <strong>23:25</strong> DXB
                        </td>
                      </tr>
                      <tr>
                        <td width="40%" valign="middle" align="right">
                          Fri, 14 Feb 2020
                        </td>
                        <td valign="middle" align="center">
                          1h 50m
                        </td>
                        <td width="40%" valign="middle" align="left">
                          Fri, 14 Feb 2020
                        </td>
                      </tr>
                      <tr>
                        <td width="40%" valign="top" align="right">
                          Riyadh - King Khaled
                        </td>
                        <td valign="top" align="center">
                          Economy
                        </td>
                        <td width="40%" valign="top" align="left">
                          Dubai - Dubai International Airport
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colSpan="2" valign="top">
                  <table
                    className="baggage"
                    width="100%"
                    cellPadding="0"
                    cellSpacing="0"
                    border="0"
                  >
                    <tbody>
                      <tr>
                        <td className="font-10">
                          <span>Baggage (per Adult/Child) &ndash; </span>
                          <span>Check-in: 20 KG, Cabin: 7 KG </span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table
            className="border t-color1"
            cellPadding="0"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <td valign="middle" colSpan="12">
                  <strong>Sharjah to Riyadh</strong>
                  <span> Mon, 17 Feb 2020</span>
                </td>
                <td valign="middle" colSpan="12"></td>
              </tr>
            </tbody>
          </table>
          <table width="100%" cellPadding="0" cellSpacing="0" border="0">
            <tbody>
              <tr>
                <td width="15%" valign="top">
                  <table
                    width="100%"
                    cellPadding="0"
                    cellSpacing="0"
                    border="0"
                  >
                    <tbody>
                      <tr>
                        <td width="140px">
                          <strong className="t-color1">flydubai</strong>
                        </td>
                      </tr>
                      <tr>
                        <td> FZ-848 </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td width="80%" valign="top">
                  <table cellPadding="0" cellSpacing="0" border="0">
                    <tbody>
                      <tr>
                        <td
                          className="t-color1 font-16"
                          width="40%"
                          valign="middle"
                          align="right"
                        >
                          RUH <strong>20:35</strong>
                        </td>
                        <td valign="middle" align="center">
                          <img
                            width="16"
                            src="https://qa2.cltpstatic.com/images/icons/landing.png"
                          />
                        </td>
                        <td
                          width="40%"
                          className="t-color1 font-16"
                          valign="middle"
                          align="left"
                        >
                          <strong>23:25</strong> DXB
                        </td>
                      </tr>
                      <tr>
                        <td width="40%" valign="middle" align="right">
                          Fri, 14 Feb 2020
                        </td>
                        <td valign="middle" align="center">
                          1h 50m
                        </td>
                        <td width="40%" valign="middle" align="left">
                          Fri, 14 Feb 2020
                        </td>
                      </tr>
                      <tr>
                        <td width="40%" valign="top" align="right">
                          Riyadh - King Khaled
                        </td>
                        <td valign="top" align="center">
                          Economy
                        </td>
                        <td width="40%" valign="top" align="left">
                          Dubai - Dubai International Airport
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colSpan="2" valign="top">
                  <table
                    className="baggage"
                    width="100%"
                    cellPadding="0"
                    cellSpacing="0"
                    border="0"
                  >
                    <tbody>
                      <tr>
                        <td className="font-10">
                          <span>Baggage (per Adult/Child) &ndash; </span>
                          <span>Check-in: 20 KG, Cabin: 7 KG </span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table
            width="100%"
            className="travel-table"
            cellPadding="5"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <th align="left" nowrap="nowrap">
                  Travellers
                </th>
                <th className="pl-10" align="left">
                  Airline PNR
                </th>
                <th className="pl-10" align="left">
                  Ticket No.
                </th>
              </tr>

              <tr>
                <td>
                  <table className="no-border">
                    <tbody>
                    <tr>
                      <td width="20">
                        <img
                          src="https://qa2.cltpstatic.com/images/icons/women.png"
                          width="14"
                        />
                      </td>
                      <td width="90%">
                        <strong className="t-color1">Ms. kirti pandey</strong>
                        <span className="font-10 d-b mt-5">
                          Passport no. 3456345676
                        </span>
                        <span className="font-10">
                          FF no. 1231231234 (flydubai), 6543456543 (Air Arabia)
                        </span>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
                <td className="pl-10">1w234, NR5PPZR</td>
                <td className="pl-10">YPLR74, NR5PPZR</td>
              </tr>
            </tbody>
          </table>
          <table
            className="t-c"
            width="100%"
            cellPadding="0"
            cellSpacing="0"
            border="0"
          >
            <tbody>
              <tr>
                <td valign="top">
                  <strong className="t-color1">TIP : </strong>
                  <span>
                    Keep all vital medications with you on board. Ask for your
                    doctor to issue extra prescriptions if the medications are
                    essential to your health.
                  </span>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <strong className="t-color1">IMPORTANT : </strong>
                  <span>
                    Customers traveling to UAE on a visit visa are required to
                    get their visas verified. After verification, the relevant
                    offices will insert an 'Ok to Board' message in the PNR.
                  </span>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <strong className="t-color1">ABOUT THIS TRIP</strong>
                  <ul>
                    <li>
                      Use your Trip ID for all communication with Cleartrip
                      about this booking{" "}
                    </li>
                    <li>
                      Check-in counters for International flights close 90
                      minutes before departure
                    </li>
                    <li>
                      Your carry-on baggage shouldn't weigh more than 7kgs
                    </li>
                    <li>
                      Carry photo identification, you will need it as proof of
                      identity while checking-in
                    </li>
                    <li>
                      Kindly ensure that you have the relevant visa, immigration
                      clearance and travel with a passport, with a validity of
                      at least 6 months.
                    </li>
                    <li>
                      For hassle free refund processing, cancel/amend your
                      tickets with Cleartrip Customer Care instead of doing so
                      directly with Airline.
                    </li>
                  </ul>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <table className="price" width="100%">
                    <tbody>
                      <tr>
                        <td width="140px">
                          <strong className="t-color1">FARE BREAKUP</strong>
                        </td>
                      </tr>
                      <tr>
                        <td className="border-none">
                          <ul>
                            <li>
                              <span>Base fare: </span>
                              <span>Rs. 3,986 </span>
                            </li>
                            <li>
                              <span>Taxes and fees: </span>
                              <span>Rs. 12,282</span>
                            </li>
                            <li>
                              <span>Total fare: </span>
                              <span>Rs. 16,268</span>
                            </li>
                          </ul>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table
            className="border support mt-20"
            cellPadding="0"
            cellSpacing="5"
            border="0"
            width="100%"
          >
            <tbody>
            <tr>
              <td align="right" width="30" valign="top">
                <img
                  src="https://ui.cltpstatic.com/images/icons/helpline.gif"
                  width="20"
                />
              </td>
              <td>
              <strong className="d-b">Cleartrip support</strong>
              (+91) 95 95 333 333
              </td>

              <td align="right" width="30" valign="top">
                <img
                  src="https://ui.cltpstatic.com/images/icons/helpline.gif"
                  width="20"
                />
              </td>
              <td>
              <strong className="d-b">flydubai helpline </strong>
                600 544 445
              </td>
              <td align="right" width="30" valign="top">
                <img
                  src="https://ui.cltpstatic.com/images/icons/helpline.gif"
                  width="20"
                />
              </td>
              <td>
              <strong className="d-b">Air Arabia helpline </strong>
                (022) 71004777
              </td>

              <td align="right" width="30" valign="top">
                <img
                  src="https://ui.cltpstatic.com/images/icons/home.gif"
                  width="20"
                />
              </td>
              <td> 
              <strong className="d-b">Need a hotel?</strong>
               (+91) 95 95 333 333
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default EticketCompToPrint;
