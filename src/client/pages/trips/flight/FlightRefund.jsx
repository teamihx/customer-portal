import React, { Component } from 'react'
import { connect } from 'react-redux';
//import Icon from 'Components/Icon';
import FlightService from '../../../services/trips/flight/FlightService';
import NumberUtils from '../../../services/commonUtils/NumberUtils.js';
import log from 'loglevel';

class FlightRefund extends Component {

    constructor(props) {
        super(props)
        this.state = {
            responseData1: this.props.tripDetail,
			refundInfoList:[],
			refundTxn : '0',
			totalRevarsal :'0',
			totalSup :'0',
			totalCt :'0',
			totalCshBack: '0',
			totalDiscount: '0',
			totalSTax: '0',
			totalCTSTax: '0',			
			totalMiscCharge: '0',
			totalAmdCharge: '0',
			totalOtherCharge: '0',
			totalWtCbCharge: '0',
			totalBundleCharge: '0',
			totalGateWayCharge: '0',
			totalTotalRefund: '0',
          
        }
 		this.calculateTotalRefund = this.calculateTotalRefund.bind(this);
	
        //console.log('constructor');
    }

   UNSAFE_componentWillMount(){
	 //console.log('componentWillMount');
	let txns = this.state.responseData1.txns.filter( function (txns) {
      return txns.air_refund_records.length > 0
    });

	
	this.state.refundTxn = txns;
	this.calculateTotalRefund();
	}
	
	calculateTotalRefund() {		
	this.state.refundInfoList =[];
	try{
		for(let refundTxns of this.state.refundTxn){
		//console.log('sdfsd');
		for(let air_refund_records of refundTxns.air_refund_records){
			//console.log('abc');
		let air_refund_rcd = new Object(); 
		air_refund_rcd = air_refund_records;
			
		let air_cancellationsdata = refundTxns.air_cancellations.filter( function (air_cancellations) {
					      return air_cancellations.air_refund_record_id === air_refund_records.id
					    });
		let bookingInfoId = air_cancellationsdata[0].air_booking_info_id;
		
		let bookInfos = this.state.responseData1.air_bookings[0].air_booking_infos.filter( function (airbookInfo) {
					      return airbookInfo.id === bookingInfoId
					    });
		let segmentId = bookInfos[0].segment_id;
		
		let paxId = bookInfos[0].pax_info_id;
		let pnr = 	bookInfos[0].airline_pnr;
		air_refund_rcd.pnr = pnr;		
		let paxInfos = this.state.responseData1.air_bookings[0].pax_infos.filter( function (paxInfo) {
					      return paxInfo.id === paxId
					    });
		
		let name = paxInfos[0].first_name + ' ' +paxInfos[0].last_name;
		air_refund_rcd.name = name;
		let paxType = '';
		if(paxInfos[0].pax_type_code=='ADT'){
			paxType = 'Adult'
		}else if(paxInfos[0].pax_type_code=='CHD'){
			paxType = 'Child'
		}else if(paxInfos[0].pax_type_code=='INF'){
			paxType = 'Infant'
		}
		air_refund_rcd.paxType = paxType;
		let fltseg ='';	
		let flitAirLineNum ='';
		for(let flts of this.state.responseData1.air_bookings[0].flights){
			 for(let seg of flts.segments){
				flitAirLineNum = flitAirLineNum.concat(seg.marketing_airline+'-'+seg.flight_number+ ' ');								
				if (seg.id === segmentId ){
					fltseg =flts;					
					}
				}
							
		}
		air_refund_rcd.flitAirLineNum =flitAirLineNum;
		air_refund_rcd.segment =fltseg;
		let paymode = this.state.responseData1.payments_service_data[0].payment_type;
		let tnxNumb = '';
		let paymentmode = this.props.paymentTypeMaster[this.state.responseData1.payments_service_data[0].payment_type];
		if(paymode ==='CC'){
			 tnxNumb = this.state.responseData1.payments_service_data[0].payment_card_details[0].card_number;
		}	
		air_refund_rcd.paymentmode =paymentmode;
		air_refund_rcd.tnxNumb =tnxNumb;
			
		this.state.refundInfoList.push(air_refund_rcd);	
		
		this.state.totalRevarsal = Number(this.state.totalRevarsal) + Number(air_refund_records.total_rev);		
		this.state.totalSup = Number(this.state.totalSup) + Number(air_refund_records.total_sup_charge);		
		this.state.totalCt = Number(this.state.totalCt) + Number(air_refund_records.total_ct_charge);
		this.state.totalCshBack = Number(this.state.totalCshBack) + Number(air_refund_records.total_cb);		
		this.state.totalCshBack = Number(this.state.totalCshBack) + Number(air_refund_records.total_cb);
		this.state.totalDiscount = Number(this.state.totalDiscount) + Number(air_refund_records.total_dis);
		this.state.totalSTax = Number(this.state.totalSTax) + Number(air_refund_records.total_stx_charge);		
		this.state.totalMiscCharge = Number(this.state.totalMiscCharge) + Number(air_refund_records.total_misc_charge);
		this.state.totalAmdCharge = Number(this.state.totalAmdCharge) + Number(air_refund_records.total_amd_charge);
		this.state.totalOtherCharge = Number(this.state.totalOtherCharge) + Number(air_refund_records.total_oth_charge);		
		this.state.totalOtherCharge = Number(this.state.totalOtherCharge) + Number(air_refund_records.total_oth_charge);
		this.state.totalWtCbCharge = Number(this.state.totalWtCbCharge) + Number(air_refund_records.total_wt_cb_charge);
		this.state.totalBundleCharge = Number(this.state.totalBundleCharge) + Number(air_refund_records.total_bundle_charge);
		this.state.totalGateWayCharge = Number(this.state.totalGateWayCharge) + Number(air_refund_records.total_gw_charge);
		this.state.totalTotalRefund = Number(this.state.totalTotalRefund) + Number(air_refund_records.refund_amount);
			
		}
		
	}
		
	}catch(err){
    log.error('Exception occured in calculateTotalRefund function---'+err);
  }
	
  	}
    
    render() {
        
        return (
            <>
            <div>
            <h4 className="in-tabTTl-sub mb-10">Refund Computation details</h4>
			<span className="mb-10 d-b font-18">Booking Details</span>
			<div className="overflow-x-auto">
			<table className="dataTblPaddingR pax-tbl dataTable5 scrlTable">
                  <thead><tr>
				<th width="7%">Pax</th>
				<th width="7%">PNR</th>
                <th width="7%">Reversal  (+)</th>
                <th width="7%">Supplier (-)</th>
                <th width="6%">Cleartrip (-)</th>
                <th width="7%">Cashback  (-)</th>
				<th width="7%">Discount (-)</th>
                <th width="6%">S.Tax (-)</th>
                <th width="6%">CT-S.Tax</th>
				<th width="6%">Misc  (-)</th>				
				<th width="6%">Amend (-)</th>
				<th width="6%">Other (-)</th>
				<th width="10%">Wallet Cashback (-)</th>
				<th width="8%">Bundle Fee (-)</th>
				<th width="12%">Gateway  Fee (-) </th>
				<th width="6%">Refund</th>
                   
                  </tr></thead>
                  <tbody>

				{this.state.refundInfoList.map(refundInfos => {					
			
					
			  return(
				<>
				<tr><td colSpan="16">
					<div className="destination ml-0 font-14">{FlightService.getAirPortNameFromAirPorteCode(this.props.airPortMaster,refundInfos.segment.departure_airport)}-{FlightService.getAirPortNameFromAirPorteCode(this.props.airPortMaster,refundInfos.segment.arrival_airport)} fare on {refundInfos.flitAirLineNum}
						
						</div>
					</td></tr>
				 <tr>
				 	<td><span>{refundInfos.name}({refundInfos.paxType})</span></td>
				 	<td><span>{refundInfos.pnr}</span></td>
                    <td><span>{NumberUtils.numberFormat(refundInfos.total_rev)} </span></td>
                     <td><span>{NumberUtils.numberFormat(refundInfos.total_sup_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_ct_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_cb)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_dis)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_stx_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_ct_stx_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_misc_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_amd_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_oth_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_wt_cb_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_bundle_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.total_gw_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundInfos.refund_amount)} </span></td>
				
                  </tr>
				<tr >
				<td colSpan="16"><span className="t-color1">
				Refund Breakup : Refund to {refundInfos.paymentmode}({refundInfos.tnxNumb}) : {NumberUtils.numberFormat(refundInfos.refund_amount)} 
				 </span>
				</td>
				</tr>

				</>
			);
			
			})}
			<tr>
				<td colSpan="2"><strong className="font-14">Total</strong></td>
                
				<td>{NumberUtils.numberFormat(this.state.totalRevarsal)}</td>
                <td>{NumberUtils.numberFormat(this.state.totalSup)}</td>
                <td>{NumberUtils.numberFormat(this.state.totalCt)}</td>
                <td>{NumberUtils.numberFormat(this.state.totalCshBack)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalDiscount)}</td>
                <td>{NumberUtils.numberFormat(this.state.totalSTax)}</td>
                <td>{NumberUtils.numberFormat(this.state.totalCTSTax)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalMiscCharge)}</td>				
				<td>{NumberUtils.numberFormat(this.state.totalAmdCharge)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalOtherCharge)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalWtCbCharge)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalBundleCharge)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalGateWayCharge)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalTotalRefund)}</td>
			</tr>
				
                  </tbody>
                </table>
				</div>
            </div>
           
</>

        )
    }


}

const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,       
        airPortMaster: state.trpReducer.airPortMaster,
		paymentTypeMaster:state.trpReducer.paymentTypeMaster, 
        
       
     };
}

export default connect(mapStateToProps, null)(FlightRefund);



