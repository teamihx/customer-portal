import React, { Component} from 'react'
import { connect } from 'react-redux';

class TripStatus extends Component { 
    constructor(props) {
        super(props)
        let transactions = [];
        let airBookingInfo=[];
        this.state = {
            transactions: this.props.tripDetail.txns,
            airBookingInfo:this.props.tripDetail.air_bookings[0].air_booking_infos,
            days:'',
            cancelTxnId:'',
            qtype:'',
            paxName:'',
            sector:'',
            entryTime:'',
            timetoAction:'',
            misc:'',
            showMessage:true,
            cancelInfoList:[]

        }
        this.getAirCancellationData();
    }
    getPaxNameAndSector(bookingInfoId){
     let paxFullname="";
     var paxList = this.props.tripDetail.air_bookings[0].pax_infos;
     var flights = this.props.tripDetail.air_bookings[0].flights;
     if(this.state.airBookingInfo){
      var airbookingInfoList =  this.state.airBookingInfo.filter(airinfo => airinfo.id === bookingInfoId);
      if(airbookingInfoList){
      for(let info of airbookingInfoList){
        var paxInfoList =  paxList.filter(pax => pax.id === info.pax_info_id);
        var paxNme="";
        if(paxInfoList[0].title!==null && paxInfoList[0].title!==undefined){
         paxNme=paxInfoList[0].title+" "+paxInfoList[0].first_name+" "+paxInfoList[0].last_name
         }else{
         paxNme=paxInfoList[0].first_name+" "+paxInfoList[0].last_name
         }
         for(let flight of flights){
          for(let segment of flight.segments){
           if(segment.id===info.segment_id){
            var sector=segment.departure_airport+"-"+segment.arrival_airport;
           }
          }
        }
      }
      if(paxNme!==null && paxNme!==undefined){
        paxFullname=paxNme
       }
      if(sector!==null && sector!==undefined){
        paxFullname=paxFullname+"("+sector+")";
      }
     this.state.paxName=paxFullname;
     //console.log('PAX NAME------:'+this.state.paxName);
      }

     }


    }

    getAirCancellationData(){
      let cancelInfo=[];
      let refundId='';
    if(this.state.transactions.length!==0){
      for(let trnxn of this.state.transactions){
        if(trnxn.txn_type===10 && trnxn.misc!==null){
          if(trnxn.misc.includes('in_manual_q') || trnxn.misc.includes('in_refund_compute_q')){
          var miscVal=trnxn.misc.replace("\\\",");
          var miscFinal=miscVal.replace("\",");
          var res = miscFinal.substring(3);
          var miscObj=JSON.parse(res);
          var miscObj1=JSON.parse(miscObj);
          var totalRefundTime=48;
          if(miscObj1!==null && miscObj1.status_tool!==null && miscObj1.status_tool.in_manual_q!==undefined &&  miscObj1.status_tool.in_manual_q!==null && miscObj1.status_tool.in_refund_compute_q!==undefined && miscObj1.status_tool.in_refund_compute_q){
            var miscDtl = new Object(); 
            var paxname="";
            miscDtl.cancelTxnId=trnxn.id;
            for(let aircancel of trnxn.air_cancellations){
              this.getPaxNameAndSector(aircancel.air_booking_info_id);
              paxname=this.state.paxName;
              if(miscDtl.paxName===null ||  miscDtl.paxName===undefined){
                miscDtl.paxName=paxname;
              }else{
                miscDtl.paxName=miscDtl.paxName+", "+paxname;
              }
              }
            //Cancellation 
            miscDtl.qtype='Cancellation Q';
            miscDtl.entryTime=trnxn.air_cancellations[0].created_at;
            if(trnxn.air_cancellations[0].cancellation_status==='D'){
              miscDtl.exitTime=trnxn.air_cancellations[0].updated_at;
            }
            if(miscDtl.exitTime!==null && miscDtl.exitTime!==undefined){
             var time=new Date(miscDtl.exitTime).getTime()-new Date(miscDtl.entryTime).getTime()
             miscDtl.timeTaken=time;
             if(time!==null && time!==undefined){
              var refunTime=totalRefundTime-time;
              miscDtl.canRefundTime=refunTime;
              }
            }
           //Refund Computation 
            miscDtl.typeRefund='Refund Computation Q' 
            if(trnxn.air_refund_records.length!==0){
              for(let refund of trnxn.air_refund_records){
                if(refund.id===trnxn.air_cancellations[0].air_refund_record_id){
                 miscDtl.entryRefTime=refund.created_at;
                 if(refund.status==='D'){
                   miscDtl.exitRefTime=refund.updated_at;
                 }
                 if(miscDtl.exitRefTime!=null && miscDtl.exitRefTime!==undefined){
                  var time=new Date(miscDtl.exitRefTime).getTime()-new Date(miscDtl.entryRefTime).getTime()
                  miscDtl.timeRefTaken=time;
                  var refunTime=totalRefundTime-time;
                  miscDtl.refundTime=refunTime;
                  }else if(refund.status==='S'){
                    miscDtl.shelve_date_time=refund.updated_at;
                    miscDtl.unshelve_date_time=refund.unshelve_at;
                  }else{
                    miscDtl.refComTime=refunTime;
                  }
                }
              }
             }else{
              miscDtl.entryRefTime=trnxn.updated_at;
             }
            cancelInfo.push(miscDtl);
            this.state.cancelInfoList=cancelInfo;
            this.state.showMessage=false;
          }else if(miscObj1.status_tool.in_refund_compute_q){

            var miscDtl = new Object(); 
            miscDtl.cancelTxnId=trnxn.id;
            var paxname="";
            for(let aircancel of trnxn.air_cancellations){
              this.getPaxNameAndSector(aircancel.air_booking_info_id);
              paxname=this.state.paxName;
              if(miscDtl.paxName===null ||  miscDtl.paxName===undefined){
                miscDtl.paxName=paxname;
              }else{
                miscDtl.paxName=miscDtl.paxName+", "+paxname;
              }
              }
              //Refund Computation 
            miscDtl.typeRefund='Refund Computation Q' 
            if(trnxn.air_refund_records.length!==0){
              for(let refund of trnxn.air_refund_records){
                if(refund.id===trnxn.air_cancellations[0].air_refund_record_id){
                 miscDtl.entryRefTime=refund.created_at;
                 if(refund.status==='D'){
                   miscDtl.exitRefTime=refund.updated_at;
                 }
                 if(miscDtl.exitRefTime!=null && miscDtl.exitRefTime!==undefined){
                  var time=new Date(miscDtl.exitRefTime).getTime()-new Date(miscDtl.entryRefTime).getTime()
                  miscDtl.timeRefTaken=time;
                  var refunTime=totalRefundTime-time;
                  miscDtl.refundTime=refunTime;
                  }else if(refund.status==='S'){
                    miscDtl.shelve_date_time=refund.updated_at;
                    miscDtl.unshelve_date_time=refund.unshelve_at;
                  }else{
                    miscDtl.refComTime=refunTime;
                  }
                }
              }
            }else{
              miscDtl.entryRefTime=trnxn.updated_at;
            }
            cancelInfo.push(miscDtl);
            this.state.cancelInfoList=cancelInfo;
            this.state.showMessage=false;
          }else if(miscObj1.status_tool.in_manual_q){
            var miscDtl = new Object(); 
            var paxname="";
            miscDtl.cancelTxnId=trnxn.id;
            for(let aircancel of trnxn.air_cancellations){
              this.getPaxNameAndSector(aircancel.air_booking_info_id);
              paxname=this.state.paxName;
              if(miscDtl.paxName===null ||  miscDtl.paxName===undefined){
                miscDtl.paxName=paxname;
              }else{
                miscDtl.paxName=miscDtl.paxName+", "+paxname;
              }
              }
            miscDtl.qtype='Cancellation Q';
            miscDtl.entryTime=trnxn.air_cancellations[0].created_at;
            if(trnxn.air_cancellations[0].cancellation_status==='D'){
              miscDtl.exitTime=trnxn.air_cancellations[0].updated_at;
            }
            if(miscDtl.exitTime!==null && miscDtl.exitTime!==undefined){
             var time=new Date(miscDtl.exitTime).getTime()-new Date(miscDtl.entryTime).getTime()
             miscDtl.timeTaken=time;
             if(time!==null && time!==undefined){
              var refunTime=totalRefundTime-time;
              miscDtl.refundTime=refunTime;
              }
              }
              cancelInfo.push(miscDtl);
              this.state.cancelInfoList=cancelInfo;
              
              this.state.showMessage=false;

          }
          if(trnxn.air_refund_records.length!==0){
            for(let refund of trnxn.air_refund_records){
            refundId=refund.refund_id;
            
            }
          }
          this.state.showMessage=false;
        }else{
          //console.log('No status information for this trip');
        }
      }
      }
      for(let value of this.state.transactions){
       if(value.refunds.length!==0){
        var refunds =  value.refunds.filter(ref => ref.id === refundId);
       }
      }
    }else{
      //('No status information for this trip');
    }
    
    }

    render() {
      this.getAirCancellationData;
        return (
          <div>
             {this.state.showMessage && 
             <h6>No status information for this trip</h6>
             }
             <br/>
              {this.state.cancelInfoList.map((trxn, index) => (
                <div key={index}>
                  <h3>Cancellation : (Processing Time: 2d, Cancel Txn id: <strong>{trxn.cancelTxnId}</strong>)</h3>
                  <div className ='resTable'>
                  <table className="dataTbl pax-tbl dataTable5">
                    <tbody>
                    <tr>
                      <td> Pax(Segment details) : {trxn.paxName}</td>
                   </tr>
                   {trxn.qtype==='Cancellation Q' && (
                   <tr>
                      <td>
                      <strong className="d-b">Cancellation Q</strong>
                      Entry Time: {trxn.entryTime}

                      {trxn.exitTime!=null && trxn.exitTime!==undefined && (
                        <>
                        Exit Time : {trxn.exitTime}
                        </>
                      )}
                      </td>

                   </tr>
                   
                   )}
                     {trxn.typeRefund==='Refund Computation Q' && (
                   <tr>
                      <td>
                      <strong className="d-b">Refund Computation Q</strong>
                      Entry Time: {trxn.entryRefTime}

                      {trxn.exitRefTime!==null && trxn.exitRefTime!==undefined && (
                        <>
                        Exit Time : {trxn.exitRefTime}
                        </>
                      )}
                      </td>

                   </tr>
                   
                   )}

                    {trxn.uploadQ==='Refund Upload Q' && (
                   <tr>
                      <td>
                      <strong className="d-b">Refund Upload Q</strong>
                      Entry Time: {trxn.uploadEntryTime}

                      {trxn.uploadExitTime!==null && trxn.uploadExitTime!==undefined && (
                        <>
                       Exit Time : {trxn.uploadExitTime}
                        </>
                      )}
                      </td>

                   </tr>
                   
                   )}

                    </tbody>
                  </table>
                  </div>
                </div>
              ))}
        </div>
        )
      }
}
const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail
    };
  }
  export default connect(mapStateToProps, null)(TripStatus);
