import React, { Component} from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Icon from 'Components/Icon';
import 'react-day-picker/lib/style.css';
import MasterService from 'Services/master/MasterService'
import DateUtils from 'Services/commonUtils/DateUtils.js'
import FlightService from '../../../services/trips/flight/FlightService.js'
const fltRS = require('Assets/json/data.json');
import { connect } from 'react-redux';
import log from 'loglevel';
import Button from 'Components/buttons/button.jsx';
import * as actions from '../../../store/actions/index';
import paymntService from '../../../services/trips/payment/PaymentService';
export const USER_AUTH_DATA = 'userAuthData';
export const PAX_DATA = 'paxData';
import NoteService from '../../../services/trips/note/NoteService';
export const TRIPS_ROLES = 'tripsRoles';
import Notification from "Components/Notification.jsx";
//const pax = fltRS.air_bookings[0].pax_infos;

class FlightPax extends Component {  

  constructor(props) {
    super(props)
    let paxInfos = [];
    this.state = {
      //fphRS: this.props.paxData,
      paxInfos: [...this.props.tripDetail.air_bookings[0].pax_infos],
      showEditPax: false,
      selectedDay: undefined,
      mealMap: [
        { code: "AVML", name: "Asian Veg. Meal" },
        { code: "BBML", name: "Inf/Baby Food" },
        { code: "BLML", name: "Bland Meal" },
        { code: "CHML", name: "Child Meal" },
        { code: "DBML", name: "Diabetic Meal" },
        { code: "GFML", name: "Gluten Free" },
        { code: "HFML", name: "High Fiber Meal" },
        { code: "HNML", name: "Hindu" },
        { code: "KSML", name: "Kosher" },
        { code: "LCML", name: "Low Calorie" },
        { code: "LCRB", name: "Low Carbohydrate" },
        { code: "LFML", name: "Low Choles./Fat" },
        { code: "LPML", name: "Low Protein" },
        { code: "LSML", name: "Low Sodium" },
        { code: "MOML", name: "Muslim Meal" },
        { code: "NFML", name: "No Fish Meal" },
        { code: "ORML", name: "Oriental" },
        { code: "PFML", name: "Peanut Free Meal" },
        { code: "PRML", name: "Low Purine Meal" },
        { code: "RVML", name: "Raw Vegetarian" },
        { code: "SFML", name: "Seafood Meal" },
        { code: "SPML", name: "Spec Request" },
        { code: "VGML", name: "Vegetarian" },
        { code: "VLML", name: "Veg/Lacto-Ovo" }
      ],
      airlinesMaster:this.props.airLineMaster,
      startDate: new Date(),
      expDate: new Date(),
      sucessmsg:'',
      errormsg:'',
      airlinesList:[],
      loading:false,
      applicableAirList:[],
      count: 1,
      showMessage:false,
      transactionId:'',
      tranxOpenData:[],
      showMessageTrxn:false,
      publicIp:'',
      tripNotes:(Object.values(this.props.tripDetail.notes)),
      addNote:false,
      ftripsPermissions:''

    }
    this.handleEditPaxDetail = this.handleEditPaxDetail.bind(this);
    this.savePaxDetails = this.savePaxDetails.bind(this);
    this.cancelPax=this.cancelPax.bind(this);
    this.handleChangeAirline = this.handleChangeAirline.bind(this);
    this.handleChangeFfpNum = this.handleChangeFfpNum.bind(this);
    localStorage.removeItem(PAX_DATA);
    //Creating Airlines List
    if(this.state.airlinesMaster){
      for (var key in this.state.airlinesMaster) {
        const airObj = {
          airCode:key,
          airName:this.state.airlinesMaster[key].name
        };
      this.state.airlinesList.push(airObj);
     }
    }
   this.createPaxInfo();
   //Getting ROLES DATA from local storage
   let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
   this.state.ftripsPermissions=tripsObj.ftripPermissions;
  }

  storePaxInfo(){
    let paxInfosData=JSON.stringify(this.props.tripDetail.air_bookings[0].pax_infos);
    localStorage.setItem(PAX_DATA,paxInfosData);
  }

  createPaxInfo(){
    try{
      for(let value of this.state.paxInfos){
        if(value.date_of_birth!==null && value.date_of_birth!=="" && value.date_of_birth.includes('T')){
          const date = DateUtils.convertStringToDate(value.date_of_birth);
          value.date_of_birth=date;
        }
        if(value.passport_detail!==null && value.passport_detail.date_of_expiry!=null && value.passport_detail.date_of_expiry!=="" && value.passport_detail.date_of_expiry!==undefined){
          if(parseInt(value.passport_detail.date_of_expiry)) {
          const date = DateUtils.convertStringToDate(value.passport_detail.date_of_expiry);
          value.passport_detail.date_of_expiry=date;
          }
        }
        if(value.meal_request_code!==null && value.meal_request_code!=="" && value.meal_request_code!==undefined){
          for(let meal of this.state.mealMap){
              if(meal.code!==null && meal.code===value.meal_request_code){
                value.meal_request_code=meal.name;
                break;
              }
          }
          }
          if(value.frequent_flier_numbers){
            for(let ffq of value.frequent_flier_numbers){
              for(let airline of this.state.airlinesList){
                if(airline.airCode!==null && ffq.airline!==null && airline.airCode===ffq.airline){
                  ffq.airline=airline.airName;
                }
            }
            }
          }
      }
    }catch(err){
  log.error('Exception occured in FlightPax Component fetchPublicIp '+err);

}
}

  cancelPax(){
    let paxInfoArray=[];
    var paxData=localStorage.getItem(PAX_DATA);
    paxInfoArray=JSON.parse(paxData);
    this.setState({ paxInfos: paxInfoArray });
    for(let value of this.state.paxInfos){
    if(value.meal_request_code!==null && value.meal_request_code!=="" && value.meal_request_code!==undefined){
      for(let meal of this.state.mealMap){
          if(meal.code!==null && meal.code===value.meal_request_code){
            value.meal_request_code=meal.name;
            break;
          }
      }
      }
      if(value.frequent_flier_numbers){
        for(let ffq of value.frequent_flier_numbers){
          for(let airline of this.state.airlinesList){
            if(airline.airCode!==null && ffq.airline!==null && airline.airCode===ffq.airline){
              ffq.airline=airline.airName;
            }
        }
        
        }
      }
    }
    this.forceUpdate();
    localStorage.removeItem(PAX_DATA);
    this.setState({ showEditPax: false });
  }

  handleEditPaxDetail(e) {
     if(this.state.paxInfos){
      this.storePaxInfo();
      //console.log('PAX :'+JSON.stringify(this.state.paxInfos));
      let appAirlines=[];
      let marketingAirline=[];
      var flights = this.props.tripDetail.air_bookings[0].flights;
      for(let flt of flights){
        marketingAirline.push(flt.segments[0].marketing_airline);
        appAirlines=Array.from(new Set(marketingAirline));
      } 
      if(appAirlines){
        for(let value of appAirlines){
         const air = {
           airCode:value
           }
         this.state.applicableAirList.push(air);
        }
       }
      
     let counter=1;
      for(let value of this.state.paxInfos){
        //Passport
         if(value.passport_detail===null || value.passport_detail===undefined || value.passport_detail===""){
          const passportObj = {
            date_of_expiry:'',
            issuing_country:'',
            nationality:'',
            passport_number:''
          };
          value.passport_detail=passportObj
         }
         //Frequent Flier
         if(value.frequent_flier_numbers===undefined || value.frequent_flier_numbers.length===0){
          if(this.state.applicableAirList.length!==0){
           var count =this.state.applicableAirList.length;
           for(let air of this.state.applicableAirList){
          const freqObj = {
            airline:'',
            freq_flier_number:undefined,
            applicable_airline:air.airCode,
            selectedFfp:counter
            };
           value.frequent_flier_numbers.push(freqObj);
           counter=counter+1;
           }
          }
         }else{
          for(let ffq of value.frequent_flier_numbers){
            ffq.selectedFfp=counter;
             counter=counter+1;
          }
         }
      //Visa
      if(value.poi_detail===null || value.poi_detail===undefined || value.poi_detail===""){
        const visaObj = {
          visa_type:''
          };
        value.poi_detail=visaObj
       }

       if(value.meal_request_code!==null && value.meal_request_code!==""){
        for(let meal of this.state.mealMap){
            if(meal.name===value.meal_request_code){
              value.meal_request_code=meal.code;
              break;
            }
        }
        }
        if(value.frequent_flier_numbers){
          for(let ffq of value.frequent_flier_numbers){
            for(let airline of this.state.airlinesList){
              if(airline.airCode!==null && ffq.airline!==null && airline.airName===ffq.airline){
                ffq.airline=airline.airCode;
              }
          }
          }
        }
        
     }
    }
this.setState({ showEditPax: true, sucessmsg: "", errormsg: "" });
}

fetchPublicIp() {
  //console.log('fightpax fetchPublicIp-');

  try{
  return new Promise((resolve, reject) => {   
    let txnReq ='';      
    FlightService.getpublicIp(txnReq).then(response => {
     //console.log('fightpax this.state.publicIp respo--'+JSON.stringify(response));
      if(response.status===200){

      this.state.publicIp=response.data.ip_address;
     //console.log('fightpax this.state.publicIp--'+JSON.stringify(this.state.publicIp));
      resolve (response.data.ip_address);

      }
    }).catch((error) => {
      assert.isNotOk(error,'Promise error');
      reject(error);
    });
  })
}catch(err){
  reject(err);
  log.error('Exception occured in FlightPax Component fetchPublicIp '+err);

}
}

createPaxTrxn(trxnId){
  var authData= localStorage.getItem(USER_AUTH_DATA);
  var obj = JSON.parse(authData);
  let txnObj = new Object();
  txnObj.status="C",
  txnObj.trip_id=this.props.tripDetail.id,
  txnObj.txn_type=40,
  txnObj.source_type="HQ",
  txnObj.user_id=obj.data.id,
  txnObj.misc='',
  txnObj.emails=[],
  txnObj.source_id=this.state.publicIp;
  if(trxnId!==""){
    txnObj.id=trxnId;
  }
  let txnArr=[txnObj]
  const txnReq={
      txns:[...txnArr]
  }
  //console.log("txnReq==="+JSON.stringify(txnReq))
  paymntService.createNewTransaction(txnReq).then(response =>{
    var obj=JSON.parse(response.data);
    if (response !== undefined &&
      obj !== "" &&
      obj.responsecode !== undefined &&
      obj.responsecode !== "" &&
      obj.responsecode === 200) {
        //console.log("Closed Pax type Trxn Successfully "+this.props.tripDetail.id);
        if(obj.modelsUpdated!==null && obj.modelsUpdated.txns[0]!==null){
          this.props.tripDetail.txns.push(obj.modelsUpdated.txns[0]);
          this.props.updatePax(this.props.tripDetail);
        }
      }
  });
}

async savePaxDetails(e){  
    //console.log('save PaxDetails start..')
    await this.fetchPublicIp();
    this.setState({loading:true});
    var trxn=true;
   if(this.props.tripDetail.txns){
     for(let value of this.props.tripDetail.txns){
          if(value.txn_type === 40 && value.status==="O"){
            trxn=false;
            break;
          }
     }
   }
if(trxn){
  //Opening the Transaction
  var authData= localStorage.getItem(USER_AUTH_DATA);
  var obj = JSON.parse(authData);
  let txnObj = new Object();
  txnObj.status="O",
  txnObj.trip_id=this.props.tripDetail.id,
  txnObj.txn_type=40,
  txnObj.source_type="HQ",
  txnObj.user_id=obj.data.id,
  txnObj.misc='',
  txnObj.emails=[],
  txnObj.source_id=this.state.publicIp;
  let txnArr=[txnObj]
  const txnReq={
      txns:[...txnArr]
  }
  paymntService.createNewTransaction(txnReq).then(response =>{
    var obj=JSON.parse(response.data);
    //console.log("Created Pax type Trxn Successfully1 "+response.data);
    if (obj !== undefined &&
      obj !== "" &&
      obj.status !== undefined && obj.status !== "" && obj.status === 200) {
        //console.log("Created Pax type Trxn Successfully "+this.props.tripDetail.id);
        if(response.data.modelsUpdated!==null && obj.modelsUpdated.txns[0]!==null){
          this.setState({ tranxOpenData: obj.modelsUpdated.txns[0], transactionId: obj.modelsUpdated.txns[0].id});
        }
        this.savePaxDetailsNew();
        this.addPaxNote();
      }else{
        this.setState({ showMessageTrxn: true, loading:false, showEditPax: true, showAlert: true  }); 
      }
  });
}else{
  this.setState({ showMessage: true, loading:false, showEditPax: true, showAlert: true }); 
  this.forceUpdate();
}
}


savePaxDetailsNew(){  
  //console.log('save savePaxDetailsNew start..')
 FlightService.updatePaxDetails(this.state.paxInfos).then(response => {
  try{
  const updatedRS = JSON.parse(response.data);
  //console.log("save savePaxDetailsNew Res " + updatedRS.msg);
  if (updatedRS !== undefined &&
    updatedRS !== "" &&
    updatedRS.responsecode !== undefined &&
    updatedRS.responsecode !== "" &&
    updatedRS.responsecode === 200) {
      this.props.tripDetail.air_bookings[0].pax_infos=[...this.state.paxInfos];
      this.props.updatePax(this.props.tripDetail);
      if(this.state.transactionId!==null && this.state.transactionId!==""){
      this.createPaxTrxn(this.state.transactionId);
      }
      for(let value of this.state.paxInfos){
        if(value.date_of_birth!==null && value.date_of_birth!==""){
          var newdate = value.date_of_birth.split("-").reverse().join("-");
          value.date_of_birth=newdate;
        }
        if(value.passport_detail!==null && value.passport_detail.date_of_expiry!=null && value.passport_detail.date_of_expiry!==""){
          var newdate = value.passport_detail.date_of_expiry.split("-").reverse().join("-");
          value.passport_detail.date_of_expiry=newdate;
        }
        if(value.meal_request_code!==null && value.meal_request_code!==""){
          for(let meal of this.state.mealMap){
              if(meal.code===value.meal_request_code){
                value.meal_request_code=meal.name;
                break;
              }
          }
          }
          if(value.frequent_flier_numbers){
            for(let ffq of value.frequent_flier_numbers){
              for(let airline of this.state.airlinesList){
                if(airline.airCode!==null && ffq.airline!==null && airline.airCode===ffq.airline){
                  ffq.airline=airline.airName;
                }
            }
            }
          }
      }
    const msg='Pax Details updated successfully'
    this.setState({ sucessmsg: msg, loading:false, showEditPax: false, showAlert: true });
    this.forceUpdate();
    window.scrollTo(0, 0); 
  } else {
    this.props.tripDetail.txns.push(this.state.tranxOpenData);
    this.setState({ paxInfos: this.props.tripDetail.air_bookings[0].pax_infos });
    for(let value of this.state.paxInfos){
    if(value.frequent_flier_numbers){
      for(let ffq of value.frequent_flier_numbers){
        for(let airline of this.state.airlinesList){
          if(airline.airCode!==null && ffq.airline!==null && airline.airCode===ffq.airline){
            ffq.airline=airline.airName;
          }
      }
      }
    }
    }
    this.setState({ errormsg: "Pax Details not updated ", loading:false, showEditPax: true, showAlert: true })
    this.forceUpdate();        
    window.scrollTo(0, 0);
  }
}catch(err){
  this.setState({loading:false, showEditPax: true, showAlert: true, errormsg: "Pax Details not updated " });
  this.forceUpdate();
  log.error('Exception occured in FLight pax PaxDetails function---'+err);
}
});
}

 handlePassportChange = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, passport_detail: {
      ...paxInfo.passport_detail,passport_number: evt.target.value
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo }, () => console.log("Handle  :  " + JSON.stringify(this.state.paxInfos)));
}
handleChangeCountry = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, passport_detail: {
      ...paxInfo.passport_detail,issuing_country: evt.target.value
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleChangeNation = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, pax_nationality: evt.target.value
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleChangeMealcode = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, meal_request_code: evt.target.value
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleChangeVisaType = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, poi_detail: {
      ...paxInfo.poi_detail,visa_type: evt.target.value
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}


handleChangeAirline = ffqOb => evt => {
  if(evt.target.value!=="" && evt.target.value!==null){
  for(let pax of this.state.paxInfos){
     if(pax.frequent_flier_numbers!==0){
       for(let ffq of pax.frequent_flier_numbers){
          if(ffq.selectedFfp===ffqOb.selectedFfp){
            ffq.airline=evt.target.value;
          }
     }
  }
}
this.setState({ paxInfos: this.state.paxInfos });
}
}

handleChangeFfpNum = ffqOb => evt => {
  if(evt.target.value!=="" && evt.target.value!==null){
  for(let pax of this.state.paxInfos){
     if(pax.frequent_flier_numbers!==0){
       for(let ffq of pax.frequent_flier_numbers){
          if(ffq.selectedFfp===ffqOb.selectedFfp){
            ffq.freq_flier_number=evt.target.value;
          }
     }
  }
}
}else{
  for(let pax of this.state.paxInfos){
    if(pax.frequent_flier_numbers!==0){
      for(let ffq of pax.frequent_flier_numbers){
        if(ffq.selectedFfp===ffqOb.selectedFfp){
           ffq.freq_flier_number="";
         }
    }
 }
}
}
this.setState({ paxInfos: this.state.paxInfos });
}

handleChangeffp = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, frequent_flier_numbers: [{
      ...paxInfo.frequent_flier_numbers[0],freq_flier_number: evt.target.value
      }]
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}


handleDayChange = idx => startDate => {
  this.setState({ startDate });
  const date = DateUtils.convertStringToDate(startDate);
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, date_of_birth: date
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleDayChangeExpiry = idx => expDate => {
  this.setState({ expDate });
  const date = DateUtils.convertStringToDate(expDate);
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, passport_detail: {
      ...paxInfo.passport_detail,date_of_expiry: date
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

addPaxNote(event){
  try{
  //console.log('in addPaxNote handle'+this.props.tripDetail.id)
      let authDat= localStorage.getItem(USER_AUTH_DATA);
      let obj = JSON.parse(authDat);
      const noteTobeAdded={
          notes :[
              {
              note: "Pax Info updated",
              parent_note_id: null,
              subject:"Pax Info update Note",
              user_id:obj.data.id,
              trip_id:this.props.tripDetail.id,
              created_at:new Date().toUTCString()
              } 
          ]   
          };
      NoteService.updatetripNotes(noteTobeAdded)
      .then(response =>{    
         const data = JSON.parse(response.data)
         const {updateStatus} = data
          if(updateStatus==="true"){
            this.updateTripNotes(response.data);
            this.forceUpdate();
          }else{
            var message='Note not updated'
          }
        })
        .catch(error => this.setState({ error }));
   }catch(err){
  this.forceUpdate();
  message='Note not updated'
}
}

 formatDate(string){
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  return string.toLocaleDateString([],options);
}
  setStartDate = (startDate)=> {
    this.setState({
      startDate
    })
  }

  updateTripNotes(noteTobeAdded){
     let noteData=JSON.parse(noteTobeAdded);
     let noteDetails=noteData.modelsUpdated.notes;
    this.setState({
        tripNotes:this.state.tripNotes.concat(noteDetails).sort((a,b)=>
            ( new Date(a.created_at).getTime() - new Date(b.created_at).getTime())
        ).reverse(),
        newNote:''
    })
    this.props.tripDetail.notes=this.state.tripNotes
    this.props.updateNotes(this.props.tripDetail);
   }
   toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  
  render() {
    let meals = this.state.mealMap;
    let mealsMenu = meals.map((meal) =>
      <option value={meal.code}>{meal.name}</option>
    );
    let airMaster = this.state.airlinesList;
    let airlines = airMaster.map((airline) =>
      <option value={airline.airCode}>{airline.airName} [{airline.airCode}]</option>
    );
    let airAppMaster = this.state.applicableAirList;
    let applicableAirs = airAppMaster.map((app) =>
    <option value={app.airline}>{app.airline}</option>
     );
  const appCount=this.state.applicableAirList.length;
    // const [startDate, setStartDate] = useState(new Date());
    const { startDate } = this.state;

    let viewType, messageText;
    if (this.state.errormsg !== "" && this.state.errormsg !== undefined) {
      viewType = "error";
      messageText = this.state.errormsg;
    } else if (this.state.sucessmsg !== "" && this.state.sucessmsg !== undefined) {
      viewType = "success";
      messageText = this.state.sucessmsg;
    } else if (this.state.showMessage) {
      viewType = "warning";
      messageText = "Since some txns in this trip are open you cannot update Pax details now"
    } else if (this.state.showMessageTrxn) {
      viewType = "warning";
      messageText = "Not able to update the Pax details now. Please try again later"
    }
    return (

      <div>
        {viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}

        {/* {this.state.errormsg !== "" && this.state.errormsg !== undefined && (
              <div className="alert alert-danger"><Icon className="noticicon" color="#cda11e" size={16} icon="warning"/> {this.state.errormsg}</div>
        )}
       {this.state.sucessmsg !== "" && this.state.sucessmsg !== undefined && (
              <div className="alert notification-success"><Icon color="#02AE79" size={16} icon="success"/> {this.state.sucessmsg}</div>
       )}
       {this.state.showMessage && (
              <div className="alert alert-warning"><Icon className="noticicon" color="#cda11e" size={16} icon="warning"/>Since some txns in this trip are open you cannot update Pax details now</div>
        )}
         {this.state.showMessageTrxn && (
              <div className="alert alert-warning"><Icon className="noticicon" color="#cda11e" size={16} icon="warning"/>Not able to update the Pax details now. Please try again later</div>
        )} */}
      <div>
     
        {this.state.paxInfos.map((paxInfo, index) => (
          <div key={index}>
            <div className="highInfo">{paxInfo.title}. {paxInfo.first_name}  {paxInfo.last_name} <small>{paxInfo.pax_type_code}</small></div>
            <div className="resTable mobOverflow">
            <table className="dataTbl pax-tbl dataTable5">
              <thead><tr>
                <th width="15%">Date Of Birth</th>
                <th width="15%">Passport#</th>
                <th width="15%">Issuing country</th>
                <th width="15%">Nationality</th>
                <th width="15%">Expires on</th>
                <th width="15%">Meal code</th>
                <th width="10%">Visa type</th>
              </tr></thead>
              <tbody>
                <tr>
                  
                  <td>
                  {this.state.showEditPax === true && (
                    <div className="dtPik">
                    <Icon className="calendar" color="#333" size={16} icon="calendar3"/> 
                     <DatePicker
                      value={paxInfo.date_of_birth}
                      onChange={this.handleDayChange(index)}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                    />
                    
                    </div>
                  )}
                  {this.state.showEditPax === false && (
                      <span>{paxInfo.date_of_birth}</span>
                    )}
                  </td>

                  <td>
                  {this.state.showEditPax === true && (
                      <input type="text" id="passport_number" name="passport_number" value={paxInfo.passport_detail.passport_number} onChange={this.handlePassportChange(index)} />
                  )}
                    {this.state.showEditPax === false && paxInfo.passport_detail!==null && paxInfo.passport_detail.passport_number!==null && paxInfo.passport_detail.passport_number!=='' && (
                      <span>{paxInfo.passport_detail.passport_number} </span>
                  )}
                   {this.state.showEditPax === false && ((paxInfo.passport_detail!==null && (paxInfo.passport_detail.passport_number==='' || paxInfo.passport_detail.passport_number===null || paxInfo.passport_detail.passport_number===undefined)) || (paxInfo.passport_detail===null || paxInfo.passport_detail===undefined) ) && (
                      <span>--</span>
                  )}
                  </td>
                  <td>
                  {this.state.showEditPax === true  && (
                      <input type="text" id="issuing_country" name="issuing_country" value={paxInfo.passport_detail.issuing_country} onChange={this.handleChangeCountry(index)} />
                    )}
                     {this.state.showEditPax === false && paxInfo.passport_detail!==null && paxInfo.passport_detail.issuing_country!==null && paxInfo.passport_detail.issuing_country!=='' && (
                      <span>{paxInfo.passport_detail.issuing_country}</span>
                    )}
                    {this.state.showEditPax === false && ((paxInfo.passport_detail!==null && (paxInfo.passport_detail.issuing_country==='' || paxInfo.passport_detail.issuing_country===null || paxInfo.passport_detail.issuing_country===undefined)) || (paxInfo.passport_detail===null || paxInfo.passport_detail===undefined) ) && (
                      <span>--</span>
                  )}
                  </td>
                  <td>
                  {this.state.showEditPax === true && (
                      <input type="text" name="pax_nationality" value={paxInfo.pax_nationality} onChange={this.handleChangeNation(index)} />
                    )}
                    {this.state.showEditPax === false && paxInfo.pax_nationality!==null && paxInfo.pax_nationality!=='' && (
                      <span>{paxInfo.pax_nationality} </span>
                    )}
                    {this.state.showEditPax === false && (paxInfo.pax_nationality===null || paxInfo.pax_nationality===undefined) && (
                      <span>--</span>
                    )}
                  </td>
                 <td>
                  {this.state.showEditPax === true && (
                     <div className="dtPik">
                     <Icon className="calendar" color="#333" size={16} icon="calendar3"/> 
                     <DatePicker
                      value={paxInfo.passport_detail.date_of_expiry}
                      onChange={this.handleDayChangeExpiry(index)}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                    />
                    </div>
                  )}
                  {this.state.showEditPax === false && paxInfo.passport_detail!==null && paxInfo.passport_detail.date_of_expiry!==null && paxInfo.passport_detail.date_of_expiry!=='' && (
                      <span>{paxInfo.passport_detail.date_of_expiry}</span>
                  )}
                  </td> 
                  <td> 
                  {this.state.showEditPax === true && (<div className="custom-select-v3"><Icon className="select-me" color="#CAD6E3" size={20} icon="down-arrow" />
                  <select name="meal_request_code" value={paxInfo.meal_request_code} onChange={this.handleChangeMealcode(index)}>
                    {mealsMenu}
                  </select></div>)}
                  {this.state.showEditPax === false && paxInfo.meal_request_code!==null && paxInfo.meal_request_code!=='' && (
                      <span>{paxInfo.meal_request_code}</span>
                    )}
                    {this.state.showEditPax === false && (paxInfo.meal_request_code==='' || paxInfo.meal_request_code===null || paxInfo.meal_request_code===undefined) && (
                      <span>--</span>
                    )}
                  </td>

                  <td>
                    
                     {this.state.showEditPax === true &&  paxInfo.poi_detail!=null && (
                      <input type="text" name="visaType" value={paxInfo.poi_detail.visa_type} onChange={this.handleChangeVisaType(index)} />
                    )}
                    {this.state.showEditPax === false && paxInfo.poi_detail!=null && paxInfo.poi_detail.visa_type!==null && (
                      <span>{paxInfo.poi_detail.visa_type}  </span>
                    )}
                     {this.state.showEditPax === false && (paxInfo.poi_detail!=null && (paxInfo.poi_detail.visa_type==='' || paxInfo.poi_detail===null || paxInfo.poi_detail===undefined) || (paxInfo.poi_detail===null || paxInfo.poi_detail===undefined)) && (
                      <span>--</span>
                    )}
                  </td>
                </tr>
              </tbody>
            </table>
            <table className="dataTbl pax-tbl dataTable5">
            <thead><tr>
                <th width="27%">Airline </th>
                <th width="27%">Frequent flier number </th>
                <th width="33.3%">Applicable Airline </th>
              </tr></thead>
                <tbody>
                {paxInfo.frequent_flier_numbers.map((ffqNumber, index) => (
                <React.Fragment key={index}> 
                <tr>
                <td>
                    <span>
                      {this.state.showEditPax === true && (
                      <div className="custom-select-v3">
                      <div className="custom-select-v3"><Icon className="select-me" color="#CAD6E3" size={20} icon="down-arrow" />
                      <select name="airline" id="airline" value={ffqNumber.airline} onChange={this.handleChangeAirline(ffqNumber)}>
                      {airlines}
                      </select>
                      </div>
                      </div>
                      )}
                      {this.state.showEditPax === false &&  (
                        <span>{ffqNumber.airline}</span>
                      )}
                      {this.state.showEditPax === false && (paxInfo.frequent_flier_numbers!=null && paxInfo.frequent_flier_numbers[0]!=='' && paxInfo.frequent_flier_numbers[0]!==undefined && (paxInfo.frequent_flier_numbers[0].airline==='' || paxInfo.frequent_flier_numbers[0].airline===null || paxInfo.frequent_flier_numbers[0].airline===undefined) || (paxInfo.frequent_flier_numbers.length===0 || paxInfo.frequent_flier_numbers.length===undefined)) && (
                      <span>--</span>
                      )}
                    </span>
                  </td>
                  <td>
                    <span>{this.state.showEditPax === true &&  (
                       <div className="custom-select-v3">
                      <input type="text" id="ffn" name="ffn" value={ffqNumber.freq_flier_number} onChange={this.handleChangeFfpNum(ffqNumber)} />
                      </div>
                       )}
                    {this.state.showEditPax === false &&  (
                         <span>{ffqNumber.freq_flier_number}</span>
                      )}
                      {this.state.showEditPax === false && (paxInfo.frequent_flier_numbers!=null && paxInfo.frequent_flier_numbers[0]!=='' && paxInfo.frequent_flier_numbers[0]!==undefined && (paxInfo.frequent_flier_numbers[0].freq_flier_number==='' || paxInfo.frequent_flier_numbers[0].freq_flier_number===null || paxInfo.frequent_flier_numbers[0].freq_flier_number===undefined) || (paxInfo.frequent_flier_numbers.length===0 || paxInfo.frequent_flier_numbers.length===undefined)) && (
                      <span>--</span>
                      )}
                    </span>
                  </td>
                  <td>
                  {ffqNumber.applicable_airline!==undefined && ffqNumber.applicable_airline!=="" &&  (
                  <span>[{ffqNumber.applicable_airline}] {FlightService.getAirlineNameFromAirLineCode(this.props.airLineMaster,ffqNumber.applicable_airline)}</span>
                  )}
                  </td>
                </tr>
                </React.Fragment>
             ))}
              </tbody>
              </table>
            </div>
          </div>
        ))}
      </div>
        <div className="btnSec">
        {this.state.ftripsPermissions.PAX_EDIT_PERMSN_ENABLE && !this.state.showEditPax && (
                 <Button
                    size="xs"                    
                    type="primary"
                    id="editPax"
                    onClickBtn={this.handleEditPaxDetail}
                    btnTitle='Edit Pax'
                  >
                  </Button>
              )}
        {this.state.showEditPax && (
               <Button
                    size="xs"     
                    //type="default"  
                    className="btn-default"             
                    id="cancel"
                    onClickBtn={this.cancelPax}
                    btnTitle='Cancel'
                  >
                  </Button>
              )}
       {this.state.showEditPax && (
               <Button
                    size="xs"                    
                    type="primary"
                    id="savePax"
                    onClickBtn={this.savePaxDetails}
                    btnTitle='Update Pax'
                    loading = {this.state.loading}
                  >
                  </Button>
              )}
      </div></div>
    )
  }

}
const mapDispatchToProps = dispatch => {
  //console.log("mapDispatchToProps");
  return {
      updatePax: (req) => dispatch(actions.updatePaxDetails(req)),
      updateNotes:(req) => dispatch(actions.updateNoteDetails(req)),
  }
}
const mapStateToProps = state => {
  return {
      tripDetail: state.trpReducer.tripDetail,
      airLineMaster: state.trpReducer.airLineMaster,
      prevPaxInfos:state.trpReducer.tripDetail.air_bookings[0].pax_infos
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(FlightPax);


