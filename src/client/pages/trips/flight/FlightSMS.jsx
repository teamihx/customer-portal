import React, { Component } from 'react';
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import FlightService from '../../../services/trips/flight/FlightService.js'
import log from 'loglevel';
import DateUtils from 'Services/commonUtils/DateUtils.js';

class FlightSMS extends Component {
    constructor(props) {
        super(props)
        this.state = {
          tripDetail:this.props.tripDetail,
          mobile:'',
          smsData:'',
          showMessage:false,
          view_trip_link:'',
          cancel_trip_link:'',
          viewLinkAvail:false,
          short_link:''
        }
        this.state.mobile=this.props.getMobileNumber;
        this.sendTripSMS(this.state.mobile);
      }
    
createSMSData(){
        try{
            let newTripObj=this.props.tripDetail;
            var paxInfo=newTripObj.air_bookings[0].pax_infos;
            var data='';
            var flag=false;
            if(newTripObj.domain!==null && newTripObj.domain!==undefined){
            if(newTripObj.contact_detail.country_code===971 || newTripObj.domain==='cleartripforbusiness.ae'){
                flag=true;
            }
            }
           if(newTripObj.air_bookings[0].flights){
             if(newTripObj.air_bookings[0].flights.length!==0){
              for(let value of newTripObj.air_bookings[0].flights){
                var daparureDate=value.segments[0].departure_date_time;
                var airlineName=FlightService.getAirlineNameFromAirLineCode(this.props.airLineMaster,value.segments[0].operating_airline);
                var fltNumber=value.segments[0].flight_number;
                var airlinePnr=newTripObj.air_bookings[0].air_booking_infos[0].airline_pnr;
                if(data===''){
                data="Trip ID : "+newTripObj.trip_ref+"\n";
                if(paxInfo.length===1){
                    var paxName=this.props.tripDetail.contact_detail.first_name+" "+this.props.tripDetail.contact_detail.last_name;
                    data=data+paxName+"\n";
                }
                data=data+"-"+"\n";
                data=data+DateUtils.convertToDateFormate(daparureDate)+"\n";
                data=data+airlineName+","+fltNumber+"\n";
                data=data+"PNR : "+airlinePnr+"\n";
                data=data+"-"+"\n";
                }else{
                data=data+DateUtils.convertToDateFormate(daparureDate)+"\n";
                data=data+airlineName+","+fltNumber+"\n";
                data=data+"PNR : "+airlinePnr+"\n";
                }
               }
               //console.log("Send SMS Resp: " + this.state.view_trip_link);
               if(!flag && this.state.view_trip_link!=""){
                data=data+"\n";
                data=data+"View trip in app:"+this.state.view_trip_link+"\n";
               }
               if(!flag && this.state.cancel_trip_link!=""){
                data=data+"\n";
                data=data+"If your plans change:"+this.state.cancel_trip_link+"\n";
               }
               if(this.state.short_link!==""){
                data=data+"\n";
                data=data+"Cancellation link:"+this.state.short_link+"\n";
               }
               if(flag){
                data=data+"\n";
                data=data+"Download our free app:"+"\n";
                data=data+"IOS:"+bit.ly/cleartripios+"\n"; 
                data=data+"android:"+bit.ly/cleartripandroid+"\n";
               }
               this.state.smsData=data;
              }
            }
            
          }catch(err){
            this.setState({loading:false});
            log.error('Exception occured in FLight pax PaxDetails function---'+err);
          }
      }
      getFltShortViewLink(triRefNumber){
        try{
            FlightService.getSMSLink(triRefNumber).then(response => {
                const updatedRS = JSON.parse(response.data);
                if(updatedRS.status===200){
                    var linkUrl=updatedRS.data.url;
                    this.state.short_link=linkUrl;
                }else{
                    //console.log("Send SMS Link URL is not available: "+updatedRS.status);
                }
              }).catch(function (error) {
                if (error.response) {
                    var resp = '';
                    resp = {
                        status: '404',
                        msg:'Resource Not Found'
                        } 
                } 
                });
        }catch(err){
            log.error('Exception occured in Send SMS LINK function---'+err);
            }
      }
      sendFlightTripSMS(triRefNumber,mobileNumber){
        try{
            FlightService.getSMSLink(triRefNumber).then(response => {
                const updatedRS = JSON.parse(response.data);
                if(updatedRS.status===200){
                    var linkUrl=updatedRS.data.url;
                    this.state.view_trip_link=linkUrl;
                    FlightService.getFltSmsCancelLink(triRefNumber).then(response => {
                        const cancelRS = JSON.parse(response.data);
                        if(cancelRS.status===200){
                            var linkUrl=cancelRS.data.url;
                            this.state.cancel_trip_link=linkUrl;
                            this.createTripSMS(mobileNumber);
                           // console.log("Send SMS Cancel Link URL available: "+cancelRS.status);
                        }else{
                            this.createTripSMS(mobileNumber);
                            //console.log("Send SMS Cancel Link URL is not available: "+cancelRS.status);
                        }
                      }).catch(function (error) {
                        if (error.response) {
                            var resp = '';
                            resp = {
                                status: '404',
                                msg:'Cancel url not found'
                                } 
                        } 
                        });
                }else{
                    this.createTripSMS(mobileNumber);
                    //console.log("Send SMS Link URL is not available: "+updatedRS.status);
                }
              }).catch(function (error) {
                if (error.response) {
                    var resp = '';
                    resp = {
                        status: '404',
                        msg:'Resource Not Found'
                        } 
                } 
                });
        }catch(err){
            log.error('Exception occured in Send SMS LINK function---'+err);
        }
      }
      
      createTripSMS(mobileNumber){
        try{
        let newTripObj=this.props.tripDetail;
        this.createSMSData();
        const smsObj = {
        mobileNumber:mobileNumber,
        content:this.state.smsData,
        ccId:newTripObj.trip_ref,
        Tags:["test", "test-q"]
      }
      //console.log("Send SMS req : " + smsObj);
      FlightService.sendFlightSMS(smsObj).then(response => {
      const updatedRS = JSON.parse(response.data);
      if(updatedRS.status === 'success'){
        //console.log("Send SMS Resp: " + updatedRS.status);
      }
    }).catch(function (error) {
        if (error.response) {
            var resp = '';
            resp = {
                status: '404',
                msg:'Response Not Found'
                } 
        } 
        });
        }catch(err){
            log.error('Exception occured in Send SMS function---'+err);
       }
      }

sendTripSMS(mobileNumber){
    try{
      var CONFIRMATION_SMS_INCLUDE_DEEPLINKS='ON';
      let newTripObj=this.props.tripDetail;
      if(CONFIRMATION_SMS_INCLUDE_DEEPLINKS==='ON' && (newTripObj.txn_source_type==='B2C' || newTripObj.txn_source_type==='MOBILE' || newTripObj.txn_source_type==='CORP' || newTripObj.domain==='cleartrip.com')){
        this.sendFlightTripSMS(newTripObj.trip_ref,mobileNumber);
    }else{
        if(newTripObj.txn_source_type==='B2C' || newTripObj.txn_source_type==='MOBILE'){
        this.getFltShortViewLink(newTripObj.trip_ref);
        }
        this.createSMSData();
        const smsObj = {
        mobileNumber:mobileNumber,
        content:this.state.smsData,
        ccId:newTripObj.trip_ref,
        Tags:["test", "test-q"]
      }
      //console.log("sendTripSMS req : " + smsObj);
      FlightService.sendFlightSMS(smsObj).then(response => {
        const updatedRS = JSON.parse(response.data);
        if(updatedRS.status === 'success'){
         // console.log("Send SMS Resp: " + updatedRS.status);
        }
      }).catch(function (error) {
          if (error.response) {
              var resp = '';
              resp = {
                  status: '404',
                  msg:'Response Not Found'
                  } 
          } 
          });
    } 
}catch(err){
    log.error('Exception occured in Send SMS function---'+err);
    }
}
render() {
    return (
      <>
           <ul className="mb-20">
           <li className="pl-20 ">
            {this.state.showMessage && <div className="alert alert-error">
            <Icon className="noticicon" color="#F4675F" size={16} icon="warning"/> Enter Mobile number</div>}
              </li>
            </ul>
           </>
    );
  }
}
const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail,
      airLineMaster: state.trpReducer.airLineMaster
    };
  }
export default connect(mapStateToProps, null)(FlightSMS);