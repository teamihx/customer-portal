import React, { Component } from 'react';
import { connect } from 'react-redux';
import log from 'loglevel';
import utility from '../../common/Utilities'
import ShowHide from 'Components/ShowHide.jsx';
import * as actions from '../../../store/actions/index';
import { json } from 'body-parser';

class FlightAmendmentPricingDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
         pricingDetail:{}
        }
        
    }
    isEmpty = (obj) => {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }
      loadPricingDetail=(tripDetail)=>{
          
        
        
        if(!this.isEmpty(tripDetail)){
          let txn_id=null
          let pmnt_data={}
          for(let pmnt of tripDetail.payments_service_data){
            
            if(pmnt.payment_category==='A' && pmnt.status==='S'){
              txn_id=pmnt.txn_id;
              pmnt_data={...pmnt}
            }
          }
            let total_fee_airl_amd=0;
            let total_fee_amd=0;
            let total_fee_cncl=0;
            let total_fare=0
            for(let pricinObj of tripDetail.air_bookings[0].pricing_objects){
              if(pricinObj.txn_id===txn_id && pricinObj.amd_status==='A'){
                total_fee_airl_amd+=pricinObj.total_fee_airl_amd
                total_fee_amd+=pricinObj.total_fee_amd
                total_fee_cncl+=pricinObj.total_fee_cncl
                total_fare+=pricinObj.total_fare
              }
            }
            let total_amd=total_fee_airl_amd+total_fee_amd+total_fee_cncl
            let new_fare=total_fare-total_amd
            let paid_earlier=new_fare+total_amd-pmnt_data.amount
            let difference=pmnt_data.amount
            let amdPrice={}
             amdPrice={
              total_amd:total_amd,
              new_fare:new_fare,
              paid_earlier:paid_earlier,
              difference:difference,
              currency:tripDetail.currency==='INR'?'Rs':tripDetail.currency
            }
            this.setState({
              pricingDetail: {
                     ...this.state.pricingDetail,
                     ...amdPrice
                    }
         });
           //this.state.pricingDetail={...amdPrice}
          // console.log("pricingDetail==="+JSON.stringify(this.state.pricingDetail))
            //console.log("pridcscingDetail==="+JSON.stringify())
            //this.setState({pricingDetail:amdPrice},()=>console.log("pricingDetail==="+JSON.stringify(this.state.pricingDetail)))
          
        }
      }
    componentDidMount(){
      this.loadPricingDetail(this.props.tripDetail)
    }
    amendmentAvlble=()=>{
      let pmntList = this.props.tripDetail.payments_service_data;
      
      for(let pmnt of pmntList){
        if(pmnt.status==='S' && pmnt.payment_category==='A' ){
            return true;
        }
      }
      return false;
    }
render() {
    //{this.loadPricingDetail(this.props.tripDetail)}
    return (
      <>{!this.isEmpty(this.state.pricingDetail)?
     this.amendmentAvlble()? <div className="side-pnl">
    
        <ShowHide title='Amendment Pricing Details'>
        <div className="showHide-content">
                <ul className="price-info">
                 <li><span>Paid Earlier</span><span>{utility.PriceFormat(this.state.pricingDetail.paid_earlier,this.state.pricingDetail.currency)}</span></li>
                 <li><span>New Fare</span>  {utility.PriceFormat(this.state.pricingDetail.new_fare,this.state.pricingDetail.currency)}</li>
                 <li><span>Amendment Fee</span>{utility.PriceFormat(this.state.pricingDetail.total_amd,this.state.pricingDetail.currency)}</li>
                 
                 <li className="total"><span>Difference</span> <span> {utility.PriceFormat(this.state.pricingDetail.difference,this.state.pricingDetail.currency)}</span></li>   
                </ul>
                </div>
                </ShowHide>
                </div>:<div><ShowHide title='Amendment Pricing Details'>
                <div className="showHide-content">
                <ul className="price-info">
                 <li><span>Not Available</span><span>Old itinerary is not yet refunded</span></li>
                   
                </ul>
                </div>
                  </ShowHide></div>:null}
               
           </>
    );
  }
}
const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail,
      //airLineMaster: state.trpReducer.airLineMaster
    };
  }
  const mapDispatchToProps = dispatch => {
    return {
        
        onInitPricingObject: (req) => dispatch(actions.initPricingObject(req))
        
        
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FlightAmendmentPricingDetail);