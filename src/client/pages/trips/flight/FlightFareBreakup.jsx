import React, { Component } from 'react'
import Popup from "reactjs-popup";
import { Link } from 'react-router-dom'
import utility from '../../common/Utilities'

class FlightFareBreakup extends Component {
    constructor(props) {
      super(props);
      this.state = { open: false };
      this.openModal = this.openModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
      //  console.log('open'+this.state.open)
      this.setState({ open: true });
    }
    closeModal() {
       // console.log('close'+this.state.open)
      this.setState({ open: false });
    }
  
    render() {
     // console.log("total_base_fare"+ typeof this.props.data.total_base_fare)
      return (
        <div>
          
          <Popup
          trigger={<a>
          {utility.PriceFormat(parseInt(this.props.data.total_base_fare,10)+parseInt(this.props.data.total_tax,10)-parseInt(this.props.data.total_tax_svc,10),this.props.data.currency)}
          </a>}
            open={this.state.open}y
            closeOnDocumentClick
            onClose={this.closeModal}
            position="bottom right"
          >
            <div className="modal pop-grid">
              {/* <a className="close" onClick={this.closeModal}>
                &times;
              </a> */}
              <ul>
             {this.props.data.total_base_fare!==0?  <li><span>Base fare </span> {utility.PriceFormat(parseInt(this.props.data.total_base_fare,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_markup!==0?   <li><span>Service Fee</span> {utility.PriceFormat(parseInt(this.props.data.total_markup,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_tax_yr!==0?      <li><span>Congestion fee</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_yr,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_tax_yq!==0?     <li><span>Airline fuel charge</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_yq,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_tax_unknown!==0?      <li><span>Total Tax Unknown</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_unknown,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_tax_psf!==0?      <li><span>Pax Service Fee</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_psf,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_tax_svc!==0?      <li><span>Service Tax</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_svc,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_tax_jn!==0?      <li><span>Govt. Service Tax</span>  {utility.PriceFormat(parseInt(this.props.data.total_tax_jn,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_tax_cute!==0?     <li><span>Cute Fee</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_cute,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_discount!==0?      <li><span>Discount(-)</span> {utility.PriceFormat(-1*parseInt(this.props.data.total_discount,10),this.props.data.currency)}</li>:null}
             {this.props.data.total_cashback!==0?     <li><span>Cashback(-)</span> {utility.PriceFormat(-1*parseInt(this.props.data.total_cashback,10),this.props.data.currency)}</li>:null}
                  <li className="total"><span>total </span>{utility.PriceFormat(parseInt(this.props.data.total_fare,10) - parseInt(this.props.data.total_nc_fee,10)- parseInt(this.props.data.total_nc_fee,10)-parseInt(this.props.data.total_fee_con,10)-parseInt(this.props.data.total_fee_pgc,10),this.props.data.currency)}</li>
              </ul>
            </div>
          </Popup>
        </div>
      );
    }
  }
  export default FlightFareBreakup;