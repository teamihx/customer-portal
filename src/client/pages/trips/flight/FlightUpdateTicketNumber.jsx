import React, { Component } from 'react'
import Button from 'Components/buttons/button.jsx'
import CommonMsg from "Components/commonMessages/CommonMsg.jsx";
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import FlightService from '../../../services/trips/flight/FlightService.js';
import NoteService from '../../../services/trips/note/NoteService.js'
import NumberUtils from '../../../services/commonUtils/NumberUtils.js';
import DateUtils from '../../../services/commonUtils/DateUtils.js';
import Notification from 'Components/Notification.jsx'; 
import log from 'loglevel';
import * as actions from '../../../store/actions/index'
export const USER_AUTH_DATA = 'userAuthData';
export const TRIPS_ROLES = 'tripsRoles';
class FlightUpdateTicketNumber extends Component {

    constructor(props) {
        super(props)
       
        this.state = {

            responseData: this.props.tripDetail,            
            ticketDetails:'',
            tktDtlsList:[],
            responseDatabkp:'',
            loading:false,
            updateTktRes:'',
            sucessmsg:'',
            errormsg:'',
            ticketTxn:'',
            opentxnMsg:'',
            txns:this.props.tripDetail.txns,
            updatedTxns:'',
            publicIp:'',
            tripNotes:this.props.tripDetail.notes,   
            ftripsPermissions:''
          
        }
        
        this.gdspnrhandleChangeEvent = this.gdspnrhandleChangeEvent.bind(this);
        this.airlinepnrhandleChangeEvent = this.airlinepnrhandleChangeEvent.bind(this);
        this.ticketnumberhandleChangeEvent = this.ticketnumberhandleChangeEvent.bind(this);
        this.agentpcchandleChangeEvent = this.agentpcchandleChangeEvent.bind(this);
        this.updateAirTktDetails = this.updateAirTktDetails.bind(this);
        this.resetOldDetails = this.resetOldDetails.bind(this);
        this.paxdetailsDisplay = this.paxdetailsDisplay.bind(this);
        this.findClass = this.findClass.bind(this);
        this.loadTicketDetails();

        //Getting ROLES DATA from local storage
       let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
       this.state.ftripsPermissions=tripsObj.ftripPermissions;
        
    }




    loadTicketDetails=()=>{

      if(this.state.responseData!==undefined){
        let txnDetails=this.state.responseData.txns;
        for(let txn in txnDetails){
          let txnData=txnDetails[txn];

          if(txnData.status!==null && txnData.status!=='' && txnData.status==='O'){
            this.state.opentxnMsg="Since some txns in this trip are open you cannot update any PNR's now.";

          }
        }

      }
     

      if(this.state.opentxnMsg===null || this.state.opentxnMsg===undefined || this.state.opentxnMsg===''){
        this.state.tktDtlsList =[];
        let tktDetData=[];
        var tempTripDtls = this.state.responseData;
        this.state.responseDatabkp=this.state.responseData;
        try{
       //console.log('Complete Trip Response is----'+JSON.stringify(this.state.responseData));
        var airbookingInfo = tempTripDtls.air_bookings[0].air_booking_infos;
        var paxList = tempTripDtls.air_bookings[0].pax_infos;
        var flights = tempTripDtls.air_bookings[0].flights;
        var currency =tempTripDtls.currency;
        let paxdetails=tempTripDtls.travellers
        let bkngstatus=tempTripDtls.air_bookings[0].booking_status

        let noofPax=this.paxdetailsDisplay(paxdetails);
        if(noofPax===undefined && noofPax===''){
          noofPax=paxdetails;

        }
       
        
        if(paxList){
        for(let pax of paxList){
            var paxDtl = new Object(); 
            var segList =[];
            paxDtl.pax_info_id=pax.id
            paxDtl.title =pax.title;
            paxDtl.pax_type_code =pax.pax_type_code;
            paxDtl.first_name =pax.first_name;
            paxDtl.last_name =pax.last_name;
            paxDtl.totalAmt =pax.total_fare;
            if((pax.pax_type_code!==undefined && pax.pax_type_code!=='' ) 
            && (pax.pax_type_code.toUpperCase()!=='ADT')){
            paxDtl.dob=pax.date_of_birth;
            }
           

            if(airbookingInfo){
              var airbookingInfoList =  airbookingInfo.filter(air => air.pax_info_id === pax.id);
            if(airbookingInfoList){
                for(let flt of airbookingInfoList){
                   var fltdtls = new Object();
                   fltdtls.id=flt.id
                   fltdtls.segment_id=flt.segment_id;
                   fltdtls.pax_info_id=flt.pax_info_id;
                   fltdtls.agent_pcc = (flt.agent_pcc!==null?flt.agent_pcc:'');
                   fltdtls.airline_pnr = (flt.airline_pnr!==null ? flt.airline_pnr:'');
                   fltdtls.ticket_number = (flt.ticket_number !==null?flt.ticket_number:'');
                   fltdtls.gds_pnr = (flt.gds_pnr!==null ? flt.gds_pnr:'');
                   fltdtls.is_changed=false;
                   
                   if(flights){
                       for(let seg of flights){
                        var dat = seg.segments.filter(segmnt => segmnt.id === flt.segment_id);
                     if(dat != '' && dat != 'undefined'){
                        fltdtls.sector= dat[0].departure_airport+"-"+dat[0].arrival_airport;
                      }
                    }
                   }
                   segList.push(fltdtls);
                }
                paxDtl.segList = segList;
                this.state.tktDtlsList.push(paxDtl);  
                tktDetData.push(paxDtl);
            }
            }
        }
    }
    this.tktData = {
        no_of_pax: noofPax,
        bkng_status: bkngstatus,
        currency:currency,
        tkt_Dtls_List:tktDetData
      };
      this.state.ticketDetails=this.tktData;
      //console.log('Constructed Json is----'+JSON.stringify(this.state.ticketDetails));
    }catch(err){

    log.error('Exception occured in FlightUpdateTicketNumber Component loadTicketDetails function---'+err);

    }
  }
    }

    paxdetailsDisplay(noofPax1){
      let paxData='';
      try{
      noofPax1=noofPax1.replace(/\--- | /g, "");
      noofPax1=noofPax1.replace(/:/g,'');
      let paxDetails=noofPax1.replace(/\s/g, "-").split("-");
      let adt='';
      let chd='';
      let inf='';

       for(let itm of paxDetails) {
          if(itm.slice(0,3).toLowerCase().trim()==='adt'){
          adt = itm.slice(3,4)+" adult ";
          }
        else if(itm.slice(0,3).toLowerCase().trim()==='chd'){
        chd= " "+itm.slice(3,4)+" child";
        }
        else if(itm.slice(0,3).toLowerCase().trim()==='inf'){
        inf = " "+itm.slice(3,4)+" infant"; 
        }
        } 

        if(adt!==undefined && adt!==''){

          paxData=paxData+adt;

        }if(chd!==undefined && chd!==''){

          if(inf!==undefined && inf!==''){
            paxData=paxData+", "+chd;

          }else{
            paxData=paxData+" and "+chd;
          }
          

        }if(inf!==undefined && inf!==''){

          paxData=paxData+" and "+inf;
        }


      }catch(err){
       
        log.error('Exception occured in FlightUpdateTicketNumber Component paxdetailsDisplay function---'+err);

        return paxData;
      }
      return paxData;
    }


    
    gdspnrhandleChangeEvent = idx => evt => {
      
        let newFlyerDtls = '';
        let htlDtlListt=[];
        let noofpax=this.state.ticketDetails.no_of_pax;
        let bkngstatus=this.state.ticketDetails.bkng_status;
        let currency=this.state.ticketDetails.currency;
        this.setState({ errormsg: '',sucessmsg:'' });
        try{
          this.state.ticketDetails.tkt_Dtls_List.map((flyerDtl, sidx) => {
              var tktData = new Object(); 
              tktData.pax_info_id=flyerDtl.pax_info_id
              tktData.title=flyerDtl.title
              tktData.pax_type_code=flyerDtl.pax_type_code
              tktData.first_name=flyerDtl.first_name
              tktData.last_name=flyerDtl.last_name
              tktData.totalAmt=flyerDtl.totalAmt
  
            newFlyerDtls=flyerDtl.segList.map( (tkt, idx1) => {
              
     
               if (idx.segment_id !== tkt.segment_id || idx.pax_info_id !== tkt.pax_info_id){
                 return tkt;  
                  
               }else {
                
                return { 
                  ...tkt, 
                  
                  gds_pnr: evt.target.value,
                  is_changed:true
                
                };   
               }
              
  
            })
            tktData.segList = newFlyerDtls;
            htlDtlListt.push(tktData);
            return htlDtlListt;
           });
  
            this.gsdtktData = {
            no_of_pax: noofpax,
            bkng_status: bkngstatus,
            currency:currency,
            tkt_Dtls_List:htlDtlListt
          };
          this.state.ticketDetails=this.gsdtktData; 
          this.forceUpdate();
          
         // console.log('gds_pnr is----'+JSON.stringify(this.state.ticketDetails));
           
       
    }catch(err){
  
        log.error('Exception occured in FlightUpdateTicketNumber Component gdspnrhandleChangeEvent function---'+err);
  
    }
    }
  
    airlinepnrhandleChangeEvent = idx => evt => {
        
      let newFlyerDtls = '';
      let htlDtlListt=[];
      let noofpax=this.state.ticketDetails.no_of_pax;
      let bkngstatus=this.state.ticketDetails.bkng_status;
      let currency=this.state.ticketDetails.currency;
      this.setState({ errormsg: '',sucessmsg:'' });
      try{
        this.state.ticketDetails.tkt_Dtls_List.map((flyerDtl, sidx) => {
            var tktData = new Object(); 
            tktData.pax_info_id=flyerDtl.pax_info_id
            tktData.title=flyerDtl.title
            tktData.pax_type_code=flyerDtl.pax_type_code
            tktData.first_name=flyerDtl.first_name
            tktData.last_name=flyerDtl.last_name
            tktData.totalAmt=flyerDtl.totalAmt
  
          newFlyerDtls=flyerDtl.segList.map( (tkt, idx1) => {
            
   
             if (idx.segment_id !== tkt.segment_id || idx.pax_info_id !== tkt.pax_info_id){
               return tkt;  
                
             }else {
              
              return { 
                ...tkt, 
                
                airline_pnr: evt.target.value,
                is_changed:true
                
              
              };   
             }          
  
          })
          tktData.segList = newFlyerDtls;
          htlDtlListt.push(tktData);
          return htlDtlListt;
         });
  
          this.airpnrtktData = {
          no_of_pax: noofpax,
          bkng_status: bkngstatus,
          currency:currency,
          tkt_Dtls_List:htlDtlListt
        };
        this.state.ticketDetails=this.airpnrtktData; 
        this.forceUpdate();
        
        //console.log('ariline_pnr is----'+JSON.stringify(this.state.ticketDetails));
         
     
  }catch(err){
  
    log.error('Exception occured in FlightUpdateTicketNumber Component airlinepnrhandleChangeEvent function---'+err);
  
  }
  }
  
  ticketnumberhandleChangeEvent = idx => evt => {
        
    let newFlyerDtls = '';
    let htlDtlListt=[];
    let noofpax=this.state.ticketDetails.no_of_pax;
    let bkngstatus=this.state.ticketDetails.bkng_status;
    let currency=this.state.ticketDetails.currency;
    this.setState({ errormsg: '',sucessmsg:'' });
    try{
      this.state.ticketDetails.tkt_Dtls_List.map((flyerDtl, sidx) => {
          var tktData = new Object(); 
          tktData.pax_info_id=flyerDtl.pax_info_id
          tktData.title=flyerDtl.title
          tktData.pax_type_code=flyerDtl.pax_type_code
          tktData.first_name=flyerDtl.first_name
          tktData.last_name=flyerDtl.last_name
          tktData.totalAmt=flyerDtl.totalAmt
  
        newFlyerDtls=flyerDtl.segList.map( (tkt, idx1) => {
          
  
           if (idx.segment_id !== tkt.segment_id || idx.pax_info_id !== tkt.pax_info_id){
             return tkt;  
              
           }else {
            
            return { 
              ...tkt, 
              
              ticket_number: evt.target.value,
              is_changed:true
              
            
            };   
           }          
  
        })
        tktData.segList = newFlyerDtls;
        htlDtlListt.push(tktData);
        return htlDtlListt;
       });
  
        this.tktnumberData = {
        no_of_pax: noofpax,
        bkng_status: bkngstatus,
        currency:currency,
        tkt_Dtls_List:htlDtlListt
      };
      this.state.ticketDetails=this.tktnumberData; 
      this.forceUpdate();
      
     // console.log('ticket_number is----'+JSON.stringify(this.state.ticketDetails));
       
   
  }catch(err){
  
    log.error('Exception occured in FlightUpdateTicketNumber Component ticketnumberhandleChangeEvent function---'+err);
  
  }
  }
  
  agentpcchandleChangeEvent = idx => evt => {
        
    let newFlyerDtls = '';
    let htlDtlListt=[];
    let noofpax=this.state.ticketDetails.no_of_pax;
    let bkngstatus=this.state.ticketDetails.bkng_status;
    let currency=this.state.ticketDetails.currency;
    this.setState({ errormsg: '',sucessmsg:'' });
    try{
      this.state.ticketDetails.tkt_Dtls_List.map((flyerDtl, sidx) => {
          var tktData = new Object(); 
          tktData.pax_info_id=flyerDtl.pax_info_id
          tktData.title=flyerDtl.title
          tktData.pax_type_code=flyerDtl.pax_type_code
          tktData.first_name=flyerDtl.first_name
          tktData.last_name=flyerDtl.last_name
          tktData.totalAmt=flyerDtl.totalAmt
  
        newFlyerDtls=flyerDtl.segList.map( (tkt, idx1) => {
          
  
           if (idx.segment_id !== tkt.segment_id || idx.pax_info_id !== tkt.pax_info_id){
             return tkt;  
              
           }else {
            
            return { 
              ...tkt, 
              
              agent_pcc: evt.target.value,
              is_changed:true
              
            
            };   
           }          
  
        })
        tktData.segList = newFlyerDtls;
        htlDtlListt.push(tktData);
        return htlDtlListt;
       });
  
        this.agentpccData = {
        no_of_pax: noofpax,
        bkng_status: bkngstatus,
        currency:currency,
        tkt_Dtls_List:htlDtlListt
      };
      this.state.ticketDetails=this.agentpccData; 
      this.forceUpdate();    
      
      //console.log('agent_pcc is----'+JSON.stringify(this.state.ticketDetails));     
   
  }catch(err){
  
    log.error('Exception occured in FlightUpdateTicketNumber Component agentpcchandleChangeEvent function---'+err);
  
  }
  }
 
  
   async updateAirTktDetails(event){

      try{
        this.setState({updateAirTkt:true}); 
        await this.fetchPublicIp();
        
        let airbookingInfo = this.state.responseData.air_bookings[0].air_booking_infos;
        let data='';
        let airbknginfo=[];
  
        airbookingInfo.map((flyerDtl, sidx) => {     
  
         this.state.ticketDetails.tkt_Dtls_List.map( (tkt, idx1) => {
            data=tkt.segList.map((seg, id1)=>{
              if(seg.id===flyerDtl.id){
                return { 
                  ...flyerDtl,               
                  agent_pcc:seg.agent_pcc,
                  airline_pnr:seg.airline_pnr,
                  gds_pnr: seg.gds_pnr,
                  ticket_number:seg.ticket_number
  
                }; 
              }            
            });
            
            let filteredData = data.filter(function (dat) {
              return dat != null;
            });
            Array.prototype.push.apply(airbknginfo, filteredData)
          })
       });
  
       this.airinfoDataReq = {
        air_booking_infos:airbknginfo
      };
  
     // console.log('this.airinfoData;----'+JSON.stringify(this.airinfoDataReq));
        //this.updateTicketReq(this.airinfoDataReq);
        this.createUpdatTicketTransaction(this.airinfoDataReq);
        this.setState({loading:false});
  
      }catch(err){
        this.setState({updateAirTkt:false}); 
        log.error('Exception occured in FlightUpdateTicketNumber Component updateAirTktDetails function---'+err);
  
      }
  
  
    }

    createUpdatTicketTransaction(airinfoDataReq){
    
      let authData= localStorage.getItem(USER_AUTH_DATA);
      let obj = JSON.parse(authData);      
      let txnObj = new Object();      
      txnObj.status='O';
      txnObj.trip_id=this.state.responseData.id;
      txnObj.txn_type=41;
      txnObj.user_id=obj.data.id;
      txnObj.source_type='HQ',
      txnObj.misc='',
      txnObj.emails=[],
      txnObj.source_id=this.state.publicIp;
      let txnArr=[txnObj];          
      const txnReq={
          txns:[...txnArr]
      }

      //console.log('createUpdatTicketTransaction req is--'+JSON.stringify(txnReq));
      FlightService.createNewTransaction(txnReq).then(response => {
      //console.log('createNewTransaction res '+JSON.stringify(response));
      try{
        
        let data1=JSON.parse(response.data);
        if(data1.status===200){
        let data=JSON.parse(response.data);
       // console.log('FlightService modelsUpdated res '+JSON.stringify(data.modelsUpdated.txns));
        this.state.ticketTxn=data.modelsUpdated.txns;
       
        
        this.setState({         
           updatedTxns:this.state.txns.concat(data.modelsUpdated.txns).sort((a,b)=>
               ( new Date(a.created_at).getTime() - new Date(b.created_at).getTime())
           ).reverse()
           
       })

       //this.props.tripDetail.txns=this.state.updatedTxns
       //this.props.updateTxns(this.props.tripDetail);
       this.createNote();
       //this.buildNoteDetails();
       this.updateTicketReq(airinfoDataReq);
        }else{
          this.setState({ errormsg: "Not able to update the Ticket details now. Please try again later", showAlert: true,updateAirTkt:false })
          this.forceUpdate();        
          window.scrollTo(0, 0);
        }
      }catch(err){
        this.setState({updateAirTkt:false}); 
        log.error('Exception occured in FlightUpdateTicketNumber Component createUpdatTicketTransaction function---'+err);
  
      }

      });

    
      
}

closeUpdatTicketTransaction(){

  
  let authDat= localStorage.getItem(USER_AUTH_DATA);
  let obj1 = JSON.parse(authDat);
  let txnObj = new Object();
  
  txnObj.id=this.state.ticketTxn[0].id;  
  txnObj.status='C';
  txnObj.trip_id=this.state.responseData.id;
  txnObj.txn_type=41;
  txnObj.user_id=obj1.data.id;
  txnObj.source_type='HQ',
  txnObj.misc='',
  txnObj.emails=[]
  let txnArr=[txnObj];          
  const txnReq={
      txns:[...txnArr]
  }
  
  FlightService.createNewTransaction(txnReq).then(response => {

    try{
    //console.log('FlightUpdateTicketNumber closeUpdatTicketTransaction entered '+JSON.stringify(response));
    let data1=JSON.parse(response.data);
    
    if(data1.responsecode===200){
    let data=JSON.parse(response.data);   
    this.state.ticketTxn=data.modelsUpdated.txns;
    this.setState({         
      updatedTxns:this.state.txns.concat(data.modelsUpdated.txns).sort((a,b)=>
          ( new Date(a.created_at).getTime() - new Date(b.created_at).getTime())
      ).reverse()
      
  })
  this.props.tripDetail.txns=this.state.updatedTxns;
  this.props.updateTxns(this.props.tripDetail);
    
    }
  }catch(err){

    log.error('Exception occured in FlightUpdateTicketNumber Component closeUpdatTicketTransaction function---'+err);
  
  }
  });


  
}   

       fetchPublicIp() {
        //console.log('fetchPublicIp-');

        try{
        return new Promise((resolve, reject) => {   
          let txnReq ='';      
          FlightService.getpublicIp(txnReq).then(response => {
           // console.log('this.state.publicIp respo--'+JSON.stringify(response));
            if(response.status===200){
    
            this.state.publicIp=response.data.ip_address;
            //console.log('this.state.publicIp--'+JSON.stringify(this.state.publicIp));
            resolve (response.data.ip_address);
    
            }
          }).catch((error) => {
            assert.isNotOk(error,'Promise error');
            reject(error);
          });
        })
      }catch(err){
        reject(err);
        log.error('Exception occured in FlightUpdateTicketNumber Component closeUpdatT'+err);

      }
      }


      /* fetchPublicIp = async () => {
        console.log('fetchPublicIp--entered');
        let txnReq='';
        await FlightService.getpublicIp(txnReq)
        .then(response => {
          if(response.status===200){
  
            this.state.publicIp=response.data.ip_address;
            console.log('this.state.publicIp--'+JSON.stringify(this.state.publicIp));
    
            }

             }).catch(function (error) {
            console.log('the error is '+error);
            
            });
      } */

      
      createNote(){

        try{
          let noteData=[];

          noteData=this.buildNoteDetails();

     //console.log('createNote is---'+JSON.stringify(noteData))


          if(noteData!==null && noteData!==undefined && noteData!==''){

            //console.log('createNote is---'+noteData.length)
            
            let authDat= localStorage.getItem(USER_AUTH_DATA);
            let obj1 = JSON.parse(authDat);
            let userid=obj1.data.id;

            for(var key in noteData){

              let note=noteData[key]

               const noteTobeAdded={
                notes :[
                    {
                    note: note,
                    parent_note_id: null,
                    subject:'Trip Details : ',
                    user_id:userid,
                    trip_id:this.state.responseData.id,
                    created_at:new Date().toUTCString()

                    } 
                ]   
            }; 
           // console.log( 'note request from Fliup------'+JSON.stringify(noteTobeAdded));
            NoteService.updatetripNotes(noteTobeAdded)
            .then(response =>{
               // console.log("updated response===="+JSON.stringify(response));
                
               const data = JSON.parse(response.data)
               const {updateStatus} = data
               this.forceUpdate();
                 if(updateStatus==="true"){
                      //console.log('response from json {response.notes}'+response.data)
                       this.updateTripNotes(response.data);
                       this.forceUpdate();
                } 
               
              }).catch(error => this.setState({ error }));
            }
        
            
                
        
      }
        
    }catch(err){
        
      log.error('Exception occured in FlightUpdateTicketNumber createNote function---'+err);
       
      }
}


buildNoteDetails(){
let noteData=[];
//console.log('construct data---'+JSON.stringify(this.state.ticketDetails));
  try{
 this.state.ticketDetails.tkt_Dtls_List.map( (tkt, idx1) => {
 
  let data=1; 
  let finalNote='';
  let funcName='';
  let paxname='';
  let sector='';  
  tkt.segList.map((seg, id1)=>{
    
      
    if(seg.is_changed===true){

      if(Number(data)==1){
    funcName='Updated Ticket Number for '
    paxname=tkt.first_name+" "+tkt.last_name;                    
    
      }
      sector=seg.sector+","+sector;
      data=data+1;
     // console.log('data is==='+data);
      
    }  
         
  });
  
  if(funcName!==undefined && funcName!==''){ 
    finalNote=funcName+' '+paxname+' :  '+sector;
    noteData.push(finalNote); 
    } 
 //console.log('final note is---'+JSON.stringify(noteData));
 
}) 

  }catch(err){
    log.error('Exception occured in FlightUpdateTicketNumber buildNoteDetails function---'+err);
  }

  return noteData;
}


updateTripNotes(noteTobeAdded){
 //console.log("updateTripNotes==== entered")

  let noteData=JSON.parse(noteTobeAdded);
  let noteDetails=noteData.modelsUpdated.notes;
 this.setState({
    
     tripNotes:this.state.tripNotes.concat(noteDetails).sort((a,b)=>
         ( new Date(a.created_at).getTime() - new Date(b.created_at).getTime())
     ).reverse(),
     newNote:''
     
 })
 this.props.tripDetail.notes=this.state.tripNotes
 this.props.updateNotes(this.props.tripDetail);
 //console.log("updateTripNotes==== end")
 

}
      
  
      updateTicketReq(updateTktReq){

      //console.log('FlightService updateTicketReq entered ');

  
      FlightService.updateTicketDetails(updateTktReq).then(response => {
  
        try{
        this.state.updateTktRes = JSON.parse(response.data);
        //console.log("upDateTicketDetails Res " + JSON.stringify(this.state.updateTktRes.responsecode));
        if (this.state.updateTktRes !== undefined &&
           this.state.updateTktRes !== "" &&
           this.state.updateTktRes.responsecode !== undefined &&
           this.state.updateTktRes.responsecode !== "" &&
           this.state.updateTktRes.responsecode === 200) {
           // console.log("trip update"+ JSON.stringify(updateTktReq))
           this.closeUpdatTicketTransaction();
           this.props.tripDetail.air_bookings[0].air_booking_infos=[...updateTktReq.air_booking_infos];
         // console.log("trip detail"+ this.props.tripDetail.air_bookings[0])
           this.props.updateTickets(this.props.tripDetail);
          
          const msg='Ticket details updated successfully'          
          this.setState({ sucessmsg: msg,showAlert: true ,updateAirTkt:false});
          this.forceUpdate();
          window.scrollTo(0, 0);     
           
        
        } else {
          this.props.tripDetail.txns=this.state.updatedTxns
          this.props.updateTxns(this.props.tripDetail);
          this.setState({ errormsg: "Ticket details not updated ",updateAirTkt:false,showAlert: true })
          this.forceUpdate();        
          window.scrollTo(0, 0);
          
        }
      }catch(err){
        this.setState({updateAirTkt:false});
        this.forceUpdate();
        log.error('Exception occured in FlightUpdateTicketNumber updateTicketReq function---'+err);
    
      }
      });
    }

    resetOldDetails(e){
     // console.log('resetOldDetails---')
      try{
        this.loadTicketDetails();
        this.setState({ errormsg: "Ticket details has been reset! ", showAlert: true })
        this.forceUpdate();
        window.scrollTo(0, 0);
      }catch(err){
        log.error('Exception occured in FlightUpdateTicketNumber resetOldDetails function---'+err);
      }

    }

    findClass(status){

      if(status==="Q" || status==="H" ||status==="K" ){
        return "des cancel";

      }else{
        return  "des";
      }
    }
    toggleAlert = () => {
      this.setState({
        showAlert: false
      });
    };
      
      render() {
        let viewType, messageText;
        if (this.state.errormsg !== "" && this.state.errormsg !== undefined) {
          viewType = "error";
          messageText = this.state.errormsg;
        } else if (this.state.sucessmsg !== "" && this.state.sucessmsg !== undefined) {
          viewType = "success";
          messageText = this.state.sucessmsg;
        }
        const currency=(this.state.ticketDetails.currency==="INR" ? "Rs" : this.state.ticketDetails.currency);
        const paxCount=this.state.ticketDetails.no_of_pax
          
          return (
            <>
            {viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}
           { (this.state.opentxnMsg===null || this.state.opentxnMsg===undefined || this.state.opentxnMsg==='')?(
            
              <>

              <div>
             <h4 className="in-tabTTl-sub">Flight booking details - <span className="t-color3 font-14">{paxCount}</span></h4> 
  
  
              {this.state.ticketDetails.bkng_status.toUpperCase() === "P" ? (      
  
            <>
              {this.state.ticketDetails.tkt_Dtls_List.map((pax, index) => (
                <div key={index}>
                <div className="highInfo">{pax.title} {pax.first_name}  {pax.last_name} {(pax.dob!==undefined && pax.dob!=='') &&  "(DOB : "+DateUtils.convertStringdateFormat(pax.dob)+")" }    <span className="t-color3 font-12">{currency} {NumberUtils.numberFormat(pax.totalAmt)}</span></div>
                  <div className ='resTable'>
                  <table className="dataTbl pax-tbl dataTable5">
                    <thead><tr key={index}>
                      <th width="15%">Sector</th>
                      <th width="10%">GDS PNR</th>
                      <th width="10%">Airline PNR</th>
                      <th width="10%">Ticket #</th>
                      <th width="15%"> Pcc</th>
                     
                    </tr></thead>
                    <tbody>
                    {pax.segList.map((tkt, idx) => (
                      <tr key={idx}>
                       <td>
                        {tkt.sector}
                       </td>
                        <td>
                         <input type="text" id='id1'  name="gds_pnr" value={tkt.gds_pnr.trim()} onChange={this.gdspnrhandleChangeEvent(tkt)}  />
                        </td>
                        <td>
                        <input type="text"  id='id2' name="ariline_pnr" value={tkt.airline_pnr.trim()} onChange={this.airlinepnrhandleChangeEvent(tkt)}/>
                        </td>
                        <td>
                         <input type="text"  id='id3' name="ticket_number" value={tkt.ticket_number.trim()} onChange={this.ticketnumberhandleChangeEvent(tkt)}/>
                        </td>
                        <td>
                        <input type="text" id='id4'  name="agent_pcc" value={tkt.agent_pcc.trim()}  onChange={this.agentpcchandleChangeEvent(tkt)}/>
                        </td>
                     
                      </tr>
                      ))}
                    </tbody>
                  </table>
                  
                      </div>
                  
                </div>
              ))}
              <div className="btnSec">

                   <Button
                      size="xs"
                      viewType="default"
                      id="updatetktbtnId"
                      className="w-80"
                      onClickBtn={this.resetOldDetails}                      
                      btnTitle='Cancel'>                      
                    </Button>
                    
                    {this.state.ftripsPermissions.PAX_UPDATE_TKT_PERMSN_ENABLE &&
                    <Button
                      size="xs"
                      viewType="primary"
                      id="updatetktbtnId"
                      className="w-80"
                      onClickBtn={this.updateAirTktDetails}
                      loading = {this.state.updateAirTkt}
                      btnTitle='Update'>
                      
                    </Button>
                    }
                  </div>
              </>
              
                  
            ) : (
            
            <>           
              
              {this.state.ticketDetails.tkt_Dtls_List.map((pax, index) => (
              <div key={index}>
                <div className="highInfo">{pax.title} {pax.first_name}  {pax.last_name} {(pax.dob!==undefined && pax.dob!=='') &&  "(DOB : "+DateUtils.convertStringdateFormat(pax.dob)+")" } <small className="t-color1">{currency} {NumberUtils.numberFormat(pax.totalAmt)}</small></div>
                <div className ='resTable'>
                <table className="dataTbl pax-tbl dataTable5">
                  <thead><tr key={index}>
                    <th width="15%">Sector</th>
                    <th width="10%">GDS PNR</th>
                    <th width="10%">Airline PNR</th>
                    <th width="10%">Ticket #</th>
                    <th width="15%"> Pcc</th>
                   
                  </tr></thead>
                  <tbody>
                  {pax.segList.map((tkt, index) => (
                    <tr key={index}>
                     <td>
                     <span className={this.findClass(this.state.ticketDetails.bkng_status)}>
                     {tkt.sector}</span>                     
                     </td>
                      <td>
                      <span className={this.findClass(this.state.ticketDetails.bkng_status)}>
                      {tkt.gds_pnr}</span>
                      </td>
                      <td>
                      <span className={this.findClass(this.state.ticketDetails.bkng_status)}>
                      {tkt.airline_pnr}</span>
                      </td>
                      <td>
                      <span className={this.findClass(this.state.ticketDetails.bkng_status)}>                         
                      {tkt.ticket_number}</span>
                      </td>
                      <td>
                      <span className={this.findClass(this.state.ticketDetails.bkng_status)}> 
                      {tkt.agent_pcc}</span>
                      </td>
                   
                    </tr>
                    ))}
                  </tbody>
                </table>
                
                </div>
              </div>
            ))}</>
              )}
  
              </div>
             
             </>
           ):<CommonMsg errorType="datanotFound" errorMessageText="since some txns in this trip are open you cannot update any PNR's now."></CommonMsg>}
  </>
          )
      }
  
  
  }

  const mapDispatchToProps = dispatch => {
    //console.log("mapDispatchToProps");
    return {
      
        
        updateTickets: (req) => dispatch(actions.updateTicketDetails(req)),
        updateTxns: (req) => dispatch(actions.updateTxnsDetails(req)),
        updateNotes:(req) => dispatch(actions.updateNoteDetails(req)),
        
    }
  }
const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,       
        bkngSatatusMaster:state.trpReducer.bkngSatatusMaster
       
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(FlightUpdateTicketNumber);