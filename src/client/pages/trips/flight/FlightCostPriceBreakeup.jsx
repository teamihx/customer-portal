import React, { Component } from 'react'
import Popup from "reactjs-popup";
import { Link } from 'react-router-dom'
import utility from '../../common/Utilities'

class FlightCostPriceBreakup extends Component {
    constructor(props) {
      super(props);
      this.state = { open: false };
      this.openModal = this.openModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
      this.setState({ open: true });
    }
    closeModal() {
      this.setState({ open: false });
    }
  
    render() {
      return (
        <div>
          
          <Popup
            trigger={<a  onClick={this.openModal}>
            {utility.PriceFormat(parseInt(this.props.data.total_fare,10),this.props.data.currency)}
            </a>}
            open={this.state.open}
            on="focus"
            position="top left"
            closeOnDocumentClick
            onClose={this.closeModal}
            position="bottom right"
          >
            <div className="modal pop-grid">
              <ul>
             { this.props.data.total_bf!==0? <li><span>Base fare</span>  {utility.PriceFormat(parseInt(this.props.data.total_bf,10),this.props.data.currency)}</li>:null}
             { this.props.data.total_tax_yr!==0?  <li><span>Congestion fee</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_yr,10),this.props.data.currency)}</li>:null}
             { this.props.data.total_tax_yq!==0?    <li><span>Airline fuel charge</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_yq,10),this.props.data.currency)}</li>:null}
             { this.props.data.total_tax!==0?  <li><span>Total Tax Unknown</span> {utility.PriceFormat(parseInt(this.props.data.total_tax,10)-parseInt(this.props.data.total_tax_yr,10)
                  -parseInt(this.props.data.total_tax_yq,10)-parseInt(this.props.data.total_tax_psf,10)-parseInt(this.props.data.total_tax_jn,10)-parseInt(this.props.data.total_tax_cute,10),this.props.data.currency)}</li>:null}
            { this.props.data.total_tax_psf!==0?   <li><span>Pax Service Fee</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_psf,10),this.props.data.currency)}</li>:null}
            {this.props.data.total_tax_jn!==0?  <li><span>Govt. Service Tax</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_jn,10),this.props.data.currency)}</li>:null}
            { this.props.data.total_tax_cute!==0?   <li><span>Cute Fee</span> {utility.PriceFormat(parseInt(this.props.data.total_tax_cute,10),this.props.data.currency)}</li>:null}
                  
              </ul>
            </div>
          </Popup>
        </div>
      );
    }
  }
  export default FlightCostPriceBreakup;