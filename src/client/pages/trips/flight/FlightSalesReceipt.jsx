import React, { Component } from 'react'
import { connect } from 'react-redux';
import EmailService from '../../../services/trips/email/EmailService';
import Notification from 'Components/Notification.jsx';
import Button from 'Components/buttons/button.jsx'
export const contextPath=process.env.domainpath;

class FlightSalesReceipt extends Component {

    constructor(props) {
        super(props)
        this.state = {
            responseData1: this.props.tripDetail,
			email: this.props.tripDetail.contact_detail.email,
			showSucessMessage:false,
            showFailMessage:false,
 			message:false,
			loading:false,
			iCalendar :true,
			errorMsgList:[],
			showInputMessage:false,
          
        }
 		
        //console.log('constructor');
		this.sendFlightSalesReceipt = this.sendFlightSalesReceipt.bind(this);
		this.handleChange = this.handleChange.bind(this);	
		this.getMessage = this.getMessage.bind(this);	
		this.validateEmails = this.validateEmails.bind(this);		
    }

   componentWillMount(){
	 //console.log('componentWillMount');
	
	}
	
	handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
		if(e.target.id === 'icalInputSalesRecpt'){
			if(this.state.iCalendar){
					this.state.iCalendar = false;
				}else{
					this.state.iCalendar = true;
				}
		}
    }

	sendFlightSalesReceipt = () => {
		try{ 
		this.setState({sendSalesReceipt:true});   
		if(this.validateEmails()){
			let email = new Object();		
		email.to=this.state.email;
		email.tripRefNumber=this.state.responseData1.trip_ref;
		email.type='FLIGHT_SALES_RECEIPT';
		email.ical=this.state.iCalendar;		
				
		EmailService.sendEmails(email).then((valid) => {
	    if (valid) {
		 //console.log('send Emails : '+JSON.stringify(valid));
		if(valid !==undefined && valid !==''){
			let response = valid;
			//console.log('description: '+response.description);
			if(response.description.includes('success')){
				this.setState({showSucessMessage: true, sendSalesReceipt:false, showAlert: true});
				//console.log('description: '+response.description);
			 
			}else{
				//console.log('description: '+response.description);
				this.setState({showFailMessage: true, sendSalesReceipt:false, showAlert: true});

				
			}
			
		}
	
		}
	 	
		});
		}  	
		}catch(err){
			this.setState({showFailMessage: true, sendSalesReceipt:false, showAlert: true});  
		}
		
    }
	
	getMessage(type){
		let msg ='';
		if(type === 'success'){
			msg = 'We have sent Sales Receipt details in an email to '+this.state.email;;
		}else if(type === 'error'){
			msg = 'We are not able to send a mail with Sales Receipt details. You can try again at some other time ';
		}
		return msg;
	}
	validateEmails(){
		let isValid = true;
		if(this.state.email === null || this.state.email.trim() === ''){
	        this.state.errorMsgList.push('Email must be provided')
	        isValid= false;
			this.setState({showInputMessage: true, sendSalesReceipt:false, showAlert: true});
	     }
		 if(this.state.email != null && this.state.email.trim() !== ''){
			const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
			isValid =  expression.test(String(this.state.email).toLowerCase());
			if(!isValid){
			this.state.errorMsgList.push('Enter a valid email address');
			this.setState({showInputMessage: true, sendSalesReceipt:false, showAlert: true}); 	
			}
				
		}
		//console.log('valid email---'+isValid);
		return isValid;
	
	}
	toggleAlert = () => {
	  this.setState({
		showAlert: false
	  });
	};
    render() {
		let viewType, messageText;
		if (this.state.showInputMessage) {
		  viewType = "error";
		  messageText = this.state.errorMsgList;
		} else if (this.state.showFailMessage) {
		  viewType = "error";
		  messageText = this.getMessage("error");
		} else if (this.state.showSucessMessage) {
		  viewType = "success";
		  messageText = this.getMessage("success");
		}
        return (
            <>
            <div>
			{viewType && messageText && (
            <Notification
			  viewType={viewType}
			  viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}
            <div className="form-group">
			  <div className="row">
			  <div className="col-12">
				 	<input type="text" name="email" value={this.state.email} onChange={this.handleChange} />
					
					 <div className="dis-flx-btw-ned w-100p">
					 <div>
					<input
		                  id="icalInputSalesRecpt"
		                  type="checkbox"
		                  className="customCheckbox"
		                  defaultChecked={this.state.iCalendar}
		                  value={this.state.iCalendar}
		                  onChange={this.handleChange}
		                />
                	<label htmlFor="icalInputSalesRecpt">Send iCalendar</label>  
			  		</div>
					<div>
					  <Button 
						size="xs"
						viewType="primary"
						onClickBtn={this.sendFlightSalesReceipt}
						loading = {this.state.sendSalesReceipt}
						btnTitle='Send'> 
						</Button>
				 
				 </div>
				 </div>
				 </div>
                   </div>

			</div>

            </div>
           
</>

        )
    }


}

const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,       
        
        
       
     };
}

export default connect(mapStateToProps, null)(FlightSalesReceipt);



