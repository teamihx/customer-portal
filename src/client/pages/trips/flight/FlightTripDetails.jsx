import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Popup from "reactjs-popup"
import utility from '../../common/Utilities'
import { connect } from 'react-redux';
import dateUtil from '../../../services/commonUtils/DateUtils';
import FlightService from '../../../services/trips/flight/FlightService';
import Tooltip from 'react-tooltip-lite';
import FlightItineraryDetail from './FlightItineraryDetail';
import FlightBookingDetail from './FlightBookingDetail';
import UserFetchService from '../../../services/trips/user/UserFetchService';
import * as actions from '../../../store/actions/index'
import FlightItineraryDetailOld from './FlightItineraryDetailOld';
import FlightBookingDetailOld from './FlightBookingDetailOld';

//import * as actions from '../../../store/actions/index'


class FlightTripDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
            //bkngStatuses:bkngStatMaster,
           //cbinTypeMaster:cabinTypeMaster,
            tripDtls :this.props.tripDetail,
            fltDtlList:[],
            paxFltIndex:1,
            fltItinerary:{},
            showCurrentItinerary:true,
            insuranceAvailable:false
           
        }
      this.bookingAmended=false;
      this.no_of_pax=0;
    }
    
    loadTripDetails=()=>{
        let air_bookings = new Object();
        if(!this.isEmpty(this.props.tripDetail)){
            console.log("trip dtl=="+JSON.stringify(this.props.tripDetail))
            
                this.props.tripDetail.insurances.length>0?this.state.insuranceAvailable=true:null
                air_bookings={...this.props.tripDetail.air_bookings[0]}
                air_bookings.itinerary_id=this.props.tripDetail.itinerary_id;
                //air_bookings.amend_itinerary_id=this.props.tripDetail.amend_itinerary_id;
                air_bookings.display_current_itinerary=this.state.showCurrentItinerary?true:false;
                //air_bookings.is_amended=typeof this.props.tripDetail.air_bookings[0].amend_itinerary_id!=='undefined'&& air_bookings.amend_itinerary_id!==null?true:false
                if(this.props.tripDetail.currency==="INR"){
                    air_bookings.currency='Rs'
                }else{
                    air_bookings.currency=this.props.tripDetail.currency
                }
                
                //this.state.fltItinerary={...air_bookings}
                this.state.insuranceAvailable?air_bookings.insurances=[...this.props.tripDetail.insurances]:null
                if(this.props.tripDetail.insurances.length>0){
                    let name=''
                    for(let insurance of this.props.tripDetail.insurances[0].insureds){
                        name+=insurance.title + " "+insurance.first_name +" " + insurance.last_name+","
                    }
                    air_bookings.insurances[0].name=name.substr(0,name.length-1);
                }
                for(let info of air_bookings.air_booking_infos){
                    for(let flight of air_bookings.flights){
                        flight.itinerary_id=air_bookings.itinerary_id;
                        flight.air_booking_type=air_bookings.air_booking_type
                        for(let seg of flight.segments){
                            if(seg.id===info.segment_id){
                                seg.cabin_type =info.cabin_type;
                              }
                        }
                    }
                   
                }
                for(let flt of air_bookings.flights){
                    if(flt.segments.length>1){
                        for(let seg of flt.segments){
                            let last_index =flt.segments.length-1
                            if(flt.segments.indexOf(seg)!==last_index){
                                let nextSeg=flt.segments[flt.segments.indexOf(seg)+1]
                                seg.lay_over = Math.abs(new Date(nextSeg.departure_date_time)-new Date(seg.arrival_date_time))
                            }
                        }
                    }
                }
                //console.log("fltItinerary===="+JSON.stringify(air_bookings.flights))
                {this.loadFlightBookingDetaiil(air_bookings)}
                
                if(air_bookings.amend_itinerary_id!==null || (air_bookings.air_booking_info_history!==null && air_bookings.air_booking_info_history.length>0)){
                    console.log("is_amended===")
                    this.bookingAmended=true;
                    air_bookings.is_amended=true
                }
                if(this.bookingAmended){
                    {this.loadFlightBookingDetaiilOld(air_bookings)};
                }
                
                air_bookings.travellers=this.props.tripDetail.travellers;
                this.setState({fltItinerary:{...air_bookings}},()=>console.log("fltItinerary=="+JSON.stringify(this.state.fltItinerary)))
                
            

        }
    }
    loadFlightBookingDetaiilOld=(air_bookings)=>{
       
        //pricingObject.source_type=tripDetail.txn_source_type;
        let flight_to_pax_map_history=new Object();
         //let airbkng = this.props.tripDetail.air_bookings[0];
         //let paxToPriceMapObj=[];
         let paxToPriceMapObj=[];
         for(let flight of air_bookings.flights){
            if(flight.segments_history!==null){
                for(let segment of flight.segments_history){
                    for(let info of air_bookings.air_booking_info_history) {
                        if(segment.segment_id===info.segment_id){
                            info.arrival_airport=segment.arrival_airport
                            info.departure_airport=segment.departure_airport
                        }
                    }
                }
            }
            
        }
        
         for(let pax of air_bookings.pax_infos){
             let pax_infos = new Object()
             
             pax_infos={...pax}
             let paxPriceInfo=[];
             if(air_bookings.air_booking_info_history!==null){
             for(let info of air_bookings.air_booking_info_history) {
                
                //console.log('checkdss'+ pax.id.stringify===info.pax_info_id)
                
               if(pax.id===parseInt(info.pax_info_id,10)){
                //console.log('checkdss'+ pax.id.stringify===info.pax_info_id)
                 let air_booking_infos_history = new Object();
                 
                air_booking_infos_history={...info}
                 let segsPrice=[];
                 let pricing_objects=new Object();
                 for(let price of air_bookings.pricing_objects_history){
                    //console.log('checkdss=='+  info.pricing_object_id)
                    //console.log('checkdss=='+  price.id)
                   if( parseInt(info.pricing_object_id,10)===parseInt(price.pricing_object_id,10)){
                    //console.log('checkdss==='+ typeof pax.id)
                    //console.log('checkdss==='+ typeof info.pricing_object_id)
                    price.currency=air_bookings.currency;
                    price.cost_pricing_objects_history.currency=air_bookings.currency;
                    pricing_objects={...price};
                    console.log('check true')
                   }
                 }
                 
                 air_booking_infos_history.pricing_objects={...pricing_objects}
                 let addElement = true;
                
                 if(Array.isArray(paxPriceInfo) && paxPriceInfo.length>0){
                   for(let paxInfo of paxPriceInfo){
                    console.log('find'+ typeof paxInfo.pricing_object_id)
                    console.log('find'+ typeof air_booking_infos_history.pricing_object_id)
                     if(paxInfo.pricing_object_id===air_booking_infos_history.pricing_object_id){
                        air_booking_infos_history.fare_basis_code=air_booking_infos_history.pricing_objects.fare_basis_code
                       delete air_booking_infos_history.pricing_objects;
                       console.log('check true123')

                       break;
                     }
                   }
                 }else{
                    air_booking_infos_history.fare_basis_code=air_booking_infos_history.pricing_objects.fare_basis_code
                 }
                
                  paxPriceInfo.push(air_booking_infos_history);
                 
                 
                 
                }
                
                pax_infos.air_booking_infos_history=[...paxPriceInfo];
            }
             
             //paxObj.paxToFlightMap=
             paxToPriceMapObj.push(pax_infos);
             for(let pax of paxToPriceMapObj){
                pax.currency=air_bookings.currency
               pax.is_insured=false;
               let pax_total_premium=0
               let no_of_pax=0
               //console.log("Insured"+air_bookings.insurances>0)
               if(this.state.insuranceAvailable){
                for(let insurance of air_bookings.insurances[0].insureds){
                   //console.log("isinsured====:"+JSON.stringify(insurance))
                    if(insurance.pax_info_id===pax.id){
                       no_of_pax+=1;
                       pax.provider=air_bookings.insurances[0].insurance_master_id
                       pax.is_insured=true;
                       pax.insured={...insurance};
                       pax_total_premium=insurance.total_premium
                       pax.insured.insurance_master_id=air_bookings.insurances[0].insurance_master_id
                    }
                   
                }
                air_bookings.insurances[0].no_of_pax=no_of_pax;
                air_bookings.insurances[0].pax_total_premium=pax_total_premium;

               }
             }
         
        }
         }

         flight_to_pax_map_history=[...paxToPriceMapObj];
         air_bookings.flight_to_pax_map_history=[...flight_to_pax_map_history]
         console.log('flight_to_pax_map_hist'+JSON.stringify(air_bookings.flight_to_pax_map_history)) 
         //this.setState({showCurrentItinerary:!this.state.showCurrentItinerary})

    }
    loadFlightBookingDetaiil=(air_bookings)=>{
        //pricingObject.source_type=tripDetail.txn_source_type;
        let flight_to_pax_map=new Object();
         //let airbkng = this.props.tripDetail.air_bookings[0];
         //let paxToPriceMapObj=[];
         let paxToPriceMapObj=[];

         for(let flight of air_bookings.flights){
            for(let segment of flight.segments){
                for(let info of air_bookings.air_booking_infos) {
                    if(segment.id===info.segment_id){
                        info.arrival_airport=segment.arrival_airport
                        info.departure_airport=segment.departure_airport
                    }
                }
            }
        }
         for(let pax of air_bookings.pax_infos){
             let pax_infos = new Object()
             
             pax_infos={...pax}
             let paxPriceInfo=[];
             for(let info of air_bookings.air_booking_infos) {
               if(pax.id===info.pax_info_id){
                 let air_booking_infos = new Object();
                //console.log('sdcs')
                air_booking_infos={...info}
                 let segsPrice=[];
                 let pricing_objects=new Object();
                 for(let price of air_bookings.pricing_objects){
                  
                   if( info.pricing_object_id===price.id){
                    
                    price.currency=air_bookings.currency;
                    price.cost_pricing_object.currency=air_bookings.currency;
                    pricing_objects={...price};
                      
                   }
                 }
                 
                 air_booking_infos.pricing_objects={...pricing_objects}
                 let addElement = true;
                 
                 air_booking_infos.fare_basis_code=air_booking_infos.pricing_objects.fare_basis_code
                 console.log("paxPriceInfoos==="+JSON.stringify(paxPriceInfo))
                 if(Array.isArray(paxPriceInfo) && paxPriceInfo.length>0){
                   for(let paxInfo of paxPriceInfo){
                   //console.log("paxInfo.==="+JSON.stringify(paxInfo))
                   if(typeof paxInfo.pricing_objects!=='undefined' && typeof air_booking_infos.pricing_objects!=='undefined'){
                    if(paxInfo.pricing_objects.id===air_booking_infos.pricing_objects.id){
                        air_booking_infos.fare_basis_code=air_booking_infos.pricing_objects.fare_basis_code
                       delete air_booking_infos.pricing_objects;
                      

                       break;
                     }
                   }
                    //console.log("air_booking_infos.pricing_objects.id==="+JSON.stringify(air_booking_infos.pricing_objects.id))
                     
                   }
                 }
                 
                  paxPriceInfo.push(air_booking_infos);
                 
                 
                 //console.log('sdcs'+JSON.stringify(bkngInfo))
                }
                
                pax_infos.air_booking_infos=[...paxPriceInfo];
                
             }
             //paxObj.paxToFlightMap=
             paxToPriceMapObj.push(pax_infos);
             if(this.state.insuranceAvailable){
             this.no_of_pax=air_bookings.insurances[0].insureds.length;
             for(let pax of paxToPriceMapObj){
                 pax.currency=air_bookings.currency
                pax.is_insured=false;
                let pax_total_premium=0
                let no_of_pax=0
                //console.log("Insured"+air_bookings.insurances>0)
               
                 for(let insurance of air_bookings.insurances[0].insureds){
                    //console.log("isinsured====:"+JSON.stringify(insurance))
                     if(insurance.pax_info_id===pax.id){
                        console.log("isinsured====:"+JSON.stringify(insurance))
                        no_of_pax+=1;
                        pax.provider=air_bookings.insurances[0].insurance_master_id
                        pax.is_insured=true;
                        pax.insured={...insurance};
                        pax_total_premium=insurance.total_premium
                        pax.insured.insurance_master_id=air_bookings.insurances[0].insurance_master_id
                     }
                    
                 }
                 air_bookings.insurances[0].no_of_pax=air_bookings.insurances[0].insureds.length;
                 air_bookings.insurances[0].pax_total_premium=pax_total_premium;

                }
             }
         
         } 
         flight_to_pax_map=[...paxToPriceMapObj];
         air_bookings.flight_to_pax_map=[...flight_to_pax_map]
         //console.log('flight_to_pax_map'+JSON.stringify(air_bookings)) 
         

    }
     secondsToHm=(d) =>{
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);
    
        var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes ") : "";
        
        return hDisplay + mDisplay; 
    }
    isAirIndiaExpress=(flt)=>{
       for(let seg of flt.segments) {
        if(seg.marketing_airline==='IX'){
            return true;
        }
       }
        
      return false;
    }
    showNewPopUp(flt,e){
        e.persist();
        let url ='';
        console.log("e.target.id +"+ JSON.stringify(flt) )
       if(flt.air_booking_type==="I" && !this.isAirIndiaExpress(flt)){
        url="https://qa2.cleartrip.com/hq/trips/"+flt.itinerary_id+"/get_int_fare_rule"
       }else{
        url = "https://qa2.cleartrip.com/hq/trips/flight/"+flt.id+"/fare_rule"
       }
        
       window.open(
        url
          );
    }
    isEmpty = (obj) => {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }
    getTravellers=(travellers)=>{
        let pricingObject=new Object();
         let adultCount =0;
         let childCount =0;
         let infantCount =0;
        
            travellers = travellers.trimRight();
           let travellerString=travellers.substring(travellers.trimRight().indexOf('|')+1, travellers.length);
           let ttravellerArr=travellerString.trimLeft().split('\n')
            
           for(let travel of ttravellerArr){
               if(travel.includes('INF')){
                infantCount=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
               if(travel.includes('ADT')){
                adultCount=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
               if(travel.includes('CHD')){
                childCount=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
           }
          let adtTravel= adultCount>0 ?adultCount +(adultCount==1 ? " Adult  ":" Adults  "):""
          let childTravel= childCount>0 ?childCount +(childCount==1 ? " Child  ":" Children  "):""
          let infTravel= infantCount>0 ?infantCount +(infantCount==1 ? " Infant  ":" ,Infant  "):""
          return adtTravel + childTravel + infTravel
        }
    
    displayItinerary=()=>{
        
        this.setState({showCurrentItinerary:!this.state.showCurrentItinerary},console.log("showCurrentItinerary=="+this.state.showCurrentItinerary))
        
    }
      componentDidMount(){
        {this.loadTripDetails()}  
      }
    render() {
        
        
            
    
       return(
           
        <div>   
       {!this.isEmpty(this.state.fltItinerary)?
        <>
          {this.state.showCurrentItinerary ?<FlightItineraryDetail data={this.state.fltItinerary} showOld={this.displayItinerary}/>:null}
           
          {!this.state.showCurrentItinerary ?<FlightItineraryDetailOld data={this.state.fltItinerary} showNew={this.displayItinerary}/>:null}
          {this.state.showCurrentItinerary ?  <FlightBookingDetail data={this.state.fltItinerary} />:null}

          {!this.state.showCurrentItinerary ?<FlightBookingDetailOld data={this.state.fltItinerary} />:null}
          
           
              
          {this.state.insuranceAvailable?  <>  <h4 className="in-tabTTl-sub">Insurance</h4>
               <ul className="mt-10">
                    <li><span className="d-ib w-70 mb-5">Product </span>: {this.props.InsuranceMasterData[this.state.fltItinerary.insurances[0].insurance_master_id]}</li>
                    <li><span className="d-ib w-70 mb-5">Policy</span>: {typeof this.state.fltItinerary.insurances[0].status!=='undefined' && this.state.fltItinerary.insurances[0].status!==null?this.props.InsuranceStatusMasterData[this.state.fltItinerary.insurances[0].status]:"-"}</li>
                    <li><span className="d-ib w-70 mb-5">Travellers</span>: {this.state.fltItinerary.insurances[0].name}</li>
                    <li><span className="d-ib w-70 mb-5">Price</span>: {utility.PriceFormat(this.state.fltItinerary.insurances[0].total_premium,this.state.fltItinerary.currency)}({utility.PriceFormat(this.state.fltItinerary.insurances[0].insureds[0].total_premium,this.state.fltItinerary.currency)} * {this.no_of_pax})</li>
                </ul></>:null}
               

           </>:null}
       </div>
       )
       
         
}

}
const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,
        airPortMaster: state.trpReducer.airPortMaster,
        airLineMaster: state.trpReducer.airLineMaster,
        InsuranceStatusMasterData:state.trpReducer.InsuranceStatusMasterData,
        bkngSatatusMaster:state.trpReducer.bkngSatatusMaster,
        paymentStatusMaster:state.trpReducer.paymntStatusMaster,
        paymentTypeMaster:state.trpReducer.paymentTypeMaster,
        cabinTypeMaster:state.trpReducer.cabinTypeMaster,
        InsuranceMasterData:state.trpReducer.InsuranceMasterData
    };
}
const mapDispatchToProps = dispatch => {
    console.log("mapDispatchToProps");
    return {
      
        
        updateUserDetails: (req) => dispatch(actions.updateUserDetails(req)),
        
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(FlightTripDetails);