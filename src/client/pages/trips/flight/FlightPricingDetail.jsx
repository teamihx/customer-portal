import React, { Component } from 'react';
import { connect } from 'react-redux';
import log from 'loglevel';
import utility from '../../common/Utilities'
import ShowHide from 'Components/ShowHide.jsx';
import * as actions from '../../../store/actions/index';

class FlightPricingDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
         pricingDetail:{}
        }
        
    }
    isEmpty = (obj) => {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }
      loadPricingDetail=(tripDetail)=>{
          
        let pricingObject=new Object();
        
        if(!this.isEmpty(tripDetail)){
           let travellers = tripDetail.travellers.trimRight();
           let travellerString=travellers.substring(travellers.trimRight().indexOf('|')+1, travellers.length);
           let ttravellerArr=travellerString.trimLeft().split('\n')
           for(let travel of ttravellerArr){
               if(travel.includes('INF')){
                   pricingObject.noOfInf=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
               if(travel.includes('ADT')){
                   pricingObject.noOfADT=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
               if(travel.includes('CHD')){
                   pricingObject.noOfCHD=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
           }
        pricingObject.source_type=tripDetail.txn_source_type;
        pricingObject.currency=tripDetail.currency
        
          pricingObject.insurances=[...tripDetail.insurances]
        
         let airbkng = tripDetail.air_bookings[0];
         //let paxToPriceMapObj=[];
         let paxToPriceMapObj=[];
         for(let pax of airbkng.pax_infos){
             let paxObj = new Object()
             
             paxObj.pax_type_code=pax.pax_type_code
             let paxPriceInfo=[];
             for(let info of airbkng.air_booking_infos) {
               if(pax.id===info.pax_info_id){
                 let bkngInfo = new Object();
                //console.log('sdcs')
                 let segsPrice=[];
                 let segToPrice=new Object();
                 for(let price of airbkng.pricing_objects){
                  
                   if( info.pricing_object_id===price.id){
                    
                    
                    segToPrice={...price};
                      
                   }
                 }
                 
                 bkngInfo.seg_price={...segToPrice}
                 let addElement = true;
                 if(paxPriceInfo.length>0){
                   for(let paxInfo of paxPriceInfo){
                     if(paxInfo.seg_price.id===bkngInfo.seg_price.id){
                       addElement=false;
                       break;
                     }
                   }
                 }
                 if(addElement){
                  paxPriceInfo.push(bkngInfo);
                 }
                 
                 //console.log('sdcs'+JSON.stringify(bkngInfo))
                }
                
                paxObj.flight_to_pax=[...paxPriceInfo];
                
             }
             //paxObj.paxToFlightMap=
             paxToPriceMapObj.push(paxObj);
             
         } 
         pricingObject.pax_Detail=[...paxToPriceMapObj];
          this.state.pricingObject={...pricingObject}
        }
        
      
        this.setPricingObject(this.state.pricingObject)
        //console.log("Price Object===== "+JSON.stringify(this.state.pricingObject))
      }
    componentDidMount(){
      this.props.onInitPricingObject(this.state.pricingDetail)
    }
   setPricingObject=(priceObject)=>{
    this.state.pricingDetail.no_of_adt=0;
    this.state.pricingDetail.no_of_chd=0;
    this.state.pricingDetail.no_of_inf=0;
    if(priceObject.currency==='INR'){
      this.state.pricingDetail.currency='Rs'
    }else{
      this.state.pricingDetail.currency=priceObject.currency;
    }
    
          if(priceObject.noOfADT>0){
            this.state.pricingDetail.no_of_adt=priceObject.noOfADT
              let adtBaseFare=0;
              for(let paxPrice of priceObject.pax_Detail){
                  if(paxPrice.pax_type_code==='ADT'){
                    for(let seg of paxPrice.flight_to_pax){
                      adtBaseFare+= seg.seg_price.total_base_fare;
                    }
                    
                    break;
                  }
              }
              this.state.pricingDetail.adt_base_fare=adtBaseFare
          }
          if(priceObject.noOfCHD>0){
            this.state.pricingDetail.no_of_chd=priceObject.noOfCHD;
            let chdBaseFare=0;
            for(let paxPrice of priceObject.pax_Detail){
                if(paxPrice.pax_type_code==='CHD'){
                  for(let seg of paxPrice.flight_to_pax){
                  chdBaseFare+= seg.seg_price.total_base_fare;
                  }
                  break;
                }
            }
            this.state.pricingDetail.chd_base_fare=chdBaseFare
        }
        if(priceObject.noOfInf>0){
            this.state.pricingDetail.no_of_inf=priceObject.noOfInf;
           let infBaseFare=0;
            for(let paxPrice of priceObject.pax_Detail){
                if(paxPrice.pax_type_code==='INF'){
                  for(let seg of paxPrice.flight_to_pax){
                    infBaseFare+= seg.seg_price.total_base_fare;
                  }
                  break;
                }
            }
            this.state.pricingDetail.inf_base_fare=infBaseFare

        }
        let total_cash_back=0;
        let total_discount=0;
        let total_tax_ttf=0;
        let total_markUp=0;
        let total_tax=0;
        let total_gst=0
        let total_emi=0
        let total_amend_fee_air=0
        let total_fee_cncl=0
        let total_fee_meal=0
        let total_fee_baggage=0
        let total_fee_seat=0;
        let total_fee_bundle=0;
        let total_con_fee=0;
        let total_price=0;
        let total_nc_fee=0;
        let insurance_fee=0;
        let total_fee=0;
        let total_fee_amd=0;
        let total_fee_amend=0
        if(priceObject.insurances.length>0){
          insurance_fee=priceObject.insurances[0].total_premium;
          this.state.pricingDetail.insurance=insurance_fee
        }
        for(let paxPrice of priceObject.pax_Detail){
          for(let seg of paxPrice.flight_to_pax){
            total_cash_back+=seg.seg_price.total_cashback;
            total_discount+=seg.seg_price.total_discount;
            total_tax_ttf+=seg.seg_price.total_tax_ttf;
            total_markUp+=seg.seg_price.total_markup;
            total_tax+=seg.seg_price.total_tax
            total_gst=total_gst+seg.seg_price.total_tax_cgst+seg.seg_price.total_tax_sgst
            total_emi+=seg.seg_price.total_fee_pgc;
            total_amend_fee_air+=seg.seg_price.total_fee_airl_amd;
            total_fee_amd+=seg.seg_price.total_fee_amd
            total_fee_cncl+=seg.seg_price.total_fee_cncl
            total_fee_meal+=seg.seg_price.total_fee_meal
            total_fee_baggage+=seg.seg_price.total_fee_baggage
            total_fee_seat+=seg.seg_price.total_fee_seat
            total_fee_bundle+=seg.seg_price.total_fee_bundle
            total_price+=seg.seg_price.total_fare
            total_nc_fee+=seg.seg_price.total_nc_fee
            total_fee+=seg.seg_price.total_fee;
            total_con_fee+=seg.seg_price.total_fee_con

          }
          }
          total_fee_amend=total_amend_fee_air+total_fee_amd
          let misc_fee=total_con_fee+total_emi+total_fee_amend+total_fee_cncl+total_fee_meal+total_fee_baggage+total_fee_seat+total_fee_bundle
          let total_con_fee_ext=total_fee-misc_fee;
         
        this.state.pricingDetail.total_cash_back=total_cash_back;
        this.state.pricingDetail.total_discount=total_discount;
        this.state.pricingDetail.total_tax_ttf=total_tax_ttf;
        this.state.pricingDetail.total_markUp=total_markUp;
        this.state.pricingDetail.total_tax=total_tax;
        this.state.pricingDetail.total_gst=total_gst;
        this.state.pricingDetail.total_tax_ttf=total_tax_ttf;
        let other_charges=total_tax+total_markUp-total_tax_ttf-total_gst;
        this.state.pricingDetail.other_charges=other_charges
        this.state.pricingDetail.total_gst=total_gst
        this.state.pricingDetail.total_emi=total_emi
        this.state.pricingDetail.total_fee_amend=total_fee_amend
        this.state.pricingDetail.total_fee_cncl=total_fee_cncl
        this.state.pricingDetail.total_fee_meal=total_fee_meal
        this.state.pricingDetail.total_fee_baggage=total_fee_baggage
        this.state.pricingDetail.total_fee_seat=total_fee_seat
        this.state.pricingDetail.total_fee_bundle=total_fee_bundle
        this.state.pricingDetail.total_con_fee=total_con_fee
        this.state.pricingDetail.total_con_fee_ext=total_con_fee_ext
        if(priceObject.source_type==='AGENCY'){
            this.state.pricingDetail.total_price=total_price-total_nc_fee+insurance_fee;
        }else{
            this.state.pricingDetail.total_price=total_price+insurance_fee
        }
      
      //console.log("Pricing====="+JSON.stringify(this.state.pricingDetail));
      //this.state.pricingDetail.total_cash_back= this.state.pricingDetail.total_cash_back!==0? this.state.pricingDetail.total_cash_back.trimLeft(): this.state.pricingDetail.total_cash_back;
      //this.state.pricingDetail.total_discount=this.state.pricingDetail.total_discount!==0? this.state.pricingDetail.total_discount.trimLeft(): this.state.pricingDetail.total_discount;;
      
   }   
render() {
    {this.loadPricingDetail(this.props.tripDetail)}
    return (
      <>
      <div className="side-pnl">
        <ShowHide visible="true" title="Pricing Details">
        <div className="showHide-content">
                <ul className="price-info pb-10">
                 {typeof this.state.pricingDetail.adt_base_fare!=='undefined'?<li><span>Adult</span>  <span>{utility.PriceFormat(this.state.pricingDetail.adt_base_fare * this.state.pricingDetail.no_of_adt,this.state.pricingDetail.currency) }<em className="d-b font-12 mt-4 t-color3">({utility.PriceFormat(this.state.pricingDetail.adt_base_fare,this.state.pricingDetail.currency)} * {this.state.pricingDetail.no_of_adt})</em></span></li>:''}
                 {typeof this.state.pricingDetail.chd_base_fare!=='undefined'?<li><span>Child</span>  <span>{utility.PriceFormat(this.state.pricingDetail.chd_base_fare * this.state.pricingDetail.no_of_chd,this.state.pricingDetail.currency) }<em className="d-b font-12 mt-4 t-color3">({utility.PriceFormat(this.state.pricingDetail.chd_base_fare,this.state.pricingDetail.currency)} * {this.state.pricingDetail.no_of_chd})</em></span></li>:''}
                 {typeof this.state.pricingDetail.inf_base_fare!=='undefined'?<li><span>Infant</span>  <span>{utility.PriceFormat(this.state.pricingDetail.inf_base_fare * this.state.pricingDetail.no_of_inf,this.state.pricingDetail.currency) }<em className="d-b font-12 mt-4 t-color3">({utility.PriceFormat(this.state.pricingDetail.inf_base_fare,this.state.pricingDetail.currency)} * {this.state.pricingDetail.no_of_inf})</em></span></li>:''}
                 { this.state.pricingDetail.total_cash_back!==0?<li><span>Cash back (-)</span>  <span>{utility.PriceFormat(-1*this.state.pricingDetail.total_cash_back,this.state.pricingDetail.currency)}</span></li>:''}
                 { this.state.pricingDetail.total_tax_ttf!==0?<li><span>Transaction Fee</span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_tax_ttf,this.state.pricingDetail.currency)}</span></li>:''}
                 {this.state.pricingDetail.total_discount!==0?<li><span>Discount (-)</span> <span>{utility.PriceFormat(-1*this.state.pricingDetail.total_discount,this.state.pricingDetail.currency)}</span></li>:''}
                 { this.state.pricingDetail.other_charges!==0?<li><span>Other Charges</span>  <span>{utility.PriceFormat(this.state.pricingDetail.other_charges,this.state.pricingDetail.currency)}</span></li>:''} 
                 {this.state.pricingDetail.total_gst!==0?<li><span>GST(Airline)</span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_gst,this.state.pricingDetail.currency)}</span></li>:''}
                 {this.state.pricingDetail.total_emi!==0?<li><span>EMI process Fee</span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_emi,this.state.pricingDetail.currency)}</span></li>:''} 

                 {this.state.pricingDetail.total_fee_amend!==0?<li><span>Amendment Fee</span> <span>{utility.PriceFormat(this.state.pricingDetail.total_fee_amend,this.state.pricingDetail.currency)}</span></li>:''} 
                 {this.state.pricingDetail.total_fee_cncl!==0?<li><span>Cancellation Charges</span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_fee_cncl,this.state.pricingDetail.currency)}</span></li>:''} 
                 {this.state.pricingDetail.total_fee_meal!==0?<li><span>Meal Fee </span> <span>{utility.PriceFormat(this.state.pricingDetail.total_fee_meal,this.state.pricingDetail.currency)}</span></li>:''} 
                 {this.state.pricingDetail.total_fee_baggage!==0?<li><span>Baggage Fee </span> <span>{utility.PriceFormat(this.state.pricingDetail.total_fee_baggage,this.state.pricingDetail.currency)}</span></li>:''} 
                 {this.state.pricingDetail.total_fee_seat!==0?<li><span>Seat Fee </span> <span>{utility.PriceFormat(this.state.pricingDetail.total_fee_seat,this.state.pricingDetail.currency)}</span></li>:''} 
                 {this.state.pricingDetail.total_fee_bundle!==0?<li><span>Bundle Fee </span> <span>{utility.PriceFormat(this.state.pricingDetail.total_fee_bundle,this.state.pricingDetail.currency)}</span></li>:''} 
                 {this.state.pricingDetail.total_con_fee!==0?<li><span>Convenience Fee</span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_con_fee,this.state.pricingDetail.currency)}</span></li>:''}
                 {this.state.pricingDetail.total_con_fee_ext!==0?<li><span>Convenience Fee</span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_con_fee_ext,this.state.pricingDetail.currency)}</span></li>:''}
                 {typeof this.state.pricingDetail.insurance!=='undefined'?<li><span>Insurance</span>  <span>{utility.PriceFormat(this.state.pricingDetail.insurance,this.state.pricingDetail.currency)}</span></li>:''}
                 {typeof this.state.pricingDetail.total_price!=='undefined'?<li className="total"><span>Total</span> <span> {utility.PriceFormat(this.state.pricingDetail.total_price,this.state.pricingDetail.currency)}</span></li>:''}     
                </ul>
                </div>
                </ShowHide>
                </div>
               
           </>
    );
  }
}
const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail,
      //airLineMaster: state.trpReducer.airLineMaster
    };
  }
  const mapDispatchToProps = dispatch => {
    return {
        
        onInitPricingObject: (req) => dispatch(actions.initPricingObject(req))
        
        
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FlightPricingDetail);