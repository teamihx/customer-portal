import React, { Component } from "react";
import { Link } from "react-router-dom";
import Popup from "reactjs-popup";
import utility from "../../common/Utilities";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import FlightService from "../../../services/trips/flight/FlightService";
import Tooltip from "react-tooltip-lite";
import FlightFareBreakup from "./FlightFareBreakup";
import FlightCostPriceBreakup from "./FlightCostPriceBreakeup";
import Icon from "Components/Icon";
//import * as actions from '../../../store/actions/index'

class FlightBookingDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;
  }
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  getTravellers = travellers => {
    let pricingObject = new Object();
    let adultCount = 0;
    let childCount = 0;
    let infantCount = 0;

    travellers = travellers.trimRight();
    let travellerString = travellers.substring(
      travellers.trimRight().indexOf("|") + 1,
      travellers.length
    );
    let ttravellerArr = travellerString.trimLeft().split("\n");

    for (let travel of ttravellerArr) {
      if (travel.includes("INF")) {
        infantCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
      if (travel.includes("ADT")) {
        adultCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
      if (travel.includes("CHD")) {
        childCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
    }
    let adtTravel =
      adultCount > 0
        ? adultCount + (adultCount == 1 ? " Adult  " : " Adults  ")
        : "";
    let childTravel =
      childCount > 0
        ? childCount + (childCount == 1 ? " Child  " : " Children  ")
        : "";
    let infTravel =
      infantCount > 0
        ? infantCount + (infantCount == 1 ? " Infant  " : " ,Infants  ")
        : "";
    return adtTravel + childTravel + infTravel;
  };
  getBookingStatus = (bkngStatus, info) => {
    //console.log("info.booking_status")
    let tripDetail = this.props.tripDetail;
    let paymentDtl = this.props.tripDetail.payments_service_data.sort((a, b) =>
      a.seq_no < b.seq_no ? 1 : b.seq_no < a.seq_no ? -1 : 0
    );
    let curPmnt = paymentDtl[0];
    let configData = {};
    let statusChngEnble = false;
    if (
      !this.isEmpty(this.props.ctConfigData) &&
      this.props.ctConfigData.status === 200
    ) {
      configData = this.props.ctConfigData.data;
      statusChngEnble =
        configData["ct.rails.is_booking_status_pending_change_enabled"] ===
        "ON";
    }
    //let statusChngEnble =this.props.ctConfigData.status===200 && this.props.ctConfigData[ct.rails.is_booking_status_pending_change_enabled]==='ON'?true:false;
    if (
      tripDetail.trip_type === 1 &&
      tripDetail.air_bookings[0].air_booking_type === "I" &&
      paymentDtl.payment_category !== "T"
    ) {
      return this.props.bkngSatatusMaster[bkngStatus];
    } else if (statusChngEnble) {
      // console.log("info.booking_status"+statusHist)
      if (bkngStatus === "Z") {
        if (curPmnt.status === "I" || curPmnt.status === "O") {
          return "Payment Initialized";
        } else if (curPmnt.status === "F") {
          return this.props.bkngSatatusMaster[bkngStatus];
        } else if (curPmnt.status === "S") {
          return "Booking Initialised";
        }
      } else if (bkngStatus === "P") {
        return this.props.bkngSatatusMaster[bkngStatus];
      } else if (bkngStatus === "H") {
        let airBkng = this.props.tripDetail.air_bookings[0];
        for (let flight of airBkng.flights) {
          for (let segment of flight.segments) {
            if (segment.id === info.segment_id) {
              if (segment.failed_segment !== null) {
                if (
                  segment.failed_segment.status === "P" &&
                  segment.failed_segment.booking_status === "H"
                ) {
                  return "Booking Pending";
                } else {
                  return this.props.bkngSatatusMaster[bkngStatus];
                }
              }
            }
          }
        }
      }
    } else {
      if (bkngStatus === "Z") {
        if (curPmnt.status === "I" || curPmnt.status === "O") {
          return "Payment Initialised";
        } else if (curPmnt.status === "F") {
          return this.props.bkngSatatusMaster[bkngStatus];
        } else if (curPmnt.status === "S") {
          return "Booking Initialised";
        }
      } else if (bkngStatus === "P") {
        return this.props.bkngSatatusMaster[bkngStatus];
      } else if (bkngStatus === "H") {
        return this.props.bkngSatatusMaster[bkngStatus];
      }
    }
    return this.props.bkngSatatusMaster[bkngStatus];
  };
  render() {
    return this.props.data.flight_to_pax_map.map(ele => (
      <React.Fragment key={ele.id}>
        <div className="heading dis-flx-btw mb-5">
          {this.props.data.flight_to_pax_map.indexOf(ele) === 0 ? (
            <h4 className="in-tabTTl-sub">
              <Icon
                className="noticicon"
                color="#36c"
                size={15}
                icon="flight"
              /> Booking details-
              <span className="t-color3 font-14">
                {this.getTravellers(this.props.data.travellers)}
              </span>
            </h4>
          ) : null}

          <div className="trip-sub ">
            {ele.is_insured ? (
              <Tooltip
                content={
                  <ul>
                    <li>
                      <span>Product</span> :
                      {
                        this.props.InsuranceMasterData[
                          ele.insured.insurance_master_id
                        ]
                      }
                    </li>
                    <li>
                      <span>Policy </span> : {typeof this.props.data.insurances[0].status!=='undefined' && this.props.data.insurances[0].status!==null?this.props.InsuranceStatusMasterData[this.props.data.insurances[0].status]:"-"}
                    </li>
                    <li>
                      <span>Date of birth</span> : {ele.insured.date_of_birth}
                    </li>
                    <li>
                      <span>Price</span> :
                      {utility.PriceFormat(
                        ele.insured.net_premium,
                        ele.currency
                      )}
                    </li>
                  </ul>
                }
                direction="right"
                tagName="span"
                className="target"
              >
                <a id="priceId" className="linkRight">
                  Insured
                </a>
              </Tooltip>
            ) : null}
            {ele.title}. {ele.first_name} {ele.last_name} (
            {utility.PriceFormat(ele.total_fare, this.currency)})
          </div>
        </div>

        <div className="resTable overflow-x-auto mb-10">
          <table className="dataTable3 elpstable scrlTable">
            <thead>
              <tr>
                <th width="10%">Sector</th>
                <th width="10%">Status</th>
                <th width="8%">Airline PNR</th>
                <th width="8%">Split PNR</th>
                <th width="6%">GDS PNR</th>
                <th width="8%">Fare Class</th>
                <th width="10%">Fare Basis Code</th>
                <th width="6%">Ticket#</th>
                <th width="14%">BF + Airline Tax</th>
                <th width="10%">Booking Type</th>
                <th width="10%">Cost Pricing</th>
              </tr>
            </thead>
            <tbody>
              {ele.air_booking_infos.map(info => (
                <tr
                  className={
                    this.props.bkngSatatusMaster[info.booking_status] ===
                      "CANCELLED" ||
                    this.props.bkngSatatusMaster[info.booking_status] ===
                      "REFUNDED"
                      ? "des cancel"
                      : null
                  }
                  key={info.id}
                >
                  <td>
                    <span>
                      {info.departure_airport} - {info.arrival_airport}
                    </span>
                  </td>
                  <td>
                    <span className="n-strk">
                      {this.getBookingStatus(info.booking_status, info)}
                    </span>
                  </td>
                  <td>
                    <span>
                    {typeof info.airline_pnr !== "undefined" ? (
                        info.airline_pnr
                      ) : (
                        "..."
                      )}</span>
                  </td>
                  <td>...</td>
                  <td>
                    
                    { typeof info.gds_pnr !== 'undefined' && info.gds_pnr !== '' &&  info.gds_pnr !== null ? 
                       <span> {info.gds_pnr}</span>
                       : 
                        "..."
                      }
                    
                  </td>
                  <td>
                    <span>{info.booking_class}</span>
                  </td>
                  <td>
                    <div className="w-115">
                      <span
                        title={
                          typeof info.pricing_objects !== "undefined"
                            ? info.pricing_objects.fare_basis_code
                            : info.fare_basis_code
                        }
                      >
                        {typeof info.pricing_objects !== "undefined"
                          ? info.pricing_objects.fare_basis_code
                          : info.fare_basis_code}
                      </span>
                    </div>
                  </td>
                  <td>
                    <span>{info.ticket_number}</span>
                  </td>
                  <td>
                    <span className="lnkLine">
                      {typeof info.pricing_objects !== "undefined" ? (
                        <FlightFareBreakup data={info.pricing_objects} />
                      ) : (
                        "..."
                      )}
                    </span>
                  </td>

                  <td>
                    {typeof info.pricing_objects !== "undefined" ? (
                      <span className="n-strk">
                        {info.pricing_objects.fare_sub_type === null
                          ? "Retail Fare Booking"
                          : info.pricing_objects.fare_sub_type +
                            " FARE BOOKING"}
                      </span>
                    ) : (
                      "..."
                    )}
                  </td>
                  <td>
                    <span className="lnkLine">
                      {typeof info.pricing_objects !== "undefined" ? (
                        <FlightCostPriceBreakup
                          data={info.pricing_objects.cost_pricing_object}
                        />
                      ) : (
                        "..."
                      )}
                    </span>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    ));
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airPortMaster: state.trpReducer.airPortMaster,
    airLineMaster: state.trpReducer.airLineMaster,
    ctConfigData: state.trpReducer.ctConfigData,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster,
    InsuranceStatusMasterData:state.trpReducer.InsuranceStatusMasterData,
    InsuranceMasterData: state.trpReducer.InsuranceMasterData
  };
};
export default connect(mapStateToProps, null)(FlightBookingDetail);
