import React, { Component } from "react";
import { Link } from "react-router-dom";
import Popup from "reactjs-popup";
import utility from "../../common/Utilities";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import FlightService from "../../../services/trips/flight/FlightService";
import Tooltip from "react-tooltip-lite";
import Icon from "Components/Icon";
//import * as actions from '../../../store/actions/index'
import converter from "number-to-words";
import Domainpath from "../../../services/commonUtils/Domainpath";

class FlightItineraryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  isAirIndiaExpress = flt => {
    for (let seg of flt.segments) {
      if (seg.marketing_airline === "IX") {
        return true;
      }
    }

    return false;
  };
  showNewPopUp(flt, e) {
    e.persist();
    let url = "";
    // console.log("e.target.id +"+ JSON.stringify(flt) )
    if (flt.air_booking_type === "I" && !this.isAirIndiaExpress(flt)) {
      url =
        Domainpath.getHqDomainPath() +
        "/hq/trips/" +
        flt.itinerary_id +
        "/get_int_fare_rule";
    } else {
      url =
        Domainpath.getHqDomainPath() +
        "/hq/trips/flight/" +
        flt.id +
        "/fare_rule";
    }

    window.open(url);
  }
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  render() {
    // console.log('currnet itidsd'+this.props.data.display_current_itinerary)
    //{this.loadTripDetails()}

    return this.props.data.flights.map(ele => {
      return (
        <React.Fragment key={ele.id}>
          <div className="heading dis-flx-btw mb-5">
            {this.props.data.flights.indexOf(ele) === 0 ? (
              <>
                <h4 className="in-tabTTl-sub">
                  <Icon
                    className="noticicon"
                    color="#36c"
                    size={15}
                    icon="flight"
                  />
                  Itinerary
                  <>
                    {this.props.data.is_amended ? (
                      <>
                        
                        <Link
                          className="font-12 linkRight"
                          onClick={this.props.showOld}
                        >
                          Show Old
                        </Link>
                      </>
                    ) : null}
                  </>
                </h4>
              </>
            ) : null}
            <div className="trip-sub">
              
              <a
                id="fareRule"
                className="linkRight"
                onClick={e => this.showNewPopUp(ele, e)}
              >
                Fare Rule
              </a>
              {FlightService.getCityFromAirPorteCode(
                this.props.airPortMaster,
                ele.departure_airport
              )}
              to
              {FlightService.getCityFromAirPorteCode(
                this.props.airPortMaster,
                ele.arrival_airport
              )}
              -
              {dateUtil.prettyDate(ele.departure_date_time, "ddd, DD MMM YYYY")}
            </div>
          </div>
          <div className="resTable">
            <table className="dataTable3">
              <thead>
                <tr>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="10%">Airlines</th>
                  <th width="15%">Flight</th>
                  <th width="15%">Duration</th>
                  <th width="10%">Stops</th>
                  <th width="10%">Layover</th>
                  <th width="10%">Class</th>
                  <th width="10%">Supplier</th>
                </tr>
              </thead>
              <tbody>
                {ele.segments.map(seg => (
                  <tr key={seg.id}>
                    <td className="pr-10">
                        <p>
                      <span className="d-b mb-3 t-color1">
                        {FlightService.getCityFromAirPorteCode(
                          this.props.airPortMaster,
                          seg.departure_airport
                        )}
                      </span>
                      <span className="d-b">                        
                        {FlightService.getAirPortNameFromAirPorteCode(
                          this.props.airPortMaster,seg.departure_airport
                        )} ({seg.departure_airport})
                      </span>
                      <span className="t-color2 font-12 d-ib">
                        {dateUtil.prettyDate(
                          seg.departure_date_time,
                          "HH:mm  DD MMM"
                        )}
                      </span>
                      <span className="t-color3 font-12 d-b">
                        
                        {seg.departure_terminal !== null
                          ? "Terminal -" + seg.departure_terminal
                          : ""}
                      </span></p>
                    </td>
                    <td className="pr-10">
                      <span className="d-b mb-3 t-color1">
                        {FlightService.getCityFromAirPorteCode(
                          this.props.airPortMaster,
                          seg.arrival_airport
                        )}
                      </span>
                      <span className="d-b">
                        {FlightService.getAirPortNameFromAirPorteCode(
                          this.props.airPortMaster,
                          seg.arrival_airport
                        )}
                        ({seg.arrival_airport})
                      </span>
                      <span className="t-color2 font-12 d-ib">
                        {dateUtil.prettyDate(
                          seg.arrival_date_time,
                          "HH:mm   DD MMM"
                        )}
                      </span>
                      <span className="t-color3 font-12 d-b">
                        
                        {seg.arrival_terminal !== null
                          ? "Terminal -" + seg.arrival_terminal
                          : ""}
                      </span>
                    </td>

                    <td>
                      {FlightService.getAirlineNameFromAirLineCode(
                        this.props.airLineMaster,
                        seg.operating_airline
                      )}
                    </td>

                    <td>
                      {seg.operating_airline} - {seg.flight_number}
                    </td>
                    <td>{dateUtil.secondsToHm(seg.duration)}</td>
                    <td>{converter.toWords(seg.stopover_count)}</td>
                    <td>
                      {typeof seg.lay_over !== "undefined"
                        ? dateUtil.msToTime(seg.lay_over)
                        : "-"}
                    </td>
                    <td>{this.props.cabinTypeMaster[seg.cabin_type]}</td>
                    <td>{this.props.FlightSupplierMasterData[seg.supplier]}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </React.Fragment>
      );
    });
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airPortMaster: state.trpReducer.airPortMaster,
    airLineMaster: state.trpReducer.airLineMaster,
    FlightSupplierMasterData: state.trpReducer.FlightSupplierMasterData,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster
  };
};
export default connect(mapStateToProps, null)(FlightItineraryDetail);
