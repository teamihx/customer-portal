import React, { Component } from 'react';
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import Button from 'Components/buttons/button.jsx'
import ToggleDisplay from 'react-toggle-display';
import { Link } from 'react-router-dom';
import log from 'loglevel';
import FlightSMS from "./FlightSMS";
import Notification from 'Components/Notification.jsx';
import FlightEmailDetails from './FlightEmailDetails';
import FlightEmailReceipt from './FlightEmailReceipt';
export const contextPath=process.env.contextpath;
import FlightPricingDetail from "./FlightPricingDetail";
import FlightAmendmentPricingDetail from "./FlightAmendmentPricingDetail";
import ChangeBkgStatusClsTxn from "../generic/ChangeBkgStatusClsTxn";
import Tags from 'Pages/trips/generic/Tags.jsx';
import ShowHide from 'Components/ShowHide.jsx';
import FlightSalesReceipt from './FlightSalesReceipt';
import FlightEmailETicket from './FlightEmailETicket';
import FlightEmailVATInvoice from './FlightEmailVATInvoice';
import EmailBookStepScreenShot from '../generic/EmailBookStepScreenShot'
import TripGenericService from '../../../services/trips/generic/TripGenericService'
import SourceTypeInfo from '../generic/SourceTypeInfo'
import FlightService from '../../../services/trips/flight/FlightService.js'
import TripAmendment from '../generic/TripAmendment.jsx';
import loader1 from 'Assets/images/loader1.gif';
export const TRIPS_ROLES = 'tripsRoles'
import Domainpath from '../../../services/commonUtils/Domainpath';
class FlightRightSidebar extends Component {
  constructor(props) {
    super(props)
    //let tripid = [];
    this.redirect=this.redirect.bind(this);
    this.state = {
      //fphRS: this.props.paxData,
      tripid: this.props.tripDetail.trip_ref,
      tripDetail:this.props.tripDetail,
      show: false,
	  emailDetailsShow: false,
	  emailReceiptShow: false,
 	  emailDetailsContent: '',
      loading:false,
      mobileNum:"",
      showMessage:false,
      sucessmsg:false,
      smsData:'',
      buttonclick:false,
	  emailSalesReceiptShow:false,
	  emailTicketShow:false,
 	  emailVATInvoiceShow:false,
	  emailShowBookStepSceenShow:false,
      pricingObject:{},
	  ticketUrl : Domainpath.getHqDomainPath()+'/ticket',
	  openTxnAvailable:false,
	  ticketAvailable:false,
	  vatInvoiceApplicatable:false,
    receiptAvailable:false,
    enableSyncGDS:false,
    showGdsSuccessMsg:false,
    showGdsFailMsg:false,
    loadLoader:false,
    triplinkRoles:''
    }
    this.handleChange = this.handleChange.bind(this)
    this.sendFltSMS = this.sendFltSMS.bind(this)
 	this.renderComponents = this.renderComponents.bind(this)
  
  this.enableSyncGDS();
  //Getting ROLES DATA from local storage
  let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
  this.state.triplinkRoles=tripsObj.ftriplinks;
  }

	checkIfOpenTxn=()=>{
	this.state.openTxnAvailable = TripGenericService.checkForOpenTxnAvailable(this.props.tripDetail.txns);
	for(let air_booking_info in this.props.tripDetail.air_bookings[0].air_booking_infos){
		let air_boookingInfoData = this.props.tripDetail.air_bookings[0].air_booking_infos[air_booking_info];
		if(air_boookingInfoData.ticket_number!==null && air_boookingInfoData.ticket_number!==undefined && air_boookingInfoData.ticket_number!==''){
			this.state.ticketAvailable = true;
			break;
			
		}
		
	}
	let country_code = TripGenericService.getDomain(this.props.tripDetail.domain);
	if(country_code!==null && country_code!==undefined && country_code!==''){
		if(country_code ==='ae' || country_code ==='bh' || country_code ==='bh' || country_code ==='sa'){
			this.state.vatInvoiceApplicatable = true;
		}
		
	}
	
	for(let invoices in this.props.tripDetail.invoices){
		let invoiceData = this.props.tripDetail.invoices[invoices];
		if(invoiceData.category!==null && invoiceData.category!==undefined && invoiceData.category!==''){
			if(invoiceData.category==='PaymentReceipt'){
				this.state.receiptAvailable=true;
				break;
			}
		}
	}
	
	
	}
	redirect(event){

        try{
        event.preventDefault();
        let url="";
        var domainpath = Domainpath.getHqDomainPath();
        if(event.target.id === 'bookInsurance'){
          url = domainpath+"/hq/trips/"+this.state.tripid+"/book_insurance/"
        }else if(event.target.id === 'lossTracker'){
          url = domainpath+"/hq/trips/"+this.state.tripid+"/loss_tracker/"
        }else if(event.target.id === 'amendmnt'){
         url = domainpath+"/hq/trips/"+this.state.tripid+"/amend_air/"        
        }else if(event.target.id === 'xml'){
          url =domainpath+"/hq/trips/"+this.state.tripid+"/xml"
        }else if(event.target.id === 'tripCancel'){
          url =domainpath+"/hq/trips/"+this.state.tripid+"/cancel/"
        }else if(event.target.id === 'onlineamendmnt'){
          url =domainpath+"/hq/trips/"+this.state.tripid+"/reschedule/intro"
        }else if(event.target.id === 'bookstepScreen'){
          url =domainpath+"/hq/trips/"+this.state.tripid+"/screenshots"
          //window.location.replace(url);
        }else if(event.target.id === 'printReceipt'){
		 url =domainpath+"/hq/trips/"+this.state.tripid+"/receipt?air_receipt=true"
		}else if(event.target.id === 'printSaleinvoice'){
		 url =domainpath+"/hq/trips/"+this.state.tripid+"/sale_invoice"
		}else if(event.target.id === 'printVATinvoice'){
		 url =domainpath+"/hq/trips/"+this.state.tripid+"/invoice"
		}else if(event.target.id === 'PrintCancellationinvoice'){
            url = domainpath+"/hq/trips/"+this.state.tripid+"/cancellation_invoice"
          }
          window.open(
            url
          );
        }catch(err){

            log.error('Exception occured in Footer redirect function---'+err);

        }

    }
    handleChange(event) {
      this.setState(
          {
              [event.target.name]
                  : event.target.value
          }
      )
  }
  
  handleClick() {
    this.setState({
      show: !this.state.show,
    });
    if(this.state.tripDetail.contact_detail.mobile!==null && this.state.tripDetail.contact_detail.mobile!==undefined){
    this.state.mobileNum=this.state.tripDetail.contact_detail.mobile;
    }else{
    this.state.mobileNum=this.state.tripDetail.contact_detail.mobile_number;
    }
  }

	renderComponents(e) {
        this.currentClick = e.target.id;
      
        if (this.currentClick === 'flightemailDtls') {
            this.setState({
      				emailDetailsShow: !this.state.emailDetailsShow,
    		});
        }else if (this.currentClick === 'flightemailReceipt') {
			this.setState({
      				emailReceiptShow: !this.state.emailReceiptShow,
    		});
		}else if (this.currentClick === 'flightemailSalesReceipt') {
			this.setState({
      				emailSalesReceiptShow: !this.state.emailSalesReceiptShow,
    		});
		}else if (this.currentClick === 'flightemaiTicket') {
			this.setState({
      				emailTicketShow: !this.state.emailTicketShow,
    		});
		}else if (this.currentClick === 'flightemailVATInvoice') {
			this.setState({
      				emailVATInvoiceShow: !this.state.emailVATInvoiceShow,
    		});
		}else if (this.currentClick === 'flightemailBKStepScreen') {
			this.setState({
      				emailShowBookStepSceenShow: !this.state.emailShowBookStepSceenShow,
    		});
		}  
		  
        

    }

  sendFltSMS(event){
    try{
    if(this.state.mobileNum!==""){
      this.state.buttonclick=true;
      //console.log('Entered Mobile number :'+this.state.mobileNum);
      this.forceUpdate();
      this.setState({smsSend:true});
    }else{
      this.setState({showMessage:true, smsSend:false});
      
    }
  }catch(err){
    this.setState({smsSend:false});
    log.error('Exception occured in Send SMS function---'+err);
  }
}

processSyncGDS(){
  try{
    this.setState({ loadLoader:true});
    //console.log("processSyncGDS start "+this.state.tripDetail.trip_ref);
    FlightService.getSyncGDS(this.state.tripDetail.trip_ref).then(response =>{
      var obj=JSON.parse(response.data);
      //console.log("processSyncGDS response: "+JSON.stringify(response.data));
      if (obj !== undefined && obj !== "" && obj.responsecode !== undefined && obj.responsecode !== "" && obj.responsecode === 200) {
          //console.log("processed SyncGDS Successfully "+obj.responsecode);
          this.setState({ showGdsSuccessMsg:true, showAlert: true}); 
        }else{
          this.setState({ showGdsFailMsg:true, showAlert: true}); 
        }
        this.setState({ loadLoader:false});
    });
  }catch(err){
    log.error('Exception occured in processSyncGDS function---'+err);
    this.setState({ showGdsFailMsg:true, loadLoader:false, showAlert: true}); 
  }

}

enableSyncGDS(){
  try{
    let tripBookingInfos=[];
    tripBookingInfos=this.state.tripDetail.air_bookings[0].air_booking_infos;
    if(tripBookingInfos){
       for(let value of tripBookingInfos){
          if(value.gds_pnr!==null && value.gds_pnr!==undefined && (value.ticket_number===null || value.ticket_number===undefined)){
            this.state.enableSyncGDS=true;
            break;
          }
       }
    } 
  }catch(err){
    log.error('Exception occured in enableSyncGDS function---'+err);
  }
}
toggleAlert = () => {
  this.setState({
    showAlert: false
  });
};

amendmentAvlble=()=>{
  let air_bookings = this.props.tripDetail.air_bookings[0];  
  if(air_bookings.amend_itinerary_id!==null || (air_bookings.air_booking_info_history!==null && air_bookings.air_booking_info_history.length>0)){
    return true;
  }
  return false;
}


  render() {
    let viewType, messageText;
    {this.checkIfOpenTxn()}
    if (this.state.showGdsFailMsg) {
      viewType = "error";
      messageText ="PNR Sync failed";
    } else if (this.state.showGdsSuccessMsg) {
      viewType = "success";
      messageText = "PNR Sync Successfully.";
    }
    let tripJsonUrl=contextPath+"/trips/"+this.state.tripDetail.trip_ref+"/json"
    
    return (
      <>

        {viewType && messageText && (
            <Notification
              viewType={viewType}              
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}

          {this.state.buttonclick && (
            <FlightSMS  getMobileNumber={this.state.mobileNum} />
          )}
          
          <div className="container"> 
        
          {this.state.showMessage && <div className="alert alert-error"><Icon className="noticicon" color="#F4675F" size={16} icon="warning"/>Enter mobile number</div>}
          </div>
          <div className="side-pnl">
          <ShowHide visible="true" title="Tips, tools & extras">
          <div className="showHide-content">
           <ul className="mb-20">
			{this.state.triplinkRoles.EMAIL_DT_ROLE_ENABLE && !this.state.openTxnAvailable && this.props.tripDetail.air_bookings[0].booking_status==='P' && this.state.ticketAvailable &&
            <li>
			<a id="flightemailDtls" onClick={this.renderComponents} className={this.currentClick === "flightemailDtls" ? "active" : ""}>
			<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Trip Details</a>
			 <ToggleDisplay show={this.state.emailDetailsShow}>
				<FlightEmailDetails />
			</ToggleDisplay>	
				
			</li>            
            }
			{this.state.triplinkRoles.EMAIL_RECE_ROLE_ENABLE &&  !this.state.openTxnAvailable && this.props.tripDetail.air_bookings[0].booking_status!=='Z' && this.props.tripDetail.air_bookings[0].booking_status!=='H' && this.state.receiptAvailable &&
			<li>
			<a id="flightemailReceipt" onClick={this.renderComponents} className={this.currentClick === "flightemailReceipt" ? "active" : ""}>
			<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Receipt</a>
			 <ToggleDisplay show={this.state.emailReceiptShow}>
				<FlightEmailReceipt />
			</ToggleDisplay>
			</li>
			 }
			{this.state.triplinkRoles.SALE_RECE_ROLE_ENABLE && !this.state.openTxnAvailable && !this.state.vatInvoiceApplicatable && this.props.tripDetail.air_bookings[0].booking_status!=='Z' && this.props.tripDetail.air_bookings[0].booking_status!=='H' &&
			<li>
			<a id="flightemailSalesReceipt" onClick={this.renderComponents} className={this.currentClick === "flightemailReceipt" ? "active" : ""}>
			<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Sale invoice</a>
			 <ToggleDisplay show={this.state.emailSalesReceiptShow}>
				<FlightSalesReceipt />
			</ToggleDisplay>
			</li>
			 }
			{this.state.triplinkRoles.EMAIL_TKT_ROLE_ENABLE && !this.state.openTxnAvailable && this.state.ticketAvailable && this.props.tripDetail.air_bookings[0].booking_status==='P' &&
			<li>
			<a id="flightemaiTicket" onClick={this.renderComponents} className={this.currentClick === "flightemailReceipt" ? "active" : ""}>
			<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email e-tickets</a>
			 <ToggleDisplay show={this.state.emailTicketShow}>
				<FlightEmailETicket />
			</ToggleDisplay>	
			</li>
			}
			{this.state.triplinkRoles.EMAIL_VAT_INV_ROLE_ENABLE && !this.state.openTxnAvailable && this.state.vatInvoiceApplicatable && this.props.tripDetail.air_bookings[0].booking_status!=='Z' && this.props.tripDetail.air_bookings[0].booking_status!=='H' && this.state.ticketAvailable &&
			<li>
			<a id="flightemailVATInvoice" onClick={this.renderComponents} className={this.currentClick === "flightemailReceipt" ? "active" : ""}>
			<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email VAT Invoice</a>
			 <ToggleDisplay show={this.state.emailVATInvoiceShow}>
				<FlightEmailVATInvoice />
			</ToggleDisplay>
			</li>
			}
      {this.state.triplinkRoles.EMAIL_BK_STRP_ROLE_ENABLE &&
			<li>
			<a id="flightemailBKStepScreen" onClick={this.renderComponents} className={this.currentClick === "flightemailReceipt" ? "active" : ""}>
			<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Bookstep Screenshots</a>
			 <ToggleDisplay show={this.state.emailShowBookStepSceenShow}>
				<EmailBookStepScreenShot />
			</ToggleDisplay>	
			</li>
      }

            {this.state.triplinkRoles.SYNC_GDS_ROLE_ENABLE && this.state.enableSyncGDS && (
              <li>
              <a id="syncGDSId" title="Sync ticket# from GDS" onClick={ () => this.processSyncGDS() } >
                <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Sync ticket# from GDS  {this.state.loadLoader && (
               <img 
                src={loader1}
                className="loader" 
                alt="loader"  
                width="12px"
                />
              )}
              </a>
             
              </li>
            )}
			 {this.state.triplinkRoles.SMS_ROLE_ENABLE && (
           <li>
              <a  onClick={ () => this.handleClick() }> <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>SMS trip details</a>
            <ToggleDisplay className="test123s" show={this.state.show}>
            <div className="row">
                  <div className="col-9 form-group">
                  <input type="text"  name="mobileNum" value={this.state.mobileNum} onChange={this.handleChange} />
					       </div>	
                <div className="col-3 pl-0 mob-txt-right">
                <Button
                      size="xs"
                      viewType="primary"                      
                      onClickBtn={this.sendFltSMS}
                      //loading = {this.state.smsSend}
                      btnTitle="Send"
                      />
                </div>
              </div>
            </ToggleDisplay>
              </li>
            )}

				{this.state.triplinkRoles.PRINT_RECE_ROLE_ENABLE && !this.state.openTxnAvailable && this.props.tripDetail.air_bookings[0].booking_status!=='Z' && this.props.tripDetail.air_bookings[0].booking_status!=='H' && this.state.receiptAvailable &&
			   <li><a id="printReceipt"  href="/" onClick={this.redirect} title="Print Receipt">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print Receipt</a></li>
				}
				{this.state.triplinkRoles.PRINT_SALE_INV_ROLE_ENABLE && !this.state.openTxnAvailable && !this.state.vatInvoiceApplicatable && this.props.tripDetail.air_bookings[0].booking_status!=='Z' && this.props.tripDetail.air_bookings[0].booking_status!=='H' && 
			  <li><a id="printSaleinvoice"  href="/" onClick={this.redirect} title="Print Sale invoice">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print Sale invoice</a></li>
				}
				{this.state.triplinkRoles.PRINT_VAT_INV_ROLE_ENABLE && !this.state.openTxnAvailable && this.state.vatInvoiceApplicatable && this.props.tripDetail.air_bookings[0].booking_status!=='Z' && this.props.tripDetail.air_bookings[0].booking_status!=='H' &&
			  <li><a id="printVATinvoice"  href="/" onClick={this.redirect} title="Print VAT Invoice">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print VAT Invoice</a></li>
				}
				{this.state.triplinkRoles.PRINT_CAN_INV_ROLE_ENABLE && !this.state.openTxnAvailable && this.props.tripDetail.air_bookings[0].booking_status==='K' &&
				<li><a id="PrintCancellationinvoice" href="/" onClick={this.redirect} title="Print Cancellation invoice">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print Cancellation invoice</a></li>
				}
				{this.state.triplinkRoles.PRINT_ETKT_ROLE_ENABLE && !this.state.openTxnAvailable && this.state.ticketAvailable && this.props.tripDetail.air_bookings[0].booking_status==='P' &&
				<li>
				<form id="eticketForm" target="_blank" action={this.state.ticketUrl} method="post">
				<input type="hidden" name="confirmation_number" value={this.state.tripDetail.trip_ref} />
				<input type="hidden" name="last_name" value={this.state.tripDetail.air_bookings[0].pax_infos[0].last_name} />	
				 <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>			
				<input className="btn btn-text link-Color no-text-decr" type="submit" value="&nbsp;Print e-tickets"/>		
				</form>
				</li>            
               }
         {this.state.triplinkRoles.LOSS_TRCR_ROLE_ENABLE && 
               <li><a id="lossTracker"  href="/loss_tracker" target="_blank" onClick={this.redirect} title=" Loss Tracker For trip">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Loss Tracker For trip</a></li>
        }
        {this.state.triplinkRoles.INSU_ROLE_ENABLE &&
        <li><a id="bookInsurance"  href="/book_insurance" target="_blank" onClick={this.redirect} title="Book Insurance">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Book Insurance</a></li>
        }
        {this.state.triplinkRoles.BK_STRP_SCREEN_ROLE_ENABLE &&
        <li><a id="bookstepScreen"  href="/" onClick={this.redirect} title="Bookstep Screenshots">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Bookstep Screenshots</a></li>
        }     
        {/* <li><a href=""> <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Reward program</a></li> */}
        {this.state.triplinkRoles.XML_ROLE_ENABLE &&
        <li><a id="xml"  href="/xml_tracker" target="_blank"   onClick={this.redirect} title="Trip XML">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Trip XML</a></li>
        }
        {this.state.triplinkRoles.JSON_ROLE_ENABLE &&
               <li>
                <a id="json"  href={tripJsonUrl}  target="_blank" title="Trip JSON"><Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Trip JSON</a></li>
        }
               </ul>

               <>
               {this.state.triplinkRoles.CHANGE_BK_STATUS_CLSTXN_ROLE_ENABLE &&
               <ChangeBkgStatusClsTxn tripId={this.state.tripDetail.trip_ref}/>
               }
                </> 

               {this.state.triplinkRoles.CAN_TRIP_CALCREFU_ROLE_ENABLE &&
               <div>
               {this.props.tripDetail.air_bookings[0].booking_status==='P'?<div className="amndmnt mt-10 pl-20 pr-20">  
                              <a id="tripCancel" className="btn btn-default btn-xs"  href="/cancel" target="_blank" onClick={this.redirect} title="Cancel trip/Calculate refund"> Cancel trip/Calculate refund</a>
                             </div>:''} 
               </div>
                }
                </div>
                </ShowHide>
               </div>
               
               <TripAmendment/>

               <SourceTypeInfo/>
               {this.state.triplinkRoles.TAGS_ROLE_ENABLE &&
                <Tags/>
               }
               {this.state.triplinkRoles.PRICE_DT_ROLE_ENABLE &&
                <FlightPricingDetail />
               }
               {this.amendmentAvlble() &&
                <FlightAmendmentPricingDetail/>
               }
             
           </>

    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
}
export default connect(mapStateToProps, null)(FlightRightSidebar);