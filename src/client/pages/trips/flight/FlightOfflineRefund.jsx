import React, { Component } from 'react'
import Button from 'Components/buttons/button.jsx'
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import FlightService from '../../../services/trips/flight/FlightService.js';
import log from 'loglevel';
export const TRIPS_ROLES = 'tripsRoles';

class FlightOfflineRefund extends Component {

    constructor(props) {
        super(props)
       
        this.state = {

            responseData: this.props.tripDetail,            
            refundticketDetails:'',
            refundtktDtlsList:[],
            responseDatabkp:'',
            loading:false,
            updateTktRes:'',
            sucessmsg:'',
            errormsg:'',
            amtexceedmsg:[],
            newNote:'',
            refundTxn:'',
            ftripsPermissions:''
          
        }
        
        this.pgFeehandleChangeEvent = this.pgFeehandleChangeEvent.bind(this);
        this.supCanAmthandleChangeEvent = this.supCanAmthandleChangeEvent.bind(this);
        this.ctCanAmthandleChangeEvent = this.ctCanAmthandleChangeEvent.bind(this);
        this.updateRefundDetails = this.updateRefundDetails.bind(this);
		this.initiateRefund = this.initiateRefund.bind(this);
		this.handleChange = this.handleChange.bind(this);
		
        //console.log('this.props.tripDetail---'+JSON.stringify(this.props.tripDetail));        
        this.loadOfflineRefundDetails();

        //Getting ROLES DATA from local storage
        let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
        this.state.ftripsPermissions=tripsObj.ftripPermissions;
        
    }




    loadOfflineRefundDetails=()=>{
        this.state.refundtktDtlsList =[];
        let refundtktDetData=[];
        var tempTripDtls = this.state.responseData;
        this.state.responseDatabkp=this.state.responseData;
        try{
        var airbookingInfo = tempTripDtls.air_bookings[0].air_booking_infos;
        var paxList = tempTripDtls.air_bookings[0].pax_infos;
        var flights = tempTripDtls.air_bookings[0].flights;
        var pricingList = tempTripDtls.air_bookings[0].pricing_objects;
        var currency =tempTripDtls.currency;
        let noofPax=tempTripDtls.travellers;
        let bkngstatus=tempTripDtls.air_bookings[0].booking_status;
        let total_fare=tempTripDtls.air_bookings[0].total_fare;
        let trip_id=tempTripDtls.id;
        let txnsData = this.state.responseData.txns.filter( function (txns) {
          return txns.air_refund_records!==null ? txns.air_refund_records.length > 0:''
        });   
      
        if(txnsData!==null && txnsData!==undefined){
         this.state.refundTxn = txnsData;
        }
        
        if(paxList){
        for(let pax of paxList){
            var paxDtl = new Object(); 
            var segList =[];
            var airpricingids =[];
            paxDtl.pax_info_id=pax.id
            paxDtl.title =pax.title;
            paxDtl.pax_type_code =pax.pax_type_code;
            paxDtl.first_name =pax.first_name;
            paxDtl.last_name =pax.last_name;
            paxDtl.totalAmt =pax.total_fare;
           

            if(airbookingInfo){
              var airbookingInfoList =  airbookingInfo.filter(air => air.pax_info_id === pax.id);
            if(airbookingInfoList){
                for(let flt of airbookingInfoList){
                   var fltdtls = new Object();
                   fltdtls.id=flt.id
                   fltdtls.segment_id=flt.segment_id;
                   fltdtls.pax_info_id=flt.pax_info_id;
                   fltdtls.pricingObjid=flt.pricing_object_id;
                   fltdtls.pg_fee = '';
                   fltdtls.sup_can_amt = '';
                   fltdtls.ct_can_amt = '';                   
                   fltdtls.isinput='false';
                  let pricedat = pricingList.filter(priceobj => priceobj.id === flt.pricing_object_id);
                  fltdtls.segFare=pricedat[0].total_fare;
                  fltdtls.gw_Fee=pricedat[0].total_fee_gw;
                  let refundData=this.getCTCancelAmountSUPCancelAmount(flt.id,flt.segment_id);
                  //console.log('refundData  is---'+JSON.stringify(refundData));
                  //console.log('ct_cancel_Amount  is---'+refundData.ct_cancel_Amount);

                  //console.log('pricedat is---'+JSON.stringify(pricedat));
                  if(refundData!==null && refundData!==undefined && refundData!=''){                  
                  fltdtls.sup_can_charges = refundData.sup_cancel_amount;
                  fltdtls.ct_can_charges = refundData.ct_cancel_Amount;
                  }else{
                    fltdtls.sup_can_charges = 0;
                    fltdtls.ct_can_charges = 0;

                  } 

                  const exists = airpricingids.some(v => (v=== flt.pricing_object_id));
                  const exists1= pricingList.some(v1 => (v1.id=== flt.pricing_object_id));

                  if(exists1 && !exists){
                    airpricingids.push(flt.pricing_object_id);
                    fltdtls.isinput='true';                   

                  }
                  
                   if(flights){
                       for(let seg of flights){
                        var dat = seg.segments.filter(segmnt => segmnt.id === flt.segment_id);
                     if(dat != '' && dat != 'undefined'){
                        fltdtls.sector= dat[0].departure_airport+"-"+dat[0].arrival_airport;
                      }
                    }
                   }
                   segList.push(fltdtls);
                }
                paxDtl.segList = segList;
                this.state.refundtktDtlsList.push(paxDtl);  
                refundtktDetData.push(paxDtl);
            }
            }
        }
    }

    this.tktData = {
        no_of_pax: noofPax,
        bkng_status: bkngstatus,
        currency:currency,
        bkng_total_fare:total_fare,
        trip_id:trip_id,
        refund_tkt_Dtls_List:refundtktDetData
      };
      this.state.refundticketDetails=this.tktData;
      //console.log('Constructed Json is----'+JSON.stringify(this.state.refundticketDetails));
    }catch(err){

    log.error('Exception occured in FlightOfflineRefund Component loadOfflineRefundDetails function---'+err);

    }
    }

    getCTCancelAmountSUPCancelAmount(bookinfo_id,segment_id){  

      this.refund_data='';
      try{
      if(this.state.refundTxn!==null && this.state.refundTxn!==undefined && this.state.refundTxn!==''){
      for(let refundTxns of this.state.refundTxn){
       // console.log('for loop enetered---');
        for(let air_refund_records of refundTxns.air_refund_records){
          //console.log('for loop enetered1---');
        let air_refund_rcd = new Object(); 
        air_refund_rcd = air_refund_records;
          
        let air_cancellationsdata = refundTxns.air_cancellations.filter( function (air_cancellations) {
                    return air_cancellations.air_refund_record_id === air_refund_records.id
                  });
        let bookingInfoId = air_cancellationsdata[0].air_booking_info_id;
        
        let bookInfos = this.state.responseData.air_bookings[0].air_booking_infos.filter( function (airbookInfo) {
                    return airbookInfo.id === bookingInfoId
                  });
          let segmentId = bookInfos[0].segment_id;
      if(bookingInfoId===bookinfo_id && segmentId===segment_id){
        
        let ctCancelAmt= air_refund_records.total_ct_charge;
        let supCancelAmt= air_refund_records.total_sup_charge;

        this.refund_data = {
          ct_cancel_Amount: ctCancelAmt,
          sup_cancel_amount: supCancelAmt,
          seg_id:segmentId
         
        };
      }
      }
    }
    }else{
      this.refund_data = {
        ct_cancel_Amount: 0,
        sup_cancel_amount: 0,
        seg_id:segmentId
       
      };

    }
  }catch(err){

    log.error('Exception occured in FlightOfflineRefund Component getCTCancelAmountSUPCancelAmount function---'+err);

  }
  //console.log('this.refund_data---'+JSON.stringify(this.refund_data));
    return this.refund_data;
  }


    
    pgFeehandleChangeEvent = idx => evt => {
      
        let newFlyerDtls = '';
        let htlDtlListt=[];
        this.state.amtexceedmsg=[];
        let noofpax=this.state.refundticketDetails.no_of_pax;
        let bkngstatus=this.state.refundticketDetails.bkng_status;
        let currency=this.state.refundticketDetails.currency;
        let total_fare=this.state.refundticketDetails.bkng_total_fare;
        let trip_id=this.state.refundticketDetails.trip_id;
        
        this.setState({ errormsg: '',sucessmsg:'' });
        try{
          this.state.refundticketDetails.refund_tkt_Dtls_List.map((flyerDtl, sidx) => {
              var tktData = new Object(); 
              tktData.pax_info_id=flyerDtl.pax_info_id
              tktData.title=flyerDtl.title
              tktData.pax_type_code=flyerDtl.pax_type_code
              tktData.first_name=flyerDtl.first_name
              tktData.last_name=flyerDtl.last_name
              tktData.totalAmt=flyerDtl.totalAmt
  
            newFlyerDtls=flyerDtl.segList.map( (tkt, idx1) => {
              
     
               if (idx.segment_id !== tkt.segment_id || idx.pax_info_id !== tkt.pax_info_id 
                   || idx.pricingObjid !== tkt.pricingObjid){
                 return tkt;  
                  
               }else {
                
                return { 
                  ...tkt, 
                  
                  pg_fee: evt.target.value
                
                };   
               }
              
  
            })
            tktData.segList = newFlyerDtls;
            htlDtlListt.push(tktData);
            return htlDtlListt;
           });
  
            this.gsdtktData = {
            no_of_pax: noofpax,
            bkng_status: bkngstatus,
            bkng_total_fare:total_fare,
            trip_id:trip_id,
            currency:currency,
            refund_tkt_Dtls_List:htlDtlListt
          };
          this.state.refundticketDetails=this.gsdtktData;
          //console.log('this.state.refundticketDetails1----'+JSON.stringify(this.state.refundticketDetails)); 
          this.forceUpdate();
          
           
       
    }catch(err){
  
        log.error('Exception occured in FlightOfflineRefund Component pgFeehandleChangeEvent function---'+err);
  
    }
    }

    supCanAmthandleChangeEvent = idx => evt => {
      
        let newFlyerDtls = '';
        let htlDtlListt=[];
        this.state.amtexceedmsg=[];
        let noofpax=this.state.refundticketDetails.no_of_pax;
        let bkngstatus=this.state.refundticketDetails.bkng_status;
        let currency=this.state.refundticketDetails.currency;
        let total_fare=this.state.refundticketDetails.bkng_total_fare;
        let trip_id=this.state.refundticketDetails.trip_id;
        this.setState({ errormsg: '',sucessmsg:'' });
        try{
          this.state.refundticketDetails.refund_tkt_Dtls_List.map((flyerDtl, sidx) => {
              var tktData = new Object(); 
              tktData.pax_info_id=flyerDtl.pax_info_id
              tktData.title=flyerDtl.title
              tktData.pax_type_code=flyerDtl.pax_type_code
              tktData.first_name=flyerDtl.first_name
              tktData.last_name=flyerDtl.last_name
              tktData.totalAmt=flyerDtl.totalAmt
  
            newFlyerDtls=flyerDtl.segList.map( (tkt, idx1) => {
              
     
               if (idx.segment_id !== tkt.segment_id || idx.pax_info_id !== tkt.pax_info_id 
                   || idx.pricingObjid !== tkt.pricingObjid){
                 return tkt;  
                  
               }else {
                
                return { 
                  ...tkt, 
                  
                  sup_can_amt: evt.target.value
                
                };   
               }
              
  
            })
            tktData.segList = newFlyerDtls;
            htlDtlListt.push(tktData);
            return htlDtlListt;
           });
  
            this.gsdtktData = {
            no_of_pax: noofpax,
            bkng_status: bkngstatus,
            bkng_total_fare:total_fare,
            trip_id:trip_id,
            currency:currency,
            refund_tkt_Dtls_List:htlDtlListt
          };
          this.state.refundticketDetails=this.gsdtktData; 
         // console.log('this.state.refundticketDetails2----'+JSON.stringify(this.state.refundticketDetails)); 
          this.forceUpdate();
          
           
       
    }catch(err){
  
        log.error('Exception occured in supCanAmthandleChangeEvent Component supCanAmthandleChangeEvent function---'+err);
  
    }
    }

    ctCanAmthandleChangeEvent = idx => evt => {
      
        let newFlyerDtls = '';
        let htlDtlListt=[];
        this.state.amtexceedmsg=[];
        let noofpax=this.state.refundticketDetails.no_of_pax;
        let bkngstatus=this.state.refundticketDetails.bkng_status;
        let currency=this.state.refundticketDetails.currency;
        let total_fare=this.state.refundticketDetails.bkng_total_fare;
        let trip_id=this.state.refundticketDetails.trip_id;
        this.setState({ errormsg: '',sucessmsg:'' });
        try{
          this.state.refundticketDetails.refund_tkt_Dtls_List.map((flyerDtl, sidx) => {
              var tktData = new Object(); 
              tktData.pax_info_id=flyerDtl.pax_info_id
              tktData.title=flyerDtl.title
              tktData.pax_type_code=flyerDtl.pax_type_code
              tktData.first_name=flyerDtl.first_name
              tktData.last_name=flyerDtl.last_name
              tktData.totalAmt=flyerDtl.totalAmt
  
            newFlyerDtls=flyerDtl.segList.map( (tkt, idx1) => {
              
     
               if (idx.segment_id !== tkt.segment_id || idx.pax_info_id !== tkt.pax_info_id 
                   || idx.pricingObjid !== tkt.pricingObjid){
                 return tkt;  
                  
               }else {
                
                return { 
                  ...tkt, 
                  
                  ct_can_amt: evt.target.value
                
                };   
               }
              
  
            })
            tktData.segList = newFlyerDtls;
            htlDtlListt.push(tktData);
            return htlDtlListt;
           });
  
            this.gsdtktData = {
            no_of_pax: noofpax,
            bkng_status: bkngstatus,
            bkng_total_fare:total_fare,
            trip_id:trip_id,
            currency:currency,
            refund_tkt_Dtls_List:htlDtlListt
          };
          this.state.refundticketDetails=this.gsdtktData; 
          //console.log('this.state.refundticketDetails3----'+JSON.stringify(this.state.refundticketDetails)); 
          this.forceUpdate();
          
           
       
    }catch(err){
  
        log.error('Exception occured in FlightOfflineRefund Component ctCanAmthandleChangeEvent function---'+err);
  
    }
    }

  
  	updateRefundDetails(event){
      
    this.state.loading=true;
      try{
		this.state.amtexceedmsg =[];
        let pricingObjInfo = this.state.responseData.air_bookings[0].pricing_objects;
        let uiRefundDeatisl=this.state.refundticketDetails;
        let total_bkng_fare=this.state.refundticketDetails.bkng_total_fare;
		let checkIfAnyValue = false;
		let validateInput = true;
		let amountExceeded = false;
        this.state.refundticketDetails.refund_tkt_Dtls_List.map((uirefund,index) => {
          uirefund.segList.map((uiseg,uiindex) => {
       
            if((uiseg.pg_fee!==undefined && uiseg.pg_fee!=='') || 
                (uiseg.sup_can_amt!==undefined && uiseg.sup_can_amt!=='') || 
                (uiseg.ct_can_amt!==undefined && uiseg.ct_can_amt!=='') ){
				checkIfAnyValue = true;
				if(uiseg.pg_fee!==undefined && uiseg.pg_fee!==''){
					if(isNaN(uiseg.pg_fee)){
						validateInput = false;
					}
				}
					
                 
                  let SUPCanValue=-1*(uiseg.sup_can_charges);
                  let uiCTCanValue=-1*(uiseg.ct_can_charges);
                  if((uiseg.pg_fee>uiseg.gw_Fee) || (uiseg.sup_can_amt>SUPCanValue)||(uiseg.ct_can_amt>uiCTCanValue)){
                    this.state.amtexceedmsg.push('Entered ('+uiseg.sector+') amount exceeds eligible_amount') 
					 amountExceeded = true; 
                    
                    this.forceUpdate();
                    window.scrollTo(0, 0);

                  }else{

                    this.addNoteEventHandler();

                  }

            }

          })
        })
		//console.log('check Valid  '+checkIfAnyValue +' : '+ validateInput +' : amountExceeded '+!amountExceeded); 
		if(!validateInput){
			this.state.amtexceedmsg.push('A number is expected.')
		}
		if(!checkIfAnyValue){
			this.state.amtexceedmsg.push('No value are provided')
		}
		if(checkIfAnyValue && validateInput && !amountExceeded){
			this.initiateRefund();
		}
		this.state.loading=false;
        this.forceUpdate();
  
      }catch(err){
        this.setState({loading:false});
        log.error('Exception occured in FphUpdateTicketNumber Component updateAirTktDetails function---'+err);
  
      }
  
  
    }


    addNoteEventHandler(){

     // console.log('addNoteEventHandler'+JSON.stringify(this.state.newNote));
      if(this.state.newNote!==undefined  && this.state.newNote!==''){
        let trip_id=this.state.refundticketDetails.trip_id; 
        let note_data='processing balance refund Initiate Refund for'        

          const noteTobeAdded={
              notes :[
                  {
                  note: this.state.newNote,
                  parent_note_id: null,
                  //subject:this.props.subject,
                  subject:'Initiate Refund',
                  user_id:0,
                  trip_id:trip_id,
                  created_at:new Date().toUTCString()

                  } 
              ]   
          };
         // console.log( 'addNoteEventHandler note is------'+JSON.stringify(noteTobeAdded))
                    
      }
      
	}

    resetOldDetails(e){
      //console.log('resetOldDetails---')
      try{
        this.loadTicketPricingDetails();
        this.setState({ errormsg: "Pricing details has been reset! " })
        this.forceUpdate();
        window.scrollTo(0, 0);
      }catch(err){
        log.error('Exception occured in FphUpdateTicketNumber resetOldDetails function---'+err);
      }

    }

	initiateRefund(){
		let myArray = {};
		this.state.refundticketDetails.refund_tkt_Dtls_List.map((uirefund,index) => {
		
          uirefund.segList.map((uiseg,uiindex) => {
	 	if(uiseg.pg_fee!==undefined && uiseg.pg_fee!==''){
		 let gwKey ="gw_amt_"+uiseg.pricingObjid;
		 myArray[gwKey]=uiseg.pg_fee;
		
		}
        if(uiseg.sup_can_amt!==undefined && uiseg.sup_can_amt!==''){
			 let supKey ="sup_amt_"+uiseg.pricingObjid;
		 	myArray[supKey]=uiseg.sup_can_amt;
		} 
		if(uiseg.ct_can_amt!==undefined && uiseg.ct_can_amt!==''){
			 let ctKey ="ct_amt_"+uiseg.pricingObjid;
		 	myArray[ctKey]=uiseg.ct_can_amt;
		} 
		let abiKey ="abi_id_"+uiseg.pricingObjid;			
		myArray[abiKey]=uiseg.id;
		
		
		
		let noteKey ="note_"+uiseg.pricingObjid;			
		myArray[noteKey]=this.state.newNote;
			
          })

        })
		
		let addnoteKey ="add_note";			
		myArray[addnoteKey]=this.state.newNote;
		
		myArray["initiate_refund"]="on";
		myArray["tripRefNumber"]=this.state.responseData.trip_ref;
		
		//console.log('test: '+ JSON.stringify(myArray));
		
		
		FlightService.offlineRefund(myArray).then((valid) => {
	    if (valid) {
		 //console.log('changeBkgStatsClsTxn: '+JSON.stringify(valid));
		if(valid !==undefined && valid !==''){
			let response = valid;
			//console.log('description: '+response.description);
			if(response.description.includes('Operation Successful')){
				this.state.sucessmsg = "Operation Successful";
				this.forceUpdate();
			}else{
				this.setState({ errormsg: "Operation Un-Successful " })
				this.forceUpdate();
			}
			
		}
	
		}
	 	
		});
	}
	
	handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
  
      
    render() {

      
        
        return (
            <>

         {this.state.errormsg !== "" && this.state.errormsg !== undefined && (
            <div className="alert alert-danger"><Icon className="noticicon" color="#F4675F" size={16} icon="warning"/> {this.state.errormsg}</div>
          )}
        

       {this.state.amtexceedmsg !== '' && this.state.amtexceedmsg.length > 0 && <div className="alert alert-danger">
                <Icon className="noticicon" color="#F4675F" size={16} icon="warning"/> {this.state.amtexceedmsg.map((item, key) => <span key={item}>{item}</span>)}
                </div>
        } 

          {this.state.sucessmsg !== "" && this.state.sucessmsg !== undefined && (
            <div className="alert notification-success"><Icon color="#02AE79" size={16} icon="success"/> {this.state.sucessmsg}</div>
          )}
            <div>

            {(this.state.refundticketDetails.bkng_status.toUpperCase() === "P" || 
              this.state.refundticketDetails.bkng_status.toUpperCase() === "Q" || 
              this.state.refundticketDetails.bkng_status.toUpperCase() === "K" ) ? (      

          <>
            {this.state.refundticketDetails.refund_tkt_Dtls_List.map((pax, index) => (
              <div key={index}>
                <div className="destination">{pax.title} {pax.first_name}  {pax.last_name} {(pax.dob!==undefined && pax.dob!=='') &&  "(DOB : "+DateUtils.convertStringdateFormat(pax.dob)+")" } </div>
                
                
                  
                    <div className="resTable">
                    <table className="dataTbl pax-tbl dataTable5">
                      <thead>
                      <tr>
                      <th width="15%">Sector</th>
                      <th width="10%">PG FEE</th>
                      <th width="10%">Sup.Can.Amt</th>
                      <th width="10%">CT Can.Amt</th>
                      </tr>
                      </thead>
                    {pax.segList.map((tkt, idx) => (
                    <tbody key={idx}>
                    <tr key={idx}>
                     <td>
                      {tkt.sector}
                     </td>
                     
                {(tkt.isinput === "true") ? (



                  <>
                  <td>
                  <input type="text"  name="pg_fee" value={tkt.pg_fee} onChange={this.pgFeehandleChangeEvent(tkt)}  />
                  </td>
                  {(this.state.refundticketDetails.bkng_status.toUpperCase() === "K") ?
                   <>
                  <td>
                  <input type="text"  name="cashback_code" value={tkt.sup_can_amt} onChange={this.supCanAmthandleChangeEvent(tkt)}/>
                  </td>
                  <td>
                  <input type="text"  name="discount_amt" value={tkt.ct_can_amt} onChange={this.ctCanAmthandleChangeEvent(tkt)}/>
                  </td>
                  </>
                :(<><td>{"---"}</td>
                  <td>{"---"}</td></>)}
                  
                  </> ):(

                    <>
                      <td> {"---"}</td>
                      <td>{"---"}</td>
                      <td>{"---"}</td>
                      
                    </>
                  
                )}
                </tr>
                    </tbody>
                    ))}
                    </table>
                    </div>
              </div>
            ))}
		

          <div className="destination">Add a note to this trip</div>
          <div className="resTable">
            <textarea className="fullWidth h150" type="text" name="newNote" value={this.state.newNote} onChange={this.handleChange} ></textarea>
            <div className="btnSec">    
            <Button
                    size="xs"
                    viewType="default"
                    id="updatetktbtnId"
                    onClickBtn={this.resetOldDetails}                      
                    btnTitle='Cancel'>                      
                  </Button>             
                 {this.state.ftripsPermissions.UPDATE_OFFLINE_REF_PERMSN_ENABLE &&
                  <Button
                    size="xs"
                    viewType="primary"
                    id="updatetktbtnId"
                    onClickBtn={this.updateRefundDetails}
                    loading = {this.state.loading}
                    btnTitle='Initiate Refund'>
                    
                  </Button>
                  }
                  
                </div>
                </div>
            </>
            
                
          ) : ("This trip is either expired or the booking is not confirmed")}

            </div>
           
           </>

        )
    }
  
  
  }


const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,       
        bkngSatatusMaster:state.trpReducer.bkngSatatusMaster
       
    };
}
export default connect(mapStateToProps, null)(FlightOfflineRefund);