import React, { Component } from "react";
import Button from "Components/buttons/button.jsx";
import Icon from "Components/Icon";
import { connect } from "react-redux";
import FlightService from "../../../services/trips/flight/FlightService.js";
import log from "loglevel";
export const TRIPS_ROLES = "tripsRoles";

class FlightUpdatePricing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      responseData: this.props.tripDetail,
      pricingticketDetails: "",
      pricingtktDtlsList: [],
      responseDatabkp: "",
      loading: false,
      updateTktRes: "",
      sucessmsg: "",
      errormsg: "",
      ftripsPermissions: ""
    };

    this.cashBackAmthandleChangeEvent = this.cashBackAmthandleChangeEvent.bind(
      this
    );
    this.cashBackCodehandleChangeEvent = this.cashBackCodehandleChangeEvent.bind(
      this
    );
    this.diacountAmthandleChangeEvent = this.diacountAmthandleChangeEvent.bind(
      this
    );
    this.discountCodehandleChangeEvent = this.discountCodehandleChangeEvent.bind(
      this
    );
    this.markupAmthandleChangeEvent = this.markupAmthandleChangeEvent.bind(
      this
    );
    this.markupCodehandleChangeEvent = this.markupCodehandleChangeEvent.bind(
      this
    );
    this.resetOldDetails = this.resetOldDetails.bind(this);
    this.loadTicketPricingDetails();
    //Getting ROLES DATA from local storage
    let tripsObj = JSON.parse(localStorage.getItem(TRIPS_ROLES));
    this.state.ftripsPermissions = tripsObj.ftripPermissions;
  }

  loadTicketPricingDetails = () => {
    this.state.pricingtktDtlsList = [];
    let pricingtktDetData = [];
    var tempTripDtls = this.state.responseData;
    this.state.responseDatabkp = this.state.responseData;
    try {
      // console.log('Complete Trip Response is----'+JSON.stringify(this.state.responseData));
      var airbookingInfo = tempTripDtls.air_bookings[0].air_booking_infos;
      var paxList = tempTripDtls.air_bookings[0].pax_infos;
      var flights = tempTripDtls.air_bookings[0].flights;
      var pricingList = tempTripDtls.air_bookings[0].pricing_objects;
      var currency = tempTripDtls.currency;
      let noofPax = tempTripDtls.travellers;
      let bkngstatus = "";
      if (
        tempTripDtls.air_bookings[0].booking_status !== undefined &&
        tempTripDtls.air_bookings[0].booking_status !== null
      ) {
        bkngstatus = tempTripDtls.air_bookings[0].booking_status;
      }

      if (paxList) {
        for (let pax of paxList) {
          var paxDtl = new Object();
          var segList = [];
          var airpricingids = [];
          paxDtl.pax_info_id = pax.id;
          paxDtl.title = pax.title;
          paxDtl.pax_type_code = pax.pax_type_code;
          paxDtl.first_name = pax.first_name;
          paxDtl.last_name = pax.last_name;
          paxDtl.totalAmt = pax.total_fare;

          if (airbookingInfo) {
            var airbookingInfoList = airbookingInfo.filter(
              air => air.pax_info_id === pax.id
            );
            if (airbookingInfoList) {
              for (let flt of airbookingInfoList) {
                var fltdtls = new Object();
                fltdtls.id = flt.id;
                fltdtls.segment_id = flt.segment_id;
                fltdtls.pax_info_id = flt.pax_info_id;
                fltdtls.pricingObjid = flt.pricing_object_id;
                fltdtls.cashback_amt = "";
                fltdtls.cashback_code = "";
                fltdtls.discount_amt = "";
                fltdtls.discount_code = "";
                fltdtls.markup_amt = "";
                fltdtls.markup_code = "";
                fltdtls.isinput = "false";

                const exists = airpricingids.some(
                  v => v === flt.pricing_object_id
                );
                const exists1 = pricingList.some(
                  v1 => v1.id === flt.pricing_object_id
                );

                if (exists1 && !exists) {
                  airpricingids.push(flt.pricing_object_id);
                  fltdtls.isinput = "true";
                }

                //console.log('airpricingids---'+JSON.stringify(airpricingids))

                if (flights) {
                  for (let seg of flights) {
                    var dat = seg.segments.filter(
                      segmnt => segmnt.id === flt.segment_id
                    );
                    if (dat != "" && dat != "undefined") {
                      fltdtls.sector =
                        dat[0].departure_airport + "-" + dat[0].arrival_airport;
                    }
                  }
                }
                segList.push(fltdtls);
              }
              paxDtl.segList = segList;
              this.state.pricingtktDtlsList.push(paxDtl);
              pricingtktDetData.push(paxDtl);
            }
          }
        }
      }
      this.tktData = {
        no_of_pax: noofPax,
        bkng_status: bkngstatus,
        currency: currency,
        pricing_tkt_Dtls_List: pricingtktDetData
      };
      this.state.pricingticketDetails = this.tktData;
      console.log(
        "Constructed Json is----" +
          JSON.stringify(this.state.pricingticketDetails)
      );
    } catch (err) {
      log.error(
        "Exception occured in FlightUpdatePricing Component loadTicketPricingDetails function---" +
          err
      );
    }
  };

  cashBackAmthandleChangeEvent = idx => evt => {
    //console.log('cashBackAmthandleChangeEvent---'+JSON.stringify(this.state.pricingticketDetails));

    let newFlyerDtls = "";
    let htlDtlListt = [];
    let noofpax = this.state.pricingticketDetails.no_of_pax;
    let bkngstatus = this.state.pricingticketDetails.bkng_status;
    let currency = this.state.pricingticketDetails.currency;
    this.setState({ errormsg: "", sucessmsg: "" });
    try {
      this.state.pricingticketDetails.pricing_tkt_Dtls_List.map(
        (flyerDtl, sidx) => {
          var tktData = new Object();
          tktData.pax_info_id = flyerDtl.pax_info_id;
          tktData.title = flyerDtl.title;
          tktData.pax_type_code = flyerDtl.pax_type_code;
          tktData.first_name = flyerDtl.first_name;
          tktData.last_name = flyerDtl.last_name;
          tktData.totalAmt = flyerDtl.totalAmt;

          newFlyerDtls = flyerDtl.segList.map((tkt, idx1) => {
            if (
              idx.segment_id !== tkt.segment_id ||
              idx.pax_info_id !== tkt.pax_info_id ||
              idx.pricingObjid !== tkt.pricingObjid
            ) {
              return tkt;
            } else {
              return {
                ...tkt,

                cashback_amt: evt.target.value
              };
            }
          });
          tktData.segList = newFlyerDtls;
          htlDtlListt.push(tktData);
          return htlDtlListt;
        }
      );

      this.gsdtktData = {
        no_of_pax: noofpax,
        bkng_status: bkngstatus,
        currency: currency,
        pricing_tkt_Dtls_List: htlDtlListt
      };
      this.state.pricingticketDetails = this.gsdtktData;
      this.forceUpdate();

      //console.log('gds_pnr is----'+JSON.stringify(this.state.pricingticketDetails));
    } catch (err) {
      log.error(
        "Exception occured in FlightUpdatePricing Component cashBackAmthandleChangeEvent function---" +
          err
      );
    }
  };

  cashBackCodehandleChangeEvent = idx => evt => {
    //console.log('cashBackCodehandleChangeEvent---'+JSON.stringify(this.state.pricingticketDetails));

    let newFlyerDtls = "";
    let htlDtlListt = [];
    let noofpax = this.state.pricingticketDetails.no_of_pax;
    let bkngstatus = this.state.pricingticketDetails.bkng_status;
    let currency = this.state.pricingticketDetails.currency;
    this.setState({ errormsg: "", sucessmsg: "" });
    try {
      this.state.pricingticketDetails.pricing_tkt_Dtls_List.map(
        (flyerDtl, sidx) => {
          var tktData = new Object();
          tktData.pax_info_id = flyerDtl.pax_info_id;
          tktData.title = flyerDtl.title;
          tktData.pax_type_code = flyerDtl.pax_type_code;
          tktData.first_name = flyerDtl.first_name;
          tktData.last_name = flyerDtl.last_name;
          tktData.totalAmt = flyerDtl.totalAmt;

          newFlyerDtls = flyerDtl.segList.map((tkt, idx1) => {
            if (
              idx.segment_id !== tkt.segment_id ||
              idx.pax_info_id !== tkt.pax_info_id ||
              idx.pricingObjid !== tkt.pricingObjid
            ) {
              return tkt;
            } else {
              return {
                ...tkt,

                cashback_code: evt.target.value
              };
            }
          });
          tktData.segList = newFlyerDtls;
          htlDtlListt.push(tktData);
          return htlDtlListt;
        }
      );

      this.gsdtktData = {
        no_of_pax: noofpax,
        bkng_status: bkngstatus,
        currency: currency,
        pricing_tkt_Dtls_List: htlDtlListt
      };
      this.state.pricingticketDetails = this.gsdtktData;
      this.forceUpdate();

      //console.log('gds_pnr is----'+JSON.stringify(this.state.pricingticketDetails));
    } catch (err) {
      log.error(
        "Exception occured in FlightUpdatePricing Component cashBackCodehandleChangeEvent function---" +
          err
      );
    }
  };

  diacountAmthandleChangeEvent = idx => evt => {
    //console.log('diacountAmthandleChangeEvent---'+JSON.stringify(this.state.pricingticketDetails));

    let newFlyerDtls = "";
    let htlDtlListt = [];
    let noofpax = this.state.pricingticketDetails.no_of_pax;
    let bkngstatus = this.state.pricingticketDetails.bkng_status;
    let currency = this.state.pricingticketDetails.currency;
    this.setState({ errormsg: "", sucessmsg: "" });
    try {
      this.state.pricingticketDetails.pricing_tkt_Dtls_List.map(
        (flyerDtl, sidx) => {
          var tktData = new Object();
          tktData.pax_info_id = flyerDtl.pax_info_id;
          tktData.title = flyerDtl.title;
          tktData.pax_type_code = flyerDtl.pax_type_code;
          tktData.first_name = flyerDtl.first_name;
          tktData.last_name = flyerDtl.last_name;
          tktData.totalAmt = flyerDtl.totalAmt;

          newFlyerDtls = flyerDtl.segList.map((tkt, idx1) => {
            if (
              idx.segment_id !== tkt.segment_id ||
              idx.pax_info_id !== tkt.pax_info_id ||
              idx.pricingObjid !== tkt.pricingObjid
            ) {
              return tkt;
            } else {
              return {
                ...tkt,

                discount_amt: evt.target.value
              };
            }
          });
          tktData.segList = newFlyerDtls;
          htlDtlListt.push(tktData);
          return htlDtlListt;
        }
      );

      this.gsdtktData = {
        no_of_pax: noofpax,
        bkng_status: bkngstatus,
        currency: currency,
        pricing_tkt_Dtls_List: htlDtlListt
      };
      this.state.pricingticketDetails = this.gsdtktData;
      this.forceUpdate();

      //console.log('gds_pnr is----'+JSON.stringify(this.state.pricingticketDetails));
    } catch (err) {
      log.error(
        "Exception occured in FlightUpdatePricing Component diacountAmthandleChangeEvent function---" +
          err
      );
    }
  };

  discountCodehandleChangeEvent = idx => evt => {
    // console.log('discountCodehandleChangeEvent---'+JSON.stringify(this.state.pricingticketDetails));

    let newFlyerDtls = "";
    let htlDtlListt = [];
    let noofpax = this.state.pricingticketDetails.no_of_pax;
    let bkngstatus = this.state.pricingticketDetails.bkng_status;
    let currency = this.state.pricingticketDetails.currency;
    this.setState({ errormsg: "", sucessmsg: "" });
    try {
      this.state.pricingticketDetails.pricing_tkt_Dtls_List.map(
        (flyerDtl, sidx) => {
          var tktData = new Object();
          tktData.pax_info_id = flyerDtl.pax_info_id;
          tktData.title = flyerDtl.title;
          tktData.pax_type_code = flyerDtl.pax_type_code;
          tktData.first_name = flyerDtl.first_name;
          tktData.last_name = flyerDtl.last_name;
          tktData.totalAmt = flyerDtl.totalAmt;

          newFlyerDtls = flyerDtl.segList.map((tkt, idx1) => {
            if (
              idx.segment_id !== tkt.segment_id ||
              idx.pax_info_id !== tkt.pax_info_id ||
              idx.pricingObjid !== tkt.pricingObjid
            ) {
              return tkt;
            } else {
              return {
                ...tkt,

                discount_code: evt.target.value
              };
            }
          });
          tktData.segList = newFlyerDtls;
          htlDtlListt.push(tktData);
          return htlDtlListt;
        }
      );

      this.gsdtktData = {
        no_of_pax: noofpax,
        bkng_status: bkngstatus,
        currency: currency,
        pricing_tkt_Dtls_List: htlDtlListt
      };
      this.state.pricingticketDetails = this.gsdtktData;
      this.forceUpdate();

      console.log(
        "gds_pnr is----" + JSON.stringify(this.state.pricingticketDetails)
      );
    } catch (err) {
      log.error(
        "Exception occured in FlightUpdatePricing Component discountCodehandleChangeEvent function---" +
          err
      );
    }
  };

  markupAmthandleChangeEvent = idx => evt => {
    //console.log('markupAmthandleChangeEvent---'+JSON.stringify(this.state.pricingticketDetails));

    let newFlyerDtls = "";
    let htlDtlListt = [];
    let noofpax = this.state.pricingticketDetails.no_of_pax;
    let bkngstatus = this.state.pricingticketDetails.bkng_status;
    let currency = this.state.pricingticketDetails.currency;
    this.setState({ errormsg: "", sucessmsg: "" });
    try {
      this.state.pricingticketDetails.pricing_tkt_Dtls_List.map(
        (flyerDtl, sidx) => {
          var tktData = new Object();
          tktData.pax_info_id = flyerDtl.pax_info_id;
          tktData.title = flyerDtl.title;
          tktData.pax_type_code = flyerDtl.pax_type_code;
          tktData.first_name = flyerDtl.first_name;
          tktData.last_name = flyerDtl.last_name;
          tktData.totalAmt = flyerDtl.totalAmt;

          newFlyerDtls = flyerDtl.segList.map((tkt, idx1) => {
            if (
              idx.segment_id !== tkt.segment_id ||
              idx.pax_info_id !== tkt.pax_info_id ||
              idx.pricingObjid !== tkt.pricingObjid
            ) {
              return tkt;
            } else {
              return {
                ...tkt,

                markup_amt: evt.target.value
              };
            }
          });
          tktData.segList = newFlyerDtls;
          htlDtlListt.push(tktData);
          return htlDtlListt;
        }
      );

      this.gsdtktData = {
        no_of_pax: noofpax,
        bkng_status: bkngstatus,
        currency: currency,
        pricing_tkt_Dtls_List: htlDtlListt
      };
      this.state.pricingticketDetails = this.gsdtktData;
      this.forceUpdate();

      //console.log('gds_pnr is----'+JSON.stringify(this.state.pricingticketDetails));
    } catch (err) {
      log.error(
        "Exception occured in FlightUpdatePricing Component markupAmthandleChangeEvent function---" +
          err
      );
    }
  };

  markupCodehandleChangeEvent = idx => evt => {
    //console.log('markupCodehandleChangeEvent---'+JSON.stringify(this.state.pricingticketDetails));

    let newFlyerDtls = "";
    let htlDtlListt = [];
    let noofpax = this.state.pricingticketDetails.no_of_pax;
    let bkngstatus = this.state.pricingticketDetails.bkng_status;
    let currency = this.state.pricingticketDetails.currency;
    this.setState({ errormsg: "", sucessmsg: "" });
    try {
      this.state.pricingticketDetails.pricing_tkt_Dtls_List.map(
        (flyerDtl, sidx) => {
          var tktData = new Object();
          tktData.pax_info_id = flyerDtl.pax_info_id;
          tktData.title = flyerDtl.title;
          tktData.pax_type_code = flyerDtl.pax_type_code;
          tktData.first_name = flyerDtl.first_name;
          tktData.last_name = flyerDtl.last_name;
          tktData.totalAmt = flyerDtl.totalAmt;

          newFlyerDtls = flyerDtl.segList.map((tkt, idx1) => {
            if (
              idx.segment_id !== tkt.segment_id ||
              idx.pax_info_id !== tkt.pax_info_id ||
              idx.pricingObjid !== tkt.pricingObjid
            ) {
              return tkt;
            } else {
              return {
                ...tkt,

                markup_code: evt.target.value
              };
            }
          });
          tktData.segList = newFlyerDtls;
          htlDtlListt.push(tktData);
          return htlDtlListt;
        }
      );

      this.gsdtktData = {
        no_of_pax: noofpax,
        bkng_status: bkngstatus,
        currency: currency,
        pricing_tkt_Dtls_List: htlDtlListt
      };
      this.state.pricingticketDetails = this.gsdtktData;
      this.forceUpdate();

      // console.log('gds_pnr is----'+JSON.stringify(this.state.pricingticketDetails));
    } catch (err) {
      log.error(
        "Exception occured in FlightUpdatePricing Component markupCodehandleChangeEvent function---" +
          err
      );
    }
  };

  updatePricingDetails(event) {
    this.state.loading = true;
    try {
      let pricingObjInfo = this.state.responseData.air_bookings[0]
        .pricing_objects;
      let data = "";
      let pricinginfo = [];

      pricingObjInfo.map((flyerDtl, sidx) => {
        this.state.pricingticketDetails.pricing_tkt_Dtls_List.map(
          (tkt, idx1) => {
            data = tkt.segList.map((seg, id1) => {
              if (seg.pricingObjid === flyerDtl.id) {
                return {
                  ...flyerDtl,
                  total_cashback: seg.cashback_amt - flyerDtl.total_cashback,
                  total_discount: seg.discount_amt - flyerDtl.total_discount,
                  total_markup: seg.markup_amt + flyerDtl.total_markup
                };
              }
            });

            let filteredData = data.filter(function(dat) {
              return dat != null;
            });
            Array.prototype.push.apply(pricinginfo, filteredData);
          }
        );
      });

      this.pricingObjDataReq = {
        pricing_objects: pricinginfo
      };

      console.log(
        "this.pricingObjDataReq;----" + JSON.stringify(this.pricingObjDataReq)
      );
      //this.updatePricingReq(this.airinfoDataReq);
      this.setState({ loading: false });
    } catch (err) {
      this.setState({ loading: false });
      log.error(
        "Exception occured in FphUpdateTicketNumber Component updateAirTktDetails function---" +
          err
      );
    }
  }

  updatePricingReq(updateTktReq) {
    console.log(this.state.loading);

    FlightService.updateTicketDetails(updateTktReq).then(response => {
      try {
        this.state.updateTktRes = JSON.parse(response.data);
        console.log(
          "upDateTicketDetails Res " +
            JSON.stringify(this.state.updateTktRes.responsecode)
        );
        if (
          this.state.updateTktRes !== undefined &&
          this.state.updateTktRes !== "" &&
          this.state.updateTktRes.responsecode !== undefined &&
          this.state.updateTktRes.responsecode !== "" &&
          this.state.updateTktRes.responsecode === 200
        ) {
          const msg = "Ticket details updated successfully";
          this.setState({ sucessmsg: msg });
          this.setState({ loading: false });
          this.forceUpdate();
          window.scrollTo(0, 0);
        } else {
          this.setState({ errormsg: "Ticket details not updated " });
          this.setState({ loading: false });
          this.forceUpdate();
          window.scrollTo(0, 0);
        }
      } catch (err) {
        this.setState({ loading: false });
        this.forceUpdate();
        log.error(
          "Exception occured in FphUpdateTicketNumber updateTicketReq function---" +
            err
        );
      }
    });
  }

  resetOldDetails(e) {
    console.log("resetOldDetails---");
    try {
      this.loadTicketPricingDetails();
      this.setState({ errormsg: "Pricing details has been reset! " });
      this.forceUpdate();
      window.scrollTo(0, 0);
    } catch (err) {
      log.error(
        "Exception occured in FphUpdateTicketNumber resetOldDetails function---" +
          err
      );
    }
  }

  render() {
    return (
      <>
        {this.state.errormsg !== "" && this.state.errormsg !== undefined && (
          <div className="alert alert-danger">
            <Icon
              className="noticicon"
              color="#F4675F"
              size={16}
              icon="warning"
            />{" "}
            {this.state.errormsg}
          </div>
        )}

        {this.state.sucessmsg !== "" && this.state.sucessmsg !== undefined && (
          <div className="alert notification-success">
            <Icon color="#02AE79" size={16} icon="success" />
            {this.state.sucessmsg}
          </div>
        )}
        <div>
          {this.state.pricingticketDetails.bkng_status.toUpperCase() === "P" ||
          this.state.pricingticketDetails.bkng_status.toUpperCase() === "Q" ||
          this.state.pricingticketDetails.bkng_status.toUpperCase() === "K" ? (
            <>
              {this.state.pricingticketDetails.pricing_tkt_Dtls_List.map(
                (pax, index) => (
                  <div key={index}>
                    <div className="highInfo">
                      {pax.title} {pax.first_name} {pax.last_name}
                      {pax.dob !== undefined &&
                        pax.dob !== "" &&
                        "(DOB : " +
                          DateUtils.convertStringdateFormat(pax.dob) +
                          ")"}
                    </div>

                    <div className="resTable">
                      <table className="dataTbl pax-tbl dataTable5">
                        <thead>
                          <tr>
                            <th width="10%">Sector</th>
                            <th width="15%">Cashback Amt</th>
                            <th width="15%">Cashback Code</th>
                            <th width="15%">Discount Amt</th>
                            <th width="15%">Discount Code</th>
                            <th width="15%">Markup Amt</th>
                            <th width="15%">Markup Code</th>
                          </tr>
                        </thead>
                        {pax.segList.map((tkt, idx) => (
                          <tbody key={idx}>
                            <tr>
                              <td>{tkt.sector}</td>
                              {tkt.isinput === "true" ? (
                                <>
                                  <td>
                                    <input
                                      type="text"
                                      name="cashback_amt"
                                      value={tkt.cashback_amt}
                                      onChange={this.cashBackAmthandleChangeEvent(
                                        tkt
                                      )}
                                    />
                                  </td>
                                  <td>
                                    <input
                                      type="text"
                                      name="cashback_code"
                                      value={tkt.cashback_code}
                                      onChange={this.cashBackCodehandleChangeEvent(
                                        tkt
                                      )}
                                    />
                                  </td>
                                  <td>
                                    <input
                                      type="text"
                                      name="discount_amt"
                                      value={tkt.discount_amt}
                                      onChange={this.diacountAmthandleChangeEvent(
                                        tkt
                                      )}
                                    />
                                  </td>
                                  <td>
                                    <input
                                      type="text"
                                      name="discount_code"
                                      value={tkt.discount_code}
                                      onChange={this.discountCodehandleChangeEvent(
                                        tkt
                                      )}
                                    />
                                  </td>
                                  <td>
                                    <input
                                      type="text"
                                      name="markup_amt"
                                      value={tkt.markup_amt}
                                      onChange={this.markupAmthandleChangeEvent(
                                        tkt
                                      )}
                                    />
                                  </td>
                                  <td>
                                    <input
                                      type="text"
                                      name="markup_code"
                                      value={tkt.markup_code}
                                      onChange={this.markupCodehandleChangeEvent(
                                        tkt
                                      )}
                                    />
                                  </td>
                                </>
                              ) : (
                                <>
                                  <td>{"---"}</td>
                                  <td>{"---"}</td>
                                  <td>{"---"}</td>
                                  <td>{"---"}</td>
                                  <td>{"---"}</td>
                                  <td>{"---"}</td>
                                </>
                              )}
                            </tr>
                          </tbody>
                        ))}
                      </table>
                    </div>
                  </div>
                )
              )}
              <div className="btnSec">
                <Button
                  size="xs"
                  viewType="default"
                  id="updatetktbtnId"
                  className="w-80"
                  onClickBtn={this.resetOldDetails}
                  btnTitle="Cancel"
                ></Button>
                {this.state.ftripsPermissions.UPDATE_PRICE_PERMSN_ENABLE && (
                  <Button
                    size="xs"
                    viewType="primary"
                    id="updatetktbtnId"
                    className="w-80"
                    onClickBtn={this.updateAirTktDetails}
                    loading={this.state.loading}
                    btnTitle="Update"
                  ></Button>
                )}
              </div>
            </>
          ) : (
            "This trip is either expired or the booking is not confirmed"
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster
  };
};
export default connect(mapStateToProps, null)(FlightUpdatePricing);
