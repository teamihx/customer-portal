import React, { Component } from "react";
import { connect } from "react-redux";
import EmailService from "../../../services/trips/email/EmailService";
import Notification from "Components/Notification.jsx";
export const contextPath = process.env.domainpath;

class FlightEmailRefundCreditNote extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseData1: this.props.tripDetail,
      email: "",
      showSucessMessage: false,
      showFailMessage: false,
      message: false,
      loading: false,
      iCalendar: true
    };

    //console.log("constructor");
    this.sendVatInvoice = this.sendVatInvoice.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    //console.log("componentWillMount");
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.id === "icalInputRefundRecpt") {
      if (this.state.iCalendar) {
        this.state.iCalendar = false;
      } else {
        this.state.iCalendar = true;
      }
    }
  };

  sendCrdtNote = () => {
    this.setState({ loadSendCrdtNote: true });
    let email = new Object();
    email.to = this.state.email;
    email.tripRefNumber = this.state.responseData1.trip_ref;
    email.type = "FLIGHT_REFUND_INVOICE";
    email.data = this.state.responseData1.contact_detail.last_name;
    email.ical = this.state.iCalendar;

    EmailService.sendEmails(email)
      .then(valid => {
        if (valid) {
         // console.log("send Emails : " + JSON.stringify(valid));
          if (valid !== undefined && valid !== "") {
            let response = valid;
            //console.log("description: " + response.description);
            if (response.description.includes("success")) {
              this.setState({
                showSucessMessage: true,
				loadSendCrdtNote: false,
				showAlert: true
              });
            } else {
              this.setState({ showFailMessage: true, loadSendCrdtNote: false, showAlert: true });
            }
          }
        }
      })
      .catch(err => {});
  };
  toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  render() {
	let viewType, messageText;
   if (this.state.showFailMessage) {
      viewType = "error";
      messageText = "Failed to Send Refund Credit Note";
    } else if (this.state.showSucessMessage) {
      viewType = "success";
      messageText = "Sent Refund Credit Note";
    }
    return (
      <>
        <div>
        {viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}

          <div className="form-group">
            <div className="row">
              <div className="col-12">
                <input
                  type="text"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              
              <div className="dis-flx-btw-ned w-100p">
              <div>
                <input
                  id="icalInputRefundRecpt"
                  type="checkbox"
                  className="customCheckbox"
                  defaultChecked={this.state.iCalendar}
                  value={this.state.iCalendar}
                  onChange={this.handleChange}
                />
                <label htmlFor="icalInputRefundRecpt">Send iCalendar</label>
              </div>
              <div>
                <Button
                  size="xs"
                  viewType="primary"
                  onClickBtn={this.sendCrdtNote}
                  loading={this.state.loadSendCrdtNote}
                  btnTitle="Send"
                ></Button>
              </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};

export default connect(mapStateToProps, null)(FlightEmailRefundCreditNote);
