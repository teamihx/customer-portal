import React, { Component } from "react";
import Popup from "reactjs-popup";
import { Link } from "react-router-dom";
import utility from "../../common/Utilities";
import { json } from "body-parser";
import { connect } from 'react-redux';

class HotelCostPriceBreakup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      costPricingObj: {},
     
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.currency=this.props.tripDetail.currency==='INR'?"Rs":this.props.tripDetail.currency;
    this. is_gst_model=false
  }
  openModal() {
    this.setState(
      { open: !this.state.open },
      //console.log("dfvdvd1" + this.state.open)
    );
  }
  closeModal() {
    this.setState({ open: !this.state.open }, () =>
      console.log("dfvdvd" + this.state.open)
    );
  }
  
  loadCostpticeDetail = (RoomRates) => {
    console.log("GST==="+RoomRates[0].total_tax_sup_gst)
    if(this.props.tripDetail.hotel_bookings[0].is_gst_model==="Y" && (RoomRates[0].total_tax_sup_gst!==0 || RoomRates[0].cost_room_rate.total_ct_claim_comm_gst!==0)){
     this.is_gst_model=true
     //console.log("GST==="+RoomRates[0].total_tax_sup_gst)
    }
    let costRoomRateObj = new Object();

     let total_base_fare = 0;
     let total_bf=0;
     let total_dis_supmkp=0
     let commission =0;
     let total_mkp_sup=0;
     let total_serv_charge =0
     let comm_vat =0
     let total_tax = 0;
     let st_on_commission = 0;
     let st_on_plb = 0;
     let tcs = 0;
     let total_discount =0;
     let total_dis_bf=0;
     let total_cb_bf=0;
     let total_cb_suptax=0;
     let sup_vat =0;
     let total_tax_sup_vat=0;
     let total_tax_prop_srv_chg_vat=0;
     let city_municip_fee =0;
     let total_tax_city_tax=0;
     let total_tax_municipal_fee=0;
     let total_plb_discount =0;
     let total_comm_tds =this.props.tripDetail.hotel_bookings[0].total_comm_tds;
     let total_plb_tds =this.props.tripDetail.hotel_bookings[0].total_plb_tds;
     let total_markUp=0
     //let total_dis_supmkp=0;
    for(let roomRate of RoomRates){
      total_bf+=roomRate.cost_room_rate.total_bf;
      total_markUp+=roomRate.total_markup
     // total_dis_supmkp+=roomRate.total_dis_supmkp;
      total_mkp_sup+=roomRate.total_mkp_sup;
      total_serv_charge+=roomRate.total_supfee_prop_srv_chg;
      comm_vat+=roomRate.cost_room_rate.total_ct_claim_comm_vat;
      total_plb_discount +=roomRate.cost_room_rate.total_dis_plb;
      //console.log("roomRate.is_gst_model==="+roomRate[is_gst_model])
     // total_plb_tds+=roomRate.total_plb_tds
      if (this.is_gst_model) {
        total_tax += roomRate.total_tax_sup_gst;
        st_on_commission += roomRate.cost_room_rate.total_ct_claim_comm_gst;
        st_on_plb += roomRate.cost_room_rate.total_ct_claim_plb_gst;
        tcs += roomRate.cost_room_rate.total_ct_claim_tcs;
      } else {
        total_tax +=roomRate.cost_room_rate.total_tax_sup + roomRate.cost_room_rate.total_dis_suptax;
        st_on_commission += roomRate.cost_room_rate.total_ct_claim_st_mkp;
        st_on_plb += roomRate.cost_room_rate.total_ct_claim_st_plb;
      }
      total_dis_bf+=roomRate.cost_room_rate.total_dis_bf;
      total_cb_bf+=roomRate.cost_room_rate.total_cb_bf;
      total_cb_suptax+=roomRate.cost_room_rate.total_cb_suptax;
      total_dis_supmkp+=roomRate.total_dis_supmkp;
      total_tax_sup_vat +=roomRate.cost_room_rate.total_tax_sup_vat;
      total_tax_prop_srv_chg_vat+=roomRate.cost_room_rate.total_tax_prop_srv_chg_vat;
      total_tax_city_tax+=roomRate.cost_room_rate.total_tax_city_tax;
      total_tax_municipal_fee+=roomRate.cost_room_rate.total_tax_municipal_fee
    }
    total_base_fare=total_bf
    //console.log("total_base_fare==="+total_bf+" "+total_dis_supmkp)
    commission = total_mkp_sup + total_dis_supmkp;
    //console.log("commission==="+total_mkp_sup+" "+total_dis_supmkp)
     total_discount =total_dis_bf +total_cb_bf +total_cb_suptax +total_dis_supmkp;
     sup_vat =total_tax_sup_vat +total_tax_prop_srv_chg_vat;
     city_municip_fee =total_tax_city_tax +total_tax_municipal_fee;
     
    
    
    let total_fare = total_bf + total_serv_charge + total_tax+sup_vat+ city_municip_fee+st_on_commission+comm_vat+total_discount-total_plb_discount+st_on_plb+total_comm_tds+total_plb_tds+tcs;
    let duration=Math.abs(new Date(this.props.tripDetail.hotel_bookings[0].check_out_date)-new Date(this.props.tripDetail.hotel_bookings[0].check_in_date))
    let one_day=1000*60*60*24;
    let duration_in_day=Math.round(duration/one_day)
    let duration_people=this.props.tripDetail.hotel_bookings[0].room_count*duration_in_day;
    let cost_price =total_fare/duration_people;
//console.log("total price==="+duration)
    costRoomRateObj = {
      total_base_fare: total_bf,
      total_markUp:total_markUp,
      commission: commission,
      total_serv_charge: total_serv_charge,
      comm_vat: comm_vat,
      total_tax: total_tax,
      st_on_commission: st_on_commission,
      st_on_plb: st_on_plb,
      tcs: tcs,
      total_discount: total_discount,
      sup_vat: sup_vat,
      city_municip_fee: city_municip_fee,
      total_plb_discount: total_plb_discount,
      total_comm_tds: total_comm_tds,
      total_plb_tds: total_plb_tds,
      total_fare: total_fare,
      cost_price:cost_price,
      duration_people:duration_people
    };
  // console.log("costRoomRateObj===" + JSON.stringify(costRoomRateObj))
    this.state.costPricingObj = { ...costRoomRateObj };
  };
  componentDidMount(){
    
  }
  render() {
    this.loadCostpticeDetail(this.props.tripDetail.hotel_bookings[0].room_rates);
    return (
      <>
        <Popup
          trigger={
            <a className="linkRight" onClick={this.openModal}>
              {utility.PriceFormat(
                this.state.costPricingObj.cost_price,
                this.currency
              )}
            </a>
          }
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
          position="bottom right"
        >
          {close => (
            <div className="modal pop-grid text-left">
              <a className="close" onClick={close}>
                &times;
              </a>
              <ul>
                <li>
                <span>Room Rate :</span>
                  {utility.PriceFormat(this.state.costPricingObj.total_base_fare+this.state.costPricingObj.total_markUp,
                this.currency
              ) }
                </li>
                {this.is_gst_model ? (
                  this.state.costPricingObj.total_tax!==0?<li key="total_tax">
                    <span>GST on Hotel Rate :</span>{utility.PriceFormat(this.state.costPricingObj.total_tax,
                this.currency
              )}
                  </li>:null
                ) : (
                  this.state.costPricingObj.total_tax!==0? <li key="total_tax1">
                    <span>Hotel taxes and Fees :</span>{utility.PriceFormat(this.state.costPricingObj.total_tax,
                this.currency
              )}
                  </li>:null
                )}
                {this.state.costPricingObj.total_discount!==0?<li key="total_discount"><span>Discount(-) :</span>{utility.PriceFormat(-1*this.state.costPricingObj.total_discount,
                this.currency
              )}</li>:null}
                {this.state.costPricingObj.sup_vat!==0?<li key="sup_vat"><span>VAT :</span>{utility.PriceFormat(this.state.costPricingObj.sup_vat,
                this.currency
              )}</li>:null}
               { this.state.costPricingObj.city_municip_fee!==0 ? <li key="city_municip_fee">
               <span>City Tax/Municipal Fee :</span>{utility.PriceFormat(this.state.costPricingObj.city_municip_fee,
                this.currency
              )}
                </li>:null}
                {this.state.costPricingObj.commission!==0?<li key="commission"><span>Commission(-) :</span>{utility.PriceFormat(this.state.costPricingObj.commission,
                this.currency
              )}</li>:null}
                {this.is_gst_model? (
                 this.state.costPricingObj.st_on_commission!==0? <li key="on_commission">
                    <span>GST on Commission(-) :</span>
                    {utility.PriceFormat(-1*this.state.costPricingObj.st_on_commission,
                this.currency
              )}
                  </li>:null
                ) : (
                  this.state.costPricingObj.st_on_commission!==0?<li key="st_on_commission">
                    <span>ST on Commission(-1) :</span>
                    {utility.PriceFormat(-1*this.state.costPricingObj.st_on_commission,
                this.currency
              )}
                  </li>:null
                )}
               {this.state.costPricingObj.comm_vat!==0? <li key="comm_vat">
                <span>VAT 5% on Commission :</span>{utility.PriceFormat(this.state.costPricingObj.comm_vat,
                this.currency
              )}
                </li>:null}
               { this.state.costPricingObj.total_plb_discount!==0? <li key="plb_discount">
                <span>PLB Discount(-) :</span>{utility.PriceFormat(-1*this.state.costPricingObj.total_plb_discount,
                this.currency
              )}
                </li>:null}
                {this.is_gst_model ? (
                 this.state.costPricingObj.st_on_plb!==0? <li key="on_plb">
                   <span> GST on PLB discount :</span>{utility.PriceFormat(this.state.costPricingObj.st_on_plb,
                this.currency
              )}
                  </li>:null
                ) : (
                  this.state.costPricingObj.st_on_plb!==0?<li key="st_on_plb"><span> PLB Discount(-) :</span>{utility.PriceFormat(-1*this.state.costPricingObj.st_on_plb,
                    this.currency
                  )}</li>:null
                )}
               {this.state.costPricingObj.total_comm_tds!==0 ?<li key="comm_tds">
                <span>TDS on commission :</span>{utility.PriceFormat(this.state.costPricingObj.total_comm_tds,
                this.currency
              )}
                </li>:null}
                {this.state.costPricingObj.total_plb_tds!==0?<li key="plb_tds"><span>TDS on PLB </span>:{utility.PriceFormat(this.state.costPricingObj.total_plb_tds,
                this.currency
              )}</li>:null}
                {this.is_gst_model ? (
                 this.state.costPricingObj.tcs!==0? <li key="tcs"><span>TCS(-) :</span>{utility.PriceFormat(-1*this.state.costPricingObj.tcs,
                    this.currency
                  )}</li>:null
                ) : null}
                <li key="total_fare" className="total"><span> Total :</span>{utility.PriceFormat(this.state.costPricingObj.total_fare,
                this.currency
              )}</li>
              </ul>
            </div>
          )}
        </Popup>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
      tripDetail: state.trpReducer.tripDetail,
     
  };
}

export default connect(mapStateToProps, null) (HotelCostPriceBreakup);
