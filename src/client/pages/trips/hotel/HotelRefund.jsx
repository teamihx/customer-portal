import React, { Component } from 'react'
import { connect } from 'react-redux';

import NumberUtils from '../../../services/commonUtils/NumberUtils.js';

class HotelRefund extends Component {

    constructor(props) {
        super(props)
        this.state = {
            responseData1: this.props.tripDetail,
			refundTxn : '',
			totalRevarsal :'0',
			totalSup :'0',
			totalCt :'0',
			totalCshBack: '0',
			totalDiscount: '0',
			totalSTax: '0',
			totalCTSTax: '0',			
			totalMiscCharge: '0',
			totalAmdCharge: '0',
			totalOtherCharge: '0',
			totalWtCbCharge: '0',
			totalBundleCharge: '0',
			totalGateWayCharge: '0',
			totalTotalRefund: '0',
			total_stx_charge:'0',
			total_ct_stx_charge: '0'
          
        }
 		this.calculateTotalRefund = this.calculateTotalRefund.bind(this);
	
        //console.log('constructor');
    }

   componentWillMount(){
	 //console.log('componentWillMount');
	let txns = this.state.responseData1.txns.filter( function (txns) {
      return txns.hotel_refund_records.length > 0
    });

	
	this.state.refundTxn = txns;
	this.calculateTotalRefund();
	}
	
	calculateTotalRefund() {
	for(let refundFlt of this.state.refundTxn){
		this.state.totalRevarsal = Number(this.state.totalRevarsal) + Number(refundFlt.hotel_refund_records[0].total_rev);		
		this.state.totalSup = Number(this.state.totalSup) + Number(refundFlt.hotel_refund_records[0].total_sup_charge);
		this.state.totalDiscount = Number(this.state.totalDiscount) + Number(refundFlt.hotel_refund_records[0].total_dis);
		this.state.totalCshBack = Number(this.state.totalCshBack) + Number(refundFlt.hotel_refund_records[0].total_cb);	
		this.state.totalCt = Number(this.state.totalCt) + Number(refundFlt.hotel_refund_records[0].total_ct_charge);
		this.state.totalOtherCharge = Number(this.state.totalOtherCharge) + Number(refundFlt.hotel_refund_records[0].total_oth_charge);
		this.state.totalWtCbCharge = Number(this.state.totalWtCbCharge) + Number(refundFlt.hotel_refund_records[0].total_wt_cb_charge);
		this.state.totalGateWayCharge = Number(this.state.totalGateWayCharge) + Number(refundFlt.hotel_refund_records[0].total_gw_charge);
		
		this.state.total_stx_charge = Number(this.state.total_stx_charge) + Number(refundFlt.hotel_refund_records[0].total_stx_charge);
		this.state.total_ct_stx_charge = Number(this.state.total_ct_stx_charge) + Number(refundFlt.hotel_refund_records[0].total_ct_stx_charge);
		}
	
  	}
    
    render() {
        
        return (
            <>
            <div>
			<h4 className="in-tabTTl-sub"> Refund Computation details</h4>
			
			<div className ='resTable overflow-x-auto'>
			<table className="dataTblPaddingR pax-tbl dataTable5 scrlTable">
                  <thead><tr>
                <th width="10%">Room</th>
                <th width="6%">Reversal <span className="d-b mt-3">(+)</span></th>
                <th width="6%">Supplier<span className="d-b mt-3">(–)</span> </th>
                <th width="8%">Supplier Tax<span className="d-b mt-3">(–)</span> </th>
                <th width="6%">Discount <span className="d-b mt-3">(–)</span></th>
				<th width="6%">Cashback <span className="d-b mt-3">(–)</span></th>
                <th width="6%">Cleartrip <span className="d-b mt-3">(–)</span></th>
                <th width="6%">CT Tax<span className="d-b mt-3">(–)</span> </th>
				<th width="8%">Other charges<span className="d-b mt-3"> (+/-)</span></th>				
				<th width="10%">Wallet Cashback<span className="d-b mt-3">(–)</span> </th>
				<th width="8%">Gateway Fee<span className="d-b mt-3">(–)</span> </th>
				<th width="10%">Tax on Gateway<span className="d-b mt-3">(–)</span> </th>
				<th width="6%">Refund</th>
				
                   
                  </tr></thead>
                  <tbody>

				{this.state.refundTxn.map(refundTxn => {
						
			let bookInfoId = refundTxn.hotel_cancellations[0].hotel_booking_info_id;
			let bookInfos = this.state.responseData1.hotel_bookings[0].hotel_booking_infos.filter( function (htlbookInfo) {
					      return htlbookInfo.id === bookInfoId
			});
			
			let roomId = bookInfos[0].room_id;
			let roomInfo = this.state.responseData1.hotel_bookings[0].rooms.filter( function (room) {
					      return room.id === roomId
			});
			
			let roomTypeId = roomInfo[0].room_type_id;
			
			let roomType = this.state.responseData1.hotel_bookings[0].room_types.filter( function (roomTyp) {
					      return roomTyp.id === roomTypeId
			});
			let paymode = this.state.responseData1.payments_service_data[0].payment_type;
			let tnxNumb = '';
			let paymentmode = this.props.paymentTypeMaster[this.state.responseData1.payments_service_data[0].payment_type];
			if(paymode ==='CC'){
				 tnxNumb = this.state.responseData1.payments_service_data[0].payment_card_details[0].card_number;
			}	
			refundTxn.paymentmode =paymentmode;
			refundTxn.tnxNumb =tnxNumb;
			
			let totalGuest = this.state.responseData1.hotel_bookings[0].total_guests;
					
			  return(
				<>
				 <tr>
                   
                    <td>
						 <span>
					{roomType[0].name} <span className="d-b mt-3">{totalGuest} Guests</span>
						
                       </span>
                    </td>
                     <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_rev)} </span></td>
                     <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_sup_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_stx_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_dis)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_cb)} </span></td>					 
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_ct_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_ct_stx_charge)}</span></td>
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_oth_charge)} </span></td>
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_wt_cb_charge)} </span></td>			
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].total_gw_charge)} </span></td>					 
					 <td>0</td>
					 <td><span>{NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].refund_amount)} </span></td>
					
				
                  </tr>
				<tr>
				<td colSpan="13">
				<span className="t-color2"><strong className="d-b">Refund Breakup</strong>  Refund to {refundTxn.paymentmode}({refundTxn.tnxNumb}) : {NumberUtils.numberFormat(refundTxn.hotel_refund_records[0].refund_amount)}</span>
				</td>
				</tr>

				</>
			);
			
			})}
			<tr>
				 <td><strong>Total</strong></td>
                <td>{NumberUtils.numberFormat(this.state.totalRevarsal)}</td>
                <td>{NumberUtils.numberFormat(this.state.totalSup)}</td>
				<td>{NumberUtils.numberFormat(this.state.total_stx_charge)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalDiscount)}</td>
				 <td>{NumberUtils.numberFormat(this.state.totalCshBack)}</td>
                <td>{NumberUtils.numberFormat(this.state.totalCt)}</td>
				<td>{NumberUtils.numberFormat(this.state.total_ct_stx_charge)}</td>
				<td >{NumberUtils.numberFormat(this.state.totalOtherCharge)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalWtCbCharge)}</td>
				<td>{NumberUtils.numberFormat(this.state.totalGateWayCharge)}</td>
				 <td>0</td>
				<td>{NumberUtils.numberFormat(this.state.totalTotalRefund)}</td>
				
               
				
				
			</tr>
				
                  </tbody>
                </table>
				</div>
            </div>
           
</>

        )
    }


}

const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,       
        airPortMaster: state.trpReducer.airPortMaster, 
        paymentTypeMaster:state.trpReducer.paymentTypeMaster, 
       
     };
}

export default connect(mapStateToProps, null)(HotelRefund);
