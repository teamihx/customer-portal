import React, { Component } from 'react';

import { connect } from 'react-redux';
import utility from '../../common/Utilities'
import ShowHide from 'Components/ShowHide.jsx';
import log from 'loglevel';
import DateUtils from 'Services/commonUtils/DateUtils.js';
import * as actions from '../../../store/actions/index';

class HotelPricingDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
         pricingDetail:{}
        }
        
    }
    isEmpty = (obj) => {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }
    componentDidMount(){
      this.props.onInitPricingObject(this.state.pricingDetail);
    }
      loadPricingDetail=(tripDetail)=>{        

        try{
        if(tripDetail!==null && tripDetail!==undefined && tripDetail!==''
        && tripDetail.status===200){
          
        let pricingObject=new Object();
        if(!this.isEmpty(tripDetail)){
          if(tripDetail.currency==='INR'){
            pricingObject.currency='Rs'
          }else{
            pricingObject.currency=tripDetail.currency
          }
          
          pricingObject.txn_source_type=tripDetail.txn_source_type;
         let htlbkng = tripDetail.hotel_bookings[0];
         let hotelPricingDetail = new Object();
        let total_base_fare=htlbkng.total_base_fare;
        let total_markup=htlbkng.total_markup
         pricingObject.room_rent=total_base_fare+total_markup;
         let total_vat = htlbkng.total_prop_srv_chg_vat+htlbkng.total_sup_vat;
         pricingObject.total_prop_srv_chg=htlbkng.total_prop_srv_chg
         if(htlbkng.total_sup_gst>0){
           pricingObject.total_sup_gst=htlbkng.total_sup_gst;
         }else if(total_vat>0){
          pricingObject.total_sup_vat=total_vat
         }else{
          pricingObject.total_tax_sup=htlbkng.total_tax_sup
         }
         pricingObject.txn_fee=htlbkng.room_rates[0].total_fee_agency_markup;
         pricingObject.total_discount=htlbkng.total_discount
         pricingObject.city_muncipal_tax=htlbkng.total_city_tax+htlbkng.total_municipal_fee
         pricingObject.total_cashback=htlbkng.total_cashback
         pricingObject.total_fee_pgc=htlbkng.room_rates[0].total_fee_pgc
         pricingObject.total_fee_con=htlbkng.total_fee_gw+htlbkng.total_gw_gst+htlbkng.total_gw_vat-htlbkng.room_rates[0].total_fee_agency_markup
         if(pricingObject.txn_source_type==='AGENCY'){
          pricingObject.total_fare=htlbkng.total_fare+htlbkng.room_rates[0].total_nc_fee
         }else{
          pricingObject.total_fare=htlbkng.total_fare;
         }
        
        }
        this.state.pricingDetail={...pricingObject}
        //this.setState({...this.state.pricingDetail,...pricingObject})
      //console.log("Price Object "+JSON.stringify(this.state.pricingDetail))
        //this.setPricingObject(this.state.pricingObject);
      }
    }catch(err){

      log.error('Exception occured in Hotel pricing  Component loadPricingDetail function---'+err);

    }
      }
  
render() {
  {this.loadPricingDetail(this.props.tripDetail)}
  return (
    <>
    <div className="side-pnl">
     <ShowHide visible="true" title="Pricing Detail" >
     <div className="showHide-content">
         <ul className="price-info">
               { this.state.pricingDetail.room_rent!==0?<li><span>Room rate</span>  <span>  {utility.PriceFormat(this.state.pricingDetail.room_rent ,this.state.pricingDetail.currency )}</span></li>:''}
               { this.state.pricingDetail.total_prop_srv_chg!==0?<li><span>Service charge</span>  <span>  {utility.PriceFormat(this.state.pricingDetail.total_prop_srv_chg,this.state.pricingDetail.currency ) }</span></li>:''}
               { typeof this.state.pricingDetail.total_sup_gst!=='undefined'?<li><span>GST on Hotel Rate</span>  <span>  {utility.PriceFormat(this.state.pricingDetail.total_sup_gst ,this.state.pricingDetail.currency )}</span></li>:''}
               { typeof this.state.pricingDetail.total_sup_vat!=='undefined'?<li><span>Vat </span>  <span> {utility.PriceFormat(this.state.pricingDetail.total_sup_vat,this.state.pricingDetail.currency ) }</span></li>:''}
               { typeof this.state.pricingDetail.total_tax_sup!=='undefined'?<li><span>Hotel Taxes and Fees</span>  <span>  {utility.PriceFormat(this.state.pricingDetail.total_tax_sup,this.state.pricingDetail.currency ) }</span></li>:''}
               { this.state.pricingDetail.txn_fee !==0?<li><span>Transaction Fee </span>  <span> {utility.PriceFormat(this.state.pricingDetail.txn_fee ,this.state.pricingDetail.currency ) }</span></li>:''}
               { this.state.pricingDetail.total_discount !==0?<li><span>Discount </span>  <span> {utility.PriceFormat(-1*this.state.pricingDetail.total_discount,this.state.pricingDetail.currency )  }</span></li>:''}
               { this.state.pricingDetail.city_muncipal_tax !==0?<li><span>City Tax/Municipal Fee </span>  <span> {utility.PriceFormat(this.state.pricingDetail.city_muncipal_tax,this.state.pricingDetail.currency )  }</span></li>:''}
               { this.state.pricingDetail.total_cashback !==0?<li><span>Cash Back</span>  <span>  {utility.PriceFormat(-1*this.state.pricingDetail.total_cashback ,this.state.pricingDetail.currency ) }</span></li>:''}
               { this.state.pricingDetail.total_fee_pgc !==0?<li><span>EMI Pross Fee</span>  <span> {utility.PriceFormat(this.state.pricingDetail.total_fee_pgc,this.state.pricingDetail.currency )  }</span></li>:''}
               { this.state.pricingDetail.total_fee_con !==0?<li><span>Convenience fee </span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_fee_con,this.state.pricingDetail.currency )  }</span></li>:''}
               { this.state.pricingDetail.total_fare !==0?<li className="total"><span>Total </span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_fare,this.state.pricingDetail.currency )  }</span></li>:''}

      
                        
              </ul>
              </div>
              </ShowHide>
              </div>
         </>
  );
}
}
const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail,
      //airLineMaster: state.trpReducer.airLineMaster
    };
  }
  const mapDispatchToProps = dispatch => {
    return {
        
        onInitPricingObject: (req) => dispatch(actions.initPricingObject(req))
        
        
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HotelPricingDetail);