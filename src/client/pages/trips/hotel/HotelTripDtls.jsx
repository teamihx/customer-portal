import React, { Component } from "react";
import { connect } from "react-redux";
import hotelUtil from "./HotelUtil";
import HotelCustomerPriceBreakup from "./HotelCustomerPriceBreakup";
import HotelCostPriceBreakup from "./HotelCostPriceBreakup";
import DateUtils from "../../../services/commonUtils/DateUtils";
import { Link } from "react-router-dom";
import RateRule from "../hotel/RateRule";
import Icon from "Components/Icon";
import ReactHtmlParser, {
  processNodes,
  convertNodeToElement,
  htmlparser2
} from "react-html-parser";
export const TRIPS_ROLES = "tripsRoles";
class HotelTripDtls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: "",
      hotelDetailList: [],
      inclusion: "",
      showRateRule: false,
      cancel_policy: "",
      tripPermission: "",
      isCancelTrip:this.props.cancelTrip
    };
    //Getting ROLES DATA from local storage
    let tripsObj = JSON.parse(localStorage.getItem(TRIPS_ROLES));
    this.state.tripPermission = tripsObj.htripPermissions;
  }
  showNewPopUp(id, e) {
    e.persist();
    let url = "";
    //console.log("e.target.id +"+ JSON.stringify(flt) )

    url = Domainpath.getHqDomainPath() + "/hq/hotels/" + id;

    window.open(url);
  }
  getTravellers = travellers => {
    let pricingObject = new Object();
    let adultCount = 0;
    let childCount = 0;
    let infantCount = 0;

    travellers = travellers.trimRight();
    let travellerString = travellers.substring(
      travellers.trimRight().indexOf("|") + 1,
      travellers.length
    );
    let ttravellerArr = travellerString.trimLeft().split("\n");

    for (let travel of ttravellerArr) {
      if (travel.includes("INF")) {
        infantCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
      if (travel.includes("ADT")) {
        adultCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
      if (travel.includes("CHD")) {
        childCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
    }
    let adtTravel =
      adultCount > 0
        ? adultCount + (adultCount == 1 ? " Adult  " : " Adults  ")
        : "";
    let childTravel =
      childCount > 0
        ? childCount + (childCount == 1 ? " Child  " : " Children  ")
        : "";
    let infTravel =
      infantCount > 0
        ? infantCount + (infantCount == 1 ? " Infant  " : " ,Infant  ")
        : "";
    return adtTravel + childTravel + infTravel;
  };
  getGuestRoomWise = guests => {
    let adultCount = 0;
    let childCount = 0;
    let infantCount = 0;
    //let guests=room.guests;
    let guestArr = guests.substring(13).split("\n");
    //let number =guest[0].substring(guest[0].indexOf(":")+1,guest[0].length)
    //console.log("guestArr===" + guestArr);
    for (let guest of guestArr) {
      let guestType = guest
        .substring(guest.indexOf(":") + 1, guest.length)
        .substring(1);
      //console.log("guestType=====")
      if (guestType === "INF") {
        infantCount += 1;
      }
      if (guestType === "ADT") {
        adultCount += 1;
      }
      if (guestType === "CHD") {
        childCount += 1;
      }
    }
    let adtTravel =
      adultCount > 0
        ? adultCount + (adultCount == 1 ? " Adult  " : " Adults  ")
        : "";
    let childTravel =
      childCount > 0
        ? childCount + (childCount == 1 ? " Child  " : " Children  ")
        : "";
    let infTravel =
      infantCount > 0
        ? infantCount + (infantCount == 1 ? " Infant  " : " ,Infant  ")
        : "";
    //console.log("travellers=====" + adtTravel);
    return adtTravel + childTravel + infTravel;
  };
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  loadTripDetails = () => {
    this.state.hotelDetailList = [];
    
    if (this.props.tripDetail!==null 
      && this.props.tripDetail!==undefined && this.props.tripDetail!=='' && this.props.tripDetail.status==200
      && !this.isEmpty(this.props.tripDetail) && this.props.tripDetail.hotel_bookings.length > 0) {
     
      let htlBookObj = new Object();
      let htlBkngDtl = { ...this.props.tripDetail.hotel_bookings[0] };
      htlBookObj.check_in_date = htlBkngDtl.check_in_date;
      htlBookObj.check_out_date = htlBkngDtl.check_out_date;
      htlBookObj.city_name = htlBkngDtl.hotel_detail.full_city_name;
      htlBookObj.hotel_name = htlBkngDtl.hotel_detail.name;
      htlBookObj.room_type = htlBkngDtl.room_types[0].name;
      htlBookObj.room_type_name = htlBkngDtl.room_types[0].room_type_name;
      htlBookObj.room_count = htlBkngDtl.room_count;
      htlBookObj.type = "hotel_booking";
      htlBookObj.htl_id = htlBkngDtl.hotel_id;
      htlBookObj.id = htlBkngDtl.id;
      let duration = Math.abs(
        new Date(htlBookObj.check_out_date) - new Date(htlBookObj.check_in_date)
      );
      let one_day = 1000 * 60 * 60 * 24;
      htlBookObj.duration = Math.round(duration / one_day);
      //htlBookObj.room_cost={...roomCost}
      this.state.hotelDetailList.push(htlBookObj);

      let roomCost = { ...htlBkngDtl.room_rates[0] };
      roomCost.is_gst_model = htlBkngDtl.is_gst_model;
      roomCost.duration = htlBookObj.duration;
      roomCost.comm_vat = htlBkngDtl.total_comm_vat;
      roomCost.room_count = htlBkngDtl.room_count;
      roomCost.total_comm_tds = htlBkngDtl.total_comm_tds;
      roomCost.total_plb_tds = htlBkngDtl.total_plb_tds;
      if (this.props.tripDetail.currency === "INR") {
        roomCost.currency = "Rs";
      } else {
        roomCost.currency = this.props.tripDetail.currency;
      }

      for (let type of htlBkngDtl.room_types) {
        let roomDtlObj = new Object();
        roomDtlObj.room_cost = { ...roomCost };
        roomDtlObj.room_info = [];
        roomDtlObj.type = "room details";
        roomDtlObj.room_count = htlBkngDtl.room_count;
        roomDtlObj.no_of_adult = this.props.tripDetail.travellers;
        roomDtlObj.inclusion = type.inclusions;
        this.setState({ inclusion: type.inclusions });
        /* console.log(
          "Total Ininclusion" + type.inclusions + this.state.inclusion
        ); */
        for (let room of htlBkngDtl.rooms) {
          if (type.id === room.room_type_id) {
            let roomObj = new Object();
            roomObj.guests = room.guests;

            roomObj.noOfAdults = hotelUtil.numberOfAdults(room.guests);
            roomObj.noOfChild = hotelUtil.numberOfChild(room.guests);
            for (let info of htlBkngDtl.hotel_booking_infos) {
              if (info.room_id === room.id) {
                roomObj.room_name = type.name;
                roomObj.room_type_name = type.room_type_name;
                roomObj.supplier_name = type.supplier_id;
                roomObj.voucher_number = info.voucher_number;

                roomObj.status = info.booking_status;
                roomDtlObj.room_info.push(roomObj);
                break;
              }
            }
          }
        }
        this.state.hotelDetailList.push(roomDtlObj);
      }
      this.setState({
        cancel_policy: htlBkngDtl.room_types[0].cancellation_policy
      });
    }
  };
  getInclusion = inclusion => {
    if (inclusion !== null) {
      //console.log("jhghjgj===" + inclusion);
      let inclusionList =
        typeof inclusion !== "undefined"
          ? inclusion.substring(17).split("- ")
          : null;
      //console.log("jhghjgj===" + inclusionList);
      let newInclusionList = [];
      for (let inclusion of inclusionList) {
        if (inclusion[0] === '"') {
          inclusion = inclusion.substring(1, inclusion.length - 2);
        }
        inclusion.length > 0 ? newInclusionList.push(inclusion) : null;
      }
      return newInclusionList;
    }
  };
  showRateRule = (ele, e) => {
    this.setState({ showRateRule: !this.state.showRateRule }, () =>
      console.log("showRateRule" + this.state.showRateRule)
    );
  };
  render() {
    return this.state.hotelDetailList.map((ele, i) => {
      return ele.type === "hotel_booking" ? (
        <React.Fragment key={ele.id}>
          <div className="heading dis-flx-btw mb-5">
            <h4 className="in-tabTTl-sub">
              <Icon
                className="noticicon mt--5"
                color="#36c"
                size={18}
                icon="hotel"
              />{" "}
              Itinerary
            </h4>
            <div className="trip-sub">
              Hotel in {ele.city_name} -{" "}
              <Link
                to="#"
                className="linkRight"
                onClick={e => this.showNewPopUp(ele.htl_id, e)}
              >
                {ele.hotel_name}
              </Link>
              {this.state.tripPermission.RATE_RULE_PERMSN_ENABLE && (
                <a
                  id="fareRule"
                  className="linkRight"
                  onClick={e => this.showRateRule(ele, e)}
                >
                  Rate Rule
                </a>
              )}
            </div>
          </div>
          <div className="resTable">
            <table className="dataTable3">
              <thead>
                <tr>
                  <th width="25%">Check-in</th>
                  <th width="25%">Check-out</th>
                  <th width="25%">Duration</th>
                  <th width="25%">Rooms</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    {DateUtils.prettyDate(
                      ele.check_in_date,
                      "hh:mm A,ddd,MMM DD YYYY "
                    )}
                  </td>
                  <td>
                    {DateUtils.prettyDate(
                      ele.check_out_date,
                      "hh:mm A,ddd,MMM DD YYYY "
                    )}
                  </td>
                  <td>
                    {ele.duration} {ele.duration > 1 ? "nights" : "night"}
                  </td>
                  <td>
                    {ele.room_count} {ele.room_type}
                  </td>
                </tr>
              </tbody>
            </table>
            {this.state.showRateRule ? (
              <RateRule
                data={this.state.cancel_policy}
                tripId={this.props.tripDetail.trip_ref}
              />
            ) : null}
          </div>
        </React.Fragment>
      ) : (
        <React.Fragment key={ele.id}>
          <div className="heading dis-flx-btw mb-5">
            <h4 className="in-tabTTl-sub">
              <Icon
                className="noticicon mt--5"
                color="#36c"
                size={18}
                icon="hotel"
              />{" "}
              Booking details -{" "}
              <span className="t-color3 font-14">
                {this.getTravellers(ele.no_of_adult)}
              </span>
            </h4>
            <div className="trip-sub">
              {ele.room_cost.room_count} Room -{" "}
              <HotelCustomerPriceBreakup data={ele.room_cost} />{" "}
              <em className="t-color2">
                {" "}
                Per room per night ( Payable to hotel -{" "}
                <HotelCostPriceBreakup />)
              </em>
            </div>
          </div>

          <div className="resTable">
            <table className="dataTable3">
              <thead>
                <tr>
                  <th width="30%">Room Type</th>
                  <th width="15%">Supplier</th>
                  <th width="20%">Voucher#</th>
                  <th width="20%">Number of Travellers</th>
                  <th width="15%">Status</th>
                </tr>
              </thead>
              <tbody>
                {ele.room_info.map(info => (
                  <tr className={info.status === "Q" || info.status === "K" ? "des cancel": null}
                    key={info.id}>
                    <td>
                      <span>{info.room_type_name} </span>
                    </td>
                    <td>
                      <span>
                        {this.props.hotelSupplierMaster[info.supplier_name]}
                      </span>
                    </td>
                    <td>
                      <span>{info.voucher_number}</span>
                    </td>
                    <td>
                      <span>{this.getGuestRoomWise(info.guests)} </span>
                    </td>
                    <td>
                      <span className="n-strk">
                        {this.props.bkngSatatusMaster[info.status]}
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          {this.state.inclusion !== null && 
          (this.state.isCancelTrip===null ||
          this.state.isCancelTrip===undefined ||
          this.state.isCancelTrip==='') ? (
            <div className="lineHeight20">
              <h4 className="in-tabTTl-sub mb-5">Inclusion And Extra</h4>
              <span>
                Inclusion for{" "}
                {this.state.hotelDetailList.length > 0
                  ? this.state.hotelDetailList[0].room_type
                  : null}{" "}
                room :
              </span>
              <ul className="lstStlyCrl ml-15">
                {this.getInclusion(this.state.inclusion).map((item, index) => (
                  <li key={index}>{item}</li>
                ))}
              </ul>
            </div>
          ) : null}
        </React.Fragment>
      );
    });
  }
  componentDidMount() {
    {
      this.loadTripDetails();
    }
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airPortMaster: state.trpReducer.airPortMaster,
    airLineMaster: state.trpReducer.airLineMaster,

    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster,
    hotelSupplierMaster: state.trpReducer.hotelSupplierMaster
  };
};
export default connect(mapStateToProps, null)(HotelTripDtls);
