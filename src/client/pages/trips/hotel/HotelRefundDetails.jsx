import React, { Component } from "react";
import log from "loglevel";
import CancellationService from "../../../services/trips/cancel/CancellationService";
import Utility from "../../common/Utilities";

class HotelRefundDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //trip_id:this.props.trip_ref,
      trip_id: "",
      refundDetailsresponse: this.props.refund_details
    };

    //console.log('HotelRefundDetails trip id is---'+this.props.trip_ref);
    console.log("HotelRefundDetails trip id is---" + this.props.refund_details);

    this.loadRefundCalculationDetails11 = this.loadRefundCalculationDetails11.bind(
      this
    );
    // this.loadRefundCalculationDetails11();
  }

  /* UNSAFE_componentWillMount(){
        console.log('UNSAFE_componentWillMount in htl refund details---');
        this.loadRefundCalculationDetails11();
    } */

  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  loadRefundCalculationDetails11() {
    console.log("loadRefundCalculationDetails11--entered");
    const req = {
      tripRefNumber: this.state.trip_id
    };
    try {
      CancellationService.cancelRefundInfo(req)
        .then(response => {
          console.log("refundCalresponse----" + JSON.stringify(response));
          if (
            response.status !== null &&
            response.status !== undefined &&
            response.status === 200 &&
            response.data !== null &&
            response.data !== undefined &&
            response.data !== "" &&
            response.data.data !== null &&
            response.data.data !== undefined &&
            response.data.data !== ""
          ) {
            console.log(
              "refundCalresponse123----" + JSON.stringify(response.data.data)
            );
            this.state.refundDetailsresponse = response.data.data;
            //this.forceUpdate();
            console.log("GW11111----" + this.state.refundDetailsresponse["GW"]);
          }
        })
        .catch(error => {
          log.error(
            "Exception occured in HotelRefundDetails component loadRefundCalculationDetails" +
              error
          );
        });
    } catch (error) {
      log.error(
        "Exception occured in HotelRefundDetails component loadRefundCalculationDetails function---" +
          error
      );
    }
  }

  render() {
    //{this.loadRefundCalculationDetails11()}
    console.log("HotelRefundDetails trip id is---");
    console.log(
      "GW11111render refund----" + this.state.refundDetailsresponse["GW"]
    );
    const revesalAmt = this.state.refundDetailsresponse["REV"];
    const supplierAmt = this.state.refundDetailsresponse["SUP"];
    const cashBack = this.state.refundDetailsresponse["CB"];
    const discount = this.state.refundDetailsresponse["DIS"];
    const cleartrip = this.state.refundDetailsresponse["CT"];
    const convenienceFee = this.state.refundDetailsresponse["GW"];
    const otherChanrges = this.state.refundDetailsresponse["OTH"];
    const refundValue = this.state.refundDetailsresponse["refund"];

    return (
      <>
        <div>
          <ul className="price-info">
            {revesalAmt !== null && revesalAmt !== undefined ? (
              <li>
                <span>Reversal Amount</span>{" "}
                <span>
                  {Utility.PriceFormat(parseFloat(revesalAmt, 10), "")}
                </span>
              </li>
            ) : (
              ""
            )}

            {supplierAmt !== null && supplierAmt !== undefined ? (
              <li>
                <span>Supplier</span>{" "}
                <span>
                  {Utility.PriceFormat(parseFloat(supplierAmt, 10), "")}
                </span>
              </li>
            ) : (
              ""
            )}

            {cashBack !== null && cashBack !== undefined ? (
              <li>
                <span>Cashback</span>{" "}
                <span>{Utility.PriceFormat(parseFloat(cashBack, 10), "")}</span>
              </li>
            ) : (
              ""
            )}

            {discount !== null && discount !== undefined ? (
              <li>
                <span>Discount</span>{" "}
                <span>{Utility.PriceFormat(parseFloat(discount, 10), "")}</span>
              </li>
            ) : (
              ""
            )}

            {cleartrip !== null && cleartrip !== undefined ? (
              <li>
                <span>Cleartrip</span>{" "}
                <span>
                  {Utility.PriceFormat(parseFloat(cleartrip, 10), "")}
                </span>
              </li>
            ) : (
              ""
            )}

            {convenienceFee !== null && convenienceFee !== undefined ? (
              <li>
                <span>Convenience Fee</span>{" "}
                <span>
                  {Utility.PriceFormat(parseFloat(convenienceFee, 10), "")}
                </span>
              </li>
            ) : (
              ""
            )}

            {otherChanrges !== null && otherChanrges !== undefined ? (
              <li>
                <span>Other Charges</span>{" "}
                <span>
                  {Utility.PriceFormat(parseFloat(otherChanrges, 10), "")}
                </span>
              </li>
            ) : (
              ""
            )}

            {refundValue !== null && refundValue !== undefined ? (
              <li className="total">
                <span>Refund</span>{" "}
                <span>
                  {Utility.PriceFormat(parseFloat(refundValue, 10), "")}
                </span>
              </li>
            ) : (
              ""
            )}
          </ul>
        </div>
      </>
    );
  }
}

export default HotelRefundDetails;
