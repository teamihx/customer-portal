import React, { Component } from "react";
import { connect } from "react-redux";
import EmailService from "../../../services/trips/email/EmailService";
import Notification from "Components/Notification.jsx";
export const contextPath = process.env.domainpath;

class HotelEmailCreditMemo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseData1: this.props.tripDetail,
      email: this.props.tripDetail.contact_detail.email,
      showSucessMessage: false,
      showFailMessage: false,
      message: false,
      loading: false
    };

    //console.log("constructor");
    this.sendCreditMemo = this.sendCreditMemo.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getMessage = this.getMessage.bind(this);
    this.validateEmails = this.validateEmails.bind(this);
  }

  componentWillMount() {
    //console.log("componentWillMount");
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  validateEmails() {
    let isValid = true;
    if (this.state.email === null || this.state.email.trim() === "") {
      this.state.errorMsgList.push("Email must be provided");
      isValid = false;
      this.setState({ showInputMessage: true, sendHSalesInvoice: false });
    }
    if (this.state.email != null && this.state.email.trim() !== "") {
      const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
      isValid = expression.test(String(this.state.email).toLowerCase());
      if (!isValid) {
        this.state.errorMsgList.push("Enter a valid email address");
        this.setState({ showInputMessage: true, sendHSalesInvoice: false });
      }
    }
    //console.log("valid email---" + isValid);
    return isValid;
  }

  sendCreditMemo = () => {
    this.setState({ showAlert: true });
    if (this.validateEmails()) {
      let email = new Object();
      email.to = this.state.email;
      email.tripRefNumber = this.state.responseData1.trip_ref;
      email.type = "HOTEL_CREDIT_NOTE";
      email.data = this.state.responseData1.contact_detail.last_name;
      email.souceType = this.state.responseData1.txn_source_type;

      EmailService.sendEmails(email).then(valid => {
        if (valid) {
         // console.log("send Emails : " + JSON.stringify(valid));
          if (valid !== undefined && valid !== "") {
            let response = valid;
           // console.log("description: " + response.description);
            if (response.description.includes("success")) {
              this.setState({ showSucessMessage: true,sendCrdMemo: false, showAlert: true });
            } else {
              this.setState({ showFailMessage: true,sendCrdMemo: false, showAlert: true });
            }
          }
        }
      });
    }
    this.setState({ sendCrdMemo: false });
  };

  getMessage(type) {
    let msg = "";
    if (type === "success") {
      msg =
        "We have sent Credit Memo details in an email to " + this.state.email;
    } else if (type === "error") {
      msg =
        "We are not able to send a mail with Credit Memo details. You can try again at some other time ";
    }
    return msg;
  }
  toggleAlert = () => {
	debugger
  this.setState({
	showAlert: false
  });
};
  render() {
	let viewType, messageText;
    if (this.state.showFailMessage) {
      viewType = "error";
      messageText = this.getMessage("error");
    } else if (this.state.showSucessMessage) {
      viewType = "success";
      messageText = this.getMessage("success");
    }
    return (
      <>
        <div>
		{viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}

          <div className="form-group">
            <div className="row">
              <div className="col-9">
                <input
                  type="text"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-3 pl-0 mob-txt-right">
			  <Button
                  size="xs"
                  viewType="primary"
                  onClickBtn={this.sendCreditMemo}
                  loading={this.state.sendCrdMemo}
                  btnTitle="Send"
                ></Button>
                <button
                  type="button"
                  onClick={this.sendCreditMemo}
                  className="btn btn-xs btn-primary"
                >
                  Send
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};

export default connect(mapStateToProps, null)(HotelEmailCreditMemo);
