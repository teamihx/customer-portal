import React, { Component } from 'react'
import { connect } from "react-redux";
import log from 'loglevel';
import CancellationService from '../../../services/trips/cancel/CancellationService.js';
import DateUtils from '../../../services/commonUtils/DateUtils.js';
class HotelRefundModes extends Component{
    constructor(props) {
        super(props);
        this.state={
            payments_service_data:[],
            checkRefundtoWalletOptn:false,
            currentDate:new Date(),
            uniqPayment:false,
            walletEnableDomain:false,
            checkFullWalletPayment:false,
            walletEnableTripCurrency:false,
            fullyGiftVoucherBooking:false,
            refundToWalletPaymentToCT:"",
            refndToWalletUserEnable:false,
            apiBookingWithTxnSoureceB2c:false,
            expressWayBookingInvolved:false,
            refundModeType:"Original payment mode",
            expresswayCardNumber:"",
            checkRefundMode:true,
            enbaleGiftCard:false,
            giftvoucherPartiallyinvolvedinBooking:false,
            hideWalletOpt:false,
            show_only_partial:false,
            paymentTypes:[],
            termsCondition:true,
            domainsArray:["cleartrip.com","cleartrip.ae","qa.cleartrip.com","cleartrip.sa","kw.cleartrip.com","bh.cleartrip.com","om.cleartrip.com","cleartrip.qa"]
        }
        this.handleChange = this.handleChange.bind(this);
        //this.props.getRefundMode(this.state.refundModeType);
    }

    handleChange(event) {
      this.state.refundModeType=event.target.value;
      //console.log('event.target.value---'+event.target.value);
      this.props.getRefundMode(event.target.value);
    }

    componentDidMount(){
      if(this.props.tripDetail.payments_service_data!==null 
        && this.props.tripDetail.payments_service_data!==undefined
        && this.props.tripDetail.payments_service_data!==''){
      this.setState({payments_service_data: this.props.tripDetail.payments_service_data});
      }
    }

    checkRefundToWalletOption(){
        var date=new Date(DateUtils.convertToDateFormate(this.props.tripDetail.created_at));
        var hours=date.getHours();
        var totalh=hours+24;
        date.setHours(totalh);
      if(date < this.state.currentDate){
        //this.setState({checkRefundtoWalletOptn:true})
        this.state.checkRefundtoWalletOptn=true;
      }
    }
    
    checkUinqPayment(){
      if(this.state.payments_service_data.length>0){
      var uniqueArray = Array.from(new Set(this.state.payments_service_data.map(pay=> pay.payment_type)));
      if(uniqueArray.length>1){
        //this.setState({uniqPayment:true})
        this.state.uniqPayment=true;
      }
    }
    }

    getExpressWayDetails(){
      try{
         const request={
           userId:this.props.tripDetail.user_id,
           currency:this.props.tripDetail.currency
         }
         CancellationService.expressWayCardNumber(request).then(response => {
             if(response.data!==null && response.data!=="" && response.data.data.length!==0 && response.data.status===200){
             if (response.data.card_number!=="" && response.data.data.card_number!==null){
               //this.setState({expresswayCardNumber:response.data.data.card_number})
               this.state.expresswayCardNumber=response.data.data.card_number;
             }else{
               this.setState({expresswayCardNumber:"Card Number not Available"})
             }
            }else{
               this.setState({expresswayCardNumber:"Card Number not Available"})
            }
           });
          
     }catch(err){
         log.error('Exception occured in getExpressWayDetails function---'+err);
     }
 }

 getApiKeyDetails(){
  try{
    const req={
      user_id:this.props.tripDetail.booked_user_id
    }
     CancellationService.getUserApiKey(req).then(response => {
         if(response.data!==null && response.data.status===200 && response.data.data.length!==0 && response.data.data.length!==0){
         if (response.data.data[0].api_key!==null && response.data.data[0].api_key!==undefined){
           this.state.show_only_partial=true;
         }
        }
       });
      
 }catch(err){
     log.error('Exception occured in getApiKeyDetails function---'+err);
 }
}

 toggleAlert = () => {
  this.setState({
    showAlert: false
  });
};

    checkPaymentsTypeNote(){
      try{
      if(this.state.payments_service_data.length!==0){
        for(let payment of this.state.payments_service_data){
          if(payment.status==="S"){
          if(payment.payment_type==="GV"){
            this.setState({enbaleGiftCard:true});
          }else{
             if(payment.payment_card_details.length!==0 && payment.payment_card_details[0].card_number!==null && (payment.payment_type==="CC" || payment.payment_type==="DC")){
              var cardNumber=this.props.paymentStatusMaster[payment.payment_type] + ": " +payment.payment_card_details[0].card_number;
              this.state.paymentTypes.push(cardNumber);
             }else if(payment.emi_count!==undefined && payment.emi_fee!==undefined){
              this.state.paymentTypes.push("EMI" +payment.emi_fee+ "-"+ (payment.emi_count));
             }else if(payment.payment_type==="WT"){
              this.state.paymentTypes.push(this.props.paymentStatusMaster[payment.payment_type]);
             }else if(payment.payment_type==="GV"){
              if(payment.payment_gift_voucher_txns.length!==0){
                this.state.paymentTypes.push(tthis.props.paymentStatusMaster[payment.payment_type]+": " +payment.payment_gift_voucher_txns[0].card_number);
              }
             }else if(payment.payment_type==="TW"){
               //need to check wallet type
               if(payment.payment_tp_wallet_txns.length!==0 && payment.payment_subtype!==undefined && payment.payment_subtype!==null){
                this.state.paymentTypes.push(payment.payment_tp_wallet_txns.length[0].transaction_type);
             }
             }else if(payment.payment_type==="UP"){
              if(payment.payment_subtype!==undefined){
              this.state.paymentTypes.push(this.props.paymentSubTypeMaster[payment.payment_subtype]);
               }
             }else if(payment.payment_type==="EP"){
              this.getExpressWayDetails();
              if(this.state.expresswayCardNumber!==""){
                this.state.paymentTypes.push(this.props.paymentStatusMaster[payment.payment_type] + " " +this.state.expresswayCardNumber);
              }
            }else if(payment.payment_type==="RP"){
              if(payment.payment_subtype!==null && payment.payment_subtype!==undefined && payment.payment_subtype==="ADCB"){
                var subValue="ADCB TouchPoints";
                this.state.paymentTypes.push(subValue);
              }else{
                if(payment.payment_subtype!==null && payment.payment_subtype!==undefined){
                this.state.paymentTypes.push(this.props.paymentSubTypeMaster[payment.payment_subtype]);
                }
              }
             }else{
              this.state.paymentTypes.push(this.props.paymentStatusMaster[payment.payment_type]);
             }
          }
        }
        }
      }
      }catch(err){
        log.error('Exception occured in checkPaymentsTypeNote function---'+err);
        }
    }

    checkRefundModeConditions() {
      try{
        this.checkRefundToWalletOption();
        this.checkUinqPayment();
        this.checkPaymentsTypeNote();

        var enableTripCurrency=this.props.ctConfigRespData.data['ct.accounts.wallet_enabled_trip_currencies']
        var refndToWaltPayment=this.props.ctConfigRespData.data['ct.rails.refund_to_wallet_only_payment_list']
        var refndToWalletUsers=this.props.ctConfigRespData.data['ct.rails.disable_refund_wallet_users']
        var apiTxnSourecTypeUserIds=this.props.ctConfigRespData.data['ct.rails.api.users_with_txn_source_type_b2c']
        var hideWalletOptions=this.props.ctConfigRespData.data['ct.rails.hide_wallet_refund_option']
        
        var domainList =  this.state.domainsArray.filter(domain => domain===this.props.tripDetail.domain);
        if(domainList.length!==0){
        this.state.walletEnableDomain=true;
        }
        if(refndToWalletUsers!==undefined && !refndToWalletUsers.includes(this.props.tripDetail.user_id)){
        this.state.refndToWalletUserEnable=true;
        }
        if(apiTxnSourecTypeUserIds!==undefined && apiTxnSourecTypeUserIds.includes(this.props.tripDetail.user_id)){
        this.state.apiBookingWithTxnSoureceB2c=true;
        }
        if(enableTripCurrency!==undefined && enableTripCurrency.includes(this.props.tripDetail.currency)){
          this.state.walletEnableTripCurrency=true;
        }
        if(this.state.payments_service_data.length!==0){
         var fullWalletPayments =  this.state.payments_service_data.filter(pay => pay.status === "S" && pay.payment_type ==="WT");
         if(fullWalletPayments.length!=0){
          this.state.checkFullWalletPayment=true;
         }
         var fullGiftVoucherBkng =  this.state.payments_service_data.filter(pay => pay.status === "S" && pay.payment_type ==="GV");
         if(fullGiftVoucherBkng.length!=0){
          this.state.fullyGiftVoucherBooking=true;
         }
         var expressWayBkngInvoled =  this.state.payments_service_data.filter(pay => pay.status === "S" && pay.payment_type ==="EP");
         if(expressWayBkngInvoled.length!=0){
          this.state.expressWayBookingInvolved=true;
         }

         var partialGiftVoucherBkng =  this.state.payments_service_data.filter(pay => pay.status === "S" && pay.payment_type ==="GV");
         if(partialGiftVoucherBkng.length!=0){
          this.state.giftvoucherPartiallyinvolvedinBooking=true;
         }

         for(let payment of this.state.payments_service_data){
            if(payment.status==="S" && refndToWaltPayment!==undefined && payment.payment_subtype!==undefined && refndToWaltPayment.includes(payment.payment_subtype) ){
              this.state.refundToWalletPaymentToCT=this.props.paymentSubTypeMaster[payment.payment_subtype];
              break;
            }
         }
         var url = window.location.href;
         if(hideWalletOptions!==undefined && hideWalletOptions==="ON" && url.includes("account/trips")){
          this.state.hideWalletOpt=true;
         }
      }
      }catch(err){
      log.error('Exception occured in checkRefundModeConditions function---'+err);
      }
    }

    render() {
     if(this.state.checkRefundMode && this.state.payments_service_data.length!==0 && this.props.ctConfigRespData.data!==undefined){
       this.checkRefundModeConditions();
       this.getApiKeyDetails();
       this.setState({checkRefundMode:false})
     }
     
      return (
        <>
              {!this.state.checkRefundtoWalletOptn? (
                 <div className="side-pnl">
                    Want to proceed with cancellation? <br/>
                    <label>
                  <input type="radio" value="Original payment mode" defaultChecked onChange={this.handleChange} name="refundModeType"/>Original payment mode     
                  </label>   
                  {this.state.uniqPayment && (
                  <div>
                    <label>Get refund partially to </label>
                  </div>
                  )}
                 
                    {this.state.enbaleGiftCard && (
                      <div>
                    <label>GIFT FLAG : Gift Card</label>
                     </div>
                    )}
                    <br/>
                  
                    {this.state.paymentTypes!==null && this.state.paymentTypes !== "" && 
                    this.state.paymentTypes !== undefined && this.state.paymentTypes.length>0 && (
                    <div>
                    {this.state.paymentTypes.map((item, key) => <span key={item}>{item} <br/> </span>)}
                    </div>
                    )}
                 
                   
              </div>
            ) : (
              <div className="side-pnl">
              {!this.state.show_only_partial && this.state.walletEnableDomain && !this.state.checkFullWalletPayment 
              && this.state.walletEnableTripCurrency && !this.state.giftvoucherPartiallyinvolvedinBooking && !this.state.fullyGiftVoucherBooking && !this.state.hideWalletOpt && (
                <div>
                  <label>Want to proceed with cancellation? Select refund mode.</label>
                  <br/>
                  <label>
                  <input type="radio" value="Original payment mode" onChange={this.handleChange} name="refundModeType"/>Original payment mode     
                  </label>   
                {this.state.payments_service_data.length !== 0 && this.state.uniqPayment && (
                  <div>
                    <label>Get refund partially to</label>
                  </div>
                  )}
                  <div>
                  {this.state.enbaleGiftCard && (
                  <label>GIFT FLAG : Gift Card</label>
                  )}
                  <br/>
                  <div>
                    {this.state.paymentTypes!==null && this.state.paymentTypes !== "" && 
                    this.state.paymentTypes !== undefined && this.state.paymentTypes.length>0 && (
                    <div>
                    {this.state.paymentTypes.map((item, key) => <span key={item}>{item} <br/> </span>)}
                    </div>
                    )}
                  </div>
                 </div>
                  {this.state.refundToWalletPaymentToCT!=="" && (
                    <div>
                    <span>{this.state.refundToWalletPaymentToCT} will be refunded back to Cleartrip wallet</span>
                    </div>
                  )}

                {(this.props.tripDetail.txn_source_type==="ACCOUNT" || this.props.tripDetail.txn_source_type==="MOBILE") && this.state.refndToWalletUserEnable &&
                !this.state.apiBookingWithTxnSoureceB2c && this.props.tripDetail.txn_source_type!=="API" && !this.state.expressWayBookingInvolved && (
                  <div>
                 <label>
                    <input type="radio" value="Wallet" onChange={this.handleChange} name="refundModeType"/>Wallet
                </label>
                <br/>
             <label>Get faster refunds to your wallet and book your next trip without waiting for your bank <a href="http://www.cleartrip.com/wallet" target="_blank"> Learn more</a></label>
                 <br/>
                 <ul>
                 <li>By selecting Cleartrip Wallet you have agreed to park money in your Cleartrip Account as a prepaid balance, and can be used only for bookings of Flights or Hotels at <a href="/">Cleartrip</a></li>
                 <li>Cleartrip will not accept any dispute on the transactions wherein the refund is processed in Cleartrip Wallet. The customer should contact Cleartrip customer support for further assistance.</li>
                 <li>The Cleartrip Wallet can be used only for the online transactions.</li>
                 <li>The balance in Cleartrip Wallet account shall not be refunded in any circumstances.</li>
                </ul>
                <br/>
                <div>
                <input
                  id="termsCondition"
                  type="checkbox"
                  className="customCheckbox"
                  defaultChecked={this.state.termsCondition}
                  value={this.state.termsCondition}
                />
                <label>Please go through the booking policies and terms that are linked from below. Then mark the checkbox if you agree, we know it's boring but it's important. &nbsp;I understand and agree to the</label>
                <a href="https://www.cleartrip.com/html/b2c/wallet_policy.shtml" target="_blank">Terms &amp; Conditions</a>  of using Wallet
              
              </div>
              </div>
                 )}
              </div>
                )}
              </div>
            )}
           
            </>
    )
}
}
    

const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail,
      ctConfigRespData:state.trpReducer.ctConfigData,
      paymentStatusMaster: state.trpReducer.paymentTypeMaster,
      paymentSubTypeMaster: state.trpReducer.paymentSubTypeMaster
    };
  };
  const mapDispatchToProps = dispatch => {
    return {
        onInitCTConfigDtl: (req) => dispatch(actions.initCTConfigData(req)),
    }
  }
export default connect(mapStateToProps,mapDispatchToProps)(HotelRefundModes);