import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import { Link } from "react-router-dom";
import log from "loglevel";
import HotelTripDtls from "./HotelTripDtls";
import Header from "Pages/common/Header";
import DateUtils from "../../../services/commonUtils/DateUtils";
import Icon from "Components/Icon";
import ShowHide from "Components/ShowHide.jsx";
import CommonTripDetails from "../generic/CommonTripDetails";
import Button from "Components/buttons/button.jsx";
import HotelRefundDetails from "./HotelRefundDetails";
import CancellationService from "../../../services/trips/cancel/CancellationService.js";
import NoteService from "../../../services/trips/note/NoteService.js";
import FlightService from "../../../services/trips/flight/FlightService.js";
import HotelPricingDetail from "./HotelPricingDetail";
export const USER_AUTH_DATA = "userAuthData";
import UserFetchService from "../../../services/trips/user/UserFetchService";
import HotelRefundModes from "./HotelRefundModes";
import Notification from '../../../components/Notification'; 
import CommonMsg from "../../../components/commonMessages/CommonMsg";
export const contextPath=process.env.contextpath;
export let refundmode='';
class HotelCancelTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tripId: window.location.href.split("/")[
        window.location.href.split("/").length - 2
      ],
      flagLogo: "https://" + process.env.s3bucket,
      cancelNote: "",
      cancelbtn: false,
      refundDetailsresponse: "",
      cancelDetailsresponse: "",
      tripNotes: '',
      publicIp: "",
      usrDtlUpdated: false,
      errormsg:'',
      sucessmsg:'',
      txns:'',
      cancelTxn:'',
      updatedTxns:'',
      updatedNotes:'',
      showAlert:false

    };

    this.req = {
      tripId: this.state.tripId
    };
    this.handleCancelnote = this.handleCancelnote.bind(this);
    this.hotelCancelHandler = this.hotelCancelHandler.bind(this);
    this.loadRefundCalculationDetails = this.loadRefundCalculationDetails.bind(
      this
    );

    console.log("this.state.data---" + window.location);
    console.log("this.state.data---" + JSON.stringify(window.refund_data));
    console.log("constructor");

    if (
      window.refund_data === null ||
      window.refund_data === undefined ||
      window.refund_data == ""
    ) {
      this.loadRefundCalculationDetails();
    } else {
      this.state.refundDetailsresponse = window.refund_data;
    }
  }

  componentDidMount() {
    console.log("componentDidMount");
    this.props.onInitTripDtl(this.req);
  }

  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  redirectUserAccount = (usrId, e) => {
    e.persist();
    let url = "";
    url = Domainpath.getHqDomainPath() + "/hq/people/" + usrId;
    window.open(url);
  };

  getDomain = domain => {
    let country = null;
    if (typeof domain !== "undefined") {
      let frm = domain.lastIndexOf(".") + 1;
      let country = domain.substring(frm, domain.length);
      if (country === "com") {
        country = "in";
      }
      return country;
    }
  };

  loadTripDetail = () => {
    let first_name = "";
    let last_name = "";
    let title = "";

    if (
      !this.isEmpty(this.props.tripDetailSt) &&
      this.props.tripDetailSt.msg === "SUCCESS"
    ) {
      if (
        this.isEmpty(this.props.usermasterdetails) ||
        typeof this.props.usermasterdetails === "undefined"
      ) {
        this.loadUsrDetail();
      }

      if (
        !this.isEmpty(this.props.usermasterdetails) &&
        this.props.usermasterdetails.status !== "404"
      ) {
        let usrDtl = this.props.usermasterdetails[
          this.props.tripDetailSt.booked_user_id
        ];

        if (
          typeof usrDtl !== "undefined" &&
          typeof usrDtl.personal_data !== "undefined"
        ) {
          title = usrDtl.personal_data.title;
          first_name = usrDtl.personal_data.first_name;
          last_name = usrDtl.personal_data.last_name;
        }
      }
    }
    title = title !== null ? title + ". " : "";
    first_name = first_name !== null ? first_name : "";
    last_name = last_name !== null ? last_name : "";
    return title + " " + first_name + " " + last_name;
  };

  handleCancelnote(event) {
    console.log("event.target.value--" + event.target.value);
    event.persist();
    this.setState({ cancelNote: event.target.value });
  }

  loadRefundCalculationDetails() {
    const req = {
      tripRefNumber: this.state.tripId
    };
    try {
      CancellationService.cancelRefundInfo(req)
        .then(response => {
          if (
            response.status !== null &&
            response.status !== undefined &&
            response.status === 200 &&
            response.data !== null &&
            response.data !== undefined &&
            response.data !== "" &&
            response.data.data !== null &&
            response.data.data !== undefined &&
            response.data.data !== ""
          ) {
            this.state.refundDetailsresponse = response.data.data;
            this.forceUpdate();
          }
        })
        .catch(error => {
          log.error(
            "Exception occured in HotelRefundDetails component loadRefundCalculationDetails" +
              error
          );
        });
    } catch (error) {
      log.error(
        "Exception occured in HotelRefundDetails component loadRefundCalculationDetails function---" +
          error
      );
    }
  }

  loadUsrDetail = () => {
    //console.log("Loyalty===" + this.props.tripDetailSt.loyalty_user_type)
    if (!this.isEmpty(this.props.tripDetail)) {
      let userIds = [];
      userIds.push(this.props.tripDetail.booked_user_id);
      const reqUsr = {
        person_ids: userIds,
        query: ["username", "personal_data"]
      };

      let userDetailsresponse = {};
      UserFetchService.fetchUserMailid(reqUsr).then(response => {
        userDetailsresponse = JSON.parse(response.data);
        /* console.log(
          "userDetailsresponse" + JSON.stringify(userDetailsresponse)
        ); */

        const usrClassReq = {
          username: userDetailsresponse[userIds[0]].username
        };
        //console.log("userIds===" + JSON.stringify(userDetailsresponse[userIds[0]]))
        UserFetchService.fetchUserClassDtl(usrClassReq).then(res => {
          // let userclassDetailsresponse = JSON.parse(res.data);
          this.userclassDetailsresponse = JSON.parse(res.data);
          //console.log("fetchUserClassDtl==="+JSON.stringify(userclassDetailsresponse))
          this.props.updateUserDetails(userDetailsresponse);
          this.setState({ usrDtlUpdated: true }, () =>
            console.log("userdtl===" + this.state.usrDtlUpdated)
          );
        });
      });
    }
  };

  loadTripDetail = () => {
    //console.log("user detail====" + JSON.stringify(this.props.usermasterdetails));

    if(!this.isEmpty(this.props.tripDetail)){
      this.state.txns=this.props.tripDetail.txns;
      this.state.tripNotes=this.props.tripDetail.notes;
      console.log("this.state.txns" + JSON.stringify(this.state.txns));
      console.log("this.state.tripNotes" + JSON.stringify(this.state.tripNotes));

    }

    if (
      !this.isEmpty(this.props.tripDetail) &&
      this.props.tripDetail.msg === "SUCCESS"
    ) {
      if (
        this.isEmpty(this.props.usermasterdetails) ||
        typeof this.props.usermasterdetails === "undefined"
      ) {
        this.loadUsrDetail();
      }
    }
  };

  

  async hotelCancelHandler(event) {
    console.log("hotelCancelHandler--entered");    
    let wallet_value=false;    
    try {
      if(refundmode!==null && refundmode!==undefined 
        && refundmode!==''  && refundmode==='Wallet'){
        wallet_value=true;
      }
      this.setState({ cancelbtn: true });
      await this.fetchPublicIp();
      console.log("this.state.publicip is--" + this.state.publicIp);
      let authData = localStorage.getItem(USER_AUTH_DATA);
      let obj = JSON.parse(authData);
      this.req = {
        source_id: this.state.publicIp,
        source_type: 'HQ',
        user_id: obj.data.id,
        wallet: wallet_value,
        tripRefNumber: this.state.tripId,
        trip_id:this.props.tripDetail.id,
        note:this.state.cancelNote
      };

      console.log("autoCancellation----req is---" + JSON.stringify(this.req));
      CancellationService.autoCancellation(this.req)
        .then(response => {
          console.log("autoCancellation .then----res is---" + JSON.stringify(response.data));
         

          if(response.data !== null &&
            response.data !== undefined &&
            response.data !== ""){
              const canceResponse=response.data[0];
              const noteResponse=response.data[1];
              const txnResponse=response.data[2];
          if (canceResponse!==null && canceResponse!==undefined && canceResponse!=='' &&
            canceResponse.status !== null &&
            canceResponse.status !== undefined &&
            canceResponse.status === 200 &&
            canceResponse.data !== null &&
            canceResponse.data !== undefined &&
            canceResponse.data === 'Success') {
            console.log("autoCancellation res is--- ");          
           //this.updateCancelTxns(txnResponse);
           //this.updateTripNotes(noteResponse);
           this.redirectTripDetailsPage();
           this.setState({ cancelbtn: false });
           this.forceUpdate();
          }else{
            console.log("autoCancellation res is else block--- ");
            this.updateCancelTxns(txnResponse);
            this.updateTripNotes(noteResponse);
            this.state.errormsg='Hotel Cancellation is not successful';
            this.state.showAlert=true;
            this.setState({ cancelbtn: false });
            this.forceUpdate();
            
          }
        }
        })
        .catch(error => {
          this.setState({ cancelbtn: false });
          log.error(
            "Exception occured in HotelCancel component hotelCancelHandler" +
              error
          );
        });
    } catch (err) {
      this.setState({ cancelbtn: false });
      log.error(
        "Exception occured in HotelCancel component hotelCancelHandler function---" +
          err
      );
    }
  }

  updateCancelTxns(data){
    //console.log("this.state.txns==== entered"+JSON.stringify(this.state.txns));   
    
    this.setState({         
      updatedTxns:this.state.txns.concat(data.data.modelsUpdated.txns).sort((a,b)=>
          ( new Date(a.created_at).getTime() - new Date(b.created_at).getTime())
      ).reverse()
      
  })
  //console.log("this.state.updatedTxns==== entered"+JSON.stringify(this.state.updatedTxns))  
  this.props.tripDetail.txns=this.state.updatedTxns;
  this.props.updateTxns(this.props.tripDetail);
  //console.log("updateTripNotes==== end")
    
   
   }

  updateTripNotes(data){
    //console.log("updateTripNotes==== entered"+JSON.stringify(data.data.modelsUpdated.notes));
    //console.log("this.state.tripNotes==== entered"+JSON.stringify(this.state.tripNotes));
   
    
     let noteDetails=data.data.modelsUpdated.notes;
    this.setState({
       
      updatedNotes:this.state.tripNotes.concat(noteDetails).sort((a,b)=>
            ( new Date(a.created_at).getTime() - new Date(b.created_at).getTime())
        ).reverse(),
        newNote:''
        
    })
   // console.log("this.state.updatedNotes==== entered"+JSON.stringify(this.state.updatedNotes));
    this.props.tripDetail.notes=this.state.updatedNotes
    this.props.updateNotes(this.props.tripDetail);
    //console.log("updateTripNotes==== end")
    
   
   }

   redirectTripDetailsPage(){    
    const url=contextPath+"/trips/"+this.state.tripId;    
    localStorage.setItem("msg_value", 'Hotel Cancellation is successful');
    window.location.assign(url);
  
  } 

   

  fetchPublicIp() {   

    try {
      return new Promise((resolve, reject) => {
        let txnReq = "";
        FlightService.getpublicIp(txnReq)
          .then(response => {
            // console.log('this.state.publicIp respo--'+JSON.stringify(response));
            if (response.status === 200) {
              this.state.publicIp = response.data.ip_address;              
              resolve(response.data.ip_address);
            }
          })
          .catch(error => {
            assert.isNotOk(error, "Promise error");
            reject(error);
          });
      });
    } catch (err) {
      reject(err);
      log.error(
        "Exception occured in FlightUpdateTicketNumber Component closeUpdatT" +
          err
      );
    }
  }

  toggleAlert = () => {    
    this.setState({
      showAlert: false
    });
  };

  loadRefundModes(selRefundMode) {    
    if (selRefundMode !== undefined && selRefundMode !== null) {    
        refundmode=selRefundMode;       
    }    
  }

  render() {
    console.log("render-----");
    let viewType, messageText;
    if (this.state.errormsg !== null && this.state.errormsg !== undefined 
     && this.state.errormsg !=='' ) {
       viewType = "error";
       messageText = this.state.errormsg;
     }
    
    return (
      <>
        <Header />
        {viewType && messageText && (
        <Notification
          viewType={viewType}
          viewPosition="fixed"
          messageText={messageText}
          showAlert={this.state.showAlert}
          toggleAlert={this.toggleAlert}
        />
      )}
        {(this.props.tripDetail!==null && this.props.tripDetail!==undefined 
        && this.props.tripDetail!=='' && this.props.tripDetail.status===200) ? (
          <>
            <CommonTripDetails
              usrDtlUpdated={this.state.usrDtlUpdated}
              loadTripDetail={this.loadTripDetail}
            />

            <section className="topSec bg-c-gr">
              <div className="main-container">
                <div className="row">
                  <div className="col-9 tabPanel">
                    <div className="tripDetails cancelTrip mt-5">
                      <HotelTripDtls cancelTrip="htlCanceltrip" />

                      <div className="highInfo border mb-10 ">
                        Add a note about this cancellation
                      </div>
                      <textarea
                        id="cnclNoteId"
                        type="text"
                        value={this.state.cancelNote}
                        onChange={this.handleCancelnote}
                      ></textarea>
                      <br/>
                      <div>
                      <HotelRefundModes getRefundMode={this.loadRefundModes}/>
                      </div>
                      <div className="btnSec">
                        <Button
                          size="xs"
                          viewType="primary"
                          onClickBtn={this.hotelCancelHandler}
                          loading={this.state.cancelbtn}
                          btnTitle="Make Cancellation"
                        ></Button>
                      </div>
                    </div>
                  </div>
                  <aside className="col-3 right-bar pt-0">
                    <div className="trip-summary">
                      <ShowHide visible="true" title="Refund calculation">
                        <div className="showHide-content">
                          {this.state.refundDetailsresponse !== null &&
                            this.state.refundDetailsresponse !== undefined &&
                            this.state.refundDetailsresponse !== "" ? (
                              <HotelRefundDetails
                                refund_details={
                                  this.state.refundDetailsresponse
                                }
                              />
                            ):("Refund details not avaliable")}
                        </div>
                      </ShowHide>
                      <HotelPricingDetail />
                    </div>
                  </aside>
                </div>
              </div>
            </section>
          </>
        ) : (
          <>
          {(this.props.tripDetail!==null && this.props.tripDetail!==undefined 
              && this.props.tripDetail!=='' && this.props.tripDetail.status!==200) ?
         (<CommonMsg errorType= "datanotFound" errorMessageText="Trip is not found" />):("test123")         
         }
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  console.log("mapStateToProps function---");
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};

const mapDispatchToProps = dispatch => {
  console.log("mapDispatchToProps function---");
  return {
    onInitTripDtl: req => dispatch(actions.initTripNdCtConfig(req)),
    updateTxns: (req) => dispatch(actions.updateTxnsDetails(req)),
    updateNotes: req => dispatch(actions.updateNoteDetails(req)),
    updateUserDetails: req => dispatch(actions.updateUserDetails(req))
   
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HotelCancelTrip);
