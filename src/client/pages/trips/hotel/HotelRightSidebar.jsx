import React, { Component } from 'react'
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
export const contextPath=process.env.contextpath;
import HotelPricingDetail from "./HotelPricingDetail";
import Tags from 'Pages/trips/generic/Tags.jsx';
import ShowHide from 'Components/ShowHide.jsx';
import ChangeBkgStatusClsTxn from "../generic/ChangeBkgStatusClsTxn";
import HotelEmailDetails from './HotelEmailDetails';
import HotelEmailVoucher from './HotelEmailVoucher';
import HotelEmailSalesInvoice from './HotelEmailSalesInvoice';
import EmailBookStepScreenShot from '../generic/EmailBookStepScreenShot';
import { Link } from 'react-router-dom';
import ToggleDisplay from 'react-toggle-display';
import TripGenericService from '../../../services/trips/generic/TripGenericService';
import SourceTypeInfo from '../generic/SourceTypeInfo'
export const TRIPS_ROLES = 'tripsRoles'
import Domainpath from '../../../services/commonUtils/Domainpath';
import Button from 'Components/buttons/button.jsx'
import CancellationService from "../../../services/trips/cancel/CancellationService";
class HotelRightSidebar extends Component {
    constructor(props) {
        super(props)
	this.state = {
      emailDetailsShow: false,
 	  emailVoucherShow: false,
	  emailSalesInvoiceShow: false,
	emailShowBookStepSceenShow:false,
	voucherUrl : Domainpath.getHqDomainPath()+'/voucher',
	 openTxnAvailable:false,
	  voucherAvailable:false,
	 receiptAvailableHtl:false,
	  triplinkRoles:'',
	  refundDetails:'',
	  cancelURL:''
    }
		this.redirect=this.redirect.bind(this);
		this.loadCancelTripDetails=this.loadCancelTripDetails.bind(this);
		
this.renderComponents = this.renderComponents.bind(this);

//Getting ROLES DATA from local storage
  let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
  this.state.tripHtlRoles=tripsObj.htrips;
      }

	loadTripConditions=()=>{
	this.state.openTxnAvailable = TripGenericService.checkForOpenTxnAvailable(this.props.tripDetail.txns);
	let voucherNumber = this.props.tripDetail.hotel_bookings[0].voucher_number;
	if(voucherNumber!==null && voucherNumber!==undefined && voucherNumber!==''){
		this.state.voucherAvailable=true;
	}
	for(let invoices in this.props.tripDetail.invoices){
		let invoiceData = this.props.tripDetail.invoices[invoices];
		if(invoiceData.category!==null && invoiceData.category!==undefined && invoiceData.category!==''){
			if(invoiceData.category==='PaymentReceipt'){
				this.state.receiptAvailableHtl=true;
				break;
			}
		}
	}
	}
    redirect(event){
        try{
        event.preventDefault();
        let url="";
        var domainpath = Domainpath.getHqDomainPath();
        if(event.target.id === 'xml'){
          url =domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/xml"
          }else if(event.target.id === 'lossTracker'){
            url = domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/loss_tracker/"
          }else if(event.target.id === 'tripCancel'){
            url =domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/cancel/"
          }else if(event.target.id === 'bookstepscreen'){
            url = domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/screenshots"
          }else if(event.target.id === 'Printtaxinvoice'){
            url = domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/invoice?st_invoice=true"
          }else if(event.target.id === 'Printpaymentreceipt'){
            url = domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/invoice?payment_receipt=true"
          }else if(event.target.id === 'Printhotelinvoice'){
            url = domainpath+"/hq/trips/hotel_invoice/"+this.props.tripDetail.trip_ref
          }else if(event.target.id === 'Printtaxcreditmemo'){
            url = domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/invoice?credit_memo=true"
          }else if(event.target.id === 'PrintCancellationinvoice'){
            url = domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/cancellation_invoice"
          }
          
          window.open(
            url
          );
        }catch(err){

            log.error('Exception occured in Footer redirect function---'+err);

        }

    }

renderComponents(e) {
        this.currentClick = e.target.id;
      
        if (this.currentClick === 'hotelemailDtls') {
            this.setState({
      				emailDetailsShow: !this.state.emailDetailsShow,
    		});
        }else if (this.currentClick === 'hotelemailVoucher') {
            this.setState({
      				emailVoucherShow: !this.state.emailVoucherShow,
    		});
        }else if (this.currentClick === 'hotelemailSalesInvoice') {
            this.setState({
      				emailSalesInvoiceShow: !this.state.emailSalesInvoiceShow,
    		});
        }else if (this.currentClick === 'hotelemailBKStepScreen') {
            this.setState({
      				emailShowBookStepSceenShow: !this.state.emailShowBookStepSceenShow,
    		});
        }
		  
        

    }

   loadCancelTripDetails1(event){
   const url=contextPath+"/trips/"+this.props.tripDetail.trip_ref+"/cancel";
    const req = {
        tripRefNumber: this.props.tripDetail.trip_ref
      };
      try{
        CancellationService.cancelRefundInfo(req).then(response => {            
            if(response.status!==null && response.status!==undefined && response.status===200 && 
                response.data!==null && response.data!==undefined && response.data!=='' &&
                response.data.data!==null && response.data.data!==undefined && response.data.data!=='' ){
					this.state.refundDetails=response.data.data;
					var newWindow=window.open(url);
		            newWindow.refund_data = this.state.refundDetails;
            }            
             
          }
          ).catch((error) => {
			var newWindow=window.open(url);
			newWindow.refund_data = '';
            log.error('Exception occured in HotelRightSideBar component loadCancelTripDetails cancelRefundInfo call---'+error);
         });  

      }catch(error){
		var newWindow=window.open(url);
		newWindow.refund_data = '';
        log.error('Exception occured in HotelRightSideBar component loadCancelTripDetails---'+error);
	  }
	  
	  }
	  
	  loadCancelTripDetails(event){
		const url=contextPath+"/trips/"+this.props.tripDetail.trip_ref+"/cancel";		 
		window.open(url);
	   }
	  


    render() {
    {this.loadTripConditions()}
        let tripJsonUrl=contextPath+"/trips/"+this.props.tripDetail.trip_ref+"/json"
        return (
     
            <>
          <div className="side-pnl">
          <ShowHide visible="true" title="Tips, tools & extras">
		  <div className="showHide-content">
           <ul className="mb-20">
			{this.state.tripHtlRoles.HTL_EMAI_TRIP_DETAILS && !this.state.openTxnAvailable && this.props.tripDetail.hotel_bookings[0].booking_status==='P' &&
			  <li>
				<a id="hotelemailDtls" onClick={this.renderComponents} className={this.currentClick === "hotelemailDtls" ? "active" : ""}>
				<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Trip Details</a>
				 <ToggleDisplay show={this.state.emailDetailsShow}>
					<HotelEmailDetails />
				</ToggleDisplay>
				</li>
				}
				{this.state.tripHtlRoles.HTL_EMAI_VOUCHER && !this.state.openTxnAvailable && this.state.voucherAvailable && this.props.tripDetail.hotel_bookings[0].booking_status==='P' &&
				<li>
				<a id="hotelemailVoucher" onClick={this.renderComponents} className={this.currentClick === "hotelemailVoucher" ? "active" : ""}>
				<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Hotel Vocher </a>
				 <ToggleDisplay show={this.state.emailVoucherShow}>
					<HotelEmailVoucher />
				</ToggleDisplay>
				</li>
				}
				  {this.state.tripHtlRoles.HTL_EMAI_TAX_INVOICE &&  !this.state.openTxnAvailable && this.props.tripDetail.hotel_bookings[0].booking_status!=='H' && this.props.tripDetail.hotel_bookings[0].booking_status!=='Z' &&
			 <li>
				<a id="hotelemailSalesInvoice" onClick={this.renderComponents} className={this.currentClick === "hotelemailSalesInvoice" ? "active" : ""}>
				<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Tax Invoice</a>
				 <ToggleDisplay show={this.state.emailSalesInvoiceShow}>
					<HotelEmailSalesInvoice />
				</ToggleDisplay>
				</li> 
				}
				{this.state.tripHtlRoles.HTL_EMAI_BOOK_STEP_SCREEN &&
				 <li>
				<a id="hotelemailBKStepScreen" onClick={this.renderComponents} className={this.currentClick === "hotelemailBKStepScreen" ? "active" : ""}>
				<Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Bookstep Screenshots</a>
				 <ToggleDisplay show={this.state.emailShowBookStepSceenShow}>
					<EmailBookStepScreenShot />
				</ToggleDisplay>			
			
				
				</li>
				}
               {/* <li><a href=""><Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>SMS Trip Details</a></li> */}
			 {this.state.tripHtlRoles.HTL_PRINT_TAX_INVOICE && !this.state.openTxnAvailable && this.props.tripDetail.hotel_bookings[0].booking_status!=='H' && this.props.tripDetail.hotel_bookings[0].booking_status!=='Z' &&
			  <li><a id="Printtaxinvoice" href="/" onClick={this.redirect} title="Print tax invoice">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print tax invoice</a></li>
               
				}
             
				{this.state.tripHtlRoles.HTL_PRINT_PAY_RECEIPT && !this.state.openTxnAvailable && this.props.tripDetail.hotel_bookings[0].booking_status!=='H' && this.props.tripDetail.hotel_bookings[0].booking_status!=='Z' && this.state.receiptAvailableHtl &&
				<li><a id="Printpaymentreceipt" href="/" onClick={this.redirect} title="Print payment receipt">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print Payment Receipt</a></li> 
				}
				{this.state.tripHtlRoles.HTL_PIRNT_INVOICE && !this.state.openTxnAvailable &&  this.props.tripDetail.hotel_bookings[0].booking_status!=='H' && this.props.tripDetail.hotel_bookings[0].booking_status!=='Z' &&              
				<li><a id="Printhotelinvoice" href="/" onClick={this.redirect} title="Print hotel invoice">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print Hotel Invoice</a></li>
				}
				{this.state.tripHtlRoles.HTL_PRINT_TAX_CREDIT_MEMO && !this.state.openTxnAvailable && this.props.tripDetail.hotel_bookings[0].booking_status!=='H' && this.props.tripDetail.hotel_bookings[0].booking_status!=='Z' &&
				<li><a id="Printtaxcreditmemo" href="/" onClick={this.redirect} title="Print tax credit memo">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print Tax Credit Memo</a></li>
				}
				{this.state.tripHtlRoles.HTL_PRINT_CANCLE_INVOICE && !this.state.openTxnAvailable && this.props.tripDetail.hotel_bookings[0].booking_status=='K' &&
				<li><a id="PrintCancellationinvoice" href="/" onClick={this.redirect} title="Print Cancellation invoice">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print Cancellation Invoice</a></li>   
				}            
				{this.state.tripHtlRoles.HTL_PRINT_VOUCHER && !this.state.openTxnAvailable && this.state.voucherAvailable && this.props.tripDetail.hotel_bookings[0].booking_status==='P' &&
				<li>	
        			
				<form id="eticketForm" target="_blank" action={this.state.voucherUrl} method="post">
				<input type="hidden" name="confirmation_number" value={this.props.tripDetail.trip_ref} />
				<input type="hidden" name="last_name" value={this.props.tripDetail.contact_detail.last_name} />		
        		<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/> 		
				<input className="btn btn-text link-Color no-text-decr font-13" type="submit" value="  &nbsp;Print Hotel voucher"/>		
				</form>
				</li>  
				} 
              {this.state.tripHtlRoles.HTL_LOSS_TRACKER && 
               <li><a id="lossTracker"  href="/loss_tracker" target="_blank" onClick={this.redirect} title=" Loss Tracker For trip">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Loss Tracker For Trip</a>
				</li>
				}
				{this.state.tripHtlRoles.HTL_BOOK_STEP_SCREEN && 
               <li><a id="bookstepscreen" href="/" onClick={this.redirect} title="Bookstep Screenshots">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Bookstep Screenshots</a>
				</li>
				}
               {/* <li><a href=""> <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Reward program</a></li> */}
              
			   {this.state.tripHtlRoles.HTL_TRIP_XML && 
			 <li><a id="xml"  href="/xml_tracker" target="_blank"   onClick={this.redirect} title="Trip XML"><Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Trip XML</a></li>
			 }
			   {this.state.tripHtlRoles.HTL_TRIP_JSON &&    
			 <li>
                <a id="json"  href={tripJsonUrl}  target="_blank" title="Trip JSON"><Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Trip JSON</a>
			</li>
				}
               
               </ul>
 				<>
			{this.state.tripHtlRoles.HTL_CHANG_BKNG_STATUS_CLS_TXN &&    
               <ChangeBkgStatusClsTxn tripId={this.props.tripDetail.trip_ref}/>
			}
                </> 
                 {this.state.tripHtlRoles.HTL_CANCLEL_TRIP && this.props.tripDetail.hotel_bookings[0].booking_status==='P'?<div className="amndmnt mb-10 pl-20 pr-20">  
                    <a id="tripCancel" className="btn btn-default btn-xs"  href="/cancel" target="_blank" onClick={this.redirect} title="Cancel trip/Calculate refund"> Cancel trip/Calculate refund</a>
                    </div>:''} 

					{/* {this.state.tripHtlRoles.HTL_CANCLEL_TRIP && this.props.tripDetail.hotel_bookings[0].booking_status==='P'?<div className="amndmnt mb-10 pl-20 pr-20">  
                    <a id="tripCancel1" className="btn btn-default btn-xs"  href={this.state.cancelURL} target="_blank" onClick={this.loadCancelTripDetails} title="Cancel trip/Calculate refund New"> Cancel trip/Calculate refund New</a>
                    </div>:''} */}

    {/*  {this.state.tripHtlRoles.HTL_CANCLEL_TRIP && this.props.tripDetail.hotel_bookings[0].booking_status==='P'?
	        <div className="btnSec">
                   <Button
                      size="xs"
                      viewType="default"
                      id="updatetktbtnId"
                      className="w-80"
                      onClickBtn={this.loadCancelTripDetails}                      
                      btnTitle='Cancel trip/Calculate refund New'>                      
                    </Button> </div> :''}  */}

     

               </div>
			   </ShowHide>
               </div>
			   <SourceTypeInfo/>
			   {this.state.tripHtlRoles.HTL_TAG_ROLE &&    
                 <Tags/>
			   }
              
			   {this.state.tripHtlRoles.HTL_PRICING_DEATAILS &&    
                <HotelPricingDetail />
			}
				
              

           </>

        )
    }

}
const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
}
export default connect(mapStateToProps, null)(HotelRightSidebar);

