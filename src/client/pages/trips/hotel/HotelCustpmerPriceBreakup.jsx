import React, { Component } from "react";
import Popup from "reactjs-popup";
import { Link } from "react-router-dom";
import utility from "../../common/Utilities";

class HotelCustomerPriceBreakup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  openModal() {
    this.setState({ open: true });
  }
  closeModal() {
    this.setState({ open: false });
  }

  render() {
    return (
      <>
        <Popup
          trigger={
            <a className="linkRight" onClick={this.openModal}>
              {utility.PriceFormat(
                this.props.data.total_fare / this.props.data.room_count,
                this.props.data.currency
              )}
            </a>
          }
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
          position="bottom right"
        >
          {close => (
            <div className="modal pop-grid text-left">
              <a className="close" onClick={close}>
                &times;
              </a>
              <ul>
                <li>
                <span>Room Rate :</span>
                  {this.props.data.total_base_fare +
                    this.props.data.total_markup}
                </li>
                {this.props.data.total_supfee_prop_srv_chg !== 0 && (
                  <li>
                    <span>Hotel Service Charge : </span>
                    {this.props.data.total_supfee_prop_srv_chg}
                  </li>
                )}
                {this.props.data.is_gst !== "Y" ? (
                  this.props.data.total_tax_sup !== 0 ? (
                    <li>
                     <span> Hotel Taxes and Fees : </span> {this.props.data.total_tax_sup}
                    </li>
                  ) : null
                ) : this.props.data.total_tax_sup_gst !== 0 ? (
                  <li><span>GST : </span>{this.props.data.total_tax_sup_gst}</li>
                ) : null}
                {this.props.data.total_tax !== 0 ? (
                  <li>
                   <span> Other tax :</span>
                    {this.props.data.total_tax -
                      this.props.data.total_tax_sup_gst -
                      this.props.data.total_tax_sup -
                      this.props.data.total_tax_svc}
                  </li>
                ) : null}
                {this.props.data.total_discount !== 0 ? (
                  <li><span>Discount </span>{this.props.data.total_discount}</li>
                ) : null}
                {this.props.data.total_tax_sup_vat !== 0 ||
                this.props.data.total_tax_prop_srv_chg_vat !== 0 ? (
                  <li>
                    <span>Vat : </span>
                    {this.props.data.total_tax_sup_vat +
                      this.props.data.total_tax_prop_srv_chg_vat}
                  </li>
                ) : null}
                {this.props.data.total_tax_municipal_fee !== 0 ||
                  (this.props.data.total_tax_city_tax !== 0 && (
                    <li>
                      <span>City Tax/Municipal Fee : </span>
                      {this.props.data.total_tax_city_tax +
                        this.props.data.total_tax_municipal_fee}
                    </li>
                  ))}
                {this.props.data.total_cashback !== 0 ? (
                  <li><span>Cashback : </span>{-1*this.props.data.total_cashback}</li>
                ) : null}
                {this.props.data.total_fee_pgc !== 0 ? (
                  <li><span>EMI fee : </span>{this.props.data.total_fee_pgc}</li>
                ) : null}
                {this.props.data.total_fee_gw !== 0 ||
                this.props.data.total_fee_gw_gst !== 0 ||
                this.props.data.total_fee_gw_vat !== 0 ? (
                  <li>
                    <span>Convenience fee : </span>
                    {this.props.data.total_fee_gw +
                      this.props.data.total_fee_gw_gst +
                      this.props.data.total_fee_gw_vat}
                  </li>
                ) : null}
                {this.props.data.total_fare !== 0 ? (
                  <li className="total">
                    <span>Total : </span>
                    {this.props.data.total_fare -
                      this.props.data.total_fee_agency_markup}
                  </li>
                ) : null}
              </ul>
            </div>
          )}
        </Popup>
      </>
    );
  }
}
export default HotelCustomerPriceBreakup;
