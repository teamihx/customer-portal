import React, { Component } from 'react'
import TripGenericService from '../../../services/trips/generic/TripGenericService';
import HtmlParser from 'react-html-parser';
class RateRule extends Component {
    constructor(props) {
      super(props);
      this.state = { 
          open: false,
          costPricingObj:{},
		  bookingPolices:''
		 
      
    };
	this.getRateRules();
}

 getRateRules=()=>{
        try{
             //console.log('getRateRules start...');
            if(this.state.bookingPolices === ''){
				TripGenericService.getBookingPolicies(this.props.tripId).then(response => {
                 if(response.data!==null && response.data!==""){
                 const walletCurResp = JSON.parse(response.data);
        //console.log('Get getRateRules response :'+JSON.stringify(walletCurResp));
        //console.log('Get getRateRules response data :'+JSON.stringify(walletCurResp.data));
                 if (walletCurResp !== undefined &&
                  walletCurResp !== "" &&
                  walletCurResp.status !== undefined &&
                  walletCurResp.status !== "" &&
                  walletCurResp.status === 200){
                    this.state.bookingPolices = walletCurResp.data;
                    this.forceUpdate();
						//console.log('Get getRateRules Status :'+response.status);
					}else{
                      //console.log('getRateRules NO data ..');
                 }
                }else{
                     //console.log('getRateRules NO data ..');
                }
               });
	
			}
             
         
         }catch(err){
             log.error('Exception occured in getRateRules function---'+err);
         }
         }

showContent=()=>{
	
    return HtmlParser(this.state.bookingPolices)
	//return HtmlParser("<p> <b>Your hotel has laid down some rules, and it's our job to let you know what they are.</b></p><ul><li> Standard check-in time is 2.00 PM and check-out time is 10.00 AM. Early check-in or late check-out is subject to availability and the hotel might charge you extra for it.</li><li>The meal plan for your booking is as per the plan available at the hotel.</li><li>The kids' tariff applies only to children up to 12 years of age. Children above 12 years are charged adult rates. Kids under 5 sharing a bed with their parents in the same room are normally not charged. Hotels will, however, levy a meal supplement charge for children.</li><li>The total price is inclusive of all applicable taxes. It does not include additional personal expenses like telephone charges, extra meals that aren't part of your meal plan, any hotel services you use (like laundry and room service) or tips.</li><li>The hotel will charge you directly for the incidental costs when you're checking out directly from you.</li><li>If you've booked a flight along with your hotel and your flight arrives earlier (or later) than the normal check-in time of your hotel, the hotel can be requested to allow you to check in earlier (or later). This is subject to the hotel's availability, though, and can't be guaranteed. Similarly, if your flight departs earlier or later than your hotel's normal check-out time, the hotel can be requested to adjust your check-out time accordingly. Again, this depends on the hotel's availability, and can't be guaranteed. You might be charged additionally for these adjusted check-in/check-out times, depending on each hotel's policy.</li><li> Some hotels enter guest names no earlier than 7 days before the check-in date. So if you call more than a week ahead and the hotel doesn't have your name on file,don't panic - your room's still reserved.</li><li>Any special requests are subject to availability.</li><p><b>Cancellation Policy</b></p><li>Cleartrip charges a separate service fee of Rs 250 for all cancellations.</li><li>The standard cancellation penalty is one night's retention, if you cancel within 48 hours before checking in. However, some tariffs may be non-refundable depending on the hotel's policy.</li><li>Cleartrip charges a separate service fee of Rs 250 for all cancellations.</li><li><b>If you cancel up to 24 hours before your check in date, you will be charged the entire cost of your stay.</b></li><p><b>No Show</b></p><li> If you don't show up at the hotel, you'll still be charged the entire amount. </li><p><b>Payment Policy</b></p><li>Your credit card will be charged for the total cost of the stay at time of purchase. We can't guarantee prices and room availability until your whole payment comes through. </li><li>The rates quoted are valid for Indian Residents & Foreigners with Indian work permits. </li><p><b>Modifications & Refunds</b></p><li>Cleartrip doesn't support changes and modifications to online bookings once they are made. </li><li>After a booking's done, no changes can be made to the name(s) on the reservation. </li><li>Don't call the hotel directly for the changes or cancellations to your reservation - their agents can't make changes or process refunds for these specially-negotiated rates.</li><li>There are no refunds for unused nights, including those resulting from delayed check-ins or early check-outs. </li><li> There are no refunds for early check-outs, and hotels aren't authorized to make exceptions to this policy. </li><li> We take at least 14 working days to process refunds for bookings cancelled online.</li><li>Your bank may debit its own separate charges from refunds made to your credit card or bank account. The time it takes for the refund to go through will also depend on your particular bank's policy.</li></ul>")
}
    render() {
    //console.log("this.props.data.hotel_bookings[0]--"+this.state.bookingPolices)  
      return (
        <>
        <div className="hotelPolicy">
          <>
        <h4>Cleartrip booking policy</h4>
            <strong className="d-b mb-5">Specific rules for Superior Double Room</strong>
            <div>{this.props.data}</div>
          </>
          {this.showContent()}
           
        </div>
         
        </>
      );
    }
  }
  export default RateRule;