import React, { Component } from "react";
import Popup from "reactjs-popup";
import { Link } from "react-router-dom";
import utility from "../../common/Utilities";
import { connect } from 'react-redux';

class HotelCustomerPriceBreakup extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      open: false,
      custPricingObj:{},
      currency:this.props.tripDetail.currency==="INR"?"Rs":this.props.tripDetail.currency
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  openModal() {
    this.setState({ open: true });
  }
  closeModal() {
    this.setState({ open: false });
  }
 
  loadCostpticeDetail = (RoomRates) => {
    
    if(this.props.tripDetail.hotel_bookings[0].is_gst_model==="Y" && (RoomRates[0].total_tax_sup_gst!==0 || RoomRates[0].cost_room_rate.total_ct_claim_comm_gst!==0)){
     this.is_gst_model=true
    }
    let total_base_fare = 0;
    let total_markup=0;
    let total_supfee_prop_srv_chg=0;
    let total_tax_sup=0;
    let total_tax_sup_gst=0;
    let total_tax=0;
    let total_tax_svc=0;
    let total_discount=0;
    let total_tax_sup_vat=0;
    let total_tax_prop_srv_chg_vat=0;
    let total_tax_municipal_fee=0;
    let total_tax_city_tax=0;
    let total_cashback=0;
    let total_fee_pgc=0;
    let total_fee_gw=0;
    let total_fee_gw_gst=0;
    let total_fee_gw_vat=0;
    let total_con_fee=0;
    let total_fare=0;
    let total_fee_agency_markup=0;
    for(let roomRate of RoomRates){
     // console.log("dfvdvd=="+roomRate.total_markup)
      total_base_fare+=roomRate.total_base_fare;
      total_fare+=roomRate.total_fare;
      total_fee_agency_markup+=roomRate.total_fee_agency_markup;
      total_fee_gw+=roomRate.total_fee_gw;
      total_fee_gw_vat+=roomRate.total_fee_gw_vat;
      total_fee_gw_gst+=roomRate.total_fee_gw_gst;
      total_markup+=roomRate.total_markup;
      total_supfee_prop_srv_chg+=roomRate.total_supfee_prop_srv_chg;
      
      if(this.is_gst_model){
        total_tax_sup_gst+=roomRate.total_tax_sup_gst;
      }else{
        total_tax_sup+=roomRate.total_tax_sup;
      }
      total_fee_pgc+=roomRate.total_fee_pgc;
      total_cashback+=roomRate.total_cashback;
      total_discount+=roomRate.total_discount;
      total_tax+=roomRate.total_tax;
      total_tax_svc+=roomRate.total_tax_svc;
      total_tax_sup_vat+=roomRate.total_tax_sup_vat;
      total_tax_prop_srv_chg_vat+=roomRate.total_tax_prop_srv_chg_vat;
      total_tax_municipal_fee+=roomRate.total_tax_municipal_fee;
      total_tax_city_tax+=roomRate.total_tax_city_tax;
    }

    total_con_fee=total_fee_gw+total_fee_gw_gst+total_fee_gw_vat;
    let other_tax=total_tax-total_tax_sup_gst-total_tax_sup-total_tax_svc;
    let vat=total_tax_sup_vat+total_tax_prop_srv_chg_vat;
    let total_city_muncipal_tax=total_tax_municipal_fee+total_tax_city_tax
    let total_cust_fare=total_fare-total_fee_agency_markup
    let duration=Math.abs(new Date(this.props.tripDetail.hotel_bookings[0].check_out_date)-new Date(this.props.tripDetail.hotel_bookings[0].check_in_date))
    let one_day=1000*60*60*24;
    let duration_in_day=Math.round(duration/one_day)
    let duration_people=this.props.tripDetail.hotel_bookings[0].room_count*duration_in_day;
    let cust_price =total_fare/duration_people;
    let custRoomRateObj = new Object();

     
     
     //let total_dis_supmkp=0;
    
    
    
    
//console.log("total price==="+duration)
custRoomRateObj = {
      total_base_fare: total_base_fare,
      
      total_supfee_prop_srv_chg: total_supfee_prop_srv_chg,
      vat: vat,
      total_fee_pgc:total_fee_pgc,
      total_markup:total_markup,
      total_fee_agency_markup:total_fee_agency_markup,
      total_tax_sup_gst:total_tax_sup_gst,
      total_tax_sup:total_tax_sup,
      other_tax: other_tax,
      total_discount: total_discount,
      total_city_muncipal_tax: total_city_muncipal_tax,
      total_fare: total_fare,
      duration_people:duration_people,
      total_cashback:total_cashback,
      total_con_fee:total_con_fee,
      cust_price:cust_price,
      total_cust_fare:total_cust_fare
    };
  
    this.state.custPricingObj = { ...custRoomRateObj };
    //console.log("custRoomRateObj===" + JSON.stringify(custRoomRateObj))
  };
  render() {
    this.loadCostpticeDetail(this.props.tripDetail.hotel_bookings[0].room_rates);
    return (
      <>
        <Popup
          trigger={
            <a className="linkRight" onClick={this.openModal}>
              {utility.PriceFormat(
                this.state.custPricingObj.cust_price,
                this.state.currency
              )}
            </a>
          }
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
          position="bottom right"
        >
          {close => (
            <div className="modal pop-grid text-left">
              <a className="close" onClick={close}>
                &times;
              </a>
              <ul>
                <li>
                <span>Room Rate :</span>
                  {utility.PriceFormat(this.state.custPricingObj.total_base_fare +
                    this.state.custPricingObj.total_markup,
                    this.state.currency
                  )}
                </li>
                {this.state.custPricingObj.total_supfee_prop_srv_chg !== 0 && (
                  <li key="srv_chg">
                    <span>Hotel Service Charge : </span>
                    {utility.PriceFormat(this.state.custPricingObj.total_supfee_prop_srv_chg,
                this.state.currency
              )}
                  </li>
                )}
                {!this.is_gst_model  ? (
                 this.state.custPricingObj.total_tax_sup !== 0 ? (
                    <li key="tax_sup">
                     <span> Hotel Taxes and Fees : </span> {utility.PriceFormat(this.state.custPricingObj.total_tax_sup,
                this.state.currency
              )}
                    </li>
                  ) : null
                ) : this.state.custPricingObj.total_tax_sup_gst !== 0 ? (
                  <li  key="sup_gst"><span>GST : </span>{utility.PriceFormat(this.state.custPricingObj.total_tax_sup_gst,
                    this.state.currency
                  )}</li>
                ) : null}
                {this.state.custPricingObj.other_tax !== 0 ? (
                  <li key="other_tax">
                   <span> Other tax :</span>
                    {utility.PriceFormat(this.state.custPricingObj.other_tax,
                this.state.currency
              )}
                  </li>
                ) : null}
                {this.state.custPricingObj.total_discount !== 0 ? (
                  <li key="discount"><span>Discount(-) </span>{utility.PriceFormat(-1*this.state.custPricingObj.total_discount,
                    this.state.currency
                  )}</li>
                ) : null}
                {this.state.custPricingObj.vat !== 0 ? (
                  <li key="vat">
                    <span>Vat : </span>
                    {utility.PriceFormat(this.state.custPricingObj.vat,
                this.state.currency
              )}
                  </li>
                ) : null}
                {this.state.custPricingObj.total_city_muncipal_tax!== 0  && (
                    <li key="muncipal_tax">
                      <span>City Tax/Municipal Fee : </span>
                      {utility.PriceFormat(his.state.custPricingObj.total_city_muncipal_tax,
                this.state.currency
              )}
                    </li>
                  )}
                {this.state.custPricingObj.total_cashback !== 0 ? (
                   <li key="cashback"><span>Cashback(-) : </span>{utility.PriceFormat(-1*this.state.custPricingObj.total_cashback,
                    this.state.currency
                  )}</li>
                ) : null}
                {this.state.custPricingObj.total_fee_pgc !== 0 ? (
                   <li key="fee_pgc"><span>EMI fee : </span>{utility.PriceFormat(this.state.custPricingObj.total_fee_pgc,
                    this.state.currency
                  )}</li>
                ) : null}
                {this.state.custPricingObj.total_con_fee !== 0  ? (
                   <li key="con_fee">
                    <span>Convenience fee : </span>
                    {utility.PriceFormat(this.state.custPricingObj.total_con_fee,
                this.state.currency
              )}
                  </li>
                ) : null}
                {this.state.custPricingObj.total_cust_fare !== 0 ? (
                  <li key="cust_fare" className="total">
                    <span>Total : </span>
                    {utility.PriceFormat(this.state.custPricingObj.total_cust_fare,
                this.state.currency
              ) }
                  </li>
                ) : null}
              </ul>
            </div>
          )}
        </Popup>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
      tripDetail: state.trpReducer.tripDetail,
     
  };
}
export default connect(mapStateToProps, null) (HotelCustomerPriceBreakup);
