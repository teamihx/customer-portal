import React, { Component } from 'react'
import { connect } from 'react-redux'
import Tooltip from 'react-tooltip-lite'
import DateUtils from '../../../services/commonUtils/DateUtils'
import UserFetchService from '../../../services/trips/user/UserFetchService'
import * as actions from '../../../store/actions/index'
import log from 'loglevel';


class AuditTrail extends Component {

    constructor(props) {
        super(props)
        //var data = require('../../../assets/json/data.json');
        this.state = {
            txns :this.props.tripDetail.txns,
            userDetailsresponse:'',
            userid:'',
            userIds:[],
            userMasterDatamap:'',
            flightInfo:(this.props.tripDetail.air_bookings[0]!==undefined && this.props.tripDetail.air_bookings[0]!=='')?this.props.tripDetail.air_bookings[0].flights:''
        }
        
    }

   componentDidMount(){

        if(this.state.txns!==undefined && this.state.txns!==''){
        this.setState({
                
            txns:this.state.txns.sort((a,b)=>{
                return new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
            }).reverse()
        })
        this.fetchUserMailids();
    }
        
        
        
    }

    builduserDataMap(){
       let userData=this.props.usermasterdetails;
       var items = {};
       if(this.state.userids!==undefined && this.state.userids!==''){
       for(var key in this.state.userids){
       

        if(userData!==undefined && userData!==''){
           const d=userData[this.state.userids[key]];
                let key1 = new Object();
                    if(d!==undefined && d!=='' &&
                    key!=='responsecode' && key!=='msg'){
                    key1.uname=d.username
                    if(d.personal_data!==undefined && d.personal_data!=='' && d.personal_data!==null){
                    key1.fname=d.personal_data.first_name
                    key1.lname=d.personal_data.last_name
                    }
                    items[this.state.userids[key]]=key1
            }

       } 
    }
 }

    this.state.userMasterDatamap=items;
    
    


    }
    getEmailid(userid){      

       let emailid='';
       if(this.state.userMasterDatamap!==undefined && this.state.userMasterDatamap!==''){
       let data=this.state.userMasterDatamap[userid];
       if(data!==undefined && data!==''){
        emailid=data.uname;
       }
    }
     
    return emailid;

    }

    

    fetchUserMailids(){

        try{

        let userids=[];        

        this.state.txns.map((tx, idx) => {

            userids.push(tx.user_id);
            
          
         });
         //console.log('userids----'+userids);

         
         userids=Array.from(new Set(userids));
         userids = userids.filter(function (el) {
            return el != null;
          });
         this.state.userids=userids;
         this.fetchUserDetails(userids)
         
        }catch(error){

            log.error('Exception occured in AuditTrail fetchUserMailids function---'+error);

        }



    }

    fetchUserDetails(usermailids){  
        //console.log('fetchUserDetails----'+usermailids);
 
 
        for(var id in usermailids){
 
         const data=this.props.usermasterdetails[usermailids[id]];
         if(data!==undefined){

         }else{
             this.state.userid=usermailids[id];
             this.fetchUserEmailService(usermailids[id]);
             
         }
         
 
        }
       
 
        
     }
      
 
      fetchUserEmailService(usermailids){
         //console.log('fetchUserEmailService----'+usermailids);
 
         let userids=[];  
         userids.push(usermailids);
 
         const usermailidsReq = {
             person_ids: userids,
             query:["username","personal_data"]
             
           };

           UserFetchService.fetchUserMailid(usermailidsReq).then(response => {            
             let userDetailsresponse = JSON.parse(response.data);
             this.state.userDetailsresponse=userDetailsresponse;
             this.props.usermasterdetails[this.state.userid]=this.state.userDetailsresponse[this.state.userid];
             this.props.updateUserDetails(this.state.userDetailsresponse);       
           }
           ).catch((error) => {
           log.error('exception occured in fetchUserMailid is---'+error);
          });     
           
           
     }

     buildingMiscValueForWhat(txntype,misc){        

         if(txntype!==undefined && txntype!==''){
            let finalwhatValue='';

            try{
            //Air Refund Shelved
            if(txntype===51 && (misc!==null && misc.includes('shelve_email_type'))){

                //console.log('misc original is '+misc); 
                let miscnew=misc.slice(misc.indexOf('{'), misc.indexOf('}')+1);               
                let miscnew2=miscnew.replace(/\\/g, ""); 
                let miscfinal=JSON.parse(miscnew2); 

               let shelveEmailType= this.props.txnDescription.SHELVE_EMAIL_TYPE1[miscfinal.shelve_email_type];
               let shelveReason=this.props.txnDescription.SHELVE_REASONS[miscfinal.shelve_reason];
               if(shelveEmailType===undefined){
                shelveEmailType='';

               }if(shelveReason===undefined){
                shelveReason='';
               }
               finalwhatValue= "Email Type : "+shelveEmailType+", Shelve Reason:"+shelveReason;

               return finalwhatValue;
            }
            //Auto-Refund Remarks Added (air)
            else if(txntype===52 && (misc!==null && misc.includes('partial_check_off'))){

                let miscnew=misc.slice(misc.indexOf('{'), misc.indexOf('}')+1);               
                let miscnew2=miscnew.replace(/\\/g, ""); 
                let miscfinal=JSON.parse(miscnew2);
                if(miscfinal.partial_check_off!==undefined && miscfinal.partial_check_off!==''){

                    finalwhatValue='Partially done';

                    return finalwhatValue;

                } 
            }
            //AlertOne Update
            else if(txntype===86 && (misc!==null && misc.includes('sms'))){

                let miscnew=misc.slice(misc.indexOf('{'), misc.indexOf('}')+1);               
                let miscnew2=miscnew.replace(/\\/g, ""); 
                let miscfinal=JSON.parse(miscnew2);
                if(miscfinal.sms!==undefined && miscfinal.sms!==''){

                    finalwhatValue=miscfinal.sms == true ? 'Sms Sent' : 'Sms Failed';
                    return finalwhatValue;

                } 

            }
            //"103": "Reconfirmation sms delivered",
            //"104": "Customer confirmed by sms",
            //"105": "Customer rejected by sms",
            //"106": "Customer confirmed by call",
            //"107": "Customer rejected by call",
            //"108": "Phone call made",
            //"109": "Reconfirmation sms sent",
            else if(txntype===103 || txntype===104 || txntype===105 || txntype===106 || txntype===107 
                || txntype===108 || txntype===109){
                    
                 if(misc!==undefined && misc!==''){
                 let miscnew=misc.slice(misc.indexOf('{'), misc.indexOf('}')+1); 
                 let miscnew2=miscnew.replace(/\\/g, ""); 
                
                 if(miscnew2!==undefined && miscnew2!==''){
                 let miscfinal='';                
                 miscfinal=JSON.parse(miscnew2);

                for(var key in miscfinal){

                 finalwhatValue=key+":"+miscfinal[key];

                    }
                }
                 return finalwhatValue;

                } 

            }else if(misc!==null && misc!==undefined && misc!=='' && misc.includes('send_eticket')){

                finalwhatValue='Ticket Number Updated';   
                return finalwhatValue;
            }

            //"10": "Flight(s) Cancelled",
            //"20": "Hotel Cancelled",
            else if(txntype===10 || txntype===20){
                let miscnew=misc.slice(misc.indexOf('{'), misc.lastIndexOf('}')+1); 
                let miscnew2=miscnew.replace(/\\/g, "");
                let finalwhatValue='';                
                if(miscnew2!==undefined && miscnew2!==''){

                let miscfinal='';
                
                miscfinal=JSON.parse(miscnew2);

                 if(miscnew2.includes('refund_type')){

                    let refundData=miscfinal['refund_type'];
                    if(refundData!==undefined && refundData!=='' && refundData.in_refund!==undefined 
                    && refundData.in_refund!=='' && refundData.in_refund==='WALLET'){

                        finalwhatValue='Refund to Wallet';

                    }else{
                        finalwhatValue='Refund to original payment mode';
                    }
                 }else{
                    finalwhatValue='Refund to original payment mode';
                 }

                    
                }
                return finalwhatValue;
 
            }
            //"124": "Automated Trip conversion attempted",
            //"125": "Automated Trip conversion successfull",
            //"128": "Automated Trip Conversion Failed",
            else if((txntype===124 || txntype===125 || txntype===128) && (misc!==null && misc.includes('segment_ids'))){
                
                let finalwhatValue='';
                let ddd=misc.replace(/\n|\r/g, "").replace(/ /g, "");
                let miscnew=ddd.slice(ddd.indexOf(':')+1, ddd.length); 
                let finalstr= miscnew.replace(/:-/g, ":")
                let segids= finalstr.split(':')[1].split('-'); 
                let finalsector=''

                 for(var segid in segids){
                     //console.log('segid is'+segids[segid])
                     let segid1=segids[segid];
                     let sector='';

                     for(let flt of this.state.flightInfo){
                        var dat = flt.segments.filter(segmnt => segmnt.id === Number(segid1));
                     if(dat != '' && dat != 'undefined'){
                        sector= dat[0].departure_airport+"-"+dat[0].arrival_airport;
                        if(finalsector===undefined || finalsector==''){
                        finalsector=finalsector+sector
                        }else{
                            finalsector=finalsector+","+sector

                        }
                      }
                    } 

                 }

                 finalwhatValue='Segments: '+finalsector;
                 return finalwhatValue;

            }
        }catch(error){

            log.error('Exception occured in AuditTrails Component buildingMiscValueForWhat function '+error)
            return finalwhatValue;
        } 

         }
     }

     buildingValueForWhy(txn){

        let txnData='';
        let finalWhyValue=''
        try{
        if(txn!==undefined && txn!=='' && txn.air_cancellations!==undefined && txn.air_cancellations!==''){
           txnData=  txn.air_cancellations.sort((a,b)=>{
            return parseInt(a.id) > parseInt(b.id)
        }).reverse()
        }

        if(txnData!==undefined && txnData!==''){

            finalWhyValue=txnData[0].user_cancel_reason;
            if(finalWhyValue===undefined || finalWhyValue==='' || finalWhyValue===null){
                finalWhyValue='NA';

            }

        }
    }catch(err){
        log.error('Exception occured in AuditTrails Component buildingValueForWhy function '+err)
        return finalWhyValue;

    }
        
         return finalWhyValue;

     }
     showTxn(txn){
         if(txn.status!==undefined && txn.status!==null && txn.status==='O' ){
             return 'Open Txn';
            
         }

     }

     tooolTipForEmail(mailId,created_at){

        let title='';
        try{
            let sentTo='sent to'

            let mail=(mailId!==null && mailId!==undefined) ? mailId: '';

            let date=(created_at!==null && created_at!==undefined) ? DateUtils.prettyDate(created_at,'MMM DD YYYY, hh:mm A'):'';

            title=sentTo+' '+mail+' '+'on '+ date;


         return title;

        }catch(err){

            log.error('Exception occured in AuditTrails Component tooolTipForEmail function '+err)
            return title;
        }



     }

     getTxnTypeValue(txn_id){
        
        let txn_value= this.props.txnTypeMasterData[txn_id];
        txn_value=(txn_value!==null && txn_value!==undefined && txn_value!=='')?txn_value:txn_id
        return txn_value;
     }

    //   componentWillReceiveProps(nextProps){
   
    //      this.setState({txns:nextProps.tripDetail.txns})
      
    //  }
    static getDerivedStateFromProps(nextProps, prevState){
        //console.log("componentWillReceiveProps+"  + nextProps.tripDetail===prevState.tripDetail)
        if(nextProps.tripDetail.txns!==prevState.txns){
         // console.log("componentWillReceiveProps+" + prevState.cpyPaymentLnk + prevState.sndEmail )
        
          return {txns:nextProps.tripDetail.txns}
        
       }else{
        return null;
       }
      
     }

    
    render() {
        this.builduserDataMap()
        return (
            <>
            <div>
            <div className="highInfo">Audit trail</div>
           
            <div className ='resTable'>
                    <table className="pax-tbl dataTable5">
                      <thead><tr>
                      <th width="30%">Txn Type</th>
                      <th width="30%">Details</th>
                      <th width="40%">Emails</th>
                    </tr></thead>

                    <tbody>

                    {this.state.txns.map((txn,idx)=>(

                    <tr key={idx}>

                    <td>
                    <span title={this.getTxnTypeValue(txn.txn_type)} className="d-b mb-10 w-300"><em className="d-ib w-60 t-color2 v-align">Type </em> <em className="w-70p d-ib">{this.getTxnTypeValue(txn.txn_type)}</em></span>
                    <span className="d-b mb-10"><em className="d-ib w-60 t-color2">Txn ID </em> {txn.id}</span>
                    <span className="d-b mb-10"><em className="d-ib w-60 t-color2 v-align">IP </em> <em className="d-ib"><em className="d-b">{txn.source_id}</em><em className="secondary-cr">{this.showTxn(txn)}</em></em></span>


                    </td>
                     

                   <td>                    
                   <span className="d-b mb-10"><em className="d-ib w-60 t-color1">When? </em> : {DateUtils.prettyDate(txn.created_at,'MMM DD YYYY, hh:mm A')}
                   </span>
                   {(txn.user_id !==undefined && txn.user_id !=='') ?
                    <span className="d-b mb-10"><em className="d-ib w-60 t-color1">Who?  </em> <em className="w-70p d-ib ellipsis" title={this.getEmailid(txn.user_id)}>: {this.getEmailid(txn.user_id)}</em> </span>
                     :''
                    }
                    {(this.buildingMiscValueForWhat(txn.txn_type,txn.misc) !==undefined && this.buildingMiscValueForWhat(txn.txn_type,txn.misc) !=='') ?
                     <span className="d-b mb-10"><em className="d-ib w-60 t-color1">What? </em> <em className="w-70p d-ib ellipsis" title={this.buildingMiscValueForWhat(txn.txn_type,txn.misc)}>: {this.buildingMiscValueForWhat(txn.txn_type,txn.misc)}</em></span>
                    :''
                    }
                    {(txn.txn_type !==undefined && txn.txn_type !=='' && txn.txn_type===10) ?
                    <span className="d-b mb-10"><em className="d-ib w-60 t-color1">Why? </em> : {this.buildingValueForWhy(txn)}</span>
                    :''
                    }
                    {(txn.source_type !=='' && txn.source_type !==0) ?
                    <span className="d-b mb-10"><em className="d-ib w-60 t-color1">Source </em> : {txn.source_type}</span>
                    :''
                    }
                    </td>

                    <td>
                        <ul className="lstStlyCrl">
                    {(txn.emails !==null && txn.emails!==undefined && txn.emails.length > 0) &&( txn.emails.map((item, index) => {
                    return(
                    <Tooltip key={index} content={(
                        this.tooolTipForEmail(item.address,item.created_at)
                      )}
                      direction="bottom"
                      tagName="span"
                      className="target">
                       <li className="t-color3">{item.subject}</li> 
                      </Tooltip>)
      
                    }))}
                    </ul>
{/*                     </ul>
                    {(txn.emails !==null && txn.emails!==undefined && txn.emails.length > 0) &&( txn.emails.map((item, index) => {
                    return <div className="t-color3" key={index} title={this.tooolTipForEmail(item.address,item.created_at)}>
                    {item.subject}</div>;
                    }))} */}
                   
                    </td> 



                      </tr>
                    ))}


                    </tbody>


                    </table>
                    </div>
   

            </div>
           
</>

        )
    }


}

const mapDispatchToProps = dispatch => {
    //console.log("mapDispatchToProps");
    return {
      
        
        updateUserDetails: (req) => dispatch(actions.updateUserDetails(req)),
        
    }
  }
const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,
        txnTypeMasterData: state.trpReducer.txnTypeMasterData,
        usermasterdetails: state.trpReducer.usermasterdetails,
        txnDescription: state.trpReducer.txnDescription,
        
        
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(AuditTrail);
