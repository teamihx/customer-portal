import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import DateUtils from "../../../services/commonUtils/DateUtils";
import ActivityRateRule from "../activities/ActivityRateRule";
import utility from "../../common/Utilities";
import log from "loglevel";
import * as actions from "../../../store/actions/index";
import Icon from "Components/Icon";
export const TRIPS_ROLES = 'tripsRoles';
class ActivityTripDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tripDtls: this.props.tripDetail,
      activitiesList: [],
      insuranceAvailable: false
    };
    let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
    this.state.localtripRoles=tripsObj.localtripsRoles;
    this.constructActivityDetails();
  }

  constructActivityDetails = () => {
    this.state.activitiesList = [];
    try {
      for (let activitie of this.props.tripDetail.activity_bookings[0]
        .activities) {
        let activities = new Object();
        activities = activitie;
        let paxPrice = JSON.parse(activities.pricing_details);
        console.log(paxPrice);
        let adtPrice = 0;
        let chdPrice = 0;
        let totTime = Math.abs(
          new Date(activities.start_date_time) -
            new Date(activities.end_date_time)
        );
        let durations = DateUtils.msToTime(totTime);
        activities.duration = durations;

        if (paxPrice!==null && paxPrice.ADT !== null && paxPrice.ADT !== undefined) {
          adtPrice = paxPrice.ADT;
        }

        if (paxPrice!==null && paxPrice.CHD !== null && paxPrice.CHD !== undefined) {
          chdPrice = paxPrice.CHD;
        }
        activities.adtPrice = adtPrice;
        activities.chdPrice = chdPrice;

        let activity_booking_info = this.props.tripDetail.activity_bookings[0].activity_booking_infos.filter(
          function(activity_booking_infos) {
            return activity_booking_infos.activity_id === activitie.id;
          }
        );

        let pax_info = this.props.tripDetail.activity_bookings[0].pax_infos.filter(
          function(pax_infos) {
            return activity_booking_info[0].pax_info_id === pax_infos.id;
          }
        );

        let activity_rate = this.props.tripDetail.activity_bookings[0].activity_rates.filter(
          function(activity_rates) {
            return (
              activity_booking_info[0].activity_rate_id === activity_rates.id
            );
          }
        );
        activities.activity_booking_infos = activity_booking_info[0];
        activities.pax_infos = pax_info[0];
        activities.activity_rates = activity_rate[0];
        activities.travellers = this.props.tripDetail.travellers;
        activities.currency = this.props.tripDetail.currency;
        if (this.props.tripDetail.currency === "INR") {
          activities.currency = "Rs";
        }

        this.state.activitiesList.push(activities);
      }
    } catch (err) {
      log.error(
        "Exception occured in constructActivityDetails function---" + err
      );
    }
  };

  getTravellers = travellers => {
    let pricingObject = new Object();
    let adultCount = 0;
    let childCount = 0;
    let infantCount = 0;

    travellers = travellers.trimRight();
    let travellerString = travellers.substring(
      travellers.trimRight().indexOf("|") + 1,
      travellers.length
    );
    let ttravellerArr = travellerString.trimLeft().split("\n");

    for (let travel of ttravellerArr) {
      if (travel.includes("INF")) {
        infantCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
      if (travel.includes("ADT")) {
        adultCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
      if (travel.includes("CHD")) {
        childCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
      }
    }
    let adtTravel =
      adultCount > 0
        ? adultCount + (adultCount == 1 ? " Adult  " : " Adults  ")
        : "";
    let childTravel =
      childCount > 0
        ? childCount + (childCount == 1 ? " Child  " : " Children  ")
        : "";
    let infTravel =
      infantCount > 0
        ? infantCount + (infantCount == 1 ? " Infant  " : " ,Infant  ")
        : "";
    return adtTravel + childTravel + infTravel;
  };

  showRateRule=(ele,e)=>{
    this.setState({showRateRule:!this.state.showRateRule},()=>console.log("showRateRule"+this.state.showRateRule))
 }

  
  render() {    
    

    return this.state.activitiesList.map((ele, i) => {
      return (
        <React.Fragment key={ele.id}>
          <div className="heading dis-flx-btw mb-5">
            <h4 className="in-tabTTl-sub">
            <Icon
                className="noticicon mt--5"
                color="#36c"
                size={18}
                icon="Activities"
              /> In {ele.city}
             
            </h4>
            <div className="trip-sub">
            <Link to="#" className="linkRight">
                {ele.name}
              </Link>
              
              {this.state.localtripRoles.LOCAL_RATERULE_ROLE_ENABLE &&
              <a id="rateRule" className="linkRight" onClick= {(e)=>this.showRateRule(ele,e)}>Rate Rule</a>
               }
              
            </div>
          
          </div>
          {this.state.showRateRule && <ActivityRateRule/>}
          <div className="resTable">
            <table className="dataTable3 mb-30">
              <thead>
                <tr>
                  <th width="25%">Start Date</th>
                  <th width="25%">end date</th>
                  <th width="25%">Duration</th>
                </tr>
              </thead>
              <tbody>              
                  <tr>
                    <td>
                      {DateUtils.prettyDate(
                        ele.start_date_time,
                        "hh:mm A ddd, MMM DD, YYYY "
                      )}
                    </td>
                    <td>
                      {DateUtils.prettyDate(
                        ele.end_date_time,
                        "hh:mm A ddd, MMM DD, YYYY "
                      )}
                    </td>
                    <td>{ele.duration}</td>
                  </tr>
              </tbody>
            </table>
          </div>
          <div className="heading dis-flx-btw mb-5">
            {ele.max_pax_per_unit !== null &&
              ele.unit_type !== null &&
              ele.no_of_units !== null && (
                <h4 className="in-tabTTl-sub">
                  <Icon
                      className="noticicon mt--5"
                      color="#36c"
                      size={18}
                      icon="Activities"
                    /> Booking details -{" "}
                  <span className="t-color3 font-14">
                    {ele.no_of_units} {ele.unit_type} - {ele.max_pax_per_unit}{" "}
                    Persons/{ele.unit_type}{" "}
                  </span>
                </h4>
              )}

            {ele.max_pax_per_unit === null && (
              <h4 className="in-tabTTl-sub">
              <Icon
                  className="noticicon mt--5"
                  color="#36c"
                  size={18}
                  icon="Activities"
                />
                {" "}
                Booking details -{" "}
                <span className="t-color3 font-14">
                  {this.getTravellers(ele.travellers)}{" "}
                </span>
              </h4>
            )}

            <div className="trip-sub">{ele.name}</div>
            </div>
            <div className="resTable">
              <table className="dataTable3">
                <thead>
                  <tr>
                    <th width="30%">Variant</th>
                    <th width="15%">Voucher #</th>
                    <th width="20%">Cost price</th>
                    <th width="20%">Total paid</th>
                    <th width="15%">Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className={
                    ele.activity_booking_infos.booking_status ===
                    "Q" ||
                    ele.activity_booking_infos.booking_status ===
                    "K"
                    ? "des cancel"
                    : null
                } key={ele.id}>
                    <td>
                      <span>{ele.variant_name} </span>
                    </td>
                    <td>
                      <span>{ele.activity_booking_infos.voucher_number}</span>
                    </td>
                    <td>
                      <span>
                        {utility.PriceFormat(
                          parseFloat(ele.activity_rates.total_base_fare, 10) +
                            parseFloat(ele.activity_rates.total_markup, 10),
                          ""
                        )}
                      </span>
                    </td>
                    <td>
                      <span>
                        {utility.PriceFormat(
                          parseFloat(ele.activity_rates.total_fare, 10),
                          ""
                        )}
                      </span>
                    </td>
                    <td>
                      <span className="n-strk">
                        {
                          this.props.bkngSatatusMaster[
                            ele.activity_booking_infos.booking_status
                          ]
                        }
                      </span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          
            <div className="heading dis-flx-btw mb-5">
          <h4 className="in-tabTTl-sub">  
              Supplier pricing details for -{" "}
              <span className="t-color3 font-14">
                {this.getTravellers(ele.travellers)}
              </span>
            </h4>
            <div className="trip-sub">{ele.name}</div>
                        </div>
            <div className="resTable">
              <table className="dataTable3">
                <thead>
                  <tr>
                    
              {ele.max_pax_per_unit !== null &&
              ele.unit_type !== null &&
              ele.no_of_units !== null && (<th width="30%">Unit Price</th>)}
              {ele.max_pax_per_unit === null && (<th width="30%">Price (adult & child)</th>)}
                    <th width="15%">Commission</th>
                    <th width="20%">Net payable</th>
                    <th width="20%">Payment gateway fee</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    {this.props.tripDetail.activity_bookings[0].adults !==
                      null &&
                      this.props.tripDetail.activity_bookings[0].adults !== 0 &&
                      this.props.tripDetail.activity_bookings[0].children !==
                        null &&
                      this.props.tripDetail.activity_bookings[0].children !==
                        0 && ele.max_pax_per_unit === null && (
                        <td>
                          <span>
                            {this.props.tripDetail.activity_bookings[0].adults}{" "}
                            ADT x {ele.adtPrice} +{" "}
                            {
                              this.props.tripDetail.activity_bookings[0]
                                .children
                            }{" "}
                            CHD x {ele.chdPrice} ={" "}
                            {utility.PriceFormat(
                              parseFloat(
                                ele.activity_rates.total_base_fare +
                                  ele.activity_rates.total_markup,
                                10
                              ),
                              ""
                            )}
                          </span>
                        </td>
                      )}

                      {ele.max_pax_per_unit !== null &&
                        ele.unit_type !== null &&
                       ele.no_of_units !== null && (
                        <td>
                          <span>{ele.no_of_units}units{" "}={" "}
                            {utility.PriceFormat(
                              parseFloat(
                                ele.activity_rates.total_base_fare +
                                  ele.activity_rates.total_markup,
                                10
                              ),
                              ""
                            )}
                          </span>
                        </td>
                      )}

                    {(this.props.tripDetail.activity_bookings[0].adults ===
                      null ||
                      this.props.tripDetail.activity_bookings[0].adults ===
                        0) &&
                      this.props.tripDetail.activity_bookings[0].children !==
                        null &&
                        this.props.tripDetail.activity_bookings[0].children !==
                          0 &&
                        this.props.tripDetail.activity_bookings[0].children !==
                          undefined && (
                        <td>
                          <span>
                            {
                              this.props.tripDetail.activity_bookings[0]
                                .children
                            }{" "}
                            CHD x {ele.chdPrice} ={" "}
                            {utility.PriceFormat(
                              parseFloat(
                                ele.activity_rates.total_base_fare +
                                  ele.activity_rates.total_markup,
                                10
                              ),
                              ""
                            )}
                          </span>
                        </td>
                      )}

                    <td>
                      <span>
                        {utility.PriceFormat(
                          parseFloat(ele.activity_rates.total_markup, 10),
                          ""
                        )}
                      </span>
                    </td>
                    <td>
                      <span>
                                                    
                        {utility.PriceFormat(
                          parseFloat(
                            (ele.activity_rates.total_base_fare +
                              ele.activity_rates.total_dis_gw+ele.activity_rates.total_dis_ct_sup_igst),
                            10
                          ),
                          ""
                        )}
                      </span>
                    </td>
                    <td>
                      <span>
                        {utility.PriceFormat(
                          parseFloat(ele.activity_rates.total_fee_gw, 10),
                          ""
                        )}
                      </span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
        
          <div>
          <h4 className="in-tabTTl-sub">
            Inclusions & extras</h4>
            <div className="resTable">
              {ele.name} - Inclusions {ele.inclusions}
            </div>
          </div>
        </React.Fragment>
      );
    });
  }
}
const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,

    InsuranceStatusMasterData: state.trpReducer.InsuranceStatusMasterData,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster,
    InsuranceMasterData: state.trpReducer.InsuranceMasterData
  };
};
const mapDispatchToProps = dispatch => {
  console.log("mapDispatchToProps");
  return {
    updateUserDetails: req => dispatch(actions.updateUserDetails(req))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityTripDetails);
