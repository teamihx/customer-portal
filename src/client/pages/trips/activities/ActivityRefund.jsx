import React, { Component } from 'react'
import { connect } from 'react-redux';
import NumberUtils from '../../../services/commonUtils/NumberUtils.js';
import utility from "../../common/Utilities";
import log from 'loglevel';

class ActivityRefund extends Component {

    constructor(props) {
        super(props)
        this.state = {
            responseData1: this.props.tripDetail,
			refundInfoList:[],
			refundTxn : '0',
			totalRevarsal :'0',
			totalSup :'0',
			totalConvenience:'0',
			totalCt :'0',
			totalCshBack: '0',
			totalSTax: '0',
			totalCTSTax: '0',			
			totalMiscCharge: '0',
			totalOtherCharge: '0',
			totalWtCbCharge: '0',
			totalCTFundedDis:'0',
			totalAgencyCommission:'0',
			lossadjustment:'0',			
			totalRefund: '0',
			currency:''
          
		}
 		this.calculateTotalRefund = this.calculateTotalRefund.bind(this);
	
    }

   UNSAFE_componentWillMount(){
	let txns = this.state.responseData1.txns.filter( function (txns) {
      return txns.activity_refund_records.length > 0
    });

	
	this.state.refundTxn = txns;
	this.calculateTotalRefund();
	}
	
	calculateTotalRefund() {		
	this.state.refundInfoList =[];
	try{
		for(let refundTxns of this.state.refundTxn){
		for(let act_refund_records of refundTxns.activity_refund_records){
		let act_refund_rcd = new Object(); 
		act_refund_rcd = act_refund_records;			
		
		/*let act_cancellationsdata = refundTxns.activity_cancellations.filter( function (act_cancellations) {
					      return act_cancellations.activity_refund_record_id === act_refund_records.id
					    });
		 let actbookingInfoId = act_cancellationsdata[0].activity_booking_info_id;
		
		let actbookInfos = this.state.responseData1.activity_bookings[0].activity_booking_infos.filter( function (actbookInfo) {
					      return actbookInfo.activity_booking_id === actbookingInfoId
						});
						
		let actPaxInfos = this.state.responseData1.activity_bookings[0].pax_infos.filter( function (actpaxbookInfo) {
			return actpaxbookInfo.air_booking_id === actbookingInfoId
		});

		 let actInfos = this.state.responseData1.activity_bookings[0].activities.filter( function (actInfo) {
			return actInfo.activity_booking_id === actbookingInfoId
		});  */

		let actInfos = this.state.responseData1.activity_bookings[0].activities;
		
		act_refund_rcd.activity_name=actInfos[0].name;
		act_refund_rcd.unit_type=actInfos[0].unit_type;
		act_refund_rcd.max_pax_per_unit=actInfos[0].max_pax_per_unit;
		act_refund_rcd.no_of_units=actInfos[0].no_of_units;
		act_refund_rcd.unit_type_str='';
		act_refund_rcd.traveller_details_str='';
		if(actInfos[0].no_of_units!==null){
			act_refund_rcd.unit_type_str=actInfos[0].no_of_units+' '+actInfos[0].unit_type +'-'+actInfos[0].max_pax_per_unit+'Persons/'+actInfos[0].unit_type;
                    
		}
		if(actInfos[0].no_of_units===null){
		 let traveller_data=this.getTravellers(this.state.responseData1.travellers);
		 act_refund_rcd.traveller_details_str=traveller_data;
		}
		this.state.currency = this.state.responseData1.currency;
        if (this.state.responseData1.currency === "INR") {
			this.state.currency = "Rs.";
        }
		
		act_refund_rcd.travellers=this.state.responseData1.travellers;
		let paymode = this.state.responseData1.payments_service_data[0].payment_type;
		let tnxNumb = '';
		let paymentmode = this.props.paymentTypeMaster[this.state.responseData1.payments_service_data[0].payment_type];
		if(paymode ==='CC'){
			 tnxNumb = this.state.responseData1.payments_service_data[0].payment_card_details[0].card_number;
		}	
		act_refund_rcd.paymentmode =paymentmode;
		act_refund_rcd.tnxNumb =tnxNumb;			
		this.state.refundInfoList.push(act_refund_rcd);
		this.state.totalRevarsal = Number(this.state.totalRevarsal) + 
		act_refund_records.total_rev!==null?(Number(act_refund_records.total_rev)):0;		
		this.state.totalSup = Number(this.state.totalSup) + 
		act_refund_records.total_sup_charge!==null?(Number(act_refund_records.total_sup_charge)):0;
		this.state.totalConvenience = Number(this.state.totalConvenience) + 
		act_refund_records.total_con_charge!==null?(Number(act_refund_records.total_con_charge)):0;				
		this.state.totalCt = Number(this.state.totalCt) + 
		act_refund_records.total_ct_charge!==null?(Number(act_refund_records.total_ct_charge)):0;
		this.state.totalCshBack = Number(this.state.totalCshBack) + 
		act_refund_records.total_cb!==null?(Number(act_refund_records.total_cb)):0;
		this.state.totalDiscount = Number(this.state.totalDiscount) + 
		act_refund_records.total_dis!==null?(Number(act_refund_records.total_dis)):0;
		this.state.totalSTax = Number(this.state.totalSTax) + 
		act_refund_records.total_stx_charge!==null?(Number(act_refund_records.total_stx_charge)):0;
		this.state.totalCTSTax = Number(this.state.totalCTSTax) + 
		act_refund_records.total_ct_stx_charge!==null?(Number(act_refund_records.total_ct_stx_charge)):0;			
		this.state.totalMiscCharge = Number(this.state.totalMiscCharge) + 
		act_refund_records.total_misc_charge!==null?(Number(act_refund_records.total_mis_ccharge)):0;
		this.state.totalOtherCharge = Number(this.state.totalOtherCharge) + 
		act_refund_records.total_oth_charge!==null?(Number(act_refund_records.total_oth_charge)):0;		
		this.state.totalWtCbCharge = Number(this.state.totalWtCbCharge) + 
		act_refund_records.total_wt_cb_charge!==null?(Number(act_refund_records.total_wt_cb_charge)):0;
		this.state.totalCTFundedDis = Number(this.state.totalCTFundedDis) + 
		act_refund_records.total_ct_fund!==null?(Number(act_refund_records.total_ct_fund)):0;
		this.state.totalAgencyCommission = Number(this.state.totalAgencyCommission);
		this.state.lossadjustment = Number(this.state.lossadjustment);		
		this.state.totalRefund = Number(this.state.totalRefund) + 
		act_refund_records.refund_amount!==null?(Number(act_refund_records.refund_amount)):0;
			
		}
		
	}
		
	}catch(err){
    log.error('Exception occured in Activity Refund component calculateTotalRefund function---'+err);
  }
	
	  }
	  getTravellers = travellers => {
		let adultCount = 0;
		let childCount = 0;
		let infantCount = 0;
		let adtTravel='';
		let childTravel='';
		let infTravel='';

	
		try{
		travellers = travellers.trimRight();
		let travellerString = travellers.substring(
		  travellers.trimRight().indexOf("|") + 1,
		  travellers.length
		);
		let ttravellerArr = travellerString.trimLeft().split("\n");
	
		for (let travel of ttravellerArr) {
		  if (travel.includes("INF")) {
			infantCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
		  }
		  if (travel.includes("ADT")) {
			adultCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
		  }
		  if (travel.includes("CHD")) {
			childCount = travel.substring(travel.indexOf(" ") + 1, travel.length);
		  }
		}
		adtTravel =
		  adultCount > 0
			? adultCount + (adultCount == 1 ? " Adult  " : " Adults  ")
			: "";
		childTravel =
		  childCount > 0
			? childCount + (childCount == 1 ? " Child  " : " Children  ")
			: "";
		infTravel =
		  infantCount > 0
			? infantCount + (infantCount == 1 ? " Infant  " : " ,Infant  ")
			: "";
		}catch(err){
			log.error('Exception occured in Activity Refund component getTravellers function---'+err);
		  }
		return adtTravel + childTravel + infTravel;
	  };
    
    render() {
        
        return (
            <>
            <div>
            <h4 className="in-tabTTl-sub mb-10">Refund Computation details</h4>			
			<div className="overflow-x-auto">
			<table className="dataTblPaddingR pax-tbl dataTable5 scrlTable">
                <thead><tr>
				<th width="7%">Activity Name</th>				
                <th width="7%">Reversal  (+)</th>
                <th width="7%">Supplier (-)</th>
				<th width="7%">Convenience Charge (–)</th>				
                <th width="6%">Cleartrip (-)</th>
                <th width="7%">Cashback  (-)</th>				
                <th width="6%">S.Tax (-)</th>
                <th width="6%">CT-S.Tax (-)</th>
				<th width="6%">Misc  (-)</th>
				<th width="6%">Other (-)</th>
				<th width="10%">Wallet Cashback (-)</th>
				<th width="8%">CT Funded Dis. (–)</th>
				<th width="12%">Agency Comm.(–)</th>
				<th width="12%">Loss Adjustment</th>
				<th width="6%">Refund</th>                   
                </tr></thead>
                <tbody>

				{this.state.refundInfoList.map((refundInfos, index) => {					
					
			  return(
				<>
				
				     <tr key={index}>
						{(refundInfos.unit_type_str!==null && refundInfos.unit_type_str!=='') &&(
					  <td><span>({(refundInfos.unit_type_str!==null &&
					  refundInfos.unit_type_str!==undefined && refundInfos.unit_type_str!=='') && ((refundInfos.activity_name)+' '+(refundInfos.unit_type_str))})</span></td>
					  )}
					 {(refundInfos.unit_type_str===null ||
					  refundInfos.unit_type_str===undefined || refundInfos.unit_type_str==='') && (
				     <td><span>({(refundInfos.unit_type_str===null ||
					  refundInfos.unit_type_str===undefined || refundInfos.unit_type_str==='') && ((refundInfos.activity_name)+' '+(refundInfos.traveller_details_str))})</span></td>
				 	  )}
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_rev, 10),'')}</span></td>
                     <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_sup_charge, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_con_charge, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_ct_charge, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_cb, 10),'')}</span></td>					 
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_stx_charge, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_ct_stx_charge, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_mis_ccharge, 10),'')}</span></td>					 
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_oth_charge, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_wt_cb_charge, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.total_ct_fund, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalAgencyCommission, 10),'')}</span></td>
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.lossadjustment, 10),'')}</span></td>					 
					 <td><span>{this.state.currency}{utility.PriceFormat(parseFloat(refundInfos.refund_amount, 10),'')}</span></td>
				
                  </tr>
				<tr >
				<td colSpan="15"><span className="t-color1">
				Refund Breakup : Refund to {refundInfos.paymentmode}({refundInfos.tnxNumb}) : {NumberUtils.numberFormat(refundInfos.refund_amount)} 
				 </span>
				</td>
				</tr>

				</>
			);
			
			})}
			<tr>
				<td><strong className="font-14">Total</strong></td>                
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalRevarsal, 10),'')}</td>
                <td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalSup, 10),'')}</td>
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalConvenience, 10),'')}</td>				
                <td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalCt, 10),'')}</td>
                <td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalCshBack, 10),'')}</td>
                <td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalSTax, 10),'')}</td>
                <td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalCTSTax, 10),'')}</td>
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalMiscCharge, 10),'')}</td>				
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalOtherCharge, 10),'')}</td>
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalWtCbCharge, 10),'')}</td>
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalCTFundedDis, 10),'')}</td>
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalAgencyCommission, 10),'')}</td>
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.lossadjustment, 10),'')}</td>
				<td>{this.state.currency}{utility.PriceFormat(parseFloat(this.state.totalRefund, 10),'')}</td>
			</tr>
				
                  </tbody>
                </table>
				</div>
            </div>
           
</>

        )
    }


}

const mapStateToProps = state => {
    return {
		tripDetail: state.trpReducer.tripDetail,
		paymentTypeMaster:state.trpReducer.paymentTypeMaster, 
     };
}

export default connect(mapStateToProps, null)(ActivityRefund);



