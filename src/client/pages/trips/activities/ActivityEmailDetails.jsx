import React, { Component } from "react";
import { connect } from "react-redux";
import ActivityService from "../../../services/trips/activity/ActivityService";
import Notification from "Components/Notification.jsx";
import Button from "Components/buttons/button.jsx";
import log from "loglevel";
export const contextPath = process.env.domainpath;
import {
  isArrayNotEmpty,
  isNotNull,
  isSuccessResponse
} from "../../common/validators.js";

class ActivityEmailDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseData1: this.props.tripDetail,
      email: this.props.tripDetail.contact_detail.email,
      showSucessMessage: false,
      showFailMessage: false,
      showInputMessage: false,
      errorMsgList: [],
      iCalendar: true,
      mailSentmsg: "",
      mailnotSentmsg: "",
      sendTrip: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.getMessage = this.getMessage.bind(this);
    this.validateEmails = this.validateEmails.bind(this);
    this.handleChangeIcalender = this.handleChangeIcalender.bind(this);
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.id === "icalInput") {
      if (this.state.iCalendar) {
        this.state.iCalendar = false;
      } else {
        this.state.iCalendar = true;
      }
    }
  };

  handleChangeIcalender = e => {
    if (this.state.iCalendar) {
      this.state.iCalendar = false;
    } else {
      this.state.iCalendar = true;
    }
  };

  //below function service call for Emailtripdetails
  sendActivityDetailsEmail = () => {
    try {
      this.setState({ sendTrip: true });
      if (this.validateEmails()) {
        const mailReq = {
          trip_id: this.props.tripDetail.trip_ref,
          ical: this.state.iCalendar,
          mail_id: this.state.email
        };
        //console.log("mail Request is--" + JSON.stringify(mailReq));
        ActivityService.sendEmailDetails(mailReq).then(res => {
          //console.log("got response in UI" + JSON.stringify(res));
          
          if (
            isNotNull(res) &&
            isSuccessResponse(res.status) &&
            isNotNull(res.data)
          ) {
            //console.log("entered--if block");
            // this.setState({ mailSentmsg: "mail sent successfully" });
            //this.setState({ sendTrip: false });
            this.state.mailSentmsg = "mail sent successfully";
            this.state.sendTrip = false;
            this.forceUpdate();
          } else {
            //console.log("entered--else block");
            this.setState({ mailSentmsg: "mail not sent " });
            this.setState({ sendTrip: false });
            this.forceUpdate();
          }
        });
      }
    } catch (err) {
      this.setState({ sendTrip: false });
    }
  };

  //below function Hq redirection for Emailtripdetails
  sendActivityEmail = () => {
    try {
      this.setState({ sendTrip: true });
      if (this.validateEmails()) {
        let email = new Object();
        email.to = this.state.email;
        email.tripRefNumber = this.state.responseData1.trip_ref;
        email.type = "Activity_Email";
        email.ical = this.state.iCalendar;
        ActivityService.sendEmails(email).then(response => {
          if (
            response !== undefined &&
            response !== null &&
            response.data !== undefined &&
            response.data !== null &&
            response.data.status !== undefined &&
            response.data.status !== null &&
            response.data.status == 200
          ) {
            this.setState({
              showSucessMessage: true,
              sendTrip: false,
              showAlert: true
            });
          } else {
            this.setState({
              showFailMessage: true,
              sendTrip: false,
              showAlert: true
            });
          }
        });
      }
    } catch (err) {
      log.error(
        "Exception occured in Activity Email details Component sendActivityEmail function---" +
          err
      );
      this.setState({ sendTrip: false });
    }
  };

  getMessage(type) {
    let msg = "";
    if (type === "success") {
      msg =
        "We have sent the itinerary details in an email to " + this.state.email;
    } else if (type === "error") {
      msg =
        "We are not able to send a mail with itinerary details. You can try again at some other time ";
    }
    return msg;
  }

  validateEmails() {
    let isValid = true;
    if (this.state.email === null || this.state.email.trim() === "") {
      this.state.errorMsgList.push("Email must be provided");
      isValid = false;
      this.setState({
        showInputMessage: true,
        sendTrip: false,
        showAlert: true
      });
    }
    if (this.state.email != null && this.state.email.trim() !== "") {
      const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
      isValid = expression.test(String(this.state.email).toLowerCase());
      if (!isValid) {
        this.state.errorMsgList.push("Enter a valid email address");
        this.setState({
          showInputMessage: true,
          sendTrip: false,
          showAlert: true
        });
      }
    }

    return isValid;
  }
  toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  render() {
    let viewType, messageText;
    if (this.state.showInputMessage) {
      viewType = "error";
      messageText = this.state.errorMsgList;
    } else if (this.state.showFailMessage) {
      viewType = "error";
      messageText = this.getMessage("error");
    } else if (this.state.showSucessMessage) {
      viewType = "success";
      messageText = this.getMessage("success");
    }

    return (
      <>
        <div>
          {viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}

          <div className="form-group">
            <div className="row">
              <div className="col-12">
                <input
                  type="text"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />

                <div className="dis-flx-btw-ned w-100p">
                  <div>
                    <input
                      id="icalInput"
                      type="checkbox"
                      className="customCheckbox"
                      defaultChecked={this.state.iCalendar}
                      value={this.state.iCalendar}
                      onChange={this.handleChange}
                    />
                    <label htmlFor="icalInput">Send iCalendar</label>
                  </div>
                  <div>
                    <Button
                      size="xs"
                      viewType="primary"
                      onClickBtn={this.sendActivityEmail}
                      loading={this.state.sendTrip}
                      btnTitle="Send"
                    ></Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airPortMaster: state.trpReducer.airPortMaster,
    airLineMaster: state.trpReducer.airLineMaster
  };
};

export default connect(mapStateToProps, null)(ActivityEmailDetails);
