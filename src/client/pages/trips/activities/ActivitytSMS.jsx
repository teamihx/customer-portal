import React, { Component } from "react";
import Icon from "Components/Icon";
import { connect } from "react-redux";
import ActivityService from "../../../services/trips/activity/ActivityService.js";
import log from "loglevel";
import DateUtils from "Services/commonUtils/DateUtils.js";
import Notification from "Components/Notification.jsx";

class ActivitytSMS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tripDetail: this.props.tripDetail,
      mobile: "",
      smsData: "",
      showMessage: false,
      view_trip_link: "",
      cancel_trip_link: "",
      viewLinkAvail: false,
      short_link: "",
      smssucmsg: false,
      smsfailmsg: false
    };
    this.state.mobile = this.props.getMobileNumber;
    this.sendActivityTripSMS(this.state.mobile);
  }

  sendActivityTripSMS(mobileNumber) {
    let triRefNumber = this.props.tripDetail;
    this.smsLinkReq = {
      trip_id: triRefNumber.trip_ref
    };

    ActivityService.activitySMSLink(this.smsLinkReq)
      .then(response => {
        //console.log('response in  sendActivityTripSMS start---'+JSON.stringify(response));
        const updatedRS = response.data;
        if (
          updatedRS.status !== undefined &&
          updatedRS.status !== null &&
          updatedRS.status === 200
        ) {
          const linkUrl = updatedRS.data.url;
          this.state.view_trip_link = linkUrl;
          this.createActivityTripSMS(mobileNumber);
        } else {
          this.createActivityTripSMS(mobileNumber);
        }
      })
      .catch(function(error) {
        this.setState({ smssucmsg: false, smsfailmsg: true, showAlert: true });
        this.forceUpdate();
        log.error(
          "Exception occured in Send SMS function---" + JSON.stringify(error)
        );
      });
  }

  createActivityTripSMS(mobileNumber) {
    this.state.smssucmsg = "";
    let newTripObj = this.props.tripDetail;
    this.createActivitySMSData();
    const smsObj = {
      mobileNumber: mobileNumber,
      content: this.state.smsData,
      ccId: newTripObj.trip_ref,
      Tags: ["test", "test-q"]
    };
    ActivityService.sendActivitySMS(smsObj)
      .then(response => {
        //console.log("ActivityService.sendActivitySMS " + JSON.stringify(response));
        if (
          response !== undefined &&
          response !== "" &&
          response.data !== undefined &&
          response.data !== "" &&
          response.data.data.status !== undefined &&
          response.data.data.status !== "" &&
          response.data.data.status === "success"
        ) {
          this.setState({ smssucmsg: true, smsfailmsg: false, showAlert: true });
          //this.state.smssucmsg = "SMS sent successfully!!! to " + mobileNumber;
          this.forceUpdate();
        } else {
          this.setState({ smssucmsg: false, smsfailmsg: true, showAlert: true });
          //this.state.smsfailmsg = "SMS not sent";
          this.forceUpdate();
        }
      })
      .catch(function(error) {
        this.setState({ smssucmsg: false, smsfailmsg: true, showAlert: true });
        this.forceUpdate();
        log.error(
          "Exception occured in Send SMS function---" + JSON.stringify(error)
        );
      });
  }

  createActivitySMSData() {
    try {
      const newTripObj = this.props.tripDetail;
      var paxInfo = newTripObj.activity_bookings[0].pax_infos;
      let data = "";
      let flag = false;
      if (newTripObj.domain !== null && newTripObj.domain !== undefined) {
        if (
          newTripObj.contact_detail.country_code === 971 ||
          newTripObj.domain === "cleartripforbusiness.ae"
        ) {
          flag = true;
        }
      }
      if (newTripObj.activity_bookings[0].activities) {
        if (newTripObj.activity_bookings[0].activities.length !== 0) {
          if (paxInfo.length === 1) {
            var paxName =
              newTripObj.contact_detail.first_name +
              " " +
              this.props.tripDetail.contact_detail.last_name;
            data = "Hi " + paxName + "," + "\n";
          }
          data =
            data +
            "Booking for " +
            newTripObj.activity_bookings[0].activities[0].name +
            "\n";

          const adult = newTripObj.activity_bookings[0].adults;
          const child = newTripObj.activity_bookings[0].children;
          let pax_details = adult + " adult " + child + " children";
          const activit_satrtdate =
            newTripObj.activity_bookings[0].activities[0].start_date_time;
          data =
            data +
            "for " +
            pax_details +
            " on " +
            DateUtils.prettyDate(activit_satrtdate, "DD MMM YYYY, hh:mm A") +
            " is confirmed" +
            "\n";
          data = data + "Trip ID : " + newTripObj.trip_ref + "\n";
          if (this.state.view_trip_link !== "") {
            data = data + "\n";
            data =
              data + "View trip in app:" + this.state.view_trip_link + "\n";
          }
          this.state.smsData = data;
        }
      }
    } catch (err) {
      log.error(
        "Exception occured in activity SMS details createActivitySMSData function---" +
          JSON.stringify(err)
      );
    }
  }
  getMessage(type) {
    let msg = "";
    if (type === "success") {
      msg =
      "SMS sent successfully to " + this.state.mobile;
    } else if (type === "error") {
      msg =
      "SMS not sent";
    }
    return msg;
  }
  toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  render() {
    let viewType, messageText;
     if (this.state.smsfailsmsg !== undefined && this.state.smsfailmsg !== "") {
      viewType = "error";
      messageText = this.getMessage("error");
    } else if (this.state.smssucmsg !== undefined && this.state.smssucmsg !== "") {
      viewType = "success";
      messageText = this.getMessage("success");
    }
    return (
      <>
      	{viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}

        {/* <ul className="mb-20">
          <li className="pl-10 pr-10">
            {this.state.smssucmsg !== undefined && this.state.smssucmsg !== "" && (
              <div className="alert notification-success">
                <Icon color="#02AE79" size={16} icon="success" />
                {this.state.smssucmsg}
              </div>
            )}
          </li>

          <li className="pl-10 pr-10 ">
            {this.state.smsfailsmsg !== undefined && this.state.smsfailmsg !== "" && (
                <div className="alert alert-danger">
                  <Icon color="#02AE79" size={16} />
                  {this.state.smsfailmsg}
                </div>
              )}
          </li>
        </ul> */}
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airLineMaster: state.trpReducer.airLineMaster
  };
};
export default connect(mapStateToProps, null)(ActivitytSMS);
