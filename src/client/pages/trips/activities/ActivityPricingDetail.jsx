import ShowHide from "Components/ShowHide.jsx";
import utility from "../../common/Utilities";
import React, { Component } from "react";
import { connect } from "react-redux";

class ActivityPricingDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pricingDetail: ""
    };
  }
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  loadPricingDetail = tripDetail => {
    //console.log("loadPricingDetail---enter");
    let pricingObject = new Object();
    

    if (!this.isEmpty(tripDetail)) {
      let travellers = tripDetail.travellers.trimRight();
      let travellerString = travellers.substring(
        travellers.trimRight().indexOf("|") + 1,
        travellers.length
      );
      let ttravellerArr = travellerString.trimLeft().split("\n");
      for (let travel of ttravellerArr) {
        if (travel.includes("INF")) {
          pricingObject.noOfInf = travel.substring(
            travel.indexOf(" ") + 1,
            travel.length
          );
        }
        if (travel.includes("ADT")) {
          pricingObject.noOfADT = travel.substring(
            travel.indexOf(" ") + 1,
            travel.length
          );
        }
        if (travel.includes("CHD")) {
          pricingObject.noOfCHD = travel.substring(
            travel.indexOf(" ") + 1,
            travel.length
          );
        }
      }
      pricingObject.source_type = tripDetail.txn_source_type;
      pricingObject.currency = tripDetail.currency;
      if (tripDetail.currency === "INR") {
        pricingObject.currency = "Rs";
      }
      pricingObject.insurance = 0;
      if (tripDetail.insurances.length > 0) {
        insurance_fee = tripDetail.insurances[0].total_premium;
        pricingObject.insurance = insurance_fee;
      }
      const activity_data = tripDetail.activity_bookings[0].activities[0];
      const activity_rates = tripDetail.activity_bookings[0].activity_rates[0];     
        pricingObject.total_fare = activity_rates.total_fare;
        pricingObject.total_base_fare = activity_rates.total_base_fare;
        pricingObject.total_markup = activity_rates.total_markup;
        pricingObject.total_tax = activity_rates.total_tax;
        pricingObject.total_fee_gw = activity_rates.total_fee_gw
        pricingObject.total_fee = activity_rates.total_fee;
        pricingObject.total_discount = activity_rates.total_discount;
        pricingObject.total_cashback = activity_rates.total_cashback;
        pricingObject.total_wallet_cashback = activity_rates.total_wallet_cashback;
        pricingObject.total_dis_gw = activity_rates.total_dis_gw;
        pricingObject.total_fee_con = activity_rates.total_fee_con

        
        pricingObject.sup_currency = activity_data.sup_currency;
        pricingObject.sup_total_currency_rate =
          activity_data.sup_total_currency_rate;

      pricingObject.max_pax_per_unit = activity_data.max_pax_per_unit;
      pricingObject.booking_status = activity_data.booking_status;

      let payments_service_data='';
    let payment_mode_desc='';
    if(tripDetail.payments_service_data!==null && tripDetail.payments_service_data.length>0){
      payments_service_data=tripDetail.payments_service_data[0];
      
      if(payments_service_data!==null){
       // console.log("payments_service_data---enter"+payments_service_data);
        

        const payment_type=payments_service_data.payment_type;
        const amount=payments_service_data.amount;
        if(payment_type!==null && amount!==null){
         
        
         if(payment_type==='CC'){        //credit card
          const card_details=payments_service_data.payment_card_details[0];          
          payment_mode_desc='Through Credit Card# '+card_details.card_number;

         }else if(payment_type==='WT'){  //wallet
          payment_mode_desc='Through Wallet Payment ' +pricingObject.currency+'.'+amount;

         }else if(payment_type==='DA'){  //Deposite account
          payment_mode_desc='Through Deposit Account ' +pricingObject.currency+'.'+amount;

         }else if(payment_type==='NB'){  //Net Banking
          payment_mode_desc='Through Net Banking ' +pricingObject.currency+'.'+amount;

         }
        }

        pricingObject.payment_mode_desc=payment_mode_desc;

      }

    }



      this.state.pricingObject = pricingObject;
    }

    
  };

  render() {
    {
      this.loadPricingDetail(this.props.tripDetail);
    }
    let base_fare =
      this.state.pricingObject.total_base_fare +
      this.state.pricingObject.total_markup;
    let total_Amount =
      base_fare +
      this.state.pricingObject.total_tax +
      this.state.pricingObject.total_fee +
      this.state.pricingObject.total_discount+this.state.pricingObject.total_cashback;
    return (
      <>
        <div className="side-pnl">
          <ShowHide visible="true" title="Pricing Details">
            <div className="showHide-content">
              <ul className="price-info pb-10">
                {(base_fare!== undefined && base_fare !==null && base_fare !== 0) ? (
                  <li>
                    <span>Base Fare</span>{" "}
                    <span>                      
                      {this.state.pricingObject.currency}
                      {utility.PriceFormat(
                          parseFloat(base_fare, 10),
                          ""
                        )}
                    </span>
                  </li>
                ) : (
                  ""
                )}
                {(this.state.pricingObject.total_tax !== null && this.state.pricingObject.total_tax !== 0) ? (
                  <li>
                    <span>Taxes & Fees</span>{" "}
                    <span>
                    {this.state.pricingObject.currency}
                    {utility.PriceFormat(
                          parseFloat(this.state.pricingObject.total_tax, 10),
                          ""
                        )}
                      
                    </span>
                  </li>
                ) : (
                  ""
                )}
                {(this.state.pricingObject.total_fee_gw !== null && this.state.pricingObject.total_fee_gw !== 0) ? (
                  <li>
                    <span>Processing Fee</span>{" "}
                    <span>
                    {this.state.pricingObject.currency}
                    {utility.PriceFormat(
                          parseFloat(this.state.pricingObject.total_fee_gw, 10),
                          ""
                        )}
                      
                    </span>
                  </li>
                ) : (
                  ""
                )}


                {(this.state.pricingObject.total_fee_con !== null && this.state.pricingObject.total_fee_con !== 0) ? (
                  <li>
                    <span>Convenience Fee</span>{" "}
                    <span>
                    {this.state.pricingObject.currency}
                    {utility.PriceFormat(
                          parseFloat(this.state.pricingObject.total_fee_con, 10),
                          ""
                        )}
                      
                    </span>
                  </li>
                ) : (
                  ""
                )}
                {(this.state.pricingObject.total_discount !== null && this.state.pricingObject.total_discount !== 0) ? (
                  <li>
                    <span>Discount (-)</span>{" "}
                    <span>
                    {this.state.pricingObject.currency}
                    {utility.PriceFormat(
                          parseFloat((-1*this.state.pricingObject.total_discount), 10),
                          ""
                        )}                      
                    </span>
                  </li>
                ) : (
                  ""
                )}

              {(this.state.pricingObject.total_cashback !== null && this.state.pricingObject.total_cashback !== 0) ? (
                  <li>
                    <span>Cashback (-)</span>{" "}
                    <span>
                    {this.state.pricingObject.currency}
                    {utility.PriceFormat(
                          parseFloat((-1*this.state.pricingObject.total_cashback), 10),
                          ""
                        )}                      
                    </span>
                  </li>
                ) : (
                  ""
                )}

                {total_Amount!==undefined && total_Amount!==null && total_Amount !== 0 ? (
                  <li className="total">
                    <span>Total</span>{" "}
                    <span>
                    {this.state.pricingObject.currency}
                    {utility.PriceFormat(
                          parseFloat(total_Amount, 10),
                          ""
                        )}
                      
                    </span>
                  </li>
                ) : (
                  ""
                )}
              </ul>
              <span className="font-12 t-color3 d-b">{this.state.pricingObject.payment_mode_desc}</span>
            </div>
          </ShowHide>
        </div>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};

export default connect(mapStateToProps)(ActivityPricingDetail);
