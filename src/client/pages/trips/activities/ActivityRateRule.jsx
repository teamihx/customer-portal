import React, { Component } from 'react'
import { connect } from "react-redux";
class ActivityRateRule extends Component {
    constructor(props) {
      super(props);
      this.state = { 
          activity_name: this.props.tripDetail.activity_bookings[0].activities[0].name,          
          showRateRule:false,
          rateRuleData: []
    };
	this.getRateRules();
}

getRateRules=(event)=>{
  try {
    this.state.rateRuleData = [];
    let currency = this.props.tripDetail.currency;
    if (this.props.tripDetail.currency === "INR") {
      currency = "Rs.";
    }
    const activities_data = this.props.tripDetail.activity_bookings[0]
      .activities[0];

    const obj_data = JSON.parse(activities_data.cancellation_charges);
    let count=0;
    for (let obj of obj_data) {
        
        //console.log('obj'+(count+1));
        //console.log('obj123'+obj);
      let rateRuleStr = " If cancelled between";
      let rateRuleStr1 = " will be deducted";
      let rateRuleStr2 = " If cancelled before";
      const base_fare = this.props.tripDetail.activity_bookings[0]
        .activity_rates[0].total_base_fare;
      const markup = this.props.tripDetail.activity_bookings[0]
        .activity_rates[0].total_markup;
      const tot_base_fare = base_fare + markup;
      if (obj["0h-6h"] !== null && obj["0h-6h"] !== undefined) {
        const firstRule = " 0h-6h ";
        let refundStr = "";
        const percenatgevalue = obj["0h-6h"];
        if (
          percenatgevalue !== null &&
          percenatgevalue !== undefined &&
          percenatgevalue >= 0
        ) {
          const deducted_amount = (tot_base_fare * percenatgevalue) / 100;
          let refund_value = tot_base_fare - deducted_amount;             
          if (
            refund_value !== null &&
            refund_value !== undefined &&
            refund_value > 0
          ) {
            refundStr = "(refund : " + currency + refund_value + ")";
          }
        }
        let final_first_rule =
          rateRuleStr +
          firstRule +
          percenatgevalue +
          "%" +
          rateRuleStr1 +
          refundStr;
        this.state.rateRuleData.push(final_first_rule);
      } 
      else if (obj["6h-24h"] !== null && obj["6h-24h"] !== undefined) {
        const secondRule = " 6h-24h  ";
        let refundStr = "";
        const percenatgevalue = obj["6h-24h"];
        if (
          percenatgevalue !== null &&
          percenatgevalue !== undefined &&
          percenatgevalue >= 0
        ) {
          const deducted_amount = (tot_base_fare * percenatgevalue) / 100;
          let refund_value = tot_base_fare - deducted_amount;
          if (
            refund_value !== null &&
            refund_value !== undefined &&
            refund_value > 0
          ) {
            refundStr = "(refund : " + currency + refund_value + ")";
          }
        }
        let final_second_rule =
          rateRuleStr +
          secondRule +
          percenatgevalue +
          "%" +
          rateRuleStr1 +
          refundStr;
        this.state.rateRuleData.push(final_second_rule);
      } 
      else if (
        obj["24h-48h"] !== null &&
        obj["24h-48h"] !== undefined
      ) {
        const thirdRule = " 24h-48h  ";
        let refundStr = "";
        const percenatgevalue = obj["24h-48h"];
        if (
          percenatgevalue !== null &&
          percenatgevalue !== undefined &&
          percenatgevalue >= 0
        ) {
          const deducted_amount = (tot_base_fare * percenatgevalue) / 100;
          let refund_value = tot_base_fare - deducted_amount;
          if (
            refund_value !== null &&
            refund_value !== undefined &&
            refund_value > 0
          ) {
            refundStr = "(refund : " + currency + refund_value + ")";
          }
        }
        let final_third_rule =
          rateRuleStr +
          thirdRule +
          percenatgevalue +
          "%" +
          rateRuleStr1 +
          refundStr;
        this.state.rateRuleData.push(final_third_rule);
      } 
       else if (obj["48h"] !== null && obj["48h"] !== undefined) {
        const fourthRule = " 48h  ";
        let refundStr = "";
        const percenatgevalue = obj["48h"];
        if (
          percenatgevalue !== null &&
          percenatgevalue !== undefined &&
          percenatgevalue >= 0
        ) {
          const deducted_amount = (tot_base_fare * percenatgevalue) / 100;
          let refund_value = tot_base_fare - deducted_amount;

          if (
            refund_value !== null &&
            refund_value !== undefined &&
            refund_value > 0
          ) {
            refundStr = "(refund : " + currency + refund_value + ")";
          }
        }
        let final_fourth_rule =
          rateRuleStr2 +
          fourthRule +
          percenatgevalue +
          "%" +
          rateRuleStr1 +
          refundStr;
        this.state.rateRuleData.push(final_fourth_rule);
      }
    }

    //console.log("this.state.rateRuleData----" + this.state.rateRuleData);
  } catch (e) {
    log.error(
      "Exception occured in ActivityTripDetails showRateRule function---" +
        e
    );
  }


}

 


    render() {
      
      return (
        <>
        <div className="hotelPolicy mb-15">
          <>
      <p className="t-color1"><strong>Cleartrip booking policy</strong> for :{this.state.activity_name}</p>            
      <div>
            {(this.state.rateRuleData !== null && 
            this.state.rateRuleData !== undefined &&
            this.state.rateRuleData.length > 0) && (
              <ul>                
                {this.state.rateRuleData.map((item, key) => (
                  <li key={item}>{item}<br/></li>
                ))}
              </ul>
            )}
            </div>
          </>
         
           
        </div>
         
        </>
      );
    }
  }
  const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail
    };
  };
  
  
  export default connect(mapStateToProps)(ActivityRateRule);