import React, { Component } from "react";
import Icon from "Components/Icon";
import { connect } from "react-redux";
import ToggleDisplay from "react-toggle-display";
import { Link } from "react-router-dom";
import log from "loglevel";
import ActivityEmailDetails from "./../activities/ActivityEmailDetails";
import ActivityEmailInvoice from "./../activities/ActivityEmailInvoice";
import ActivityPricingDetail from "./../activities/ActivityPricingDetail";
import ActivitySMS from "./../activities/ActivitytSMS";
export const contextPath = process.env.contextpath;
import ChangeBkgStatusClsTxn from "../generic/ChangeBkgStatusClsTxn";
import SourceTypeInfo from '../generic/SourceTypeInfo'
import Tags from "Pages/trips/generic/Tags.jsx";
import ShowHide from "Components/ShowHide.jsx";
import TripGenericService from "../../../services/trips/generic/TripGenericService";
import Button from "Components/buttons/button.jsx";
export const TRIPS_ROLES = "tripsRoles";
import Domainpath from '../../../services/commonUtils/Domainpath';
class ActivityRightSidebar extends Component {
  constructor(props) {
    super(props);
    this.redirect = this.redirect.bind(this);
    this.state = {
      tripid: this.props.tripDetail.trip_ref,
      tripDetail: this.props.tripDetail,
      show: false,
      emailDetailsShow: false,
      emailReceiptShow: false,
      emailDetailsContent: "",
      loading: false,
      mobileNum: "",
      showMessage: false,
      sucessmsg: false,
      smsData: "",
      buttonclick: false,
      emailSalesReceiptShow: false,
      emailTicketShow: false,
      emailVATInvoiceShow: false,
      emailShowBookStepSceenShow: false,
      pricingObject: {},
      ticketUrl: '',
      voucherUrl: '',
      invoiceUrl: '',
      openTxnAvailable: false,
      ticketAvailable: false,
      vatInvoiceApplicatable: false,
      voucherAvailable: false,
      emailInvoiceShow: false,
      receiptAvailableHtl: false,
      openTxnAvailable: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.sendFltSMS = this.sendFltSMS.bind(this);
    this.renderComponents = this.renderComponents.bind(this);
    let tripsObj = JSON.parse(localStorage.getItem(TRIPS_ROLES));
    this.state.localtripRoles = tripsObj.localtripsRoles;
    
    var domainPath=Domainpath.getHqDomainPath();
    this.state.ticketUrl= domainPath + "/ticket";
    this.state.voucherUrl=domainPath + "/voucher";
    this.state.invoiceUrl=domainPath + "/voucher";
  }

  checkIfOpenTxn = () => {
    this.openTxnAvailable = TripGenericService.checkForOpenTxnAvailable(
      this.props.tripDetail.txns
    );
  };

  redirect = trip_ref => event => {
    try {
      event.preventDefault();
      let url = "";
      var domainpath = Domainpath.getHqDomainPath();
      if (event.target.id === "lossTracker") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/loss_tracker/";
      } else if (event.target.id === "printInvoice") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/invoice";
      } else if (event.target.id === "bookInsurance") {
        url =
          domainpath + "/hq/trips/" + this.state.tripid + "/book_insurance/";
      } else if (event.target.id === "bookstepScreen") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/screenshots";
        //window.location.replace(url);
      } else if (event.target.id === "xml") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/xml";
      } else if (event.target.id === "cancel") {
        url = domainpath + "/hq/trips/" + trip_ref + "/cancel";
      }

      window.open(url);
    } catch (err) {
      log.error("Exception occured in Footer redirect function---" + err);
    }
  };
  handleChange(event) {
    this.state.showMessage = false;
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleClick() {
    this.setState({
      show: !this.state.show
    });
    if (
      this.state.tripDetail.contact_detail.mobile !== null &&
      this.state.tripDetail.contact_detail.mobile !== undefined
    ) {
      this.state.mobileNum = this.state.tripDetail.contact_detail.mobile;
    } else {
      this.state.mobileNum = this.state.tripDetail.contact_detail.mobile_number;
    }
  }

  renderComponents(e) {
    this.currentClick = e.target.id;

    if (this.currentClick === "fphemailDtls") {
      this.setState({
        emailDetailsShow: !this.state.emailDetailsShow
      });
    } else if (this.currentClick === "emailInvoice") {
      this.setState({
        emailInvoiceShow: !this.state.emailInvoiceShow
      });
    }
  }

  sendFltSMS(event) {
    try {
      this.state.showMessage = false;
      if (this.state.mobileNum === undefined || this.state.mobileNum === "") {
        this.state.showMessage = true;
        this.forceUpdate();
      } else {
        this.state.loading = true;
        this.state.buttonclick = true;
        this.state.loading = false;
        this.forceUpdate();
      }
    } catch (err) {
      this.state.loading = false;
      log.error("Exception occured in Send SMS function---" + err);
    }
  }

  render() {
    {
      this.checkIfOpenTxn();
    }
    const tripJsonUrl =
      contextPath + "/trips/" + this.state.tripDetail.trip_ref + "/json";
    const tripXMl =
      process.env.domainpath + "/hq/trips/" + this.state.tripid + "/xml";
    const loss_track_url =
      process.env.domainpath +
      "/hq/trips/" +
      this.state.tripid +
      "/loss_tracker/";
    const print_invoice =
      process.env.domainpath + "/hq/trips/" + this.state.tripid + "/invoice/";
    const booking_status = this.state.tripDetail.booking_status;
    const cancel_Trip =
      process.env.domainpath + "/hq/trips/" + this.state.tripid + "/cancel";
    const trip_ref = this.state.tripid;
    return (
      <>
        <div className="side-pnl">
          <ShowHide visible="true" title="Tips, tools & extras">
            <div className="showHide-content">
              <ul className="mb-20">
                {/* Email Trip Details */}
                {this.state.localtripRoles.LOCAL_SEND_EMAIL_ROLE_ENABLE &&
                  !this.state.openTxnAvailable &&
                  (this.props.tripDetail.booking_status !== "H" &&
                  this.props.tripDetail.booking_status !== "Z" ) && (
                    <li className="pl-20 ">
                      <a
                        id="fphemailDtls"
                        onClick={this.renderComponents}
                        className={
                          this.currentClick === "fphemailDtls" ? "active" : ""
                        }
                      >
                        <Icon
                          className="arrow rotate90 mt--3"
                          color="#36c"
                          size={10}
                          icon="down-arrow"
                        />
                        Email Trip Details
                      </a>
                      <ToggleDisplay show={this.state.emailDetailsShow}>
                        <ActivityEmailDetails />
                      </ToggleDisplay>
                    </li>
                  )}

                {/* SMS Trip Details */}
                {this.state.localtripRoles.LOCAL_SEND_SMS_ROLE_ENABLE && (
                  <li className="pl-20 ">
                    {this.state.buttonclick && (
                      <ActivitySMS getMobileNumber={this.state.mobileNum} />
                    )}
                    {this.state.showMessage && (
                      <div className="alert alert-error">
                        <Icon
                          className="noticicon"
                          color="#F4675F"
                          size={16}
                          icon="warning"
                        />
                        Enter mobile number
                      </div>
                    )}

                    <a onClick={() => this.handleClick()}>
                      <Icon
                        className="arrow rotate90 mt--3"
                        color="#36c"
                        size={10}
                        icon="down-arrow"
                      />SMS trip details
                    </a>
                    <ToggleDisplay className="test123s" show={this.state.show}>
                      <div className="row">
                        <div className="col-9 form-group">
                          <input
                            type="text"
                            name="mobileNum"
                            value={this.state.mobileNum}
                            onChange={this.handleChange}
                          />
                        </div>
                        <div className="col-3 pl-0">
                          <Button
                            size="xs"
                            viewType="primary"
                            onClickBtn={this.sendFltSMS}
                            loading={this.state.loading}
                            btnTitle="Send"
                          />
                        </div>
                      </div>
                    </ToggleDisplay>
                  </li>
                )}

                {/* Email invoice for this trip */}
                {this.state.localtripRoles
                  .LOCAL_SEND_EMAIL_INVOICE_ROLE_ENABLE &&
                  !this.state.openTxnAvailable &&
                  booking_status !== "Z" &&
                  booking_status !== "H" &&
                  booking_status !== "K" &&
                  booking_status !== "Q" && (
                    <li className="pl-20 ">
                      <a
                        id="emailInvoice"
                        onClick={this.renderComponents}
                        className={
                          this.currentClick === "emailInvoice" ? "active" : ""
                        }
                      >
                        <Icon
                          className="arrow rotate90 mt--3"
                          color="#36c"
                          size={10}
                          icon="down-arrow"
                        />
                        Email invoice for this trip
                      </a>
                      <ToggleDisplay show={this.state.emailInvoiceShow}>
                        <ActivityEmailInvoice />
                      </ToggleDisplay>
                    </li>
                  )}

                {/* Print Tax invoice for this trip */}
                {this.state.localtripRoles.LOCAL_PRINT_INVOICE_ROLE_ENABLE &&
                  !this.state.openTxnAvailable &&
                  booking_status !== "Z" &&
                  booking_status !== "H" &&
                  booking_status !== "Q" &&
                  booking_status !== "K" && (
                    <li className="pl-20 ">
                      <a
                        id="printInvoice"
                        href={print_invoice}
                        target="_blank"
                        onClick={this.redirect}
                        title="Print invoice"
                      >
                        <Icon
                          className="arrow rotate90 mt--3 "
                          color="#36c"
                          size={10}
                          icon="down-arrow"
                        />
                        Print invoice for this trip
                      </a>
                    </li>
                  )}

                {/* Loss Tracker For trip */}
                {this.state.localtripRoles.LOCAL_LOSS_TRACKER_ROLE_ENABLE && (
                  <li className="pl-20 ">
                    <a
                      id="lossTracker"
                      href={loss_track_url}
                      target="_blank"
                      onClick={this.redirect}
                      title=" Loss Tracker For trip"
                    >
                      <Icon
                        className="arrow rotate90 mt--3 "
                        color="#36c"
                        size={10}
                        icon="down-arrow"
                      />
                      Loss Tracker For trip
                    </a>
                  </li>
                )}

                {/* Trip XML */}
                {this.state.localtripRoles.LOCAL_TRIP_XML_ROLE_ENABLE && (
                  <li className="pl-20 ">
                    <a
                      id="xml"
                      href={tripXMl}
                      target="_blank"
                      onClick={this.redirect}
                      title="Trip XML"
                    >
                      <Icon
                        className="arrow rotate90 mt--3 "
                        color="#36c"
                        size={10}
                        icon="down-arrow"
                        title="Trip XML"
                      />
                      Trip XML
                    </a>
                  </li>
                )}

                {/* Trip JSON */}
                {this.state.localtripRoles.LOCAL_TRIP_JSON_ROLE_ENABLE && (
                  <li className="pl-20 ">
                    <a
                      id="json"
                      href={tripJsonUrl}
                      target="_blank"
                      title="Trip JSON"
                    >
                      <Icon
                        className="arrow rotate90 mt--3 "
                        color="#36c"
                        size={10}
                        icon="down-arrow"
                      />
                      Trip JSON
                    </a>
                  </li>
                )}
              </ul>
              {/* Change Booking status Closes Txn */}
              {this.state.localtripRoles
                .LOCAL_CHANGE_BKNG_STS_CLOSE_TXN_ROLE_ENABLE && (
                <ChangeBkgStatusClsTxn
                  tripId={this.state.tripDetail.trip_ref}
                />
              )}
              
              {/* Cancel Trip for Activity */}
              {this.state.localtripRoles.LOCAL_CANCEL_TRIP_ROLE_ENABLE &&
                booking_status !== null &&
                booking_status === "P" && (
                  <div className="amndmnt mt-10 pl-20 pr-20">
                    <a
                      id="cancel"
                      href={cancel_Trip}
                      className="btn btn-default btn-xs"
                      target="_blank"
                      onClick={this.redirect(trip_ref)}
                      title="Cancel Trip"
                    >
                      Cancel Trip
                    </a>
                  </div>
                )}
            </div>
          </ShowHide>
          <SourceTypeInfo/>
          {this.state.localtripRoles.LOCAL_TAG_ROLE_ENABLE && <Tags />}

          {this.state.localtripRoles.LOCAL_PRICING_DETAILS_ROLE_ENABLE && (
            <ActivityPricingDetail />
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};
export default connect(mapStateToProps, null)(ActivityRightSidebar);
