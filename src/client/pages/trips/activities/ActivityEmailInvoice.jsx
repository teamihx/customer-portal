import React, { Component } from "react";
import { connect } from "react-redux";
import ActivityService from "../../../services/trips/activity/ActivityService.js";
import Notification from "Components/Notification.jsx";
import Button from "Components/buttons/button.jsx";
export const contextPath = process.env.domainpath;

class ActivityEmailInvoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseData1: this.props.tripDetail,
      email: this.props.tripDetail.contact_detail.email,
      showSucessMessage: false,
      showFailMessage: false,
      message: false,
      sendTicket: false,
      errorMsgList: [],
      showInputMessage: false
    };

    this.sendActivityEmailInvoice = this.sendActivityEmailInvoice.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getMessage = this.getMessage.bind(this);
    this.validateEmails = this.validateEmails.bind(this);
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  sendActivityEmailInvoice = () => {
    this.setState({ sendTicket: true });
    if (this.validateEmails()) {
      this.invoiceReq = {
        trip_id: this.state.responseData1.trip_ref,
        email_to: this.state.email
      };

      ActivityService.sendEmailInvoice(this.invoiceReq)
        .then(res => {
          if (res) {
            if (res !== undefined && res !== "") {
              let response = res.data;
              if (response.msg === "SUCCESS") {
                this.setState({
                  showSucessMessage: true,
                  sendTicket: false,
                  showAlert: true
                });
              } else {
                this.setState({
                  showFailMessage: true,
                  sendTicket: false,
                  showAlert: true
                });
              }
            }
          }
        })
        .catch(function(error) {
          this.setState({ showFailMessage: true, sendTicket: false });
          log.error(
            "Exception occured in ActivityEmailInvoice.jsx sendActivityEmailInvoice  function---" +
              JSON.stringify(error)
          );
        });
    }
  };

  toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  getMessage(type) {
    let msg = "";
    if (type === "success") {
      msg = "We have sent Invoice in an email to " + this.state.email;
    } else if (type === "error") {
      msg =
        "We are not able to send a mail with Invoice  details. You can try again at some other time ";
    } else if (type === "warning") {
      msg = "Please enter mobile number";
    }
    return msg;
  }

  validateEmails() {
    let isValid = true;
    this.state.errorMsgList = [];
    if (this.state.email === null || this.state.email.trim() === "") {
      this.state.errorMsgList.push("Email must be provided");
      isValid = false;
      this.setState({
        showInputMessage: true,
        sendTicket: false,
        showAlert: true
      });
    }
    if (this.state.email != null && this.state.email.trim() !== "") {
      const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
      isValid = expression.test(String(this.state.email).toLowerCase());
      if (!isValid) {
        this.state.errorMsgList.push("Enter a valid email address");
        this.setState({
          showInputMessage: true,
          sendTicket: false,
          showAlert: true
        });
      }
    }
    return isValid;
  }

  render() {
    let viewType, messageText;
    if (this.state.showInputMessage) {
      viewType = "warning";
      messageText = this.state.errorMsgList;
    } else if (this.state.showFailMessage) {
      viewType = "error";
      messageText = this.getMessage("error");
    } else if (this.state.showSucessMessage) {
      viewType = "success";
      messageText = this.getMessage("success");
    }
    return (
      <>
        <div>
          {viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}

          <div className="form-group">
            <div className="row">
              <div className="col-9">
                <input
                  type="text"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-3 pl-0">
                <Button
                  size="xs"
                  viewType="primary"
                  onClickBtn={this.sendActivityEmailInvoice}
                  loading={this.state.sendTicket}
                  btnTitle="Send"
                ></Button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};

export default connect(mapStateToProps, null)(ActivityEmailInvoice);
