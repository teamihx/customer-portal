import React, { Component } from "react";
import { connect } from "react-redux";
import TrainService from "../../../services/trips/train/TrainService";
import Button from "Components/buttons/button.jsx";
import Notification from "Components/Notification.jsx";
export const contextPath = process.env.domainpath;

class TrainSMS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseData1: this.props.tripDetail,
      phoneNumber: this.props.tripDetail.contact_detail.mobile,
      showSucessMessage: false,
      showFailMessage: false,
      showInputMessage: false,
      message: false,
      loading: false,
      errorMsgList: []
    };
   // console.log("constructor");
    this.sendSMSDetails = this.sendSMSDetails.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getMessage = this.getMessage.bind(this);
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  sendSMSDetails = () => {
    this.setState({ sendTrip: true });
    try {
     // console.log("send SMS Phone number: " + this.state.phoneNumber);
      if (this.state.phoneNumber !== "") {
        let phoneObj = new Object();
        phoneObj.tripRefNumber = this.state.responseData1.trip_ref;
        phoneObj.phoneNum = this.state.phoneNumber;
        TrainService.sendTrainSMS(phoneObj).then(response => {
         // console.log("send SMS : " + JSON.stringify(response));
          if (response.data !== undefined && response.data !== "") {
            if (
              response.data.data !== undefined &&
              response.data.data === "success"
            ) {
              this.setState({
                showSucessMessage: true,
                sendTrip: false,
                showAlert: true
              });
            } else {
              this.setState({
                showFailMessage: true,
                sendTrip: false,
                showAlert: true
              });
            }
          }
        });
      } else {
        this.state.errorMsgList.push("Phone number must be provided");
        this.setState({
          showInputMessage: true,
          sendTrip: false,
          showAlert: true
        });
      }
    } catch (err) {
      console.log("sms err due to: " + err);
    }
  };

  getMessage(type) {
    let msg = "";
    if (type === "success") {
      msg =
        "We have sent the itinerary details by sms to " +
        this.state.phoneNumber;
    } else if (type === "error") {
      msg =
        "We are not able to send a sms with itinerary details. You can try again after some time ";
    }
    return msg;
  }
  toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  render() {
    let viewType, messageText;
    if (this.state.showInputMessage) {
      viewType = "error";
      messageText = this.state.errorMsgList;
    } else if (this.state.showFailMessage) {
      viewType = "error";
      messageText = this.getMessage("error");
    } else if (this.state.showSucessMessage) {
      viewType = "success";
      messageText = this.getMessage("success");
    }
    //console.log('TRAIN SMSSSSS RENDER');
    return (
      <>
        <div>
          {viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}
            <div className="row">
              <div className="col-9 form-group">
                <input
                  type="text"
                  name="phoneNumber"
                  value={this.state.phoneNumber}
                  onChange={this.handleChange}
                />
                </div>

                <div className="col-3 pl-0">
                  <div>
                    <Button
                      size="xs"
                      viewType="primary"
                      onClickBtn={this.sendSMSDetails}
                      loading={this.state.sendTrip}
                      btnTitle="Send"
                    ></Button>
                  </div>
                </div>
              </div>
        </div>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};

export default connect(mapStateToProps, null)(TrainSMS);
