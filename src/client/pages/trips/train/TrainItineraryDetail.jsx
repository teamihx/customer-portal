import React, { Component } from "react";
import { Link } from "react-router-dom";
import Popup from "reactjs-popup";
import utility from "../../common/Utilities";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import TrainService from "../../../services/trips/train/TrainService";
import Tooltip from "react-tooltip-lite";
import Icon from "Components/Icon";
//import * as actions from '../../../store/actions/index'
import converter from "number-to-words";
import Domainpath from "../../../services/commonUtils/Domainpath";

class TrainItineraryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TrainItineraryDetail: [],
      deptStationName: "",
      arrailStationName: "",
      checkStationName: true
    };
  }

  isAirIndiaExpress = flt => {
    for (let seg of flt.segments) {
      if (seg.marketing_airline === "IX") {
        return true;
      }
    }

    return false;
  };
  showNewPopUp(flt, e) {
    e.persist();
    let url = "";
    //console.log("e.target.id +" + JSON.stringify(flt));
    let domainpath = Domainpath.getHqDomainPath();
    if (flt.air_booking_type === "I" && !this.isAirIndiaExpress(flt)) {
      url = domainpath + "/hq/trips/" + flt.itinerary_id + "/get_int_fare_rule";
    } else {
      url = domainpath + "/hq/trips/flight/" + flt.id + "/fare_rule";
    }

    window.open(url);
  }
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  loadTrainItinerary = () => {
    let trainBkng = this.props.tripDetail.train_bookings[0];
    for (let route of trainBkng.train_routes) {
      this.getStationName(route.arrival_station).then(res => {
        route.arrival_station_name = res;
       // console.log("route.arrival_station===" + route.arrival_station);
        //this.setState({TrainItineraryDetail:trainBkng.train_routes})
      });
      this.getStationName(route.departure_station).then(res => {
        route.departure_station_name = res;
        //console.log("route.departure_station===" + route.departure_station);
        //this.setState({TrainItineraryDetail:trainBkng.train_routes})
      });
      for (let train of route.trains) {
        this.getStationName(train.arrival_station).then(res => {
          train.arrival_station_name = res;
         // console.log("route.arrival_station===" + route.arrival_station);
          this.setState({ TrainItineraryDetail: trainBkng.train_routes });
        });
        this.getStationName(train.boarding_station).then(res => {
          train.boarding_station_name = res;
         // console.log("route.departure_station===" + route.departure_station);
          this.setState({ TrainItineraryDetail: trainBkng.train_routes });
        });
      }
    }
  };
  componentDidMount() {
    this.loadTrainItinerary();
  }

  getStationName = stationCode => {
    return new Promise(
      function(resolve, reject) {
       // console.log("Save transaction statrts");
        TrainService.getStaionName(stationCode).then(response => {
         // console.log("getStationName 1 " + JSON.stringify(response));
          if (response !== null && response !== "" && response !== undefined) {
            //console.log("getStationName 2 " + response.data);
            resolve(response.data);
          }
        });
      }.bind(this)
    );
  };

  render() {
    var domainpath = Domainpath.getHqDomainPath();
    return (
      <>
        {this.state.TrainItineraryDetail.length > 0
          ? this.state.TrainItineraryDetail.map(ele => {
              return (
                <React.Fragment key={ele.id}>
                  <div className="heading dis-flx-btw mb-5">
                    {this.state.TrainItineraryDetail.indexOf(ele) === 0 ? (
                      <>
                        <h4 className="in-tabTTl-sub">
                          <Icon
                            className="noticicon"
                            color="#36c"
                            size={15}
                            icon="train"
                          /> Itinerary
                        </h4>
                      </>
                    ) : null}
                    <div className="trip-sub">                     
                      {ele.departure_station_name} to {ele.arrival_station_name}{" "} 
                      - {dateUtil.prettyDate(ele.departure_date_time,"ddd, DD MMM YYYY")}
                    </div>
                  </div>
                  <div className="resTable">
                    <table className="dataTable3">
                      <thead>
                        <tr>
                          <th width="30%">Departure</th>
                          <th width="30%">Arrival</th>
                          <th width="30%">Train</th>
                          <th width="10%">Duration</th>
                        </tr>
                      </thead>
                      <tbody>
                        {ele.trains.map(train => (
                          <tr key={train.id}>
                            <td className="pr-10">
                              <span className="d-b mb-3 t-color1">
                                {train.boarding_station_name} (
                                {train.boarding_station})
                              </span>
                              <span className="t-color2 font-12 d-ib">
                                {dateUtil.prettyDate(
                                  train.boarding_date_time,
                                  "HH:mm, DD MMM"
                                )}
                              </span>{" "}
                            </td>
                            <td className="pr-10">
                              <span className="d-b mb-3 t-color1">
                                {train.arrival_station_name} (
                                {train.arrival_station})
                              </span>
                              <span className="t-color2 font-12 d-ib">
                                {dateUtil.prettyDate(
                                  train.arrival_date_time,
                                  "HH:mm, DD MMM"
                                )}
                              </span>{" "}
                            </td>
                            <td className="pr-10">
                              <span className="lnkLine d-b mb-3">
                                <a
                                  id="trainName"
                                  href={
                                    domainpath +
                                    "/trains/" +
                                    train.train_number +
                                    "?depart_date=" +
                                    dateUtil.convertToDate(
                                      train.departure_date_time
                                    ) +
                                    "&from=" +
                                    train.departure_station +
                                    "&to=" +
                                    train.arrival_station
                                  }
                                  target="_blank"
                                >
                                  {train.train_name}
                                </a>
                              </span>
                              <span className="t-color2 font-12 d-b">
                                {train.train_number}
                              </span>{" "}
                            </td>
                            <td>{dateUtil.secondsToHm(train.duration)}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>{" "}
                </React.Fragment>
              );
            })
          : null}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airPortMaster: state.trpReducer.airPortMaster,
    airLineMaster: state.trpReducer.airLineMaster,
    FlightSupplierMasterData: state.trpReducer.FlightSupplierMasterData,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster
  };
};
export default connect(mapStateToProps, null)(TrainItineraryDetail);
