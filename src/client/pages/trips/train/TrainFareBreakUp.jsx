import React, { Component } from 'react'
import Popup from "reactjs-popup";
import { Link } from 'react-router-dom'
import utility from '../../common/Utilities'

class TrainFareBreakUp extends Component {
    constructor(props) {
        super(props);
        this.state = { open: false };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
        this.setState({ open: true });
    }
    closeModal() {
        this.setState({ open: false });
    }

    render() {
        return (
            <div>

                <Popup
                    trigger={<a onClick={this.openModal}>
                        {utility.PriceFormat(this.props.data.total_fare, this.props.data.currency)}
                    </a>}
                    open={this.state.open}
                    on="focus"
                    position="top left"
                    closeOnDocumentClick
                    onClose={this.closeModal}
                    position="bottom right"
                >
                    <div className="modal pop-grid">
                        <ul>
                        {this.props.data.total_base_fare!==0? <li><span>Base fare</span>  {utility.PriceFormat(this.props.data.total_base_fare, this.props.data.currency)}</li>:null}
                           {this.props.data.total_markup!==0? <li className="font-w-b t-color1"><span className="t-color1">Total markup</span>  {utility.PriceFormat(this.props.data.total_markup, this.props.data.currency)}</li>:null}
                           {this.props.data.total_cashback!==0? <li className="font-w-b t-color1"><span className="t-color1">Total cashback</span>  {utility.PriceFormat(this.props.data.total_cashback, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee!==0? <li className="font-w-b t-color1"><span className="t-color1">Total fee</span>  {utility.PriceFormat(this.props.data.total_fee, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_irctc_tc!==0? <li><span>IRCTC tatkal fee</span>  {utility.PriceFormat(this.props.data.total_fee_irctc_tc, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_irctc_svf!==0? <li><span>IRCTC service fee</span>  {utility.PriceFormat(this.props.data.total_fee_irctc_svf, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_irctc_cc!==0 || this.props.data.total_fee_unknown!==0?<li><span>IRCTC catering charges</span>  {utility.PriceFormat(this.props.data.total_fee_unknown + this.props.data.total_fee_irctc_cc, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_ct_svf!==0? <li><span>Cleartrip service fee</span>  {utility.PriceFormat(this.props.data.total_fee_ct_svf, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_gw!==0? <li><span>Processing fee</span>  {utility.PriceFormat(this.props.data.total_fee_gw, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_irctc_tic!==0? <li><span>IRCTC insurance charges</span>  {utility.PriceFormat(this.props.data.total_fee_irctc_tic, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_irctc_rf!==0?<li><span>IRCTC reservation fee</span>  {utility.PriceFormat(this.props.data.total_fee_irctc_rf, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_irctc_sf!==0? <li><span>IRCTC superfast fee</span>  {utility.PriceFormat(this.props.data.total_fee_irctc_sf, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_irctc_oc!==0? <li><span>IRCTC othere chrg.</span>  {utility.PriceFormat(this.props.data.total_fee_irctc_oc, this.props.data.currency)}</li>:null}
                          {this.props.data.total_fee_unknown!==0?<li><span>Unknown</span>  {utility.PriceFormat(this.props.data.total_fee_unknown, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax!==0? <li className="font-w-b t-color1"><span className="t-color1">Total tax</span>  {utility.PriceFormat(this.props.data.total_tax, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_irctc_st!==0? <li><span>IRCTC service tax</span>  {utility.PriceFormat(this.props.data.total_tax_irctc_st, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_ct_st!==0?<li><span>Cleartrip service tax</span>  {utility.PriceFormat(this.props.data.total_tax_ct_st, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_ct_cgst!==0?<li><span>Cleartrip CGST</span>  {utility.PriceFormat(this.props.data.total_tax_ct_cgst, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_ct_sgst!==0?<li><span>Cleartrip SGST</span>  {utility.PriceFormat(this.props.data.total_tax_ct_sgst, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_ct_igst!==0? <li><span>Cleartrip IGST</span>  {utility.PriceFormat(this.props.data.total_tax_ct_igst, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_unknown!==0? <li><span>Unknown tax</span>  {utility.PriceFormat(this.props.data.total_tax_unknown, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_irctc_tist!==0? <li><span>IRCTC ins service tax</span>  {utility.PriceFormat(this.props.data.total_tax_irctc_tist, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_gw_st!==0? <li><span>Processing service tax</span>  {utility.PriceFormat(this.props.data.total_tax_gw_st, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_gw_cgst!==0?<li><span>Processing CGST</span>  {utility.PriceFormat(this.props.data.total_tax_gw_cgst, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_gw_sgst!==0?<li><span>Processing SGST</span>  {utility.PriceFormat(this.props.data.total_tax_gw_sgst, this.props.data.currency)}</li>:null}
                           {this.props.data.total_tax_gw_igst!==0?<li><span>Processing IGST</span>  {utility.PriceFormat(this.props.data.total_tax_gw_igst, this.props.data.currency)}</li>:null}
                           {this.props.data.total_fee_irctc_conc!==0?<li className="font-w-b t-color1"><span className="t-color1">IRCTC concession ( - )</span>  {utility.PriceFormat( -1* this.props.data.total_fee_irctc_conc, this.props.data.currency)}</li>:null}
                           
                        </ul>
                    </div>
                </Popup>
            </div>
        );
    }
}
export default TrainFareBreakUp;