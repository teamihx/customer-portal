import React, { Component } from "react";
import Icon from "Components/Icon";
import { connect } from "react-redux";
import ToggleDisplay from "react-toggle-display";
import { Link } from "react-router-dom";
import log from "loglevel";
export const contextPath = process.env.contextpath;
import ChangeBkgStatusClsTxn from "../generic/ChangeBkgStatusClsTxn";
import Tags from "Pages/trips/generic/Tags.jsx";
import ShowHide from "Components/ShowHide.jsx";
import TripGenericService from "../../../services/trips/generic/TripGenericService";
import Button from "Components/buttons/button.jsx";
import TrainPricingDetail from "../train/TrainPricingDetail.jsx";
import TrainEmailDetails from "../train/TrainEmailDetails.jsx";
import TrainSMS from "../train/TrainSMS.jsx";
export const TRIPS_ROLES = "tripsRoles";
import Domainpath from '../../../services/commonUtils/Domainpath';
class TrainSideBar extends Component {
  constructor(props) {
    super(props);
    this.redirect = this.redirect.bind(this);
    this.state = {
      tripid: this.props.tripDetail.trip_ref,
      tripDetail: this.props.tripDetail,
      show: false,
      loading: false,
      openTxnAvailable:false,
      emailDetailsShow: false,
      currentClick:'',
      ticketUrl : Domainpath.getHqDomainPath()+'/ticket',
      smsDetailsShow: false
    };
    this.checkIfOpenTxn();
    let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
    this.state.trainTripsRoles=tripsObj.trainTripsRoles;
  }

  checkIfOpenTxn = () => {
    this.state.openTxnAvailable = TripGenericService.checkForOpenTxnAvailable();
    this.renderComponents = this.renderComponents.bind(this)
  };

renderComponents(event){
    try {
    event.preventDefault();
    this.state.currentClick = event.target.id;
    if (this.state.currentClick === 'trainEmailDtls'){
        this.setState({emailDetailsShow: !this.state.emailDetailsShow});
    }else if (this.state.currentClick === 'trainSMSDtls'){
      this.setState({smsDetailsShow: !this.state.smsDetailsShow});
    }
  } catch (err) {
    log.error("Exception occured in Footer redirect function---" + err);
  }
  } 

  redirect(event) {
    try {
      event.preventDefault();
      let url = "";
      var domainpath = Domainpath.getHqDomainPath();
      if (event.target.id === "lossTracker") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/loss_tracker/";
      } else if (event.target.id === "xml") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/xml";
      } else if (event.target.id === "printinvoice") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/invoice?tax_invoice=true";
      }else if (event.target.id === "e_receipt") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/invoice?e_receipt=true";
      }else if (event.target.id === "cancel") {
        url = domainpath + "/hq/trips/" + this.state.tripid + "/cancel";
      }

      window.open(url);
    } catch (err) {
      log.error("Exception occured in Footer redirect function---" + err);
    }
  }
  render() {
    var domainpath = Domainpath.getHqDomainPath();
    const tripJsonUrl =
      contextPath + "/trips/" + this.state.tripDetail.trip_ref + "/json";
    const tripXMl =
    domainpath + "/hq/trips/" + this.state.tripid + "/xml";
    const loss_track_url =
    Domainpath.getHqDomainPath() +
      "/hq/trips/" +
      this.state.tripid +
      "/loss_tracker/";
      const cancel_Trip =Domainpath.getHqDomainPath() +"/hq/trips/" +this.state.tripDetail.trip_ref +"/cancel";
    return (
      <>
        <div className="side-pnl">
          <ShowHide visible="true" title="Tips, tools & extras">
            <div className="showHide-content">
              <ul className="mb-20">
          
        {this.state.trainTripsRoles.TRAIN_SEND_EMAIL_ROLE_ENABLE && 
        <li className="pl-20 ">
        <a id="trainEmailDtls" onClick={this.renderComponents} className={this.state.currentClick === "trainEmailDtls" ? "active" : ""}>
        <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Email Trip Details</a>
        <ToggleDisplay show={this.state.emailDetailsShow}>
          <TrainEmailDetails />
        </ToggleDisplay>	
        </li>            
        }
        {this.state.trainTripsRoles.TRAIN_SEND_SMS_ROLE_ENABLE && 
        <li className="pl-20 ">
        <a id="trainSMSDtls" onClick={this.renderComponents} className={this.state.currentClick === "trainSMSDtls" ? "active" : ""}>
        <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>SMS Trip Details</a>
        <ToggleDisplay show={this.state.smsDetailsShow}>
          <TrainSMS />
        </ToggleDisplay>	
        </li>            
        }

			  <li className="pl-20 ">
        <form id="eticketForm" target="_blank" action={this.state.ticketUrl} method="post">
				<input type="hidden" name="confirmation_number" value={this.state.tripDetail.trip_ref} />
				<input type="hidden" name="last_name" value={this.state.tripDetail.contact_detail.last_name} />	
				 <Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>			
				<input className="btn btn-text link-Color no-text-decr" type="submit" value="&nbsp;Print E-Tickets"/>		
				</form>          
          </li>
        {/* Print e-receipt for this trip */}
			  <li className="pl-20 "><a id="e_receipt" href="/" onClick={this.redirect} title="Print e-receipt">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print E-Receipt</a></li>
        
            {/* Print Tax invoice for this trip */}
        {this.state.trainTripsRoles.TRAIN_PRINT_INVOICE_ROLE_ENABLE &&    
			  <li className="pl-20 "><a id="printinvoice" href="/" onClick={this.redirect} title="Print tax invoice">
				<Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Print Invoice</a></li>
        }
                {/* Loss Tracker For trip */}
                {this.state.trainTripsRoles.TRAIN_LOSS_TRACKER_ROLE_ENABLE && 
                <li className="pl-20 ">
                  <a
                    id="lossTracker"
                    href={loss_track_url}
                    target="_blank"
                    onClick={this.redirect}
                    title=" Loss Tracker For trip"
                  >
                    <Icon
                      className="arrow rotate90 mt--3 "
                      color="#36c"
                      size={10}
                      icon="down-arrow"
                    />
                    Loss Tracker For trip
                  </a>
                </li>
                  }
                {/* Trip XML */}
                {this.state.trainTripsRoles.TRAIN_TRIP_XML_ROLE_ENABLE && 
                <li className="pl-20 ">
                  <a
                    id="xml"
                    href={tripXMl}
                    target="_blank"
                    onClick={this.redirect}
                    title="Trip XML"
                  >
                    <Icon
                      className="arrow rotate90 mt--3 "
                      color="#36c"
                      size={10}
                      icon="down-arrow"
                      title="Trip XML"
                    />
                    Trip XML
                  </a>
                </li>
              }         
                {/* Trip JSON */}
                {this.state.trainTripsRoles.TRAIN_TRIP_JSON_ROLE_ENABLE &&
                <li className="pl-20 ">
                  <a
                    id="json"
                    href={tripJsonUrl}
                    target="_blank"
                    title="Trip JSON"
                  >
                    <Icon
                      className="arrow rotate90 mt--3 "
                      color="#36c"
                      size={10}
                      icon="down-arrow"
                    />
                    Trip JSON
                  </a>
                </li>
                }
              </ul>
              {/* Change Booking status Closes Txn */}
              {this.state.trainTripsRoles.TRAIN_CHANGE_BKNG_STS_CLOSE_TXN_ROLE_ENABLE &&
              <ChangeBkgStatusClsTxn tripId={this.state.tripDetail.trip_ref} />
              }
              {this.state.trainTripsRoles.TRAIN_CANCEL_TRIP_ROLE_ENABLE &&
              <div>
              {this.props.tripDetail.train_bookings[0].booking_status==='P'?
                <div className="amndmnt mt-10 pl-20 pr-20">            
                    <a
                      id="cancel"
                      href={cancel_Trip}
                      className="btn btn-default btn-xs"
                      target="_blank"
                      onClick={this.redirect}
                      title="Cancel Trip"
                    >                
                      Cancel Trip
                    </a>
                    </div>:''} 
               </div>
                }   
            </div>
          </ShowHide>
          {this.state.trainTripsRoles.TRAIN_TAG_ROLE_ENABLE &&
          <Tags />
          }
          {this.state.trainTripsRoles.TRAIN_PRICING_DETAILS_ROLE_ENABLE  &&
          <TrainPricingDetail />
          }
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};
export default connect(mapStateToProps, null)(TrainSideBar);
