import React, { Component } from "react";
import { Link } from "react-router-dom";
import Popup from "reactjs-popup";
import utility from "../../common/Utilities";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";

import Tooltip from "react-tooltip-lite";

import Icon from "Components/Icon";
import TrainFareBreakUp from "./TrainFareBreakUp";
//import * as actions from '../../../store/actions/index'

class TrainBookingDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      trainToPaxMap: {},
      bearthType: {
        LB: "Lower",
        MB: "Middle",
        UB: "Upper",
        SL: "Side Lower",
        SM: "Side Middle",
        SU: "Side Upper",
        WS: "Window Seat",
        NC: "No Choice"
      },
      bookingClass: {
        "1A": "First AC",
        "2A": "AC 2 Tier",
        FC: "First",
        "3A": "AC 3 Tier",
        "3E": "AC 3 Economy",
        CC: "AC Chair Car",
        SL: "Sleeper",
        "2S": "Second Sitting",
        STD: "Standard",
        CMF: "Comfort",
        CPL: "Comfort Plus",
        DLX: "Deluxe",
        "3AC": "3 Tier AC",
        EC1: "Economy",
        EC2: "Economic"
      },
      trinCnfrmStatus: {
        CNF: "Confirmed",
        AC: "RAC",
        WL: "WL",
        RQWL: "RQWL",
        RLWL: "RLWL",
        CKWL: "CKWL",
        GNWL: "GNWL",
        PQWL: "PQWL"
      },
      trainPaxType: {
        ADT: "Adult",
        CHD: "Child",
        INF: "Child",
        SCM: "Senior Male",
        SCF: "Senior Female"
      }
    };
    this.currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;
  }
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  travellerCount = trainBkng => {
    //let trainroute=trainBkng.train_routes[0];
    let pax_info = trainBkng.pax_infos;
    let adtCount = 0;
    let chdCount = 0;
    let infCount = 0;
    let senCount = 0;
    for (let pax of pax_info) {
      if (pax.pax_type_code === "ADT") {
        adtCount += 1;
      } else if (pax.pax_type_code === "CHD") {
        chdCount += 1;
      } else if (pax.pax_type_code === "INF") {
        infCount += 1;
      } else if (pax.pax_type_code === "SCF" || pax.pax_type_code === "SCM") {
        senCount += 1;
      }
    }
    let adtTravel =
      adtCount > 0 ? adtCount + (adtCount == 1 ? " Adult  " : " Adults  ") : "";
    let childTravel =
      chdCount > 0
        ? chdCount + (chdCount == 1 ? " Child  " : " Children  ")
        : "";
    let infTravel =
      infCount > 0
        ? infCount + (infCount == 1 ? " Infant  " : " Infants  ")
        : "";
    let senTravel =
      senCount > 0
        ? senCount +
          (senCount == 1 ? " Senior Citizen  " : " Senior Citizens  ")
        : "";
    return adtTravel + childTravel + infTravel + senTravel;
  };

  loadTrainBookingDtl = () => {
    let trainBkng = this.props.tripDetail.train_bookings[0];
    let booking_info_list = trainBkng.train_routes[0].trains[0].booking_info_list.sort(
      (a, b) => (a.seq_no > b.seq_no ? 1 : b.seq_no > a.seq_no ? -1 : 0)
    );
    for (let info of booking_info_list) {
      if(info.booking_status!=='P' || info.confirmation_status==='CNF'){
        info.status_to_display=this.props.bkngSatatusMaster[info.booking_status];
      }else{
        info.status_to_display=this.state.trinCnfrmStatus[
          info.confirmation_status
        ];
      }
      for (let fare of trainBkng.train_fares) {
        if (info.train_fare_id === fare.id) {
          fare.currency =
            this.props.tripDetail.currency === "INR"
              ? "Rs"
              : this.props.tripDetail.currency;
          info.train_fare = { ...fare };
        }
      }
    }
    this.setState({
      trainToPaxMap: {
        ...this.state.trainToPaxMap,
        ...trainBkng
      }
    });
  };
  componentDidMount() {
    this.loadTrainBookingDtl();
  }
  render() {
    return (
      <div>
        {!this.isEmpty(this.state.trainToPaxMap)
          ? this.state.trainToPaxMap.train_routes.map(routes => {
              return routes.trains.map(train => {
                return (
                  <React.Fragment key={routes.id}>
                    <div className="heading dis-flx-btw mb-5">
                      <h4 className="in-tabTTl-sub">
                        <Icon
                          className="noticicon"
                          color="#36c"
                          size={15}
                          icon="train"
                        />{" "}
                        Booking details{" "}
                        <span className="t-color3 font-14">
                          {" "}
                          {this.travellerCount(
                            this.props.tripDetail.train_bookings[0]
                          )}
                        </span>
                      </h4>
                      <div className="trip-sub">
                        <ul className="trip-sub-info">
                          <li>
                            <label>{train.train_name} :</label>
                            <span> {train.train_number}</span>
                          </li>
                          <li>
                            <label>PNR :</label>
                            <span>
                              {" "}
                              {train.booking_info_list[0].pnr_number}{" "}
                            </span>
                          </li>
                          <li>
                            <label>Ticket# </label>
                            <span>
                              {" "}
                              {train.booking_info_list[0].ticket_number}
                            </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="resTable">
                      <table className="dataTable3">
                        <thead>
                          <tr>
                            <th width="30%">Pax Name</th>
                            <th width="10%">Age</th>
                            <th width="15%">Status</th>
                            <th width="15%">Seat</th>
                            <th width="15%">Class</th>
                            <th width="15%">Price</th>
                          </tr>
                        </thead>
                        <tbody>
                          {train.booking_info_list.map(info => (
                            <tr  className={
                              info.status_to_display ===
                                "CANCELLED" ||
                                info.status_to_display ===
                                "REFUNDED"
                                ? "des cancel"
                                : null
                            } key={info.id}>
                              <td>
                                <span>
                                  {info.pax_info.title}{" "}
                                  {info.pax_info.first_name}{" "}
                                  {info.pax_info.last_name} (
                                  {
                                    this.state.trainPaxType[
                                      info.pax_info.pax_type_code
                                    ]
                                  }
                                  )
                                </span>
                              </td>
                              <td>
                                <span>
                                  {info.pax_info.age}
                                </span>
                              </td>
                              <td>
                                <span className="n-strk">
                                 {info.status_to_display !== "FAILED" ?
                                 <>{info.status_to_display}                                   
                                 </>:'BOOKING FAILED'}
                                </span>
                              </td>
                              <td>
                                <span>
                                {info.coach_number !==null && info.berth_type !==null ?
                                  <>{info.coach_number}-{info.seat_number}{" "}                                   
                                  ({this.state.bearthType[info.berth_type]})</>:'-'}
                                </span>
                              </td>

                              <td>
                                <span>
                                  {this.state.bookingClass[info.booking_class]}
                                </span>
                              </td>
                              <td>
                                <span className="lnkLine">
                                  {typeof info.train_fare !== "undefined" ? (
                                    <TrainFareBreakUp data={info.train_fare} />
                                  ) : (
                                    "..."
                                  )}
                                </span>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>{" "}
                  </React.Fragment>
                );
              });
            })
          : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airPortMaster: state.trpReducer.airPortMaster,
    airLineMaster: state.trpReducer.airLineMaster,
    ctConfigData: state.trpReducer.ctConfigData,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster,
    InsuranceMasterData: state.trpReducer.InsuranceMasterData
  };
};
export default connect(mapStateToProps, null)(TrainBookingDetail);
