import React, { Component } from 'react';
import { connect } from 'react-redux';
import log from 'loglevel';
import utility from '../../common/Utilities'
import ShowHide from 'Components/ShowHide.jsx';
import * as actions from '../../../store/actions/index';

class TrainPricingDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
         pricingDetail:{}
        }
        
    }
    isEmpty = (obj) => {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }
      loadPricingDetail=(trainBkng)=>{
          
        
     //let trainroute=trainBkng.train_routes[0];
     let pax_info=trainBkng.pax_infos
     let adtCount=0;
     let chdCount=0;
     let infCount=0;
     let senCount=0;
     for(let pax of pax_info){
         if(pax.pax_type_code==='ADT'){
          adtCount+=1
         }else if(pax.pax_type_code==='CHD'){
          chdCount+=1
         }else if(pax.pax_type_code==='INF'){
          infCount+=1
        }else if(pax.pax_type_code==='SCF' || pax.pax_type_code==='SCM'){
          senCount+=1
        }
     }
     let adtBaseFare=0
    let total_fee_irctc_conc_adt=0;
    let total_base_fare_adt=0
    let chdBaseFare=0
    let total_fee_irctc_conc_chd=0;
    let total_base_fare_chd=0
    let infBaseFare=0
    let total_fee_irctc_conc_inf=0;
    let total_base_fare_inf=0
    let senBaseFare=0
    let total_fee_irctc_conc_sen=0;
    let total_base_fare_sen=0;
    let total_cashback=0;
    let total_discount=0;
    let total_fee_irctc_rf=0;
    let total_fee_irctc_sf=0;
    let total_fee_irctc_svf=0;
    let total_fee_irctc_oc=0;
    let total_fee_irctc_tc=0;
    let total_tax_irctc_st=0;
    //let total_fee_gw=0;
    let rlw_charge=0;
    let total_tax_gw_cgst=0;
    let total_tax_gw_st=0
    let total_tax_gw_igst=0;
    let txn_fee=0;
    let total_tax_ct_st=0;
    let total_tax_ct_cgst=0;
    let total_tax_ct_sgst=0;
    let total_fee_gw=0;
    //let total_tax_ct_igst=0;
    let total_tax_ct_igst=0;
    let total_fee_ct_svf=0;
    let total_ct_fee=0;
    let total_tax_gw_sgst=0;
    let total_fare=0;
    let total_fee_irctc_cc=0;
      for(let train of trainBkng.train_routes[0].trains){
           for(let info of train.booking_info_list){
            for(let fare of trainBkng.train_fares ){
             if(fare.id===info.train_fare_id){
            total_fee_gw+=fare.total_fee_gw
            total_cashback+=fare.total_cashback
            total_discount+=fare.total_discount
            total_fare+=fare.total_fare
            total_fee_ct_svf+=fare.total_fee_ct_svf;
            total_fee_irctc_rf+=fare.total_fee_irctc_rf;
            total_fee_irctc_sf+=fare.total_fee_irctc_sf;
            total_fee_irctc_svf+=fare.total_fee_irctc_svf;
            total_fee_irctc_oc+=fare.total_fee_irctc_oc;
            total_fee_irctc_tc+=fare.total_fee_irctc_tc;
            total_tax_irctc_st+=fare.total_tax_irctc_st;
            total_tax_gw_cgst+=fare.total_tax_gw_cgst;
            total_tax_gw_igst+=fare.total_tax_gw_igst;
            total_tax_gw_st+=fare.total_tax_gw_st;
            total_tax_ct_cgst+=fare.total_tax_ct_cgst;
            total_tax_ct_igst+=fare.total_tax_ct_igst;
            total_tax_ct_sgst+=fare.total_tax_ct_sgst;
            total_tax_ct_st+=fare.total_tax_ct_st;
            total_tax_gw_sgst+=fare.total_tax_gw_sgst;
            total_fee_irctc_cc+=fare.total_fee_irctc_cc;
           }
        }
            if(info.pax_info.pax_type_code==='ADT'){
                for(let fare of trainBkng.train_fares ){
                    if(fare.id===info.train_fare_id){
                        total_fee_irctc_conc_adt+=fare.total_fee_irctc_conc
                        total_base_fare_adt+=fare.total_base_fare
                    }
                }
            }
            if(info.pax_info.pax_type_code==='CHD'){
                for(let fare of trainBkng.train_fares ){
                    if(fare.id===info.train_fare_id){
                        total_fee_irctc_conc_chd+=fare.total_fee_irctc_conc
                        total_base_fare_chd+=fare.total_base_fare
                    }
                }
            }
            if(info.pax_info.pax_type_code==='INF'){
                for(let fare of trainBkng.train_fares ){
                    if(fare.id===info.train_fare_id){
                        total_fee_irctc_conc_inf+=fare.total_fee_irctc_conc
                        total_base_fare_inf+=fare.total_base_fare
                    }
                }
            }
            if(info.pax_info.pax_type_code==='SCM' || info.pax_info.pax_type_code==='SCF'){
                for(let fare of trainBkng.train_fares ){
                    if(fare.id===info.train_fare_id){
                        total_fee_irctc_conc_sen+=fare.total_fee_irctc_conc
                        total_base_fare_sen+=fare.total_base_fare
                    }
                }
            }
            
           }
          
         
      }
      txn_fee=total_tax_gw_cgst+total_tax_gw_igst+total_tax_gw_sgst+total_tax_gw_st+total_fee_gw;
      total_ct_fee=total_tax_ct_cgst+total_tax_ct_igst+total_tax_ct_st+total_tax_ct_sgst+total_fee_ct_svf;
      adtBaseFare=total_fee_irctc_conc_adt+total_base_fare_adt;
      chdBaseFare=total_fee_irctc_conc_chd+total_base_fare_chd;
      infBaseFare=total_fee_irctc_conc_inf+total_base_fare_inf;
      senBaseFare=total_fee_irctc_conc_sen+total_base_fare_sen ; 
      total_fare=total_fare
      rlw_charge=total_fee_irctc_rf+total_fee_irctc_sf+total_fee_irctc_svf+total_fee_irctc_oc+total_fee_irctc_tc+total_tax_irctc_st+total_fee_irctc_cc;    
    let pricingObj={}
    pricingObj={
        txn_fee:txn_fee,
        total_ct_fee:total_ct_fee,
        adtBaseFare:adtBaseFare,
        chdBaseFare:chdBaseFare,
        infBaseFare:infBaseFare,
        senBaseFare:senBaseFare,
        rlw_charge:rlw_charge,
        total_cashback:total_cashback,
        total_discount:total_discount,
        total_fare:total_fare,
        adtCount:adtCount,
        chdCount:chdCount,
        infCount:infCount,
        senCount:senCount,
        currency:this.props.tripDetail.currency==='INR'?'Rs':this.props.tripDetail.currency
      }
      this.setState({
        pricingDetail: {
               ...this.state.pricingDetail,
               ...pricingObj
              }
   },()=>console.log("this.state.pricingDetail===="+JSON.stringify(this.state.pricingDetail)));
    }
    componentDidMount(){
      this.loadPricingDetail(this.props.tripDetail.train_bookings[0])
    }
      
render() {
    //{this.loadPricingDetail(this.props.tripDetail.train_bookings[0])}
    return (
     <>
     {!this.isEmpty(this.state.pricingDetail)? 
      <div className="side-pnl">
        <ShowHide visible="true" title="Pricing Details">
        <div className="showHide-content">
                <ul className="price-info pb-10">
                 {this.state.pricingDetail.adtBaseFare!==0?<li><span>Adult</span>  <span>{utility.PriceFormat(this.state.pricingDetail.adtBaseFare ,this.state.pricingDetail.currency) }<em className="d-b font-12 mt-4 t-color3">({utility.PriceFormat(this.state.pricingDetail.adtBaseFare/this.state.pricingDetail.adtCount,this.state.pricingDetail.currency)} *{this.state.pricingDetail.adtCount})</em></span></li>:''}
                 { this.state.pricingDetail.chdBaseFare!==0?<li><span>Child</span>  <span>{utility.PriceFormat(this.state.pricingDetail.chdBaseFare ,this.state.pricingDetail.currency) }<em className="d-b font-12 mt-4 t-color3">({utility.PriceFormat(this.state.pricingDetail.chdBaseFare,this.state.pricingDetail.currency)}*{this.state.pricingDetail.chdCount} )</em></span></li>:''}
                 {this.state.pricingDetail.infBaseFare!==0?<li><span>Infant</span>  <span>{utility.PriceFormat(this.state.pricingDetail.infBaseFare ,this.state.pricingDetail.currency) }<em className="d-b font-12 mt-4 t-color3">({utility.PriceFormat(this.state.pricingDetail.infBaseFare,this.state.pricingDetail.currency)} *{this.state.pricingDetail.infCount})</em></span></li>:''}
                 { this.state.pricingDetail.senBaseFare!==0?<li><span>Senior Citizen</span>  <span>{utility.PriceFormat(this.state.pricingDetail.senBaseFare,this.state.pricingDetail.currency)}<em className="d-b font-12 mt-4 t-color3">({utility.PriceFormat(this.state.pricingDetail.senBaseFare,this.state.pricingDetail.currency)} *{this.state.pricingDetail.senCount})</em></span></li>:''}
                 {this.state.pricingDetail.total_cashback!==0?<li><span>Cash Back (-)</span> <span>{utility.PriceFormat(-1*this.state.pricingDetail.total_cashback,this.state.pricingDetail.currency)}</span></li>:''}
                 {this.state.pricingDetail.total_discount!==0?<li><span>Discount (-)</span> <span>{utility.PriceFormat(-1*this.state.pricingDetail.total_discount,this.state.pricingDetail.currency)}</span></li>:''}
                 { this.state.pricingDetail.txn_fee!==0?<li><span>Transaction Fee</span>  <span>{utility.PriceFormat(this.state.pricingDetail.txn_fee,this.state.pricingDetail.currency)}</span></li>:''} 
                 {this.state.pricingDetail.total_ct_fee!==0?<li><span>Cleartrip Fee</span>  <span>{utility.PriceFormat(this.state.pricingDetail.total_ct_fee,this.state.pricingDetail.currency)}</span></li>:''}
                 {this.state.pricingDetail.rlw_charge!==0?<li><span>Railway Charges</span>  <span>{utility.PriceFormat(this.state.pricingDetail.rlw_charge,this.state.pricingDetail.currency)}</span></li>:''} 

                 { this.state.pricingDetail.total_fare!==0?<li className="total"><span>Total</span> <span> {utility.PriceFormat(this.state.pricingDetail.total_fare,this.state.pricingDetail.currency)}</span></li>:''}     
                </ul>
                </div>
                </ShowHide>
                </div>:null}
               
           </>
    );
  }
}
const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail,
      //airLineMaster: state.trpReducer.airLineMaster
    };
  }
  const mapDispatchToProps = dispatch => {
    return {
        
        onInitPricingObject: (req) => dispatch(actions.initPricingObject(req))
        
        
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TrainPricingDetail);