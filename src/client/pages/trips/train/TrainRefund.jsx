import React, { Component } from 'react'
import { connect } from 'react-redux'
import CommonMsg from "Components/commonMessages/CommonMsg.jsx";
import log from 'loglevel';
import NumberUtils from '../../../services/commonUtils/NumberUtils.js';

class TrainRefund extends Component {
    constructor(props) {
        super(props)
        this.state = {
            responseData1: this.props.tripDetail,
			refundInfoList:[],
			refundTxn : '0',
			totalRevarsal :'0',
			totalSup :'0',
			totalCt :'0',
			totalOtherCharge: '0',		
			totalTotalRefund: '0'
        }
        this.calculateTotalRefund = this.calculateTotalRefund.bind(this);
    }

    UNSAFE_componentWillMount(){
       this.calculateTotalRefund();
    }

    calculateTotalRefund() {		
        this.state.refundInfoList =[];
        try{
           if(this.state.responseData1.txns!==undefined){
            for(let refundTxns of this.state.responseData1.txns){
                 if(refundTxns.train_refund_records!==undefined){
                    for(let train_refund_rcds of refundTxns.train_refund_records){
                    let train_refund_rcd = new Object();
                    train_refund_rcd=train_refund_rcds;
                    let train_cancellationsdata = refundTxns.train_cancellations.filter( function (traincncl) {
                        var refundRecrdId = parseInt(traincncl.train_refund_record_id, 10);
                        return refundRecrdId === train_refund_rcds.id
                      });

                      let bookingInfoId = train_cancellationsdata[0].train_booking_info_id;
                      var bookingInfoIds = parseInt(bookingInfoId, 10);
                      let bookInfos = this.state.responseData1.train_bookings[0].train_booking_infos.filter( function (trainbookInfo) {
                      //  console.log('Test1   '+trainbookInfo.id);
                      //  console.log('Test2   '+bookingInfoId);
                        return trainbookInfo.id === bookingInfoIds
                      });
                      let paxId = bookInfos[0].pax_info_id;
                      let paxInfos = this.state.responseData1.train_bookings[0].pax_infos.filter( function (paxInfo) {
                        return paxInfo.id === paxId
                      });
                      let name = paxInfos[0].first_name + ' ' +paxInfos[0].last_name;
                      train_refund_rcd.name = name;
                    this.state.refundInfoList.push(train_refund_rcd);
                    this.state.totalRevarsal = Number(this.state.totalRevarsal) + Number(train_refund_rcds.total_reversal);		
                    this.state.totalSup = Number(this.state.totalSup) + Number(train_refund_rcds.total_sup_charge);		
                    this.state.totalCt = Number(this.state.totalCt) + Number(train_refund_rcds.total_ct_charge);
                    this.state.totalOtherCharge = Number(this.state.totalOtherCharge) + Number(train_refund_rcds.total_oth_charge);
                    this.state.totalTotalRefund = Number(this.state.totalTotalRefund) + Number(train_refund_rcds.refund_amount);
                    //console.log("Train Refund List Size : "+this.state.refundInfoList.length);
                }
            }
           }
        }
        }catch(err){
        log.error('Exception occured in calculateTotalRefund function---'+err);
      }
        
          }

    render() {
        return (
            <>
            
            {this.state.refundInfoList.length!==0 && (
            <div>
            <h4 className="in-tabTTl-sub mb-10">Refund Computation details</h4>
			<span className="mb-10 d-b font-18">Booking Details</span>
			<div className="overflow-x-auto">
			<table className="dataTblPaddingR pax-tbl dataTable5 scrlTable">
                  <thead><tr>
				<th width="7%">Pax</th>
                <th width="7%">Reversal  (+)</th>
                <th width="7%">Supplier (-)</th>
                <th width="6%">Cleartrip (-)</th>
                <th width="7%">Other (-)</th>
				<th width="6%">Refund</th>
                </tr></thead>
                  <tbody>
				{this.state.refundInfoList.map(refundInfos => {					
			  return(
				<>
				 <tr>
				 	<td><span>{refundInfos.name}</span></td>
                    <td><span>Rs. {NumberUtils.numberFormat(refundInfos.total_reversal)} </span></td>
                    <td><span>Rs. {NumberUtils.numberFormat(refundInfos.total_sup_charge)} </span></td>
					<td><span>Rs. {NumberUtils.numberFormat(refundInfos.total_ct_charge)} </span></td>
					<td><span>Rs. {NumberUtils.numberFormat(refundInfos.total_oth_charge)} </span></td>
                    <td><span>Rs. {NumberUtils.numberFormat(refundInfos.refund_amount)} </span></td>
                  </tr>
				</>
			);
			})}
			<tr>
				<td colSpan="1"><strong className="font-14">Total</strong></td>
				<td>Rs. {NumberUtils.numberFormat(this.state.totalRevarsal)}</td>
                <td>Rs. {NumberUtils.numberFormat(this.state.totalSup)}</td>
                <td>Rs. {NumberUtils.numberFormat(this.state.totalCt)}</td>
				<td>Rs. {NumberUtils.numberFormat(this.state.totalOtherCharge)}</td>
				<td>Rs. {NumberUtils.numberFormat(this.state.totalTotalRefund)}</td>
			</tr>
                </tbody>
                </table>
				</div>
            </div>
            )}
            {this.state.refundInfoList.length===0 && 
              <CommonMsg errorType="datanotFound" errorMessageText="No Refunds information for this trip" />
             }
    </>
        )
    }
}
const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail   
     };
}
export default connect(mapStateToProps, null)(TrainRefund);
