import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Popup from "reactjs-popup"
import utility from '../../common/Utilities'
import { connect } from 'react-redux';
import dateUtil from '../../../services/commonUtils/DateUtils';
export const TRIPS_ROLES = "tripsRoles";

import * as actions from '../../../store/actions/index'
import TrainItineraryDetail from './TrainItineraryDetail';

import TrainBookingDetail from './TrainBookingDetail';
//import * as actions from '../../../store/actions/index'


class TrainTripDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
            //bkngStatuses:bkngStatMaster,
           //cbinTypeMaster:cabinTypeMaster,
            tripDtls :this.props.tripDetail,
           
            
           
        }
     
    }
    
    
     
    
    
    isEmpty = (obj) => {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }
    getTravellers=(travellers)=>{
        let pricingObject=new Object();
         let adultCount =0;
         let childCount =0;
         let infantCount =0;
        
            travellers = travellers.trimRight();
           let travellerString=travellers.substring(travellers.trimRight().indexOf('|')+1, travellers.length);
           let ttravellerArr=travellerString.trimLeft().split('\n')
            
           for(let travel of ttravellerArr){
               if(travel.includes('INF')){
                infantCount=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
               if(travel.includes('ADT')){
                adultCount=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
               if(travel.includes('CHD')){
                childCount=travel.substring(travel.indexOf(' ')+1,travel.length)
               }
           }
          let adtTravel= adultCount>0 ?adultCount +(adultCount==1 ? " Adult  ":" Adults  "):""
          let childTravel= childCount>0 ?childCount +(childCount==1 ? " Child  ":" Children  "):""
          let infTravel= infantCount>0 ?infantCount +(infantCount==1 ? " Infant  ":" ,Infant  "):""
          return adtTravel + childTravel + infTravel
        }
    
   
      
    render() {
        
        
            
    
       return(
           
        <div>   
       {!this.isEmpty(this.state.tripDtls)?
        <>
          <TrainItineraryDetail data={this.state.tripDtls}/>
          <TrainBookingDetail data={this.state.tripDtls}/>
          

           </>:null}
       </div>
       )
       
         
}

}
const mapStateToProps = state => {
    return {
        tripDetail: state.trpReducer.tripDetail,
        airPortMaster: state.trpReducer.airPortMaster,
        airLineMaster: state.trpReducer.airLineMaster,
        InsuranceStatusMasterData:state.trpReducer.InsuranceStatusMasterData,
        bkngSatatusMaster:state.trpReducer.bkngSatatusMaster,
        paymentStatusMaster:state.trpReducer.paymntStatusMaster,
        paymentTypeMaster:state.trpReducer.paymentTypeMaster,
        cabinTypeMaster:state.trpReducer.cabinTypeMaster,
        InsuranceMasterData:state.trpReducer.InsuranceMasterData
    };
}
const mapDispatchToProps = dispatch => {
    //console.log("mapDispatchToProps");
    return {
      
        
        updateUserDetails: (req) => dispatch(actions.updateUserDetails(req)),
        
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(TrainTripDetails);