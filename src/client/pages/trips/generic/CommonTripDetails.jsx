import React, { Component } from "react";
import Header from "Pages/common/Header";
import Footer from "Pages/common/Footer";
import Messages from "Pages/common/Messages";
import DomainMessages from "Pages/common/DomainMessages";

import Icon from "Components/Icon";

import { Link } from "react-router-dom";
import { connect } from "react-redux";

import DateUtils from "../../../services/commonUtils/DateUtils";

export const TRIPS_ROLES = "tripsRoles";

import Domainpath from "../../../services/commonUtils/Domainpath";
class CommonTripDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tripid: this.props.tripDetail.trip_ref,
      // contact_detail: {},

      bookedBy: "",
      flag: "",
      usermasterdetails: this.props.usermasterdetails,
      ftripsRoles: "",
      htripsRoles: "",
      flagLogo: "https://" + process.env.s3bucket,
      checktrips: true
    };

    this.findProdType = this.findProdType.bind(this);
    //console.log('param:' + this.state.tripid)
    //console.log("product" + this.props.tripDetailSt.trip_type)
    //this.loadData(this.state.tripid);
    this.currentClick = "tripDetails";
    this.buttonClicked = false;
    this.req = {
      tripId: this.state.tripid
    };
    this.userclassDetailsresponse = "";
    this.airlineReq = {
      fileType: "airlines"
    };
    this.airPortReq = {
      fileType: "airports"
    };
    this.country = this.countryReq = {
      country: this.country
    };
  }
  findProdType = () => {
    //console.log("triptype ===" + JSON.stringify(this.props.tripDetailSt));

    //const exp = new RegExp(/[a-z]{1,15}/, 'i');
    //const found = travellers.match(exp);
    //let travelArr =travellers.split(/\b\s+/);
    //console.log("sdcsdc" + travellers);
    switch (this.props.tripDetail.trip_type) {
      case 1:
        return "Air";
        break;
      case 2:
        return "Hotel";
        break;
      case 3:
        return "Package";
        break;
      case 4:
        return "Trains";
        break;
      case 16:
        return "Local";
        break;

      default:
        break;
    }
  };

  findTripType = () => {
    //var travellers = this.props.tripDetailSt.travellers;
    switch (this.props.tripDetail.trip_type) {
      case 1:
        return "flight-one";
        break;
      case 2:
        return "hotel-one";
        break;
      case 3:
        return "flight-hotel-one";
        break;
      case 4:
        return "train-one";
        break;
      case 16:
        return "Activities";
        break;

      default:
        break;
    }
  };

  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  getDomain = domain => {
    let country = null;
    //console.log('domain===' + JSON.stringify(this.props.tripDetailSt));
    if (typeof domain !== "undefined") {
      let frm = domain.lastIndexOf(".") + 1;
      // let countryCd=domain.substring(frm,domain.length-1);
      let country = domain.substring(frm, domain.length);
      //console.log("Country===" + country)
      if (country === "com") {
        country = "in";
      }
      //console.log("Country===" + country)
      return country;
    }
  };
  loadUsrDetail = () => {
    let first_name = "";
    let last_name = "";
    let title = "";
    console.log("user detail====" );
    //this.isEmpty(this.props.usermasterdetails)?this.loadUsrDetail():null

    if (
      !this.isEmpty(this.props.usermasterdetails) &&
      this.props.usermasterdetails.status !== "404"
    ) {
      //let usrDtl = this.props.usermasterdetails[this.props.tripDetailSt.user_id]
      //console.log("trip user Detail" + JSON.stringify(usrDtl));

      let usrDtl = this.props.usermasterdetails[
        this.props.tripDetail.booked_user_id
      ];
      // console.log("trip user Detail" + JSON.stringify(usrDtl));

      if (
        typeof usrDtl !== "undefined" &&
        typeof usrDtl.personal_data !== "undefined"
      ) {
        title =
          usrDtl.personal_data.title !== null
            ? usrDtl.personal_data.title + ". "
            : "";
        first_name =
          usrDtl.personal_data.first_name !== null
            ? usrDtl.personal_data.first_name
            : "";
        last_name =
          usrDtl.personal_data.last_name !== null
            ? usrDtl.personal_data.last_name
            : "";
      }
    }

    return title + " " + first_name + " " + last_name;
  };

  redirectUserAccount = (usrId, e) => {
    e.persist();
    let url = "";
    //console.log("e.target.id +" + JSON.stringify(usrId))

    url = Domainpath.getHqDomainPath() + "/hq/people/" + usrId;

    window.open(url);
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.usermasterdetails !== prevState.usermasterdetails) {
      console.log("componentWillReceiveProps+");
      return {
        usermasterdetails: nextProps.usermasterdetails
      };
    } else {
      return null;
    }
  }

  componentDidMount() {
    this.props.loadTripDetail();
  }
  render() {
    //console.log("Logo===" + this.state.flagLogo)
    return this.props.usrDtlUpdated ? (
      <section>
        <div className="main-container pageTtl pt-10 pb-10">
          <ul>
            <li className="trip-Type">
              <div>
                <h1 className="font-12 t-color2 txt-transfUp">Trip ID</h1>
                <p className="t-color1 font-18 font-w-b">
                  {this.props.tripDetail.trip_ref}
                </p>
              </div>
              <div title={this.findProdType()}>
                <Icon
                  className="noticicon"
                  color="#36c"
                  size={35}
                  icon={this.findTripType()}
                />
              </div>
            </li>
            <li>
              <h2 className="font-12 t-color2 txt-transfUp">Trip Name</h2>
              <p className="t-color1 font-18 font-w-b">
                {this.props.tripDetail.trip_name}&nbsp;
                <img
                  src={
                    this.state.flagLogo +
                    "/images/flags/mini/" +
                    this.getDomain(this.props.tripDetail.domain) +
                    ".gif"
                  }
                />
              </p>
            </li>

            <li>
              <h2 className="font-12 t-color2 txt-transfUp">Booking Date</h2>
              <p className="t-color1 font-18 font-w-b">
                {DateUtils.prettyDate(
                  this.props.tripDetail.created_at,
                  "MMM DD YYYY ,hh:mm A"
                )}
              </p>
            </li>
            <li>
              <h2 className="font-12 t-color2 txt-transfUp">Booked by</h2>
              <p className="t-color1 font-18 font-w-b">
                <Link
                  to={this.state.tripid}
                  id="redirectUserID"
                  onClick={e =>
                    this.redirectUserAccount(
                      this.props.tripDetail.booked_user_id,
                      e
                    )
                  }
                >
                  {this.loadUsrDetail()}
                </Link>
              </p>
            </li>
          </ul>
        </div>
      </section>
    ) : null;
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airLineMaster: state.trpReducer.airLineMaster,
    airPortMaster: state.trpReducer.airPortMaster,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster,
    usermasterdetails: state.trpReducer.usermasterdetails
  };
};

export default connect(mapStateToProps, null)(CommonTripDetails);
