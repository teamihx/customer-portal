import React, { Component } from 'react'
import { connect } from 'react-redux';
import Icon from 'Components/Icon';
import * as actions from '../../../store/actions/index'
import Button from 'Components/buttons/button.jsx'
import TripGenericService from '../../../services/trips/generic/TripGenericService';
import Notification from "Components/Notification.jsx";

class ChangeBkgStatusClsTxn extends Component {

    constructor(props) {
        super(props)
        this.state = {
            tripId: this.props.tripId,
			chgMsg: '',
			failChgMsg: '',
			loading:false

        }
	this.closeTransaction = this.closeTransaction.bind(this),
	this.req={
		tripId:this.props.tripId
	}
    }

	closeTransaction(event){
		try{
			this.setState({closeTxnLdr:true});
			const req = {
	            tripRefNumber: this.state.tripId
	        }
			TripGenericService.changeBkgStatsClsTxn(req).then((valid) => {
	    if (valid) {
		// console.log('changeBkgStatsClsTxn'+JSON.stringify(valid));
			if (valid !== undefined && valid !== "") {
              let response = valid;
             // console.log("description: " + response.description);
              if (response.description.includes("Success")) {
			   this.setState({ chgMsg:true, showAlert: true, closeTxnLdr:false}); 
				
			   this.props.onInitTripDtl(this.req)
			   
              } else {
				this.setState({ failChgMsg:true, showAlert: true, closeTxnLdr:false}); 
              }
            }else{
				this.setState({ failChgMsg:true, showAlert: true, closeTxnLdr:false}); 
	
			}
 			
	 	
		// window.location.reload();
		}
	 	
			});
			 
			
		}catch(err){	
	            log.error('Exception occured in Footer redirect function---'+err);
				this.setState({closeTxnLdr:false});
	     }
		
	}
	toggleAlert = () => {
		this.setState({
		  showAlert: false
		});
	  };
    render() {
		let viewType, messageText;
		if (this.state.failChgMsg) {
		  viewType = "error";
		  messageText = "Operation is not Successful"
		} else if (this.state.chgMsg) {
		  viewType = "success";
		  messageText = "Operation is Successful."
		}
        return (

           <div className="pl-20 pr-20">

			{viewType && messageText && (
            <Notification
			  viewType={viewType}
			  viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
		  )}
		
		     <Button
				size="sm"
				id="closeTxn"
				className="w-100p"
                viewType="default"
                onClickBtn={this.closeTransaction}
                loading = {this.state.closeTxnLdr}
                btnTitle='Change Status & Close Txn'
              >
                
              </Button>	
			

		</div>

        )
    }


}

const mapDispatchToProps = dispatch => {
    return {
        
        onInitTripDtl: (req) => dispatch(actions.initTripDtl(req)),
        
    }
}
export default connect(null,mapDispatchToProps)(ChangeBkgStatusClsTxn);



