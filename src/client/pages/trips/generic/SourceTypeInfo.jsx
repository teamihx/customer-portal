import React, { Component } from 'react';
import { connect } from 'react-redux';
import Icon from 'Components/Icon';
import ShowHide from 'Components/ShowHide.jsx';
import TripGenericService from '../../../services/trips/generic/TripGenericService'

class SourceTypeInfo extends Component {
    constructor(props) {
        super(props);
       
        this.state = {
            
        };
	this.loadTripDetail = this.loadTripDetail.bind(this);
       
    }

	loadTripDetail=()=>{
      let userName='';
      if(!TripGenericService.isEmpty(this.props.usermasterdetails) && this.props.usermasterdetails.status!=='404'){

         let usrDtl=this.props.usermasterdetails[this.props.tripDetail.user_id]
         //console.log("trip user Detail"+JSON.stringify(usrDtl));
         if(typeof usrDtl!=='undefined'){
            userName=usrDtl.username
         }
      }
        return userName;
    }

   
    render() {
      
        return (
            <>
                <div className="side-pnl">
				{this.props.tripDetail.txn_source_type==='CORP' &&
                 <>
                <ShowHide title="Corporate Booking">
                <div className="showHide-content">
                <div className="alert alert-info"><Icon className="noticicon" color="#365aa1" size={16} icon="info2"/>
                 Corp: {this.props.tripDetail.domain}
			    </div>
                </div>
                </ShowHide>
                </>
				}
				  {this.props.tripDetail.txn_source_type==='API' &&
                 <>
                <ShowHide title="API Booking ">
                <div className="showHide-content">
                <div className="alert alert-info"><Icon className="noticicon" color="#365aa1" size={16} icon="info2"/>
                 User: {this.loadTripDetail()}
			    </div>
                </div>
                </ShowHide>
                </>
				}
				{this.props.tripDetail.txn_source_type==='AGENCY' &&
                 <>
                <ShowHide title="Agency Booking">
                <div className="showHide-content">
                <div className="alert alert-info"><Icon className="noticicon" color="#365aa1" size={16} icon="info2"/>
                 User: {this.props.tripDetail.domain}
			    </div>
                </div>
                </ShowHide>
                </>
				}
				{this.props.tripDetail.txn_source_type==='WL' &&
                 <>
                <ShowHide title="WL Booking ">
                <div className="showHide-content">
                <div className="alert alert-info"><Icon className="noticicon" color="#365aa1" size={16} icon="info2"/>
                 User: {this.props.tripDetail.domain}
                 </div>
                 </div>
                </ShowHide>
                </>
				}
				{this.props.tripDetail.txn_source_type==='MOBILE_WL' &&
                 <>
                <ShowHide title="WL Booking ">
                <div className="showHide-content">
                <div className="alert alert-info"><Icon className="noticicon" color="#365aa1" size={16} icon="info2"/>
                 WL Partner: {this.props.tripDetail.domain}
                 </div>
                 </div>
                </ShowHide>
                </>
				}
                  
                </div>

            </>
        );
    }
}
const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail,
	  usermasterdetails:state.trpReducer.usermasterdetails
    };
  }
export default connect(mapStateToProps)(SourceTypeInfo);

