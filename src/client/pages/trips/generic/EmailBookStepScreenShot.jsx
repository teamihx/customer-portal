import React, { Component } from "react";
import { connect } from "react-redux";
import EmailService from "../../../services/trips/email/EmailService";
import Notification from "Components/Notification.jsx";
import Button from "Components/buttons/button.jsx";
export const contextPath = process.env.domainpath;

class EmailBookStepScreenShot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseData1: this.props.tripDetail,
      email: this.props.tripDetail.contact_detail.email,
      showSucessMessage: false,
      showFailMessage: false,
      message: false,
      loading: false
    };

    //console.log("constructor");
    this.sendStepScreenShot = this.sendStepScreenShot.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getMessage = this.getMessage.bind(this);
  }

  componentWillMount() {
    //console.log("componentWillMount");
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  sendStepScreenShot = () => {
    this.setState({ sendScreenShot: true });
    const { responseData1 } = this.state;
    let email = new Object();
    email.to = this.state.email;
    email.tripRefNumber = responseData1.trip_ref;
    email.type = "BOOK_STEP_SCREENSHOT";
    email.data = responseData1.contact_detail.last_name;

    EmailService.sendEmails(email)
      .then(valid => {
        if (valid) {
         // console.log("send Emails : " + JSON.stringify(valid));
          if (valid !== undefined && valid !== "") {
            let response = valid;
            //console.log("description: " + response.description);
            if (response.description.includes("success")) {
              this.setState({ showSucessMessage: true, sendScreenShot: false, showAlert: true });
            } else {
              this.setState({ showFailMessage: true, sendScreenShot: false, showAlert: true });
            }
            return;
          }
        }
      })
      .catch(err => {});
  };

  getMessage(type) {
    let msg = "";
    if (type === "success") {
      msg =
        "We have sent Bookstep Screenshots in an email to " + this.state.email;
    } else if (type === "error") {
      msg =
        "We are not able to send a mail with Bookstep Screenshots details. You can try again at some other time ";
    }
    return msg;
  }
  toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  render() {
	let viewType, messageText;
     if (this.state.showFailMessage) {
      viewType = "error";
      messageText = this.getMessage("error");
    } else if (this.state.showSucessMessage) {
      viewType = "success";
      messageText = this.getMessage("success");
    }
    return (
      <>
        <div>
        {viewType && messageText && (
            <Notification
              viewType={viewType}
              viewPosition="fixed"
              messageText={messageText}
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}

          <div className="form-group">
            <div className="row">
              <div className="col-9">
                <input
                  type="text"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-3 pl-0 mob-txt-right">
                <Button
                  size="xs"
                  viewType="primary"
                  onClickBtn={this.sendStepScreenShot}
                  loading={this.state.sendScreenShot}
                  btnTitle="Send"
                >
                  {" "}
                </Button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail
  };
};

export default connect(mapStateToProps, null)(EmailBookStepScreenShot);
