import React, { Component } from 'react';
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import ShowHide from 'Components/ShowHide.jsx';
import log from 'loglevel';
import * as actions from '../../../store/actions/index'
import { Link } from 'react-router-dom';
import loader1 from 'Assets/images/loader1.gif';
import FlightService from '../../../services/trips/flight/FlightService.js'
import Notification from "Components/Notification.jsx";
export const TRIPS_ROLES = 'tripsRoles'
import Domainpath from '../../../services/commonUtils/Domainpath';
class TripAmendment extends Component {
    constructor(props) {
        super(props);
        this.redirect=this.redirect.bind(this);
        this.processRollBackAmend=this.processRollBackAmend.bind(this);
        //this.handleAmendment=this.handleAmendment.bind(this);
        this.state = {
            OfflineAmendLinkEnable:false,
            amendTripLinkEnable:false,
            roleBackAmendAvail:false,
            showAmendSuccessMsg:false,
            showAmendFailMsg:false,
            loadLoader:false,
            onlineAmendAvail:false,
            partialEligibleAmend:false,
            callOfflineAmend:true,
            callRollbackAmend:true,
            tripsRoles:''
        };
        this.req = {
            tripId: this.props.tripDetail.trip_ref
        }
        //Getting ROLES DATA from local storage
        var trips=localStorage.getItem(TRIPS_ROLES);
        let tripsObj=JSON.parse(trips);
        this.state.tripsRoles=tripsObj.ftriplinks;
    }
    // componentDidMount(){
    //     this.props.onInitCTConfigDtl(this.req);
    // }

    redirect(event){
        try{
        event.preventDefault();
        let url="";
        var domainpath = Domainpath.getHqDomainPath();
        if(event.target.id === 'amendmnt'){
            url = domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/amend_air/"        
        }if(event.target.id === 'onlineamendmnt'){
            url =domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/reschedule/intro"
        }if(event.target.id === 'amendId'){
            url =domainpath+"/hq/trips/"+this.props.tripDetail.trip_ref+"/amend"
        }if(event.target.id === 'newonlineamendmnt'){
            url =domainpath+"/amendments/hq/select-dates/"+this.props.tripDetail.trip_ref
        }
        window.open(
            url
          );
        }catch(err){
            log.error('Exception occured in Footer redirect function---'+err);
        }

    }

    checkPartialAmendEligibleFlight(){
        try{
          let partialEnable=false;
          let flights=[];
          let pricingObjIdList=[];
          flights=this.state.tripDetail.air_bookings[0].flights;
          if(flights.length!==1){
            partialEnable=true;
          }else{
            for(let flight of flights){
              for(let segment of flight.segments){
                  this.state.tripDetail.air_bookings[0].air_booking_infos.map((air, sidx) => {
                  if(air.segment_id!==null && segment.segment_id!==null){
                     if(air.segment_id===segment.segment_id){
                      pricingObjIdList.push(air.pricing_object_id);
                     }
                    }
                   });
                if(pricingObjIdList){
                  let pricingUniqList = [...new Set(pricingObjIdList)];
                  if(pricingObjIdList.length!==pricingUniqList.length){
                    partialEnable=true;
                  }
                }
              }
            }
          }
          if(this.props.tripDetail.journey_type==="R" && partialEnable){
            this.state.partialEligibleAmend=true;
          }
    
        }catch(err){
            log.error('Exception occured in checkPartialAmendEligibleFlight function---'+err);
        }
       }
    
      

    checkOnlineAmendment(){
        try{
            let flexiPayBooking=false;
            let futureFlights=[];
             trxnAmends = this.state.tripDetail.air_bookings[0].air_booking_infos.map((air, sidx) => {
             if(air.status_reason!==null && status_reason!==undefined){
                if(air.status_reason.uniq==="E" && this.props.tripDetail.booking_status==="L"){
                 flexiPayBooking=true;
                }
                return air;
               }
              });

            if(!this.props.tripDetail.booking_status==="R" && !this.props.tripDetail.booking_status==="H" && !flexiPayBooking){
                this.state.onlineAmendAvail=true;
                
            }else if(this.props.tripDetail.trip_type === 1){
                var onlineAmend=this.props.ctConfigRespData.data['ct.hq.online_amend_valid_req_domains'];
                if(!onlineAmend.includes(this.props.tripDetail.domain)){
                    this.state.onlineAmendAvail=true;
                }else{
                    let checkPartialAmendAvail=false;
                    this.checkPartialAmendEligibleFlight();
                    if(this.state.partialEligibleAmend){
                        for(let flight of this.state.tripDetail.air_bookings[0].flights){
                          const date = DateUtils.convertStringToDate(flight.departure_date_time);
                              if(date > this.state.currentDate){
                                futureFlights.push(flight);
                              }
                        }
                        let suppliers=[];
                        if(futureFlights){
                          for(let flt of futureFlights){
                              for(let seg of flt.segments){
                                suppliers.push(seg.supplier);
                              }
                          }
                          if(suppliers){
                            var atc_partial_amend_eligible=this.props.ctConfigRespData.data['ct.rails.partial_amend_eligible_airlines'];
                           if(suppliers.length===1 && suppliers.includes("AMD")){
                              if(atc_partial_amend_eligible==="ON"){
                                checkPartialAmendAvail=true;
                              }
                           }else{
                            let opAirCode=[];
                            let amendAvail=false;
                            var partailAmendEligibleAirlines=this.props.ctConfigRespData.data['ct.rails.partial_amend_eligible_airlines'];
                            if(partailAmendEligibleAirlines){
                              for(let flight of this.state.tripDetail.air_bookings[0].flights){
                                for(let seg of flight.segments){
                                  opAirCode.push(seg.marketing_airline);
                                }
                              }
                              if(opAirCode.includes(partailAmendEligibleAirlines)){
                                amendAvail=true;
                              }
                              if(this.props.tripDetail.booking_status==="P" && amendAvail){
                                checkPartialAmendAvail=true;
                              }
                            }
                           }
                          }
              
                        }
                      }
                }

            let trxnAmends='';
            let checkTrxnOpen='';
            let checPaxkInfAvail='';
            let multiCityavail=false;
            let fullyConfirmedList='';
            let fullyConfirmed=true;
            let isAmedAvailable=true;
            if(this.props.tripDetail.txns){
              trxnAmends = this.props.tripDetail.txns.map((trxn, sidx) => {
               if(trxn.txn_type!==null && trxn.txn_type!==undefined && trxn.status!==null && trxn.status!==undefined){
                 if(trxn.txn_type===2){
                     return trxn;
                 }
                 }
             });

             checkTrxnOpen = this.props.tripDetail.txns.map((trxn, sidx) => {
                if(trxn.txn_type!==null && trxn.txn_type!==undefined && trxn.status!==null && trxn.status!==undefined){
                  if(trxn.status==="O"){
                      return trxn;
                  }
                  }
              });
           }
          
           if(this.props.tripDetail.air_bookings[0].pax_infos){
            checkInfAvail = this.props.tripDetail.air_bookings[0].pax_infos.map((pax, sidx) => {
                if(pax.pax_type_code==="INF"){
                      return pax;
                  }
              });
           }
            if(this.props.tripDetail.trip_type === 1){
                const flifhtsSize=this.props.tripDetail.air_bookings[0].flights.length;
                if(flifhtsSize>=2){
                    multiCityavail=true;
                }
            }
            if(this.props.tripDetail.trip_type === 1){
                fullyConfirmedList = this.props.tripDetail.air_bookings[0].map((air, sidx) => {
                    if(air.booking_status==="F"){
                          return air;
                      }
                  });
              if(!fullyConfirmedList || !this.props.tripDetail.booking_status==="P"){
                fullyConfirmed=false;
              }
            }else if(this.props.tripDetail.trip_type === 2){
                const cancelbookingstatus=process.env.cancelbookingstatus;
                  if(this.props.tripDetail.hotel_bookings.booking_status==="H" || this.props.tripDetail.hotel_bookings.booking_status==="Z" || !cancelbookingstatus.includes(this.props.tripDetail.hotel_bookings.booking_status)){
                    fullyConfirmed=false;
                  }
            }
            if(this.props.tripDetail.trip_type===1){
                let checkGdsPnr='';
                let suppliers=[];
                checkGdsPnr = this.props.tripDetail.air_bookings[0].air_booking_infos.map((air, sidx) => {
                    if(air.gds_pnr!==null && air.gds_pnr!==undefined){
                          return air;
                      }
                  });
                  for(let flt of this.props.tripDetail.air_bookings[0].flights){
                  for(let seg of flt.segments){
                    suppliers.push(seg.supplier);
                  }
                }
                  const isGds=process.env.checksuppliers;
                  if(isGds.includes(suppliers) || checkGdsPnr){
                    isAmedAvailable=true;
                  }else{
                    var amendAllowedAirlines=this.props.ctConfigRespData.data['ct.air.amendment_allowed_airlines'];
                    isAmedAvailable=true;
                  }
    
            }
        if(this.props.tripDetail.booking_status==="P" && checkPartialAmendAvail && !trxnAmends && !checkTrxnOpen
            && !checPaxkInfAvail && !multiCityavail && fullyConfirmed && isAmedAvailable){
                this.state.onlineAmendAvail=true;
            }
        }
        }catch(err){
          log.error('Exception occured in checkOnlineAmend function---'+err);
      }
      }

    checkOfflineAmendment(){
        try{
            this.setState({ callOfflineAmend:false});
            var OffAmend=this.props.ctConfigRespData.data['ct.air.hq.is_air_offline_amend_enabled'];
            if(OffAmend!==null && OffAmend!==undefined){
                   if(OffAmend==="ON"){
                    let trxnAmends='';
                   if(this.props.tripDetail.txns){
                     trxnAmends = this.props.tripDetail.txns.map((trxn, sidx) => {
                      if(trxn.txn_type!==null && trxn.txn_type!==undefined && trxn.status!==null && trxn.status!==undefined){
                        if(trxn.txn_type===2 && trxn.status==="O"){
                            return trxn;
                        }
                        }
                    });
                  }
                if(!trxnAmends){
                    this.state.OfflineAmendLinkEnable=true;
                }else{
                if(this.state.tripsRoles.OFFLINE_AMEND_ROLE_ENABLE && this.props.tripDetail.booking_status==="P"){
                    this.state.OfflineAmendLinkEnable=true;
                }
                }
                }else{
                  if(this.state.tripsRoles.AMEND_TRIP_ROLE_ENABLE && this.props.tripDetail.booking_status==="P"){
                    this.state.amendTripLinkEnable=true;
                  }
                }
              }
        }catch(err){
            log.error('Exception occured in checkOfflineAmendment function---'+err);
        }
    }

    checkRollbackAmend(){
        try{
            this.setState({ callRollbackAmend:false});
            let trxnAmends='';
            if(this.props.tripDetail.txns){
                trxnAmends = this.props.tripDetail.txns.map((trxn, sidx) => {
                 if(trxn.txn_type!==null && trxn.txn_type!==undefined && trxn.status!==null && trxn.status!==undefined){
                   if(trxn.txn_type===2 && trxn.status==="O"){
                       return trxn;
                   }
                   }
               });
             }
            if(trxnAmends){
            this.state.roleBackAmendAvail=true;
            }
        }catch(err){
            log.error('Exception occured in checkOfflineAmendment function---'+err);
        }
    }

    processRollBackAmend(){
        try{
          this.setState({ loadLoader:true});
          //("processRollBackAmend start "+this.props.tripDetail.trip_ref);
          FlightService.processRollbackAmend(this.props.tripDetail.trip_ref).then(response =>{
            var obj=JSON.parse(response.data);
            //console.log("processRollBackAmend response: "+JSON.stringify(response.data));
            if (obj !== undefined && obj !== "" && obj.responsecode !== undefined && obj.responsecode !== "" && obj.responsecode === 200) {
               // console.log("processRollBackAmend  Successfully "+obj.responsecode);
                this.setState({ showAmendSuccessMsg:true, showAlert: true}); 
              }else{
                this.setState({ showAmendFailMsg:true, showAlert: true}); 
              }
              this.setState({ loadLoader:false});
          });
        }catch(err){
          log.error('Exception occured in processRollBackAmend function---'+err);
          this.setState({ showAmendFailMsg:true, loadLoader:false}); 
        }
      
      }
      toggleAlert = () => {
        this.setState({
          showAlert: false
        });
      };
    render() {
        if(this.state.callOfflineAmend){
            this.checkOfflineAmendment();
        }
        if(this.state.callRollbackAmend){
            this.checkRollbackAmend();
        }

        let viewType, messageText;
        if (this.state.showAmendSuccessMsg) {
          viewType = "success";
          messageText = "Rollback amendment processed Successfully";
        } else if (this.state.showAmendFailMsg) {
          viewType = "error";
          messageText = "Rollback amendment can not be performed on this trip. Please refresh the page";
        }
        return (
            <>
            <div className="App">         
            {this.props.tripDetail!==null? 
            <div className="side-pnl amndmnt">
            <ShowHide title="Amendment">
            <div className="showHide-content">
            {viewType && messageText && (
            <Notification
              viewType={viewType}
              messageText={messageText}
              viewPosition="fixed"
              showAlert={this.state.showAlert}
              toggleAlert={this.toggleAlert}
            />
          )}
            <ul>
            {this.props.tripDetail.trip_type === 1 && this.state.OfflineAmendLinkEnable && (
              <li><a id="amendmnt"  href="/amend_air" target="_blank" onClick={this.redirect} title="Offline Amend"><Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Offline Amend</a></li>
            )}
           
           {this.props.tripDetail.trip_type === 1 && this.state.tripsRoles.ONLINE_AMEND_ROLE_ENABLE && (
           <li><a id="onlineamendmnt" href="/reschedule/intro" target="_blank" onClick={this.redirect} title="Online Amendment"><Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>Online Amendment</a></li>
           )}
		   {this.props.tripDetail.trip_type === 1 && this.state.tripsRoles.ONLINE_AMEND_ROLE_ENABLE && (
           <li><a id="newonlineamendmnt" href="/amendments/hq/select-dates" target="_blank" onClick={this.redirect} title="New Online Amendment"><Icon className="arrow rotate90 mt--3 " color="#36c" size={10} icon="down-arrow"/>New Online Amendment</a></li>
           )}

           {/* {this.props.tripDetail.trip_type === 1 && this.state.roleBackAmendAvail && this.state.tripsRoles.ROLL_BACK_AMEND_ROLE && (
           <Link className="pl-20" id="rollbackId" title="Rollback Amendment" onClick={ () => this.processRollBackAmend() } >
                <Icon className="arrow rotate90 mt--3" color="#36c" size={10} icon="down-arrow"/>Rollback Amendment {this.state.loadLoader && (
               <img 
                src={loader1}
                className="loader" 
                alt="loader"  
                width="12px"
                />
              )}
              </Link>
              )} */}

           </ul>
           </div>
            </ShowHide>
           </div>: ''}
          </div>

            </>
        );
    }
}
const mapStateToProps = state => {
    return {
      tripDetail: state.trpReducer.tripDetail,
      ctConfigRespData:state.trpReducer.ctConfigData
    };
  }
  const mapDispatchToProps = dispatch => {
    return {
        onInitCTConfigDtl: (req) => dispatch(actions.initCTConfigData(req)),
    }
  }
export default connect(mapStateToProps,mapDispatchToProps)(TripAmendment);

