import React, { Component } from 'react'
import ReactJson from 'react-json-view'
import CustomScroll from 'react-custom-scroll';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index'

class TripJson extends Component {

    constructor(props) {
        super(props)
        this.req = {
            tripId: this.props.match.params.tripId
        }
    }

    componentDidMount() {
        this.props.onInitTripDtl(this.req);
    }
    
    render() {     
        return (
            <>
                <br/>
                <div className="jsonSec bg-c-gr">
                <CustomScroll heightRelativeToParent="calc(100% - 20px)">
                  <ReactJson src={this.props.tripDetailsData} />
                </CustomScroll> 
                </div>
            </>

        )
    }
}

const mapStateToProps = state => {
    return {
        tripDetailsData: state.trpReducer.tripDetail
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onInitTripDtl: (req) => dispatch(actions.initTripDtl(req)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TripJson);