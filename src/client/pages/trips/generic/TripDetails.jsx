import React, { Component } from "react";
import Header from "Pages/common/Header";
import Footer from "Pages/common/Footer";
import Messages from "Pages/common/Messages";
import DomainMessages from "Pages/common/DomainMessages";

import Icon from "Components/Icon";
import ErrorPage from "Pages/common/ErrorPage.jsx";

//import FlightTripDtls from '../flight/FlightTripDtls'

//import FlightTripDtls from '../flight/FlightTripDtls'

import FlightTripDetails from "../flight/FlightTripDetails";
import HotelTripDtls from "../hotel/HotelTripDtls";
import PaymentDetail from "../payment/PaymentDetail";
import Notes from "../notes/Notes";
import AuditTrail from "../audit/AuditTrail";
import FlightUpdateTicketNumber from "../flight/FlightUpdateTicketNumber";
import FlightUpdatePricing from "../flight/FlightUpdatePricing";
import FlightOfflineRefund from "../flight/FlightOfflineRefund";
import FlightRefund from "../flight/FlightRefund";
import HotelRefund from "../hotel/HotelRefund";
import FlightPax from "../flight/FlightPax";
import FPHPax from "../../fphTrips/pax/FPHPax.jsx";
import FlightRightSidebar from "Pages/trips/flight/FlightRightSidebar.jsx";
import HotelRightSidebar from "Pages/trips/hotel/HotelRightSidebar.jsx";
import FPHRightSidebar from "Pages/trips/fph/FPHRightSidebar.jsx";
import CommonMsg from "Components/commonMessages/CommonMsg.jsx";
import PageLoad from "../../common/PageLoad.jsx";
import ContactDetails from "Pages/trips/generic/ContactDetails.jsx";

import ActivityTripDetails from "Pages/trips/activities/ActivityTripDetails.jsx";
import ActivityRightSidebar from "Pages/trips/activities/ActivityRightSidebar.jsx";
import ActivityRefund from "Pages/trips/activities/ActivityRefund.jsx";

import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import TripStatus from "../flight/TripStatus";
import DateUtils from "../../../services/commonUtils/DateUtils";
import FlightService from "../../../services/trips/flight/FlightService";
import UserFetchService from "../../../services/trips/user/UserFetchService";
import WalletPromo from "../generic/WalletPromo";
import MiscTripInfo from "../generic/MiscTripInfo.jsx";
import TrainTripDetails from "../train/TrainTripDetails";
import TrainSideBar from "../train/TrainSideBar";
export const TRIPS_ROLES = "tripsRoles";
import TrainRefund from "../train/TrainRefund";
import CommonTripDetails from "./CommonTripDetails";
let fphRS = require("Assets/json/data.json");
import Domainpath from '../../../services/commonUtils/Domainpath';
import Notification from '../../../components/Notification'; 
class TripDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tripid: this.props.match.params.tripId,
      // contact_detail: {},
      content: "",
      isActive: false,
      productType: "",
      bookedBy: "",
      flag: "",
      userClassDtl: {},
      ftripsRoles: "",
      htripsRoles: "",
      flagLogo: "https://" + process.env.s3bucket,
      checktrips: true,
      usrDtlUpdated:false,
      cancelSucMsg:'',
      showAlert:false
    };
    
    const msg = localStorage.getItem("msg_value");
    if(msg!==null && msg!==undefined && msg!==''){
    this.state.cancelSucMsg=msg;
    this.state.showAlert=true;
    localStorage.removeItem("msg_value");

    }
    
    
    
    this.renderComponents = this.renderComponents.bind(this);

    this.findProdType = this.findProdType.bind(this);
    //console.log('param:' + this.state.tripid)
    //console.log("product" + this.props.tripDetailSt.trip_type)
    //this.loadData(this.state.tripid);
    this.currentClick = "tripDetails";
    this.buttonClicked = false;
    this.req = {
      tripId: this.props.match.params.tripId
    };
    this.userclassDetailsresponse = "";
    this.airlineReq = {
      fileType: "airlines"
    };
    this.airPortReq = {
      fileType: "airports"
    };
    this.country = this.countryReq = {
      country: this.country
    };
  }

  createTripsRoles() {
    //Getting ROLES DATA from local storage
    let tripsObj = JSON.parse(localStorage.getItem(TRIPS_ROLES));
    if (tripsObj !== null){
      this.state.checktrips = false;
      this.state.ftripsRoles = tripsObj.ftrips;
      this.state.htripsRoles = tripsObj.htrips;
      this.state.localtripsRoles = tripsObj.localtripsRoles;
      this.state.trainTripsRoles = tripsObj.trainTripsRoles;
    } 
     
      // console.log('trainTripsRoleslllllllllllllllllll' + this.state.trainTripsRoles);
      
    }
  

  renderComponents(e, tripTab) {
    if (e === null && tripTab !== null) {
      this.currentClick = tripTab;
    } else {
      this.buttonClicked = true;
      this.currentClick = e.target.id;
    }
    //this.currentClick = e.target.id;
    // console.log("event==="+e);
    if (this.currentClick === "pax") {
      this.state.content = <FlightPax />;
    } else if (this.currentClick === "notes") {
      this.state.content = <Notes />;
    } else if (this.currentClick === "paymentDetails") {
      //{this.loadDataPaymentData()};
      this.state.content = <PaymentDetail />;
    } else if (this.currentClick === "audittrail") {
      this.state.content = <AuditTrail />;
    } else if (
      this.currentClick !== undefined &&
      this.currentClick !== "" &&
      this.currentClick === "updateTktNum"
    ) {
      this.state.content = <FlightUpdateTicketNumber />;
    } else if (
      this.currentClick !== undefined &&
      this.currentClick !== "" &&
      this.currentClick === "updatePricing"
    ) {
      this.state.content = <FlightUpdatePricing />;
    } else if (
      this.currentClick !== undefined &&
      this.currentClick !== "" &&
      this.currentClick === "offlineRefund"
    ) {
      this.state.content = <FlightOfflineRefund />;
    } else if (this.currentClick === "refunds") {
      this.state.content = <FlightRefund />;
    } else if (this.currentClick === "htlRefunds") {
      this.state.content = <HotelRefund />;
    } else if (this.currentClick === "actRefunds") {      
      this.state.content = <ActivityRefund />;
    }else if (this.currentClick === "tripStatus") {
      this.state.content = <TripStatus />;
    } else if (this.currentClick === "walletpromo") {
      this.state.content = <WalletPromo />;
    }else if (this.currentClick === "trainrefund") {
      this.state.content = <TrainRefund />;
    }
  }

  componentDidMount() {
    //{ this.getTripDetail(this.req) };
    //console.log('asdasd___' + this.airlineReq);
    //this.props.onInitAirPortMaster(this.airPortReq);
    this.props.onInitTripDtl(this.req);

    //this.setFlag(this.countryReq)
    //this.props.onInitAirLineMaster(this.airlineReq);
  }
  localTabsRoleBased = () => {
    if (!this.state.localtripsRoles.LOCAL_TRIP_ROLE_ENABLE) {
      if (this.state.localtripsRoles.LOCAL_NOTE_ROLE_ENABLE) {
        this.renderComponents(null, "notes");
      } else if (this.state.localtripsRoles.LOCAL_PAYMENT_ROLE_ENABLE) {
        this.renderComponents(null, "paymentDetails");
      } else if (this.state.localtripsRoles.LOCAL_AUDIT_ROLE_ENABLE) {
        this.renderComponents(null, "audittrail");
      } else if (this.state.localtripsRoles.LOCAL_REFUND_ROLE_ENABLE) {
        this.renderComponents(null, "actRefunds");
      } 
    }
  };
  hotelTabsRoleBased = () => {
    if (!this.state.htripsRoles.HTL_TRIP_ROLE_ENABLE) {
      if (this.state.htripsRoles.HTL_NOTE_ROLE_ENABLE) {
        this.renderComponents(null, "notes");
      } else if (this.state.htripsRoles.HTL_PAYMENT_ROLE_ENABLE) {
        this.renderComponents(null, "paymentDetails");
      } else if (this.state.htripsRoles.HTL_AUDIT_ROLE_ENABLE) {
        this.renderComponents(null, "audittrail");
      } else if (this.state.htripsRoles.HTL_REFUND_ROLE_ENABLE) {
        this.renderComponents(null, "htlRefunds");
      } else if (this.state.htripsRoles.HTL_WALLET_PRO_ROLE_ENABLE) {
        this.renderComponents(null, "walletpromo");
      }
    }
  };
  flightTabsRoleBased = () => {
    if (!this.state.ftripsRoles.FLT_TRIP_ROLE_ENABLE) {
      if (this.state.ftripsRoles.PAX_ROLE_ENABLE) {
        this.renderComponents(null, "pax");
      } else if (this.state.ftripsRoles.FLT_NOTE_ROLE_ENABLE) {
        this.renderComponents(null, "notes");
      } else if (this.state.ftripsRoles.FLT_PAYMENT_ROLE_ENABLE) {
        this.renderComponents(null, "paymentDetails");
      } else if (this.state.ftripsRoles.FLT_AUDIT_ROLE_ENABLE) {
        this.renderComponents(null, "audittrail");
      } else if (this.state.ftripsRoles.UPDATE_TKT_ROLE_ENABLE) {
        this.renderComponents(null, "updateTktNum");
      } else if (this.state.ftripsRoles.UPDATE_PRICE_ROLE_ENABLE) {
        this.renderComponents(null, "updatePricing");
      } else if (this.state.ftripsRoles.OFFLINE_REF_ROLE_ENABLE) {
        this.renderComponents(null, "offlineRefund");
      } else if (this.state.ftripsRoles.FLT_REFUND_ROLE_ENABLE) {
        this.renderComponents(null, "refunds");
      } else if (this.state.ftripsRoles.TRIP_STATUS_ROLE_ENABLE) {
        this.renderComponents(null, "tripStatus");
      } else if (this.state.ftripsRoles.FLT_WALLET_PRO_ROLE_ENABLE) {
        this.renderComponents(null, "walletpromo");
      }
    }
  };
  tabsBasedOnRole = buttonClicked => {
    if (this.props.tripDetailSt.trip_type === 1 && !buttonClicked) {
      this.flightTabsRoleBased();
    } else if (this.props.tripDetailSt.trip_type === 2 && !buttonClicked) {
      this.hotelTabsRoleBased();
    }else if (this.props.tripDetailSt.trip_type === 16 && !buttonClicked) {
      this.localTabsRoleBased();
    }else if (this.props.tripDetailSt.trip_type === 4 && !buttonClicked) {
      this.trainTabsRoleBased();
    }
  };

  trainTabsRoleBased = () => {
    if (!this.state.trainTripsRoles.TRAIN_TRIP_ROLE_ENABLE) {
      if (this.state.trainTripsRoles.TRAIN_NOTE_ROLE_ENABLE) {
        this.renderComponents(null, "notes");
      } else if (this.state.trainTripsRoles.TRAIN_PAYMENT_ROLE_ENABLE) {
        this.renderComponents(null, "paymentDetails");
      } else if (this.state.trainTripsRoles.TRAIN_AUDIT_ROLE_ENABLE) {
        this.renderComponents(null, "audittrail");
      } else if (this.state.trainTripsRoles.TRAIN_REFUND_ROLE_ENABLE) {
        this.renderComponents(null, "trainrefund");
      } 
    }
  };
  getComponent = (producType, buttonClicked) => {
    this.tabsBasedOnRole(buttonClicked);
    switch (producType) {
      //case 1: return this.currentClick==='tripDetails' ? <FlightTripDtls />:this.state.content;

      case 1:
        return this.currentClick === "tripDetails"
          ? this.props.tripDetailSt.trip_type === 1 &&
              this.state.ftripsRoles.FLT_TRIP_ROLE_ENABLE && (
                <FlightTripDetails />
              )
          : this.state.content;

      case 2:
        return this.currentClick === "tripDetails"
          ? this.props.tripDetailSt.trip_type === 2 &&
              this.state.htripsRoles.HTL_TRIP_ROLE_ENABLE && <HotelTripDtls />
          : this.state.content;
      case 3:
        return this.currentClick === "tripDetails"
          ? [<FlightTripDetails />, <HotelTripDtls />]
          : this.state.content;
      case 16:
        return this.currentClick === "tripDetails" ? this.props.tripDetailSt.trip_type === 16 &&
        this.state.localtripsRoles.LOCAL_TRIP_ROLE_ENABLE && (
          <ActivityTripDetails />
        ) : (
          this.state.content
        );
        case 4:
          return this.currentClick === "tripDetails" ? (
            <TrainTripDetails />
          ) : (
            this.state.content
          );
    }
  };
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  findProdType = () => {
    //console.log("triptype ===" + JSON.stringify(this.props.tripDetailSt));
    var travellers = this.props.tripDetailSt.travellers;
    //const exp = new RegExp(/[a-z]{1,15}/, 'i');
    //const found = travellers.match(exp);
    //let travelArr =travellers.split(/\b\s+/);
    //console.log("sdcsdc" + travellers);
    switch (this.props.tripDetailSt.trip_type) {
      case 1:
        return "Air";
        break;
      case 2:
        return "Hotel";
        break;
      case 3:
        return "Package";
        break;
      case 4:
        return "Trains";
        break;
      case 16:
          return "Local";
          break;  

      default:
        break;
    }
  };
  findTripType = () => {
    //var travellers = this.props.tripDetailSt.travellers;
    switch (this.props.tripDetailSt.trip_type) {
      case 1:
        return "flight-one";
        break;
      case 2:
        return "hotel-one";
        break;
      case 3:
        return "flight-hotel-one";
        break;
      case 4:
        return "train-one";
        break;    
      case 16:
          return "Activities";
          break;  

      default:
        break;
    }
  };
  getDomain = domain => {
    let country = null;
    //console.log('domain===' + JSON.stringify(this.props.tripDetailSt));
    if (typeof domain !== "undefined") {
      let frm = domain.lastIndexOf(".") + 1;
      // let countryCd=domain.substring(frm,domain.length-1);
      let country = domain.substring(frm, domain.length);
      //console.log("Country===" + country)
      if (country === "com") {
        country = "in";
      }
      //console.log("Country===" + country)
      return country;
    }
  };
  loadTripDetail = () => {
    //console.log("user detail====" + JSON.stringify(this.props.usermasterdetails));
    
    if (
      !this.isEmpty(this.props.tripDetailSt) &&
      this.props.tripDetailSt.msg === "SUCCESS"
    ) {
      if (
        this.isEmpty(this.props.usermasterdetails) ||
        typeof this.props.usermasterdetails === "undefined"
      ) {
        
        this.loadUsrDetail();
      }

      
    }
   
  };
  // loadContactDetail=()=>{

  //   if(!this.isEmpty(this.props.tripDetailSt)){
  //       this.state.contact_detail=this.props.tripDetailSt.contact_detail

  //   }

  // }
  setFlag = () => {
    this.getDomain(this.props.tripDetailSt.domain).then(res => {
      if (res !== "") {
        this.countryReq.country = res;
        FlightService.getFlag(this.countryReq).then(res =>
          console.log("Flags" + JSON.stringify(res))
        );
      } else {
        return null;
      }
    });
  };
  loadUsrDetail = () => {
    //console.log("Loyalty===" + this.props.tripDetailSt.loyalty_user_type)
    if (!this.isEmpty(this.props.tripDetailSt)) {
      let userIds = [];
      userIds.push(this.props.tripDetailSt.booked_user_id);
      const reqUsr = {
        person_ids: userIds,
        query: ["username", "personal_data"]
      };
      
      let userDetailsresponse = {};
      UserFetchService.fetchUserMailid(reqUsr).then(response => {
        
        userDetailsresponse = JSON.parse(response.data);
        /* console.log(
          "userDetailsresponse" + JSON.stringify(userDetailsresponse)
        ); */
        
        const usrClassReq = {
          username: userDetailsresponse[userIds[0]].username
        };
        //console.log("userIds===" + JSON.stringify(userDetailsresponse[userIds[0]]))
        UserFetchService.fetchUserClassDtl(usrClassReq).then(res => {
          // let userclassDetailsresponse = JSON.parse(res.data);
          this.userclassDetailsresponse = JSON.parse(res.data);
          //console.log("fetchUserClassDtl==="+JSON.stringify(userclassDetailsresponse))
          this.props.updateUserDetails(userDetailsresponse);
          this.setState({usrDtlUpdated:true},()=>console.log("userdtl==="+this.state.usrDtlUpdated))
        });
      });
    }
  };
  redirectUserAccount = (usrId, e) => {
    e.persist();
    let url = "";
    //console.log("e.target.id +" + JSON.stringify(usrId))

    url = Domainpath.getHqDomainPath() + "/hq/people/" + usrId;

    window.open(url);
  };
  loadUserClassDtl = () => {
    let userClassDtl = new Object();
    if (!this.isEmpty(this.userclassDetailsresponse)) {
      userClassDtl.custLevel =
        this.userclassDetailsresponse.userProfile &&
        this.userclassDetailsresponse.userProfile.customerLevel
          ? this.userclassDetailsresponse.userProfile.customerLevel
          : "L1";

      // userClassDtl.overallStatus =
      //   this.userclassDetailsresponse.userStatus.length > 0
      //     ? this.userclassDetailsresponse.userStatus[0].overallStatus
      //     : "";
      userClassDtl.overallStatus ="";
      userClassDtl.userStatus ="";
      for(let status of this.userclassDetailsresponse.userStatus){
        if(status.productType===this.findProdType()){
          userClassDtl.overallStatus=status.overallStatus;
          userClassDtl.userStatus=status.userStatus
        }
      }  
      // userClassDtl.userStatus =
      //   this.userclassDetailsresponse.userStatus.length > 0
      //     ? this.userclassDetailsresponse.userStatus[0].userStatus
      //     : "";
    }

    //console.log("Class Dtl===" + JSON.stringify(userClassDtl))
    return userClassDtl;
  };

  checktripRoles() {
    let roleAvailable = false;

    if (
      this.state.ftripsRoles.FLT_TRIP_ROLE_ENABLE ||
      this.state.htripsRoles.HTL_TRIP_ROLE_ENABLE ||
      this.state.ftripsRoles.PAX_ROLE_ENABLE ||
      this.state.ftripsRoles.FLT_NOTE_ROLE_ENABLE ||
      this.state.htripsRoles.HTL_NOTE_ROLE_ENABLE ||
      this.state.ftripsRoles.FLT_PAYMENT_ROLE_ENABLE ||
      this.state.htripsRoles.HTL_PAYMENT_ROLE_ENABLE ||
      this.state.ftripsRoles.FLT_AUDIT_ROLE_ENABLE ||
      this.state.htripsRoles.HTL_AUDIT_ROLE_ENABLE ||
      this.state.ftripsRoles.UPDATE_TKT_ROLE_ENABLE ||
      this.state.ftripsRoles.OFFLINE_REF_ROLE_ENABLE ||
      this.state.ftripsRoles.FLT_REFUND_ROLE_ENABLE ||
      this.state.htripsRoles.HTL_REFUND_ROLE_ENABLE ||
      this.state.ftripsRoles.FLT_WALLET_PRO_ROLE_ENABLE ||
      this.state.htripsRoles.HTL_WALLET_PRO_ROLE_ENABLE
    ) {
      roleAvailable = true;
    }

    if (!roleAvailable) {
      this.error = "accessDenied";
      this.msg = "Access denied";
    }
   
    return roleAvailable;
  }
  tripfound = () => {
    let tripDtl = this.props.tripDetailSt;
    let tripAvailable = true;
    /* console.log(
      "tripDtl==" + !this.isEmpty(tripDtl) + "hhj" + tripDtl.status !== 200
    ); */
    if (!this.isEmpty(tripDtl) && tripDtl.status !== 200) {
      tripAvailable = false;
    }
    if (!tripAvailable) {
      this.error = "datanotFound";
      this.msg = "Trip is not found";
    }
    return tripAvailable;
  };

  toggleAlert = () => {
    console.log('toggleAlert--')
    this.setState({
      showAlert: false
    });
  };

  render() {

       let viewType, messageText;
       if (this.state.cancelSucMsg !== null && this.state.cancelSucMsg !== undefined 
        && this.state.cancelSucMsg !=='' ) {
          viewType = "success";
          messageText = this.state.cancelSucMsg;
        }
    if (this.state.checktrips) {
      this.createTripsRoles();
    }

    //console.log("Logo===" + this.state.flagLogo)
    return (
      <>
        <Header />
        {viewType && messageText && (
        <Notification
          viewType={viewType}
          viewPosition="fixed"
          messageText={messageText}
          showAlert={this.state.showAlert}
          toggleAlert={this.toggleAlert}
        />
      )}
     
        {!this.isEmpty(this.props.tripDetailSt) ? (
          this.checktripRoles() && this.tripfound() ? (
            <>

            
             <CommonTripDetails usrDtlUpdated={this.state.usrDtlUpdated} loadTripDetail={this.loadTripDetail}/>
             {this.state.usrDtlUpdated ?<>

              <section className="topSec bg-c-gr">
                <div className="main-container">
                  <Messages />
                  <div className="row">
                    <div className="col-12">
                
                      <div className="mobTriplink">
                        <ul className="trip-link">
                          {((this.props.tripDetailSt.trip_type === 1 &&
                            this.state.ftripsRoles.FLT_TRIP_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 2 &&
                              this.state.htripsRoles.HTL_TRIP_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 16 && 
                             this.state.localtripsRoles.LOCAL_TRIP_ROLE_ENABLE) ||  this.props.tripDetailSt.trip_type === 3 || 
                             (this.props.tripDetailSt.trip_type === 4 && this.state.trainTripsRoles.TRAIN_TRIP_ROLE_ENABLE) ) && (
                              <li key="tripid">
                              <Link
                                to={this.state.tripid}
                                id="tripDetails"
                                onClick={this.renderComponents}
                                className={
                                  this.currentClick === "tripDetails"
                                    ? "active"
                                    : ""
                                }
                              >
                                Trip Details
                              </Link>
                            </li>
                          )}
                          {this.state.ftripsRoles.PAX_ROLE_ENABLE &&
                            (this.props.tripDetailSt.trip_type === 1 ||
                              this.props.tripDetailSt.trip_type === 3) && (
                                <li key="pax">
                                <Link
                                  to={this.state.tripid}
                                  id="pax"
                                  onClick={this.renderComponents}
                                  className={
                                    this.currentClick === "pax" ? "active" : ""
                                  }
                                >
                                  Pax Details
                                </Link>
                              </li>
                            )}
                          {((this.props.tripDetailSt.trip_type === 1 &&
                            this.state.ftripsRoles.FLT_NOTE_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 2 &&
                              this.state.htripsRoles.HTL_NOTE_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 16 &&
                              this.state.localtripsRoles.LOCAL_NOTE_ROLE_ENABLE ) ||
                            (this.props.tripDetailSt.trip_type === 4 && this.state.trainTripsRoles.TRAIN_NOTE_ROLE_ENABLE)) && (
                              <li key="Note">
                              <Link
                                to={this.state.tripid}
                                id="notes"
                                onClick={this.renderComponents}
                                className={
                                  this.currentClick === "notes" ? "active" : ""
                                }
                              >
                                Notes
                              </Link>
                            </li>
                          )}
                          {((this.props.tripDetailSt.trip_type === 1 &&
                            this.state.ftripsRoles.FLT_PAYMENT_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 2 &&
                              this.state.htripsRoles.HTL_PAYMENT_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 16 &&
                              this.state.localtripsRoles.LOCAL_PAYMENT_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 4 &&
                              this.state.trainTripsRoles.TRAIN_PAYMENT_ROLE_ENABLE)) && (
                                <li key="payment">
                              <Link
                                to={this.state.tripid}
                                id="paymentDetails"
                                onClick={this.renderComponents}
                                className={
                                  this.currentClick === "paymentDetails"
                                    ? "active"
                                    : ""
                                }
                              >
                                Payment Details
                              </Link>
                            </li>
                          )}
                          {((this.props.tripDetailSt.trip_type === 1 &&
                            this.state.ftripsRoles.FLT_AUDIT_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 2 &&
                              this.state.htripsRoles.HTL_AUDIT_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 16 && 
                              this.state.localtripsRoles.LOCAL_AUDIT_ROLE_ENABLE) ||
                            (this.props.tripDetailSt.trip_type === 4 &&
                              this.state.trainTripsRoles.TRAIN_AUDIT_ROLE_ENABLE))&& (
                                <li key="audit">
                              <Link
                                to={this.state.tripid}
                                id="audittrail"
                                onClick={this.renderComponents}
                                className={
                                  this.currentClick === "audittrail"
                                    ? "active"
                                    : ""
                                }
                              >
                                Audit Trail
                              </Link>
                            </li>
                          )}
                          {this.state.ftripsRoles.UPDATE_TKT_ROLE_ENABLE &&
                            (this.props.tripDetailSt.trip_type === 1 ||
                              this.props.tripDetailSt.trip_type === 3) && (
                                <li key="updateTicket">
                                <Link
                                  to={this.state.tripid}
                                  id="updateTktNum"
                                  onClick={this.renderComponents}
                                  className={
                                    this.currentClick === "updateTktNum"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  Update Ticket Number
                                </Link>
                              </li>
                            )}

                          {this.state.ftripsRoles.OFFLINE_REF_ROLE_ENABLE &&
                            this.props.tripDetailSt.trip_type === 1 && (
                              <li key="offlineRefund">
                                <Link
                                  to={this.state.tripid}
                                  id="offlineRefund"
                                  onClick={this.renderComponents}
                                  className={
                                    this.currentClick === "offlineRefund"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  Offline Refund
                                </Link>
                              </li>
                            )}
                          {this.state.ftripsRoles.FLT_REFUND_ROLE_ENABLE &&
                            this.props.tripDetailSt.trip_type === 1 &&
                            this.props.tripDetailSt.air_bookings[0]
                              .booking_status === "K" && (
                                <li key="refunds">
                                <Link
                                  to={this.state.tripid}
                                  id="refunds"
                                  onClick={this.renderComponents}
                                  className={
                                    this.currentClick === "refunds"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  Refunds
                                </Link>
                              </li>
                            )}
                          {this.state.htripsRoles.HTL_REFUND_ROLE_ENABLE &&
                            this.props.tripDetailSt.trip_type === 2 &&
                            this.props.tripDetailSt.hotel_bookings[0]
                              .booking_status == "K" && (
                                <li key="htlRefunds">
                                <Link
                                  to={this.state.tripid}
                                  id="htlRefunds"
                                  onClick={this.renderComponents}
                                  className={
                                    this.currentClick === "htlRefunds"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  Refunds
                                </Link>
                              </li>
                            )}
                            {(this.state.localtripsRoles.LOCAL_REFUND_ROLE_ENABLE &&
                            this.props.tripDetailSt.trip_type === 16 &&
                            this.props.tripDetailSt.booking_status === "K") && (
                              <li key="actRefunds">
                                <Link
                                  to={this.state.tripid}
                                  id="actRefunds"
                                  onClick={this.renderComponents}
                                  className={
                                    this.currentClick === "actRefunds"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  Refunds
                                </Link>
                              </li>
                            )}
                          {this.props.tripDetailSt.has_wallet_promotion !==
                            null &&
                            this.props.tripDetailSt.has_wallet_promotion ===
                              "true" &&
                            ((this.props.tripDetailSt.trip_type === 1 &&
                              this.state.ftripsRoles
                                .FLT_WALLET_PRO_ROLE_ENABLE) ||
                              (this.props.tripDetailSt.trip_type === 2 &&
                                this.state.htripsRoles
                                  .HTL_WALLET_PRO_ROLE_ENABLE)) && (
                              <li key="walletpromo">
                                <Link
                                  to={this.state.tripid}
                                  id="walletpromo"
                                  onClick={this.renderComponents}
                                  className={
                                    this.currentClick === "walletpromo"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  Wallet Promo
                                </Link>
                              </li>
                            )}

                        {this.state.trainTripsRoles.TRAIN_REFUND_ROLE_ENABLE &&
                            this.props.tripDetailSt.trip_type === 4 && 
                            (
                              <li key="trainRefund">
                                <Link
                                  to={this.state.tripid}
                                  id="trainrefund"
                                  onClick={this.renderComponents}
                                  className={
                                    this.currentClick === "trainrefund"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  Refunds
                                </Link>
                              </li>
                            )}

                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-9 tabPanel">
                      <div className="tripDetails">
                        {this.getComponent(
                          this.props.tripDetailSt.trip_type,
                          this.buttonClicked
                        )}
                      </div>
                    </div>
                    <aside className="col-3 right-bar">
                      <div className="trip-summary">
                      <ul className="booking-tag">
                     {( this.loadUserClassDtl().custLevel &&
                     this.loadUserClassDtl().overallStatus)&&<li key="overallKey">
                      <span>Overall</span>
                      <span>{this.loadUserClassDtl().custLevel} {this.loadUserClassDtl().overallStatus}</span>
                     </li>}                  
                    {this.loadUserClassDtl().userStatus &&
                    <li key="overallKey">
                      <span>Product ({this.findProdType()})</span>
                      <span> {this.loadUserClassDtl().userStatus}</span>
                    </li>}

                    {this.props.tripDetailSt.trip_type === 1 ? (
                      <>
                        <li key="overallKey"><span>Loyalty User Type </span>
                        {this.props.tripDetailSt.air_bookings[0]
                          .air_booking_detail.loyalty_user_type !==
                        null ? (
                          <span>
                            {
                              this.props.tripDetailSt.air_bookings[0]
                                .air_booking_detail.loyalty_user_type
                            }
                          </span>
                        ) : (
                          <span>NONE</span>
                        )}
                        </li>
                      </>
                    ) : null}
                  </ul>

                  <DomainMessages/>


                        {this.props.tripDetailSt.trip_type === 1 && (
                          <FlightRightSidebar />
                        )}
                        {this.props.tripDetailSt.trip_type === 2 && (
                          <HotelRightSidebar />
                        )}

                        {this.props.tripDetailSt.trip_type === 3 && (
                          <FPHRightSidebar />
                        )}

                    
                       {this.props.tripDetailSt.trip_type === 4 && (
                        <TrainSideBar />
                      )}

                     {this.props.tripDetailSt.trip_type === 16 && (
                        <ActivityRightSidebar />
                      )}

                        {((this.props.tripDetailSt.trip_type === 1 &&
                          this.state.ftripsRoles
                            .FLT_CONTACT_INFO_ROLE_ENABLE) ||
                          (this.props.tripDetailSt.trip_type === 2 &&
                            this.state.htripsRoles
                              .HTL_CONTACT_INFO_ROLE_ENABLE)
                              || (this.props.tripDetailSt.trip_type === 16 && 
                                this.state.localtripsRoles.LOCAL_CONTACT_DETAILS_ROLE_ENABLE)
                                || (this.props.tripDetailSt.trip_type === 4 && this.state.trainTripsRoles.TRAIN_CONTACT_DETAILS_ROLE_ENABLE)) && (
                          <ContactDetails />
                        )}
                        {((this.props.tripDetailSt.trip_type === 1 &&
                          this.state.ftripsRoles.FLT_MISC_INFO_ROLE_ENABLE) ||
                          (this.props.tripDetailSt.trip_type === 2 &&
                            this.state.htripsRoles
                              .HTL_MISC_INFO_ROLE_ENABLE)
                              || (this.props.tripDetailSt.trip_type === 16 && 
                                this.state.localtripsRoles.LOCAL_MISC_TRIP_INFO_ROLE_ENABLE)) && <MiscTripInfo />}
                      </div>
                    </aside>
                  </div>
                </div>
              </section></>:<PageLoad />}
            </>
          ) : (
            <CommonMsg errorType={this.error} errorMessageText={this.msg} />
          )
        ) : (
          <PageLoad />
        )}
      {
        this.state.usrDtlUpdated? <Footer />:null
      }
       
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetailSt: state.trpReducer.tripDetail,
    airLineMaster: state.trpReducer.airLineMaster,
    airPortMaster: state.trpReducer.airPortMaster,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymntStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster,
    usermasterdetails: state.trpReducer.usermasterdetails
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onInitTripDtl: req => dispatch(actions.initTripNdCtConfig(req)),
    updateUserDetails: req => dispatch(actions.updateUserDetails(req))
    //onInitAirLineMaster:(req)=>dispatch(actions.initAirLineMasterData(req)),
    //onInitAirPortMaster:(req)=>dispatch(actions.initAirPortMasterData(req))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TripDetails);
