import React, { Component } from 'react';
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import log from 'loglevel';
import Button from 'Components/buttons/button.jsx';
import FphService from '../../../services/fph/FphService.js';
import Notification from "Components/Notification.jsx";
import { confirmAlert } from 'react-confirm-alert';
export const TRIPS_ROLES = 'tripsRoles';
import Domainpath from '../../../services/commonUtils/Domainpath';
class Tags extends Component {
    constructor(props) {
        super(props);
        let tagMasterList=[];
        this.state = {
            show: false,
            tagName: '',
            showSucessMessage:false,
            showFailMessage:false,
            tagMasterList:this.props.tripDetails.tag_masters,
            message:false,
            showRemoveMessage:false,
            removeFailMessage:false,
            ftripsPermissions:'',
            htripsPermissions:''
        };
        this.confirmDlg = this.confirmDlg.bind(this);
        this.saveTagMaster=this.saveTagMaster.bind(this);
        //this.redirect=this.redirect.bind(this);
        //Getting ROLES DATA from local storage
        let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
        this.state.ftripsPermissions=tripsObj.ftripPermissions;
        this.state.htripsPermissions=tripsObj.htripPermissions;
        this.state.localtripRoles=tripsObj.localtripsRoles;
        this.state.trainTripsRoles = tripsObj.trainTripsRoles;
    }
    addTag = (e) => {
        e.preventDefault();
        if (this.state.tagName) {
            this.setState({
                tagName: this.state.tagName,
                show: false
            });
        }
    }

    confirmDlg = (tagName,i) => {
        confirmAlert({
          customUI: ({ onClose }) => {
            return (
              <>
              <div className='custom-ui'>
                <h3>Are you sure?</h3>
                <p className="t-color2 mb-15">You want to delete the selected Tag?</p>            
              </div>
              <div className="btn-grid text-right">          
              <button className="btn btn-xs btn-primary w-50" onClick={() => {
                  this.removeTag(tagName,i)
                  onClose()
              }}>Yes</button>
              <button className="btn btn-default btn-xs w-50" onClick={onClose}>No</button>
              </div>
              </>
            )
          }
        })
      };

    removeTag(tagName,i) {
        try{
            if(tagName!==null && tagName!==""){
                var tripId=this.props.tripDetails.id;
               // console.log('removeTag start..Trip id : and '+tripId +" tag name "+ tagName);
                FphService.saveTags(tripId,tagName,"D").then(response => {
                    if(response.data!==null && response.data!==""){
                    const updatedRS = JSON.parse(response.data);
                    if (updatedRS !== undefined &&
                      updatedRS !== "" &&
                      updatedRS.status !== undefined &&
                      updatedRS.status !== "" &&
                      updatedRS.status === 200){
                        //console.log("removeTag Resp " + updatedRS.status);
                        let tags = this.state.tagMasterList.slice();
                        tags.splice(i, 1);
                        this.setState({tagName:'', tagMasterList:tags,showRemoveMessage: true, showSucessMessage: false, removeFailMessage: false, showFailMessage: false, showAlert: true });
                    }else{
                        //console.log('removeTag not removed..'+tripId);
                        this.setState({removeFailMessage: true, showAlert: true});
                    }
                   }else{
                    this.setState({removeFailMessage: true, showAlert: true});
                   }
                  });
                  }
        }catch(err){
            log.error('Exception occured in removeTag function---'+err);
        }
    }

    updateValue = (e) => {
        this.setState({ tagName: e.target.value })
    }
    handleClick() {
        this.setState({
            show: true
        });
    }
    handleClickHide() {
        this.setState({
            show: false
        });
    }
    saveTagMaster(){
        try{
           var tripId=this.props.tripDetails.id;
           //console.log('Start saveTagMaster...'+tripId);
           if(this.state.tagName!==null && this.state.tagName!==""){
           FphService.saveTags(tripId,this.state.tagName,"A").then(response => {
               if(response.data!==null && response.data!==""){
               const updatedRS = JSON.parse(response.data);
               if (updatedRS !== undefined &&
                 updatedRS !== "" &&
                 updatedRS.status !== undefined &&
                 updatedRS.status !== "" &&
                 updatedRS.status === 200){
                  // console.log("saveTagMaster Resp " + updatedRS.status);
                   this.state.tagMasterList.push(updatedRS.data[0]);
                   this.setState({showSucessMessage: true, showRemoveMessage: false, showFailMessage: false, showAlert: true});
               }else{
                   //console.log('saveTagMaster not updated'+tripId);
                   this.setState({showFailMessage: true, showSucessMessage: false, showAlert: true});
               }
              }else{
               this.setState({showFailMessage: true, showSucessMessage: false, showAlert: true });
              }
             });
             }else{
                this.setState({message: true, showSucessMessage: false, showFailMessage: false, showAlert: true});
             }
       }catch(err){
           log.error('Exception occured in getPromotionsData function---'+err);
       }
   }
   toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };
    // redirect(event){

    //     try{
    //     event.preventDefault();
    //     var domainpath = process.env.domainpath;
    //     let tripRef=this.props.tripDetails.trip_ref
    //     let url ='';
    //     if(event.target.id === 'tripCancel'){
    //       url = domainpath+"/hq/trips/"+tripRef+"/cancel/"
    //     }
    //     window.open(
    //         url
    //           );
    //     }catch(err){

    //         log.error('Exception occured in Footer redirect function---'+err);

    //     }

    // }
    getMessage(type) {
        let msg = "";
        if (type === "success") {
          msg =
          this.state.tagName + " tag saved Successfully.";
        } else if (type === "warning") {
          msg =
            this.state.tagName + " has not saved";
        }
        return msg;
      }
      
    render() {
        let viewType, messageText;
    if (this.state.message) {
      viewType = "warning";
      messageText = "Enter tag name"
    } else if (this.state.showFailMessage) {
      viewType = "warning";
      messageText = this.getMessage("warning");
    } else if (this.state.showSucessMessage) {
      viewType = "success";
      messageText = this.getMessage("success");
    }else if (this.state.showRemoveMessage) {
        viewType = "success";
        messageText = "Selected tag removed Successfully.";
      }else if (this.state.removeFailMessage) {
        viewType = "warning";
        messageText = "Unable to remove the selected Tag.";
      }
        var domainpath = Domainpath.getHqDomainPath();
        return (
            <>
                <div className="Add-tag bg-white">
                <h5 className="show-tg-line dis-flx-btw mt-5 pr-0">
                       <span>Tags</span> 
                       {((this.props.tripDetails.trip_type===1 && this.state.ftripsPermissions.ADD_TAG_PERMSN_ENABLE) || (this.props.tripDetails.trip_type===2 && this.state.htripsPermissions.HTL_ADD_TAG_PERMSN_ENABLE) 
                       || (this.props.tripDetails.trip_type===3)
                       || (this.props.tripDetails.trip_type=== 16 && 
                       this.state.localtripRoles.LOCAL_ADD_TAG_ROLE_ENABLE) || 
                       (this.props.tripDetails.trip_type=== 4 && 
                        this.state.trainTripsRoles.TRAIN_ADD_TAG_ROLE_ENABLE))  &&
                       <button className="btn btn-xs btn-primary t-color0" onClick={() => this.handleClick()}>Add</button>
                       }
                    </h5>
                   <div className="pl-10 pr-10">
                   {viewType && messageText && (
                    <Notification
                    viewType={viewType}
                    viewPosition="fixed"
                    className="mt-10"
                    messageText={messageText}
                    showAlert={this.state.showAlert}
                    toggleAlert={this.toggleAlert}
                    />
                )}
                   
                    {this.state.tagMasterList.map((tag, index) => (
                     <React.Fragment key={index}>
                    {tag.status!==null && tag.status==="A" && (
                    <div className="tag mr-5">
                    <a id="tag"  href={domainpath+"/hq/trips?trip_tag="+tag.tag_name}  target="_blank" >{tag.tag_name}</a>
                    <span className="clsTag ml-5" onClick={() => { this.confirmDlg(tag.tag_name,index) }}><Icon  color="#fff" size={8} icon="close"/> </span>
                   </div>
                    )}
                    {tag.status===null && (
                    <div className="tag mr-5">
                    <a id="tag"  href={domainpath+"/hq/trips?trip_tag="+tag.tag_name} title={tag.tag_name} target="_blank" >{tag.tag_name}</a>
                    <span className="clsTag ml-5" onClick={() => { this.confirmDlg(tag.tag_name,index) }}><Icon  color="#fff" size={8} icon="close"/> </span>
                   </div>
                    )}
                    </React.Fragment>
                    ))}
                    </div>
                    {this.state.show &&
                        <>
                            <form className="form-group dis-flx-btw pt-5" onSubmit={this.addTag}>
                            <div className="tagGrid col-6 pr-0">
                                <input
                                    placeholder="Enter Tag Name"
                                    value={this.state.tagName}
                                    onChange={this.updateValue}
                                />
                                 </div>
                            <div className="addtag col-6 mob-txt-right">
                                <Button
                                size="xs"                    
                                type="primary"
                                id="saveTagId"
                                onClickBtn={this.saveTagMaster}
                                btnTitle='Submit'
                            >
                            </Button>
                             
                             {((this.props.tripDetails.trip_type===1 && this.state.ftripsPermissions.CANCL_TAG_PERMSN_ENABLE) || (this.props.tripDetails.trip_type===2 && this.state.htripsPermissions.HTL_CANCL_TAG_PERMSN_ENABLE) 
                             || (this.props.tripDetails.trip_type===3 || (this.props.tripDetails.trip_type===4 && this.state.trainTripsRoles.TRAIN_DELETE_TAG_ROLE_ENABLE))
                             || (this.props.tripDetails.trip_type=== 16 && 
                                 this.state.localtripRoles.LOCAL_DELETE_TAG_ROLE_ENABLE))  &&
                                <button className="btn btn-text btn-xs" onClick={() => this.handleClickHide()}>Cancel</button>
                             }
                                </div>
                              
                              
                            </form>
                       
                        </>
                    }
                          </div>

            </>
        );
    }
}
const mapStateToProps = state => {
    return {
      tripDetails: state.trpReducer.tripDetail
    };
  }
export default connect(mapStateToProps)(Tags);

