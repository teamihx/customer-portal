import React, { Component } from 'react'
import log from 'loglevel';
import { connect } from 'react-redux';
import FphService from '../../../services/fph/FphService.js';
import * as actions from '../../../store/actions/index';

class WalletPromo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            promotions:[],
            showMessage:false
        }
    }
    componentDidMount(){
      if(this.props.walletPromoData===""){
        this.getPromotionsData();
      }else{
        this.state.promotions=this.props.walletPromoData.promotions;
        this.forceUpdate();
      }
    }
    getPromotionsData(){
     try{
        var tripRef=this.props.tripDetailsData.trip_ref;
        //('Start Wallet Promotions...'+tripRef);
        if(tripRef!==null && tripRef!==""){
        FphService.getWalletPromotions(tripRef).then(response => {
            if(response.data!==null && response.data!==""){
            const updatedRS = JSON.parse(response.data);
            if (updatedRS !== undefined &&
              updatedRS !== "" &&
              updatedRS.status !== undefined &&
              updatedRS.status !== "" &&
              updatedRS.status === 200){
                //console.log("getPromotionsData Resp " + updatedRS.data.status);
                this.setState({promotions: updatedRS.data.promotions});
                this.props.updateWalletPromotins(updatedRS.data);
                if(updatedRS.data.promotions.length===0){
                this.setState({showMessage: true});
                }
            }else{
               // console.log('Promotions are not available...'+tripRef);
                this.setState({showMessage: true});
            }
           }else{
            this.setState({showMessage: true});
           }
          });
          }
    }catch(err){
        log.error('Exception occured in getPromotionsData function---'+err);
    }
}

    render() {  
        return (
        <>
        <div className="topSec">
            {this.state.promotions.length!==0 && (
            <h4 className="in-tabTTl-sub">Wallet Promotion Details</h4>
            )}
             <div className="resTable">
                 <br/>
             <div className="container"> 
             {this.state.showMessage && 
             <h6>No Promotions for this trip</h6>
             }
             </div>
             {!this.state.showMessage && this.state.promotions.length!==0 &&
            <table className="dataTbl pax-tbl dataTable5">
            <thead><tr>
                <th width="15%">Promotion Id</th>
                <th width="10%">Amount</th>
                <th width="10%">Trigger Date</th>
                <th width="10%">Expiry Date</th>
                <th width="15%">Status</th>
                <th width="15%">Credited On</th>
              </tr></thead>
                <tbody>
                {this.state.promotions.map((promotion, index) => (
                <React.Fragment key={index}> 
                <tr>
                  <td>
                    <span>{promotion.id}</span>
                  </td>
                  <td>
                    <span>{promotion.currency} {promotion.amount}</span>
                  </td>
                  <td>
                    <span>{promotion.trigger_date}</span>
                  </td>
                  <td>
                    <span>{promotion.expiry_date}</span>
                  </td>
                  <td>
                    <span>{promotion.status}</span>
                  </td>
                  <td>
                  {promotion.transactions!==null && (
                   <span>{promotion.transactions[0].time}</span>
                  )}
                   {promotion.transactions===null && (
                   <span>--</span>
                  )}
                  </td>
                </tr>
                </React.Fragment>
             ))}
              </tbody>
              </table>
             }       
            </div>
        </div>
        </>
        )
    }

}

const mapDispatchToProps = dispatch => {
  //console.log("mapDispatchToProps");
  return {
      updateWalletPromotins: (req) => dispatch(actions.updateWalletPromo(req)),
  }
}

const mapStateToProps = state => {
    return {
        tripDetailsData: state.trpReducer.tripDetail,
        walletPromoData:state.trpReducer.walletPromotons
    };
}
export default connect(mapStateToProps,mapDispatchToProps)(WalletPromo);