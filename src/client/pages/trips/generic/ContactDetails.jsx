import React, { Component } from 'react';
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import ShowHide from 'Components/ShowHide.jsx';
class ContactDetails extends Component {
    constructor() {
        super();
        this.state = {
            contact_detail: {},
        };
    }
    isEmpty = (obj) => {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    }
    loadContactDetail = () => {
        if (!this.isEmpty(this.props.tripDetailSt) && !this.isEmpty(this.props.tripDetailSt.contact_detail)) {
            this.state.contact_detail = this.props.tripDetailSt.contact_detail
            //console.log("trip detailppppppppppppppppppppppppppp"+JSON.stringify(this.props.tripDetailSt.contact_detail));
            //console.log("trip detailppppppppppppppppppppppppppp"+JSON.stringify(this.props.tripDetailSt.contact_detail.address));
        }
    }
    render() {
        { this.loadContactDetail() }
        const {
            contact_detail = {}
        } = this.state
        const {
            title,
            first_name,
            last_name,
            mobile,
            landline,
            email,
            address,
            city_name,
            state_name,
            pin_code,
            country_name
        } = contact_detail
        let usrtitle= title!=='0'?title:''
        let name=usrtitle+" "+first_name+" "+last_name
        return (
            <>
                {!this.isEmpty(contact_detail) ?
                    <div className="address-Pnl side-pnl">
                        <ShowHide title={[<span key="cotact" className="t-color3">Contact </span>,name]}>
                            <div className="showHide-content">
                        <ul>
                            {contact_detail.mobile !== null || contact_detail.landline !== null ?
                                <li>
                                    <span>Phone</span> <span>
                                        {mobile !== null ?
                                            <em className="d-b">{mobile} - <em className="t-color3 font-11">Mobile</em></em> : ""
                                        }
                                        {landline !== null ?

                                            <em className="d-b mt-5">{landline} - <em className="t-color3 font-11">Home</em></em> : ""
                                        }</span></li> : ""
                            }

                            {!this.isEmpty(email) && email !== null ?
                                <li><span>Email</span> <span><a className="m0" href={"mailto:"+{email}}>{email}</a></span></li> : ""
                            }

                            {address !== undefined&&address !== null && address.trim() || city_name !== null || state_name !== null ?
                                <li><span>Home</span>
                                    <span>
                                        {address !== undefined&&address !== null  && address.trim() ?
                                            <>
                                                {address}<br /></> : ""
                                        }
                                        {city_name !== null ?
                                            <>
                                                {city_name} {pin_code},<br /></> : ""
                                        }
                                        {state_name !== null ?
                                            <> {state_name},<br /></> : ""
                                        }
                                        {country_name}</span></li> : ""
                            }

                        </ul>
                        </div>
                        </ShowHide>
                    </div> : ""
                }

            </>
        );
    }
}
const mapStateToProps = state => {
    return {
        tripDetailSt: state.trpReducer.tripDetail,
    };
}
export default connect(mapStateToProps)(ContactDetails);

