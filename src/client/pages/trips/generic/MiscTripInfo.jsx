import React, { Component } from 'react';
import Icon from 'Components/Icon';
import { connect } from 'react-redux';
import log from 'loglevel';
import FphService from '../../../services/fph/FphService.js';
import * as actions from '../../../store/actions/index';
import ShowHide from 'Components/ShowHide.jsx';
export const TRIPS_ROLES = 'tripsRoles'
class MiscTripInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paidAmount:0,
            walletData:[],
            callWallet:true,
            callPayment:true,
            fops:'',
            callFlights:true,
            callHotelStatus:true,
            hotelChmmStatus:"",
            callService:true,
            fltSupplier:'',
            ftripsRoles:'',
            htripsRoles:'',
            miscMsg:''
        };
        
        //Getting ROLES DATA from local storage
        let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
        this.state.ftripsRoles=tripsObj.ftriplinks;
        this.state.htripsRoles=tripsObj.htriplinks;
        this.state.ftrips=tripsObj.ftrips
        this.state.localtripRoles=tripsObj.localtripsRoles
    }

    getTripCHMMHotelStatus(tripRefNum){
        try{
           //  console.log('getTripCHMMHotelStatus start...'+ this.state.ftripsRoles.FLT_MISC_VIEWLINK_ROLE_ENABLE);
             this.setState({callHotelStatus: false});
             FphService.getHotelStatus(tripRefNum).then(response => {
                 if(response.data!==null && response.data!==""){
                 const hotelResponse = JSON.parse(response.data);
                 //console.log('getTrip CHMM Hotel Service Status..'+hotelResponse.status);
                 if (hotelResponse !== undefined &&
                     hotelResponse !== "" &&
                     hotelResponse.status !== undefined &&
                     hotelResponse.status !== "" &&
                     hotelResponse.status === 200){
                    if(hotelResponse.data!==null && hotelResponse.data.data!=="" && hotelResponse.data.bookingSearchDetail!==null && hotelResponse.data.data.bookingSearchDetail.reservationStatus!==null){
                        //console.log('CHMM HOTEL STATUS : '+hotelResponse.data.data.bookingSearchDetail.reservationStatus);
                        this.setState({hotelChmmStatus: hotelResponse.data.data.bookingSearchDetail.reservationStatus});
                    }
                 }else{
                   // console.log('getTripCHMMHotelStatus NO data ..'+hotelResponse.status);
                 }
                }else{
                     //console.log('getTripCHMMHotelStatus NO data ..'+tripRefNum);
                }
               });
         }catch(err){
             log.error('Exception occured in getTripCHMMHotelStatus function---'+err);
         }
         }

        getFlightSupplierName(){
            try{
               //  console.log('getFlightSupplierName start...');
                 this.setState({callService: false});
                 var agentPccObj=this.props.ctConfigRespData.data['ct.hq-trips.supplier_agentpcc_mapping'];
                 if(agentPccObj!==null && agentPccObj!==undefined){
                  var pccObj=JSON.parse(agentPccObj);
                  for(var key in pccObj){
                      var agentPcc=pccObj[key];
                      for(let trip of this.props.tripDetails.air_bookings){
                          for(let info of trip.air_booking_infos){
                              if(agentPcc!==null && info.agent_pcc!==null && info.agent_pcc!==undefined && agentPcc.includes(info.agent_pcc)){
                                  if(this.state.fltSupplier===''){
                                      this.state.fltSupplier=key;
                                  }else{
                                      if(!this.state.fltSupplier.includes(key)){
                                          this.state.fltSupplier=this.state.fltSupplier+","+key;
                                      }
                                  }
                                 // console.log('Flight Supplier Name :'+this.state.fltSupplier);
                              }
                          }
                      }
                  }
                 }
             }catch(err){
                 log.error('Exception occured in getFlightSupplierName function---'+err);
             }
             }
 

         getTripWalletData(bookedUsrId){
        try{
             //console.log('getTripWalletData start...'+ bookedUsrId);
             this.setState({callWallet: false});
             FphService.getWalletCurrencies(bookedUsrId).then(response => {
                 if(response.data!==null && response.data!==""){
                 const walletCurResp = JSON.parse(response.data);
                 if (response !== undefined &&
                     response !== "" &&
                     response.status !== undefined &&
                     response.status !== "" &&
                     response.status === 200 && walletCurResp.status===200){
                     if(walletCurResp.data!==null && walletCurResp.data!==undefined 
                      && walletCurResp.data!== '' && walletCurResp.data.length!==0){                       
                     let walletArray=[];
                     for(let wallet of walletCurResp.data){
                     FphService.fetchWallet(bookedUsrId,wallet.currency).then(walletResponse => {
                         if(walletResponse.data!==null && walletResponse.data!==""){
                         const walletResp = JSON.parse(walletResponse.data);
                         if (walletResp !== undefined &&
                             walletResp !== "" &&
                             walletResp.status !== undefined &&
                             walletResp.status !== "" &&
                             walletResp.status === 200){
                                 const walletObj = {
                                     walletNumber:walletResp.data.walletNumber,
                                     currency:wallet.currency,
                                     userId:bookedUsrId
                                 }
                                 walletArray.push(walletObj);
                                 if(walletCurResp.data.length===walletArray.length){
                                     this.setState({walletData: walletArray});
                                     //console.log('Wallet data Size :'+ this.state.walletData.length);
                                 }
                         }else{
                            // console.log('fetchWallet..Status : '+walletResp.status);                             
                             this.setState({ miscMsg: "Data is not available"});                             
                             this.forceUpdate();
                         }
                        }else{
                             //console.log('fetchWallet..No data..'+bookedUsrId);
                             this.setState({ miscMsg: "Data is not available"});   
                             this.forceUpdate();
                        }
                       });
                     }
                     }else{
                      this.setState({ miscMsg: "Data is not available"});
                      this.forceUpdate();

                     }
                 }else{
                    // console.log('Get Wallet Currencies Status :'+response.status);
                     this.setState({ miscMsg: "Data is not available"});
                     this.forceUpdate();

                 }
                }else{
                     //console.log('getWalletCurrencies NO data ..'+bookedUsrId);
                     this.setState({ miscMsg: "Data is not available"});
                     this.forceUpdate();
                }
               });
         
         }catch(err){
             log.error('Exception occurred in getTripWalletData function---'+err);
         }
         }
    

    render() {
      
         if(this.state.callWallet && this.props.tripDetails.booked_user_id!==null && this.props.tripDetails.booked_user_id!==undefined){
                this.getTripWalletData(this.props.tripDetails.booked_user_id);
         }
         if(this.props.tripDetails.trip_type === 2 && this.state.callHotelStatus && this.props.tripDetails.trip_ref!==null && this.props.tripDetails.trip_ref!==undefined){
                this.getTripCHMMHotelStatus(this.props.tripDetails.trip_ref);
         }
        if(this.props.tripDetails.trip_type === 1 && this.state.callService){
                this.getFlightSupplierName();
         } 
         
         if(this.state.callPayment){
        if(this.props.tripDetails.payments){
            for(let value of this.props.tripDetails.payments){
                if(this.state.paidAmount===0){
                    this.state.paidAmount=value.amount;
                }else{
                    this.state.paidAmount=this.state.paidAmount+value.amount; 
                }
            }
            this.setState({callPayment: false});
        }
        }
        if(this.state.callFlights){
        if(this.props.tripDetails.air_bookings!==0 && this.props.tripDetails.air_bookings!==undefined){
            for(let trip of this.props.tripDetails.air_bookings){
            for(let flight of trip.flights){
                for(let segment of flight.segments){
                    if(segment.fop!==null && segment.fop!==undefined){
                     if(this.state.fops ===''){
                      this.state.fops=segment.fop;
                     }else{
                      if(!this.state.fops.includes(segment.fop)){
                      this.state.fops=this.state.fops+","+segment.fop;
                      }
                     }
                    }else{
                        if(trip.air_booking_infos.external_references!==undefined && trip.air_booking_infos.external_references.length!==0){
                         for(let extRef of trip.air_booking_infos.external_references){
                           if(extRef.name==="FOP" || extRef.name==="fop"){
                            if(this.state.fops ===''){
                                this.state.fops=extRef.value;
                                 }else{
                                this.state.fops=this.state.fops+","+extRef.value;
                            }
                           }
                         }
                        }
                    }
                }
            }
        }
        this.setState({callFlights: false});
        }
        }
        return (
          
          <>
          
          
            {(this.props.tripDetails.trip_type === 1 ||
            this.props.tripDetails.trip_type === 2 || this.props.tripDetails.trip_type === 16) ? (
              <div className="side-pnl">
                <ShowHide title="Miscellaneous Trip Information">
                <div className="showHide-content">
                  <div className="miscellaneous">
                  
            {this.state.miscMsg !== "" && this.state.miscMsg !== undefined && (
              
              <div className="alert alert-danger">{this.state.miscMsg}</div>
           )}
                    {this.props.tripDetails.trip_type === 2 && (
                      <ul>
                        {this.props.tripDetails.hotel_bookings.length !== 0 &&
                          this.props.tripDetails.hotel_bookings[0]
                            .affiliate_txn_id !== null && (
                            <li className="font-w-m">
                              <span>External Txn ID : </span>
                              {
                                this.props.tripDetails.hotel_bookings[0]
                                  .affiliate_txn_id
                              }
                            </li>
                          )}
                        {this.state.hotelChmmStatus !== "" && (
                            <li className="font-w-m">
                              <span>CHMM Status : </span>
                              {this.state.hotelChmmStatus}
                            </li>
                          )}
                        <li className="font-w-m">
                          <span>Offer Applied : </span>

                          {this.props.tripDetails.hotel_bookings[0]
                            .applied_offers !== null && (
                            <>
                              {
                                this.props.tripDetails.hotel_bookings[0]
                                  .applied_offers
                              }
                            </>
                          )}
                        </li>
                        {this.props.tripDetails.hotel_bookings[0].sup_promos !==
                          null && (
                          <li className="font-w-m">
                            Promo Id
                            {
                              this.props.tripDetails.hotel_bookings[0]
                                .sup_promos
                            }
                          </li>
                        )}

                        {this.props.tripDetails.hotel_bookings[0]
                          .trip_villa_rate !== null && (
                          <li className="font-w-m">
                            Booking Type : Trip Villa Booking
                          </li>
                        )}
                      </ul>
                    )}

                    {this.props.tripDetails.payments !== undefined &&
                      this.props.tripDetails.currency !==
                        this.props.tripDetails.payments[0].currency && (
                        <ul>
                          <li className="font-w-m">
                            <span>Payment Currency :</span>
                            {this.props.tripDetails.payments[0].currency}
                          </li>

                          <li className="font-w-m">
                            <span>Paid Amount : </span>
                            {this.state.paidAmount}
                          </li>

                          {this.props.tripDetails.trip_type === 1 &&
                            this.props.tripDetails.air_bookings.length !== 0 &&
                            this.props.tripDetails.air_bookings[0]
                              .air_booking_detail.utm_conversion_factor !==
                              null && (
                              <li className="font-w-m">
                                <span>UTM Factor :</span>
                                {
                                  this.props.tripDetails.air_bookings[0]
                                    .air_booking_detail.utm_conversion_factor
                                }
                              </li>
                            )}
                        </ul>
                      )}

                    {this.props.tripDetails.trip_type === 1 && (
                      <ul>
                        {this.props.tripDetails.air_bookings[0].flights[0]
                          .segments[0].book_pcc !== null &&
                        this.props.tripDetails.air_bookings[0].flights[0]
                          .segments[0].book_pcc !== undefined ? (
                          <li className="font-w-m">
                            <span>Booking PCC:</span>
                            {
                              this.props.tripDetails.air_bookings[0].flights[0]
                                .segments[0].book_pcc
                            }
                          </li>
                        ) : (
                          <>
                            {this.props.tripDetails.air_bookings[0]
                              .external_references !== undefined &&
                              this.props.tripDetails.air_bookings[0]
                                .external_references.length !== 0 && (
                                <>
                                  {this.props.tripDetails.air_bookings[0].external_references.map(
                                    (ref, index) => (
                                      <React.Fragment key={index}>
                                        {ref.name.toLowerCase() ===
                                          "book_pcc" &&
                                          ref.value !== null &&
                                          ref.value !== undefined && (
                                            <li className="font-w-m">
                                              <span>Booking PCC:</span>
                                              {ref.value}
                                            </li>
                                          )}
                                      </React.Fragment>
                                    )
                                  )}
                                </>
                              )}
                          </>
                        )}
                        {this.state.fops !== "" && (
                          <li className="font-w-m">
                            <span>FOP:</span> {this.state.fops}
                          </li>
                        )}

                        {this.state.fltSupplier!=="" && (
                            <li className="font-w-m">
                              <span>Suppliers:</span> {this.state.fltSupplier}
                            </li>
                          )}
                      </ul>
                    )}

                    {this.state.walletData.length !== 0 && (
                      <>
                        {this.state.walletData.map((wallet, index) => (
                          <React.Fragment key={index}>
                          <ul className="mt-10 wallet-info">
                            <li className="w-75">Wallet:</li>
                            <li>
                              <ul>
                                <li>
                                  
                                  {wallet.walletNumber} | {wallet.currency} | 
                                  {((this.props.tripDetails.trip_type===1 && this.state.ftrips.FLT_MISC_VIEWLINK_ROLE_ENABLE) || (this.props.tripDetails.trip_type===2 && this.state.htripsRoles.HTL_MISC_VIEWLINK_ROLE_ENABLE)
                                  || (this.props.tripDetails.trip_type=== 16 && 
                                    this.state.localtripRoles.LOCAL_MISC_VIEW_TXN_LINK_ROLE_ENABLE)) &&
                                  <a
                                    className="d-ib m0" target="_blank"
                                    href="https://cards.qwikcilver.com/QwikCilver/eGMSAdmin/Security/Signin.aspx?Okey=HmoFypO9zrj3J1lu7BsjerKL5JA"
                                  > View Txn
                                  </a>
                                   }
                                </li>

                                {this.props.tripDetails.trip_type === 1 &&
                                  this.props.tripDetails.air_bookings[0]
                                    .air_booking_detail.cashback_amount !==
                                    null &&
                                  this.props.tripDetails.air_bookings[0]
                                    .air_booking_detail.cashback_amount !== 0 &&
                                  this.props.tripDetails.air_bookings[0]
                                    .air_booking_detail.cashback_type !==
                                    null && (
                                    <li>
                                     <span class>Cashback:</span>
                                     <span>
                                      {
                                        this.props.tripDetails.air_bookings[0]
                                          .air_booking_detail.cashback_type
                                      }
                                      |
                                      {
                                        this.props.tripDetails.air_bookings[0]
                                          .air_booking_detail.cashback_amount
                                      }
                                      </span>
                                    </li>
                                  )}
                                <li>
                                 <span>Redeem:</span> <span>paymode ~ ewallet|userid ~ {wallet.userId}
                                  _{wallet.currency}</span>
                                </li>
                                <li>
                                  <span>Reload:</span><span>userid ~ {wallet.userId} _
                                  {wallet.currency}</span> 
                                </li>
                              </ul>
                            </li>
                            </ul>
                          </React.Fragment>
                        ))}
                     </>
                    )}
                  </div>
                  </div>
                </ShowHide>
              </div>
            ) : (
              ""
            )}
          </>
        );
    }
}

  const mapStateToProps = state => {
      return {
        tripDetails: state.trpReducer.tripDetail,
        ctConfigRespData:state.trpReducer.ctConfigData
      };
  }
export default connect(mapStateToProps)(MiscTripInfo);

