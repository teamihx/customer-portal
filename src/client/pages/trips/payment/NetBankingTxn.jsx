import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import utility from "../../common/Utilities";

class NetBankingTxn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txn_type: ""
    };
  }
  displayTarnsactions = pmntTxn => {
    //console.log("pmntTxns" + JSON.stringify(this.props.tripDetail));
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
        pmntTxn.txn_type = txn.txn_type;
        break;
      }
    }
    for (let txn of this.props.tripDetail.txns) {
    if(txn.refunds!==null && txn.refunds.length>0){
      for (let refund of txn.refunds) {
        if (pmntTxn.id === refund.payment_id) {
          pmntTxn.refund = refund.refund_amount;
        }
      }
      }
    }
    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let creditCardDtl = [];
    creditCardDtl.push(
      "Payment Mode" + ":" + this.props.paymentTypeMaster[pmntTxn.payment_type]
    );
    creditCardDtl.push("Amount" + ":" + amount);
    creditCardDtl.push("Refunded" + ":" + refund);

    let bank = pmntTxn.payment_net_banking_details!==null?this.props.BankMasterData[
      pmntTxn.payment_net_banking_details.bank_id
    ]:'-';
    creditCardDtl.push("Bank" + ":" + bank);
    let status = this.props.paymentStatusMaster[pmntTxn.status];
    creditCardDtl.push("Payment Status" + ":" + status);

    creditCardDtl.push("Description" + ":" + pmntTxn.description);

    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );
    let payment_subtype =
      pmntTxn.payment_subtype !== null ? pmntTxn.payment_subtype : "-";
    let is_cp_pmnt = payment_subtype === "CP" ? "True" : "False";
    creditCardDtl.push("Cleartrip Payment" + ":" + is_cp_pmnt);
    //console.log("Pmnt===" + JSON.stringify(pmntTxn));
    return creditCardDtl.map((dtl, index) => {
      return (
        <li key={index}>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
        </li>
      );
    });
  };

  displayNoOfAttempt = (pmntTxns, index) => {
    let txns = pmntTxns.payment_net_banking_details;
    //console.log("Net bkng " + JSON.stringify(txns));
    let redirectInObj = {
      created_at: pmntTxns.payment_redirect_timings[0].redirection_in,
      status: txns!==null?txns.status:'-',
      bank_id: txns!==null?txns.bank_id:'-',
      credential_name: txns!==null?txns.credential_name:null,
      tran_type: "Redirect In",
      gateway_response3: "-",
      gateway_response2: "-"
    };
    let redirectOutObj = {
      created_at: pmntTxns.payment_redirect_timings[0].redirection_out,
      status: txns!==null?txns.status:'-',
      bank_id: txns!==null?txns.bank_id:'-',
      credential_name: txns!==null?txns.credential_name:'-',
      tran_type: "Redirect Out",
      gateway_response3: "-",
      gateway_response2: "-"
    };
    let redirectArray = [redirectOutObj, redirectInObj];
    //console.log("redirectArray=="+redirectArray);

    txns!=null?redirectArray.push(txns):null;
    return (
      <>
        <h3>Payment attempts</h3>
        <div className="resTable">
          <table>
            <thead>
              <tr key={index}>
                <th>Time</th>
                <th>Status</th>
                <th>G/W</th>
                <th>Credential</th>
                <th>Transaction Type</th>
                <th>G/W Transaction ID</th>
                <th>G/w Response String</th>
              </tr>
            </thead>
            <tbody>
              {redirectArray.map((txns, index) => (
                <tr key={index}>
                  <td>{dateUtil.ConvertIsoToSimpleDateFormat(txns.created_at)}</td>
                  <td>{this.props.paymentStatusMaster[txns.status]} </td>
                  <td>{this.props.BankMasterData[txns.bank_id]}</td>
                  <td>{txns.credential_name}</td>
                  <td>
                    {typeof txns.tran_type !== "undefined"
                      ? txns.tran_type
                      : "Verification"}
                  </td>
                  <td>{txns.bank_verify_resp1}</td>
                  <td>{txns.bank_verify_resp2}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </>
    );
  };
  componentDidMount() {
    this.setTxnType();
  }
  setTxnType = () => {
    for (let txn of this.props.tripDetail.txns) {
      let pmntTxn = this.props.data;
      if (pmntTxn.txn_id === txn.id) {
        this.setState({ txn_type: txn.txn_type });
        break;
      }
    }
  };
  render() {
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type - {this.props.txnTypeMasterData[this.state.txn_type]}
        </div>

        <ul>{this.displayTarnsactions(this.props.data)}</ul>

        {this.displayNoOfAttempt(this.props.data)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData,
    BankMasterData: state.trpReducer.BankMasterData
  };
};

export default connect(mapStateToProps, null)(NetBankingTxn);
