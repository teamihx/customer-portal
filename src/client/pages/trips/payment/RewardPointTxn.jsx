import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import utility from "../../common/Utilities";

class RewardPointTxn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txn_type: ""
    };
  }
  displayTarnsactions = pmntTxn => {
    //console.log("pmntTxns" + JSON.stringify(this.props.data));
    for (let txn of this.props.tripDetail.txns) {
     if(txn.refunds!==null && txn.refunds.length>0){
      for (let refund of txn.refunds) {
        if (pmntTxn.id === refund.payment_id) {
          pmntTxn.refund = refund.refund_amount;
        }
      }
      }
    }
    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let creditCardDtl = [];
    creditCardDtl.push("Payment Mode" + ":" + pmntTxn.payment_subtype);
    creditCardDtl.push("Amount" + ":" + amount);
    creditCardDtl.push("Refund" + ":" + refund);
    //creditCardDtl.push("Refunded" +":"+ )
    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );
    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );
    //console.log("Pmnt===" + JSON.stringify(pmntTxn));
    return creditCardDtl.map(dtl => {
      return (
        <li>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
        </li>
      );
    });
  };
  setTxnType = () => {
    for (let txn of this.props.tripDetail.txns) {
      let pmntTxn = this.props.data;
      if (pmntTxn.txn_id === txn.id) {
        this.setState({ txn_type: txn.txn_type });
        break;
      }
    }
  };
  componentDidMount() {
    this.setTxnType();
  }

  render() {
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type - {this.props.txnTypeMasterData[this.state.txn_type]}
        </div>
        <ul>{this.displayTarnsactions(this.props.data)}</ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData
  };
};

export default connect(mapStateToProps, null)(RewardPointTxn);
