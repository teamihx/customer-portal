import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import utility from "../../common/Utilities";

class ApplePayTxn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txn_type: ""
    };
  }
  displayCreditCaardTarnsactions = pmntTxn => {
    //console.log("pmntTxns" + JSON.stringify(this.props.tripDetail));
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
        pmntTxn.txn_type = txn.txn_type;
        break;
      }
    }
    for (let txn of this.props.tripDetail.txns) {
     if(txn.refunds!==null && txn.refunds.length>0){
      for (let refund of txn.refunds) {
        if (pmntTxn.id === refund.payment_id) {
          pmntTxn.refund = refund.refund_amount;
        }
      }
    }
    }

    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let creditCardDtl = [];
    creditCardDtl.push("Payment Mode" + ":" + "Apple Pay");
    creditCardDtl.push("Amount" + ":" + amount);
    creditCardDtl.push("Refund" + ":" + refund);

    creditCardDtl.push("Payment Subtype" + ":" + pmntTxn.payment_subtype);
    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );

    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );

    //console.log("Pmnt===" + JSON.stringify(pmntTxn));
    return creditCardDtl.map(dtl => {
      return (
        <li>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
        </li>
      );
    });
  };

  displayCreditCardNoOfAttempt = pmntTxns => {
    //console.log("pmntTxns" + JSON.stringify(pmntTxns));
    let txn = pmntTxns.payment_ap_txns;
    return (
      <>
        <div className="highInfo">Payment attempts</div>
        <div className="resTable">
          <table>
            <thead>
              <tr>
                <th width="18%">Time</th>
                <th width="18%">Updated</th>
                <th width="12%">Status</th>
                <th width="15%">Credential</th>
                <th width="15%">Transaction Type</th>
                <th width="15%">Descrption</th>
              </tr>
            </thead>
            <tbody>
              {pmntTxns.payment_ap_txns.map(txn => (
                <tr>
                  <td>{dateUtil.msToTime(txn.created_at)}</td>
                  <td>{dateUtil.msToTime(txn.updated_at)}</td>
                  <td>{this.props.paymentStatusMaster[txn.status]} </td>
                  <td>{txn.credential_name}</td>
                  <td>Verification</td>
                  <td>{txn.gateway_response1}</td>
                  <td>{txn.gateway_response2}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>{" "}
      </>
    );
  };
  componentDidMount() {
    this.setTxnType();
  }
  setTxnType = () => {
    for (let txn of this.props.tripDetail.txns) {
      let pmntTxn = this.props.data;
      if (pmntTxn.txn_id === txn.id) {
        this.setState({ txn_type: txn.txn_type });
        break;
      }
    }
  };
  render() {
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type - {this.props.txnTypeMasterData[this.state.txn_type]}
        </div>
        <ul>{this.displayCreditCaardTarnsactions(this.props.data)}</ul>
        {this.displayCreditCardNoOfAttempt(this.props.data)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData
  };
};

export default connect(mapStateToProps, null)(ApplePayTxn);
