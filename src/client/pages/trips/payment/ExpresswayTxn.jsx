import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import paymentservice from "../../../services/paymentSchedule/paymentSchedule";
import utility from "../../common/Utilities";

class ExpresswayTxn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      epRes: {}
    };
    this.txn_type = "";
    this.req = {
      userId: this.props.tripDetail.user_id,
      currency: this.props.tripDetail.currency
    };
  }
  displayTarnsactions = pmntTxn => {
    //console.log("pmntTxns" + JSON.stringify(this.props.tripDetail));
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
        this.txn_type = txn.txn_type;
        break;
      }
    }
    for (let txn of this.props.tripDetail.txns) {
      if(txn.refunds!==null && txn.refunds.length>0){
      for (let refund of txn.refunds) {
        if (pmntTxn.id === refund.payment_id) {
          pmntTxn.refund = refund.refund_amount;
        }
      }
      }
    }

    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let exp_id = !this.isEmpty(this.state.epRes)
      ? this.state.epRes.exp_id
      : null;
    let card_no = !this.isEmpty(this.state.epRes)
      ? this.state.epRes.card_number
      : null;
    let creditCardDtl = [];
    creditCardDtl.push(
      "Payment Mode" + ":" + this.props.paymentTypeMaster[pmntTxn.payment_type]
    );
    creditCardDtl.push("Amount" + ":" + amount);
    creditCardDtl.push("Refund" + ":" + refund);
    creditCardDtl.push("Expressway ID#" + ":" + exp_id);
    creditCardDtl.push("Card Number" + ":" + card_no);
    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );
    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );
    creditCardDtl.push("Payment Subtype" + ":" + pmntTxn.payment_subtype);
   // console.log("Pmnt===" + JSON.stringify(pmntTxn));
    return creditCardDtl.map(dtl => {
      return (
        <li>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
        </li>
      );
    });
  };

  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  componentDidMount() {
    paymentservice.getExpressCheckoutDtl(this.req).then(res => {
      let expData = JSON.parse(res.data);
      this.setState({ epRes: { ...expData.data } }, () =>
        console.log("Express===" + JSON.stringify(this.state.epRes))
      );
    });
  }

  render() {
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type -{" "}
          {typeof this.txn_type !== "undefined"
            ? this.props.txnTypeMasterData[this.txn_type]
            : null}
        </div>
        <ul>{this.displayTarnsactions(this.props.data)}</ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData
  };
};

export default connect(mapStateToProps, null)(ExpresswayTxn);
