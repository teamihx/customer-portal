import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import NetBankingTxn from "./NetBankingTxn";
import CreditDebitCrdTxn from "./CreditDebitCrdTxn";
import WalletTxn from "./WalletTxn";

class ThirdPartyPostPaid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txn_type: ""
    };
  }
  displayTarnsactions = pmntTxn => {
    //console.log("pmntTxns" + JSON.stringify(this.props.data));

    let creditCardDtl = [];
    creditCardDtl.push(
      "Payment Mode" + ":" + this.props.paymentTypeMaster[pmntTxn.payment_type]
    );
    creditCardDtl.push("Amount" + ":" + pmntTxn.amount);
    //creditCardDtl.push("Refunded" +":"+ )

    creditCardDtl.push(
      "Payment Sub Type" +
        ":" +
        this.props.paymentSubTypeMaster[pmntTxn.payment_subtype]
    );
    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );
    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );
    //console.log("Pmnt===" + JSON.stringify(pmntTxn));
    return creditCardDtl.map(dtl => {
      return (
        <li>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
        </li>
      );
    });
  };
  setTxnType = () => {
    for (let txn of this.props.tripDetail.txns) {
      let pmntTxn = this.props.data;
      if (pmntTxn.txn_id === txn.id) {
        this.setState({ txn_type: txn.txn_type });
        break;
      }
    }
  };
  componentDidMount() {
    this.setTxnType();
  }
  getDomain = domain => {
    let country = null;
    //console.log("domain===" + JSON.stringify(this.props.tripDetailSt));
    if (typeof domain !== "undefined") {
      let frm = domain.lastIndexOf(".") + 1;
      // let countryCd=domain.substring(frm,domain.length-1);
      let country = domain.substring(frm, domain.length);
      //console.log("Country===" + country);
      if (country === "com") {
        country = "in";
      }
      //console.log("Country===" + country);
      return country;
    }
  };

  render() {
    return (
      <div className="fph-payment-list">
        <ul>
          <CreditDebitCrdTxn data={this.props.data} />
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData,
    paymentSubTypeMaster: state.trpReducer.paymentSubTypeMaster
  };
};

export default connect(mapStateToProps, null)(ThirdPartyPostPaid);
