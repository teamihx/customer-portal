import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import utility from "../../common/Utilities";

class UpiPayTxn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      compRes: {}
    };
    (this.compName = ""), (this.txn_type = "");
  }
  displayTarnsactions = pmntTxn => {
   // console.log("pmntTxns" + JSON.stringify(this.props.data));

    //let companyRes=paymentservice.getCompanyNameForCashPmnt(req)
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
        this.txn_type = txn.txn_type;
        break;
      }
    }

    for (let txn of this.props.tripDetail.txns) {
     if(txn.refunds!==null && txn.refunds.length>0){
      for (let refund of txn.refunds) {
        if (pmntTxn.id === refund.payment_id) {
          pmntTxn.refund = refund.refund_amount;
        }
      }
      }
    }

    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let creditCardDtl = [];
    creditCardDtl.push(
      "Payment Mode" + ":" + this.props.paymentTypeMaster[pmntTxn.payment_type]
    );
    creditCardDtl.push("Amount" + ":" + amount);
    creditCardDtl.push("Refund" + ":" + refund);

    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );
    creditCardDtl.push("Payment Sub Type" + ":" + pmntTxn.payment_subtype);
    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );

    return creditCardDtl.map(dtl => {
      return (
        <li>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
        </li>
      );
    });
  };
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  // setTnxAndComp=()=>{
  //     return new Promise(
  //         function(resolve,reject){
  //             paymentservice.getCompanyNameForCashPmnt(this.req).then(
  //                 compRes=>{
  //                     let data=JSON.parse(compRes.data)
  //                     console.log("companyNameRes=="+JSON.stringify(data));
  //                    this.setState({compRes:{...data}},()=>console.log("companyNameRes=="+JSON.stringify(this.state.compRes)))
  //                 }
  //             )
  //         }.bind(this)
  //     )
  // }

  render() {
    /* console.log(
      "this.txn_type===" + this.props.txnTypeMasterData[this.txn_type]
    ); */
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type -{" "}
          {typeof this.txn_type !== "undefined"
            ? this.props.txnTypeMasterData[this.txn_type]
            : null}
        </div>

        <ul>{this.displayTarnsactions(this.props.data)}</ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData
  };
};

export default connect(mapStateToProps, null)(UpiPayTxn);
