import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import utility from "../../common/Utilities";
import Domainpath from '../../../services/commonUtils/Domainpath';
class DepositAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txn_type: ""
    };
  }
  displayTarnsactions = pmntTxn => {
    console.log("pmntTxns" + JSON.stringify(this.props.tripDetail));
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
        pmntTxn.txn_type = txn.txn_type;
        break;
      }
    }
    for (let txn of this.props.tripDetail.txns) {
      if(txn.refunds!==null && txn.refunds.length>0){
      for (let refund of txn.refunds) {
        if (pmntTxn.id === refund.payment_id) {
          pmntTxn.refund = refund.refund_amount;
        }
      }
      }
    }
    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let creditCardDtl = [];
    creditCardDtl.push(
      "Payment Mode" + ":" + this.props.paymentTypeMaster[pmntTxn.payment_type]
    );
    creditCardDtl.push("Amount" + ":" + amount);
    creditCardDtl.push("Refund" + ":" + refund);
    //creditCardDtl.push("Refunded" +":"+ )
    let accountId =
      pmntTxn.payment_da_details.length > 0
        ? pmntTxn.payment_da_details[0].deposit_account_detail.id
        : "Initialized";
    creditCardDtl.push("Account" + ":" + accountId);

    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );
    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );
    //console.log("Pmnt===" + JSON.stringify(pmntTxn));
    return creditCardDtl.map(dtl => {
      if (
        creditCardDtl.indexOf(dtl) === 3 &&
        dtl.substring(dtl.indexOf(":") + 1, dtl.length) !== "Initialized"
      ) {
        console.log(
          "dtl.substring(dtl.indexOf+1,dtl.length)===" +
            dtl.substring(dtl.indexOf(":") + 1, dtl.length)
        );
        return (
          <li>
            <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
            <Link
              onClick={e =>
                this.showNewPopUp(
                  dtl.substring(dtl.indexOf(":") + 1, dtl.length),
                  e
                )
              }
            >
              <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
            </Link>
          </li>
        );
      } else {
        return (
          <li>
            <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
            <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
          </li>
        );
      }
    });
  };

  showNewPopUp(dtl, e) {
    e.persist();
    let url = "";
    //console.log("e.target.id +"+ JSON.stringify(flt) )

    url = Domainpath.getHqDomainPath()+"/hq/pay/deposit/account/" + dtl;

    window.open(url);
  }
  componentDidMount() {
    this.setTxnType();
  }
  setTxnType = () => {
    for (let txn of this.props.tripDetail.txns) {
      let pmntTxn = this.props.data;
      if (pmntTxn.txn_id === txn.id) {
        this.setState({ txn_type: txn.txn_type });
        break;
      }
    }
  };
  render() {
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type - {this.props.txnTypeMasterData[this.state.txn_type]}
        </div>
        <ul>{this.displayTarnsactions(this.props.data)}</ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData
  };
};

export default connect(mapStateToProps, null)(DepositAccount);
