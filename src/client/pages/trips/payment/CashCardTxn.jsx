import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import utility from "../../common/Utilities";

class CashCardTxn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txn_type: ""
    };
  }
  displayCreditCaardTarnsactions = pmntTxn => {
    //console.log("pmntTxns" + JSON.stringify(this.props.tripDetail));
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
        pmntTxn.txn_type = txn.txn_type;
        break;
      }
    }
    for (let txn of this.props.tripDetail.txns) {
     if(txn.refunds!==null && txn.refunds.length>0){
      for (let refund of txn.refunds) {
        if (pmntTxn.id === refund.payment_id) {
          pmntTxn.refund = refund_amount;
        }
        }
      }
    }
    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let creditCardDtl = [];
    creditCardDtl.push(
      "Payment Mode" + ":" + this.props.paymentTypeMaster[pmntTxn.payment_type]
    );
    creditCardDtl.push("Amount" + ":" + amount);
    creditCardDtl.push("Refund" + ":" + refund);
    //creditCardDtl.push("Refunded" +":"+ )

    creditCardDtl.push("Billing Address" + ":" + "NA");
    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );
    let bank = this.props.CashCardsMasterData[
      pmntTxn.payment_cash_card_detail.cash_card_id
    ];
    creditCardDtl.push("Issuing Bank" + ":" + bank);
    creditCardDtl.push("Description" + ":" + pmntTxn.description);
    creditCardDtl.push("AVS Response" + ":" + "NA");
    //creditCardDtl.push("Exprees Checkout" +":" +this.props.tripDetail.express_checkout)
    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );
    //creditCardDtl.push("Cleartrip Payment" +":" )
    //console.log("Pmnt===" + JSON.stringify(creditCardDtl));
    return creditCardDtl.map(dtl => {
      return (
        <li>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span className="vertical-top">
            {dtl.substring(dtl.indexOf(":") + 1, dtl.length)}
          </span>
        </li>
      );
    });
  };

  displayCreditCardNoOfAttempt = pmntTxns => {
    console.log("pmntTxns" + JSON.stringify(pmntTxns));
    let redirectInObj = {
      created_at: pmntTxns.payment_redirect_timings[0].created_at,
      status: pmntTxns.payment_cash_card_detail.status,
      cash_card_id: pmntTxns.payment_cash_card_detail.cash_card_id,
      credential_name: pmntTxns.payment_cash_card_detail.credential_name,
      tran_type: "Redirect In",
      gateway_response1: "-",
      gateway_response2: "-"
    };
    let redirectOutObj = {
      created_at: pmntTxns.payment_redirect_timings[0].redirection_out,
      status: pmntTxns.payment_cash_card_detail.status,
      cash_card_id: pmntTxns.payment_cash_card_detail.cash_card_id,
      credential_name: pmntTxns.payment_cash_card_detail.credential_name,
      tran_type: "Redirect Out",
      gateway_response1: "-",
      gateway_response2: "-"
    };
    let redirectArray = [redirectOutObj, redirectInObj];
    let txn = redirectArray.concat(pmntTxns.payment_cash_card_detail);
    return (
      <>
        <div className="highInfo">Payment attempts</div>
        <div className="resTable">
          <table>
            <thead>
              <tr>
                <th width="18%">Time</th>
                <th width="12%">Status</th>
                <th width="8%">G/W</th>
                <th width="15%">Credential</th>
                <th width="15%">Transaction Type</th>
                <th width="15%">G/W Transaction ID</th>
                <th width="17%">G/w Response String</th>
              </tr>
            </thead>
            <tbody>
              {txn.map(txn => (
                <tr key={txn.id}>
                  <td>{dateUtil.ConvertIsoToSimpleDateFormat(txn.created_at)}</td>
                  <td>{this.props.paymentStatusMaster[txn.status]} </td>
                  <td>{this.props.CashCardsMasterData[txn.cash_card_id]}</td>
                  <td>{txn.credential_name}</td>
                  <td>
                    {typeof txn.tran_type !== "undefined"
                      ? txn.tran_type
                      : "Verification"}
                  </td>
                  <td>{txn.gateway_response1}</td>
                  <td>{txn.gateway_response2}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </>
    );
  };
  componentDidMount() {
    this.setTxnType();
  }
  setTxnType = () => {
    for (let txn of this.props.tripDetail.txns) {
      let pmntTxn = this.props.data;
      if (pmntTxn.txn_id === txn.id) {
        this.setState({ txn_type: txn.txn_type });
        break;
      }
    }
  };
  render() {
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type - {this.props.txnTypeMasterData[this.state.txn_type]}
        </div>
        <ul>{this.displayCreditCaardTarnsactions(this.props.data)}</ul>
        {this.displayCreditCardNoOfAttempt(this.props.data)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData,
    CashCardsMasterData: state.trpReducer.CashCardsMasterData
  };
};

export default connect(mapStateToProps, null)(CashCardTxn);
