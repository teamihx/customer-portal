import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import NetBankingTxn from "./NetBankingTxn";
import CreditDebitCrdTxn from "./CreditDebitCrdTxn";
import WalletTxn from "./WalletTxn";
import utility from "../../common/Utilities";

class ThirdPartyWallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txn_type: ""
    };
  }
  displayTarnsactions = pmntTxn => {
    //console.log("pmntTxns" + JSON.stringify(this.props.data));
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
        pmntTxn.txn_type = txn.txn_type;
        break;
      }
    }
    for (let txn of this.props.tripDetail.txns) {
      if(txn.refunds!==null && txn.refunds.length>0){
      for (let refund of txn.refunds) {
        if (pmntTxn.id === refund.payment_id) {
          pmntTxn.refund = refund.refund_amount;
        }
      }
      }
    }

    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let creditCardDtl = [];
    creditCardDtl.push(
      "Payment Mode" + ":" + this.props.paymentTypeMaster[pmntTxn.payment_type]
    );
    creditCardDtl.push("Amount" + ":" + amount);
    creditCardDtl.push("Refunded" + ":" + refund);

    creditCardDtl.push("Payment Sub Type" + ":" + pmntTxn.payment_subtype);
    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );
    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );
    //console.log("Pmnt===" + JSON.stringify(pmntTxn));
    return creditCardDtl.map(dtl => {
      return (
        <li>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span>{dtl.substring(dtl.indexOf(":") + 1, dtl.length)}</span>
        </li>
      );
    });
  };
  setTxnType = () => {
    for (let txn of this.props.tripDetail.txns) {
      let pmntTxn = this.props.data;
      if (pmntTxn.txn_id === txn.id) {
        this.setState({ txn_type: txn.txn_type });
        break;
      }
    }
  };
  componentDidMount() {
    this.setTxnType();
  }
  getDomain = domain => {
    let country = null;
    //console.log("domain===" + JSON.stringify(this.props.tripDetailSt));
    if (typeof domain !== "undefined") {
      let frm = domain.lastIndexOf(".") + 1;
      // let countryCd=domain.substring(frm,domain.length-1);
      let country = domain.substring(frm, domain.length);
    //  console.log("Country===" + country);
      if (country === "com") {
        country = "in";
      }
      //console.log("Country===" + country);
      return country;
    }
  };
  displayCreditCardNoOfAttempt = pmntTxns => {
    //console.log("pmntTxns" + JSON.stringify(pmntTxns));

    return pmntTxns.payment_subtype !==
      this.props.paymentSubTypeMaster["MASTERPASS_WALLET"] &&
      pmntTxns.payment_subtype !==
        this.props.paymentSubTypeMaster["PAYU_WALLET"] ? (
      <>
        <div className="highInfo">G/W Transaction ID</div>
        <div className="resTable">
          <table>
            <thead>
              <tr>
                <th width="18%">Time</th>
                <th width="18%">Updated</th>
                <th width="12%">Status</th>
                <th width="15%">Credential</th>
                <th width="15%">Description</th>
                <th width="15%">Transaction Type</th>
              </tr>
            </thead>
            <tbody>
              {pmntTxns.payment_tp_wallet_txns.map(txn => (
                <tr key={txn.id}>
                  <td>{dateUtil.ConvertIsoToSimpleDateFormat(txn.created_at)}</td>
                  <td>{dateUtil.ConvertIsoToSimpleDateFormat(txn.updated_at)}</td>
                  <td>{this.props.paymentStatusMaster[txn.status]} </td>
                  <td>{txn.credential_name}</td>
                  <td>{txn.description}</td>
                  <td>{txn.transaction_type}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </>
    ) : null;
  };
  dispLayWalletDtl = () => {
    if (
      this.props.data.payment_subtype ===
      this.props.paymentSubTypeMaster["MASTERPASS_WALLET"]
    ) {
      if (this.getDomain(this.props.tripDetail.domain) === "IN") {
        return <NetBankingTxn />;
      } else if (this.getDomain(this.props.tripDetail.domain) === "AE") {
        return <CreditDebitCrdTxn />;
      }
    } else if (
      this.props.data.payment_subtype ===
      this.props.paymentSubTypeMaster["PAYU_WALLET"]
    ) {
      return <WalletTxn />;
    } else {
      return this.displayTarnsactions(this.props.data);
    }
  };
  render() {
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type - {this.props.txnTypeMasterData[this.state.txn_type]}
        </div>

        <ul>{this.dispLayWalletDtl()}</ul>
        {this.displayCreditCardNoOfAttempt(this.props.data)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData,
    paymentSubTypeMaster: state.trpReducer.paymentSubTypeMaster
  };
};

export default connect(mapStateToProps, null)(ThirdPartyWallet);
