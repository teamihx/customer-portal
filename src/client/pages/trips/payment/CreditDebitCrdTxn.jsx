import React, { Component } from "react";
import ReactJson from "react-json-view";
import CustomScroll from "react-custom-scroll";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import * as actions from "../../../store/actions/index";
import utility from "../../common/Utilities";

class CreditDebitCrdTxn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txn_type: ""
    };
  }
  displayCreditCaardTarnsactions = pmntTxn => {
    console.log("pmntTxns" + JSON.stringify(this.props.tripDetail));
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
        pmntTxn.txn_type = txn.txn_type;
        break;
      }
    }
    for (let txn of this.props.tripDetail.txns) {
      //console.log("refunds===="+JSON.stringify(txn))
      if(txn.refunds!==null && txn.refunds.length>0){
        for (let refund of txn.refunds) {
          if (pmntTxn.id === refund.payment_id) {
            pmntTxn.refund = refund.refund_amount;
          }
        }
      }
      
    }

    let currency =
      this.props.tripDetail.currency === "INR"
        ? "Rs"
        : this.props.tripDetail.currency;

    pmntTxn.refund = typeof pmntTxn.refund === "undefined" ? 0 : pmntTxn.refund;
    let refund = utility.PriceFormat(pmntTxn.refund, currency);
    let amount = utility.PriceFormat(pmntTxn.amount, currency);
    let avsResp = "";
    for (let avsRes of pmntTxn.payment_cards_gateway_accesses) {
      if (
        avsRes.credential_name === "IN_AMEX3D" &&
        avsRes.tran_type === "AUTH"
      ) {
        avsResp = avsRes.gateway_response9;
        break;
      }
    }
    let exactMatch = "";
    let avsString = "";
    if (avsResp !== "") {
      exactMatch = this.props.AmexGatewayResponse[avsResp];
      let resp = avsResp !== "" ? avsResp : "";
      let exactStr =
        exactMatch !== "" ? exactMatch : "Unknown response code from AVS";
      avsString = "AVS Response Code: " + resp + "- Exact match -" + exactStr;
    } else {
      avsString = "Not Applicable";
    }

    let creditCardDtl = [];
    creditCardDtl.push(
      "Payment Mode" + ":" + this.props.paymentTypeMaster[pmntTxn.payment_type]
    );
    creditCardDtl.push("Amount" + ":" + amount);
    //creditCardDtl.push("Refunded" +":"+ )
    creditCardDtl.push("Refund" + ":" + refund);
    creditCardDtl.push(
      "Card Number" + ":" + pmntTxn.payment_card_details[0].card_number
    );
    let name_on_card = typeof pmntTxn.payment_card_details[0].name_on_card !=='undefined' ? pmntTxn.payment_card_details[0].name_on_card  : "-";
    creditCardDtl.push(
      "Name on card" + ":" + name_on_card
    );
    creditCardDtl.push("Billing Address" + ":" + "-");
    creditCardDtl.push(
      "Payment Status" + ":" + this.props.paymentStatusMaster[pmntTxn.status]
    );
    creditCardDtl.push(
      "Issuing Country" + ":" + pmntTxn.payment_card_details[0].country
    );
    creditCardDtl.push(
      "Issuing Bank" +
        ":" +
        this.props.BankMasterData[pmntTxn.payment_card_details[0].bank_id]
    );
    creditCardDtl.push("Description" + ":" + pmntTxn.description);
    creditCardDtl.push("AVS Response" + ":" + avsString);
    creditCardDtl.push(
      "Express Checkout" + ":" + (this.props.tripDetail.express_checkout!==null ? this.props.tripDetail.express_checkout : false)
    );
    creditCardDtl.push(
      "Merchant txn reference" + ":" + pmntTxn.merchant_txn_ref
    );
    //creditCardDtl.push("Cleartrip Payment" + ":");
    let payment_subtype =
      pmntTxn.payment_subtype !== null ? pmntTxn.payment_subtype : "-";
    creditCardDtl.push("Payment Subtype" + ":" + payment_subtype);
    let is_cp_pmnt = payment_subtype === "CP" ? "True" : "False";
    creditCardDtl.push("Cleartrip Payment" + ":" + is_cp_pmnt);
    //console.log("Pmnt===" + JSON.stringify(creditCardDtl));

    return creditCardDtl.map((dtl, index) => {
      return (
        <li key={index}>
          <span>{dtl.substring(0, dtl.indexOf(":") + 1)}</span>
          <span className="vertical-top">
            {dtl.substring(dtl.indexOf(":") + 1, dtl.length)}
          </span>
        </li>
      );
    });
  };

  displayCreditCardNoOfAttempt = pmntTxns => {
    let redirectArray =[]
    //console.log("pmntTxns" + JSON.stringify(pmntTxns));
   if( typeof pmntTxns.payment_redirect_timings[0]!=='undefined'){
      let redirectInObj = {
      
      created_at: pmntTxns.payment_redirect_timings[0].redirection_in,
      status: pmntTxns.payment_cards_gateway_accesses[0].status,
      gateway_id: pmntTxns.payment_cards_gateway_accesses[0].gateway_id,
      credential_name:
        pmntTxns.payment_cards_gateway_accesses[0].credential_name,
      tran_type: "Redirect In",
      gateway_response3: "-",
      gateway_response2: "-"
    };
    let redirectOutObj = {
      created_at: pmntTxns.payment_redirect_timings[0].redirection_out,
      status: pmntTxns.payment_cards_gateway_accesses[0].status,
      gateway_id: pmntTxns.payment_cards_gateway_accesses[0].gateway_id,
      credential_name:
        pmntTxns.payment_cards_gateway_accesses[0].credential_name,
      tran_type: "Redirect Out",
      gateway_response3: "-",
      gateway_response2: "-"
    };
     redirectArray = [redirectOutObj, redirectInObj];
   }
    
    //console.log("redirectArray=="+redirectArray);
    let payment_cards_gateway_accesses = redirectArray.concat(
      pmntTxns.payment_cards_gateway_accesses
    );
    // pmntTxns.payment_cards_gateway_accesses=[...txnArray]
    //console.log("redirectArray=="+txnArray);

    return (
      <>
        <div className="highInfo">Payment attempts</div>
        <div className="resTable">
          <table>
            <thead>
              <tr>
                <th width="18%">Time</th>
                <th width="12%">Status</th>
                <th width="8%">G/W</th>
                <th width="15%">Credential</th>
                <th width="15%">Transaction Type</th>
                <th width="15%">G/W Transaction ID</th>
                <th width="17%">G/W Response String</th>
              </tr>
            </thead>
            <tbody>
              {payment_cards_gateway_accesses.map((txn, index) => (
                <tr key={index}>
                  <td>
                    {typeof txn.created_at !== "undefined"
                      ? dateUtil.ConvertIsoToSimpleDateFormat(txn.created_at)
                      : "-"}
                  </td>
                  <td>{this.props.paymentStatusMaster[txn.status]} </td>
                  <td>
                    {typeof this.props.PaymentGatewayMaster[txn.gateway_id] !==
                    "undefined"
                      ?  this.props.PaymentGatewayMaster[txn.gateway_id]
                      : "Gateway ID# " + txn.gateway_id}
                  </td>
                  <td>{txn.credential_name}</td>
                  <td>{txn.tran_type}</td>
                  <td>{txn.gateway_response3}</td>
                  <td>
                    {typeof txn.gateway_response2 !== "undefined"
                      ? txn.gateway_response2
                      : txn.description}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </>
    );
  };
  componentDidMount() {
    this.setTxnType();
  }
  setTxnType = () => {
    for (let txn of this.props.tripDetail.txns) {
      let pmntTxn = this.props.data;
      if (pmntTxn.txn_id === txn.id) {
        this.setState({ txn_type: txn.txn_type });
        break;
      }
    }
  };
  render() {
    return (
      <div className="fph-payment-list">
        <div className="highInfo border mb-10">
          Txn Type - {this.props.txnTypeMasterData[this.state.txn_type]}
        </div>
        <ul>{this.displayCreditCaardTarnsactions(this.props.data)}</ul>

        {this.displayCreditCardNoOfAttempt(this.props.data)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData,
    BankMasterData: state.trpReducer.BankMasterData,
    PaymentGatewayMaster: state.trpReducer.PaymentGatewaysMasterData,
    AmexGatewayResponse: state.trpReducer.AmexGatewayResponse
  };
};

export default connect(mapStateToProps, null)(CreditDebitCrdTxn);
