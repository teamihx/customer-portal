import React, { Component } from "react";
import Button from "Components/buttons/button.jsx";
import Icon from "Components/Icon";
import { connect } from "react-redux";
import dateUtil from "../../../services/commonUtils/DateUtils";
import paymntService from "../../../services/trips/payment/PaymentService";
import paymentSchedule from "../../../services/paymentSchedule/paymentSchedule";
import FlightService from "../../../services/trips/flight/FlightService";
import Notification from "Components/Notification.jsx";
import CommonMsg from "Components/commonMessages/CommonMsg.jsx";
import * as actions from "../../../store/actions/index";
import EmailService from "../../../services/trips/email/EmailService";
import CreditDebitCrdTxn from "./CreditDebitCrdTxn";
import NetBankingTxn from "./NetBankingTxn";
import DepositAccount from "./DepositAccount";
import WalletTxn from "./WalletTxn";
import ExpresswayTxn from "./ExpresswayTxn";
import GiftVocherTxn from "./GiftVocherTxn";
import ThirdPartywallet from "./ThirdPartywallet";
import ThirdPartyPostPaid from "./ThirdPartyPostPaid";
import CashCardTxn from "./CashCardTxn";
import CashPaymentTxn from "./CashPaymentTxn";
import RewardPointTxn from "./RewardPointTxn";
import ApplePayTxn from "./ApplePayTxn";
import IVRPayTxn from "./IVRPayTxn";
import TechProTxn from "./TechProTxn";
export const USER_AUTH_DATA = "userAuthData";
export const TRIPS_ROLES = "tripsRoles";
import UpiPayTxn from "./UpiPayTxn";

class PaymentDetail extends Component {
  constructor(props) {
    super(props);
    //var payments=require('Assets/json/data.json')
    this.state = {
      tripDetail: this.props.tripDetail,
      currency: this.props.tripDetail.currency,
      amount: "",
      loading: false,
      type: "",
      paymentDtls: [],
      flightObj: {},
      paymentLinkDtl: {},
      copySuccess: false,
      displayNotification: false,
      encrypted_url: "",
      newTxn: [],
      txnType: "",
      msg: "",
      txnContent: "",
      payments_service_data: [],
      ftripsRoles: "",
      htripsRoles: "",
      pmntDataExist:
        this.props.tripDetail.payments_service_data.length > 0 ? true : false,
      cpyPaymentLnk: false,
      sndEmail: false,
      txnOpen: false,
      showWarning: false
    };

    this.req = {
      tripRefNumber: this.props.tripDetail.trip_ref
    };
    this.tripReq = {
      tripId: this.state.tripDetail.trip_ref
    };
    this.insertPmntTxn = {
      trip_ref: this.state.tripDetail.trip_ref,
      txn_id: 0
    };

    //this.txnOpen=false;

    //Getting ROLES DATA from local storage
    let tripsObj = JSON.parse(localStorage.getItem(TRIPS_ROLES));
    this.state.ftripsRoles = tripsObj.ftriplinks;
    this.state.htripsRoles = tripsObj.htriplinks;
    this.state.localtripRoles = tripsObj.localtripsRoles;
    this.state.trainTripsRoles = tripsObj.trainTripsRoles;
  }
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  loadDataPaymentData = () => {
    /* console.log(
      "Payment Link Object===" + JSON.stringify(this.state.paymentLinkDtl)
    ); */
    let paymentDtls = this.props.tripDetail.payments_service_data;
    let pmntMaster = this.props.paymentStatusMaster;
    let flightObj = new Object();
    this.state.currency = this.props.tripDetail.currency;
    flightObj.trip_ref = this.props.tripDetail.trip_ref;

    //console.log("Paymentstat...." + paymentDtls);
    let pmntData = [];
    for (let paymentDtl of paymentDtls) {
     // console.log("Paymentstat123....");
      let pmntObj = new Object();
      pmntObj.txn_type = paymentDtl.txn_type;
      pmntObj.currency = paymentDtl.currency;

      pmntObj.pay_mode = paymentDtl.payment_type;
      pmntObj.description = paymentDtl.description;
      let status = paymentDtl.status;
      pmntObj.status = pmntMaster[status];
      //console.log("Payment Status====" + pmntObj.status);
      pmntObj.checkout = this.props.tripDetail.express_checkout;
      pmntObj.txn_ref = paymentDtl.merchant_txn_ref;
      pmntObj.amount = paymentDtl.amount;
      pmntObj.subTyp = paymentDtl.payment_subtype;
      pmntObj.txns = [];
      // let pmntCardDtl =paymentDtl.payment_card_details[0];
      if (paymentDtl.payment_type === "CC") {
        for (let txn of paymentDtl.payment_cards_gateway_accesses) {
          let pmntTxns = new Object();
         // console.log("Paymentstat1...");
          //pmntTxns={...txn}
          pmntTxns.id = txn.id;
          pmntTxns.time = txn.created_at;

          pmntTxns.status = this.props.paymentStatusMaster[txn.status];
          pmntTxns.credential = txn.credential_name;
          pmntTxns.txn_type = txn.tran_type;
          pmntTxns.txn_id = txn.gateway_response3;
          pmntTxns.response = txn.description;
          if (txn.card_number != null) {
            pmntObj.card_no = txn.card_number;
          }
          if (txn.name_on_card != null) {
            pmntObj.name_on_card = txn.name_on_card;
          }

          pmntObj.txns.push(pmntTxns);
        }
      } else if (paymentDtl.payment_type === "TW") {
        for (let wlt of paymentDtl.payment_tp_wallet_txns) {
         // console.log("wallet12");
          let pmntTxns = new Object();
          pmntTxns = { ...wlt };
          pmntObj.txns.push(pmntTxns);
        }
      } else if (paymentDtl.payment_type === "NB") {
        let pmntNbTxn = new Object();
        /* console.log(
          "NetBanking====" +
            JSON.stringify(paymentDtl.payment_net_banking_details)
        ); */
        if (!this.isEmpty(paymentDtl.payment_net_banking_details)) {
          //let pmntNbTxn= new Object()

          pmntNbTxn = { ...paymentDtl.payment_net_banking_details };
          // let pmntTxns= new Object();
          pmntObj.txns.push(pmntNbTxn);
        }
      }
      let pmntCardDtl = paymentDtl.payment_card_details[0];
      if (typeof pmntCardDtl !== "undefined") {
        pmntObj.bank_name = pmntCardDtl.bank_id;
        pmntObj.country = pmntCardDtl.country;
        //console.log(this.state.paymentDtls);
      }
      for (let txn of this.props.tripDetail.txns) {
        if (paymentDtl.txn_id === txn.id) {
          pmntObj.pmnt_txn_type = txn.txn_type;
        }
      }
      pmntData.push(pmntObj);
    }
    //this.state.paymentDtls=[...pmntData]
    this.state.flightObj.payment_data = [...pmntData];
    /* console.log(
      "Payment---" + JSON.stringify(this.state.flightObj.payment_data)
    ); */
  };
  handleChange = evt => {
    evt.persist();
    this.setState({ [evt.target.name]: evt.target.value }, () =>
      console.log(this.state.type)
    );
  };
  loadDataPaymentDtl = pmntTxn => {
    for (let txn of this.props.tripDetail.txns) {
      if (pmntTxn.txn_id === txn.id) {
       // console.log("pmntTxn===" + JSON.stringify(txn.txn_type));
        return txn.txn_type;
      }
    }
  };
  componentDidMount() {
    this.setState({
      payments_service_data: this.props.tripDetail.payments_service_data.sort(
        (a, b) => (a.seq_no < b.seq_no ? 1 : b.seq_no < a.seq_no ? -1 : 0)
      )
    });
  }

  displayNetbkngTransactions = pmntTxns => {
    //console.log("Net bkng " + JSON.stringify(pmntTxns.txns));
    return (
      <>
        <h3>Payment attempts</h3>
        <table>
          <thead>
            <tr>
              <th>Time</th>
              <th>Status</th>
              <th>G/W</th>
              <th>Credential</th>
              <th>Transaction Type</th>
              <th>G/W Transaction ID</th>
              <th>G/w Response String</th>
            </tr>
          </thead>
          <tbody>
            {pmntTxns.txns.map(txns => (
              <tr key={txns.id}>
                <td>{dateUtil.msToTime(txns.created_at)}</td>
                <td>{this.props.paymentStatusMaster[txns.status]} </td>
                <td>{txns.bank_id}</td>
                <td>{txns.credential_name}</td>
                <td>{txns.txn_type}</td>
                <td>{txns.bank_id}</td>
                <td>{txns.response}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </>
    );
  };

  copyToClipboard = (payRes, e) => {
    //console.log('encrypted_url'+payRes.encrypted_url)
    return new Promise(function(resolve, reject) {
      var textField = document.createElement("textarea");
      textField.innerText = payRes.encrypted_url;
      document.body.appendChild(textField);
      textField.select();
      document.execCommand("copy");
      textField.remove();
      document.execCommand("copy");
      if (true) {
        resolve(textField.innerHTML);
      }
    });

    // This is just personal preference.
    // I prefer to not show the the whole text area selected.

    // this.setState({ copySuccess: true,
    //         copylink:false,
    //         paymentLinkDtl:{...payRes},
    //         encrypted_url:textField.innerHTML },
    //         ()=>{console.log('encrypted_url'+textField.innerText)});

    //this.setState({ copySuccess: true,paymentLinkDtl:{...payRes},encrypted_url:textField.innerHTML },()=>{console.log('encrypted_url'+textField.innerText)});

    //console.log("trip copied")
  };
  max = arrayToSearchIn => Math.max(...arrayToSearchIn.map(o => o.seq_no), 0);
  createNewTransaction = insertpayments => {
    let txnObj = new Object();
    txnObj.status = "O";
    txnObj.trip_id = this.props.tripDetail.payments_service_data[0].trip_id;
    txnObj.txn_type = 174;
    txnObj.user_id = this.props.tripDetail.user_id;
    let txnArr = [txnObj];
    const txnReq = {
      txns: [...txnArr]
    };
   // console.log("txnReq===" + JSON.stringify(txnReq));
    paymntService.createNewTransaction(txnReq).then(response => {
      const data = JSON.parse(response.data);
     // console.log("updatedcre response====" + JSON.stringify(data));
      let txnTopdate = data.modelsUpdated.txns[0];
      let updatedTripDtl = this.props.tripDetail.txns.push(txnTopdate);
      this.props.updateTxnsDetails(updatedTripDtl);
    });
    insertpayments();
    //console.log("txnRes==="+JSON.stringify(txnRes))
  };
  insertPayments = () => {
    let txnArr = this.props.tripDetail.txns;
    let currentTxn = txnArr[txnArr.length - 1];
    let payment = new Object();
    payment.seq_no = this.max(this.props.tripDetail.txns).seq_no;
    payment.trip_id = this.props.tripDetail.payments_service_data[0].trip_id;
    payment.app_ref1 = this.props.tripDetail.trip_ref;
    payment.app_ref2 = currentTxn.id;
    payment.payment_type = "CP";
    payment.payment_subtype = "CP";
    payment.amount = this.state.amount;
    payment.currency = this.state.currency;
    let payment_category = this.state.type === "fare Difference" ? "B" : "A";
    payment.payment_category = payment_category;
    payment.order_info1 = this.props.tripDetail.payments_service_data[0].order_info1;
    payment.order_info2 = this.props.tripDetail.payments_service_data[0].order_info2;
    let insertObj = { ...payment };
    let insertReq = [insertObj];
   // console.log("insertReq===" + JSON.stringify(insertReq));
  };
  copyPaymentLink = req => {
    //this.setState({ copylink: true });
    this.setState({ cpyPaymentLnk: true, copylink: true }, () =>
      console.log("cpyPaymentLnk==" + this.state.cpyPaymentLnk)
    );
    //console.log("")
    let txnOpen = false;
    for (let txn of this.state.tripDetail.txns) {
      if (txn.status === "O") {
        txnOpen = true;
        this.setState({
          displayNotification: true,
          cpyPaymentLnk: false,
          sndEmail: false,
          showWarning: true
        });
      }
    }
    // console.log("txns==="+this.state.txnOpen)
    if (!txnOpen) {
      //this.txnOpen=true;
      this.getPaymentLink(req).then(res => {
        console.log("link===" + res);
        this.props.onInitTripDtl(this.tripReq);
      });
    } else {
      this.setState({
        msg: "Some transactions are open.Please close those transactions.",
        copylink: false
      });
      // this.setState({ copylink: false });
    }
  };
  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };
  displayMsg = msgsent => {
    return new Promise(
      function(resolve, reject) {
        //console.log("display msg");

        //console.log("this.cpyPaymentLnk="+this.cpyPaymentLnk);
        if (msgsent) {
          if (this.state.cpyPaymentLnk) {
            this.setState(
              {
                msg:
                  " Following payment link is copied to your clipboard - " +
                  this.state.encrypted_url,
                showWarning: false
              },
              () => console.log(this.state.msg)
            );
          }
          if (!this.state.cpyPaymentLnk) {
            this.setState(
              {
                msg:
                  "An Email is sent to your mail id.Please check your inbox ",
                showWarning: false
              },
              () => console.log(this.state.msg)
            );
          }
        } else {
          if (this.state.cpyPaymentLnk) {
            this.setState(
              {
                msg: "Payment link is not copied.Please try again",
                showWarning: true
              },
              () => console.log(this.state.msg)
            );
          }
          if (!this.state.cpyPaymentLnk) {
            this.setState(
              {
                msg: "Sending Email is failed.Please try again",
                showWarning: true
              },
              () => console.log(this.state.msg)
            );
          }
        }

        if (true) {
          resolve(this.state.msg);
        }
      }.bind(this)
    );
  };
  getPaymentLink = req => {
    return new Promise(
      function(resolve, reject) {
        let txnObj = new Object();
        txnObj.status = "O";
        txnObj.trip_id = this.state.tripDetail.payments_service_data[0].trip_id;
        txnObj.txn_type = 174;
        txnObj.source_type = "HQ";
        // var authData = localStorage.getItem(USER_AUTH_DATA);
        //var obj = JSON.parse(authData);
        //let userid = obj.data.id;
        //txnObj.user_id = userid;
        let txnArr = [txnObj];
        let txnTopdate = [];
        const txnReq = {
          txns: [...txnArr]
        };
       // console.log("txnReq===" + JSON.stringify(txnReq));
        paymntService.createNewTransaction(txnReq).then(response => {
          //console.log("txnRes===" + JSON.stringify(response.data));
          const data = JSON.parse(response.data);
          //console.log("txnRes===" + JSON.stringify(response.data));
          if (data.status === 200) {
            txnTopdate = data.modelsUpdated.txns[0];

            let payment = new Object();
            payment.seq_no =
              this.max(this.props.tripDetail.payments_service_data) + 1;
            payment.trip_id = this.props.tripDetail.payments_service_data[0].trip_id;
            payment.app_ref1 = this.props.tripDetail.trip_ref;
            payment.app_ref2 = txnTopdate.id;
            payment.payment_type = "CP";
            payment.payment_subtype = "CP";
            payment.amount = this.state.amount;
            payment.currency = this.state.currency;
            let payment_category =
              this.state.type === "fare Difference" ? "B" : "A";
            payment.payment_category = payment_category;
            payment.order_info1 = this.props.tripDetail.payments_service_data[0].order_info1;
            payment.order_info2 = this.props.tripDetail.payments_service_data[0].order_info2;
            let insertObj = new Object();
            insertObj.payment = { ...payment };
            let insertReq = [insertObj];
            paymntService.insertPayment(insertReq).then(response => {
              this.setState({ copylink: false });
              const data = JSON.parse(response.data);
             // console.log("updated response=data===" + JSON.stringify(data));
              if (data.msg === "SUCCESS") {
                // console.log("insertReq===="+JSON.stringify(insertReq))
                //console.log("updated response===="+JSON.stringify(data))
                if (!this.isEmpty(data)) {
                  paymntService.getPaymentLink(req).then(response => {
                    this.setState({ copylink: false });
                    const payRes = JSON.parse(response.data);
                    payRes.amount = this.state.amount;
                    payRes.currency = this.state.currency;
                    payRes.type = this.state.type;
                    let email = new Object();
                    email.mailContent = this.props.tripDetail;
                    //let mailJSONRq=new Object();
                    /* console.log(
                      "paymentlink detail---:" + JSON.stringify(payRes)
                    ); */
                    this.state.sndEmail
                      ? this.createEmailJSON(payRes, email).then(res => {
                          // console.log("sendFightDetailEmailReq---:"+JSON.stringify(email));
                          EmailService.sendFightDetailEmail(email).then(res => {
                            /* console.log(
                              "emailResponsr===" +
                                JSON.stringify(res.description)
                            ); */
                            if (res.description !== "Unable to send Email") {
                              let data = JSON.parse(res.description);

                              if (data.status === "ACCEPTED") {
                                this.insertPmntTxn.txn_id = txnTopdate.id;
                                this.setState({ copySuccess: true }, () =>
                                  console.log(
                                    "copySuccess" + this.state.copySuccess
                                  )
                                );
                                //this.setState({msg:"An email is sent to your mail id.Please check your inbox"},()=>console.log("msg==="+this.state.msg))
                                this.saveTransaction(this.insertPmntTxn).then(
                                  res => {
                                    this.displayMsg(true).then(res => {
                                      if (this.state.msg.length > 0) {
                                        resolve(this.state.msg);
                                      }
                                    });
                                  }
                                );
                              } else {
                                this.displayMsg(false).then(res => {
                                  if (this.state.msg.length > 0) {
                                    resolve(this.state.msg);
                                  }
                                });
                              }
                            } else {
                              this.displayMsg(false).then(res => {
                                if (this.state.msg.length > 0) {
                                  resolve(this.state.msg);
                                }
                              });
                            }
                          });
                        })
                      : this.copyToClipboard(payRes, Event).then(res => {
                          console.log(
                            "copyToClipboard===" + JSON.stringify(res)
                          );
                          this.insertPmntTxn.txn_id = txnTopdate.id;
                          res.length > 0
                            ? this.setState(
                                { encrypted_url: res },
                                this.setState({ copySuccess: true })
                              )
                            : null;
                          this.saveTransaction(this.insertPmntTxn).then(res => {
                            //console.log("txn insert===" + res);
                            this.displayMsg(true).then(res => {
                              if (this.state.msg.length > 0) {
                                resolve(this.state.msg);
                              }
                            });
                          });
                        });
                  });
                } else {
                  this.displayMsg(false).then(res => {
                    if (this.state.msg.length > 0) {
                      resolve(this.state.msg);
                    }
                  });
                }
              } else {
                this.displayMsg(false).then(res => {
                  if (this.state.msg.length > 0) {
                    resolve(this.state.msg);
                  }
                });
              }
            });
          } else {
            this.displayMsg(false).then(res => {
              if (this.state.msg.length > 0) {
                resolve(this.state.msg);
              }
            });
          }
        });
      }.bind(this)
    );
  };
  sendPaymentLink = req => {
    //let txnOpen = false;
    this.setState({ sndEmail: true }, () =>
      console.log("sndEmail==" + this.state.sndEmail)
    );
    let txnOpen = false;
    for (let txn of this.state.tripDetail.txns) {
      if (txn.status === "O") {
        //console.log("txn==" + txn.status);
        txnOpen = true;
        this.setState(
          {
            displayNotification: true,
            cpyPaymentLnk: false,
            sndEmail: false,
            showWarning: true
          },
          () => console.log(this.state.txnOpen)
        );
        break;
      }
    }
    //console.log("txn=="+this.state.txnOpen)
    if (!txnOpen) {
      this.setState({ sendPlink: true });
      //this.txnOpen = true
      this.getPaymentLink(req).then(res => {
        console.log("link===" + res);
        this.props.onInitTripDtl(this.tripReq);
      });
    } else {
      this.setState({
        msg: "Some transactions are open.Please close those transactions.",
        sendPlink: false
      });
    }
  };
  createEmailJSON = (pmntLink, email) => {
    //console.log("payment=====")
    return new Promise(
      function(resolve, reject) {
        let tripObject = new Object();
        let tripDtl = this.props.tripDetail;
        if (this.props.tripDetail.trip_type === 1) {
          for (let flight of tripDtl.air_bookings[0].flights) {
            flight.air;
            flight.departure_city = FlightService.getCityFromAirPorteCode(
              this.props.airPortMaster,
              flight.departure_airport
            );
            flight.arrival_city = FlightService.getCityFromAirPorteCode(
              this.props.airPortMaster,
              flight.arrival_airport
            );
            flight.daparture_date = dateUtil.prettyDate(
              flight.departure_date_time,
              "hh:mm A,ddd,MMM DD YYYY "
            );
            for (let segment of flight.segments) {
              segment.airLine_name = FlightService.getAirlineNameFromAirLineCode(
                this.props.airLineMaster,
                segment.operating_airline
              );
              segment.departure_date = dateUtil.prettyDate(
                segment.departure_date_time,
                "ddd,MMM DD YYYY "
              );
              segment.departure_time = dateUtil.prettyDate(
                segment.departure_date_time,
                "HH:mm "
              );
              segment.departure_city = FlightService.getCityFromAirPorteCode(
                this.props.airPortMaster,
                segment.departure_airport
              );
              segment.arrival_city = FlightService.getCityFromAirPorteCode(
                this.props.airPortMaster,
                segment.arrival_airport
              );
              segment.arrival_date = dateUtil.prettyDate(
                segment.arrival_date_time,
                "ddd,MMM DD YYYY "
              );
              segment.arrival_time = dateUtil.prettyDate(
                segment.arrival_date_time,
                "HH:mm "
              );
              segment.duration = dateUtil.secondsToHm(segment.duration);
              segment.departure_airport_name = FlightService.getAirPortNameFromAirPorteCode(
                this.props.airPortMaster,
                segment.departure_airport
              );
              segment.arrival_airport_name = FlightService.getAirPortNameFromAirPorteCode(
                this.props.airPortMaster,
                segment.arrival_airport
              );
              segment.departure_terminal =
                segment.departure_terminal !== null &&
                typeof segment.departure_terminal !== "undefined"
                  ? segment.departure_terminal
                  : " ";
              segment.arrival_terminal =
                segment.arrival_terminal !== null &&
                typeof segment.arrival_terminal !== "undefined"
                  ? segment.arrival_terminal
                  : " ";
            }
          }
          tripObject.trip = { ...tripDtl };
          tripObject.trip.charge = pmntLink.amount;
          tripObject.trip.charge_currency = pmntLink.currency;
          tripObject.trip.type = pmntLink.type;
          tripObject.trip.payment_link = pmntLink.encrypted_url;
          let user = this.props.usermasterdetails[
            tripObject.trip.booked_user_id
          ].personal_data.first_name;
          tripObject.trip.user_name = user;
          tripObject.trip.contentHost = process.env.s3bucket;
          //let email = new Object();
          email.from = "no-reply@cleartrip.com";
          let toList = [];
          let to_user = this.props.usermasterdetails[
            tripObject.trip.booked_user_id
          ].username;
          //let to = email.mailContent.contact_detail.email;
          toList.push(to_user);
          email.to = toList;
          // email.contentHost=process.env.s3bucket
          email.subject =
            " Make payment for your" +
            " " +
            this.props.tripDetail.trip_name +
            " " +
            "trip ID " +
            this.props.tripDetail.trip_ref;
          //let air_bookings =email.mailContent.air_bookings[0]
          //email.subject = "eReceipt for "+firstsegment+"-"+lastsegment+"(Trip ID: "+flightJson.trip.tripRef +")";
          //let tripReq =JSON.stringify(tripObject)
          // console.log('Email req'+tripReq[trip]);

          email.mailContent = JSON.stringify(tripObject);

          email.useTemplate = "true";
          email.category = "bo-portal-pay-link";
          console.log("Email req" + email.mailContent);
        } else if (this.props.tripDetail.trip_type === 2) {
          tripObject.trip = { ...tripDtl };
          tripObject.trip.charge = pmntLink.amount;
          tripObject.trip.charge_currency = pmntLink.currency;
          tripObject.trip.type = pmntLink.type;
          tripObject.trip.payment_link = pmntLink.encrypted_url;
          let user = this.props.usermasterdetails[tripObject.trip.user_id]
            .personal_data.first_name;
          tripObject.trip.user_name = user;
          tripObject.trip.contentHost = process.env.s3bucket;
          //let email = new Object();
          email.from = "no-reply@cleartrip.com";
          let toList = [];
          let to = email.mailContent.contact_detail.email;
          toList.push(to);
          email.to = toList;
          // email.contentHost=process.env.s3bucket

          email.subject =
            " Make payment for your" +
            " " +
            this.props.tripDetail.trip_name +
            " " +
            "trip ID " +
            this.props.tripDetail.trip_ref;
          //let air_bookings =email.mailContent.air_bookings[0]
          //email.subject = "eReceipt for "+firstsegment+"-"+lastsegment+"(Trip ID: "+flightJson.trip.tripRef +")";
          //let tripReq =JSON.stringify(tripObject)
          // console.log('Email req'+tripReq[trip]);
          let htl = this.props.tripDetail.hotel_bookings[0];
          let duration = Math.abs(
            new Date(htl.check_out_date) - new Date(htl.check_in_date)
          );
          let one_day = 1000 * 60 * 60 * 24;
          let no_nights = Math.round(duration / one_day);
          tripObject.trip.hotel_booking_detail = {
            hotel_name: htl.hotel_detail.name,
            hotel_address: htl.hotel_detail.address,
            hotel_bkng_dtl:
              htl.room_count + " " + "for" + " " + no_nights + " " + "nights",
            from_to:
              " from" +
              dateUtil.prettyDate(htl.check_in_date, "ddd,MMM DD YYYY ") +
              " to" +
              dateUtil.prettyDate(htl.check_out_date, "ddd,MMM DD YYYY "),
            room_dtl: htl.room_types[0].name
          };
          email.mailContent = JSON.stringify(tripObject);

          email.useTemplate = "true";
          email.category = "bo-portal-pay-link_hotel";
          console.log("Email req====" + email.mailContent);
        }
        //console.log('Trip dtl'+tripDtl)
        //let tripDtl=this.props.tripDetail;

        if (true) {
          resolve(JSON.stringify(email));
        }
      }.bind(this)
    );
    //EmailService.sendFightDetailEmail(email)
  };
  saveTransaction = () => {
    return new Promise(
      function(resolve, reject) {
        //console.log("Save transaction statrts");
        paymentSchedule.insertPaymentTxn(this.insertPmntTxn);
        resolve("Value ara saved");
      }.bind(this)
    );
  };

  onCopy = () => {
    this.setState({ copied: true });
    this.setState({ copylink: false });
  };

  copyMessage = () => {
    let msg = "";
    {
      msg =
        " Below payment link is copied to clipboard - #" +
        this.state.encrypted_url;
    }
    return msg;
  };

  toggleAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  // componentWillReceiveProps(nextProps){
  //   console.log("componentWillReceiveProps+" + this.state.cpyPaymentLnk + this.state.sndEmail + this.state.msg.length)
  //   if(!(this.state.cpyPaymentLnk || this.state.sndEmail)){
  //     this.setState({tripDetail:nextProps.tripDetail,displayNotification:false,msg:"",txnOpen:false},()=>console.log("componentWillReceiveProps+"+JSON.stringify(this.state.tripDetail)))
  //   }else  {
  //     this.setState({tripDetail:nextProps.tripDetail,sendPlink: false,copySuccess:false,cpyPaymentLnk:false,sndEmail:false,displayNotification:true,txnOpen:false},()=>console.log("componentWillReceiveProps+"+JSON.stringify(this.state.tripDetail)))
  //   }

  // }
  static getDerivedStateFromProps(nextProps, prevState) {
    //console.log("componentWillReceiveProps+"  + nextProps.tripDetail===prevState.tripDetail)
    if (nextProps.tripDetail !== prevState.tripDetail) {
      /* console.log(
        "componentWillReceiveProps+" +
          prevState.cpyPaymentLnk +
          prevState.sndEmail
      ); */
      if (!(prevState.cpyPaymentLnk || prevState.sndEmail)) {
        return {
          tripDetail: nextProps.tripDetail,
          displayNotification: false,
          msg: "",
          txnOpen: false
        };
      } else {
        return {
          tripDetail: nextProps.tripDetail,
          payments_service_data: nextProps.tripDetail.payments_service_data.sort(
            (a, b) => (a.seq_no < b.seq_no ? 1 : b.seq_no < a.seq_no ? -1 : 0)
          ),
          sendPlink: false,
          copySuccess: false,
          cpyPaymentLnk: false,
          sndEmail: false,
          displayNotification: true,
          txnOpen: false
        };
      }
    } else {
      return null;
    }
  }
  render() {
    // {this.loadDataPaymentData()}
    //this.loadDataPaymentDtl()
    //console.log("this.state.txnType"+)
    return (
      <>
        {this.state.pmntDataExist ? (
          <div key={this.state.pmntDataExist}>
            {this.state.tripDetail.trip_type !== 4 && this.state.tripDetail.trip_type !== 16 && (
              <>
                <div
                  className="highInfo border mb-10"
                  key={this.state.msg.toString()}
                >
                  Send ct-pay email to the customer
                </div>
                <>
                  {this.state.displayNotification ? (
                    <div>
                      {" "}
                      {this.state.showWarning ? (
                        this.state.msg.length > 0 ? (
                          <div className="alert alert-warning">
                            <Icon
                              className="noticicon"
                              color="#cda11e"
                              size={16}
                              icon="warning"
                            />
                            {this.state.msg}
                          </div>
                        ) : null
                      ) : null}
                      {!this.state.showWarning ? (
                        <div className="alert alert-info">
                          <Icon
                            className="noticicon"
                            color="#365aa1"
                            size={16}
                            icon="info2"
                          />
                          {this.state.msg}
                        </div>
                      ) : null}
                    </div>
                  ) : (
                    ""
                  )}
                </>
                <div className="form-group pt-15">
                  <div className="row">
                    <div className="col-1">
                      <input
                        id="curId"
                        type="text"
                        name="currency"
                        value={this.state.currency}
                        readOnly
                      />
                    </div>
                    <div className="col-3">
                      <input
                        id="numId"
                        type="number"
                        name="amount"
                        value={this.state.amount}
                        step={this.state.step}
                        placeholder="Enter Amount"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="col-3">
                      <div className="custom-select-v3">
                        <Icon
                          className="select-me"
                          color="#CAD6E3"
                          size={20}
                          icon="down-arrow"
                        />
                        <select
                          className="form-control"
                          id="typeId"
                          name="type"
                          value={this.state.type}
                          onChange={this.handleChange}
                        >
                          <option value="Select Type">---Select Type---</option>
                          <option value="fare Differnce">
                            Fare Difference
                          </option>
                          <option value="Amendment Fee">Amendment Fee</option>
                        </select>
                      </div>
                    </div>

                    <div className="col-5">
                      {((this.props.tripDetail.trip_type === 1 &&
                        this.state.ftripsRoles.SEND_PAY_LINK_ROLE_ENABLE) ||
                        (this.props.tripDetail.trip_type === 2 &&
                          this.state.htripsRoles
                            .HTL_SEND_PAY_LINK_ROLE_ENABLE) ||
                        (this.props.tripDetail.trip_type === 16 &&
                          this.state.localtripRoles
                            .LOCAL_SEND_PAYMENT_ROLE_ENABLE) ||
                        (this.props.tripDetail.trip_type === 4 &&
                          this.state.trainTripsRoles
                            .TRAIN_SEND_PAYMENT_ROLE_ENABLE)) && (
                        <Button
                          size="xs"
                          viewType="primary"
                          className="w-143"
                          loading={this.state.sendPlink}
                          onClickBtn={() => this.sendPaymentLink(this.req)}
                          btnTitle="Send Payment Link"
                        ></Button>
                      )}
                      {((this.props.tripDetail.trip_type === 1 &&
                        this.state.ftripsRoles.COPY_PAY_LINK_ROLE_ENABLE) ||
                        (this.props.tripDetail.trip_type === 2 &&
                          this.state.htripsRoles
                            .HTL_COPY_PAY_LINK_ROLE_ENABLE) ||
                        (this.props.tripDetail.trip_type === 16 &&
                          this.state.localtripRoles
                            .LOCAL_COPY_PAYMENT_ROLE_ENABLE) ||
                        (this.props.tripDetail.trip_type === 4 &&
                          this.state.trainTripsRoles
                            .TRAIN_COPY_PAYMENT_ROLE_ENABLE)) && (
                        <Button
                          size="xs"
                          viewType="primary"
                          className="w-143"
                          loading={this.state.copylink}
                          btnTitle="Copy Payment Link"
                          onClickBtn={() => this.copyPaymentLink(this.req)}
                        ></Button>
                      )}
                    </div>

                    {/* <div className="col-12 mb-10"><h4>Below payment link is copied to clipboard</h4>
    <span>{this.state.encrypted_url}</span> </div>:null} */}
                  </div>
                </div>
              </>
            )}

            {this.state.payments_service_data.map((pmntTxns, index) => {
              //{this.loadTransactionContent(pmntTxns)}

              let txnType = this.loadDataPaymentDtl(pmntTxns);
              if (txnType !== 174) {
                switch (pmntTxns.payment_type) {
                  case "CC": {
                    return <CreditDebitCrdTxn data={pmntTxns} key={index} />;
                  }
                  case "DC": {
                    return <CreditDebitCrdTxn data={pmntTxns} key={index} />;
                  }
                  case "NB": {
                    return <NetBankingTxn data={pmntTxns} key={index} />;
                  }
                  case "DA": {
                    return <DepositAccount data={pmntTxns} key={index} />;
                  }
                  case "WT": {
                    return <WalletTxn data={pmntTxns} key={index} />;
                  }
                  case "EP": {
                    return <ExpresswayTxn data={pmntTxns} key={index} />;
                  }
                  case "GV": {
                    return <GiftVocherTxn data={pmntTxns} key={index} />;
                  }
                  case "TW": {
                    return <ThirdPartywallet data={pmntTxns} key={index} />;
                  }
                  case "TW": {
                    return <ThirdPartywallet data={pmntTxns} key={index} />;
                  }
                  case "KC": {
                    return <CashCardTxn data={pmntTxns} key={index} />;
                  }
                  case "CA": {
                    return <CashPaymentTxn data={pmntTxns} key={index} />;
                  }
                  case "RP": {
                    return <RewardPointTxn data={pmntTxns} key={index} />;
                  }
                  case "AP": {
                    return <ApplePayTxn data={pmntTxns} key={index} />;
                  }
                  case "IV": {
                    return <IVRPayTxn data={pmntTxns} key={index} />;
                  }
                  case "TC": {
                    return <TechProTxn data={pmntTxns} key={index} />;
                  }
                  case "UP": {
                    return <UpiPayTxn data={pmntTxns} key={index} />;
                  }
                }
              } else {
                return (
                  <div className="mb-5 font-12">
                    Txn Type:Sent Ct-Payment Link
                  </div>
                );
              }
              //<div> {this.displayCreditCaardTarnsactions(pmntTxns)}</div>
            })}
          </div>
        ) : (
          <CommonMsg
            errorType="datanotFound"
            errorMessageText="no payment details found"
          ></CommonMsg>
        )}
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    airLineMaster: state.trpReducer.airLineMaster,
    airPortMaster: state.trpReducer.airPortMaster,
    bkngSatatusMaster: state.trpReducer.bkngSatatusMaster,
    paymentStatusMaster: state.trpReducer.paymentStatusMaster,
    paymentTypeMaster: state.trpReducer.paymentTypeMaster,
    cabinTypeMaster: state.trpReducer.cabinTypeMaster,
    txnTypeMasterData: state.trpReducer.txnTypeMasterData,
    paymntSubTypeMaster: state.trpReducer.paymntSubTypeMaster,
    usermasterdetails: state.trpReducer.usermasterdetails
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onInitTripDtl: req => dispatch(actions.initTripDtl(req)),
    updateTxnsDetails: txns => dispatch(actions.updateTxnsDetails(txns))
    //onInitAirLineMaster:(req)=>dispatch(actions.initAirLineMasterData(req)),
    //onInitAirPortMaster:(req)=>dispatch(actions.initAirPortMasterData(req))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(PaymentDetail);
