import React, { Component } from 'react'
import Button from 'Components/buttons/button.jsx'
import Icon from 'Components/Icon';
let paymentStatusMaster=require('Assets/json/PaymentTypeMasterData.json');

class FPHPayment extends Component {

    constructor(props) {
        super(props)
        var payments=require('Assets/json/data.json')
        this.state = {
               pmntTypData:paymentStatusMaster,
               currency:this.props.pmntDtl[0].currency,
               amount:'',
               type:""
           
          
        }
        
    }
    handleChange=(evt)=>{
        evt.persist();
         this.setState({[evt.target.name]:evt.target.value},()=>console.log(this.state[evt.target.name]))
         
    }
    render() {
        
        return (
            <>

            <div>
            <h3>Send ct-pay email to the customer</h3>
            <form className="form-group pt-15" onSubmit={this.handleSubmit}>
            <div className="row">
            <div className="col-2">
             <input id="curId"type="text" name ="currency" value={this.state.currency} readOnly/>
             </div>
             <div className="col-3">
             <input id ="numId" type="number" name ="amount"
           value={this.state.amount} 
           step={this.state.step} placeholder="Enter Amount"
           onChange={this.handleChange} />
           </div>
           <div className="col-3">
           <div className="custom-select-v3">
           <Icon className="select-me"  color="#CAD6E3" size={20} icon="down-arrow"/>
           <select className="form-control" id="typeId"
             name="type" value={this.state.type }
             onChange={ this.handleChange} >
            <option value="Select Type" >---Select Type---</option>
            <option value="fare Differnce" >Fare Difference</option>
            <option value="Amendment Fee">Amendment Fee</option>

                  </select>
                  </div>
                  </div>
                  <div className="col-4">
            <Button size="xs"
      		 viewType="primary"
      		 className="w-143"
      		 btnTitle='Send Payment Link'>                    
      	    </Button>

            <Button size="xs"
      		 viewType="primary"
            className="w-143"
      		 btnTitle='Copy Payment Link'>                    
      	    </Button>

              </div>
              </div>
             </form>
             {this.props.pmntDtl.map(pmnt=><><h3>Txn Type: New Trip Booked</h3>
             <div className="row">
            <div className="col-6 fph-payment-list mb-30">            
            <ul>
                <li className="d-b"><span>Payment Mode </span> : {this.state.pmntTypData[pmnt.pay_mode]}</li>
                <li><span>Amount </span> : {pmnt.amount}</li>
                <li><span>Refunded </span> : </li>
                <li><span>Card Number </span> : {pmnt.card_no} </li>
                <li><span>Name On Card </span> : {pmnt.name_on_card}</li>
                <li><span>Billing Address </span> : </li>
                <li><span>Payment Status </span> : {pmnt.status}</li>
                <li><span>Issuing Country </span> : {pmnt.country} </li>
                <li><span>Issuing Bank </span> : {pmnt.bank_name}</li>
                <li><span>Description </span> : {pmnt.description}</li>
                <li><span>Avs Response </span> :</li>
                <li><span>Express Checkout </span> : {pmnt.checkout}</li>
                <li><span>Merchant txn Referrence  </span>: {pmnt.txn_ref}</li>
                <li><span>Cleartrip Payment </span> : </li>
                <li><span>Payment Subtype </span> : </li>
            </ul>
            </div>
            </div>
            <h3>Payment attempts</h3>
            <table>
            <thead>
                 <tr>
                     <th>Time</th><th>Status</th><th>G/W</th><th>Credential</th><th>Transaction Type</th><th>G/W Transaction ID</th>
                     <th>G/w Response String</th>
                     </tr>
            </thead>
            <tbody>
                 { 
                    pmnt.txns.map(txn=><tr key={txn.id}>
                     <td>{txn.time}</td>
                     <td>{txn.status} </td>
                     <td></td>
                     <td>{txn.credential}</td>
                     <td>{txn.txn_type}</td>
                     <td>{txn.txn_id}</td>
                     <td>{txn.response}</td>
                     
                    </tr>

                 )
                 }
                 
             </tbody>
            </table></>)}
             

            </div>
           
</>

        )
    }


}

export default FPHPayment;