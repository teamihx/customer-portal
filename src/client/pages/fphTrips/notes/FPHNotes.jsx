import React, { Component } from 'react'
import {MONTHS, DAYS} from 'Pages/common/constants.js'
class FPHNotes extends Component {

    
    constructor(props) {
        super(props)        
        this.addNoteHandler=this.addNoteHandler.bind(this)
        this.updateTripNotes=this.updateTripNotes.bind(this)
        this.handleChangeAddnote=this.handleChangeAddnote.bind(this)
        
       // var tripNotes1=JSON.parse(data);
       // console.log(tripNotes1);
        this.state = {
            fphData: this.props.FPHNoteData,
            minirules:this.props.FPHNoteData.air_bookings[0].minirule_info,
            baggage: this.props.FPHNoteData.air_bookings[0].free_baggage_info,
            tripNotes:(Object.values(this.props.FPHNoteData.notes)),
            newNote:'',
            errorMesssage:''
            //tripNotes: notes.sort((a,b)=>new Date(a.created_at)-new Date(b.created_at))
        }
     
    }
    componentDidMount(){
        console.log(' trip id: '+this.props.tripid)
        this.setState({
                
            tripNotes:this.state.tripNotes.sort((a,b)=>{
                return new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
            }).reverse()
        })
    }
    
    // componentWillUpdate(){
    //    this.setState({
    //     tripNotes :this.state.tripNotes.sort((a,b)=> b.created_at.valueOf-a.created_at.valueOf)
           
           
    //     })
    //     console.log(this.state.tripNotes);

    // }
    setErrorMessage(message){
        this.setState({
            errorMesssage:message
        })
    }
    handleChangeAddnote(event){
        this.setState({
            newNote:event.target.value
        })

      

    }
    
     updateTripNotes(respnote){
        this.setState({
           //tripNotes:this.state.tripNotes.concat(respnote)
            //[...this.state.tripNotes,respnote]
            tripNotes:this.state.tripNotes.concat(respnote).sort((a,b)=>
                ( new Date(a.created_at).getTime() - new Date(b.created_at).getTime())
            ).reverse(),
            newNote:''
            
        })
        console.log(this.state)
    }
   
    addNoteHandler(event){
        console.log('in addNote handle')
        if(this.state.newNote!==''){
            //call function to update
            //var jsonRequest = new FormData();
            // var message = document.getElementById('addNoteID');

            const noteTobeAdded={
                notes :[
                    {
                    note: this.state.newNote,
                    parent_note_id: null,
                    subject:this.props.subject,
                    user_id:0,
                    trip_id:this.props.tripid
                    } 
                ]   
            };
            console.log( 'request'+noteTobeAdded.notes[0].trip_id+''+this.state.newNote)
        // jsonRequest.append('json',JSON.stringify(noteTobeAdded));
        //var proxy='https://cors-anywhere.herokuapp.com/'
        // var target='http://172.17.26.11:9031/trips'
        //header("Access-Control-Allow-Origin: *");
            // fetch(target,{
            //     6method:'POST',
            //     header:{'Content-Type': 'multipart/form-data','Access-Control-Allow-Origin': '*'},
            //     body:noteTobeAdded
            // }).then(function(response) {
            //     return response.json()
            //   }).then(function(body) {
            //     console.log(body);
            //     return(<div><h1>{body}</h1></div>);
            //   }).catch(e => {
            //     console.log(e);
            //     return e;
            //   });
            var response=require('./response.json')
            if(response.updateStatus==="true"){
                console.log(new Date().toISOString())
                console.log('response from json {response.notes}')
                console.log(response.modelsUpdated.notes)
                this.updateTripNotes(response.modelsUpdated.notes[0]);
        }
        else{
            var message='Enter the  note  to be added'
            this.setErrorMessage(message)
        }
                
        }
        else{
             message='Enter the  note  to be added'
            this.setErrorMessage(message)
        }
}
    
    
    render() {
        var finalData = this.state.baggage; 
        return (
            <>
            <div className=''>
               <h5>{this.state.errorMesssage}</h5>
             <h3 >Add a note to this trip</h3>
             <textarea id ='addNoteID' type='text' value={this.state.newNote} onChange={this.handleChangeAddnote} ></textarea>
             <div className="btnSec">
             <button className="btn btn-xs btn-primary" onClick ={this.addNoteHandler}>Add this note</button>
             
             </div>

            </div>
            <h3>Mini Rule and Baggage info</h3>
            <div className ='minirule'>
            <table>
                          <tr>
                             <td width="10%">Note</td>
                             <td width="80%">
                        
                            <p className="font-12">{this.state.minirules}</p>
                             <p className="font-12"> {finalData} </p>
                             </td>                           
                          </tr>

                      </table> 
            </div>
             <div>
                <h3>Trip notes</h3>
                
               {this.state.tripNotes.map((trip) => {
                   const createdDate = new Date(trip.created_at)
                   const fomattedData = `${MONTHS[createdDate.getMonth()]} ${createdDate.getDate()}
                   ${createdDate.getHours()}:  ${createdDate.getMinutes()} `
                   return(
                      <table>
                          <tr key ={trip.id}>
                             <td width="10%">Note</td>
                             <td width="80%">
                            <span className="d-b mb-5">{trip.subject}</span> 
                            <p className="font-12">{ trip.note}</p>
                             <span>Posted by {trip.user_id}</span>
                             </td>
                             <td align="right" width="10%"><span className="w-50 d-b font-12">{fomattedData}</span></td>

                          </tr>

                      </table> 
              
                   );
               })}
                 
                   
            
                 
                </div> 
           
</>

        )
    }


}

export default FPHNotes;