import React, { Component } from 'react'
import FlightTripDtls from './FlightTripDtls'
import HotelTripDtls from './HotelTripDtls'

class FPHTrip extends Component {

    constructor(props) {
        super(props)
        this.state = {
            content:""
        }
        
    }

    
    
    render() {
        
        return (
           
            <>
                
                { this.props.fltDtl.length!=0 ?<div><h4 className="in-tabTTl">Flight Itinerary</h4><FlightTripDtls flt={this.props.fltDtl}/></div>:""}
                
                
               { this.props.htlDtl.length!=0 ?<div><h4 className="in-tabTTl">Hotel Itinerary</h4><HotelTripDtls htl={this.props.htlDtl}/>
                
                <h4 className="in-tabTTl">Inclusion And Extra</h4>
                   <span>{this.props.htlDtl[1].inclusion}</span>
                </div>:""}
           
            </>

        )
    }


}

export default FPHTrip;