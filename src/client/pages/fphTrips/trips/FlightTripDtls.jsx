import React, { Component } from 'react'
import { Link } from 'react-router-dom'
export const TRIPS_ROLES = 'tripsRoles'

class FlightTripDtls extends Component {

    constructor(props) {
        super(props)
        this.state = {
            
            tripDtls :[]
          
        }
      
        var trips=localStorage.getItem(TRIPS_ROLES);
        let tripsObj=JSON.parse(trips);
        this.state.ftripsRoles=tripsObj.ftrips;
    }
    
    showNewPopUp(fltId,e){
        e.persist();
       // console.log(e.target.id +"==="+ fltId )
        
        let url = "https://qa2.cleartrip.com/hq/trips/flight/"+fltId+"/fare_rule"
       window.open(
        url,
            "Fare Rules",
            "resizable,scrollbars,status"
          );
    }
    
    render() {
       return(
           
       this.props.flt.map(
          ele=>{
            
              return(
             
                
                ele.type_detail ==="Flight"
              ?    
              <div>{this.props.flt.indexOf(ele)===0?
              <><h4 className="in-tabTTl-sub">Itinerary</h4>
              </>:null}
              {this.state.ftripsRoles.FARERULE_ROLE_ENABLE && 
              <div className="destination">{ele.departure_airport} to {ele.arrival_airport} 
              <Link id="fareRule" className="linkRight" onClick= {(e)=>this.showNewPopUp(ele.id,e)}>Fare Rule</Link>
              </div>
              }
              <table className="dataTable5">
            <thead>
             <th width="20%">Leave</th>
             <th width="20%">Arrive</th>
             <th width="15%">Airlines</th>
             <th width="10%">Duration</th>
             <th width="15%">Flight</th>
             <th width="10%">Stops</th>
             <th width="10%">Class</th>
             <th width="10%">Supplier</th>
             </thead>
             <tbody>
                 { 
                    ele.segments.map(seg=><tr key={seg.id}>
                     <td>{seg.departure_airport} {seg.departure_terminal}</td>
                     <td>{seg.arrival_airport} {seg.arrival_terminal}</td>
                     <td>{seg.operating_airline}</td>
                     <td></td>
                     <td>{seg.operating_airline} - {seg.flight_number}</td>
                     <td>{seg.stopover_count}</td>
                     <td></td>
                     <td>{seg.supplier}</td>
                    </tr>

                 )
                 }
                 
             </tbody>
                 
              </table> </div>:
        <React.Fragment>
        <h4 className="in-tabTTl-sub">Flight booking details</h4>          
        <div className="paxName">{ele.title} {ele.first_name}  {ele.last_name}</div> 
        <table className="dataTable5">
        <thead>
        
             <th>Sector</th>
                    <th>Status</th>
                    <th>Airline Pnr</th>
                    <th>Split Pnr</th>
                    <th>Gds Pnr</th>
                    <th>Fare Class</th>
                    <th>Fare basic Code</th>
                    <th>BF + Airline Penalty</th>
                    <th>Ticket</th>
                    <th>Booking Type</th>
                    <th>Cost Pricing</th>
             </thead>
             <tbody>
                 { 
                    ele.paxToFlightMap.map(info=><tr key={info.id}>
                     <td>{info.departure_airport} - {info.arrival_airport}</td>
                    <td>{info.booking_status}</td>
                    <td>{info.airline_pnr}</td>
                    <td></td>
                    <td>{info.gds_pnr}</td>
                    <td>{info.cabin_type}</td>
                    <td>{info.pricing_objects[0].fare_basis_code}</td>
                    <td>{info.pricing_objects[0].total_base_fare}</td>
                    <td>{info.ticket_number}</td>
                    <td></td>
                    <td>{info.pricing_objects[0].cost_pricing_object.total_fare}</td>
                    </tr>

                 )
                 }
                 
             </tbody>
                 
              </table>
              </React.Fragment> )
              
          }
       )

       )
       
         
}

}


export default FlightTripDtls;