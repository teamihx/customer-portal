import React, { Component } from 'react'
import Header from 'Pages/common/Header'
import Footer from 'Pages/common/Footer'
import FPHTrip from '../trips/FPHTrip'
import FPHPax from '../pax/FPHPax'
import FPHNotes from '../notes/FPHNotes'
import FPHPayment from '../payment/FPHPayment'
import FPHAuditTrail from '../audit/FPHAuditTrail'
import FPHUpdateTktNumber from '../updateTicket/FPHUpdateTktNumber'
import FPHUpdatePricing from '../updatePrice/FPHUpdatePricing'
import FPHRefunds from '../refund/FPHRefunds'
import FphService from '../../../services/fph/FphService.js';
import { Link } from 'react-router-dom'

let fphRS = require('Assets/json/data.json');
 

class FPHComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            tripid: this.props.match.params.tripId,
            content: '',
            isActive: false,
            fltDtls:[],
            tripDtl:fphRS,
            fltDtlList:[],
            htlDtlList:[],
            paymentDtls:[],
            responseData:{},
            tktDtlsList:[]
        }
        this.renderComponents = this.renderComponents.bind(this);
        console.log('param:'+this.state.tripid)
        //this.loadData(this.state.tripid);
        this.currentClick='tripDetails';
        this.tripData={}
        this.req={
            tripId:this.props.match.params.tripId
        }

    }

    // loading ticket deatils 
    loadTicketDetails=()=>{
        this.state.tktDtlsList =[];
        var tempTripDtls = this.state.responseData;
        console.log('json structure is----'+JSON.stringify(this.state.responseData));
        var bookingInfoList = tempTripDtls.air_bookings[0].air_booking_infos;
        var paxList = tempTripDtls.air_bookings[0].pax_infos;
        var flights = tempTripDtls.air_bookings[0].flights;
        if(paxList){
        for(let pax of paxList){
            var paxDtl = new Object(); 
            var segList =[];
            paxDtl.title =pax.title;
            paxDtl.pax_type_code =pax.pax_type_code;
            paxDtl.first_name =pax.first_name;
            paxDtl.last_name =pax.last_name;
            paxDtl.currency ="Rs";
            paxDtl.totalAmt =pax.total_fare;
            if(bookingInfoList){
              var paxList1 =  bookingInfoList.filter(pax1 => pax1.pax_info_id === pax.id);
            if(paxList1){
                for(let flt of paxList1){
                   var segment = new Object();
                   segment.agent_pcc = flt.agent_pcc;
                   segment.airline_pnr = flt.airline_pnr;
                   segment.ticket_number = flt.ticket_number;
                   segment.gds_pnr = flt.gds_pnr;
                   
                   if(flights){
                       for(let seg of flights){
                        var dat = seg.segments.filter(segmnt => segmnt.id === flt.segment_id);
                     if(dat != '' && dat != 'undefined'){
                        segment.sector= dat[0].departure_airport+"-"+dat[0].arrival_airport;
                      }
                    }
                   }
                   segList.push(segment);
                }
                paxDtl.segList = segList;
                this.state.tktDtlsList.push(paxDtl);  
            }
            }

            
           
            
        }
    }
        console.log("ticketDetails1111"+this.state.tktDtlsList);
    }

    loadDataPaymentData=()=>{
        let paymentDtl = this.tripData.payments;
        let pmntData=[];
      for(let paymentDtl of paymentDtl){
           let pmntObj = new Object();
           pmntObj.txn_type=paymentDtl.txn_type;
           pmntObj.currency=paymentDtl.currency;
           pmntObj.pay_mode=paymentDtl.payment_type;
           pmntObj.description =paymentDtl.description;
           pmntObj.status =paymentDtl.status;
           pmntObj.checkout = paymentDtl.express_checkout;
           pmntObj.txn_ref = paymentDtl.merchant_txn_ref;
           pmntObj.amount=paymentDtl.amount;
           pmntObj.txns=[];
       // let pmntCardDtl =paymentDtl.payment_card_details[0];
        
        for(let txn of paymentDtl.payment_cards_gateway_accesses){
            let pmntTxns= new Object();
            pmntTxns.id=txn.id;
            pmntTxns.time=txn.created_at;
            pmntTxns.status =txn.status;
            pmntTxns.credential = txn.credential_name;
            pmntTxns.txn_type=txn.tran_type;
            pmntTxns.txn_id =txn.gateway_response3;
            pmntTxns.response=txn.description;
            if(txn.card_number!=null){
                pmntObj.card_no =txn.card_number;
            }
            if(txn.name_on_card!=null){
                pmntObj.name_on_card =txn.name_on_card;
            }
            
            pmntObj.txns.push(pmntTxns);
        }
        let pmntCardDtl = paymentDtl.payment_card_details[0];
        pmntObj.bank_name =pmntCardDtl.bank_id;
        pmntObj.country=pmntCardDtl.country;
        console.log(this.state.paymentDtls);
        pmntData.push(pmntObj);
      }
      this.state.paymentDtls=[...pmntData]
       console.log("Payment---"+this.state.paymentDtls) 
    }
     isEmpty=(obj)=> {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
    
        return true;
    }
   loadTripDetails=()=>{
   if(this.state.tripid==='16021668228'){
    this.tripData=this.state.tripDtl;
   }
   //console.log("Trip details==="+this.state.responseData.msg)
   if(!this.isEmpty(this.state.responseData) || !this.isEmpty(this.tripData)){
   if(!this.isEmpty(this.state.responseData)){
    if(this.state.responseData.msg!='TripNotFound'){
        this.tripData=this.state.responseData;
    }
   }
     
  if(this.tripData.air_bookings.length!=0){
    this.state.fltDtlList=[];
    this.state.htlDtlList=[];
    
    let airBkng= this.tripData.air_bookings;
    let fltBookDtl = airBkng[0].flights;
    var segArray=[];
    var segArrayReturn=[];

    let fltObj;
    
    
    
for(var fltDtl of fltBookDtl){
    let segments =[];
    fltObj=new Object();
    fltObj.id=fltDtl.id;
    fltObj.type_detail = "Flight";
    fltObj.departure_airport=fltDtl.departure_airport;
    fltObj.arrival_airport=fltDtl.arrival_airport;
    fltObj.segments=[];
    for(var seg of fltDtl.segments){
        let segObject =new Object();
        segObject.arrival_airport=seg.arrival_airport;
        segObject.departure_airport=seg.departure_airport;
        segObject.arrival_date_time=seg.arrival_date_time;
        segObject.departure_date_time=seg.departure_date_time;
        segObject.departure_terminal=seg.departure_terminal;
        segObject.arrival_terminal=seg.arrival_terminal;
        segObject.duration =seg.duration;
        segObject.flight_number=seg.flight_number;
        segObject.operating_airline=seg.operating_airline;
        segObject.marketing_airline=seg.marketing_airline;
        segObject.supplier=seg.supplier;
        fltObj.segments.push(segObject)
    }
        
    
    
    this.state.fltDtlList.push(fltObj);
}
var seg;
for(seg of fltBookDtl[0].segments){
    let segObject = new Object();
    
segArray.push(seg);
}
if(fltBookDtl.length>1){
for(seg of fltBookDtl[1].segments){
  segArrayReturn.push(seg);
}
for(seg of segArrayReturn){
segArray.push(seg);
}
}    
let airBkngInfo = airBkng[0].air_booking_infos;
var bkng;
for(bkng of airBkngInfo){
for(seg of segArray){
 if(bkng.segment_id===seg.id){
   bkng.departure_airport=seg.departure_airport;
   bkng.arrival_airport=seg.arrival_airport;
   break;
 }
}
}
for(bkng of airBkngInfo){
bkng.pricing_objects=[];
for(var priceObj of airBkng[0].pricing_objects){
   if(bkng.pricing_object_id=== priceObj.id){
    bkng.pricing_objects.push(priceObj);
   }
}
}

let paxToFlightMap = airBkng[0].pax_infos;

for(var pax of paxToFlightMap){
pax.type_detail = "Pax";
pax.paxToFlightMap=[];
for(var bkng  of airBkngInfo){
 if(pax.id===bkng.pax_info_id){
  pax.paxToFlightMap.push(bkng)
 }
}
}
for(var paxInfo of paxToFlightMap){
this.state.fltDtlList.push(paxInfo);
}
  }

if(this.tripData.hotel_bookings.length!=0){
let htlBookObj = new Object();
let htlBkngDtl = this.tripData.hotel_bookings[0];
htlBookObj.check_in_date = htlBkngDtl.check_in_date;
htlBookObj.check_out_date = htlBkngDtl.check_out_date;
htlBookObj.city_name = htlBkngDtl.hotel_detail.full_city_name;
htlBookObj.hotel_name =htlBkngDtl.hotel_detail.name;
htlBookObj.room_type=htlBkngDtl.room_types[0].name;
htlBookObj.room_count=htlBkngDtl.room_count;
htlBookObj.type="hotel_booking";
this.state.htlDtlList.push(htlBookObj);


for(let type of htlBkngDtl.room_types){
let roomDtlObj = new Object();
roomDtlObj.room_info =[];
roomDtlObj.type="room details";
roomDtlObj.room_count =htlBkngDtl.room_count;
roomDtlObj.no_of_adult=htlBkngDtl.hotel_booking_infos.length;
roomDtlObj.inclusion=type.inclusions;
for(let room of htlBkngDtl.rooms){
    if(type.id===room.room_type_id){
     let roomObj = new Object();
     
    for(let info of htlBkngDtl.hotel_booking_infos){
        if(info.room_id===room.id){
           roomObj.room_name=type.name;
           roomObj.supplier_name=type.supplier_id;
           roomObj.voucher_number =info.voucher_number;
           roomObj.status=info.booking_status;
           roomDtlObj.room_info.push(roomObj);
           break;
        }
    }
   }
}
this.state.htlDtlList.push(roomDtlObj);
}
}
   }
}
    renderComponents(e) {
        this.currentClick = e.target.id;
       
        if (this.currentClick === 'pax') {
            this.state.content =<FPHPax paxData={this.state.responseData}/>;
            
        } else if (this.currentClick === 'notes') {
            this.state.content =<FPHNotes {...this.state}/>;
        }else if (this.currentClick === 'paymentDetails') {
           {this.loadDataPaymentData()};
           this.state.content =<FPHPayment pmntDtl={this.state.paymentDtls}/>;
        }else if (this.currentClick === 'updateTktNum') {
            {this.loadTicketDetails()}
            this.state.content =<FPHUpdateTktNumber tktDtls={this.state.tktDtlsList}/>;
         }else if (this.currentClick === 'audittrail') {
           this.state.content =<FPHAuditTrail/>;
        }else if (this.currentClick === 'updateTktNum') {
            
           this.state.content =<FPHUpdateTktNumber />;
        }else if (this.currentClick === 'updatePricing') {
          this.state.content =<FPHUpdatePricing/>;
        }else if (this.currentClick === 'refunds') {
           this.state.content =<FPHRefunds/>;
          }
        

    }
    getTripDetail =(req) => {
        this.state.message = "";
        console.log("adsads")
        FphService.getTripDetail(req).then(response => {
          const tripRes = JSON.parse(response.data);
          this.setState({ responseData: tripRes });
    
        });
      }
    
      componentDidMount(){
       {this.getTripDetail(this.req)};
    }

    render() {
        
        if(this.currentClick==='tripDetails' ){
            {this.loadTripDetails()}  
        }
        
        
        return (
           
            <>
                <Header />
                <div className="main-container cont-Box fphPage">
                    <div className="row">
                        <div className="col-9">
                            <div className="tripInfo">
                                <h5>{this.state.responseData.trip_name} (Trip ID: {this.state.responseData.trip_ref})</h5>
                                <span className="booking-date">Booked by <a href="#">{this.state.responseData.booked_user_id}</a> {this.state.tripDtl.air_bookings[0].created_at}</span>
                                <span className="userType">Overall : <span className="user-type">L1</span>  <span className="user-type"> New User</span> Product (Package) : <span className="user-type">New User</span> </span>
                                </div>
                            <ul className="trip-link">
                                <li>
                                    <Link id="tripDetails" onClick={this.renderComponents} className={this.currentClick==="tripDetails"? "active":""}>Trip Details</Link>
                                </li>
                                <li>
                                    <Link id="pax" onClick={this.renderComponents} className={this.currentClick==="pax"? "active":""}>Pax Details</Link>
                                </li>
                                <li>
                                    <Link id="notes" onClick={this.renderComponents} className={this.currentClick==="notes"? "active":""}>Notes</Link>
                                </li>
                                <li>
                                    <Link id="paymentDetails" onClick={this.renderComponents} className={this.currentClick==="paymentDetails"? "active":""}>Payment Details</Link>
                                </li>
                                <li>
                                    <Link id="audittrail" onClick={this.renderComponents} className={this.currentClick==="audittrail"? "active":""}>Audi Trail</Link>
                                </li>
                                <li>
                                    <Link id="updateTktNum" onClick={this.renderComponents} className={this.currentClick==="updateTktNum"? "active":""}>Update Ticket Number</Link>
                                </li>
                                <li>
                                    <Link id="updatePricing" onClick={this.renderComponents} className={this.currentClick==="updatePricing"? "active":""}>Update Pricing</Link>
                                </li>
                                <li>
                                    <Link id="refunds" onClick={this.renderComponents} className={this.currentClick==="refunds"? "active":""}>Refunds</Link>
                                </li>
                            </ul>
                            
                            <div className="tripDetails">
                            {this.currentClick==='tripDetails' && !this.isEmpty(this.state.responseData)?<FPHTrip fltDtl={this.state.fltDtlList} htlDtl={this.state.htlDtlList}/>:<>
                                {this.state.content}
                            </>}</div>
                        </div>
                        <div className="col-3 right-bar">
                        <div className="trip-summary">
                        <h4 className="in-tabTTl">Tips, tools & extras</h4>
                        <Link>Email Trip Detail</Link>
                        <Link>SMS Trip Detail</Link>
                        <Link>Air Cancel</Link>
                        <Link>Hotel Cancel</Link>
                        </div>
                        <div className="trip-summary">
                        <h3 >Pricing Detail</h3> 
                           <ul>
                               <li>Adult</li>
                               <li>Transaction Fee</li>
                               <li>Other Charges</li>
                               <li>GST</li>
                               <li>Convenience Fee</li>
                               <li>Total</li>
                           </ul>
                        </div>
                        </div>
                        
                    </div>
                </div>
                <Footer />

            </>

        )
    }
}
export default FPHComponent;