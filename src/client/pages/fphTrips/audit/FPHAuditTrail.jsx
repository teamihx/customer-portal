import React, { Component } from 'react'


class FPHAuditTrail extends Component {

    constructor(props) {
        super(props)
        var data = require('../../../assets/json/data.json');
        this.state = {
            txns :Object.values(data.txns),
           email:''
        }
        
    }
    componentDidMount(){
        this.setState({
            txns :this.state.txns.sort((a,b)=>new Date(b.created_at).getTime()-new Date(a.created_at).getTime())
        })
        // this.state.tripNotes.sort((a,b)=>{
        //     return new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
        // }).reverse()
    }

    
    
    render() {
        
        return (
            <>
            <div>
            <h3>Audit trail</h3>
            <table>
                <tr>
                <th>
                    Txn Type
                </th>
                <th>Details</th>
                <th>Emails</th>
                </tr>
               {this.state.txns.map((txn)=>{
                   return(
                       <React.Fragment key={txn.id}>
                    <tr className="toptr">
                    <td>Type  {txn.txn_type}</td>
                    <td>When? {txn.created_at}</td>
                    <td rowSpan='3'>{this.state.email}</td>
                </tr>
                <tr>
                    <td>Txn iD  {txn.id}</td>
                    <td>Who? {txn.user_id}</td>
                </tr>
                <tr>
                    <td>IP  {txn.source_id}</td>
                    <td>What? {txn.source_type}</td>
                </tr>
                </React.Fragment>

                   );
               })}
                
            </table>


            </div>
           
</>

        )
    }


}

export default FPHAuditTrail;