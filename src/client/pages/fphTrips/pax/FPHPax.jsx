import React, { Component} from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Icon from 'Components/Icon';
//import 'react-day-picker/lib/style.css';
import MasterService from 'Services/master/MasterService'
import DateUtils from 'Services/commonUtils/DateUtils.js'
import FphService from 'Services/fph/FphService.js'
const fltRS = require('Assets/json/data.json');
import { connect } from 'react-redux';
//const pax = fltRS.air_bookings[0].pax_infos;

class FPHPax extends Component {  

  constructor(props) {
    super(props)
    let paxInfos = [];
    this.state = {
      //fphRS: this.props.paxData,
      paxInfos: this.props.tripDetail.air_bookings[0].pax_infos,
      showEditPax: false,
      selectedDay: undefined,
      mealMap: [
        { code: "AVML", name: "Asian Veg. Meal" },
        { code: "BBML", name: "Inf/Baby Food" },
        { code: "BLML", name: "Bland Meal" },
        { code: "CHML", name: "Child Meal" },
        { code: "DBML", name: "Diabetic Meal" }
      ],
      airlinesMaster: this.props.airLineMaster,
      startDate: new Date()
    }
    this.handleEditPaxDetail = this.handleEditPaxDetail.bind(this);
    this.savePaxDetails = this.savePaxDetails.bind(this);
    this.converDateFormate = this.converDateFormate(this);

  }

  converDateFormate() {
    this.state.paxInfos.map((paxInfo, index) => {
     /*  if (!paxInfo.date_of_birth.includes('/') && !paxInfo.passport_detail.date_of_expiry.includes('/')) {
        paxInfo.date_of_birth = DateUtils.ConvertIsoToSimpleDateFormat(paxInfo.date_of_birth);
        paxInfo.passport_detail.date_of_expiry = DateUtils.ConvertIsoToSimpleDateFormat(paxInfo.passport_detail.date_of_expiry);
      } */
    });
  }

  handleEditPaxDetail(e) {
    this.setState(state => ({
      showEditPax: !state.showEditPax
    }));
    if(this.state.paxInfos.passport_detail===undefined){
      this.state.paxInfos.passport_detail = {
      "passport_detail":{
      date_of_expiry:'',
      issuing_country:'',
      nationality:'',
      passport_number:''
      }
    };
    //this.state.paxInfos.passport_detail=myObj;
    console.log('Passport Obj Creating-------: '+JSON.stringify(this.state.paxInfos.passport_detail));
    //this.setState({ passport: myObj });
    
  }
  
  if(this.state.paxInfos.frequent_flier_numbers===undefined){

    const myObj = {
      "frequent_flier_numbers":{
        airline:'',
        freq_flier_number:'',
        applicable_airline:''
      }
    };
    this.state.paxInfos.frequent_flier_numbers=myObj;
    console.log('FRQUENT-------: '+JSON.stringify(this.state.paxInfos.frequent_flier_numbers));
    
  }
  }

  componentDidMount() {
    console.log('PAX DATA-------: '+JSON.stringify(this.state.paxInfos));
  }

  savePaxDetails(e) {
    FphService.updatePaxDetails(this.state.paxInfos)
      .then(response => {
        const updatedRS = response.data;
        this.setState({ updatedRS });
        if (updatedRS !== undefined && updatedRS.updateStatus) {
          let air_bkng = this.state.fphRS.air_bookings;
          for (let airBkng of air_bkng) {
            airBkng.pax_infos = this.state.paxInfos;
          }
          // console.log(JSON.stringify(this.state.paxInfos))
        }
      })
      .catch(this.setState({ updatedRS: '' }))
  }


  /*  handleChange = (e) => {
     this.setState(prevState => ({
       paxInfos: {
             ...prevState.paxInfos,
             [prevState.paxInfos[0].passport_detail.name]: e.target.value,
         },
     }));
 };
  */

 handlePassportChange = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, passport_detail: {
      ...paxInfo.passport_detail,passport_number: evt.target.value
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo }, () => console.log("Handle  :  " + JSON.stringify(this.state.paxInfos)));

}
handleChangeCountry = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, passport_detail: {
      ...paxInfo.passport_detail,issuing_country: evt.target.value
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleChangeNation = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, pax_nationality: evt.target.value
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleChangeMealcode = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, meal_request_code: evt.target.value
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleChangeVisaType = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, poi_detail: {
      ...paxInfo.poi_detail,visa_type: evt.target.value
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleChangeairline = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo,frequent_flier_numbers: [airline, evt.target.value]  
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleChangeffp = idx => evt => {
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, frequent_flier_numbers: {
      ...paxInfo.frequent_flier_numbers,freq_flier_number: evt.target.value
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}


handleDayChange = idx => startDate => {
  this.setState({ startDate });
  var dateString=this.formatDate(this.state.startDate);
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, date_of_birth: dateString
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}

handleDayChangeExpiry = idx => startDate => {
  this.setState({ startDate });
  var dateString=this.formatDate(this.state.startDate);
  let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
    if (idx !== sidx) return paxInfo;
    return {
      ...paxInfo, passport_detail: {
        ...paxInfo.passport_detail,date_of_expiry: dateString
      }
    };
  });
  this.setState({ paxInfos: newPaxinfo });
}
 formatDate(string){
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  return string.toLocaleDateString([],options);
}

 
  setStartDate = (startDate)=> {
    this.setState({
      startDate
    })
  }
  
  render() {
    
     //this.state.pax_infos.passport_detail=this.state.passport;
     console.log('Passport---: '+JSON.stringify(this.state.paxInfos.passport_detail));
    /* let value = this.state.selectedDay
       ? this.state.selectedDay.format("DD/MM/YYYY")
       : ""; */
    let meals = this.state.mealMap;
    let mealsMenu = meals.map((meal) =>
      <option value={meal.code}>{meal.name}</option>
    );
    let airMaster = this.state.airlinesMaster;
    let airlines = airMaster.map((airline) =>
      <option value={airline.airCode}>{airline.airName} [{airline.airCode}]</option>
    );
    // const [startDate, setStartDate] = useState(new Date());
    const { startDate } = this.state
    return (
      <div>
      <div>
     
        {this.state.paxInfos.map((paxInfo, index) => (
          <div key={index}><h3>{paxInfo.title} {paxInfo.first_name}  {paxInfo.last_name} <small>{paxInfo.pax_type_code}</small></h3>
            <table className="dataTbl pax-tbl dataTable5">
              <thead><tr>
                <th width="15%">Date Of Birth</th>
                <th width="10%">Passport#</th>
                <th width="10%">Issuing country</th>
                <th width="10%">Nationality</th>
                <th width="15%">Expires on</th>
                <th width="15%">Meal code</th>
                <th width="15%">Visa type</th>
                {this.state.showEditPax === false && (
                  <th>Baggage code</th>
                )}
              </tr></thead>
              <tbody>
                <tr>
                  
                  <td>
                  {this.state.showEditPax === true && (
                     <DatePicker
                      value={paxInfo.date_of_birth}
                      onChange={this.handleDayChange(index)}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                    />
                  )}
                  {this.state.showEditPax === false && (
                      <span>{paxInfo.date_of_birth}</span>
                    )}
                  </td>

                  <td>
                  {this.state.showEditPax === true && (
                      <input type="text" id="passport_number" name="passport_number" value={paxInfo.passport_detail.passport_number} onChange={this.handlePassportChange(index)} />
                    )}
                    {this.state.showEditPax === false && paxInfo.passport_detail!==null && paxInfo.passport_detail.passport_number!==null && paxInfo.passport_detail.passport_number!=='' && (
                      <span>{paxInfo.passport_detail.passport_number} </span>
                    )}
                  </td>
                  <td>
                  {this.state.showEditPax === true  && (
                      <input type="text" id="issuing_country" name="issuing_country" value={paxInfo.passport_detail.issuing_country} onChange={this.handleChangeCountry(index)} />
                    )}
                     {this.state.showEditPax === false && paxInfo.passport_detail!==null && paxInfo.passport_detail.issuing_country!==null && paxInfo.passport_detail.issuing_country!=='' && (
                      <span>{paxInfo.passport_detail.issuing_country}</span>
                    )}
                  </td>
                  <td>
                  {this.state.showEditPax === true && (
                      <input type="text" name="pax_nationality" value={paxInfo.pax_nationality} onChange={this.handleChangeNation(index)} />
                    )}
                    {this.state.showEditPax === false && paxInfo.pax_nationality!==null && paxInfo.pax_nationality!=='' && (
                      <span>{paxInfo.pax_nationality} </span>
                    )}
                  </td>
                 <td>
                  {this.state.showEditPax === true && (
                     <DatePicker
                      value={paxInfo.passport_detail.date_of_expiry}
                      onChange={this.handleDayChangeExpiry(index)}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                    />
                  )}
                  {this.state.showEditPax === false && paxInfo.passport_detail!==null && paxInfo.passport_detail.date_of_expiry!==null && paxInfo.passport_detail.date_of_expiry!=='' && (
                      <span>{paxInfo.passport_detail.date_of_expiry}</span>
                  )}
                  </td> 
                  <td> 
                  {this.state.showEditPax === true && (<div className="custom-select-v3"><Icon className="select-me" color="#CAD6E3" size={20} icon="down-arrow" /><select name="meal_request_code" value={paxInfo.meal_request_code} onChange={this.handleChangeMealcode}>
                    {mealsMenu}
                  </select></div>)}
                  {this.state.showEditPax === false && paxInfo.meal_request_code!==null && paxInfo.meal_request_code!=='' && (
                      <span>{paxInfo.meal_request_code}</span>
                    )}
                  </td>

                  <td>
                    
                     {this.state.showEditPax === true &&  paxInfo.poi_detail!=null && (
                      <input type="text" name="visaType" value={paxInfo.poi_detail.visa_type} onChange={this.handleChangeVisaType(index)} />
                    )}
                    {this.state.showEditPax === false && paxInfo.poi_detail!=null && (
                      <span>{paxInfo.poi_detail.visa_type}  </span>
                    )}
                  </td>
                  {this.state.showEditPax === false && (
                    <td>
                      {/*   {this.state.showEditPax === true && (
                      <input type="text" name="baggegeCode" onChange={this.handleChange} />
                    )} */}

                      <span> - </span>

                    </td>
                  )}
                </tr>
              </tbody>
            </table>
            
            <table className="dataTbl pax-tbl dataTable5">
              <thead><tr>
                <th width="33.3%">Airline </th>
                <th width="33.3%">Frequent flier number </th>
                <th width="33.3%">Applicable Airline </th>
              </tr>
                <tr>
                  <td>
                    <span>
                      {this.state.showEditPax === true && paxInfo.frequent_flier_numbers!=='' && paxInfo.frequent_flier_numbers[0]!=='' && paxInfo.frequent_flier_numbers[0]!==undefined && paxInfo.frequent_flier_numbers[0].airline!==undefined && (<div className="custom-select-v3"><Icon className="select-me" color="#CAD6E3" size={20} icon="down-arrow" />
                      <select name="airline" id="airline" value={paxInfo.frequent_flier_numbers[0].airline} onChange={this.handleChangeairline(index)}>
                      {airlines}
                      </select></div>)}
                      {this.state.showEditPax === false && paxInfo.frequent_flier_numbers!=='' && paxInfo.frequent_flier_numbers[0]!=='' && paxInfo.frequent_flier_numbers[0]!==undefined && paxInfo.frequent_flier_numbers[0].airline!==undefined &&  (
                        <span>{paxInfo.frequent_flier_numbers[0].airline}</span>
                      )}</span>
                  </td>
                  <td>
                    <span>{this.state.showEditPax === true && paxInfo.frequent_flier_numbers!=='' && paxInfo.frequent_flier_numbers[0]!=='' && paxInfo.frequent_flier_numbers[0]!==undefined && paxInfo.frequent_flier_numbers[0].freq_flier_number!==undefined &&  (
                      <input type="text" id="ffn" name="ffn" value={paxInfo.frequent_flier_numbers[0].freq_flier_number} onChange={this.handleChangeffp(index)} />
                    )}
                     {this.state.showEditPax === false && paxInfo.frequent_flier_numbers!=='' && paxInfo.frequent_flier_numbers[0]!=='' && paxInfo.frequent_flier_numbers[0]!==undefined && paxInfo.frequent_flier_numbers[0].freq_flier_number!==undefined &&  (
                        <span>{paxInfo.frequent_flier_numbers[0].freq_flier_number}</span>
                      )}</span>
                  </td>
                  <td>
                  {paxInfo.frequent_flier_numbers!=='' && paxInfo.frequent_flier_numbers[0]!=='' && paxInfo.frequent_flier_numbers[0]!==undefined && paxInfo.frequent_flier_numbers[0].applicable_airline!==undefined &&  (
                        <span>{paxInfo.frequent_flier_numbers[0].applicable_airline}</span>
                  )}
                  
                  </td>
                </tr>
              </thead>
            </table>
          </div>
        ))}
      </div>
        <div className="btnSec">
        <button type="button" className="btn btn-xs btn-primary" id="editPax" hidden={this.state.showEditPax} onClick={this.handleEditPaxDetail} ><b>Edit Pax Details</b></button>
        <button type="button" className="btn btn-xs btn-default" id="cancel" hidden={!this.state.showEditPax} onClick={this.handleEditPaxDetail} ><b>Cancel</b></button>     
        <button type="button" className="btn btn-xs btn-primary" id="savePax" hidden={!this.state.showEditPax} onClick={this.savePaxDetails} ><b>Save Pax Details</b></button>
      </div></div>
    )
  }

}
const mapStateToProps = state => {
  return {
      tripDetail: state.trpReducer.tripDetail,
      airLineMaster: state.trpReducer.airLineMaster
  };
}
export default connect(mapStateToProps, null)(FPHPax);


