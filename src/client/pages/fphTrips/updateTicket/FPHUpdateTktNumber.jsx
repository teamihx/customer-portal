import React, { Component } from 'react'


class FPHUpdateTktNumber extends Component {

    constructor(props) {
        super(props)
        this.state = {
           
          
        }
        
    }

    
    
    render() {
        
        return (
            <>
            <div>
            FPHUpdateTktNumber
            
            {this.props.tktDtls.map((pax, index) => (
            <div key={index}><h3>{pax.title} {pax.first_name}  {pax.last_name} <small>{pax.currency} {pax.totalAmt}</small></h3>
              <table className="dataTbl pax-tbl dataTable5">
                <thead><tr>
                  <th width="15%">Sector</th>
                  <th width="10%">GDS PNR</th>
                  <th width="10%">Airline PNR</th>
                  <th width="10%">Ticket #</th>
                  <th width="15%"> Pcc</th>
                 
                </tr></thead>
                <tbody>
                {pax.segList.map((tkt, index) => (
                  <tr>
                   <td>
                    {tkt.sector}
                   </td>
                    <td>
                     <input type="text" id="gds_pnr" name="gds_pnr" value={tkt.gds_pnr}/>
                    </td>
                    <td>
                    <input type="text" name="ariline_pnr" value={tkt.airline_pnr}/>
                    </td>
                    <td>
                     <input type="text" id="ticket" name="ticket" value={tkt.ticket_number}/>
                    </td>
                    <td>
                    <input type="text"  name="pcc" name="pcc" value={tkt.agent_pcc} />
                    </td>
                 
                  </tr>
                  ))}
                </tbody>
              </table>
              <div className="btnSec">
                <button type="button" className="btn btn-xs btn-primary" id="editPax" hidden={this.state.showEditPax} onClick={this.handleEditPaxDetail} ><b>Update Details</b></button>
              </div>
            </div>
          ))}



            </div>
           
</>

        )
    }


}

export default FPHUpdateTktNumber;