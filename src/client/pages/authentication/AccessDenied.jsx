import Icon from "Components/Icon";
import Footer from "Pages/common/Footer";
import Header from "Pages/common/Header";
import React, { Component } from "react";

class AccessDenied extends Component {
  render() {
    return (
      <>
        <Header />
        <section className="denied cont-Box">
          <div className="main-container text-center pt-50">
            <Icon color="#F4675F" size={40} icon="accessdenied" />
            <h3 className="secondary-cr">Access Denied..</h3>
            <div className="container">
              Please contact with Administrator....
            </div>
          </div>
        </section>
        <Footer />
      </>
    );
  }
}

export default AccessDenied;
