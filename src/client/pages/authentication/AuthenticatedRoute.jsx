import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import AuthenticationService from '../../services/authentication/AuthenticationService';
export const USER_AUTH_DATA = 'userAuthData'
export const USER_SIGNIN_DATA = 'userSignInData'
export const URL_COMPANY = 'company'
class AuthenticatedRoute extends Component {

    checkUsrAuthrization(){
        var url = window.location.href;
        var filename = url.substring(url.lastIndexOf('/') + 1);
        var urlString = "/" + filename;
        console.log('....Entered URL.....' + urlString);
        localStorage.setItem(URL_COMPANY, urlString);
        AuthenticationService.checkRegisterAutherization()
        .then(response => {
             var authResponse = response.data;
             var obj = JSON.parse(authResponse);
            console.log('Usr Authrization Status: ' + obj.data.valid)
            if (obj !== '' && obj.data.valid) {
                localStorage.setItem(USER_AUTH_DATA,obj);
                const comp = obj.data.rolesList.find(x => x === 5001000);
                const gst = obj.data.rolesList.find(y => y === 5001342);

                if (urlString === '/company'){
                    if(comp && gst){
                        this.props.history.push(`/hq/company`)
                    }else{
                        //Access denied
                        return <Redirect to="/hq/company" />
                    }
                    }else if (urlString == '/config') {
                    if(comp){
                        this.props.history.push(`/hq/company`)
                    }else{
                        //Access denied
                        return <Redirect to="/hq/company" />
                    }
                }else if (urlString === '/gst') {
                    if(gst){
                        console.log('gstttttttttttt................: '+gst);
                        this.props.history.push(`/hq/gst`)
                    }else{
                        //Access denied
                        return <Redirect to="/hq/company" />
                    }
                }else if (urlString === '/login'){
                    console.log('login1................: '+comp);
                    console.log('login2................: '+gst);
                    if(comp && gst){
                        console.log('xxxxxxxxxxxxxxxxxxxxxxxxxx................:');
                        this.props.history.push(`/hq/company`)
                    }else if(comp){
                        console.log('company...: ');
                        this.props.history.push(`/hq/config`)
                    }else if(gst){
                        console.log('gst......: ');
                        this.props.history.push(`/hq/gst`)
                    }else{
                        return <Redirect to="/hq/login" />
                    }
                }
            } else {
                var preUrl=localStorage.getItem(URL_COMPANY);
                console.log('To Login....'+preUrl)
            }
        }).catch(function (error) {
            console.log(error);
            var response = '';
            response = {
                status: '404',
                msg:'Resource Not Found'
            }
        });
    }

    render() {
        console.log('isUserLoggedIn.............')
        if (AuthenticationService.isUserLoggedIn()) {
            return <Route {...this.props} />
        } else {
            console.log('not LoggedIn')

            return <Redirect to="/hq/login" />
        }

    }
}

export default AuthenticatedRoute