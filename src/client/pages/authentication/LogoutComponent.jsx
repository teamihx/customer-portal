import React, { Component } from 'react'
import { Link } from 'react-router-dom'
export const contextPath=process.env.contextpath;
class LogoutComponent extends Component {
    render() {
        return (
            
            <div className="main-container text-center logoutpage">
              <div className="container pt-100">
                <h3 className="font-18">You are logged out</h3>              
                    <h5 className="t-color4 mb-10">Thank you for using our Application.</h5>
              {/*  <span className="t-color3"> Do you want to Sing In? <Link className="nav-link" to="/boportal/login">Sign In</Link></span> */}
              <span className="t-color3"> Do you want to Sing In? <Link className="nav-link" to={contextpath+"/login"}>Sign In</Link></span>
                </div>
            </div>
        )
    }
}

export default LogoutComponent