import React, { Component } from "react";
import AuthenticationService from "../../services/authentication/AuthenticationService";
import { Link, Redirect } from "react-router-dom";
import logo from "../../assets/images/logo.gif";
import Icon from "Components/Icon";
import Button from "Components/buttons/button.jsx";
import log from "loglevel";
export const URL_COMPANY = "company";
export const USER_AUTH_DATA = "userAuthData";
export const USER_NAME_SESSION_ATTRIBUTE_NAME = "authenticatedUser";
export const contextPath = process.env.contextpath;
export const TRIP_REF = "tripRef";
export const COMP_CONFIG_ROLES = 'compConfigRoles'

class LoginComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      hasLoginFailed: false,
      showSuccessMessage: false,
      showUserName: false,
      showPassword: false,
      resoureceNotFound: false,
      loading: false
    };

    console.log("environment is---" + process.env.environment);
    console.log("contextPath is---" + contextPath);
    this.handleChange = this.handleChange.bind(this);
    this.loginClicked = this.loginClicked.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  loginClicked(event) {
    try {
      event.preventDefault();
      this.setState({ loading: true });
      this.forceUpdate();
      if (this.state.username !== "" && this.state.password !== "") {
        this.setState({ showUserName: false });
        this.setState({ showPassword: false });
        AuthenticationService.registerSuccessfulLogin(
          this.state.username,
          this.state.password
        )
          .then(response => {
            var loginResp = response.data;
            var obj = JSON.parse(loginResp);
            if (obj.status === 200) {
              console.log(".......Login Successfully..........");
              localStorage.setItem(
                USER_NAME_SESSION_ATTRIBUTE_NAME,
                this.state.username
              );
              AuthenticationService.checkRegisterAutherization()
                .then(res => {
                  var authResponse = res.data;
                  var obj = JSON.parse(authResponse);
                  console.log("AUTHRIZE RESP........." + obj.status);
                  if (obj.status === 200) {
                    localStorage.setItem(USER_AUTH_DATA, authResponse);
                    var prevUrl = localStorage.getItem(URL_COMPANY);
                    AuthenticationService.createRolesAndPermissions("login");
                     //Getting ROLES DATA from local storage
                     let tripsObj=JSON.parse(localStorage.getItem(COMP_CONFIG_ROLES));
                     var comp = tripsObj.companyConfigRoles.COMPANY_ROLE_ENABLE;
                     var gst = tripsObj.companyConfigRoles.GST_ROLE_ENABLE;
                     if (prevUrl){
                      if (prevUrl === "/company") {
                        this.props.history.push(contextPath + '/company');
                      } else if (prevUrl === "/gst") {
                        window.location = contextPath + "/company/gst";
                      } else if (prevUrl === "/config") {
                        window.location = contextPath + "/company/config";
                      } else if (prevUrl === "trips") {
                        var triRef = localStorage.getItem(TRIP_REF);
                        localStorage.removeItem(TRIP_REF);
                        window.location = contextPath + "/trips/" + triRef;
                      } else {
                        window.location = contextPath + "/company";
                      }
                    }else{
                      if(comp && gst){
                        //window.location = contextPath + "/company";
                        this.props.history.push(contextPath + '/company')
                      }else if (comp) {
                        window.location = contextPath + "/company/config";
                      } else if (gst) {
                        window.location = contextPath + "/company/gst";
                      }else{
                        window.location = contextPath + "/accessdenied";
                      }
                    }
                  } else {
                    this.setState({ resoureceNotFound: true });
                    this.setState({ loading: false });

                    this.forceUpdate();
                  }
                })
                .catch(function(error) {
                  console.log(error);
                  this.setState({ hasLoginFailed: true });
                  this.setState({ loading: false });
                });
            } else {
              console.log("Login Failed.........." + response.data);
              this.setState({ hasLoginFailed: true });
              this.setState({ loading: false });

              this.forceUpdate();
            }
          })
          .catch(function(error) {
            this.setState({ resoureceNotFound: true });
            this.setState({ loading: false });
            if (error.response) {
              var resp = "";
              resp = {
                status: "404",
                msg: "Resource Not Found"
              };
              console.log("Login Failed.........." + resp);
            }
          });
      } else {
        if (this.state.username === "") {
          this.setState({ showUserName: true });
          this.setState({ resoureceNotFound: false });
          this.setState({ hasLoginFailed: false });
        } else {
          this.setState({ showUserName: false });
        }
        if (this.state.password === "") {
          this.setState({ showPassword: true });
          this.setState({ resoureceNotFound: false });
          this.setState({ hasLoginFailed: false });
        } else {
          this.setState({ showPassword: false });
        }
        this.setState({ loading: false });
      }
    } catch (err) {
      this.setState({ loading: false });
      log.error(
        "Exception occured in LoginComponent loginClicked function---" + err
      );
    }
  }
  render() {
    return (
      <section>
        <form onSubmit={e => this.loginClicked(e)}>
          <div className="container">
            <div className="loginForm form-group">
              <Link to="/">
                <img src={logo} className="App-logo" alt="logo" />
              </Link>
              <h4>User Login</h4>
              {this.state.resoureceNotFound && (
                <div className="alert alert-warning">
                  <Icon
                    className="noticicon"
                    color="#cda11e"
                    size={16}
                    icon="warning"
                  />
                  Resource not found
                </div>
              )}
              {this.state.showUserName && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Enter User name
                </div>
              )}
              {this.state.showPassword && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Enter Password
                </div>
              )}
              {this.state.hasLoginFailed && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Invalid Credentials
                </div>
              )}
              {this.state.showSuccessMessage && <div>Login Sucessful</div>}
              <div className="loginInput">
                <label>User Name </label>
                <input
                  type="text"
                  placeholder="Enter User Name"
                  name="username"
                  value={this.state.username}
                  onChange={this.handleChange}
                />
              </div>
              <div className="loginInput">
                <label>Password </label>
                <input
                  type="password"
                  placeholder="Enter Password"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </div>

              <Button
                size="sm"
                viewType="primary"
                className="fullWidth"
                onClickBtn={this.loginClicked}
                loading={this.state.loading}
                btnTitle="Login"
              ></Button>
            </div>
          </div>
        </form>
      </section>
    );
  }
}

export default LoginComponent;
