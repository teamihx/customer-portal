import React, { Component } from "react";
import AuthenticationService from "../../services/authentication/AuthenticationService";
import { Link, Redirect } from "react-router-dom";
import logo from "../../assets/images/logo.gif";
import Icon from "Components/Icon";
import Button from "Components/buttons/button.jsx";
import log from "loglevel";
export const contextPath = process.env.contextpath;

class LdapAuthentication extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      loading: false,
      hasLoginFailed: false,
      showSuccessMessage: false,
      showUserName: false,
      showPassword: false,
      sucessMesege:false
    };
    this.handleChange = this.handleChange.bind(this);
    this.checkLdapAuth=this.checkLdapAuth.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  checkLdapAuth(event) {
    try{
        event.preventDefault();
        this.setState({ loading: true });
        console.log('CheckLdap Authentication start...'+this.state.username);
        if(this.state.username!=="" && this.state.password!==""){
        AuthenticationService.checkLdapAuthentication(this.state.username,this.state.password).then(response => {
                var resp=JSON.parse(response.data);
                console.log('Ldap Authenticated Response....'+resp);
                if(resp!==null && resp.data!==undefined && resp.data.status!==undefined && resp.data.status==="SUCCESS"){
                  console.log('Ldap Authenticated Successfully....');
                  AuthenticationService.saveLdapToken(this.state.username,this.state.password).then(resp => {
                    if(resp.status===200){
                      console.log('Ldap Key Saved Successfully....');
                    }
                  }).catch(function(error) {
                    console.log(error);
                    console.log('Ldap Key Not Saved....');
                    this.setState({ loading: false });
                  });
                  this.setState({ loading: true });
                  window.location = contextPath + "/login";
                  //this.props.history.push(contextPath+'/login');
               }else{
                this.setState({ hasLoginFailed: true });
                this.setState({ loading: false });
               }
               //window.location = contextPath + "/login";
              }).catch(function(error) {
                console.log(error);
              });
            }else{
              if (this.state.username === "") {
                this.setState({ showUserName: true });
                this.setState({ hasLoginFailed: false });
              } else {
                this.setState({ showUserName: false });
              }
              if (this.state.password === "") {
                this.setState({ showPassword: true });
                this.setState({ hasLoginFailed: false });
              } else {
                this.setState({ showPassword: false });
              }
              this.setState({ loading: false });
            }
    }catch(err){
        log.error('Exception occured in removeTag function---'+err);
    }
}

  render() {
    return (
      <section>
        <form onSubmit={e => this.checkLdapAuth(e)}>
          <div className="container">
            <div className="loginForm form-group">
              <Link to="/">
                <img src={logo} className="App-logo" alt="logo" />
              </Link>
              <h4>LDAP Authentication</h4>
              {this.state.showUserName && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Enter User name
                </div>
              )}
              {this.state.showPassword && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Enter Password
                </div>
              )}
              {this.state.hasLoginFailed && (
                <div className="alert alert-error">
                  <Icon
                    className="noticicon"
                    color="#F4675F"
                    size={16}
                    icon="warning"
                  />
                  Invalid Credentials
                </div>
              )}
              {this.state.showSuccessMessage && <div>Login Sucessful</div>}
              <div className="loginInput">
                <label>User Name </label>
                <input
                  type="text"
                  placeholder="Enter User Name"
                  name="username"
                  value={this.state.username}
                  onChange={this.handleChange}
                />
              </div>
              <div className="loginInput">
                <label>Password </label>
                <input
                  type="password"
                  placeholder="Enter Password"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </div>

              <Button
                size="sm"
                viewType="primary"
                className="fullWidth"
                onClickBtn={this.checkLdapAuth}
                loading={this.state.loading}
                btnTitle="Login"
              ></Button>
            </div>
          </div>
        </form>
      </section>
    );
  }
}

  export default LdapAuthentication;