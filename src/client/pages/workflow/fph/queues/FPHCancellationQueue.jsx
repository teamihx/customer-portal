import React, { useEffect } from 'react';
import Header from 'Pages/common/Header';
import Footer from 'Pages/common/Footer';
import FPHRightSearchBar from '../generic/FPHRightSearchBar';
// import DynamicTable from '../../../../components/table/DynamicTable';

const FPHCancellationQueue = (props) => {
 // console.log(props);
  const TABLE_MAP = [
    { label: 'Trip ID', mappingKey: 'tripRef' },
    { label: 'Booking time' },
    { label: 'Travel time' },
    { label: 'Cancel time' },
    { label: 'Cancellation Details' },
    { label: 'Air supplier' },
    { label: 'Airline' },
    { label: 'Hotel supplier' },
    { label: 'Hotel name' },
    { label: 'Domain' },
    { label: 'Pax name' },
    { label: 'Action button' },
  ];

  useEffect(() => {}, []);
  return (
    <>
      <Header />
      <main className="main-container cont-Box fphPage bg-c-gr pt-0">
        <section className="row">
          <div className="col-9 tabPanel">
            {/* <DynamicTable /> */}
          </div>
          <aside className="col-3 right-bar">
            <FPHRightSearchBar />
          </aside>
        </section>
      </main>
      <Footer />
    </>
  );
};
export default FPHCancellationQueue;
