import React, { Component } from "react";
import logo from "Assets/images/logo.gif";
import Header from "Pages/common/Header";
import Icon from 'Components/Icon'; 
import FooterGeneric from "Pages/common/FooterGeneric";
import 'Assets/styles/_addon.scss';
const url=window.location.href
const value=url.split("/")[url.split("/").length-1];
const sample = props => (

  <>
   <Header data-path={value}/>
      <div className="main-container">
        
          <div className="title-page">
            <h2>{props.name}</h2>
          </div>

          <div className="print-me"><Icon color="#fff" size={20} icon="printer" /> {props.sideHeaderComponent}</div>
       
      </div>
    
    <div> {props.children}</div>
    <FooterGeneric />
  </>
);

export default sample;
