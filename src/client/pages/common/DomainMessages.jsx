import React, { Component } from "react";
import { connect } from "react-redux";
import log from "loglevel";

class DomainMessages extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: []
    };
  }

  checkingMessages(data, ctConfigData) {
    if (
      data !== null &&
      data !== undefined &&
      data !== "" &&
      ctConfigData !== null &&
      ctConfigData !== undefined &&
      ctConfigData !== ""
    ) {
      try {
        this.state.messages = [];
        const api_domain_data = JSON.parse(
          ctConfigData.data["ct.commonr3.user_id_based_api_domain"]
        );
        if (
          api_domain_data !== null &&
          api_domain_data !== undefined &&
          api_domain_data !== ""
        ) {
          let apiData = api_domain_data[data.user_id];
          if (apiData !== null && apiData !== undefined && apiData !== "") {
            if (apiData === "paytm.cleartrip.com") {
              this.state.messages.push("This is PAYTM booking");
            } else if (apiData === "ixigo.cleartrip.com") {
              this.state.messages.push("This is IXIGO booking");
            } else if (apiData === "flyin.cleartrip.com") {
              this.state.messages.push("This is FLYIN booking");
            } else if (apiData === "amazon.in") {
              this.state.messages.push("This is AMAZON booking");
            } else {
              this.state.messages = [];
            }
          }
        }
      } catch (err) {
        log.error(
          "Exception occured in DomainMessages Component checkingMessages function " +
            err
        );
      }
    }
  }

  render() {
    {
      this.checkingMessages(this.props.tripDetail, this.props.ctConfigData);
    }

    return (
      <>
        {this.state.messages !== null &&
          this.state.messages !== "" &&
          this.state.messages !== undefined &&
          this.state.messages.length > 0 && (
            <div className="alert alert-success pl-8 pb-8 pt-8 font-w-b">
              {this.state.messages.map((item, key) => (
                <span key={item}>{item}</span>
              ))}
            </div>
          )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripDetail: state.trpReducer.tripDetail,
    ctConfigData: state.trpReducer.ctConfigData
  };
};
export default connect(mapStateToProps, null)(DomainMessages);
