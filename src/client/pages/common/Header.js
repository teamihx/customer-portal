import React, { Component } from "react";
import MenuComponent from "../common/MenuComponent.jsx";
import logo from "Assets/images/logo.gif";
import { Link } from "react-router-dom";
import MobHeader from "Pages/common/MobHeader";
import { useMediaQuery } from "react-responsive";
const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 992 });
  return isDesktop ? children : null;
};
const Mobile = ({ children }) => {
  const isMobile = useMediaQuery({ maxWidth: 991 });
  return isMobile ? children : null;
};
const Default = ({ children }) => {
  const isNotMobile = useMediaQuery({ minWidth: 768 });
  return isNotMobile ? children : null;
};

const Header = props => {
  return (
    <header className="app-header">
      <Desktop>
        <div className="app-header__wrapper">
            <a className="app-header__logo-wrapper" href="/" onClick={e => e.preventDefault()}>
              <img
                src={logo}
                className="app-header__logo"
                title="Cleartrip CX-portal"
                alt="logo"
              />
            </a>
            <MenuComponent/>
        </div>
      </Desktop>
      <Mobile>
        <MobHeader />
      </Mobile>
    </header>
  );
};

export default Header;
