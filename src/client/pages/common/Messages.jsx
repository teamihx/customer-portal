import React, { Component } from 'react'
import { connect } from 'react-redux';
import log from 'loglevel';
import Icon from 'Components/Icon';
import DateUtils from '../../services/commonUtils/DateUtils';
import * as actions from '../../store/actions/index'

class Messages extends Component {

    constructor(props) {
        super(props)
       
        this.state = {

            responseData: '',            
            txns :'',
            paymentDetails:'',
            bkngStatus:'',
            product:'',
            messages:[],
            openTxnmsg:'',
            is_txn_open:false,
            is_payment_status_failed:false,
            is_booking_status_failed:false,
            is_txn_type_flt_convert:false,
            is_txn_type_hlt_convert:false,
            is_htl_booking_status_failed:false,
            is_train_booking_status_failed:false,

        }
        
    }


    checkingMessages(data){

      if(data!==null && data!==undefined && data!==''){
      this.state.messages=[];
      this.state.openTxnmsg='';
      this.state.is_txn_open=false;
      try{
      this.state.txns=data.txns;
      this.state.paymentDetails=data.payments_service_data;
      this.state.bkngStatus=data.booking_status;
      let created_at=data.created_at;
      let flt_dep_at='';
      let hlt_dep_at='';

      /* if(data.trip_type!==null && data.trip_type===1){
      flt_dep_at=data.air_bookings[0].flights[0].departure_date_time;
      }else if(data.trip_type!==null && data.trip_type===2){
        hlt_dep_at=data.air_bookings[0].flights[0].departure_date_time;
      }else if(data.trip_type!==null && data.trip_type===3){

      }
 */


      
      for(let txn in this.state.txns){
          let txnData=this.state.txns[txn];

          //checking is any txn is open or not
          if(txnData.status!==null && txnData.status!==undefined && txnData.status==='O'){            
            //this.setState({is_txn_open:true});
            this.state.is_txn_open=true;
            break;

          }

           //Txn type is 13-Flight Converted (offline) and Txn status Closed
          if(txnData.txn_type !==null && txnData.txn_type!==undefined && txnData.txn_type===13
              && txnData.status!==null && txnData.status!==undefined && txnData.status==='C' 
              && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1){
                this.state.is_txn_type_flt_convert=true;
              break;
          }

           //Txn type is 23-Hotel Converted (offline) data.air.trip_type
           if(txnData.txn_type !==null && txnData.txn_type!==undefined && txnData.txn_type===23
              && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===2){
              this.state.messages.push("This booking has been confirmed offline. Please contact customer support for cancellations. Your voucher will be emailed to you.");
              this.state.is_txn_type_hlt_convert=true;
              break;
          }

        
      }

        //checking is (SEATSELL FAILED)-F status is failed
      for(let pay1 in this.state.paymentDetails){          
          let pay=this.state.paymentDetails[pay1];
          if(pay.status!==null && pay.status!==undefined && pay.status!=='' && pay.status==='F'){
            this.state.is_payment_status_failed=true;
              break;
          }
        }

        //booking status is (H-FAILED and not UPCOMING-UPCOMING) for flight
        if(this.state.bkngStatus!==null && this.state.bkngStatus!==undefined && this.state.bkngStatus!==''){
            if(this.state.bkngStatus==='H' && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1 ){
              this.state.is_booking_status_failed=true;
            }

        }

         //booking status is H-FAILED for hotel
        if(this.state.bkngStatus!==null && this.state.bkngStatus!==undefined && this.state.bkngStatus!==''){
          if(this.state.bkngStatus==='H'&& data.trip_type!==null && data.trip_type!==undefined && data.trip_type===2){
            this.state.is_htl_booking_status_failed=true;  
          }

      }
      //booking status is H-FAILED for Train
      if(this.state.bkngStatus!==null && this.state.bkngStatus!==undefined && this.state.bkngStatus!==''){
        if(this.state.bkngStatus==='H'&& data.trip_type!==null && data.trip_type!==undefined && data.trip_type===4){
          this.state.is_train_booking_status_failed=true;  
        }

    }
      
       //booking status is R-BOOKING_STATUS_SEATS_RESERVED(Onhold)
       /* if(this.state.bkngStatus!==null && this.state.bkngStatus!==undefined && this.state.bkngStatus!==''){
          if(this.state.bkngStatus==='R' && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1){

            let diff='';
            let millisec=1.728e+8;//48 hours to millisec
            if(flt_dep_at!=='' && created_at!==''){
              diff=Math.abs(new Date(created_at)-new Date(flt_dep_at));            
               if(diff>=millisec){                
                let expireDate=new Date(new Date(created_at).getTime()+(1000 * 60 * 60 * 24 * 2));
                let currentDate=new Date();
                console.log('expireDate---'+expireDate);
                console.log('currentDate---'+currentDate);
                if(expireDate>=currentDate){
                let finalDate=DateUtils.prettyDate(expireDate,'ddd, MMM DD YYYY, hh:mm A');
                this.state.messages.push("This booking is on-hold and will expire on "+finalDate);
                }

              }
            }
          }

      } */
      
      try{
        //booking status is R-BOOKING_STATUS_SEATS_RESERVED(Onhold)
      if(this.state.bkngStatus!==null && this.state.bkngStatus!==undefined && this.state.bkngStatus!==''){

        if(this.state.bkngStatus==='R' && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1){

          //if flex pay booking flexpay date is showing
          if(data.air_bookings[0].flexi_pay_detail!==null && data.air_bookings[0].flexi_pay_detail!==undefined){
          
          }else if(data.air_bookings[0].pricing_objects.length>0){
            let pricing_details=data.air_bookings[0].pricing_objects;            
            for(let price in pricing_details){           
              
              let pric_Data=pricing_details[price];
              if(pric_Data.external_references!==null && pric_Data.external_references.length>0){
                
                let ext_details=pric_Data.external_references;
                for(let ext in ext_details){
                  let ext_data=ext_details[ext];

                  //if international booking LATEST_TKT_DATE date is showing
                if(data.bkg_type!==null && data.bkg_type!==undefined && data.bkg_type==='I'){
                if(ext_data.name!==null && ext_data.name!==undefined && ext_data.name!=='' && ext_data.name==='LATEST_TKT_DATE'){
                let date_value=ext_data.value;
                if(date_value!==null && date_value!==undefined){
                let finalDate=DateUtils.prettyDate(date_value,'ddd, MMM DD YYYY, hh:mm A');
                this.state.messages.push("This booking is on-hold and will expire on "+finalDate);
                 break;
                }
                }
              }
              //if domestic booking WE_PAY_DATE date is showing
              else if(data.bkg_type!==null && data.bkg_type!==undefined && data.bkg_type==='D'){
                if(ext_data.name!==null && ext_data.name!==undefined && ext_data.name!=='' && ext_data.name==='WE_PAY_DATE'){
                  let date_value1=ext_data.value;
                  if(date_value1!==null && date_value1!==undefined){
                  let finalDate1=DateUtils.prettyDate(date_value1,'ddd, MMM DD YYYY, hh:mm A');
                  this.state.messages.push("This booking is on-hold and will expire on "+finalDate1);
                   break;
                  }
                  }

              }
                }
              }
            }
          }         
        }
        //Hotel
        else if(data.trip_type!==null && data.trip_type!==undefined && data.trip_type===2){
            let ext_detail_data=data.hotel_bookings[0].external_references;
            for(let ext in ext_detail_data){
              let ext_dat=ext_detail_data[ext];
              if(ext_dat!==null && ext_dat!==undefined){
               
                //if booking status-Reserved status EXPIRATION-TIME showing
                if(this.state.bkngStatus==='R'){
                if(ext_dat.name!==null && ext_dat.name!==undefined && ext_dat.name==='EXPIRATION-TIME'){
                  let date_value=ext_dat.value;
                  if(date_value!==null && date_value!==undefined){
                  let finalDate=DateUtils.prettyDate(date_value,'ddd, MMM DD YYYY, hh:mm A');
                  this.state.messages.push("This booking is on-hold and will expire on "+finalDate);
                   break;
                  }
                }
              }
              //if booking is part_pay  PART_PAY_EXPIRATION showing
              else if(data.hotel_bookings[0].part_pay!==null && data.hotel_bookings[0].part_pay!==undefined
                && data.hotel_bookings[0].part_pay==='Y' && 
                ext_dat.name!==null && ext_dat.name!==undefined && ext_dat.name==='PART_PAY_EXPIRATION'){
                  let date_value1=ext_dat.value;
                  if(date_value1!==null && date_value1!==undefined){
                  let finalDate1=DateUtils.prettyDate(date_value1,'ddd, MMM DD YYYY, hh:mm A');
                  this.state.messages.push("This booking is on-hold and will expire on "+finalDate1);
                   break;
                  }

              }else if(ext_dat.name==='LATEST_TKT_DATE'){
                let date_value2=ext_dat.value;
                if(date_value2!==null && date_value2!==undefined){
                  let finalDate2=DateUtils.prettyDate(date_value2,'ddd, MMM DD YYYY, hh:mm A');
                  this.state.messages.push("This booking is on-hold and will expire on "+finalDate2);
                   break;
                }

              }
              }
            }

        }
      }

    }catch(error){
      log.error('Exception occured in Messages Component BOOKING_STATUS_SEATS_RESERVED scanario '+error) 
    }




      //Pay-at-Hotel booking
          if(data.trip_type!==null && data.trip_type!==undefined && data.trip_type===2
          && data.hotel_bookings[0]!==null && data.hotel_bookings[0].pay_at_hotel!==null && data.hotel_bookings[0].pay_at_hotel!==undefined  && data.hotel_bookings[0].pay_at_hotel==='Y'){
            this.state.messages.push("This is a Pay At Hotel Booking.");

          }

           //Imported PNR booking.
           if(data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1
              && data.air_bookings[0]!==null && data.air_bookings[0].air_booking_infos!==null && data.air_bookings[0].air_booking_infos[0].status_reason!==null 
              && data.air_bookings[0].air_booking_infos[0].status_reason!==undefined && data.air_bookings[0].air_booking_infos[0].status_reason==='I'  ){
                this.state.messages.push("This booking is Imported PNR booking.");
              }

              if(data.trip_type!==null && data.trip_type!==undefined && data.trip_type===4){
                //(t.hour >= 23 and t.min >= 30) or t.hour < 5 or (t.hour <= 5 and t.min == 0)
                let d=new Date();
                var h=DateUtils.convertHourMin(d.getHours(), 2);
                var m=DateUtils.convertHourMin(d.getMinutes(), 2);
               // console.log("IRCTC Hr and Min : "+h+"---"+m);
                if((h >= 23 && m >= 30) || h < 5 || (h <= 5 && m == 0)){
                  this.state.messages.push("Indian Railways ticketing and cancellation service remains closed from 2330 Hrs to 0500 Hrs daily. For emergency cancellations, please contact IRCTC directly at "+ <a href="mailto:etickets@irctc.co.in">etickets@irctc.co.in</a>);
                }
              }
              if(this.state.is_txn_open){
                this.state.openTxnmsg="Some transactions in this booking are open.";
                
              }else if(this.state.is_payment_status_failed){
                this.state.messages.push("This booking may have some failed payments");

              }else if(this.state.is_booking_status_failed){
                this.state.messages.push("This trip has some failed bookings. Don't panic. We will refund your money within 15 working days (though it usually doesn't take that long!)");

              }else if(this.state.is_txn_type_flt_convert){
                this.state.messages.push("This trip was confirmed offline. You should probably double-check the class / fare basis before processing any refund.");

              }else if(this.state.is_txn_type_hlt_convert){
                this.state.messages.push("This booking has been confirmed offline. Please contact customer support for cancellations. Your voucher will be emailed to you.");

              }else if(this.state.is_htl_booking_status_failed){
                this.state.messages.push("This trip has some failed bookings. Don't panic. We will refund your money within 15 working days (though it usually doesn't take that long!)");

              }else if(this.state.is_train_booking_status_failed){
                this.state.messages.push("This trip has some failed bookings. Don't panic. We will refund your money within 15 working days (though it usually doesn't take that long!)");

              }
              
           }catch(err){
          
            log.error('Exception occured in Messages Component checkmessages function '+err)    

          } 
      }

  }

    

    

    /* checkingMessages(data){

        if(data!==null && data!==undefined && data!==''){
        //console.log('trip data from messages is--'+JSON.stringify(data)); 
        this.state.messages=[];
        this.state.openTxnmsg='';
        try{
        this.state.txns=data.txns;
        this.state.paymentDetails=data.payments_service_data;
        this.state.bkngStatus=data.booking_status;

        
        //console.log('this.state.txns in checkingMessages '+JSON.stringify(this.state.txns));
        //console.log('this.state.paymentDetails in checkingMessages '+JSON.stringify(this.state.paymentDetails));

        
        for(let txn in this.state.txns){
            let txnData=this.state.txns[txn];
  
            //checking is any txn is open or not
            if(txnData.status!==null && txnData.status!==undefined && txnData.status==='O'){
              //this.state.messages.push("Some transactions in this booking are open.");
              this.state.openTxnmsg="Some transactions in this booking are open."
              break;
  
            }

             //Txn type is 13-Flight Converted (offline) and Txn status Closed
            if(txnData.txn_type !==null && txnData.txn_type!==undefined && txnData.txn_type===13
                && txnData.status!==null && txnData.status!==undefined && txnData.status==='C' 
                && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1){
                this.state.messages.push("This trip was confirmed offline. You should probably double-check the class / fare basis before processing any refund.");
                break;
            }

             //Txn type is 23-Hotel Converted (offline) data.air.trip_type
             if(txnData.txn_type !==null && txnData.txn_type!==undefined && txnData.txn_type===23
                && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===2){
                this.state.messages.push("This booking has been confirmed offline. Please contact customer support for cancellations. Your voucher will be emailed to you.");
                break;
            }

          
        }

          //checking is (SEATSELL FAILED)-F status is failed
        for(let pay1 in this.state.paymentDetails){
            console.log('payment details is---'+JSON.stringify(this.state.paymentDetails))
            
            let pay=this.state.paymentDetails[pay1];
            console.log('payment details loop is---'+JSON.stringify(pay))
  
            if(pay.status!==null && pay.status!==undefined && pay.status!=='' && pay.status==='F'){             
             
              if(this.state.messages.length<=0 && this.state.openTxnmsg==''){
                this.state.messages.push("This booking may have some failed payments");
                break;
              }
            }
          }

          //booking status is (H-FAILED and not UPCOMING-UPCOMING) for flight
          if(this.state.bkngStatus!==null && this.state.bkngStatus!==undefined && this.state.bkngStatus!==''){
              if(this.state.bkngStatus==='H' && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1 ){
                if(this.state.messages.length<=0 && this.state.openTxnmsg==''){
                this.state.messages.push("This trip has some failed bookings. Don't panic. We will refund your money within 15 working days (though it usually doesn't take that long!)");
                }
              }

          }

           //booking status is H-FAILED for hotel
          if(this.state.bkngStatus!==null && this.state.bkngStatus!==undefined && this.state.bkngStatus!==''){
            if(this.state.bkngStatus==='H'&& data.trip_type!==null && data.trip_type!==undefined && data.trip_type===2){
              if(this.state.messages.length<=0 && this.state.openTxnmsg==''){
              this.state.messages.push("This trip has some failed bookings. Don't panic. We will refund your money within 15 working days (though it usually doesn't take that long!)");
              }
            }

        }

         //booking status is R-BOOKING_STATUS_SEATS_RESERVED(Onhold)
         if(this.state.bkngStatus!==null && this.state.bkngStatus!==undefined && this.state.bkngStatus!==''){
            if(this.state.bkngStatus==='R' && data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1
            && data.air_bookings[0].flexi_pay_detail!==null && data.air_bookings[0].flexi_pay_detail!==undefined){
              if(this.state.messages.length<=0 && this.state.openTxnmsg==''){
              this.state.messages.push("This booking is on-hold and will expire on"+data.air_bookings[0].flexi_pay_detail[0].expiry_date);
              }
            }

        }

        //Pay-at-Hotel booking
            if(data.trip_type!==null && data.trip_type!==undefined && data.trip_type===2
            && data.hotel_bookings[0]!==null && data.hotel_bookings[0].pay_at_hotel!==null && data.hotel_bookings[0].pay_at_hotel!==undefined  && data.hotel_bookings[0].pay_at_hotel==='Y'){
              if(this.state.messages.length<=0 && this.state.openTxnmsg==''){
              this.state.messages.push("This is a Pay At Hotel Booking.");
              }

            }

             //Imported PNR booking.
             if(data.trip_type!==null && data.trip_type!==undefined && data.trip_type===1
                && data.air_bookings[0]!==null && data.air_bookings[0].air_booking_infos!==null && data.air_bookings[0].air_booking_infos[0].status_reason!==null 
                && data.air_bookings[0].air_booking_infos[0].status_reason!==undefined && data.air_bookings[0].air_booking_infos[0].status_reason==='I'  ){
                  if(this.state.messages.length<=0 && this.state.openTxnmsg==''){
                  this.state.messages.push("This booking is Imported PNR booking.");
                  }
                }

                /* if(this.state.messages!==null && this.state.messages!==undefined && this.state.messages!==''){
                  if(this.state.messages .length > 0){
                    this.state.messages=Array.from(new Set(this.state.messages));

                  }

                } 


            }catch(err){
            log.error('Exception occured in Messages Component checkmessages function '+err)    

            }
        }

    } */

 
  
      
      render() {
        //console.log("responsedata"+JSON.stringify(this.props.tripDetail));
        //console.log("messss--"+JSON.stringify(this.state.messages.lenght));
        {this.checkingMessages(this.props.tripDetail)}
         
          return ( 
            <> 

      { this.state.openTxnmsg!==null && this.state.openTxnmsg !== "" && 
            this.state.openTxnmsg !== undefined && (
            <div className="alert alert-warning"><Icon className="noticicon" color="#cda11e" size={16} icon="warning"/>
            {this.state.openTxnmsg}
            </div>
          )}


            { this.state.messages!==null && this.state.messages !== "" && 
            this.state.messages !== undefined && this.state.messages .length>0 && (
            <div className="alert alert-warning"><Icon className="noticicon" color="#cda11e" size={16} icon="warning"/>
            {this.state.messages.map((item, key) => <span key={item}>{item}</span>)}
            </div>
          )}
          </>
          )
  
  
  }

}

 const mapStateToProps = state => {
    //console.log('state.trpReducer.tripDetail Messages----'+JSON.stringify(state.trpReducer.tripDetail));   
    return {
        tripDetail: state.trpReducer.tripDetail,
          
        
       
    };
}
export default connect(mapStateToProps, null)(Messages); 
