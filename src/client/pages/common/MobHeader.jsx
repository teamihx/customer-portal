import React, { Component } from 'react';
import MobMenu from '../common/MobMenu.jsx';
import Icon from 'Components/Icon';
import logo from 'Assets/images/logo.gif';
import 'Assets/styles/_mobmenu.scss';
class MobHeader extends Component {


    constructor(){
        super();
        this.state = {
            mobMenu: false
        }
        this.showDropdownMenu = this.showDropdownMenu.bind(this);
        this.hideDropdownMenu = this.hideDropdownMenu.bind(this);
    }
    showDropdownMenu(event) {
       event.preventDefault();
       this.setState({ mobMenu: true }, () => {
       document.addEventListener('click', this.hideDropdownMenu);
       });
     }
   
     hideDropdownMenu() {
       this.setState({ mobMenu: false }, () => {
         document.removeEventListener('click', this.hideDropdownMenu);
       });
         
     }

  render() {
    return (
        <div className="header">       
        <div className="mobe"><a href="#" onClick={this.showDropdownMenu}> <Icon className="select-me" color="#21409a" size={30} icon="menu" /></a>
        <div className={(this.state.mobMenu ? "visible " : "")}>
                        < MobMenu />
                        </div>
                    </div>
      
         <div className="logo pt-2"> { <a href="/" onClick={e => e.preventDefault()}><img src={logo} className="App-logo" title="Cleartrip Boportal" alt="logo" /></a> }</div>
           
          </div> 
    );
  }
}

export default MobHeader;
