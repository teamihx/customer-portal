const isNotNull = data => data !== null
  && data !== undefined
  && data !== 'null'
  && data !== 'undefined';

const isNotNullEmptyString = data => isNotNull(data) && data !== '';

const isSuccessResponse = status => isNotNullEmptyString(status) && status === 200;
const doesObjHasProp = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);
const isArrayNotEmpty = arr => isNotNull(arr) && arr.length > 0;

module.exports = {
  isNotNull,
  isNotNullEmptyString,
  isSuccessResponse,
  doesObjHasProp,
  isArrayNotEmpty
};
