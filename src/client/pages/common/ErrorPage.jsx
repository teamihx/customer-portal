import React from 'react';
import PropTypes from 'prop-types';
import classnames from "classnames"; 
import Icon from 'Components/Icon';  

class ErrorPage extends React.Component {
 
    render() {
 
        return (
          <>
           <div className="main-container pagenotfound">
               <p className="t-color2 mb-15"> <Icon color="#F4675F" size={40} icon="warning" />
                   <strong className="secondary-cr d-b mt-10">Oops, Something went wrong.</strong>
                 {this.props.errorMessageText }</p>
            </div>
          </>
        );
    
        

  
      }

};
ErrorPage.propTypes = {

  /**
   * Specifies classnames for the Button.
   */
  className: PropTypes.string,
  /**
   * Event handler for on click event of the Button.
   */
  onClick: PropTypes.func,
  /**
   * Specifies whether to enable the ripple effect for Button.
   */
  enableRipple: PropTypes.bool
}
ErrorPage.defaultProps = {
  errorMessageText: 'We are Sorry!'

}

export default ErrorPage;