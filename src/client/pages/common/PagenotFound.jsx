import React, { Component } from "react";
import Icon from "Components/Icon";
export const contextPath = process.env.contextpath;
import { Link } from "react-router-dom";

class PagenotFound extends Component {
  constructor(props) {
    super(props);
    this.redirect = this.redirect.bind(this);
  }
  redirect(event) {
    try {
      event.preventDefault();
      let url = "";
      const contextPath = process.env.contextpath;
      //console.log("mypath-------" + contextPath);
      if (event.target.id === "login") {
        //this.props.history.push( contextPath+"/login/")
        window.location = contextPath + "/login/";
      }
    } catch (err) {
      log.error("Exception occured in Footer redirect function---" + err);
    }
  }

  render() {
    return (
      <section className="page-not-found">
        <div className="main-container">
          <p className="t-color2 mb-15">
            {" "}
            <Icon color="#F4675F" size={40} icon="warning" />
            <strong className="secondary-cr d-b mt-10">
              The page you were looking for doesn't exist.
            </strong>
            You may have mistyped the address or the page may have moved. Go to&nbsp; 
             <a id="login" href="/login" onClick={this.redirect} title="Login">
              Login
            </a>
          </p>
        </div>
      </section>
    );
  }
}

export default PagenotFound;
