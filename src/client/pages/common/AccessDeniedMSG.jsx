import Icon from 'Components/Icon';
import React, { Component } from 'react';

class AccessDeniedMSG extends Component{
    render() {
        return (
            <>
            <div className="main-container cont-Box text-center denied pt-50">
            <Icon color="#F4675F" size={40} icon="accessdenied" />
                <h3 className="secondary-cr">Access Denied..</h3>
                <div className="container">
                    Please contact with Administrator....
                </div>
                </div>
            </>
        )
    }
}

export default AccessDeniedMSG

