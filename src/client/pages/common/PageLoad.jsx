import React, { Component } from 'react'
import Icon from 'Components/Icon';
import loader1 from 'Assets/images/loader1.gif';  

class PageLoad extends Component {

    render() {
        return (
            <section>
            <div className="main-container text-center cont-sec">
               <div className="ldr"> 
               <div className="page-loader"></div>
               {/* <span><img src={loader1} className="loader" alt="loader"  /></span> */}
                   <strong className="t-color1 d-b mt-15 font-22">Searching for trip details</strong>
                 </div>
            </div>
            </section>
        )
    }
}

export default PageLoad;