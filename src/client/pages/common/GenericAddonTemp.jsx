import React, { Component } from "react";
import Icon from "Components/Icon";
import FooterGeneric from "Pages/common/FooterGeneric";
import PrintHeader from "Pages/common/PrintHeader";
import "Assets/styles/_addon.scss";
const GenericAddonTemp = props => (
  <section className="generic-addon">
    <PrintHeader />
    <section>
      <div className="main-container pageTtl">
        <h3>{props.name}</h3>
        <div className="print-me">
          <Icon color="#4D6F93" size={20} icon="printer" />
          {props.sideHeaderComponent}
        </div>
      </div>
    </section>
    <div> {props.children}</div>
    <FooterGeneric />
  </section>
);

export default GenericAddonTemp;
