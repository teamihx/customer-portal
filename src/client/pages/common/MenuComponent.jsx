import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Icon from 'Components/Icon';
import log from 'loglevel';
import AuthenticationService from '../../services/authentication/AuthenticationService';

export const contextPath=process.env.contextpath;

export const COMP_CONFIG_ROLES = 'compConfigRoles'
export const TRIPS_ROLES = 'tripsRoles'
import Domainpath from '../../services/commonUtils/Domainpath';
class MenuComponent extends Component {
    
    constructor(){
        super();
        this.redirect=this.redirect.bind(this);
        this.state = {
            displayMenu: false,
            userLink : false,
            dashboardEnable: false,
            tripEnable: false,
            peopleEnable: false,
            placesEnable: false,
            myTripEnable: false,
            myAccountEnable: false,
            whatYouThinkEnable: false,
            checkMenus:true
          };
     
       this.showDropdownMenu = this.showDropdownMenu.bind(this);
       this.hideDropdownMenu = this.hideDropdownMenu.bind(this);
       this.showUserlink = this.showUserlink.bind(this);
       this.hideUserlink = this.hideUserlink.bind(this);
     
     };

     checkMenuRoles(){
        try{
            let compObj=JSON.parse(localStorage.getItem(COMP_CONFIG_ROLES));
            if(compObj!==null && compObj!==undefined && compObj.companyMenuRoles!==null && compObj.companyMenuRoles!==undefined){
                this.loadMenuRoles(compObj);
            }else{
            let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
            if(tripsObj!==null && tripsObj!==undefined && tripsObj.companyMenuRoles!==null && tripsObj.companyMenuRoles!==undefined){
                this.loadMenuRoles(tripsObj);
            }
            }
    }catch(err){
        log.error('Exception occured in checkMenuRoles constructor ---'+err);
    }
    }

    loadMenuRoles(menuRoles){
        try{
            this.state.dashboardEnable=menuRoles.companyMenuRoles.COMPANY_DASHBOARD_ROLE_ENABLE;
            this.state.tripEnable=menuRoles.companyMenuRoles.COMPANY_TRIPS_ROLE_ENABLE;
            this.state.peopleEnable=menuRoles.companyMenuRoles.COMPANY_PEOPLE_ROLE_ENABLE;
            this.state.placesEnable=menuRoles.companyMenuRoles.COMPANY_PLACES_ROLE_ENABLE;
            this.state.myTripEnable=menuRoles.companyMenuRoles.COMPANY_MYTRIP_ROLE_ENABLE;
            this.state.myAccountEnable=menuRoles.companyMenuRoles.COMPANY_MYACCOUNT_ROLE_ENABLE;
            this.state.whatYouThinkEnable=menuRoles.companyMenuRoles.COMPANY_WHATUTHINK_ROLE_ENABLE;
            this.state.checkMenus=false;
        }catch(err){
            log.error('Exception occured in loadMenuRoles constructor ---'+err);
        }
    }
     showDropdownMenu(event) {
         try{
        event.preventDefault();
        this.setState({ displayMenu: true }, () => {
        document.addEventListener('click', this.hideDropdownMenu);
        });
    }catch(err){
        log.error('Exception occured in MenuComponent showDropdownMenu function ---'+err);
    }
      }    
      hideDropdownMenu() {
          try{
        this.setState({ displayMenu: false }, () => {
          document.removeEventListener('click', this.hideDropdownMenu);
        });
          }catch(err){
            log.error('Exception occured in MenuComponent hideDropdownMenu function ---'+err);
          }
      }
  
      showUserlink(event) {
        event.preventDefault();
        this.setState({ userLink : true }, () => {
        document.addEventListener('click', this.hideUserlink);
        });
      }
    
      hideUserlink() {
        this.setState({ userLink : false }, () => {
          document.removeEventListener('click', this.hideUserlink);
        });
          
      }

  hideDropdownMenu() {
    try {
      this.setState({ displayMenu: false }, () => {
        document.removeEventListener('click', this.hideDropdownMenu);
      });
    } catch (err) {
      log.error(
        `Exception occured in MenuComponent hideDropdownMenu function ---${err}`,
      );
    }
  }

  showUserlink(event) {
    event.preventDefault();
    this.setState({ userLink: true }, () => {
      document.addEventListener('click', this.hideUserlink);
    });
  }

  hideUserlink() {
    this.setState({ userLink: false }, () => {
      document.removeEventListener('click', this.hideUserlink);
    });
  }
  getInitials(name) {
    let initials = 'CT'
    if(name) {
      const [first,last] = name.split('.');
      initials = `${first ? first.charAt(0) : ''}${last ? last.charAt(0) : ''}`;
    }
    return initials
  }
  redirect(event) {
    try {
      var domainpath = Domainpath.getHqDomainPath();
      console.log(`domainpath${domainpath}`);
      event.preventDefault();
      if (event.target.id === 'myTrips') {
        window.location = `${domainpath}/account/trips`;
      } else if (event.target.id === 'account') {
        window.location = `${domainpath}/account/profile?bcvr=true`;
      } else if (event.target.id === 'tellUs') {
        window.location = 'https://www.surveymonkey.com/r/L2Z9GZY?sm=YICZQSE2%2fOUdX9QuCvyfjDAEgqZg8YplUIK2YGoHrqI%3d';
      } else if (event.target.id === 'dashboard') {
        window.location = `${domainpath}/hq`;
      } else if (event.target.id === 'trips') {
        window.location = `${domainpath}/hq/trips`;
      } else if (event.target.id === 'places') {
        window.location = `${domainpath}/hq/places`;
      } else if (event.target.id === 'gst') {
        // window.location = "/boportal/company/gst"
        window.location = `${contextPath}/company/gst`;
      } else if (event.target.id === 'companyConfig') {
        // window.location = "/boportal/company/config"
        window.location = `${contextPath}/company/config`;
      } else if (event.target.id === 'people') {
        window.location = `${domainpath}/hq/people`;
      }
    } catch (err) {
      log.error(
        `Exception occured in MenuComponent redirect function ---${err}`,
      );
    }
  }

    render() {
        const USER_NAME_SESSION_ATTRIBUTE_NAME = "authenticatedUser";
        let userName=localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        if(this.state.checkMenus){
            this.checkMenuRoles();
          }
        let displayMenuOptions = true
        if(this.props.value==='eTicket' || this.props.value==='voucher'){
            displayMenuOptions = false
        }

        return (
            <nav className="navigation">
                { displayMenuOptions && (
                <ul className="navigation__main-menu">
                    {this.state.dashboardEnable && (
                        <li><a id="dashboard" href="/hq/" onClick={this.redirect} className="navbar-brand">Dashboard</a></li>
                    )}
                    {this.state.tripEnable && (
                        <li><a id="trips" href="/hq/trips/" onClick={this.redirect} className="navbar-brand">Trips</a></li>
                    )}
                     {this.state.peopleEnable && (
                        <li><a id="people" href="/hq/people/" onClick={this.redirect} className="navbar-brand">People</a></li>
                    )}
                    {this.state.placesEnable && (
                        <li><a id="places" href="/hq/places/" onClick={this.redirect} className="navbar-brand">Places</a></li>
                    )}
                    <li><a id="company" href="#" onClick={this.showDropdownMenu} className={`${this.state.displayMenu ? 'show-nav' : 'hide-nav'} company-menu`}>Company<Icon className="select-me" color="#fff" size={12} icon="down-arrow" /></a>
                    { this.state.displayMenu ? (
                        <ul className="navigation__sub-menu" ref={(element) => {this.dropdownMenu = element ;}}>
                            <li><a id="companyConfig" href="#" onClick={this.redirect} className="navbar-brand">Company Config</a></li>
                            <li><a id="gst" href="#" onClick={this.redirect} className="navbar-brand">GST</a></li>
                        </ul>
                    ) : (
                        null
                    )
                    }
                    </li>
                </ul>)
                    }
                <div className="user-login">
                    <a href="#" onClick={this.showUserlink} className={this.state.userLink ?  'show-nav' : 'hide-nav'}>
                      <span className="user-login__initials">{this.getInitials(userName)}</span>
                      <span className="user-login__email">{userName}</span>
                      <Icon className="select-me" color="#fff" size={12} icon="down-arrow" />
                    </a>
                    <div className={`${this.state.userLink ? "visible " : "hidden"} user-login__menu`}>
                    <ul>
                    {this.state.myTripEnable && (
                        <li><Link id="myTrips" className="nav-link" onClick={this.redirect} to={contextPath+"/login"}><Icon className="noticicon mr-5" color="#36c" size={14} icon="view-det"/> My Trips</Link></li>
                    )}
                    {this.state.myAccountEnable && (
                        <li><Link id="account" className="nav-link" onClick={this.redirect} to={contextPath+"/login"}><Icon className="noticicon mr-5" color="#36c" size={14} icon="user2"/> My Account</Link></li>
                    )}
                    {this.state.whatYouThinkEnable && (
                        <li><Link id="tellUs" className="nav-link" onClick={this.redirect} to={contextPath+"/login"}><strong><Icon className="noticicon mr-5" color="#36c" size={14} icon="feedback"/> Tell us what you think</strong></Link></li>
                    )}
                    {<li><Link className="nav-link" onClick={AuthenticationService.logout} to={contextPath+"/login"}><Icon className="noticicon mr-5" color="#36c" size={14} icon="signoff"/> Logout</Link></li>}
                    </ul>
                    </div>
                    
                </div>
            </nav>
        )
    }
}

export default MenuComponent;
