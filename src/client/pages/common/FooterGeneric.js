import React, { Component } from "react";

class FooterGeneric extends Component {

    getYear() {
        return new Date().getFullYear();
    }
  render() {

    return (
      <footer>
        <div className="main-container footer">
          © 2006–{this.getYear()} Cleartrip Private Limited.
        </div>
      </footer>
    );
  }
}

export default FooterGeneric;
