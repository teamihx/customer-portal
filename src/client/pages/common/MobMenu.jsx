import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from 'Components/Icon';
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
import AuthenticationService from '../../services/authentication/AuthenticationService';
export const USER_AUTH_DATA = 'userAuthData'
import log from 'loglevel';

export const contextPath=process.env.contextpath;
import Domainpath from '../../services/commonUtils/Domainpath';
class MobMenu extends Component {
    
    constructor(){
        super();
        this.redirect=this.redirect.bind(this);
        this.state = {
            dashboardEnable: false,
            tripEnable: false,
            peopleEnable: false,
            placesEnable: false,
            myTripEnable: false,
            myAccountEnable: false,
            whatYouThinkEnable: false,
          };

       try{
       let authData = localStorage.getItem(USER_AUTH_DATA);
       var obj = JSON.parse(authData);
       if(obj){
            var dashBoard = parseInt(process.env.companydashboardrole, 10);
            const dshBrdEnb = obj.data.rolesList.find(x => x === dashBoard);
            if(dshBrdEnb){
                this.state.dashboardEnable=true;
            }

            var cmpTrip = parseInt(process.env.companytripsrole, 10);
            const cmpTripEnb = obj.data.rolesList.find(x => x === cmpTrip);
            if(cmpTripEnb){
                this.state.tripEnable=true;
            }

            var cmpPpl = parseInt(process.env.companypeoplerole, 10);
            const cmpPplEnb = obj.data.rolesList.find(x => x === cmpPpl);
            if(cmpPplEnb){
                this.state.peopleEnable=true;
            }

            var cmpPlace = parseInt(process.env.companyplacesrole, 10);
            const cmpPlaceEnb = obj.data.rolesList.find(x => x === cmpPlace);
            if(cmpPlaceEnb){
                this.state.placesEnable=true;
            }

            var cmpMyTrip = parseInt(process.env.companymytripsrole, 10);
            const cmpMyTripEnb = obj.data.rolesList.find(x => x === cmpMyTrip);
            if(cmpMyTripEnb){
                this.state.myTripEnable=true;
            }

            var cmpMyAcc = parseInt(process.env.companymyaccountrole, 10);
            const cmpMyAccEnb = obj.data.rolesList.find(x => x === cmpMyAcc);
            if(cmpMyAccEnb){
                this.state.myAccountEnable=true;
            }

            var cmpWhtTnk = parseInt(process.env.companywhatyouthinkrole, 10);
            const cmpWhtTnkEnb = obj.data.rolesList.find(x => x === cmpWhtTnk);
            if(cmpWhtTnkEnb){
                this.state.whatYouThinkEnable=true;
            }
        }
    }catch(err){

        log.error('Exception occured in MenuComponent constructor ---'+err);
    }
     
     };

  
    redirect(event){

        try{
        var domainpath = Domainpath.getHqDomainPath();
        //console.log('domainpath'+domainpath);
        event.preventDefault();
        if(event.target.id === 'myTrips'){
            window.location = domainpath+"/account/trips"
        }else if(event.target.id === 'account'){
            window.location = domainpath+"/account/profile?bcvr=true"
        }else if(event.target.id === 'tellUs'){
            window.location = "https://www.surveymonkey.com/r/L2Z9GZY?sm=YICZQSE2%2fOUdX9QuCvyfjDAEgqZg8YplUIK2YGoHrqI%3d"
        }else if(event.target.id === 'dashboard'){
            window.location = domainpath+"/hq"
        }else if(event.target.id === 'trips'){
            window.location = domainpath+"/hq/trips"
        }else if(event.target.id === 'places'){
            window.location = domainpath+"/hq/places"
        }else if(event.target.id === 'gst'){
           // window.location = "/boportal/company/gst"   
           window.location = contextPath+"/company/gst"         
        }else if(event.target.id === 'companyConfig'){
            //window.location = "/boportal/company/config" 
            window.location = contextPath+"/company/config" 
        }else if(event.target.id === 'people'){
            window.location =  domainpath+"/hq/people"
        }
    }catch(err){

        log.error('Exception occured in MenuComponent redirect function ---'+err);

    }
    }

    render() {
        let userName=localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        return (
            <nav className="mobNav">
                <ul>
                <li className="userName"><strong> <Icon className="select-me" color="#36c" size={20} icon="user2" /> {userName}</strong></li>
                    {this.state.dashboardEnable && (
                        <li><a id="dashboard" href="/dashboard/" onClick={this.redirect} className="navbar-brand">Dashboard</a></li>
                    )}
                    {this.state.tripEnable && (
                        <li><a id="trips" href="/trips/" onClick={this.redirect} className="navbar-brand">Trips</a></li>
                    )}
                     {this.state.peopleEnable && (
                        <li><a id="people" href="/people/" onClick={this.redirect} className="navbar-brand">People</a></li>
                    )}
                    {this.state.placesEnable && (
                        <li><a id="places" href="/places/" onClick={this.redirect} className="navbar-brand">Places</a></li>
                    )}
                    <li><a id="companyConfig" href="#" onClick={this.redirect} className="navbar-brand sub-navbar-brand">Company</a>
                    <ul className="mobSubMenu" ref={(element) => {this.dropdownMenu = element ;}}>

                            <li><a id="companyConfig" href="#" onClick={this.redirect} className="navbar-brand">Company Config</a></li>
                            <li><a id="gst" href="#" onClick={this.redirect} className="navbar-brand">GST</a></li>
                        </ul>
                    </li>
                </ul>
                <ul className="mbuser">
                    
                    {this.state.myTripEnable && (
                        <li><Link id="myTrips" className="nav-link" onClick={this.redirect} to={contextPath+"/login"}>My Trips</Link></li>
                    )}
                    {this.state.myAccountEnable && (
                        <li><Link id="account" className="nav-link" onClick={this.redirect} to={contextPath+"/login"}>My Account</Link></li>
                    )}
                    {this.state.whatYouThinkEnable && (
                        <li><Link id="tellUs" className="nav-link" onClick={this.redirect} to={contextPath+"/login"}><strong>Tell us what you think</strong></Link></li>
                    )}
                    {<li><Link className="nav-link" onClick={AuthenticationService.logout} to={contextPath+"/login"}>Logout</Link></li>}
                </ul>
            </nav>
        )
    }
}

export default MobMenu