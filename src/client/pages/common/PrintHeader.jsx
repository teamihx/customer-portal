import React, { Component } from "react";
import MenuComponent from "../common/MenuComponent.jsx";
import logo from "Assets/images/logo.gif";
class PrintHeader extends Component {
    constructor(props) {
        super(props);
    }
render() {
  const url = window.location.pathname;
  const value = url.split("/").pop();

  return (
    <header className="app-header">
      <div className="app-header__wrapper">
          <a className="app-header__logo-wrapper" href="/" onClick={e => e.preventDefault()}>
            <img
              src={logo}
              className="app-header__logo"
              title="Cleartrip CX-portal"
              alt="logo"
            />
          </a>
          <MenuComponent/>
      </div>    
  </header>
    
  );
};
}
export default PrintHeader;
