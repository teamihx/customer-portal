import React, { Component } from 'react'
import Icon from 'Components/Icon';

class Oops extends Component {

    render() {
        return (
            <div className="main-container pagenotfound">
               <p className="t-color2 mb-15"> <Icon color="#F4675F" size={40} icon="warning" />
                   <strong className="secondary-cr d-b mt-10">Oops, Something went wrong.</strong>
                 Please try again.</p>
            </div>
        )
    }
}

export default Oops