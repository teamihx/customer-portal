import React, { Component } from "react";
export const COMP_CONFIG_ROLES = 'compConfigRoles'
import log from "loglevel";
export const TRIPS_ROLES = 'tripsRoles'
import Domainpath from '../../services/commonUtils/Domainpath';

class Footer extends Component {
  constructor() {
    super();
    this.redirect = this.redirect.bind(this);
    this.state = {
      bestFareEnable: false,
      airFareEnable: false,
      hotelDirEnable: false,
      blogEnable: false,
      abtCTEnable: false,
      faqEnable: false,
      privacyEnable: false,
      securityEnable: false,
      termsOfUseEnable: false,
      contactEnable: false,
      printEmailEnable: false,
      checkFooterRoles:true
    };
  }
  checkFooterRolesLinks(){
    try{
      let compObj=JSON.parse(localStorage.getItem(COMP_CONFIG_ROLES));
      if(compObj!==null && compObj!==undefined && compObj.companyMenuRoles!==null && compObj.companyMenuRoles!==undefined){
       this.loadFooterRoles(compObj);
      }else{
      let tripsObj=JSON.parse(localStorage.getItem(TRIPS_ROLES));
      if(tripsObj!==null && tripsObj!==undefined && tripsObj.companyMenuRoles!==null && tripsObj.companyMenuRoles!==undefined){
        this.loadFooterRoles(tripsObj);
      }
      }
    }catch(err){
  
          log.error('Exception occured in checkFooterRolesLinks redirect function ---'+err);
  
      }
  }

  loadFooterRoles(roleObj){
    try{
      this.state.bestFareEnable=roleObj.companyMenuRoles.COMPANY_BESTFARE_ROLE_ENABLE;
      this.state.airFareEnable=roleObj.companyMenuRoles.COMPANY_FARES_ROLE_ENABLE;
      this.state.hotelDirEnable=roleObj.companyMenuRoles.COMPANY_HOTELDIR_ROLE_ENABLE;
      this.state.blogEnable=roleObj.companyMenuRoles.COMPANY_BLOG_ROLE_ENABLE;
      this.state.abtCTEnable=roleObj.companyMenuRoles.COMPANY_ABTCT_ROLE_ENABLE;
      this.state.faqEnable=roleObj.companyMenuRoles.COMPANY_FAQ_ROLE_ENABLE;
      this.state.privacyEnable=roleObj.companyMenuRoles.COMPANY_PRIVACY_ROLE_ENABLE;
      this.state.securityEnable=roleObj.companyMenuRoles.COMPANY_SECU_ROLE_ENABLE;
      this.state.termsOfUseEnable=roleObj.companyMenuRoles.COMPANY_TERMS_ROLE_ENABLE;
      this.state.contactEnable=roleObj.companyMenuRoles.COMPANY_CONTACT_ROLE_ENABLE;
      this.state.printEmailEnable=roleObj.companyMenuRoles.COMPANY_PRINT_ROLE_ENABLE;
      this.state.checkFooterRoles=false;
    }catch(err){
          log.error('Exception occured in loadFooterRoles redirect function ---'+err);
      }
  }
  getYear() {
    return new Date().getFullYear();
}

  redirect(event) {
    try {
      event.preventDefault();
      var domainpath = Domainpath.getHqDomainPath();
      if (event.target.id === "print") {
        window.location = domainpath + "/ticket";
      } else if (event.target.id === "airFareMonth") {
        window.location = domainpath + "/calendar/";
      } else if (event.target.id === "airFareSms") {
        window.location = domainpath + "/m";
      } else if (event.target.id === "hotelDir") {
        window.location = "https://www.cleartrip.com/hotels/india/";
      } else if (event.target.id === "blog") {
        window.location = "https://blog.cleartrip.com/";
      } else if (event.target.id === "about") {
        window.location = domainpath + "/about/";
      } else if (event.target.id === "faq") {
        window.location = domainpath + "/faq/";
      } else if (event.target.id === "policy") {
        window.location = domainpath + "/privacy/";
      } else if (event.target.id === "security") {
        window.location = domainpath + "/security/";
      } else if (event.target.id === "terms") {
        window.location = domainpath + "/terms/";
      } else if (event.target.id === "contact") {
        window.location = domainpath + "/contact/";
      }
    } catch (err) {
      log.error("Exception occured in Footer redirect function---" + err);
    }
  }

  render() {
    if(this.state.checkFooterRoles){
      this.checkFooterRolesLinks();
    }
    return (
      <footer>
        <div className="main-container footer">
          <ul id="">
            {this.state.printEmailEnable && (
              <li className="first">
                <a
                  id="print"
                  href="/ticket"
                  onClick={this.redirect}
                  title="Print or email e-tickets booked with Cleartrip"
                >
                  Print/Email your e-tickets
                </a>
              </li>
            )}
            {this.state.bestFareEnable && (
              <li>
                <a
                  id="airFareMonth"
                  href="/calendar"
                  onClick={this.redirect}
                  title="See the best air fares for an entire month"
                >
                  See best fares by month
                </a>
              </li>
            )}
            {this.state.airFareEnable && (
              <li>
                <a
                  id="airFareSms"
                  href="/mobile"
                  onClick={this.redirect}
                  title="Get airfares at your fingertips via SMS"
                >
                  Search airfares via SMS
                </a>
              </li>
            )}
            {this.state.hotelDirEnable && (
              <li>
                <a
                  id="hotelDir"
                  href="/hotels/india/"
                  onClick={this.redirect}
                  title="India hotel directory"
                >
                  India hotel directory
                </a>
              </li>
            )}
          </ul>
          <ul className="footerSub">
            {this.state.blogEnable && (
              <li className="first">
                <a id="blog" href="/blog/" onClick={this.redirect}>
                  Cleartrip Blog
                </a>
              </li>
            )}
            {this.state.abtCTEnable && (
              <li>
                <a id="about" href="/about/" onClick={this.redirect}>
                  About Cleartrip
                </a>
              </li>
            )}
            {this.state.faqEnable && (
              <li>
                <a id="faq" href="/faq/" onClick={this.redirect}>
                  FAQs
                </a>
              </li>
            )}
            {this.state.privacyEnable && (
              <li>
                <a id="policy" href="/privacy/" onClick={this.redirect}>
                  Privacy Policy
                </a>
              </li>
            )}
            {this.state.securityEnable && (
              <li>
                <a id="security" href="/security/" onClick={this.redirect}>
                  Security
                </a>
              </li>
            )}
            {this.state.termsOfUseEnable && (
              <li>
                <a id="terms" href="/terms/" onClick={this.redirect}>
                  Terms of use
                </a>
              </li>
            )}
            {this.state.contactEnable && (
              <li>
                <a id="contact" href="/contact/" onClick={this.redirect}>
                  Contact
                </a>
              </li>
            )}
          </ul>
          <p className="t-color2 mb-15">© 2006–{this.getYear()} Cleartrip Private Limited.</p>
        </div>
      </footer>
    );
  }
}

export default Footer;
