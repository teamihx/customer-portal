const axios = require("axios");
axios.defaults.timeout = 1000 * 6;
const winston = require("winston");
const { sillyLogConfig } = require("../../../helper/logger.js").winston;
// Create the logger
const logger = winston.createLogger(sillyLogConfig);
const Ldapmodel = require('../../models/ldapTask-model.js');
const circularJSON = require('circular-json');
const publicIp = require('public-ip');
const osInterfaces = require('os')

const responseCreator = (status, msg, data) => {
  const obj = {
    status,
    msg,
    data
  }; 
  return obj;
};

exports.inserUserData = async req => {

  logger.info("inserUserData request is start ");
  try {
    //get encrypted password
    return await this.encryptPassword(req).then(async responseEncrypt => {
      //logger.info("pwdReq: "+ circularJSON.stringify(responseEncrypt));
      
     // console.log("inserUserData body: "+req.body)    
      if (!req.body) {
        logger.info("You must provide transaction detail")
      }
      req.body.password = responseEncrypt.data.data;

	//get public and client ip address
	return await publicIp.v4().then(async ip => {
    //console.log("your public ip address", ip);
	const clientIpaddress = getIPAddress();
  // logger.info("ipaddress client  "+clientIpaddress);
   
 	req.body.public_ip_address = ip;
	req.body.client_ip_address = clientIpaddress;
	
	//check if user is already available in mongo
	return await this.checkIfUserAlreadyAvailableMongo(req).then(userData => {	
	logger.info('userData inserUserData '+circularJSON.stringify(userData));
	let returnValue ="";
	if (userData !== undefined && userData !== "" &&   userData.status != undefined &&   
      userData.status === 200
    ) {
	logger.info("User already available with same ip address -------");
	 returnValue = userData;
	
	}else{
	let ldapTxn = new Ldapmodel(req.body);
      
       ldapTxn
      .save()
      .then(() => {
        logger.info("Saved MongoDB successfully -------");
        returnValue = responseCreator(200, "Success");
      })

      .catch(error => {
        logger.info("Exception in transaction is not persisted due to" + error)
 		returnValue = responseCreator(500, "Failed");
      })
	}
	logger.info('userData return '+circularJSON.stringify(returnValue));
	return returnValue;
    
  }).catch((error) => {
    assert.isNotOk(error,'Promise error');
  });

	

    
	}).catch((error) => {
	   logger.info("Exception in transaction is not persisted due to" + error);
 	 returnValue = responseCreator(500, "Failed");
	});

      
        

    }).catch(error => {
      logger.info("Exception in encryptPassword  due to" + error);
      returnValue = responseCreator(500, "Failed");
    })

  } catch (e) {
    logger.error(
      "exception occured in auth.service inserUserData  function--- " +
      circularJSON.stringify(e)
    );
    returnValue = responseCreator(500, "catch block response(exception)");
  }
  logger.info("inserUserData request is end ");
  return returnValue;


};


exports.checkIfLDAPAuthAvailable =  req => {
  logger.info("checkIfLDAPAuthAvailable request is start ");
  let returnValue = null;
  try {
    const ldapTokenBrowser = getLdapKey(req);
	
    if (ldapTokenBrowser !== null && ldapTokenBrowser !== undefined) {      
           
          const request = {
            body: {
              token: ldapTokenBrowser
            }
          }
         // logger.info("findOne request is data "+request.body.token);
         return Ldapmodel.findOne({ token_id: request.body.token }).then(data => {
            logger.info("findOne request is data "+data);
            if (data.token_id === request.body.token) {
              logger.info("matched ");
              let output = responseCreator(200, "Success","OK");
              logger.info('circularJSON.stringify(output)'+circularJSON.stringify(output));
              return output;
            }else{
              logger.info("not matched ");
              return responseCreator(500, "Failed");
            }


          }).catch(err => logger.info(err))
    }

  } catch (e) {
    logger.error(
      "exception occured in auth.service checkIfLDAPAuthAvailable  function--- " +
      circularJSON.stringify(e)
    );
    returnValue = responseCreator(500, "Failed");
  }
  logger.info("checkIfLDAPAuthAvailable request is end " + circularJSON.stringify(returnValue));
  return returnValue;
};

exports.checkUserLDAPService = async req => { 
logger.info("checkUserLDAPService Authrize api....start"); 
return await this.encryptPassword(req).then(responseEncrypt => {
  //logger.info("pwdReq: "+ circularJSON.stringify(responseEncrypt));
  let returnValue = null;
  const url = process.env.ldapURL
  const pwd = responseEncrypt.data.data;
  const ldapUrl = url + "?username=" + req.body.username + "&password=" + pwd;
  //logger.info("checkUserLDAPService Authrize api...." + ldapUrl);
	if (responseEncrypt !== undefined && responseEncrypt !== "" &&
      responseEncrypt.data !== "" &&
      responseEncrypt.data !== undefined &&
      responseEncrypt.status === 200 && responseEncrypt.data.status==="SUCCESS"
    ) {
	 logger.info("encryptPassword Success" );
	return axios.get(ldapUrl).then(function (response) {
    if (response !== undefined && response !== "" &&
      response.data !== "" &&
      response.data !== undefined &&
      response.status === 200
    ) {
      //logger.info("response......" + circularJSON.stringify(response));           
      returnValue = responseCreator(response.status, response.statusTxt, generateRamdomAlphaNum(8));
      return returnValue;

    }
    else {
      return returnValue = responseCreator(500, "Unable to process the request");
    }
  })
    .catch(function (error) {
     returnValue = responseCreator(500, "Unable to process the request", "Failed");
      logger.info(
        "checkUserLDAPService exception Response data in  is ---" +
        circularJSON.stringify(returnValue)
      );
      logger.info("checkUserLDAPService error is ----" + error);
      return returnValue;
    });
	}else {
      return returnValue = responseCreator(500, "Unable to process the request");
    }

  
      
    }).catch(error => {
      logger.info("Exception in checkUserLDAPService due to" + error);
      returnValue = responseCreator(500, "Failed");
    })

};


var generateRamdomAlphaNum= function (length) {
  let result           = '';
  const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

var getLdapKey = function (req) {
  let u_session_id = null;   
  if (req.headers !== null && req.headers.cookie !== null && req.headers.cookie !== undefined) {
    u_session_id = get_cookies(req)['u_session_id'];
  }
  logger.info("checkIfLDAPAuthAvailable request is getLdapKey--- "+u_session_id);
  return u_session_id;
};


var get_cookies = function (request) {
  var cookies = {};
  request.headers && request.headers.cookie.split(';').forEach(function (cookie) {
    var parts = cookie.match(/(.*?)=(.*)$/)
    cookies[parts[1].trim()] = (parts[2] || '').trim();
  });
  return cookies;
};

exports.encryptPassword = req => {
  logger.info("encryptPassword Authrize api..start.." );
  let returnValue = null;
  const url = process.env.ldapEncriptURL
  //logger.info("encryptPassword Authrize api..1.." +circularJSON.stringify(req));
  const ldapUrl = url;

  const encryptRQ ={
	  "data": req.body.password,
	  "type": "ENCRYPT"
	}

  //logger.info("encryptPassword Authrize api...." + ldapUrl);

  return axios
    .post(ldapUrl, encryptRQ, { headers: { Accept: "application/json" } }).then(function (response) {
	//logger.info("encryptPassword response......" + circularJSON.stringify(response));  
    if (response !== undefined && response !== "" &&
      response.data !== "" &&
      response.data !== undefined &&
      response.status === 200
    ) {
               
      returnValue = responseCreator(response.status, response.statusTxt, response.data);
      return returnValue;

    }
    else {
      return returnValue = responseCreator(500, "Unable to process the request");
    }
  })
    .catch(function (error) {
      
      returnValue = responseCreator(500, "Unable to process the request", "Failed");
      logger.info(
        "encryptPassword exception Response data in  is ---" +
        circularJSON.stringify(returnValue)
      );
      logger.info("encryptPassword error is ----" + error);
      return returnValue;
    });

};

exports.checkIfUserAlreadyAvailableMongo =  req => {
  logger.info("checkIfUserAlreadyAvailableMongo request is start ");
  let returnValue = null;
  try {	
	
    //console.log("your public ip address", req.body.ip_address + " password: "+req.body.password + " username: "+ req.body.username);
     return  Ldapmodel.findOne({ password: req.body.password, username: req.body.username, public_ip_address: req.body.public_ip_address, client_ip_address: req.body.client_ip_address}).then(data => {
            //logger.info("checkIfUserAlreadyAvailableMongo findOne request is data "+data);
            if (data.token_id !== null) {
              logger.info("matched ");
			
              let output = responseCreator(200, "data available",data.token_id);
              logger.info('checkIfUserAlreadyAvailableMongo circularJSON.stringify(output)'+circularJSON.stringify(output));
              return output;
            }else{
              logger.info("not matched ");
              return responseCreator(500, "Failed");
            }


          }).catch(err => logger.info(err))


  } catch (e) {
    logger.error(
      "exception occured in auth.service checkIfUserAlreadyAvailableMongo  function--- " +
      circularJSON.stringify(e)
    );
    returnValue = responseCreator(500, "Failed");
  }
  logger.info("checkIfUserAlreadyAvailableMongo request is end " + circularJSON.stringify(returnValue));
  return returnValue;
};

function getIPAddress() {
  var interfaces = osInterfaces.networkInterfaces();
  for (var devName in interfaces) {
    var iface = interfaces[devName];

    for (var i = 0; i < iface.length; i++) {
      var alias = iface[i];
      if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal)
        return alias.address;
    }
  }

  return '0.0.0.0';
}


