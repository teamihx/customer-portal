const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');


const app = express();
const JSON = require('circular-json');
var base64 = require('base-64');
var utf8 = require('utf8');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
axios.defaults.timeout = 1000*6;

const winston = require('winston');
// App settings 
const { sillyLogConfig } = require('../../../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);

const ldapTaskctrl  = require('../../../server/controllers/ldapAuth-ctrl')

exports.authenticateUser = function (req, cb) {
    logger.info("AuthNodeService checkUsrLogin Request is : ");
    const signInUrl=process.env.companysigninapiurl;
    //logger.info("Connecting Sign api.." +signInUrl);
    axios
      .post(signInUrl, req.body, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
          Accept: "text/json",
          "X-Forwarded-Proto": "https"
        }
      })
      .then(function(response) {
        const jsondata = JSON.stringify(response);
        cb(response);
      })
      .catch(function (error) {
        logger.error(error);
        var resp = '';
        resp = {
            status: '404',
            msg:'Resource Not Found'
        }
        logger.error("Error in authenticating User : " + error);
        cb(resp);
    });
}




exports.userAuthrization = function (req, cb) {
const authUrl=process.env.companyauthapiurl;
const authKey=process.env.authkey;
var ctKey = get_cookies(req)['ct-auth'];
if(ctKey!==null && ctKey!==""){
  //logger.info("FINAL CT KEY: " + ctKey);
  //logger.info("Connecting Authrize api...." +authUrl);
  axios.get(authUrl,{
      headers: {
        "Access-Control-Allow-Origin": "*",
        AUTH_TYPE: "COOKIE",
        Cookie: "ct-auth="+JSON.stringify(ctKey),
        auth_key:authKey
      }
    })
    .then(function(response) {
      const jsondata = JSON.stringify(response);
      //logger.info('AUTH RESPONSE : ----'+jsondata);
      cb(response);
    })
    .catch(function (error) {
      var resp = '';
      resp = {
          status: '404',
          msg:'Resource Not Found'
      }
      logger.error("Error in user Authrization : " + error);
      cb(resp);
  });
}
}



var get_cookies = function(request) {
var cookies = {};
request.headers && request.headers.cookie.split(';').forEach(function(cookie) {
var parts = cookie.match(/(.*?)=(.*)$/)
cookies[ parts[1].trim() ] = (parts[2] || '').trim();
});
return cookies;
};

app.get('/logout', (req, res) => {
  res.clearCookie('token');
  return res.status(200).redirect('/login');
});

exports.logout = function (req,res) {
  logger.info("AuthNodeService logout start....");
  res.cookie("ct-auth", "", { expires: new Date(0), path: '/' });
  res.cookie('Apache', '',{ expires: new Date(0), path: '/' });
	res.cookie('userid', '',{ expires: new Date(0), path: '/' });
	res.cookie('_session_id', '',{ expires: new Date(0), path: '/' });
	res.cookie('ct-auth-preferences', '',{ expires: new Date(0), path: '/' });
	res.cookie('currency-pref', '',{ expires: new Date(0), path: '/' });
	res.cookie('usermisc', '',{ expires: new Date(0), path: '/' });	
  logger.info("AuthNodeService logout ended....");
  return res.status(200);
}

exports.getAuthKey = function (req,res) {
  if(req.headers!==null && req.headers.cookie!==null && req.headers.cookie!==undefined){
  return ctKey = get_cookies(req)['ct-auth'];    
  } 
}


exports.createEncryptToken = function (req,res) {
  var bytes="";
  if(req!=null){
  if( req.body!=null&& req.body.userToken!==null){
     bytes = utf8.encode(req.body.userToken);
  }else{
     bytes = utf8.encode(req);
  }
  var encoded = base64.encode(bytes);
  //logger.info("Token Encrypted --- "+encoded);
  return encoded;    
  } 
}
