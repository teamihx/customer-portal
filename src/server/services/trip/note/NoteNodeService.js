const express = require('express');

const bodyParser = require('body-parser');
const axios = require('axios');


const app = express();


const JSON = require('circular-json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));
axios.defaults.timeout = 1000*6;

const winston = require('winston');
const { sillyLogConfig } = require('../../../../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);


exports.upateNotesApi = function (req, res) {
  logger.info("upateNotesApi request is start--- ");
  //logger.info("upateNotesApi request is --- " + JSON.stringify(req.body));
  
  const getTripURL=process.env.updateairtkturl;
  //logger.info("upateNotesApi getTripURL is start--- " );
  axios.put(getTripURL, req.body).then(function(response) {
     //logger.info("upateNotesApi response is1--- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.status !== "" &&
          response.status !== undefined &&
          response.status === 200) {
          var updateNoteRes = response.data;
          updateNoteRes.responsecode = response.status;
          updateNoteRes.msg = "SUCCESS";
          res(updateNoteRes);
        } else {
          this.getTripRes = {
            status: "404",
            description: "Note not updated successful!!",
            updateStatus: false,
          };
          logger.info("upateNotesApi response is2--- " + JSON.stringify(this.getTripRes));
          res(this.getTripRes);
        }
      }
    })
    .catch(error => {

      this.getTripRes = {
        status: "404",
        description: "Trip not updated successful!!",
        updateStatus: false,
      };
      logger.info("getTripData response is3--- " + JSON.stringify(this.getTripRes));
      logger.error("exception ocuured in noteNodeService upateNotesApi error is--- " + JSON.stringify(error));
      res(this.getTripRes);        
        
    });
}
exports.insertPayment = function (req, res) {
  logger.info("insertPayment request is start--- ");
  //logger.info("insertPayment request is --- " + JSON.stringify(req.body));
 // const url=process.env.insertPmnt
  const txnURL=process.env.insertPmnt;
  //logger.info("insertPayment txnURL is --- " +txnURL);
  axios.post(txnURL, req.body).then(function(response) {
     logger.info("insertPayment response is1--- " + JSON.stringify(response));
      if (typeof response !=='undefined' && response !== "") {
        if (typeof response !=='undefined' && response !== "") {
          var updateTxnRes = response.data;
         // updateInsert.responsecode = response.status;
         updateTxnRes.msg = "SUCCESS";
          logger.info("insertPayment response is4--- " + JSON.stringify(response));
          res(updateTxnRes);
        } else {
          this.getTripRes = {
            status: "404",
            description: "Note not updated successful!!",
            updateStatus: false,
          };
          logger.info("updateInsert response is2--- " + JSON.stringify(this.getTripRes));
          res(this.getTripRes);
        }
      }
    })
    .catch(error => {

      this.getTripRes = {
        status: "404",
        description: "txn not updated successful!!",
        updateStatus: false,
      };
      logger.info("getTripData response is3--- " + JSON.stringify(this.getTripRes));
      logger.error("exception ocuured in noteNodeService createNewTransaction error is--- " + JSON.stringify(error));
      res(this.getTripRes);        
        
    });
}
exports.createNewTransaction = function (req, res) {
  logger.info("createNewTransaction request is start--- ");
  //logger.info("createNewTransaction request is --- " + JSON.stringify(req.body));
  
  const txnURL=process.env.updateairtkturl;
  //logger.info("createNewTransaction txnURL is --- " +txnURL);
  axios.put(txnURL, req.body).then(function(response) {
     //logger.info("createNewTransaction response is1--- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.status !== "" &&
          response.status !== undefined &&
          response.status === 200) {
          var updateTxnRes = response.data;
          updateTxnRes.status = response.status;
          updateTxnRes.msg = "SUCCESS";
          res(updateTxnRes);
        } else {
          this.getTripRes = {
            status: "404",
            description: "Note not updated successful!!",
            updateStatus: false,
          };
          logger.info("upateNotesApi response is2--- " + JSON.stringify(this.getTripRes));
          res(this.getTripRes);
        }
      }
    })
    .catch(error => {

      this.getTripRes = {
        status: "404",
        description: "txn not updated successful!!",
        updateStatus: false,
      };
      logger.info("getTripData response is3--- " + JSON.stringify(this.getTripRes));
      logger.error("exception ocuured in noteNodeService createNewTransaction error is--- " + JSON.stringify(error));
      res(this.getTripRes);        
        
    });
}