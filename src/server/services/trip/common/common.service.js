const axios = require("axios");
axios.defaults.timeout = 1000 * 10;
const winston = require("winston");
const { sillyLogConfig } = require("../../../../helper/logger.js").winston;
const logger = winston.createLogger(sillyLogConfig);
const circularJSON = require("circular-json");
const responseCreator = (status, msg, data) => {
  const obj = {
    status,
    msg,
    data
  };
  return obj;
};

exports.upateNotesApi = req => {
  logger.info("upateNotesApi service start in common.service --- "+req);
  logger.info("upateNotesApi service start in common.service111 --- "+circularJSON.stringify(req));
  const getTripURL=process.env.updateairtkturl;
  logger.info("upateNotesApi getTripURL is --- "+getTripURL);
  return axios.put(getTripURL, req) 
    .then(response => {
      logger.info("upateNotesApi .then is --- ");
      if (response !== undefined && response !== "") {
        if (response.status !== "" &&
        response.status !== undefined &&
        response.status === 200) {
          return responseCreator(
            response.status,
            response.statusText,
            response.data
          );
        } else {
          return responseCreator(
            response.status,
            response.statusText,
            response.data
          );
        }
      }
    })
    .catch(error => {
      const noteRes = {
        status: "404",
        msg: "exception",
        data: ""
      };
      return noteRes;
    });
};

exports.createNewTransaction = req => {
  logger.info("createNewTransaction service start in common.service --- "+req);
  logger.info("createNewTransaction service start in common.service111 --- "+circularJSON.stringify(req));
  const txnURL=process.env.updateairtkturl;
  logger.info("createNewTransaction txnURL is --- "+txnURL);
  return axios.put(txnURL, req) 
    .then(response => {
      logger.info("createNewTransaction .then is --- ");
      if (response !== undefined && response !== "") {
        if (response.status !== "" &&
        response.status !== undefined &&
        response.status === 200) {
          return responseCreator(
            response.status,
            response.statusText,
            response.data
          );
        } else {
          return responseCreator(
            response.status,
            response.statusText,
            response.data
          );
        }
      }
    })
    .catch(error => {
      const txnRes = {
        status: "404",
        msg: "exception",
        data: ""
      };
      return txnRes;
    });
};








  



