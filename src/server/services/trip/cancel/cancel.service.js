const axios = require("axios");
axios.defaults.timeout = 1000 * 6;
const circularJSON = require("circular-json");
const winston = require("winston");
const { sillyLogConfig } = require("../../../../helper/logger.js").winston;
// Create the logger
const logger = winston.createLogger(sillyLogConfig);
var commonService = require("../../../services/trip/common/common.service");

     exports.refundInfo = async req => {
      const refundUrl = process.env.cancelrefuninfo;
      const apiUrl = `${refundUrl}${req.body.tripRefNumber}`;
      console.log('apiUrl----refund '+apiUrl);
      return await axios.get(apiUrl,{
        }).then(function(response) {
          if (response !== undefined && response !== "") {
            console.log('response----refund '+circularJSON.stringify(response));
            if (
              response.data !== "" &&
              response.data !== undefined &&
              response.status !== null &&
              response.status !== undefined &&
              response.status !== '' 

            ) {
              return responseCreator(
                response.status,
                response.statusText,
                response.data
              );
            } else {
              this.getRefunRes = {
                status: "404",
                msg: "No Refund data!!",
                data: ""
              };
              return this.getRefunRes;
            }
          }
        })
        .catch(error => {
          this.getRefunRes = {
            status: "404",
            msg: "No Refund data!!",
            data: ""
          };
          return this.getRefunRes;
        });
    };

    

    exports.makeCancellation=  async req =>{
      logger.info("makeCancellation entered-- "+circularJSON.stringify(req.body));
      let multiResponse=[];
      const cancelRes=[];
      const noteRes=[];
      const txnResponse=[];
      try{
        if(req.body.note===null || req.body.note===undefined || req.body.note===''){
          req.body.note='Hotel cancellation';

        }
      const noteTobeAdded={
        notes :[
            {
            note: req.body.note,
            parent_note_id: null,
            subject:'Trip Details : ',
            user_id:req.body.user_id,
            trip_id:req.body.trip_id,
            created_at:new Date().toUTCString()

            } 
        ]   
    };     

    let opentxnObj = new Object();      
    opentxnObj.status='O';
    opentxnObj.trip_id=req.body.trip_id;
    opentxnObj.txn_type=20;
    opentxnObj.user_id=req.body.user_id;
    opentxnObj.source_type='HQ',
    opentxnObj.misc='',
    opentxnObj.emails=[],
    opentxnObj.source_id=req.body.source_id
    let opentxnArr=[opentxnObj];          
    const openTxnReq={
        txns:[...opentxnArr]
    }

    

    


    logger.info("noteTobeAdded ---request "+circularJSON.stringify(noteTobeAdded));
    logger.info("noteTobtxnReq---request "+circularJSON.stringify(openTxnReq));

    const txnRes=await commonService.createNewTransaction(openTxnReq);

    
    let closeTxnRes='';
    let finalTxn='';

    logger.info("open txn response "+circularJSON.stringify(txnRes));
    if(txnRes!==null && txnRes!==undefined){
      logger.info("open txn response1");

      if(txnRes.status!==null && txnRes.status!==undefined && txnRes.status!=='' && txnRes.status===200){
        logger.info("open txn response2");
      if(txnRes.data!==null && txnRes.data!==undefined && txnRes.data!==''){
        logger.info("open txn response3");
       
      if(txnRes.data.updateStatus!==null && txnRes.data.updateStatus!==undefined 
          && txnRes.data.updateStatus==='true'){
            logger.info("open txn response4");
            //finalTxn=txnRes.data.modelsUpdated.txns;
            finalTxn=txnRes;
            multiResponse = await Promise.all([this.makeHotelCancellation(req), commonService.upateNotesApi(noteTobeAdded)]);
            logger.info("multiResponse is----"+circularJSON.stringify(multiResponse));

            if(multiResponse!==null && multiResponse!==undefined){

              logger.info("multiResponse1 is----");
                const cancelResponse= multiResponse[0];
                logger.info("cancelResponse is----"+circularJSON.stringify(cancelResponse));
                if(cancelResponse!==null && cancelResponse!==undefined){
                if(cancelResponse.status!==null && cancelResponse.status!==undefined && cancelResponse.status===200
                  && cancelResponse.data!==null && cancelResponse.data!==undefined && cancelResponse.data==='Success'){
                 const txnid=txnRes.data.modelsUpdated.txns

                let closeTxn = new Object(); 
                closeTxn.id=txnid[0].id;      
                closeTxn.status='C';
                closeTxn.trip_id=req.body.trip_id;
                closeTxn.txn_type=20;
                closeTxn.user_id=req.body.user_id;
                closeTxn.source_type='HQ',
                closeTxn.misc='',
                closeTxn.emails=[],
                closeTxn.source_id=req.body.source_id
                let closeTxnArr=[closeTxn];          
                const closeTxnReq={
                    txns:[...closeTxnArr]
                }

                logger.info("close txn ---request "+circularJSON.stringify(closeTxnReq));
    

                closeTxnRes=await commonService.createNewTransaction(closeTxnReq);
                logger.info("closeTxnRes is----"+circularJSON.stringify(closeTxnRes));

                if(closeTxnRes!==null && closeTxnRes!==undefined){
              logger.info("closeTxnRes is1----");

              if(closeTxnRes.status!==null && closeTxnRes.status!==undefined && closeTxnRes.status!=='' && closeTxnRes.status===200){
              if(closeTxnRes.data!==null && closeTxnRes.data!==undefined && closeTxnRes.data!==''){
                logger.info("closeTxnRes is2----");
                
              if(closeTxnRes.data.updateStatus!==null && closeTxnRes.data.updateStatus!==undefined 
                  && closeTxnRes.data.updateStatus==='true'){
                    logger.info("closeTxnRes is3----");
                    finalTxn=closeTxnRes;
                    logger.info("final closeTxnRes is3----"+circularJSON.stringify(finalTxn));
                    multiResponse.push(finalTxn);

                  }
                }
                }else{
                  multiResponse.push(finalTxn);

                }
                
                }

               }else{
   
                  multiResponse.push(finalTxn);

                  }
                 }

                }
        }

        }

      }else{

        
        multiResponse.push(cancelRes);
        multiResponse.push(noteRes);
        multiResponse.push(txnRes);
        
      }

    }
     
  }catch(err){
    logger.error("error in "+err);
    
  }
    logger.info("final response---y "+circularJSON.stringify(multiResponse));
      return multiResponse;

    }



    exports.makeHotelCancellation = req => {
      
      //commonService.upateNotesApi();

        const cancelurl = process.env.autocancel;
        const apiUrl = `${cancelurl}${req.body.tripRefNumber}`;
       // logger.info("makeCancellation service request is---- "+circularJSON.stringify(req));
        logger.info("apiUrl---- "+apiUrl);

        const request={
            source_id:req.body.source_id,
            source_type:req.body.source_type,
            user_id:req.body.user_id,
            wallet:req.body.wallet
        }
        logger.info("makeCancellation service request "+circularJSON.stringify(request));
        return axios.post(apiUrl,request).then(function(response) {
            logger.info("makeCancellation service response111 ");
            if (response !== undefined && response !== "") {
              //logger.info("makeCancellation service response "+circularJSON.stringify(response));
              if (
                response.data !== "" &&
                response.data !== undefined &&
                response.data === "Success"
              ) {
                return responseCreator(
                  response.status,
                  response.statusText,
                  response.data
                );
              } else {
                this.getCanRes = {
                  status: "404",
                  msg: "Cancellation Failed!!",
                  data: ""
                };
                return this.getCanRes;
              }
            }
          })
          .catch(error => {
            this.getCanRes = {
              status: "404",
              msg: "Cancellation Failed!!",
              data: ""
            };
            logger.error('enytered the error '+error);
            return this.getCanRes;
          });
      };

      exports.expressWayDetails = req => {
        const apiUrl = `${expressUrl}${req.body.userId}+"/"+${req.body.currency}`;
        //const apiUrl = process.env.expresswayurl+"41683432/INR";
        return axios.get(apiUrl,{
          }).then(function(response) {
            if (response !== undefined && response !== "") {
              if (
                response !== "" &&
                response !== undefined &&
                response.status === 200
              ) {
                return responseCreator(
                  response.status,
                  response.statusText,
                  response.data
                );
              } else {
                this.getExpressRes = {
                  status: "404",
                  msg: "No Refund data!!",
                  data: ""
                };
                return this.getExpressRes;
              }
            }
          })
          .catch(error => {
            this.getExpressRes = {
              status: "404",
              msg: "No Refund data!!",
              data: ""
            };
            return this.getExpressRes;
          });
      };

      exports.usrApiKeyData = req => {
        const apikeyurl = process.env.userapikeyurl;
        const request={
          "people_ids": [
            req.body.user_id
          ],
          "filters": {
            "exists": [
              "api_key"
            ]
          },
          "projections": ["id","username","api_key"]
         }
        return axios.post(apikeyurl,request,{
          headers: {
          "Content-Type": "application/json"
          }}).then(function(response) {
            if (response !== undefined && response !== "") {
              if (
                response !== "" &&
                response !== undefined &&
                response.status ===200
              ) {
                return responseCreator(
                  response.status,
                  response.statusText,
                  response.data
                );
              } else {
                this.getApiRes = {
                  status: "404",
                  msg: "No Refund data!!",
                  data: ""
                };
                return this.getApiRes;
              }
            }
          })
          .catch(error => {
            this.getApiRes = {
              status: "404",
              msg: "No Refund data!!",
              data: ""
            };
            return this.getApiRes;
          });
      };


  
  const responseCreator = (status, msg, data) => {
    const obj = {
      status,
      msg,
      data
    };
    return obj;
  };

  


   
  
  

