const axios = require("axios");
axios.defaults.timeout = 1000 * 10;
const winston = require("winston");
const { sillyLogConfig } = require("../../../../helper/logger.js").winston;
// Create the logger
const logger = winston.createLogger(sillyLogConfig);
const circularJSON = require("circular-json");
const Trainmodel = require('../../../models/railwayStation-model.js');

     exports.sendTrainSMSDetail = req => {
      logger.info("sendTrainSMSDetail  start--- ");
      var ctKey = get_cookies(req)['ct-auth'];
      var ctAuthPref = get_cookies(req)['ct-auth-preferences'];
      var userid = get_cookies(req)['userid'];
      var usermisc = get_cookies(req)['usermisc'];
      let cookies = "ct-auth="+ctKey+";ct-auth-preferences="+ctAuthPref+";userid="+userid+";usermisc="+usermisc;
      //logger.info("sendTrainSMSDetail service start2 cookies  "+cookies);
      const emailApiUrl = process.env.syncgdsurl;
      const apiUrl = `${emailApiUrl}${req.body.tripRefNumber}/sms?mobile_number=${req.body.phoneNum}`;
      //logger.info("sendTrainSMSDetail service smsURL -- " + apiUrl);
      return axios.get(apiUrl,{
        headers: {
          "Access-Control-Allow-Origin": "*",
          AUTH_TYPE: "COOKIE",
          Cookie: cookies
        }
        }).then(function(response) {
          //logger.info("sendTrainSMSDetail response data  is --- " +circularJSON.stringify(response.data));
          if (response !== undefined && response !== "") {
            if (
              response.data !== "" &&
              response.data !== undefined &&
              response.data === "success"
            ) {
              logger.info(
                "sendTrainSMSDetail response data  is123 --- " +
                  circularJSON.stringify(
                    responseCreator(
                      response.status,
                      response.statusText,
                      response.data
                    )
                  )
              );
              return responseCreator(
                response.status,
                response.statusText,
                response.data
              );
            } else {
              this.getSmsRes = {
                status: "404",
                msg: "SMS not sent!!",
                data: ""
              };
              logger.info(
                "sendActivitySMS response is -- " +
                  circularJSON.stringify(this.getSmsRes)
              );
              return this.getSmsRes;
            }
          }
        })
        .catch(error => {
          this.getSmsRes = {
            status: "404",
            msg: "SMS not sent!!",
            data: ""
          };
          logger.info(
            "exception in sendActivitySMS response catch  block -- " +
              circularJSON.stringify(this.getSmsRes)
          );
          logger.error(
            "exception ocuured in activity.service.js sendActivitySMS function error is--- " +
              circularJSON.stringify(error)
          );
          return this.getSmsRes;
        });
    };

var get_cookies = function(request) {
  var cookies = {};
  request.headers && request.headers.cookie.split(';').forEach(function(cookie) {
  var parts = cookie.match(/(.*?)=(.*)$/)
  cookies[ parts[1].trim() ] = (parts[2] || '').trim();
  });
  return cookies;
  };

  const responseCreator = (status, msg, data) => {
    const obj = {
      status,
      msg,
      data
    };
    return obj;
  };

  exports.getStationNameByCode =  req => {
    logger.info("getStationNameByCode request is start ");
    let returnValue = null;
    try {
      if (req.body.stationCode !== null && req.body.code !== undefined) {      
            logger.info("findOne request is data "+req.body.code);
           return Trainmodel.findOne({ code:req.body.code }).then(data => {
              logger.info("findOne request is data "+data);
              if (data!=undefined){
                  logger.info('circularJSON.stringify(output)'+circularJSON.stringify(data));
                return data.name;
              }else{
                logger.info("getStationNameByCode failed ");
                return responseCreator(500, "Failed");
              }
            }).catch(err => logger.info(err))
      }
    } catch (e) {
      logger.error(
        "exception occured in auth.service getStationNameByCode  function--- " +
        circularJSON.stringify(e)
      );
      returnValue = responseCreator(500, "Failed");
    }
    logger.info("getStationNameByCode request is end " + circularJSON.stringify(returnValue));
    return returnValue;
  };


   
  
  

