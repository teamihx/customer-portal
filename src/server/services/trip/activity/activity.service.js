const axios = require("axios");
axios.defaults.timeout = 1000 * 10;
const winston = require("winston");
const { sillyLogConfig } = require("../../../../helper/logger.js").winston;
// Create the logger
const logger = winston.createLogger(sillyLogConfig);
const circularJSON = require("circular-json");
const { createDomainpath } = require('../../../utils/domainpath.js');
const responseCreator = (status, msg, data) => {
  const obj = {
    status,
    msg,
    data
  };
  return obj;
};

//below function service call for Emailtripdetails
exports.sendActivityEmailDetail = async req => {
  let returnValue = null;
  logger.info("sendActivityEmailDetail request is --- start" );
  const emailApiUrl = process.env.activitysendMail;
  const apiUrl = `${emailApiUrl}${req.trip_id}/email?email=${req.mail_id}&ical=${req.ical}`;
  logger.info("emailservicepoint is --- " + apiUrl);
  try {
    const response = await axios.get(apiUrl);
    if (response !== "undefined") {
      //logger.info("sendActivityEmailDetail SUCCESS RESPONSE--- " +JSON.stringify(response));
      let data = response.data;
      //logger.info("sendActivityEmailDetail SUCCESS data--- " + JSON.stringify(data));
      if (data.status !== "undefined" && data.status === "ACCEPTED") {
        const emailResponse = response.data;
        emailResponse.responseCode = 200;
        emailResponse.msg = "SUCCESS";
        returnValue = responseCreator(200, "SUCCESSS", emailResponse);
        logger.info("response data==2" + JSON.stringify(data));
      } else {
        const getTripRes = {
          status: response.status,
          description: response.data
        };
        logger.info(
          "sendFlightDetailEmail response Failed--- " +
            JSON.stringify(getTripRes)
        );
        returnValue = getTripRes;
      }
    } else {
      returnValue = responseCreator(450, "Response is empty");
    }
  } catch (e) {
    logger.error(
      "exception occured in activity.service sendActivityEmailDetail  function--- " +
        JSON.stringify(e)
    );
    returnValue = responseCreator(404, "catch block response(exception)");
  }
  return returnValue;
};

exports.sendActivityEmail1 = async request => {
  console.log("sendActivityEmail request is start--- " );
  let returnValue = null;
  
  
  const ctKey = get_cookies(request)["ct-auth"];
  const ctAuthPref = get_cookies(request)["ct-auth-preferences"];
  const userid = get_cookies(request)["userid"];
  const usermisc = get_cookies(request)["usermisc"];
  //console.log("after url--- --- " + JSON.stringify(request.body));

  let { ical } = request.body;
  const { to, tripRefNumber } = request.body;
  ical = ical ? "&ical=on" : "";
 // console.log("after url ical is--- --- " + ical);
  const url =
  createDomainpath(request) +
    "/hq/trips/" +
    tripRefNumber +
    "/email?" +
    "email=" +
    to +
    ical;
  //console.log("sendActivityEmail url --- " + url);
  let cookies =
    "ct-auth=" +
    ctKey +
    ";ct-auth-preferences=" +
    ctAuthPref +
    ";userid=" +
    userid +
    ";usermisc=" +
    usermisc;
 // logger.info("sendEmail service start2 cookies  " + cookies);

  try {
    const response = await axios.get(url, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        AUTH_TYPE: "COOKIE",
        Cookie: cookies
      }
    });

    //logger.info("response in activity.service is --- " + JSON.stringify(response));

    if (
      response !== "undefined" &&
      response.data !== "" &&
      response.data !== undefined &&
      response.status === 200
    ) {
      const emailResponse = response.data;
      emailResponse.responseCode = 200;
      emailResponse.msg = "SUCCESS";
      returnValue = responseCreator(200, "SUCCESSS", emailResponse);
      logger.info("response data==2" + JSON.stringify(data));
    } else {
      returnValue = responseCreator(450, "Response is empty");
    }
  } catch (e) {
    returnValue = responseCreator(404, "catch block response(exception)");

    logger.error(
      "exception occured in activity.service sendActivityEmailDetail  function--- " +
        JSON.stringify(e)
    );
  }
  return returnValue;
};

//below function Hq redirection for Emailtripdetails
exports.sendActivityEmail = request => {
  let returnValue = null;
  //logger.info("sendActivityEmail request header in activity.service.js is --- " + JSON.stringify(request.headers));
  //logger.info("sendActivityEmail request body in activity.service.js is --- " +  JSON.stringify(request.body));
  const domainpath = createDomainpath(request);
  const ctKey = get_cookies(request)["ct-auth"];
  const ctAuthPref = get_cookies(request)["ct-auth-preferences"];
  const userid = get_cookies(request)["userid"];
  const usermisc = get_cookies(request)["usermisc"];

  let { ical } = request.body;
  const { to, tripRefNumber } = request.body;
  ical = ical ? "&ical=on" : "";
  const url =
  createDomainpath(request) +
    "/hq/trips/" +
    tripRefNumber +
    "/email?" +
    "email=" +
    to +
    ical;
  //logger.info("sendActivityEmail url in activity.service.js is --- " + url);
  let cookies =
    "ct-auth=" +
    ctKey +
    ";ct-auth-preferences=" +
    ctAuthPref +
    ";userid=" +
    userid +
    ";usermisc=" +
    usermisc;

  return axios
    .get(url, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        AUTH_TYPE: "COOKIE",
        Cookie: cookies
      }
    })
    .then(function(response) {
      //logger.info("sendActivityEmail Response status in activity.service.js is ---" +response.status);
      //logger.info("sendActivityEmail Response data in activity.service.js is ---" +response.data);

      if (response !== undefined && response !== "") {
        if (
          response.data !== "" &&
          response.data !== undefined &&
          response.status === 200
        ) {
          const emailResponse = response.data;
          returnValue = responseCreator(200, "SUCCESSS", emailResponse);
          logger.info(
            "sendActivityEmail success Response data in activity.service.js is ---" +
              JSON.stringify(returnValue)
          );
          return returnValue;
        } else {
          const emailResponse = response.data;
          returnValue = responseCreator(404, "Mail Not sent", emailResponse);
          logger.info(
            "sendActivityEmail failed Response data in activity.service.js is ---" +
              JSON.stringify(returnValue)
          );
          return returnValue;
        }
      }
    })
    .catch(function(error) {
      const emailResponse = error;
      returnValue = responseCreator(404, "Mail Not sent", emailResponse);
      logger.info(
        "sendActivityEmail exception Response data in activity.service.js is ---" +
          JSON.stringify(returnValue)
      );
      logger.info("sendEmail error is ----" + error);
      return returnValue;
    });
};



//below function get link for SMS tripDetails
exports.activitySMSLink = req => {
  logger.info("getActivitySMSLink service start --- " );
  const smsLink = process.env.smslinkurl;
  
  //logger.info("getActivitySMSLink service req--- " + circularJSON.stringify(req.body));
  const smsObj = {
    feature: "sms",
    data: {
      $deeplink_path:
      createDomainpath(req) + "/account/trips/" + req.body.trip_id,
      utm_source: "xdays",
      utm_medium: "sms"
    },
    channel: "xdays",
    app_id: "114631928556048739"
  };
 // logger.info("getActivitySMSLink smsObj req--- " + circularJSON.stringify(smsObj));
  return axios
    .post(smsLink, smsObj)
    .then(response => {
      //logger.info("getActivitySMSLink data is --- " +circularJSON.stringify(response.data));
      if (response !== undefined && response !== "") {
        if (
          response.data !== "" &&
          response.data !== undefined &&
          response.status === 200
        ) {
          logger.info("getSMSLink m view url:" + circularJSON.stringify(response.data) );
          return responseCreator(
            response.status,
            response.statusText,
            response.data
          );
        } else {
          return responseCreator(
            response.status,
            response.statusText,
            response.data
          );
        }
      }
    })
    .catch(error => {
      const smsRes = {
        status: "404",
        msg: "exception",
        data: ""
      };
      logger.info(
        "getSMSLink response in catch block--- " +
          circularJSON.stringify(smsRes)
      );
      logger.error(
        "exception ocuured in activity.service.js activitySMSLink function error is--- " +
          circularJSON.stringify(error)
      );

      return smsRes;
    });
};

//below function send SMStripdetails
exports.sendActivitySMS = req => {
  const smsURL = process.env.smsurl;
  logger.info("sendActivitySMS service start -- " );
  //logger.info("sendActivitySMS service start--- " + circularJSON.stringify(req.body));
  return axios
    .post(smsURL, req.body)
    .then(function(response) {
      //logger.info("sendActivitySMS response data  is --- " +circularJSON.stringify(response.data));
      if (response !== undefined && response !== "") {
        if (
          response.data !== "" &&
          response.data !== undefined &&
          response.data.status !== undefined &&
          response.data.status === "success"
        ) {
          logger.info(
            "sendActivitySMS response data  is123 --- " +
              circularJSON.stringify(
                responseCreator(
                  response.status,
                  response.statusText,
                  response.data
                )
              )
          );
          return responseCreator(
            response.status,
            response.statusText,
            response.data
          );
        } else {
          this.getSmsRes = {
            status: "404",
            msg: "SMS not sent!!",
            data: ""
          };
          logger.info(
            "sendActivitySMS response is -- " +
              circularJSON.stringify(this.getSmsRes)
          );
          return this.getSmsRes;
        }
      }
    })
    .catch(error => {
      this.getSmsRes = {
        status: "404",
        msg: "SMS not sent!!",
        data: ""
      };
      logger.info(
        "exception in sendActivitySMS response catch  block -- " +
          circularJSON.stringify(this.getSmsRes)
      );
      logger.error(
        "exception ocuured in activity.service.js sendActivitySMS function error is--- " +
          circularJSON.stringify(error)
      );
      return this.getSmsRes;
    });
};

//below function send email invoice
exports.sendEmailInvoice = request => {
  let returnValue = null;
  logger.info("sendEmailInvoice request header in activity.service.js is start--- " );

  const { trip_id, email_to } = request.body;

  let invoiceurl = process.env.activityemailinvoice;
  const url = `${invoiceurl}/${trip_id}/invoice?email=${email_to}`;
  //logger.info("sendEmailInvoice url in activity.service.js is --- "+ url);

  return axios
    .get(url)
    .then(function(response) {
     // logger.info("sendActivityEmail Response status in activity.service.js is ---" + circularJSON.stringify(response.status));
      //logger.info("sendActivityEmail Response data in activity.service.js is ---" +circularJSON.stringify(response.data));

      if (response !== undefined && response !== "") {
        if (
          response.data !== "" &&
          response.data !== undefined &&
          response.status === 200
        ) {
          const invoiceResponse = response.data;
          returnValue = responseCreator(
            response.status,
            "SUCCESS",
            invoiceResponse
          );
          logger.info(
            "sendActivityEmail success Response data in activity.service.js is ---" +
              circularJSON.stringify(returnValue)
          );
          return returnValue;
        } else {
          const invoiceResponse = response.data;
          returnValue = responseCreator(404, "Mail Not sent", invoiceResponse);
          logger.info(
            "sendActivityEmail failed Response data in activity.service.js is ---" +
              circularJSON.stringify(returnValue)
          );
          return returnValue;
        }
      }
    })
    .catch(function(error) {
      const invoiceResponse = error;
      returnValue = responseCreator(404, "exception", invoiceResponse);
      logger.info(
        "sendActivityEmail exception Response data in activity.service.js is ---" +
          circularJSON.stringify(returnValue)
      );
      logger.info("sendEmail error is ----" + circularJSON.stringify(error));
      return returnValue;
    });
};

var get_cookies = function(request) {
  var cookies = {};
  request.headers &&
    request.headers.cookie.split(";").forEach(function(cookie) {
      var parts = cookie.match(/(.*?)=(.*)$/);
      cookies[parts[1].trim()] = (parts[2] || "").trim();
    });
  return cookies;
};
