const express = require('express');

const bodyParser = require('body-parser');
const axios = require('axios');


const app = express();


const JSON = require('circular-json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));
axios.defaults.timeout = 1000*6;

const winston = require('winston');
const { sillyLogConfig } = require('../../../../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);
const { createDomainpath } = require('../../../utils/domainpath.js');
//const domainpath=process.env.domainpath;

exports.upateAirTicketapi = function (req, res) {
  logger.info("upateAirTicketapi request is start--- ");
  //logger.info("upateAirTicketapi request is --- " + JSON.stringify(req.body));
  
  const getTripURL=process.env.updateairtkturl;
  //logger.info("upateAirTicketapi getTripURL is --- " +getTripURL);
  axios.put(getTripURL, req.body).then(function(response) {
     //logger.info("upateAirTicketapi response is1--- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.status !== "" &&
          response.status !== undefined &&
          response.status === 200) {
          var updateTktRes = response.data;
          updateTktRes.responsecode = response.status;
          updateTktRes.msg = "SUCCESS";
          res(updateTktRes);
        } else {
          this.getTripRes = {
            status: "404",
            description: "Trip not updated successful!!",
            updateStatus: false,
          };
          //logger.info("upateAirTicketapi response is2--- " + JSON.stringify(this.getTripRes));
          res(this.getTripRes);
        }
      }
    })
    .catch(error => {

      this.getTripRes = {
        status: "404",
        description: "Trip not updated successful!!",
        updateStatus: false,
      };
      logger.info("getTripData response is3--- " + JSON.stringify(this.getTripRes));
      logger.error("exception ocuured in FlightNodeService upateAirTicketapi error is--- " + JSON.stringify(error));
      res(this.getTripRes);        
        
    });
}

exports.updatePaxDetails = function (req, res) {
  logger.info("UPDATE PAX Start--- " ); 
  const updatePaxURL=process.env.updateairtkturl;
  //logger.info("updatePax url is --- " +updatePaxURL);
  axios.put(updatePaxURL, req.body).then(function(response) {
     //logger.info("updatePax response is1--- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.status !== "" &&
          response.status !== undefined &&
          response.status === 200) {
          var updateTktRes = response.data;
          updateTktRes.responsecode = response.status;
          updateTktRes.msg = "SUCCESS";
          res(updateTktRes);
        } else {
          this.getTripRes = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };
          //logger.info("updatePax response -- " + JSON.stringify(this.getTripRes));
          res(this.getTripRes);
        }
      }
    })
    .catch(error => {
      this.getTripRes = {
        status: "404",
        description: "updatePax not updated successful!!",
        updateStatus: false,
      };
      logger.info("updatePax response end--- " + JSON.stringify(this.getTripRes));
      logger.error("exception ocuured in FlightNodeService updatePax error is--- " + JSON.stringify(error));
      res(this.getTripRes);        
        
    });
}

exports.sendFlightSMS = function (req, res) {
  logger.info("sendFlightSM service start--- ");
  const smsURL=process.env.smsurl;  
  axios.post(smsURL, req.body).then(function(response) {
     //logger.info("sendFlightSM is --- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.data.status === 'success') {
          res(response);
        } else {
          this.getSmsRes = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };
          //logger.info("sendFlightSM response -- " + JSON.stringify(this.getTripRes));
          res(this.getSmsRes);
        }
      }
    })
    .catch(error => {
      this.getSmsRes = {
        status: "404",
        description: "sendFlightSM not updated successful!!",
        updateStatus: false,
      };
      logger.info("sendFlightSM response end--- " + JSON.stringify(this.getSmsRes));
      logger.error("exception ocuured in FlightNodeService sendFlightSM error is--- " + JSON.stringify(error));
      res(this.getTripRes);        
        
    });
}

exports.getSMSLink = function (req, res) {
  logger.info("getSMSLink service start --- ");
  const smsLink=process.env.smslinkurl;
  
  const smsObj={
    "feature" : "sms",
    "data" :{
     "$deeplink_path":createDomainpath(req)+"/account/trips/"+req,
     "utm_source":"xdays",
     "utm_medium":"sms"
    },
    "channel" : "xdays",
    "app_id" : "114631928556048739"
   };
  axios.post(smsLink, smsObj).then(function(response) {
     //logger.info("getSMSLink is --- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200){
          //logger.info("getSMSLink m view url:"+JSON.stringify(response.data.url));
          res(response);
        } else {
          this.getsmsRes = {
            status: "404",
            description: "getSMSLink is not available"
          };
          //logger.info("getSMSLink response -- " + JSON.stringify(this.getTripRes));
          res(this.getsmsRes);
        }
      }
    }).catch(error => {
      this.getsmsRes = {
        status: "404",
        description: "getSMSLink is not available"
      };
      logger.info("getSMSLink response end--- " + JSON.stringify(this.getTripRes));
      logger.error("exception ocuured in FlightNodeService getSMSLink error is--- " + JSON.stringify(error));
      res(this.getsmsRes);        
        
    });
}

exports.getSMSCancelLink = function (req, res) {
  logger.info("getSMSCancelLink service start --- ");
  const smsLink=process.env.smslinkurl;
  
  const smsObj={
    "feature" : "sms",
    "data" :{
     "$deeplink_path":createDomainpath(req)+"/account/trips/"+req+"/cancel",
     "utm_source":"xdays",
     "utm_medium":"sms"
    },
    "channel" : "xdays",
    "app_id" : "114631928556048739"
   };
  axios.post(smsLink, smsObj).then(function(response) {
     //logger.info("getSMSCancelLink is --- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200){
          //logger.info("getSMSCancelLink m url :"+JSON.stringify(response.data.url));
          res(response);
        } else {
          this.getsmsRes = {
            status: "404",
            description: "getSMSCancelLink is not available"
          };
          logger.info("getSMSCancelLink response -- " + JSON.stringify(this.getTripRes));
          res(this.getsmsRes);
        }
      }
    }).catch(error => {
      this.getsmsRes = {
        status: "404",
        description: "getSMSCancelLink is not available"
      };
      logger.info("getSMSCancelLink response end--- " + JSON.stringify(this.getTripRes));
      logger.error("exception ocuured in FlightNodeService getSMSCancelLink error is--- " + JSON.stringify(error));
      res(this.getsmsRes);        
        
    });
}


