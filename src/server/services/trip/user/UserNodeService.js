const express = require('express');

const bodyParser = require('body-parser');
const axios = require('axios');


const app = express();


const JSON = require('circular-json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));
axios.defaults.timeout = 1000*6;

const winston = require('winston');
const { sillyLogConfig } = require('../../../../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);


exports.fetchUserMailid = function (req, res) {
  logger.info("fetchUserMailid request is start--- ");
  //logger.info("fetchUserMailid request is --- " + JSON.stringify(req.body));
  
  const getUserFetchURL=process.env.userfetchurl;
 // logger.info("fetchUserMailid getUserURL is --- " +getUserFetchURL);
  axios.post(getUserFetchURL, req.body).then(function(response) {

    try{
     //logger.info("fetchUserMailid response is1--- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.status !== "" &&
          response.status !== undefined &&
          response.status === 200) {
          var userDetailsRes = response.data;
          userDetailsRes.responsecode = response.status;
          userDetailsRes.msg = "SUCCESS";
          res(userDetailsRes);
        } else {
          this.getUserRes = {
            status: "404",
            description: "User details empty"
          };
          //logger.info("fetchUserMailid response is2--- " + JSON.stringify(this.getUserRes));
          res(this.getUserRes);
        }
      } else {
        this.getUserRes = {
          status: "404",
          description: "User details empty"
        };
        //logger.info("fetchUserMailid response is3--- " + JSON.stringify(this.getUserRes));
        res(this.getUserRes);
      }

    }catch(error){
      this.getUserRes = {
        status: "404",
        description: "User details empty"
      };
      logger.info("fetchUserMailid response is4--- " + JSON.stringify(this.getUserRes));
      logger.error("exception ocuured in USerNodeService fetchUserMailid error is--- " + JSON.stringify(error));
      res(this.getUserRes)
    }
    }).catch((error) => {
      this.getUserRes = {
        status: "404",
        description: "User details empty"
      };
      logger.info("fetchUserMailid response is--- " + JSON.stringify(this.getUserRes));
      logger.error("exception ocuured in USerNodeService fetchUserMailid error is--- " + JSON.stringify(error));
    });
}
exports.fetchUserClassDtl = function (req, res) {
  logger.info("fetchUserClassDtl request is start--- ");
  //logger.info("fetchUserClassDtl request is --- " + JSON.stringify(req.body));
  const url=process.env.userClass
  const getUserFetchURL=url+req.body.username;
  //logger.info("fetchUserClassDtl getUserURL is --- " +getUserFetchURL);
  axios.get(getUserFetchURL).then(function(response) {

    try{
     //logger.info("fetchUserClassDtl response is1--- " + JSON.stringify(response));
      if (response !== undefined && response !== "") {
        if (response.status !== "" &&
          response.status !== undefined &&
          response.status === 200) {
          var userDetailsRes = response.data;
          userDetailsRes.responsecode = response.status;
          userDetailsRes.msg = "SUCCESS";
          res(userDetailsRes);
        } else {
          this.getUserRes = {
            status: "404",
            description: "User details empty"
          };
         // logger.info("fetchUserClassDtl response is2--- " + JSON.stringify(this.getUserRes));
          res(this.getUserRes);
        }
      } else {
        this.getUserRes = {
          status: "404",
          description: "User details empty"
        };
        //logger.info("fetchUserClassDtl response is3--- " + JSON.stringify(this.getUserRes));
        res(this.getUserRes);
      }

    }catch(error){
      this.getUserRes = {
        status: "404",
        description: "User details empty"
      };
      logger.info("fetchUserClassDtl response is4--- " + JSON.stringify(this.getUserRes));
      logger.error("exception ocuured in USerNodeService fetchUserClassDtl error is--- " + JSON.stringify(error));
      res(this.getUserRes)
    }
    })
    
}

