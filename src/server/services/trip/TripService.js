const express = require('express');

const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();

const JSON = require('circular-json');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
axios.defaults.timeout = 1000*6;

const winston = require('winston');
// App settings
const { sillyLogConfig } = require('../../../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);
const { createDomainpath } = require('../../utils/domainpath.js');
exports.getTripDetail = function (req, res) {
    //logger.info("getTripDetail request is--- " + JSON.stringify(req.body));
    const url=process.env.tripService;
    const getTripURL=url+req.body.tripId+"&refundRequired=true&historyRequired=true&paymentsRequired=true&apiVersion=V3";

    

    //logger.info("getTripURL--- " + getTripURL);
    axios
      .get(getTripURL)
      .then(function(response) {
        /* logger.info(
          "getTripData response is--- " + JSON.stringify(response)
        ); */
        if (typeof response !== 'undefined' ) {
          if (
            response.status !== "" &&
             typeof response.status !== 'undefined' &&
            response.status === 200
          ) {
            /* logger.info(
              "getTripData response1 is--- " + JSON.stringify(response)) */
            var tripResponse = response.data;
            tripResponse.responsecode = response.status;
            tripResponse.status=response.status;
            tripResponse.msg = "SUCCESS";
            res(tripResponse);
          } else {
            const getTripRes = {
              status: "404",
              msg: "TripNotFound"
            };
            res(getTripRes);
          }
        }
      })
      .catch(error => {
        const getTripRes =  {
          status: "404",
          msg: "TripNotFound"
        };
          logger.error(
          "getTripDetail response in catch block is--" +
            JSON.stringify(getTripRes)+error
        );
        logger.error(
          "exception getTripDetail response in catch block is--" +error
        );
        res(getTripRes);
      });
}

exports.getAirMasterData = function (req, res) {
  //logger.info("getAirMasterData request is--- " + JSON.stringify(req.body));
  const getTripMasterURL="http://ct-admin.cltp.com:9001/r3/ct-admin/service/json?fileType="+req.body.fileType;
  //logger.info("getAirMasterURL--- " + getTripMasterURL);
  axios
    .get(getTripMasterURL)
    .then(function(response) {getAirMasterData
      //logger.info("getAirMasterData response is--- " + JSON.stringify(response));
      if (response !== "" && response !== undefined) {
        if (
          response.status !== "" &&
          response.status !== undefined &&
          response.status === 200
        ) {
          var tripResponse = response.data;
          tripResponse.responsecode = response.status;
          tripResponse.msg = "SUCCESS";
          res(tripResponse);
        } else {
          const getTripRes = {
            status: "404",
            msg: "masterNotFound"
          };
          res(getTripRes);
        }
      }
    })
    .catch(error => {
      const getTripRes = {
        status: "404",
        msg: "masterNotFound"
      };
        logger.error(
        "getAirMasterData response in catch block is--" +
          JSON.stringify(getTripRes)
      );
      res(getTripRes);
    });
  }
  exports.getFlag = function (req, res) {
    //logger.info("getAirMasterData request is--- " + JSON.stringify(req.body));
    const getTripFlagURL="https://qa2.cltpstatic.com/images/flags/mini"+req.body.country;
    //logger.info("getFlag--- " + getTripFlagURL);
    axios
      .get(getTripFlagURL)
      .then(function(response) {getFlag
        //logger.info("getFlag response is--- " + JSON.stringify(response) );
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var tripResponse = response.data;
            tripResponse.responsecode = response.status;
            tripResponse.msg = "SUCCESS";
            res(tripResponse);
          } else {
            const getTripRes = {
              status: "404",
              msg: "masterNotFound"
            };
            res(getTripRes);
          }
        }
      })
      .catch(error => {
        const getTripRes ={
          status: "404",
          msg: "masterNotFound"
        };
          logger.error(
          "getFlag response in catch block is--" +
            JSON.stringify(getTripRes)
        );
        res(getTripRes);
      });
    }

    exports.getAirPortMasterData = function (req, res) {
      logger.info("getTripMasterDetail start is--- " );
      const getTripMasterURL="http://ct-admin.cltp.com:9001/r3/ct-admin/service/json?fileType="+req.body.fileType;
     // logger.info("getAirMasterURL--- " + getTripMasterURL);
      axios
        .get(getTripMasterURL)
        .then(function(response) {
          //logger.info("getAirMasterData response is--- " + JSON.stringify(response));
          if (response !== "" && response !== undefined) {
            if (
              response.status !== "" &&
              response.status !== undefined &&
              response.status === 200
            ) {
              var tripResponse = response.data;
              tripResponse.responsecode = response.status;
              tripResponse.msg = "SUCCESS";
              res(tripResponse);
            } else {
              const getTripRes = {
                status: "404",
                msg: "masterNotFound"
              };
              res(getTripRes);
            }
          }
        })
        .catch(error => {
          const getTripRes =  {
            status: "404",
            msg: "TripNotFound"
          };
            logger.error(
            "getAirPortMasterData response in catch block is--" +error);
          res(getTripRes);
        });
}

exports.getWalletPromotions = function (req, res) {
  logger.info("getWalletPromotions service start1 ");
  const promotionURL=process.env.promotionsurl;
  
  const promourl=promotionURL+req.body.tripRefNumber;
  //logger.info("getWalletPromotions service start2 "+promotionURL);
  axios.get(promourl).then(function(response) {
     //logger.info("getWalletPromotions is --- " + JSON.stringify(response.data));
      if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200) {
          res(response);
        } else {
          this.getSmsRes = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };
          logger.info("getWalletPromotions response -- " + JSON.stringify(this.getTripRes));
          res(this.getSmsRes);
        }
      }
    }).catch(error => {
      this.getSmsRes = {
        status: "404",
        description: "getWalletPromotions not updated successful!!",
        updateStatus: false,
      };
      logger.info("getWalletPromotions response end--- " + JSON.stringify(this.getSmsRes));
      logger.error("exception ocuured in TripService getWalletPromotions error is--- " + JSON.stringify(error));
      res(this.getTripRes);        
        
    });
    
}

exports.closeTxn = function (req, cb) {
  logger.info("closeTxn service start1 ");
  const domainpath=createDomainpath(req);
  var ctKey = get_cookies(req)['ct-auth'];
  var ctAuthPref = get_cookies(req)['ct-auth-preferences'];
  var userid = get_cookies(req)['userid'];
  var usermisc = get_cookies(req)['usermisc'];

  const closeTxnUrl=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/close_txn";
  //logger.info("closeTxn service start2 "+closeTxnUrl);
 
  let cookies = "ct-auth="+ctKey+";ct-auth-preferences="+ctAuthPref+";userid="+userid+";usermisc="+usermisc;
  //logger.info("closeTxn service start2 cookies  "+cookies);

  axios.get(closeTxnUrl,{
      headers: {
        "Access-Control-Allow-Origin": "*",
        AUTH_TYPE: "COOKIE",
        Cookie: cookies
       
      }
    })
    .then(function(response) {
      //const jsondata = JSON.stringify(response);
	 //logger.info('close txn Res: ----'+jsondata);
	   if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200) {
		//logger.info('close txn RESSSS data: ----'+JSON.stringify(response.data));
          //cb(response.data);
          this.res = {
            status: "SUCCESS",
            
          };
          cb(res);
        } else {
          const response = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };
          logger.info("close txn response -- " + JSON.stringify(response));
          cab(response);
        }
      }
      
      
    })
    .catch(function (error) {
	logger.error("ERROR===="+error);
     const erroRes = {
          status: '404',
          msg:'exception',
 		 updateStatus: false,
      }
      logger.error("Error response in close txn : " + JSON.stringify(erroRes));
      logger.error("Error in Tripervice close txn : " + JSON.stringify(error));
      cb(erroRes);
  });

}

exports.offlineRefund = function (req, cb) {
  logger.info("offlineRefund service start ");
  const domainpath=createDomainpath(req);
  var ctKey = get_cookies(req)['ct-auth'];
  var ctAuthPref = get_cookies(req)['ct-auth-preferences'];
  var userid = get_cookies(req)['userid'];
  var usermisc = get_cookies(req)['usermisc'];
  
	const priceUpdate=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/update_pricing";
  //logger.info("offlineRefund service URL "+priceUpdate);
 
  let cookies = "ct-auth="+ctKey+";ct-auth-preferences="+ctAuthPref+";userid="+userid+";usermisc="+usermisc;
  //logger.info("offlineRefund service start2 cookies  "+cookies);
  
  let newJsone = req.body;
  delete newJsone.tripRefNumber;
 
  const encodeForm = (data) => {
	  return Object.keys(data)
	      .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
	      .join('&');
	}

	//logger.info("offlineRefund service before  "+encodeForm(newJsone));
 	axios.post(priceUpdate, encodeForm(newJsone), {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "multipart/form-data",
          Accept: " */*",
          Cookie: cookies
        }
      })
      .then(function(response) {
        //const jsondata = JSON.stringify(response);
 		//logger.info('offlineRefund RESSSS: ----'+jsondata);
		if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200) {
		logger.info('close txn RESSSS data: ----'+JSON.stringify(response.data));
          cb(response.data);
        } else {
          const response = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };
          logger.info("close txn response -- " + JSON.stringify(response));
          cab(response);
        }
      }
      cb(response);
      })
      .catch(function (error) {
        logger.error(error);
        var resp = '';
        resp = {
            status: '404',
            msg:'Resource Not Found'
        }
        logger.error("Error in offlineRefund User : " + resp);
        cb(resp);
    });



}

var get_cookies = function(request) {
var cookies = {};
request.headers && request.headers.cookie.split(';').forEach(function(cookie) {
var parts = cookie.match(/(.*?)=(.*)$/)
cookies[ parts[1].trim() ] = (parts[2] || '').trim();
});
return cookies;
};


exports.saveTagMaster = function (req, res) {
  logger.info("saveTagMaster service start  ");
  const tagUrl=process.env.tagmasterurl;
  
  axios.post(tagUrl,req.body).then(function(response){
  //logger.info("saveTagMaster Response is+++: " + JSON.stringify(response.data));
      if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200) {
          res(response);
        } else {
          this.tag = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };
          logger.info("saveTagMaster response -- " + JSON.stringify(this.tag));
          res(this.tag);
        }
      }
    }).catch(error => {
      this.tag = {
        status: "404",
        description: "saveTagMaster not updated successful!!",
        updateStatus: false,
      };
      logger.info("saveTagMaster response end--- " + JSON.stringify(this.tag));
      logger.error("exception ocuured in TripService saveTagMaster error is--- " + JSON.stringify(error));
      res(this.tag);        
        
    });

}


exports.getPaymentLink = function (req, res) {
  logger.info("getPaymentLink request is start--- " );
  const url =process.env.getCtUrl
  const pmnturl=process.env.getCtUrl+req.body.tripRefNumber;
 // logger.info("pmnturl--- " + pmnturl);
  axios
    .get(pmnturl)
    .then(function(response) {
      //logger.info("getPaymentLink response is--- " + JSON.stringify(response));
      if (response !== "" && response !== undefined) {
        if (
          response.status !== "" &&
          response.status !== undefined &&
          response.status === 200
        ) {
          var tripResponse = response.data;
          tripResponse.responsecode = response.status;
          tripResponse.msg = "SUCCESS";
          res(tripResponse);
        } else {
          const getTripRes = {
            status: "404",
            msg: "masterNotFound"
          };
          res(getTripRes);
        }
      }
    })
    .catch(error => {
      const getTripRes =  {
        status: "404",
        msg: "linkNotFound"
      };
        logger.error(
        "getPaymentLink response in catch block is--" +
          JSON.stringify(getTripRes)
      );
      res(getTripRes);
    });
  }
 

  exports.getWalletCurrencies = function (req, res) {
	logger.info("getWalletCurrencies service start ");
    const walletUrl=process.env.usrcurrencyurl;
    const url=walletUrl+req.body.bookedUsrId+"/getWallet";
    
    axios.get(url).then(function(response){
   // logger.info("getWalletCurrencies Response is+++: " + JSON.stringify(response.data));
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            res(response);
          } else {
            this.wallet = {
              status: "404",
              description: "Not updated!!",
              updateStatus: false,
            };
            logger.info("getWalletCurrencies response -- " + JSON.stringify(this.tag));
            res(this.wallet);
          }
        }
      }).catch(error => {
        this.wallet = {
          status: "404",
          description: "getWalletCurrencies not updated successful!!",
          updateStatus: false,
        };
        logger.info("getWalletCurrencies response end--- " + JSON.stringify(this.wallet));
        logger.error("exception ocuured in TripService getWalletCurrencies error is--- " + JSON.stringify(error));
        res(this.wallet);        
          
      });
  
  }


  exports.fetchWalletNumbers = function (req, res) {
	logger.info("getWalletCurrencies service start  ");
    const walletUrl=process.env.fetchwalletnumurl;
    const url=walletUrl+"userId="+req.body.bookedUsrId+"&currency="+req.body.currency;
    
    axios.get(url).then(function(response){
    //logger.info("getWalletCurrencies Response is+++: " + JSON.stringify(response.data));
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            res(response);
          } else {
            this.wallet = {
              status: "404",
              description: "Not updated!!",
              updateStatus: false,
            };
            logger.info("getWalletCurrencies response -- " + JSON.stringify(this.tag));
            res(this.wallet);
          }
        }
      }).catch(error => {
        this.wallet = {
          status: "404",
          description: "getWalletCurrencies not updated successful!!",
          updateStatus: false,
        };
        logger.info("getWalletCurrencies response end--- " + JSON.stringify(this.wallet));
        logger.error("exception ocuured in TripService getWalletCurrencies error is--- " + JSON.stringify(error));
        res(this.wallet);        
          
      });
  
  }


  exports.fetchChmmHotelStatus = function (req, res) {
	logger.info("fetchChmmHotelStatus start " );
    const hotelUrl=process.env.hotelstatusurl;
    const url=hotelUrl+req.body.tripRefNum;
    //logger.info("fetchChmmHotelStatus service start URL "+url);
    axios.get(url).then(function(response){
    
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            res(response);
          } else {
            this.hotel = {
              status: "404",
              description: "Not updated!!",
              updateStatus: false,
            };
            logger.info("fetchChmmHotelStatus response -- " + JSON.stringify(this.hotel));
            res(this.hotel);
          }
        }
      }).catch(error => {
        this.hotel = {
          status: "404",
          description: "fetchChmmHotelStatus not updated successful!!",
          updateStatus: false,
        };
        logger.info("fetchChmmHotelStatus response end--- " + JSON.stringify(this.hotel));
        logger.error("exception ocuured in TripService fetchChmmHotelStatus error is--- " + JSON.stringify(error));
        res(this.hotel);        
          
      });
  
  }


  exports.getFltSuppService = function (req, res) {
	logger.info("getFltSuppService service start  ");
    const supUrl=process.env.fltsupserviceurl;
    
    axios.get(supUrl).then(function(response){
    //logger.info("getFltSuppService Response " + JSON.stringify(response));
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            res(response);
          } else {
            this.hotel = {
              status: "404",
              description: "Not updated!!",
              updateStatus: false,
            };
           // logger.info("getFltSuppService response -- " + JSON.stringify(this.hotel));
            res(this.hotel);
          }
        }
      }).catch(error => {
        this.hotel = {
          status: "404",
          description: "getFltSuppService not updated successful!!",
          updateStatus: false,
        };
        //logger.info("getFltSuppService response end--- " + JSON.stringify(this.hotel));
        logger.error("exception ocuured in TripService getFltSuppService error is--- " + JSON.stringify(error));
        res(this.hotel);        
          
      });
  
  }

  exports.getProcessSyncGDS = function (req, res) {
 logger.info("getProcessSyncGDS service start  ");
  const syncUrl=process.env.syncgdsurl 

  var ctKey = get_cookies(req)['ct-auth'];
  //logger.info("ctKey " + JSON.stringify(ctKey));
  var ctAuthPref = get_cookies(req)['ct-auth-preferences'];
  //logger.info("ctAuthPref " + JSON.stringify(ctAuthPref));
  var userid = get_cookies(req)['userid'];
 // logger.info("userid" + JSON.stringify(userid));
  var usermisc = get_cookies(req)['usermisc'];
 // logger.info("usermisc " + JSON.stringify(usermisc));

  let cookies = "ct-auth="+ctKey+";ct-auth-preferences="+ctAuthPref+";userid="+userid+";usermisc="+usermisc;

 // logger.info("cookies " + JSON.stringify(cookies));


    const url=syncUrl+req.body.tripRefNum+"/sync_pnr"
    //logger.info("getProcessSyncGDS service start URL "+url);   


    axios.get(url,{
      headers: {
        "Access-Control-Allow-Origin": "*",        
        Accept: "*/*",
        Cookie: cookies
        
      }
    }).then(function(response){
   // logger.info("getProcessSyncGDS Response " + JSON.stringify(response.data));
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            res(response);
          } else {
            this.gds = {
              status: "404",
              description: "Not updated!!",
              updateStatus: false,
            };
            //logger.info("getProcessSyncGDS response -- " + JSON.stringify(this.gds));
            res(this.gds);
          }
        }
      }).catch(error => {
        this.gds = {
          status: "404",
          description: "getProcessSyncGDS not updated successful!!",
          updateStatus: false,
        };
        //logger.info("getProcessSyncGDS response end--- " + JSON.stringify(this.gds));
        logger.error("exception ocuured in TripService getProcessSyncGDS error is--- " + JSON.stringify(error));
        res(this.gds);        
          
      });
  
  }

  exports.processRollbackAmend = function (req, res) {
	logger.info("processRollbackAmend service start  ");
    const syncUrl=process.env.syncgdsurl;
    const url=syncUrl+req.body.tripRefNum+"/rollback_amendment"
    //logger.info("processRollbackAmend service start URL "+url);
    axios.get(url).then(function(response){
    //logger.info("processRollbackAmend Response " + JSON.stringify(response));
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            res(response);
          } else {
            this.amend = {
              status: "404",
              description: "Not updated!!",
              updateStatus: false,
            };
            //logger.info("processRollbackAmend fail response -- " + JSON.stringify(this.amend));
            res(this.amend);
          }
        }
      }).catch(error => {
        this.amend = {
          status: "404",
          description: "processRollbackAmend not updated successful!!",
          updateStatus: false,
        };
        logger.info("processRollbackAmend catch response end--- " + JSON.stringify(this.amend));
        logger.error("exception ocuured in TripService processRollbackAmend error is--- " + JSON.stringify(error));
        res(this.amend);        
          
      });
  
  }
  exports.getCompanyNameForCashPmnt = function (req, res) {
	logger.info("getCompanyNameForCashPmnt service start  ");
    const companyNameFromId=process.env.companyNameFromId;
    const url=companyNameFromId+req.body.compId+"?caller=air%20"
    
    axios.get(url,{ headers: { Accept: 'text/json' } }).then(function(response){
   // logger.info("getCompanyNameForCashPmnt Response " + JSON.stringify(response));
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            //let res = JSON.parse(response)
            res(response);
          } else {
            this.res = {
              status: "404",
              description: "Not updated!!",
              updateStatus: false,
            };
            //logger.info("getCompanyNameForCashPmnt response -- " + JSON.stringify(this.res));
            res(this.res);
          }
        }
      }).catch(error => {
        this.res = {
          status: "404",
          description: "getCompanyNameForCashPmnt not updated successful!!",
          updateStatus: false,
        };
        logger.info("processRollbackAmend response end--- " + JSON.stringify(this.res));
        logger.error("exception ocuured in TripService getCompanyNameForCashPmnt error is--- " + JSON.stringify(error));
        res(this.res);        
          
      });
  
  }
  exports.getExpressCheckoutDtl = function (req, res) {
    const expressUrl=process.env.expressPayBnkDtl;
    const url=expressUrl+req.body.userId+"/"+req.body.currency
    logger.info("getExpressCheckoutDtl service start URL "+url);
    axios.get(url).then(function(response){
    logger.info("getExpressCheckoutDtl Response " + JSON.stringify(response));
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            //let res = JSON.parse(response)
            res(response);
          } else {
            this.res = {
              status: "404",
              description: "Not updated!!",
              updateStatus: false,
            };
            //logger.info("getExpressCheckoutDtl response -- " + JSON.stringify(this.res));
            res(this.res);
          }
        }
      }).catch(error => {
        this.res = {
          status: "404",
          description: "getExpressCheckoutDtl not updated successful!!",
          updateStatus: false,
        };
        logger.info("processRollbackAmend response end--- " + JSON.stringify(this.res));
        logger.error("exception ocuured in TripService getCompanyNameForCashPmnt error is--- " + JSON.stringify(error));
        res(this.res);        
          
      });
  
  }


exports.getBookingPolicy = function (req, res) {
	logger.info("getBookingPolicy service start  ");
    const rateRules=process.env.raterules;
    const url=rateRules+req.body.tripId
   
    axios.get(url).then(function(response){
    //logger.info("getBookingPolicy Response " + JSON.stringify(response));
        if (response !== undefined && response !== "") {
          if (response.data !== "" &&
          response.data !== undefined &&
          response.status === 200) {
            res(response);
          } else {
            this.gds = {
              status: "404",
              description: "Unable to find booking policies",
              updateStatus: false,
            };
           // logger.info("getBookingPolicy response -- " + JSON.stringify(this.gds));
            res(this.gds);
          }
        }
      }).catch(error => {
        this.gds = {
          status: "404",
          description: "Unable to find booking policies",
          updateStatus: false,
        };
        logger.info("getBookingPolicy response end--- " + JSON.stringify(this.gds));
        logger.error("exception ocuured in TripService getBookingPolicy error is--- " + JSON.stringify(error));
        res(this.gds);        
          
      });
  
  }
  

