const express = require('express');

const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();

const JSON = require('circular-json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));
axios.defaults.timeout = 1000*6;

const winston = require('winston');
const { sillyLogConfig } = require('../../../../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);
const { createDomainpath } = require('../../../utils/domainpath.js');

exports.sendFlightDetailEmail = function (req, res) {
  logger.info("sendFlightDetailEmail request is start--- ");
  //logger.info("sendFlightDetailEmail request is --- " + JSON.stringify(req.body));
  
  const emailApiUrl=process.env.emailApiUrl;
  //logger.info("u getTripURL is --- " +emailApiUrl);
  axios.post(emailApiUrl, req.body).then(function(response) {
     //logger.info("sendFlightDetailEmail SUCCESS RESPONSE--- " + JSON.stringify(response.data));
     
      if (typeof response !== 'undefined' ) {
        let data=response.data;
        //logger.info("response data=="+JSON.stringify(response))
        if (
        typeof data.status !== 'undefined' &&
        data.status === "ACCEPTED") {
          
          var emailResponse=response.data
          emailResponse.responsecode = 200;
          emailResponse.msg = "SUCCESS";
          res(emailResponse);
          //logger.info("response data==2"+JSON.stringify(data))
        } else {
          this.getTripRes = {
            status: response.status,
            description: response.data
            
          };
          //logger.info("sendFlightDetailEmail response Failed--- " + JSON.stringify(this.getTripRes));
          res(this.getTripRes);
        }
      }
    })
    .catch(error => {

      this.getTripRes = {
        status: "404",
        description: JSON.stringify(error)
      };
      logger.info("sendFlightDetailEmail response error--- " + error);      
      res(this.getTripRes);        
        
    });
}

exports.sendEmail = function (req, cb) {
  logger.info("sendEmail service start1 ");
  const domainpath=createDomainpath(req);
  var ctKey = get_cookies(req)['ct-auth'];
  var ctAuthPref = get_cookies(req)['ct-auth-preferences'];
  var userid = get_cookies(req)['userid'];
  var usermisc = get_cookies(req)['usermisc'];
  var url = '';
	
   if(req.body.type =='FLIGHT_RECEIPT'){
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/email_receipt?"+"email_receipt="+req.body.to+"&ical=on";
	}else if(req.body.type =='FLIGHT_DTLS'){
		let ical = '';
		if(req.body.ical){
			ical = '&ical=on';
		}
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/email?"+"email="+req.body.to+ical;
	}else if(req.body.type =='FLIGHT_SALES_RECEIPT'){
		let ical = '';
		if(req.body.ical){
			ical = '&ical=on';
		}
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/email_sale_invoice?"+"email_sale_invoice="+req.body.to+ical;
	}else if(req.body.type =='FLIGHT_TICKET'){
		url=domainpath+"/ticket/email/"+req.body.tripRefNumber+"?"+"email_address="+req.body.to+"&last_name="+req.body.data;
	}else if(req.body.type =='FLIGHT_VAT_INVOICE'){
		let ical = '';
		if(req.body.ical){
			ical = '&ical=on';
		}
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/email_invoice?"+"email_invoice="+req.body.to+ical;
	}else if(req.body.type =='FLIGHT_REFUND_INVOICE'){
		let ical = '';
		if(req.body.ical){
			ical = '&ical=on';
		}
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/email_invoice?"+"email_invoice="+req.body.to+ical;
	}else if(req.body.type =='BOOK_STEP_SCREENSHOT'){
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/bs_email?"+"email_bs="+req.body.to+"&ical=on";
	}else if(req.body.type =='HOTEL_DTLS'){
		let ical = '';
		if(req.body.ical){
			ical = '&ical=on';
		}
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/email?"+"email="+req.body.to+ical;
	}else if(req.body.type =='HOTEL_VOUCHER'){
		url=domainpath+"/voucher/email/"+req.body.tripRefNumber+"?"+"email_address="+req.body.to+"&last_name="+req.body.data;
	}else if(req.body.type =='HOTEL_SALES_INVOICE'){
		let ical = '';
		if(req.body.ical){
			ical = '&ical=on';
		}
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/email_sale_invoice?"+"email_sale_invoice="+req.body.to+ical;
  }else if(req.body.type =='FPH_EMAIL_INVOICE'){
		let ical = '';
		if(req.body.ical){
			ical = '&ical=on';
		}
		url=domainpath+"/hq/trips/"+req.body.tripRefNumber+"/email_invoice?"+"email_invoice="+req.body.to+ical;
	}
  else{
		return response = {
	            status: "404",
	            description: "Resource not found",
	            updateStatus: false,
	          };
	}
 

  //logger.info("sendEmail service start2 "+url);

 
  let cookies = "ct-auth="+ctKey+";ct-auth-preferences="+ctAuthPref+";userid="+userid+";usermisc="+usermisc;
  //logger.info("sendEmail service start2 cookies  "+cookies);

  axios.get(url,{
      headers: {
        "Access-Control-Allow-Origin": "*",
        AUTH_TYPE: "COOKIE",
        Cookie: cookies
       
      }
    })
    .then(function(response) {
      //const jsondata = JSON.stringify(response);
	 //logger.info('close txn Res: ----'+jsondata);
	   if (response !== undefined && response !== "") {
        if (response.data !== "" &&
        response.data !== undefined &&
        response.status === 200) {
		//logger.info('sendEmail txn RESSSS data: ----'+JSON.stringify(response.data));
          cb(response.data);
        } else {
          const response = {
            status: "404",
            description: "Not updated!!",
            updateStatus: false,
          };
          logger.info("sendEmail txn response -- " + JSON.stringify(response));
          cab(response);
        }
      }
      
      
    })
    .catch(function (error) {
	logger.error("sendEmail ERROR===="+error);
     const erroRes = {
          status: '404',
          msg:error,
 		 updateStatus: false,
      }
      logger.error("Error in sendEmail : " + JSON.stringify(erroRes));
      cb(erroRes);
  });

}

var get_cookies = function(request) {
var cookies = {};
request.headers && request.headers.cookie.split(';').forEach(function(cookie) {
var parts = cookie.match(/(.*?)=(.*)$/)
cookies[ parts[1].trim() ] = (parts[2] || '').trim();
});
return cookies;
};

