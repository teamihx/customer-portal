const express = require('express');

const bodyParser = require('body-parser');
const axios = require('axios');


const app = express();


const JSON = require('circular-json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));
axios.defaults.timeout = 1000*6;
const TripService  = require('../../../server/services/trip/TripService')
const NoteNodeService  = require('../../../server/services/trip/note/NoteNodeService')
const cornTaskctrl  = require('../../../server/controllers/cornTask-ctrl')
const winston = require('winston');
const { sillyLogConfig } = require('../../../helper/logger').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);
if(process.env.NODE_ENV==='development'){
  require('dotenv').config();

}else if(process.env.NODE_ENV==='preprod'){
  require('dotenv').config({path:__dirname+'/./../../.env.preproduction'});

}else if(process.env.NODE_ENV==='production'){
  require('dotenv').config({path:__dirname+'/./../../.env.production'});

}
var url = require('url');

function fullUrl(req) {
  return url.format({
    protocol: req.protocol,
    host: req.get('host'),
    pathname: req.originalUrl
  });
}

exports.updateTxnStatuses=()=>{
  logger.info("updateTxnStatus txnURL is start--- " );
    const txnURL="/api/getPaymentTxns";
    
     axios.get(txnURL).then(res=>{
         let response=res.data;
        for(let txn of response.data){
            
                let trip_ref=txn.trip_ref
            
            if(Math.abs(new Date(txn.createdAt)-new Date())>=30*60000){
                
              //logger.info('TripId=======')
              const tripUrl=process.env.tripService
              axios.get(tripUrl+trip_ref+"&historyRequried=true&refundRequired=true&paymentsRequired=true&apiVersion=V3").then(res=>{
                //logger.info('Rsponse trip status=='+res.status)
              if(typeof res.status!=='undefined'&& res.status===200){
                let tripTxns = res.data.txns;
                for(let tripTxn of tripTxns){
                    if(tripTxn.id===txn.txn_id){
                        tripTxn.status='C';
                        txnArr=[tripTxn]
                        //const txnReq=
                        const txnURL=process.env.updateairtkturl;
                      // logger.info("createNewTransaction txnURL is --- " +txnURL);
                       axios.put(txnURL, {txns:[...txnArr]}).then(res=>{
                           if(res.status===200){
                           // const delTxnURL=process.env.contextpath+"/api/deletePaymentTxn/"+trip_ref
                            //logger.info("delTxnURL URL is --- " +delTxnURL);
                               axios.delete(delTxnURL).then(res=>{
                                if(res.status===200){
                                    //logger.info("trip with referrece"+trip_ref+"deleted")
                                }
                               }
                                
                                )
                           }
                           //logger.info("update txn ==="+JSON.stringify(res.data))
                       })
                        //logger.info("request is"+json.stringify(txnReq));
                    }

                }
              }
                  
              }).catch(error => {
                //const getTripRes = "";
                this.getTripRes = {
                  status: "404",
                  msg: "TripNotFound"
                };
                  logger.error(
                  "updateTxnStatuses response in catch block is--" +
                    JSON.stringify(this.getTripRes)
                );
                
              });
            }
           
        }
    }
        )
  
}

exports.insertPaymentTxn = function (req, res) {
  logger.info("insertPaymentTxn request is start--- ");
  logger.info("insertPaymentTxn request is --- " + JSON.stringify(req.body));
  

  cornTaskctrl.createcornTask(req,res)
  
}
exports.updateTxnStatus =   ()=> {
    logger.info("getPaymentTxns request is start--- ");
     this.fetchPaymentTxns().then(res=>{
      if(Array.isArray(res)){
        for(let txn of res){
          let trip_ref=txn.trip_ref;
          if(Math.abs(new Date(txn.createdAt)-new Date())>=30*60000){
            const  tripUrl=process.env.tripService
            axios.get(tripUrl+trip_ref+"&historyRequried=true&refundRequired=true&paymentsRequired=true&apiVersion=V3").then(res=>{
              //logger.info('Rsponse trip status=='+res.status)
            if(typeof res.status!=='undefined'&& res.status===200){
              let tripTxns = res.data.txns;
              for(let tripTxn of tripTxns){
                  if(tripTxn.id===txn.txn_id){
                      tripTxn.status='C';
                      txnArr=[tripTxn]
                      //const txnReq=
                      const txnURL=process.env.updateairtkturl;
                     //logger.info("createNewTransaction txnURL is --- " +txnURL);
                     axios.put(txnURL, {txns:[...txnArr]}).then(res=>{
                         if(res.status===200){
                         
                             cornTaskctrl.deletecornTask(trip_ref).then(res=>{
                              if(res === "trip with ref"+trip_ref+"deleted"){
                                  logger.info("trip with referrece"+trip_ref+"deleted")
                              }
                             }
                              
                              )
                         }
                        
                     })
                      //logger.info("request is"+json.stringify(txnReq));
                  }

              }
            }
                
            }).catch(error=>{logger.info("error in updateTxnStatus"+error)})
          }
        }
      }else{
        logger.info("Transactions not found")
      }
      
    })
    
  }
exports.fetchPaymentTxns=()=>{
  //console.log('encrypted_url'+payRes.encrypted_url)
  return new Promise(function(resolve,reject){
    //let res={}
  cornTaskctrl.getcornTasks().then(res=>{if(res.length>0){
    resolve(res)
  }else{
    reject("txn not available")
  }
  
  })
  
  
}
  );
  
      
  
  
};