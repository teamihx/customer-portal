const express = require('express');

const bodyParser = require('body-parser');
const axios = require('axios');


const app = express();
const JSON = require('circular-json');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
axios.defaults.timeout = 1000*6;

const winston = require('winston');
// App settings
const { sillyLogConfig } = require('../../../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);

exports.getGstCompanySearch = function (req, res) {
    logger.info("getGstCompanyData start-- " );
    const gstURL=process.env.gstcompanysearch;
    //const environMent=process.env.environment;
    //logger.info('gstcompanySearchURL-----'+gstURL);
    //logger.info('node environMent is-----'+environMent);

    axios
      .get(
        gstURL +
          req.body.domain_name,
        {
          headers: {
            "Content-Type": "application/json"
          }
          
        }
      )
      .then(function(response) {
        //logger.info("getGstCompanyData response is--- " + JSON.stringify(response));         
        
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var gstResponse = response.data;
            gstResponse.responsecode = response.status;
            gstResponse.msg = "SUCCESS";
            res(gstResponse);
          } else {
            const gstsearchRes =  {
              status: "404",
              msg: "CompanyNotFound"
            };
            logger.info("getGstCompanyData response is1--" +JSON.stringify(gstsearchRes)
            );
            res(gstsearchRes);
          }
        }else{
          const gstsearchRes =  {
              status: "404",
              msg: "CompanyNotFound"
            };
            logger.info("getGstCompanyData response is2--" +JSON.stringify(gstsearchRes));
            res(gstsearchRes);

        }
      })
      .catch(error => {
        logger.error('error in getGstCompanyData is--- '+JSON.stringify(error.response));
        const gstsearchRes =  {
          status: "404",
          msg: "CompanyNotFound"
        };
        logger.info(
          "getGstCompanyData response in catch block is--" +
            JSON.stringify(gstsearchRes)
        );
        res(gstsearchRes);
      });
}

exports.editGstCompany = function (req, res) {
    logger.info("editGstCompanyData start is--- " );
    const editGstURL=process.env.editgst;
    //logger.info("editGstURL is--- " + editGstURL);
    axios
      .post(editGstURL, req.body)
      .then(function(response) {
        //logger.info("editGstCompanyData response is--- " + JSON.stringify(response));
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var gstEditResponse = response.data;
            gstEditResponse.responsecode = response.status;
            gstEditResponse.msg = "SUCCESS";
            res(gstEditResponse);
          } else {
            const gsteditRes = {
              status: "404",
              msg: "CompanyNotFound"
            };
            logger.info(
              "editGstCompanyData response is1--- " + JSON.stringify(gsteditRes)
            );
            res(gsteditRes);
          }
        }else {
          const gsteditRes =  {
            status: "404",
            msg: "CompanyNotFound"
          };
          logger.info(
            "editGstCompanyData response is2--- " + JSON.stringify(gsteditRes)
          );
          res(gsteditRes);
        }
      })
      .catch(error => {
        const gsteditRes = {
          status: "404",
          msg: "CompanyNotFound"
        };
          logger.info(
          "editGstCompanyData response in catch block is--" +
            JSON.stringify(gsteditRes)
        );
        logger.error(
          "error in editGstCompanyData is--" +
            JSON.stringify(error)
        );
       
        res(gsteditRes);
      });
}

exports.deleteGstDetails = function (req, res) {
    logger.info("deleteGstCompanyData start is--- " );
    //axios.delete("http://172.17.8.45:9001/companies/gst",req.body)--not working
    const deleGstURL=process.env.deletegst;
    //logger.info("deleGstURL is--- " + deleGstURL);
    axios({
      method: "DELETE",
      url: deleGstURL,
      data: req.body
    })
      .then(function(response) {
        //logger.info("deleteGstCompanyData response is--- " + JSON.stringify(response));
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var gstDelResponse = response.data;
            gstDelResponse.msg = "SUCCESS";
            res(gstDelResponse);
          } else {
            const gstdelRes = {
              status: "404",
              msg: "CompanyNotFound"
            };
            logger.info(
              "deleteGstCompanyData response is1--- " + JSON.stringify(gstdelRes)
            );
            res(gstdelRes);
          }
        }else {
          const gstdelRes = {
            status: "404",
            msg: "CompanyNotFound"
          };
          logger.info(
            "deleteGstCompanyData response is2--- " + JSON.stringify(gstdelRes)
          );
          res(gstdelRes);
        }
      })
      .catch(err => {
        const gstdelRes = {
          status: "404",
          msg: "CompanyNotFound"
        };
          logger.info(
          "deleteGstCompanyData response in catch block is--" +
            JSON.stringify(gstdelRes)
        );

        logger.error(
          "error in deleteGstCompanyData  is--" +
            JSON.stringify(err)
        );
        res(gstdelRes);
      });
}

exports.addGstCompanyData = function (req, res) {
    logger.info("addGstCompanyData start is--- " );
    const addGstURL=process.env.adddgst;
    //logger.info("addGstURL--- " + addGstURL);
    axios
      .post(addGstURL, req.body)
      .then(function(response) {
        //logger.info( "addGstCompanyData response is--- " + JSON.stringify(response));
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var gstAddResponse = response.data;
            gstAddResponse.responsecode = response.status;
            gstAddResponse.msg = "SUCCESS";
            res(gstAddResponse);
          } else {
            const gstaddRes =  {
              status: "404",
              msg: "CompanyNotFound"
            };
            logger.info(
              "addGstCompanyData response is1--- " + JSON.stringify(gstaddRes)
            );
            res(gstaddRes);
          }
        }else {
          const gstaddRes =  {
            status: "404",
            msg: "CompanyNotFound"
          };
          logger.info(
            "addGstCompanyData response is2--- " + JSON.stringify(gstaddRes)
          );
          res(gstaddRes);
        }
      })
      .catch(error => {
        const gstaddRes = {
          status: "404",
          msg: "CompanyNotFound"
        };
          logger.info(
          "addGstCompanyData response in catch block is--" +
            JSON.stringify(gstaddRes)
        );

        logger.error(
          "error in addGstCompanyData is--" +
            JSON.stringify(error)
        );
        res(this.gstaddRes);
      });
}