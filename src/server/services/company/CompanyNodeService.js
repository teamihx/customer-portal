const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const axios = require('axios');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();
const JSON = require('circular-json');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
axios.defaults.timeout = 1000*6;

const winston = require('winston');
// App settings
const { sillyLogConfig } = require('../../../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);

exports.getCompanyDetails = function (req, cb) {
    const domain = req.body.domain_name;
    logger.info('CompanyNodeService.js getCompanyDetails api START for Domain : ' + domain);
    const comConfigSearchURL = process.env.companyconfigsearch;
    logger.info('comConfigSearch URL : ' + comConfigSearchURL + domain + '&caller=air');
    axios.get(comConfigSearchURL + domain + '&caller=air',{ headers: { Accept: 'text/json' } })
    .then(function (response) {
        if(response !== "" && response !== undefined &&        
        response.status !== "" &&
        response.status !== undefined &&
        response.status === 200){
        var resp = response.data;
        resp.status = response.status;
        resp.msg =  response.statusText;
        logger.info('Response Code for Company Details : ' + JSON.stringify(resp));
        cb(resp);
        }else{
            resp = {
                status: '404',
                msg:'Resource Not Found'
            }
            logger.info('Response Code for Company Details : ' + JSON.stringify(resp));
            cb(resp);

        }
    }).catch(function (error) {
        var resp = '';
        resp = {
            status: '404',
            msg:'Resource Not Found'
        }
        logger.info('Response Code for Company Details : ' + JSON.stringify(resp));
        logger.error('error in getCompanyDetails is : ' + JSON.stringify(error));
        cb(resp);
    });
}

exports.updateCompanyDetails = function (req, cb){
    const companyId = req.body.companyId;
    const companyData = req.body;
    const companyupdateservice=process.env.companyupdateservice;
    
    logger.info('CompanyNodeService.js updateCompanyDetails api START for companyId : ' + companyId);

    var cmpReq={
        "company": companyData.company
    }
    logger.info("Company Config Data Update Request : " + JSON.stringify(cmpReq));
    
    axios.post(companyupdateservice + companyId, cmpReq,
            { headers: { Accept: 'text/json' } })
    .then(function (response) {
        if(response !== "" && response !== undefined &&        
        response.status !== "" &&
        response.status !== undefined &&
        response.status === 200){
        var resp = response.data;
        resp.status = response.status;
        resp.msg =  response.statusText;
        logger.info('Response Code for Company Details Update 1---' + JSON.stringify(resp));
        cb(resp);
        }else{

            var resp = '';
            this.resp = {
                status: '404',
                msg:'Resource Not Found'
            }
            logger.info('Response Code for Company Details Update is 2---' + JSON.stringify(resp));
            cb(resp);
            
        }
    }).catch(function (error) {
        console.log(error);
        var resp = '';
        this.resp = {
            status: '404',
            msg:'Resource Not Found'
        }
        logger.error('Company Config Data Update Succesfull. Response Code : ' + JSON.stringify(error));
        logger.info('Response Code for Company Details Update in catchblock---' + JSON.stringify(resp));
        cb(resp);
    });
}