const winston = require('winston');
const Paymentmodel = require('../models/cornTask-model.js')
const { sillyLogConfig } = require('../../helper/logger').winston;
const circularJSON = require('circular-json');
// Create the logger
const logger = winston.createLogger(sillyLogConfig);
createcornTask = (req) => {
    try{
        const body = req.body
        logger.info("sdfcscs"+circularJSON.stringify(body))
            if (!body) {
                logger.info("You must provide transaction detail")
                }
            
            //let cornTask=null;
            let paymentTxn = new Paymentmodel(body);
             
            if (!paymentTxn) {
                logger.info("You must payment transaction detail")
            }
            
            paymentTxn
                .save()
                .then(() => {
                    logger.info("transaction with trip ref"+body.trip_ref)
                    })
                
                .catch(error => {
                    logger.info("transaction is not persisted due to"+error)
                    })
    }catch(error){
        logger.error("createcornTask failed due to error===+"+error)
    }
    
        
}

updatecornTask = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    cornTask.findOne({ trip_ref: req.params.trip_ref }, (err, cornTask) => {
        if (err) {
            return res.status(404).json({
                err,
                message: 'corn task not found!',
            })
        }
        cornTask.trip_ref = body.trip_ref
        cornTask.time = body.time
        cornTask.txn_id = body.txn_id
        cornTask
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: cornTask._id,
                    message: 'cornTask updated!',
                })
            })
            .catch(error => {
                logger.info("error is"+error)
            })
    })
}

deletecornTask = async (req) => {
    await Paymentmodel.findOneAndDelete({ trip_ref: req }, (err, paymentTxn) => {
        if (err) {
            throw err
        }

        if (!paymentTxn) {
            logger.info('DB is empty')
        }

        return "trip with ref"+req+"deleted"
    }).catch(err => logger(err))
}

getcornTaskById = async (req, res) => {
    await cornTask.findOne({ _id: req.params.id }, (err, cornTask) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!cornTask) {
            return res
                .status(404)
                .json({ success: false, error: `Movie not found` })
        }
        return res.status(200).json({ success: true, data: cornTask })
    }).catch(err => console.log(err))
}

getcornTasks = async () => {
    //let res={};
  let txns =  await Paymentmodel.find({}, (err, paymentTxn) => {
        if (err) {
            throw  err
        }
         
    }).catch(err => console.log(err))

    return txns
    
}

module.exports = {
    createcornTask,
    updatecornTask,
    deletecornTask,
    getcornTasks,
    getcornTaskById,
}