const router = require("express").Router();
const cancelService = require("../services/trip/cancel/cancel.service");
const circularJSON = require("circular-json");

//below function service call for get the Refund info
router.post("/refundInfo", async (req, res) => {
    try {
      const response = await cancelService.refundInfo(req);
      res.send(response);
    } catch (err) {
      const getTripRes = {
        status: "404",
        msg:'exception',
        data:''
      };
      res.send(getTripRes);
    }
  });

  //below function service call for Cancellation
  router.post("/autoCancel", async (req, res) => {
    console.log('/autoCancel---'+circularJSON.stringify(req));
    //logger.info('/autoCancel---'+circularJSON.stringify(req));
    try {
      const response = await cancelService.makeCancellation(req);
      res.send(response);
    } catch (err) {      
      const getTripRes = {
        status: "404",
        msg:'exception',
        data:''
      };
      res.send(getTripRes);
    }
  });

  //below function service call for to get the Express way response
router.post("/expressCardDetails", async (req, res) => {
  try {
    const response = await cancelService.expressWayDetails(req);
    res.send(response);
  } catch (err) {
    const getTripRes = {
      status: "404",
      description: JSON.stringify(error)
    };
    res.send(getTripRes);
  }
});

//below function service call for to check User having API KEY or NOt
router.post("/userApiKey", async (req, res) => {
  try {
    const response = await cancelService.usrApiKeyData(req);
    res.send(response);
  } catch (err) {
    const getTripRes = {
      status: "404",
      description: JSON.stringify(error)
    };
    res.send(getTripRes);
  }
}); 

  module.exports = router;
  