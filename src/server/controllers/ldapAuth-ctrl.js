const winston = require('winston');
const Ldapmodel = require('../models/ldapTask-model.js')
const { sillyLogConfig } = require('../../helper/logger').winston;
// Create the logger
const logger = winston.createLogger(sillyLogConfig);

createLdapToken = async (req) => {
    const body = req.body
    logger.info("Ldap Mongo request..."+JSON.stringify(body))
    if (!body) {
        logger.info("You must provide transaction detail")
        }
    let ldapTxn = new Ldapmodel(body);
    if (!ldapTxn) {
        throw err
    }
    await ldapTxn
        .save()
        .then(() => {
            logger.info("transaction with trip ref"+body.username)
            })
        
        .catch(error => {
            logger.info("transaction is not persisted due to"+error)
            })
        
}


getLdapTokenByUsrId = async (req, res) => {
    logger.info("getLdapTokenByUsrId start..."+req.body.token_id);
  let ldapKey =  await Ldapmodel.findOne({ username: req.body.username }, (err, Ldapmodel) => {
        if (err) {
            throw  err
        }
    }).catch(err => console.log(err))
    logger.info("getLdapTokenByUsrId start..."+ldapKey.token_id);
    return ldapKey.token_id
    
}



module.exports = {
    createLdapToken,
    getLdapTokenByUsrId
}
