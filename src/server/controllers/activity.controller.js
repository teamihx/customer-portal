const router = require("express").Router();
const activityService = require("../services/trip/activity/activity.service");

//below function service call for Emailtripdetails
router.post("/sendEmailDetails", async (req, res) => {
  const body = req.body;
  try {
    const response = await activityService.sendActivityEmailDetail(body);
    res.send(response);
  } catch (err) {
    const getTripRes = {
      status: "404",
      description: JSON.stringify(error)
    };
    //console.log("in controller catch");
    res.send(getTripRes);
  }
});

//below function Hq redirection for Emailtripdetails
router.post("/sendEmail", async (req, res) => {
  try {
    const response = await activityService.sendActivityEmail(req);
    res.send(response);
  } catch (err) {
    const getTripRes = {
      status: "404",
      msg: "Mail Not Sent",
      data: ""
    };
    res.send(getTripRes);
  }
});

//below function get the SMS link
router.post("/activitySMSlink", async (req, res) => {
  //console.log("entered in activity controller activitySMSlink --");
  try {
    const response = await activityService.activitySMSLink(req);
    res.send(response);
  } catch (err) {
    const getTripRes = {
      status: "404",
      msg: "link not avaliable",
      data: ""
    };
    res.send(getTripRes);
  }
});

//below function send the sms
router.post("/sendActivitySMS", async (req, res) => {
  try {
    const response = await activityService.sendActivitySMS(req);
    res.send(response);
  } catch (err) {
    const getTripRes = {
      status: "404",
      msg: "SMS Not Sent",
      data: ""
    };
    res.send(getTripRes);
  }
});

//below function send activityEmailInvoice
router.post("/activityEmailInvoice", async (req, res) => {
  try {
    const response = await activityService.sendEmailInvoice(req);
    res.send(response);
  } catch (err) {
    const getTripRes = {
      status: "404",
      msg: "Email Not Sent",
      data: ""
    };
    res.send(getTripRes);
  }
});

module.exports = router;
