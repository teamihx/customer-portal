const router = require("express").Router();
const trainService = require("../services/trip/train/train.service");

//below function service call for Emailtripdetails
router.post("/sendTrainSMS", async (req, res) => {
    try {
      const response = await trainService.sendTrainSMSDetail(req);
      res.send(response);
    } catch (err) {
      const getTripRes = {
        status: "404",
        description: JSON.stringify(error)
      };
      //console.log("in controller catch");
      res.send(getTripRes);
    }
  });

  router.post("/getStationName", async (req, res) => {
    try {
      const response = await trainService.getStationNameByCode(req);
      res.send(response);
    } catch (err) {
      const getTripRes = {
        status: "404",
        description: JSON.stringify(error)
      };
     // console.log("in controller catch");
      res.send(getTripRes);
    }
  });

  module.exports = router;
  