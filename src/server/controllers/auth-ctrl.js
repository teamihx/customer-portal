const router = require("express").Router();
const authService = require("../services/authentication/auth.service");
const envi = process.env.NODE_ENV;
const circularJSON = require('circular-json');
const winston = require("winston");
const { sillyLogConfig } = require("../../helper/logger.js").winston;
const logger = winston.createLogger(sillyLogConfig);

const responseCreator = (status, msg, data) => {
  const obj = {
    status,
    msg,
    data
  };
  return obj;
};

router.post("/insertUserMongo", async (req, res) => {
  logger.info("insertUserMongo----auth.controller start");
  try {
    const response = await authService.inserUserData(req);
    if (response !== undefined && response !== "" &&   response.status != undefined &&   
      response.status === 200 && response.msg ==='data available' && response.data !== undefined && response.data !== '') {
	logger.info("insertUserMongo----user available adding cookies "+response.data);
	
	 if (envi === 'development') { 
		
		res.cookie('u_session_id', response.data);       
		
		logger.info("insertUserMongo----user available added cookies ---- ");
      } else {
        res.cookie('u_session_id', response.data, {
          secure: true,
          httpOnly: true,
          path: '/',
          domain: req.headers.host
        });
      }
	
	}
    logger.info("insertUserMongo----auth.controller" + circularJSON.stringify(response.data));
    res.send(response);
  } catch (err) {
    const returnValue = responseCreator(404, "catch block response(exception)");
    logger.info("in controller catch"+err);
    res.send(returnValue);
  }
  logger.info("insertUserMongo insertUserMongo----auth.controller end");
});

router.post("/checkIfAuthUser", async (req, res) => {
  logger.info("checkIfAuthUser----auth.controller start");
  try {
    const response = await authService.checkIfLDAPAuthAvailable(req);
    logger.info("checkIfAuthUser----Response" + circularJSON.stringify(response));
    res.send(response);
    //res.statusCode(response.status).send(response);
  } catch (err) {
    const returnValue = responseCreator(404, "catch block response(exception)");
    logger.info("checkIfAuthUser in controller catch");
    res.send(returnValue);
  }
  logger.info("checkIfAuthUser----auth.controller end");
});

router.post("/validateLDAP", async (req, res) => {
  logger.info("validateLDAP----auth.controller start " + envi);
  try {
    const response = await authService.checkUserLDAPService(req);
    logger.info("validateLDAP----auth.controller ");
   
    
    if (response !== null && response.status !== undefined && response.status === 200) {      
      var tokenEncodeValue = response.data;
      logger.info("validateLDAP----auth.controller env " + tokenEncodeValue);       
      if (envi === 'development') {       
        res.cookie('u_session_id', tokenEncodeValue);
      } else if (envi === 'preprod') {
        res.cookie('u_session_id', tokenEncodeValue, {
          secure: true,
          httpOnly: true,
          path: '/',
          domain: req.headers.host
        });
      } else if (envi === 'production') {
        res.cookie('u_session_id', tokenEncodeValue, {
          secure: true,
          httpOnly: true,
          path: '/',
          domain: req.headers.host
        });
      }
    }


    res.send(response);
  } catch (err) {
    const returnValue = responseCreator(404, "catch block response(exception)");
    logger.info("Exception in controller catch " + err);
    res.send(returnValue);
  }
  logger.info("validateLDAP----auth.controller end");
});


module.exports = router;
