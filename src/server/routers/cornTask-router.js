const express = require('express')

const CornTaskCtrl = require('../controllers/cornTask-ctrl')

const router = express.Router()

router.post('/insertPaymentTxn', CornTaskCtrl.createcornTask)
router.put('/paymentTxn/:id', CornTaskCtrl.updatecornTask)
router.delete('/deletePaymentTxn/:id', CornTaskCtrl.deletecornTask)
router.get('/paymentTxn/:id', CornTaskCtrl.getcornTaskById)
router.get('/getPaymentTxns', CornTaskCtrl.getcornTasks)

module.exports = router