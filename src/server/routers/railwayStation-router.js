const express = require('express')

const railwayCtrl = require('../controllers/railway-ctrl')

const router = express.Router()

router.post('/insertRailwayStation', railwayCtrl.insertRailwayStation)


module.exports = router