var CompanyNodeService = require('./services/company/CompanyNodeService.js');

var cors = require('cors');
const express = require('express');

const os = require('os');
const axios = require('axios');
const JSON = require('circular-json');
var cron = require('node-cron');
const bodyParser = require('body-parser');
var AuthNodeService = require('./services/authentication/AuthNodeService.js')
var GstNodeService = require('./services/company/GstNodeService.js')
var TripService = require('./services/trip/TripService.js')
var FlightNodeService = require('./services/trip/flight/FlightNodeService.js')
var EmailNodeService = require('./services/trip/email/EmailNodeService.js')
var NoteNodeService = require('./services/trip/note/NoteNodeService.js')
var UserNodeService = require('./services/trip/user/UserNodeService')
const publicIp = require('public-ip');

var paymentLinkSchedule = require('./services/mongoDbServices/paymentLinkSchedule')

var cornTaskRouter = require('./routers/cornTask-router')
const { createDomainpath } = require('./utils/domainpath.js');
const app = express();
var path = require("path");
let cookieParser = require('cookie-parser'); 
app.use(cookieParser());


const winston = require('winston');
// App settings
const { sillyLogConfig } = require('../helper/logger.js').winston;

// Create the logger
const logger = winston.createLogger(sillyLogConfig);
//const bodyParser = require('body-parser')


/* app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); */
app.use(cors());

app.use(express.static('dist'));
app.use(bodyParser.json({limit: '50mb' }));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use('/'+process.env.contextpath, cornTaskRouter)
if(process.env.NODE_ENV==='development'){
    require('dotenv').config();

}else if(process.env.NODE_ENV==='preprod'){
    require('dotenv').config({path:__dirname+'/./../../.env.preproduction'});

}else if(process.env.NODE_ENV==='production'){
    require('dotenv').config({path:__dirname+'/./../../.env.production'});
	require('newrelic');

}

axios.interceptors.request.use(request => {
	if(request !== null && request != 'undefined' && request.url != null){		
		logger.info('INTERCEPTORS REQUEST : '+JSON.stringify(request))
	}
  
  return request
})

axios.interceptors.response.use(response => {
	if(response !== null && response != 'undefined' && response.request != null && response.request.path !== null && response.data != null){
		if(response.request.path.includes('/trips?tripID')){
			logger.info('INTERCEPTORS RESPONSE TRIP SERVICE: STATUS: '+ response.status );
		}else{
			logger.info('INTERCEPTORS RESPONSE: STATUS: '+ response.status+" : res "+ JSON.stringify(response.data))
		}
		
	}
   
  return response
})

console.log('process.env is---'+process.env.environment);
console.log('process.env.NODE_ENV is---'+process.env.NODE_ENV);
var db = require('./db');


  app.post (process.env.contextpath+'/api/logger', function( req, res, next ) {
    //console.log('entered logger--'+JSON.stringify(req));
    {req.body.logs.map((item, key) => {
  logger.log(item.level || 'error',item.format);
  })}

  return res.send( 'OK' );

});
db.on('error', console.error.bind(console, 'MongoDB connection error:'))


app.get(
    '/api/getUsername', 
    (req, res) => res.send({ username: os.userInfo().username })
);

 app.post(process.env.contextpath+'/api/getPublicIp', (req, res) =>{
  //console.log('/api/getPublicIp')
  publicIp.v4().then(ip => {
    //console.log("your public ip address", ip);
    res.json({ ip_address: ip });
  }).catch((error) => {
    assert.isNotOk(error,'Promise error');
  });
     
     
  });

 
 


 

app.post(process.env.contextpath+'/api/getCmpConfByDomain', (req, res) => {
    logger.info('index.js /api/getCmpConfByDomain START');
    CompanyNodeService.getCompanyDetails(req, function(response){
        var resp = response;
        logger.info('index.js /api/getCmpConfByDomain END');
        res.json(resp);
    });
});

app.post(process.env.contextpath+'/api/updateCompConf', (req, res) => {
    logger.info('index.js /api/updateCompConf START');
    CompanyNodeService.updateCompanyDetails(req, function(response){
        var resp = response;
        logger.info('index.js /api/updateCompConf END');
        res.json(JSON.stringify(resp));
    });
});

app.post(process.env.contextpath+"/api/checkUsrLogin", (req, res) => {
    logger.info('index.js /api/checkUsrLogin START');
    logger.info("Dynamic Domain path "+createDomainpath(req));
    AuthNodeService.authenticateUser(req, function(response){
    try {
    if(response.status!==null && response.status!=="" && response.status===200){
    var envirnment = process.env.environment;
    var resp =  AuthNodeService.getAuthKey(req);
    if(resp){
        //logger.info('AUTH KEY IS AVAILABLE.......'+resp);
    }else{
      var userId=response.headers['set-cookie'][3];
      var str=userId.split('=');
      var date=str[4];
    if(envirnment ==='development'){
      logger.info('Start add cookie DEV...');
      var apache=response.headers['set-cookie'][0];
      var str=apache.split('=');
      res.cookie('Apache', str[1],{expires: new Date(date)});

      var userId=response.headers['set-cookie'][3];
      res.cookie('userid', str[1],{expires: new Date(date)});

       var ctAuth=JSON.stringify(response.headers['set-cookie'][1]);
       var str=ctAuth.split(';');
       var str1=str[0];
       var keys=str1.split('=');
       var ctkey = keys[1]
       //logger.info("ADD COOKIE CT-AUTH--: " +ctkey);
       res.cookie('ct-auth', ctkey,{expires: new Date(date),encode:String});
     
       var usr=response.headers['set-cookie'][2];
       var str=usr.split('=')
       res.cookie('usermisc', str[1],{expires: new Date(date)});
     
      var authPref=response.headers['set-cookie'][4];
      var str=authPref.split('=');
      res.cookie('ct-auth-preferences', str[1],{expires: new Date(date)}); 
     
       var currency=response.headers['set-cookie'][5];
       var str=currency.split('=');
       res.cookie('currency-pref', str[1],{expires: new Date(date)});
      
       var session=response.headers['set-cookie'][6];
       var str=session.split('='); 
       res.cookie('_session_id', str[1],{expires: new Date(date)});  

      }else if(envirnment ==='preprod'){
        var apache=response.headers['set-cookie'][0];
        var str=apache.split('=');
        res.cookie('Apache', str[1], {
          secure: true,
          httpOnly: true,
          path: '/',
          expires: new Date(date)
        });
  
        var userId=response.headers['set-cookie'][3];
        res.cookie('userid', str[1], {
          secure: true,
          httpOnly: true,
          path: '/',
          expires: new Date(date)
        });
  
        var ctAuth=JSON.stringify(response.headers['set-cookie'][1]);
        var str=ctAuth.split(';');
        var str1=str[0];
        var keys=str1.split('=');
        var ctkey = keys[1]
         //logger.info("ADD COOKIE CT-AUTH" +ctkey);
         logger.info("req.headers.host " +req.headers.host);
         res.cookie('ct-auth', ctkey, {
          secure: true,
          httpOnly: true,
          path: '/',
          expires: new Date(date),
          encode:String
        });
        
        
         
         var usr=response.headers['set-cookie'][2];
         var str=usr.split('=');
         res.cookie('usermisc', str[1], {
          secure: true,
          httpOnly: true,
          path: '/',
          expires: new Date(date)
        }); 
        
        var authPref=response.headers['set-cookie'][4];
        var str=authPref.split('=');
        res.cookie('ct-auth-preferences', str[1], {
          secure: true,
          httpOnly: true,
          path: '/',
          expires: new Date(date)
        }); 
  
         var currency=response.headers['set-cookie'][5];
         var str=currency.split('=');
         res.cookie('currency-pref', str[1], {
          secure: true,
          httpOnly: true,
          path: '/',
          expires: new Date(date)
        }); 
         
         var session=response.headers['set-cookie'][6];
         var str=session.split('=');
         res.cookie('_session_id', str[1], {
          secure: true,
          httpOnly: true,
          path: '/',
          expires: new Date(date)
        });
        //logger.info("after adding ct-auth response is---" +JSON.stringify(res));
        }else if(envirnment ==='production'){
            var apache=response.headers['set-cookie'][0];
            var str=apache.split('=');
            res.cookie('Apache', str[1], {
              secure: true,
              httpOnly: true,
              path: '/',
              expires: new Date(date)
            });
      
            var userId=response.headers['set-cookie'][3];
            res.cookie('userid', str[1], {
              secure: true,
              httpOnly: true,
              domain: req.headers.host,
              path: '/',
              expires: new Date(date)
            });
      
            var ctAuth=JSON.stringify(response.headers['set-cookie'][1]);
            var str=ctAuth.split(';');
            var str1=str[0];
            var keys=str1.split('=');
            var ctkey = keys[1]
             //logger.info("ADD COOKIE CT-AUTH" +ctkey);
             res.cookie('ct-auth', ctkey, {
              secure: true,
              httpOnly: true,
              domain: req.headers.host,
              path: '/',
              expires: new Date(date),
              encode:String
            });
             
             var usr=response.headers['set-cookie'][2];
             var str=usr.split('=');
             res.cookie('usermisc', str[1], {
              secure: true,
              httpOnly: true,
              domain: req.headers.host,
              path: '/',
              expires: new Date(date)
            }); 
            
            var authPref=response.headers['set-cookie'][4];
            var str=authPref.split('=');
            res.cookie('ct-auth-preferences', str[1], {
              secure: true,
              httpOnly: true,
              domain: req.headers.host,
              path: '/',
              expires: new Date(date)
            }); 
      
             var currency=response.headers['set-cookie'][5];
             var str=currency.split('=');
             res.cookie('currency-pref', str[1], {
              secure: true,
              httpOnly: true,
              domain: req.headers.host,
              path: '/',
              expires: new Date(date)
            }); 
             
             var session=response.headers['set-cookie'][6];
             var str=session.split('=');
             res.cookie('_session_id', str[1], {
              secure: true,
              httpOnly: true,
              domain: req.headers.host,
              path: '/',
              expires: new Date(date)
            }); 
      }
      }
     }
     } catch(err) {
        logger.info('Sign in response add cookie error due to : '+err);
     }
    var resp = response;
    res.json(JSON.stringify(resp));
    logger.info('index.js /api/checkUsrLogin END');
    });
});


app.post(process.env.contextpath+"/api/checkUsrAuthorzation", (req, res) => {
    logger.info('index.js /api/checkUsrAuthorzation START');
    AuthNodeService.userAuthrization(req, function(response){
        var resp = response;
        logger.info('index.js /api/checkUsrAuthorzation END');
        res.json(JSON.stringify(resp));
    });
});

app.post(process.env.contextpath+"/api/getGstCompanyData", (req, res) => {
    logger.info('index.js /api/getGstCompanyData START');
    GstNodeService.getGstCompanySearch(req, function(response){
        var resp = response;
        logger.info('index.js /api/getGstCompanyData END');
        res.json(JSON.stringify(resp));
    });
});

app.post(process.env.contextpath+"/api/editGstCompanyData", (req, res) => {
    logger.info('index.js /api/editGstCompanyData START');
    GstNodeService.editGstCompany(req, function(response){
        var resp = response;
        logger.info('index.js /api/editGstCompanyData END');
        res.json(JSON.stringify(resp));
    });
});

app.post(process.env.contextpath+"/api/deleteGstCompanyData", (req, res) => {
    logger.info('index.js /api/deleteGstCompanyData START');
    GstNodeService.deleteGstDetails(req, function(response){
        var resp = response;
        logger.info('index.js /api/deleteGstCompanyData END');
        res.json(JSON.stringify(resp));
    });
});

app.post(process.env.contextpath+"/api/addGstCompanyData", (req, res) => {
    logger.info('index.js /api/addGstCompanyData START');
    GstNodeService.addGstCompanyData(req, function(response){
        var resp = response;
        logger.info('index.js /api/addGstCompanyData END');
        res.json(JSON.stringify(resp));
    });
});

app.post(process.env.contextpath+"/api/getAuthToken", (req, res) => {
   var resp =  AuthNodeService.getAuthKey(req);
   //logger.info('GET AUTH KEY RESPONSE.......'+resp);
    res.json(resp);
});

app.post(process.env.contextpath+"/api/getTripDetail", (req, res) => {
    logger.info('index.js /api/getTripDetail START');
    TripService.getTripDetail(req, function(response){
        var resp = response;
        logger.info('index.js /api/getTripDetail END');
        res.json(JSON.stringify(resp));
    });
});

app.post(process.env.contextpath+"/api/logout", (req,res) => {
    logger.info('LOGOUT.......... start1');
    logger.info('logout js refer is-----'+JSON.stringify(req.headers.referer));
	
	var authkey =  AuthNodeService.getAuthKey(req);
	//logger.info('LOGOUT.......... auth'+authkey);
	AuthNodeService.logout(req,res);
		
   logger.info('LOGOUT.......... end');
  res.json(authkey);
    
 });

 app.post(process.env.contextpath+"/api/getTripMasterDetail", (req, res) => {
  logger.info('index.js /api/getTripDetail START');
  TripService.getAirMasterData(req, function(response){
      var resp = response;
      logger.info('index.js /api/getTripMasterDetail END');
      res.json(JSON.stringify(resp));
  });
})  

app.post(process.env.contextpath+"/api/getAirportMasterDetail", (req, res) => {
  logger.info('index.js /api/getTripDetail START');
  TripService.getAirPortMasterData(req, function(response){
      var resp = response;
      logger.info('index.js /api/getTripMasterDetail END');
      res.json(JSON.stringify(resp));
  });
})

app.post(process.env.contextpath+"/api/upateAirTicketapi", (req, res) => {  
  logger.info('index.js /api/upateAirTicketapi start');
  FlightNodeService.upateAirTicketapi(req, function(response){
      var resp = response;      
      logger.info('index.js /api/upateAirTicketapi end');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+"/api/updatePaxDetails", (req, res) => {
  logger.info('index.js /api/updatePaxDetails START');
  FlightNodeService.updatePaxDetails(req, function(response){
      var resp = response;
      logger.info('index.js /api/updatePaxDetails END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+"/api/upateNotesApi", (req, res) => {  
  logger.info('index.js /api/upateNotesApi start');

  try{
  NoteNodeService.upateNotesApi(req, function(response){
      var resp = response;      
      logger.info('index.js /api/upateNotesApi end');
      res.json(JSON.stringify(resp));
  });
}catch(err){
  logger.error('error in upateNotesApi-----'+err);

}
});

app.post(process.env.contextpath+"/api/sendFlightDetailEmailApi", (req, res) => {  
  logger.info('index.js /api/sendFlightDetailEmailApi start');
  EmailNodeService.sendFlightDetailEmail(req, function(response){
      var resp = response;      
      logger.info('index.js /api/sendFlightDetailEmailApi end');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+"/api/sendFlightSMS", (req, res) => {
  logger.info('index.js /api/sendFlightSM START');
  FlightNodeService.sendFlightSMS(req, function(response){
      var resp = response;
      logger.info('index.js /api/sendFlightSMS END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+"/api/getSMSLink", (req, res) => {
  logger.info('index.js /api/getSMSLink START');
  FlightNodeService.getSMSLink(req, function(response){
      var resp = response;
      logger.info('index.js /api/getSMSLink END');
      res.json(JSON.stringify(resp));
  });
});
app.post(process.env.contextpath+"/api/getSMSCancelLink", (req, res) => {
  logger.info('index.js /api/getSMSCancelLink START');
  FlightNodeService.getSMSCancelLink(req, function(response){
      var resp = response;
      logger.info('index.js /api/getSMSCancelLink END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+"/api/fetchUserMailid", (req, res) => {  
  logger.info('index.js /api/fetchUserMailid start');

  try{
    UserNodeService.fetchUserMailid(req, function(response){
      var resp = response;      
      logger.info('index.js /api/fetchUserMailid end');
      res.json(JSON.stringify(resp));
  });
}catch(err){
  logger.error('error in fetchUserMailid-----'+err);

}
});




app.post(process.env.contextpath+"/api/getPromotions", (req, res) => {
  logger.info('index.js /api/getPromotions START');
  TripService.getWalletPromotions(req, function(response){
      var resp = response;
      logger.info('index.js /api/getPromotions END');
      res.json(JSON.stringify(resp));
  });
});
app.post(process.env.contextpath+"/api/getPaymentLink", (req, res) => {
  logger.info('index.js /api/getPaymentLink START');
  TripService.getPaymentLink(req, function(response){
      var resp = response;
      logger.info('index.js /api/getPaymentLink END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+"/api/closeTxn", (req, res) => {
  logger.info('index.js /api/closeTxn START');
  TripService.closeTxn(req, function(response){
      var resp = response;
      logger.info('index.js /api/closeTxn END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+"/api/offlineRefund", (req, res) => {
  logger.info('index.js /api/offlineRefund START');
  TripService.offlineRefund(req, function(response){
      var resp = response;
      logger.info('index.js /api/offlineRefund END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+"/api/saveTags", (req, res) => {
  logger.info('index.js /api/saveTags START ');
  TripService.saveTagMaster(req, function(response){
      var resp = response;
      logger.info('index.js /api/saveTags END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+'/api/createNewTransaction', (req, res) => {
  logger.info('index.js /api/createNewTransaction START');
  NoteNodeService.createNewTransaction(req, function(response){
      var resp = response;
      logger.info('index.js /api/createNewTransaction END');
      res.json(JSON.stringify(resp));
  });
});
app.post(process.env.contextpath+'/api/insertPayment', (req, res) => {
  logger.info('index.js insertPayment START');
  NoteNodeService.insertPayment(req, function(response){
      var resp = response;
      logger.info('index.js /api/insertPayment END');
      res.json(JSON.stringify(resp));
  });
});
app.post(process.env.contextpath+'/api/fetchUserClassDtl', (req, res) => {
  logger.info('index.js fetchUserClassDtl START');
  UserNodeService.fetchUserClassDtl(req, function(response){
      var resp = response;
      logger.info('index.js /api/fetchUserClassDtl END');
      res.json(JSON.stringify(resp));
  });
})

app.post(process.env.contextpath+'/api/insertPaymentTxn', (req, res) => {
  logger.info('index.js paymentTxn START');
  paymentLinkSchedule.insertPaymentTxn(req, function(response){
      var resp = response;
      logger.info('index.js /api/paymentTxn END');
      res.json(JSON.stringify(resp));
  });
})

cron.schedule('* * * * *', () => {
  paymentLinkSchedule.updateTxnStatus()
  })

app.post(process.env.contextpath+'/api/sendEmail', (req, res) => {
  logger.info('index.js /api/sendEmail START');
  EmailNodeService.sendEmail(req, function(response){
      var resp = response;
      logger.info('index.js /api/createNewTransaction END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+'/api/getFlag', (req, res) => {
  logger.info('index.js /api/sendEmail START');
  TripService.getFlag(req, function(response){
      var resp = response;
      logger.info('index.js /api/getFlag END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+'/api/getWalletCurrencies', (req, res) => {
  logger.info('index.js /api/getWalletCurrencies START');
  TripService.getWalletCurrencies(req, function(response){
      var resp = response;
      logger.info('index.js /api/createNewTransaction END');
      res.json(JSON.stringify(resp));
  });
});


app.post(process.env.contextpath+'/api/fetchWallet', (req, res) => {
  logger.info('index.js /api/getWalletNumbers START');
  TripService.fetchWalletNumbers(req, function(response){
      var resp = response;
      logger.info('index.js /api/getWalletNumbers END');
      res.json(JSON.stringify(resp));
  });
});


app.post(process.env.contextpath+'/api/fetchHotelStatus', (req, res) => {
  logger.info('index.js /api/fetchHotelStatus START');
  TripService.fetchChmmHotelStatus(req, function(response){
      var resp = response;
      logger.info('index.js /api/fetchHotelStatus END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+'/api/getSupplierService', (req, res) => {
  logger.info('index.js /api/getSupplierService START');
  TripService.getFltSuppService(req, function(response){
      var resp = response;
      logger.info('index.js /api/getSupplierService END');
      res.json(JSON.stringify(resp));
  });
});


app.post(process.env.contextpath+'/api/processSyncGDS', (req, res) => {
  logger.info('index.js /api/processSyncGDS START');
  TripService.getProcessSyncGDS(req, function(response){
      var resp = response;
      logger.info('index.js /api/processSyncGDS END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+'/api/processRollbackAmend', (req, res) => {
  logger.info('index.js /api/processRollbackAmend START');
  TripService.processRollbackAmend(req, function(response){
      var resp = response;
      logger.info('index.js /api/processRollbackAmend END');
      res.json(JSON.stringify(resp));
  });
});
app.post(process.env.contextpath+'/api/getCompanyNameForCashPmnt', (req, res) => {
  logger.info('/api/getCompanyNameForCashPmnt START');
  TripService.getCompanyNameForCashPmnt(req, function(response){
      var resp = response;
      logger.info('index.js /api/getCompanyNameForCashPmnt END');
      res.json(JSON.stringify(resp));
  });
});
app.post(process.env.contextpath+'/api/getExpressCheckoutDtl', (req, res) => {
  logger.info('/api/getExpressCheckoutDtl START');
  TripService.getExpressCheckoutDtl(req, function(response){
      var resp = response;
      logger.info('index.js /api/getExpressCheckoutDtl END');
      res.json(JSON.stringify(resp));
  });
});

app.post(process.env.contextpath+'/api/bookPolicy', (req, res) => {
  logger.info('index.js /api/bookPolicy START');
  TripService.getBookingPolicy(req, function(response){
      var resp = response;
      logger.info('index.js /api/bookPolicy END');
      res.json(JSON.stringify(resp));
  });
});

app.get(`/${process.env.contextpath}/healthCheck`, (req,res)=>{
res.send(200);
});

const activityController = require('./controllers/activity.controller');
 app.use(`${process.env.contextpath}/api/trips/activity`,activityController);

const authController = require('./controllers/auth-ctrl');
app.use(`${process.env.contextpath}/api/auth`,authController);

const trainController = require('./controllers/train.controller');
app.use(`${process.env.contextpath}/api/trips/train`,trainController);

const cancelController = require('./controllers/cancel.controller');
app.use(`${process.env.contextpath}/api/trips/cancellation`,cancelController);

app.get('*', function response(req, res) {
  logger.info('get----------'+JSON.stringify(req.originalUrl));
  logger.info('index js Host is----------'+JSON.stringify(req.headers.host));
  logger.info('index js refer is-----'+JSON.stringify(req.headers.referer));
  res.sendFile(path.join(__dirname,'../../dist/index.html'));
});



 


app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));


