const contextPathPrepend = relativePath => (relativePath
  ? `${process.env.contextpath}${relativePath}`
  : process.env.contextpath);

const logMessage = (msg, data) => console.log(data ? `${msg} => ${data}` : msg);

const expiryDate = new Date('Tue Jul 01 2020 06:01:11 GMT-0400 (EDT)');
const cookieArray = () => {
  const cookies = [
    { name: 'Apache', cookie_seq: 0 },
    { name: 'usermisc', cookie_seq: 2 },
    { name: 'userid', cookie_seq: 3 },
    { name: 'ct-auth-preferences', cookie_seq: 4 },
    { name: 'currency-pref', cookie_seq: 5 },
    { name: '_session_id', cookie_seq: 6 },
  ];
  return cookies;
};
const ctAuthValue = (cookie) => {
  const ctAuth = JSON.stringify(cookie[1]);
  const str = ctAuth.split(';');
  const str1 = str[0];
  const keys = str1.split('=');
  const ctkey = keys[1];
  return ctkey;
};

const responseCreator = (response) => {
  const resp = response;
  return resp ? JSON.stringify(resp) : null;
};
module.exports = {
  contextPathPrepend,
  logMessage,
  cookieArray,
  expiryDate,
  ctAuthValue,
  responseCreator,
};
