const mongoose = require('mongoose');
mongoose.set('debug', true);
const winston = require('winston');
const { sillyLogConfig } = require('../../helper/logger.js').winston;
const logger = winston.createLogger(sillyLogConfig);
//console.log('This is before connecting to mongo')
console.log(process.env.mongoConnction)
mongoose
    .connect('mongodb:'+process.env.mongoConnction, { useNewUrlParser: true }).then(()=>logger.info('Connected to mongo'))
    .catch(e => {
        
        logger.error('logger Connection error2---', e);
    })

const db = mongoose.connection

module.exports = db