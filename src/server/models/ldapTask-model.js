const mongoose = require('mongoose')
const Schema = mongoose.Schema

const LdapTxnSchema = new Schema(
    {
        username: { type: String, required: true },
        password: { type: String, required: true },
        token_id: { type: String, required: true },
		public_ip_address: { type: String, required: true },
		client_ip_address: { type: String, required: true }
    },
    { timestamps: true },
)
module.exports = mongoose.model('LdapToken', LdapTxnSchema)