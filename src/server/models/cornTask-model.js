const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PaymentTxnSchema = new Schema(
    {
        trip_ref: { type: String, required: true },
        txn_id: { type: Number, required: true }
    },
    { timestamps: true },
)
module.exports = mongoose.model('PaymentTxn', PaymentTxnSchema)