const mongoose = require('mongoose')
const Schema = mongoose.Schema

const railwaySchema = new Schema(
    {
        code: { type: String, required: true },
        name: { type: String, required: true },
        place_city_id: { type: String },
        state_id: { type: String },
        lattitude: { type: String },
        longitude: { type: String },
        dispalyname: { type: String }
    },
    { timestamps: true },
)

module.exports = mongoose.model('railwayStationMaster', railwaySchema)