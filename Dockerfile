#bo-portal
FROM cleartrip/nodebase:12.13
MAINTAINER "Arun N" <arun.n@cleartrip.com>
WORKDIR /opt/app
ADD . /opt/app
RUN mkdir -p /opt/app/log
#ADD build /opt/app/build
#ADD build/package.json /opt/app/
COPY newrelic /opt/newrelic
EXPOSE 9080
CMD ["/usr/bin/supervisord"]
